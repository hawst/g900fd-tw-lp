.class public Lcom/bzbyte/util/audit/GlobalAuditLog;
.super Ljava/lang/Object;
.source "GlobalAuditLog.java"


# static fields
.field private static ivGlobal:Lcom/bzbyte/util/audit/GlobalAuditLog;


# instance fields
.field private ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

.field private ivBuffListener:Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;

.field private ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

.field private ivGlobalAuditLogFactory:Lcom/bzbyte/util/audit/GlobalAuditLogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/bzbyte/util/audit/GlobalAuditLog;

    invoke-direct {v0}, Lcom/bzbyte/util/audit/GlobalAuditLog;-><init>()V

    sput-object v0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivGlobal:Lcom/bzbyte/util/audit/GlobalAuditLog;

    return-void
.end method

.method constructor <init>()V
    .locals 11

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v9, Lcom/bzbyte/util/audit/DefaultAuditLog;

    invoke-direct {v9}, Lcom/bzbyte/util/audit/DefaultAuditLog;-><init>()V

    iput-object v9, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    .line 51
    invoke-static {}, Lcom/bzbyte/util/CommonProperties;->getProps()Lcom/bzbyte/util/CommonProperties;

    move-result-object v8

    .line 52
    .local v8, "props":Lcom/bzbyte/util/CommonProperties;
    invoke-virtual {p0, v8}, Lcom/bzbyte/util/audit/GlobalAuditLog;->setConsoleListener(Lcom/bzbyte/util/CommonProperties;)V

    .line 54
    new-instance v9, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;

    invoke-direct {v9}, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;-><init>()V

    iput-object v9, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivBuffListener:Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;

    .line 55
    iget-object v9, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    iget-object v10, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivBuffListener:Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;

    invoke-interface {v9, v10}, Lcom/bzbyte/util/audit/AuditLog;->addAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V

    .line 57
    invoke-virtual {v8}, Lcom/bzbyte/util/CommonProperties;->getLogDir()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 59
    iget-object v0, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    .line 60
    .local v0, "auditLog":Lcom/bzbyte/util/audit/AuditLog;
    new-instance v2, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-direct {v2, v9, v10}, Ljava/sql/Date;-><init>(J)V

    .line 61
    .local v2, "dt":Ljava/sql/Date;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v9, "MM_dd_yyyy_HH_mm_ss"

    invoke-direct {v4, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 63
    .local v4, "f":Ljava/text/SimpleDateFormat;
    new-instance v5, Ljava/io/File;

    invoke-virtual {v8}, Lcom/bzbyte/util/CommonProperties;->getLogDir()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .local v5, "logDir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_0

    .line 65
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 66
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Log"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ".xml"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 67
    .local v6, "logFileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    .local v1, "destFile":Ljava/io/File;
    new-instance v7, Lcom/bzbyte/util/audit/FileAuditLogListener;

    invoke-direct {v7, v1}, Lcom/bzbyte/util/audit/FileAuditLogListener;-><init>(Ljava/io/File;)V

    .line 69
    .local v7, "ls":Lcom/bzbyte/util/audit/FileAuditLogListener;
    invoke-interface {v0, v7}, Lcom/bzbyte/util/audit/AuditLog;->addAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V

    .line 70
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "File log inited:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, p0, v9}, Lcom/bzbyte/util/audit/AuditLog;->fireInfo(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .end local v0    # "auditLog":Lcom/bzbyte/util/audit/AuditLog;
    .end local v1    # "destFile":Ljava/io/File;
    .end local v2    # "dt":Ljava/sql/Date;
    .end local v4    # "f":Ljava/text/SimpleDateFormat;
    .end local v5    # "logDir":Ljava/io/File;
    .end local v6    # "logFileName":Ljava/lang/String;
    .end local v7    # "ls":Lcom/bzbyte/util/audit/FileAuditLogListener;
    :cond_1
    invoke-static {}, Lcom/bzbyte/util/CommonProperties;->getProps()Lcom/bzbyte/util/CommonProperties;

    move-result-object v9

    invoke-virtual {v9}, Lcom/bzbyte/util/CommonProperties;->getGlobalAuditLogFactory()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 77
    :try_start_0
    invoke-static {}, Lcom/bzbyte/util/CommonProperties;->getProps()Lcom/bzbyte/util/CommonProperties;

    move-result-object v9

    invoke-virtual {v9}, Lcom/bzbyte/util/CommonProperties;->getGlobalAuditLogFactory()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/bzbyte/util/audit/GlobalAuditLogFactory;

    iput-object v9, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivGlobalAuditLogFactory:Lcom/bzbyte/util/audit/GlobalAuditLogFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_2
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v3

    .line 81
    .local v3, "e":Ljava/lang/Exception;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v9}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method public static getLog()Lcom/bzbyte/util/audit/AuditLog;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivGlobal:Lcom/bzbyte/util/audit/GlobalAuditLog;

    invoke-virtual {v0}, Lcom/bzbyte/util/audit/GlobalAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuditLog()Lcom/bzbyte/util/audit/AuditLog;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivGlobalAuditLogFactory:Lcom/bzbyte/util/audit/GlobalAuditLogFactory;

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivGlobalAuditLogFactory:Lcom/bzbyte/util/audit/GlobalAuditLogFactory;

    iget-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    invoke-interface {v0, v1}, Lcom/bzbyte/util/audit/GlobalAuditLogFactory;->getAuditLog(Lcom/bzbyte/util/audit/AuditLog;)Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    goto :goto_0
.end method

.method public setConsoleListener(Lcom/bzbyte/util/CommonProperties;)V
    .locals 3
    .param p1, "props"    # Lcom/bzbyte/util/CommonProperties;

    .prologue
    .line 91
    invoke-virtual {p1}, Lcom/bzbyte/util/CommonProperties;->logToConsole()Z

    move-result v0

    .line 92
    .local v0, "val":Z
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    if-eqz v1, :cond_1

    .line 94
    iget-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    iget-object v2, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    invoke-interface {v1, v2}, Lcom/bzbyte/util/audit/AuditLog;->removeAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V

    .line 95
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    if-nez v1, :cond_0

    .line 99
    invoke-virtual {p1}, Lcom/bzbyte/util/CommonProperties;->getIsConsoleLog()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 101
    new-instance v1, Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    invoke-direct {v1}, Lcom/bzbyte/util/audit/ConsoleAuditLogListener;-><init>()V

    iput-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    .line 107
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivAuditLog:Lcom/bzbyte/util/audit/AuditLog;

    iget-object v2, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    invoke-interface {v1, v2}, Lcom/bzbyte/util/audit/AuditLog;->addAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V

    goto :goto_0

    .line 103
    :cond_3
    invoke-virtual {p1}, Lcom/bzbyte/util/CommonProperties;->getIsLog4jLog()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    new-instance v1, Lcom/bzbyte/util/audit/Log4jAuditLogListener;

    invoke-direct {v1}, Lcom/bzbyte/util/audit/Log4jAuditLogListener;-><init>()V

    iput-object v1, p0, Lcom/bzbyte/util/audit/GlobalAuditLog;->ivConsole:Lcom/bzbyte/util/audit/ConsoleAuditLogListener;

    goto :goto_1
.end method
