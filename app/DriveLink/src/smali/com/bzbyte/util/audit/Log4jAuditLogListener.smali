.class public Lcom/bzbyte/util/audit/Log4jAuditLogListener;
.super Lcom/bzbyte/util/audit/ConsoleAuditLogListener;
.source "Log4jAuditLogListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/bzbyte/util/audit/ConsoleAuditLogListener;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method public processAuditLogEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    .locals 2
    .param p1, "ev"    # Lcom/bzbyte/util/audit/AuditLogEvent;

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getSourceClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    .line 28
    .local v0, "logger":Lorg/apache/log4j/Logger;
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->isError()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 31
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    .line 32
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getException()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 34
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getException()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v1}, Lcom/bzbyte/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;)V

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->isWarning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_2
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->isInfo()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 43
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :cond_3
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/log4j/Logger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method
