.class public Lcom/bzbyte/util/audit/DefaultAuditLog;
.super Ljava/lang/Object;
.source "DefaultAuditLog.java"

# interfaces
.implements Lcom/bzbyte/util/audit/AuditLog;


# instance fields
.field private ivListeners:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/audit/DefaultAuditLog;->ivListeners:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/bzbyte/util/audit/AuditLogListener;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/bzbyte/util/audit/DefaultAuditLog;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    return-void
.end method

.method public fireDebug(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v0, Lcom/bzbyte/util/audit/AuditLogEvent;

    const/4 v1, 0x3

    invoke-direct {v0, p1, v1, p2}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/audit/DefaultAuditLog;->fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V

    .line 61
    return-void
.end method

.method public fireError(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/bzbyte/util/audit/AuditLogEvent;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1, p2}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/audit/DefaultAuditLog;->fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V

    .line 41
    return-void
.end method

.method public fireError(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "th"    # Ljava/lang/Throwable;

    .prologue
    .line 45
    new-instance v0, Lcom/bzbyte/util/audit/AuditLogEvent;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1, p2, p3}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/audit/DefaultAuditLog;->fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V

    .line 46
    return-void
.end method

.method public fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    .locals 4
    .param p1, "ev"    # Lcom/bzbyte/util/audit/AuditLogEvent;

    .prologue
    .line 66
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    iget-object v3, p0, Lcom/bzbyte/util/audit/DefaultAuditLog;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/bzbyte/util/audit/DefaultAuditLog;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/bzbyte/util/audit/AuditLogListener;

    .line 71
    .local v2, "listener":Lcom/bzbyte/util/audit/AuditLogListener;
    :try_start_0
    invoke-interface {v2, p1}, Lcom/bzbyte/util/audit/AuditLogListener;->processAuditLogEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v3}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_1

    .line 79
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "listener":Lcom/bzbyte/util/audit/AuditLogListener;
    :cond_0
    return-void
.end method

.method public fireInfo(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v0, Lcom/bzbyte/util/audit/AuditLogEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/audit/DefaultAuditLog;->fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V

    .line 51
    return-void
.end method

.method public fireWarning(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v0, Lcom/bzbyte/util/audit/AuditLogEvent;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1, p2}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/audit/DefaultAuditLog;->fireEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V

    .line 56
    return-void
.end method

.method public isError(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public isInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public isWarning(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public removeAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/bzbyte/util/audit/AuditLogListener;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/bzbyte/util/audit/DefaultAuditLog;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method
