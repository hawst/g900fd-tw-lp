.class public interface abstract Lcom/bzbyte/util/audit/AuditLog;
.super Ljava/lang/Object;
.source "AuditLog.java"


# virtual methods
.method public abstract addAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V
.end method

.method public abstract fireDebug(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method public abstract fireError(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method public abstract fireError(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract fireInfo(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method public abstract fireWarning(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method public abstract isError(Ljava/lang/Object;)Z
.end method

.method public abstract isInfo(Ljava/lang/Object;)Z
.end method

.method public abstract isWarning(Ljava/lang/Object;)Z
.end method

.method public abstract removeAuditLogListener(Lcom/bzbyte/util/audit/AuditLogListener;)V
.end method
