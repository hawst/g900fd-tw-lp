.class public Lcom/bzbyte/util/audit/AuditLogEvent;
.super Ljava/lang/Object;
.source "AuditLogEvent.java"


# static fields
.field private static final TYPE_NAMES:[Ljava/lang/String;

.field private static ivDateFormat:Ljava/text/SimpleDateFormat;


# instance fields
.field private ivAttributes:Lcom/bzbyte/util/HashVector;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "Info"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "Warning"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Error"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "Debug"

    aput-object v2, v0, v1

    sput-object v0, Lcom/bzbyte/util/audit/AuditLogEvent;->TYPE_NAMES:[Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivDateFormat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;ILjava/lang/String;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "type"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/bzbyte/util/audit/AuditLogEvent;-><init>(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "type"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "th"    # Ljava/lang/Throwable;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v1, Lcom/bzbyte/util/HashVector;

    invoke-direct {v1}, Lcom/bzbyte/util/HashVector;-><init>()V

    iput-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    .line 71
    iget-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v2, "Type"

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 73
    iget-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v2, "Source"

    invoke-virtual {v1, v2, p1}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 74
    iget-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v2, "Message"

    invoke-virtual {v1, v2, p3}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 75
    iget-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v2, "Exception"

    invoke-virtual {v1, v2, p4}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 76
    new-instance v0, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/sql/Date;-><init>(J)V

    .line 77
    .local v0, "dt":Ljava/sql/Date;
    iget-object v1, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v2, "Time"

    invoke-virtual {v1, v2, v0}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method private static formatDate(Ljava/sql/Date;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/sql/Date;

    .prologue
    .line 126
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 127
    .local v0, "cl":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getAttributes()Lcom/bzbyte/util/HashVector;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    return-object v0
.end method

.method public getException()Ljava/lang/Throwable;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v1, "Exception"

    invoke-virtual {v0, v1}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v1, "Message"

    invoke-virtual {v0, v1}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v1, "Source"

    invoke-virtual {v0, v1}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSourceClass()Ljava/lang/Class;
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getSource()Ljava/lang/Object;

    move-result-object v0

    .line 199
    .local v0, "source":Ljava/lang/Object;
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_0

    .line 201
    check-cast v0, Ljava/lang/Class;

    .line 203
    .end local v0    # "source":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "source":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public getSourceClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getSourceClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTime()Ljava/sql/Date;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v1, "Time"

    invoke-virtual {v0, v1}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/sql/Date;

    return-object v0
.end method

.method public getType()I
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/bzbyte/util/audit/AuditLogEvent;->ivAttributes:Lcom/bzbyte/util/HashVector;

    const-string/jumbo v1, "Type"

    invoke-virtual {v0, v1}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTypeName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/bzbyte/util/audit/AuditLogEvent;->TYPE_NAMES:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getType()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isDebug()Z
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInfo()Z
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getType()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWarning()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 111
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toDefaultString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 157
    move-object v2, p0

    .line 158
    .local v2, "ev":Lcom/bzbyte/util/audit/AuditLogEvent;
    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getSourceClassName()Ljava/lang/String;

    move-result-object v7

    .line 159
    .local v7, "sourceStr":Ljava/lang/String;
    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 160
    .local v6, "shortName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/bzbyte/util/audit/AuditLogEvent;->getTime()Ljava/sql/Date;

    move-result-object v1

    .line 163
    .local v1, "dt":Ljava/sql/Date;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-static {v1}, Lcom/bzbyte/util/audit/AuditLogEvent;->formatDate(Ljava/sql/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 164
    .local v5, "res":Ljava/lang/StringBuffer;
    const-string/jumbo v8, " "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getTypeName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    const-string/jumbo v8, " "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getAttributes()Lcom/bzbyte/util/HashVector;

    move-result-object v0

    .line 168
    .local v0, "attributes":Lcom/bzbyte/util/HashVector;
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    invoke-virtual {v0}, Lcom/bzbyte/util/HashVector;->size()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 170
    invoke-virtual {v0, v3}, Lcom/bzbyte/util/HashVector;->getKey(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 171
    .local v4, "name":Ljava/lang/String;
    const-string/jumbo v8, "Type"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string/jumbo v8, "Source"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string/jumbo v8, "Message"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string/jumbo v8, "Exception"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string/jumbo v8, "Time"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 168
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 179
    :cond_1
    const-string/jumbo v8, " "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v0, v4}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 183
    .end local v4    # "name":Ljava/lang/String;
    :cond_2
    const-string/jumbo v8, " source:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getException()Ljava/lang/Throwable;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 186
    invoke-virtual {v2}, Lcom/bzbyte/util/audit/AuditLogEvent;->getException()Ljava/lang/Throwable;

    move-result-object v8

    invoke-static {v8}, Lcom/bzbyte/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 188
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method
