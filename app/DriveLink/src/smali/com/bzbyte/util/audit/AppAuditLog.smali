.class public Lcom/bzbyte/util/audit/AppAuditLog;
.super Ljava/lang/Object;
.source "AppAuditLog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static debug(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/bzbyte/util/audit/AuditLog;->fireDebug(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public static error(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static error(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 50
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/bzbyte/util/audit/AuditLog;->fireError(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    return-void
.end method

.method public static getAuditLog()Lcom/bzbyte/util/audit/AuditLog;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/bzbyte/util/audit/GlobalAuditLog;->getLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    return-object v0
.end method

.method public static info(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/bzbyte/util/audit/AuditLog;->fireInfo(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public static warning(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p0, "source"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/bzbyte/util/audit/AuditLog;->fireWarning(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    return-void
.end method


# virtual methods
.method public isError(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 64
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bzbyte/util/audit/AuditLog;->isError(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInfo(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 69
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bzbyte/util/audit/AuditLog;->isInfo(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isWarning(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    .line 74
    invoke-static {}, Lcom/bzbyte/util/audit/AppAuditLog;->getAuditLog()Lcom/bzbyte/util/audit/AuditLog;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/bzbyte/util/audit/AuditLog;->isWarning(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
