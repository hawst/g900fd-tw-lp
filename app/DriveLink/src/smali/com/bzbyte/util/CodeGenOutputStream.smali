.class public Lcom/bzbyte/util/CodeGenOutputStream;
.super Ljava/lang/Object;
.source "CodeGenOutputStream.java"

# interfaces
.implements Lcom/bzbyte/util/SimplePrintWriter;


# instance fields
.field private ivIndent:I

.field private ivNewLine:Z

.field private ivPout:Ljava/io/PrintWriter;

.field private ivTabs:Z

.field private ivTabsEnabled:Z


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2
    .param p1, "out"    # Ljava/io/Writer;

    .prologue
    const/4 v1, 0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    .line 40
    iput-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivNewLine:Z

    .line 44
    iput-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabs:Z

    .line 46
    iput-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabsEnabled:Z

    .line 70
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, p1, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;Z)V

    iput-object v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    .line 72
    return-void
.end method

.method private final ensureTabs()V
    .locals 3

    .prologue
    .line 114
    iget-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabs:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabsEnabled:Z

    if-eqz v1, :cond_1

    .line 116
    iget-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivNewLine:Z

    if-eqz v1, :cond_1

    .line 118
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    if-ge v0, v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivNewLine:Z

    .line 125
    .end local v0    # "index":I
    :cond_1
    return-void
.end method


# virtual methods
.method public addTab()V
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    .line 105
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 161
    return-void
.end method

.method public getTabsEnabled()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabsEnabled:Z

    return v0
.end method

.method public print(C)V
    .locals 1
    .param p1, "it"    # C

    .prologue
    .line 146
    iget-object v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->print(C)V

    .line 147
    return-void
.end method

.method public print(Ljava/lang/String;)V
    .locals 1
    .param p1, "it"    # Ljava/lang/String;

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/bzbyte/util/CodeGenOutputStream;->ensureTabs()V

    .line 130
    iget-object v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public println()V
    .locals 1

    .prologue
    .line 135
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CodeGenOutputStream;->println(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public println(Ljava/lang/String;)V
    .locals 1
    .param p1, "it"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/bzbyte/util/CodeGenOutputStream;->ensureTabs()V

    .line 140
    iget-object v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivPout:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivNewLine:Z

    .line 142
    return-void
.end method

.method public removeTab()V
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivIndent:I

    .line 110
    return-void
.end method

.method public setTabsEnabled(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/bzbyte/util/CodeGenOutputStream;->ivTabsEnabled:Z

    .line 77
    return-void
.end method
