.class Lcom/bzbyte/util/CacheManager$CacheWorker;
.super Ljava/lang/Object;
.source "CacheManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bzbyte/util/CacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CacheWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/bzbyte/util/CacheManager;


# direct methods
.method constructor <init>(Lcom/bzbyte/util/CacheManager;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/bzbyte/util/CacheManager$CacheWorker;->this$0:Lcom/bzbyte/util/CacheManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 44
    :goto_0
    const/4 v3, 0x0

    .line 45
    .local v3, "objects":Ljava/util/ArrayList;
    iget-object v5, p0, Lcom/bzbyte/util/CacheManager$CacheWorker;->this$0:Lcom/bzbyte/util/CacheManager;

    monitor-enter v5

    .line 47
    :try_start_0
    iget-object v4, p0, Lcom/bzbyte/util/CacheManager$CacheWorker;->this$0:Lcom/bzbyte/util/CacheManager;

    # getter for: Lcom/bzbyte/util/CacheManager;->ivCaches:Lcom/bzbyte/util/WeakRefArrayList;
    invoke-static {v4}, Lcom/bzbyte/util/CacheManager;->access$000(Lcom/bzbyte/util/CacheManager;)Lcom/bzbyte/util/WeakRefArrayList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/bzbyte/util/WeakRefArrayList;->getObjects()Ljava/util/ArrayList;

    move-result-object v3

    .line 48
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 51
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/bzbyte/util/Cache;

    .line 52
    .local v0, "cache":Lcom/bzbyte/util/Cache;
    invoke-virtual {v0}, Lcom/bzbyte/util/Cache;->cleanupCache()V

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 48
    .end local v0    # "cache":Lcom/bzbyte/util/Cache;
    .end local v2    # "index":I
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 57
    .restart local v2    # "index":I
    :cond_0
    const-wide/16 v4, 0x7d0

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v4}, Ljava/lang/InterruptedException;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method
