.class public Lcom/bzbyte/util/WeakRefArrayList;
.super Ljava/lang/Object;
.source "WeakRefArrayList.java"


# static fields
.field private static CLEANUP_INTERVAL:J


# instance fields
.field private ivItems:Ljava/util/ArrayList;

.field private ivLastCleanup:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/bzbyte/util/WeakRefArrayList;->CLEANUP_INTERVAL:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    .line 19
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivLastCleanup:J

    .line 25
    return-void
.end method

.method private checkCleanup()V
    .locals 8

    .prologue
    .line 63
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivLastCleanup:J

    sub-long/2addr v4, v6

    sget-wide v6, Lcom/bzbyte/util/WeakRefArrayList;->CLEANUP_INTERVAL:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 67
    const/4 v1, 0x0

    .line 68
    .local v1, "itemsRemoved":I
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 69
    .local v2, "refs":Ljava/util/ArrayList;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 72
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/Reference;

    .line 73
    .local v3, "wr":Ljava/lang/ref/Reference;
    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v1, v1, 0x1

    .line 69
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    .end local v3    # "wr":Ljava/lang/ref/Reference;
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivLastCleanup:J

    .line 86
    .end local v0    # "index":I
    .end local v1    # "itemsRemoved":I
    .end local v2    # "refs":Ljava/util/ArrayList;
    :cond_2
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 2
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/bzbyte/util/WeakRefArrayList;->checkCleanup()V

    .line 30
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 31
    .local v0, "ref":Ljava/lang/ref/WeakReference;
    iget-object v1, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public getObjects()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/bzbyte/util/WeakRefArrayList;->checkCleanup()V

    .line 94
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v3, "res":Ljava/util/ArrayList;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v4, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 100
    iget-object v4, p0, Lcom/bzbyte/util/WeakRefArrayList;->ivItems:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 101
    .local v2, "ref":Ljava/lang/ref/WeakReference;
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 102
    .local v1, "it":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "it":Ljava/lang/Object;
    .end local v2    # "ref":Ljava/lang/ref/WeakReference;
    :cond_1
    return-object v3
.end method
