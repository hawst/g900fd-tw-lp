.class public Lcom/bzbyte/util/DefaultSimplePrintWriter;
.super Ljava/lang/Object;
.source "DefaultSimplePrintWriter.java"

# interfaces
.implements Lcom/bzbyte/util/SimplePrintWriter;


# instance fields
.field private ivIn:Ljava/io/PrintWriter;


# direct methods
.method public constructor <init>(Ljava/io/PrintWriter;)V
    .locals 0
    .param p1, "in"    # Ljava/io/PrintWriter;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/bzbyte/util/DefaultSimplePrintWriter;->ivIn:Ljava/io/PrintWriter;

    .line 18
    return-void
.end method


# virtual methods
.method public print(C)V
    .locals 1
    .param p1, "it"    # C

    .prologue
    .line 31
    iget-object v0, p0, Lcom/bzbyte/util/DefaultSimplePrintWriter;->ivIn:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->print(C)V

    .line 32
    return-void
.end method

.method public print(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/bzbyte/util/DefaultSimplePrintWriter;->ivIn:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 22
    return-void
.end method
