.class public Lcom/bzbyte/util/CommonProperties;
.super Lcom/bzbyte/util/PropertiesManager;
.source "CommonProperties.java"


# static fields
.field private static ivInst:Lcom/bzbyte/util/CommonProperties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/bzbyte/util/CommonProperties;

    invoke-direct {v0}, Lcom/bzbyte/util/CommonProperties;-><init>()V

    sput-object v0, Lcom/bzbyte/util/CommonProperties;->ivInst:Lcom/bzbyte/util/CommonProperties;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/bzbyte/util/PropertiesManager;-><init>()V

    .line 37
    const-string/jumbo v0, "/Common.properties"

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CommonProperties;->init(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public static getProps()Lcom/bzbyte/util/CommonProperties;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/bzbyte/util/CommonProperties;->ivInst:Lcom/bzbyte/util/CommonProperties;

    return-object v0
.end method


# virtual methods
.method public getGlobalAuditLogFactory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "GlobalAuditLogFactory"

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CommonProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsConsoleLog()Z
    .locals 2

    .prologue
    .line 51
    const-string/jumbo v0, "IsConsoleLog"

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CommonProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/bzbyte/util/Util;->stringToBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getIsLog4jLog()Z
    .locals 2

    .prologue
    .line 56
    const-string/jumbo v0, "IsLog4jLog"

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CommonProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/bzbyte/util/Util;->stringToBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLogDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string/jumbo v0, "LogDir"

    invoke-virtual {p0, v0}, Lcom/bzbyte/util/CommonProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public logToConsole()Z
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/bzbyte/util/CommonProperties;->getIsConsoleLog()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/bzbyte/util/CommonProperties;->getIsLog4jLog()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
