.class public Lcom/bzbyte/util/Cache;
.super Ljava/lang/Object;
.source "Cache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bzbyte/util/Cache$CacheItem;
    }
.end annotation


# instance fields
.field private ivDebug:Z

.field private ivItems:Lcom/bzbyte/util/HashVector;

.field private ivListeners:Ljava/util/ArrayList;

.field private ivMaxAge:I

.field private ivMaxSize:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "maxAge"    # I
    .param p2, "maxSize"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/bzbyte/util/HashVector;

    invoke-direct {v0}, Lcom/bzbyte/util/HashVector;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/bzbyte/util/Cache;->ivDebug:Z

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/Cache;->ivListeners:Ljava/util/ArrayList;

    .line 60
    iput p1, p0, Lcom/bzbyte/util/Cache;->ivMaxAge:I

    .line 61
    iput p2, p0, Lcom/bzbyte/util/Cache;->ivMaxSize:I

    .line 62
    invoke-static {}, Lcom/bzbyte/util/CacheManager;->getManager()Lcom/bzbyte/util/CacheManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/bzbyte/util/CacheManager;->add(Lcom/bzbyte/util/Cache;)V

    .line 63
    return-void
.end method

.method private fireRemoveEvent(Ljava/lang/Object;)V
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 145
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/bzbyte/util/Cache;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 147
    iget-object v2, p0, Lcom/bzbyte/util/Cache;->ivListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bzbyte/util/CacheListener;

    .line 148
    .local v1, "listener":Lcom/bzbyte/util/CacheListener;
    new-instance v2, Lcom/bzbyte/util/CacheRemoveEvent;

    invoke-direct {v2, p0, p1}, Lcom/bzbyte/util/CacheRemoveEvent;-><init>(Lcom/bzbyte/util/Cache;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Lcom/bzbyte/util/CacheListener;->itemRemoved(Lcom/bzbyte/util/CacheRemoveEvent;)V

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    .end local v1    # "listener":Lcom/bzbyte/util/CacheListener;
    :cond_0
    return-void
.end method


# virtual methods
.method declared-synchronized cleanupCache()V
    .locals 8

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/bzbyte/util/Cache;->ivDebug:Z

    if-eqz v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Cache: Cleanup running. Items:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v6}, Lcom/bzbyte/util/HashVector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 115
    :cond_0
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4}, Lcom/bzbyte/util/HashVector;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 116
    .local v3, "keys":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4}, Lcom/bzbyte/util/HashVector;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 118
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4, v0}, Lcom/bzbyte/util/HashVector;->getKey(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v0

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 122
    aget-object v2, v3, v0

    .line 123
    .local v2, "key":Ljava/lang/Object;
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4, v2}, Lcom/bzbyte/util/HashVector;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/bzbyte/util/Cache$CacheItem;

    .line 124
    .local v1, "item":Lcom/bzbyte/util/Cache$CacheItem;
    iget v4, p0, Lcom/bzbyte/util/Cache;->ivMaxAge:I

    if-lez v4, :cond_4

    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/bzbyte/util/Cache$CacheItem;->getLastAccessTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget v6, p0, Lcom/bzbyte/util/Cache;->ivMaxAge:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 128
    iget-boolean v4, p0, Lcom/bzbyte/util/Cache;->ivDebug:Z

    if-eqz v4, :cond_2

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Cache: Cleanup removing item:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/bzbyte/util/Cache$CacheItem;->getObject()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 129
    :cond_2
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v1}, Lcom/bzbyte/util/Cache$CacheItem;->getObject()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bzbyte/util/HashVector;->remove(Ljava/lang/Object;)V

    .line 130
    invoke-virtual {v1}, Lcom/bzbyte/util/Cache$CacheItem;->getObject()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/bzbyte/util/Cache;->fireRemoveEvent(Ljava/lang/Object;)V

    .line 120
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_4
    iget v4, p0, Lcom/bzbyte/util/Cache;->ivMaxSize:I

    if-lez v4, :cond_3

    .line 135
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4}, Lcom/bzbyte/util/HashVector;->size()I

    move-result v4

    iget v5, p0, Lcom/bzbyte/util/Cache;->ivMaxSize:I

    if-le v4, v5, :cond_3

    .line 137
    iget-object v4, p0, Lcom/bzbyte/util/Cache;->ivItems:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v4, v1}, Lcom/bzbyte/util/HashVector;->remove(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 114
    .end local v0    # "index":I
    .end local v1    # "item":Lcom/bzbyte/util/Cache$CacheItem;
    .end local v2    # "key":Ljava/lang/Object;
    .end local v3    # "keys":[Ljava/lang/Object;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 141
    .restart local v0    # "index":I
    .restart local v3    # "keys":[Ljava/lang/Object;
    :cond_5
    monitor-exit p0

    return-void
.end method
