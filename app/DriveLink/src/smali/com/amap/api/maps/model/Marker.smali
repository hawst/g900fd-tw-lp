.class public final Lcom/amap/api/maps/model/Marker;
.super Ljava/lang/Object;
.source "Marker.java"


# instance fields
.field a:Lcom/amap/api/mapcore/t;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/t;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    .line 20
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->n()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 214
    instance-of v0, p1, Lcom/amap/api/maps/model/Marker;

    if-nez v0, :cond_0

    .line 215
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    check-cast p1, Lcom/amap/api/maps/model/Marker;

    iget-object v1, p1, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/t;->a(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->p()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getSnippet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->o()I

    move-result v0

    return v0
.end method

.method public hideInfoWindow()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->k()V

    .line 184
    return-void
.end method

.method public isDraggable()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->i()Z

    move-result v0

    return v0
.end method

.method public isInfoWindowShown()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->l()Z

    move-result v0

    return v0
.end method

.method public isPerspective()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->r()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->m()Z

    move-result v0

    return v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->a()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setAnchor(FF)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1, p2}, Lcom/amap/api/mapcore/t;->a(FF)V

    .line 158
    return-void
.end method

.method public setDraggable(Z)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Z)V

    .line 162
    return-void
.end method

.method public setIcon(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 1

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Lcom/amap/api/maps/model/BitmapDescriptor;)V

    .line 145
    :cond_0
    return-void
.end method

.method public setObject(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Ljava/lang/Object;)V

    .line 233
    return-void
.end method

.method public setPerspective(Z)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->c(Z)V

    .line 71
    return-void
.end method

.method public setPosition(Lcom/amap/api/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 86
    return-void
.end method

.method public setSnippet(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->b(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->b(Z)V

    .line 206
    return-void
.end method

.method public showInfoWindow()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/amap/api/maps/model/Marker;->a:Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->j()V

    .line 175
    return-void
.end method
