.class Lcom/amap/api/maps/offlinemap/c;
.super Ljava/lang/Thread;
.source "FileSplitterFetch.java"


# instance fields
.field a:Ljava/lang/String;

.field b:J

.field c:J

.field d:I

.field e:Z

.field f:Z

.field g:Lcom/amap/api/maps/offlinemap/b;

.field h:Ljava/net/HttpURLConnection;

.field i:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJI)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 13
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->e:Z

    .line 14
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    .line 21
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/c;->a:Ljava/lang/String;

    .line 22
    iput-wide p3, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    .line 23
    iput-wide p5, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    .line 24
    iput p7, p0, Lcom/amap/api/maps/offlinemap/c;->d:I

    .line 25
    new-instance v0, Lcom/amap/api/maps/offlinemap/b;

    iget-wide v1, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-direct {v0, p2, v1, v2}, Lcom/amap/api/maps/offlinemap/b;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    .line 68
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/c;->interrupt()V

    .line 69
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 70
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 76
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 29
    :goto_0
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    if-nez v0, :cond_1

    .line 31
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    .line 33
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string/jumbo v1, "User-Agent"

    sget-object v2, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string/jumbo v1, "Content-Type"

    const-string/jumbo v2, "text/xml;"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    const-string/jumbo v2, "RANGE"

    invoke-virtual {v1, v2, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->h:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    .line 40
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 43
    :goto_1
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/c;->i:Ljava/io/InputStream;

    const/4 v2, 0x0

    const/16 v3, 0x400

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-lez v1, :cond_0

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/c;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-boolean v2, p0, Lcom/amap/api/maps/offlinemap/c;->f:Z

    if-nez v2, :cond_0

    .line 44
    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/c;->g:Lcom/amap/api/maps/offlinemap/b;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v1}, Lcom/amap/api/maps/offlinemap/b;->a([BII)I

    move-result v1

    int-to-long v4, v1

    add-long v1, v2, v4

    iput-wide v1, p0, Lcom/amap/api/maps/offlinemap/c;->b:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 46
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Thread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/maps/offlinemap/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " is over!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/c;->e:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 52
    :cond_1
    return-void
.end method
