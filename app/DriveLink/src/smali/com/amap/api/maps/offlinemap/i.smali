.class Lcom/amap/api/maps/offlinemap/i;
.super Ljava/lang/Thread;
.source "SiteFileFetch.java"


# instance fields
.field a:Lcom/amap/api/maps/offlinemap/j;

.field b:[J

.field c:[J

.field d:[Lcom/amap/api/maps/offlinemap/c;

.field e:J

.field f:Z

.field g:Z

.field h:Ljava/io/File;

.field i:Ljava/io/DataOutputStream;

.field j:Lcom/amap/api/maps/offlinemap/d;

.field k:Lcom/amap/api/maps/offlinemap/k;

.field private l:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/amap/api/maps/offlinemap/j;Lcom/amap/api/maps/offlinemap/d;Lcom/amap/api/maps/offlinemap/k;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->f:Z

    .line 27
    iput-boolean v3, p0, Lcom/amap/api/maps/offlinemap/i;->g:Z

    .line 36
    iput-object p1, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    .line 39
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->h:Ljava/io/File;

    .line 41
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/i;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iput-boolean v3, p0, Lcom/amap/api/maps/offlinemap/i;->f:Z

    .line 43
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/i;->e()Z

    .line 48
    :goto_0
    iput-object p2, p0, Lcom/amap/api/maps/offlinemap/i;->j:Lcom/amap/api/maps/offlinemap/d;

    .line 49
    iput-object p3, p0, Lcom/amap/api/maps/offlinemap/i;->k:Lcom/amap/api/maps/offlinemap/k;

    .line 50
    iput-object p4, p0, Lcom/amap/api/maps/offlinemap/i;->l:Landroid/content/Context;

    .line 51
    return-void

    .line 45
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/j;->d()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    .line 46
    invoke-virtual {p1}, Lcom/amap/api/maps/offlinemap/j;->d()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 224
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error Code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method private c()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps/AMapException;
        }
    .end annotation

    .prologue
    .line 113
    sget v0, Lcom/amap/api/mapcore/g;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 114
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/mapcore/g;->a(Landroid/content/Context;)Z
    :try_end_0
    .catch Lcom/amap/api/maps/AMapException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 117
    if-eqz v0, :cond_1

    .line 126
    :cond_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    const-string/jumbo v2, "AuthFailure"

    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->printStackTrace()V

    .line 114
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private d()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x64

    const-wide/16 v3, 0x0

    const/4 v0, 0x0

    .line 166
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->h:Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    .line 167
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    iget-wide v5, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    invoke-virtual {v1, v5, v6}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 168
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v2, v0

    move-wide v0, v3

    .line 170
    :goto_0
    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 172
    if-nez v2, :cond_0

    .line 173
    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v5, v5, v2

    iget-wide v5, v5, Lcom/amap/api/maps/offlinemap/c;->b:J

    add-long/2addr v0, v5

    .line 177
    :goto_1
    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v6, v6, v2

    iget-wide v6, v6, Lcom/amap/api/maps/offlinemap/c;->b:J

    invoke-virtual {v5, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 178
    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v6, v6, v2

    iget-wide v6, v6, Lcom/amap/api/maps/offlinemap/c;->c:J

    invoke-virtual {v5, v6, v7}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :cond_0
    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v5, v5, v2

    iget-wide v5, v5, Lcom/amap/api/maps/offlinemap/c;->b:J

    iget-object v7, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    add-int/lit8 v8, v2, -0x1

    aget-object v7, v7, v8

    iget-wide v7, v7, Lcom/amap/api/maps/offlinemap/c;->c:J

    sub-long/2addr v5, v7

    add-long/2addr v0, v5

    goto :goto_1

    .line 180
    :cond_1
    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/i;->i:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 181
    iget-wide v5, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    cmp-long v2, v5, v3

    if-lez v2, :cond_2

    .line 182
    mul-long v2, v0, v9

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    div-long/2addr v2, v4

    .line 183
    iget-object v4, p0, Lcom/amap/api/maps/offlinemap/i;->k:Lcom/amap/api/maps/offlinemap/k;

    invoke-virtual {v4, v0, v1}, Lcom/amap/api/maps/offlinemap/k;->a(J)V

    .line 184
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->j:Lcom/amap/api/maps/offlinemap/d;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->k:Lcom/amap/api/maps/offlinemap/k;

    const/4 v4, 0x0

    long-to-int v5, v2

    invoke-virtual {v0, v1, v4, v5}, Lcom/amap/api/maps/offlinemap/d;->a(Lcom/amap/api/maps/offlinemap/k;II)V

    .line 186
    cmp-long v0, v2, v9

    if-ltz v0, :cond_2

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->g:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 195
    :cond_2
    :goto_2
    return-void

    .line 190
    :catch_0
    move-exception v0

    .line 191
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 192
    :catch_1
    move-exception v0

    .line 193
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private e()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 198
    .line 200
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->h:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 202
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    .line 203
    iget-wide v3, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-gtz v1, :cond_0

    .line 220
    :goto_0
    return v0

    .line 206
    :cond_0
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 207
    new-array v3, v1, [J

    iput-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    .line 208
    new-array v1, v1, [J

    iput-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    move v1, v0

    .line 209
    :goto_1
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 210
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 211
    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 213
    :cond_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 214
    const/4 v0, 0x1

    goto :goto_0

    .line 215
    :catch_0
    move-exception v1

    .line 216
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 217
    :catch_1
    move-exception v1

    .line 218
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 5

    .prologue
    .line 129
    const/4 v1, -0x1

    .line 131
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    invoke-virtual {v2}, Lcom/amap/api/maps/offlinemap/j;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 134
    const-string/jumbo v2, "User-Agent"

    sget-object v3, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 136
    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 137
    invoke-direct {p0, v2}, Lcom/amap/api/maps/offlinemap/i;->a(I)V

    .line 138
    const-wide/16 v0, -0x2

    .line 161
    :goto_0
    return-wide v0

    .line 141
    :cond_0
    const/4 v2, 0x1

    .line 145
    :goto_1
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    .line 146
    if-eqz v3, :cond_2

    .line 147
    const-string/jumbo v4, "Content-Length"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 148
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_2
    move v1, v0

    .line 160
    :goto_3
    invoke-static {v1}, Lcom/amap/api/maps/offlinemap/l;->b(I)V

    .line 161
    int-to-long v0, v1

    goto :goto_0

    .line 141
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 155
    :catch_0
    move-exception v0

    .line 156
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 157
    :catch_1
    move-exception v0

    .line 158
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->g:Z

    .line 229
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 230
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/amap/api/maps/offlinemap/c;->a()V

    .line 229
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_1
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->c(Landroid/content/Context;)Z

    move-result v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/i;->c()V

    .line 59
    :cond_0
    sget v0, Lcom/amap/api/mapcore/g;->a:I

    if-eq v0, v9, :cond_2

    .line 110
    :cond_1
    :goto_0
    return-void

    .line 62
    :cond_2
    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->f:Z

    if-eqz v0, :cond_3

    .line 63
    invoke-virtual {p0}, Lcom/amap/api/maps/offlinemap/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    .line 64
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 65
    const-string/jumbo v0, "File Length is not known!"

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(Ljava/lang/String;)V

    .line 78
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v0, v0

    new-array v0, v0, [Lcom/amap/api/maps/offlinemap/c;

    iput-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    move v7, v8

    .line 79
    :goto_2
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v0, v0

    if-ge v7, v0, :cond_8

    .line 80
    iget-object v10, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    new-instance v0, Lcom/amap/api/maps/offlinemap/c;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    invoke-virtual {v1}, Lcom/amap/api/maps/offlinemap/j;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    invoke-virtual {v3}, Lcom/amap/api/maps/offlinemap/j;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->a:Lcom/amap/api/maps/offlinemap/j;

    invoke-virtual {v3}, Lcom/amap/api/maps/offlinemap/j;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    aget-wide v3, v3, v7

    iget-object v5, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    aget-wide v5, v5, v7

    invoke-direct/range {v0 .. v7}, Lcom/amap/api/maps/offlinemap/c;-><init>(Ljava/lang/String;Ljava/lang/String;JJI)V

    aput-object v0, v10, v7

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Thread "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " , nStartPos = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    aget-wide v1, v1, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", nEndPos = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    aget-wide v1, v1, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/amap/api/maps/offlinemap/c;->start()V

    .line 79
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 66
    :cond_4
    iget-wide v0, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 67
    const-string/jumbo v0, "File is not access!"

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/amap/api/maps/AMapException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 105
    :catch_0
    move-exception v0

    .line 106
    invoke-virtual {v0}, Lcom/amap/api/maps/AMapException;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    move v0, v8

    .line 69
    :goto_3
    :try_start_1
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 70
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    iget-object v6, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v6, v6

    int-to-long v6, v6

    div-long/2addr v4, v6

    mul-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v8

    .line 72
    :goto_4
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 73
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    iget-object v2, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    add-int/lit8 v3, v0, 0x1

    aget-wide v2, v2, v3

    aput-wide v2, v1, v0

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 75
    :cond_7
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-wide v2, p0, Lcom/amap/api/maps/offlinemap/i;->e:J

    aput-wide v2, v0, v1
    :try_end_1
    .catch Lcom/amap/api/maps/AMapException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 107
    :catch_1
    move-exception v0

    .line 108
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 89
    :cond_8
    :try_start_2
    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->g:Z

    if-nez v0, :cond_9

    .line 90
    invoke-direct {p0}, Lcom/amap/api/maps/offlinemap/i;->d()V

    .line 91
    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/amap/api/maps/offlinemap/l;->a(I)V

    move v0, v8

    .line 93
    :goto_5
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_b

    .line 94
    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->d:[Lcom/amap/api/maps/offlinemap/c;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/amap/api/maps/offlinemap/c;->e:Z

    if-nez v1, :cond_a

    move v0, v8

    .line 99
    :goto_6
    if-eqz v0, :cond_8

    .line 102
    :cond_9
    iget-boolean v0, p0, Lcom/amap/api/maps/offlinemap/i;->g:Z

    if-nez v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/amap/api/maps/offlinemap/i;->j:Lcom/amap/api/maps/offlinemap/d;

    iget-object v1, p0, Lcom/amap/api/maps/offlinemap/i;->k:Lcom/amap/api/maps/offlinemap/k;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/offlinemap/d;->b(Lcom/amap/api/maps/offlinemap/k;)V
    :try_end_2
    .catch Lcom/amap/api/maps/AMapException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 93
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    move v0, v9

    goto :goto_6
.end method
