.class public final Lcom/amap/api/maps/AMap;
.super Ljava/lang/Object;
.source "AMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/maps/AMap$OnMapScreenShotListener;,
        Lcom/amap/api/maps/AMap$onMapPrintScreenListener;,
        Lcom/amap/api/maps/AMap$OnMapLoadedListener;,
        Lcom/amap/api/maps/AMap$OnMapClickListener;,
        Lcom/amap/api/maps/AMap$OnMapLongClickListener;,
        Lcom/amap/api/maps/AMap$OnCameraChangeListener;,
        Lcom/amap/api/maps/AMap$OnMarkerClickListener;,
        Lcom/amap/api/maps/AMap$OnMarkerDragListener;,
        Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;,
        Lcom/amap/api/maps/AMap$CancelableCallback;,
        Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;,
        Lcom/amap/api/maps/AMap$InfoWindowAdapter;
    }
.end annotation


# static fields
.field public static final MAP_TYPE_NORMAL:I = 0x1

.field public static final MAP_TYPE_SATELLITE:I = 0x2


# instance fields
.field private final a:Lcom/amap/api/mapcore/q;

.field private b:Lcom/amap/api/maps/UiSettings;

.field private c:Lcom/amap/api/maps/Projection;

.field private d:Lcom/amap/api/maps/model/MyTrafficStyle;


# direct methods
.method protected constructor <init>(Lcom/amap/api/mapcore/q;)V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    iput-object p1, p0, Lcom/amap/api/maps/AMap;->a:Lcom/amap/api/mapcore/q;

    .line 230
    return-void
.end method


# virtual methods
.method a()Lcom/amap/api/mapcore/q;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->a:Lcom/amap/api/mapcore/q;

    return-object v0
.end method

.method public final addCircle(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/maps/model/Circle;
    .locals 2

    .prologue
    .line 359
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Circle;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Circle;-><init>(Lcom/amap/api/mapcore/r;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 360
    :catch_0
    move-exception v0

    .line 361
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addMarker(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    .locals 2

    .prologue
    .line 396
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolygon(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/maps/model/Polygon;
    .locals 2

    .prologue
    .line 375
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Polygon;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/v;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Polygon;-><init>(Lcom/amap/api/mapcore/v;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 376
    :catch_0
    move-exception v0

    .line 377
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final addPolyline(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/maps/model/Polyline;
    .locals 2

    .prologue
    .line 344
    :try_start_0
    new-instance v0, Lcom/amap/api/maps/model/Polyline;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/w;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/model/Polyline;-><init>(Lcom/amap/api/mapcore/w;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;)V
    .locals 2

    .prologue
    .line 294
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->b(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 297
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 316
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string/jumbo v1, "durationMs must be positive"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->b(ZLjava/lang/Object;)V

    .line 318
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :catch_0
    move-exception v0

    .line 322
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final animateCamera(Lcom/amap/api/maps/CameraUpdate;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2

    .prologue
    .line 304
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    return-void

    .line 308
    :catch_0
    move-exception v0

    .line 309
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 435
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->r()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    return-void

    .line 436
    :catch_0
    move-exception v0

    .line 437
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;
    .locals 2

    .prologue
    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->n()Lcom/amap/api/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getMapPrintScreen(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
    .locals 1

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V

    .line 831
    return-void
.end method

.method public final getMapScreenMarkers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->I()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMapScreenShot(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 834
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V

    .line 835
    return-void
.end method

.method public final getMapType()I
    .locals 2

    .prologue
    .line 449
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->s()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 450
    :catch_0
    move-exception v0

    .line 451
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final getMaxZoomLevel()F
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->o()F

    move-result v0

    return v0
.end method

.method public final getMinZoomLevel()F
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->p()F

    move-result v0

    return v0
.end method

.method public final getMyLocation()Landroid/location/Location;
    .locals 2

    .prologue
    .line 578
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->v()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 579
    :catch_0
    move-exception v0

    .line 580
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getMyTrafficStyle()Lcom/amap/api/maps/model/MyTrafficStyle;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->d:Lcom/amap/api/maps/model/MyTrafficStyle;

    return-object v0
.end method

.method public final getProjection()Lcom/amap/api/maps/Projection;
    .locals 2

    .prologue
    .line 655
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;

    if-nez v0, :cond_0

    .line 656
    new-instance v0, Lcom/amap/api/maps/Projection;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->x()Lcom/amap/api/mapcore/x;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/Projection;-><init>(Lcom/amap/api/mapcore/x;)V

    iput-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;

    .line 659
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->c:Lcom/amap/api/maps/Projection;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 660
    :catch_0
    move-exception v0

    .line 661
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public getScalePerPixel()F
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->G()F

    move-result v0

    return v0
.end method

.method public final getUiSettings()Lcom/amap/api/maps/UiSettings;
    .locals 2

    .prologue
    .line 636
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;

    if-nez v0, :cond_0

    .line 637
    new-instance v0, Lcom/amap/api/maps/UiSettings;

    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/amap/api/maps/UiSettings;-><init>(Lcom/amap/api/mapcore/aa;)V

    iput-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;

    .line 640
    :cond_0
    iget-object v0, p0, Lcom/amap/api/maps/AMap;->b:Lcom/amap/api/maps/UiSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 641
    :catch_0
    move-exception v0

    .line 642
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final isMyLocationEnabled()Z
    .locals 2

    .prologue
    .line 544
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->u()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final isTrafficEnabled()Z
    .locals 2

    .prologue
    .line 479
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->t()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 480
    :catch_0
    move-exception v0

    .line 481
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final moveCamera(Lcom/amap/api/maps/CameraUpdate;)V
    .locals 2

    .prologue
    .line 285
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/CameraUpdate;->a()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public runOnDrawFrame()V
    .locals 2

    .prologue
    .line 847
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 848
    return-void
.end method

.method public setCustomRenderer(Lcom/amap/api/maps/CustomRenderer;)V
    .locals 1

    .prologue
    .line 855
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/CustomRenderer;)V

    .line 856
    return-void
.end method

.method public final setInfoWindowAdapter(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    .locals 2

    .prologue
    .line 791
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 795
    return-void

    .line 792
    :catch_0
    move-exception v0

    .line 793
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setLocationSource(Lcom/amap/api/maps/LocationSource;)V
    .locals 2

    .prologue
    .line 593
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/LocationSource;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 597
    return-void

    .line 594
    :catch_0
    move-exception v0

    .line 595
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMapType(I)V
    .locals 2

    .prologue
    .line 465
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 467
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationEnabled(Z)V
    .locals 2

    .prologue
    .line 562
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->g(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    return-void

    .line 563
    :catch_0
    move-exception v0

    .line 564
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationRotateAngle(F)V
    .locals 2

    .prologue
    .line 622
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    return-void

    .line 623
    :catch_0
    move-exception v0

    .line 624
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setMyLocationStyle(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 2

    .prologue
    .line 608
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 612
    return-void

    .line 609
    :catch_0
    move-exception v0

    .line 610
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setMyTrafficStyle(Lcom/amap/api/maps/model/MyTrafficStyle;)V
    .locals 5

    .prologue
    .line 506
    iput-object p1, p0, Lcom/amap/api/maps/AMap;->d:Lcom/amap/api/maps/model/MyTrafficStyle;

    .line 507
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSmoothColor()I

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSlowColor()I

    move-result v2

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getCongestedColor()I

    move-result v3

    invoke-virtual {p1}, Lcom/amap/api/maps/model/MyTrafficStyle;->getSeriousCongestedColor()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/amap/api/mapcore/q;->a(IIII)V

    .line 510
    return-void
.end method

.method public final setOnCameraChangeListener(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    .locals 2

    .prologue
    .line 675
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    return-void

    .line 676
    :catch_0
    move-exception v0

    .line 677
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnInfoWindowClickListener(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    .locals 2

    .prologue
    .line 771
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    return-void

    .line 772
    :catch_0
    move-exception v0

    .line 773
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapClickListener(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    .locals 2

    .prologue
    .line 690
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    return-void

    .line 691
    :catch_0
    move-exception v0

    .line 692
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLoadedListener(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    .locals 2

    .prologue
    .line 804
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 808
    return-void

    .line 805
    :catch_0
    move-exception v0

    .line 806
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMapLongClickListener(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    .locals 2

    .prologue
    .line 723
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 727
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 725
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerClickListener(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    .locals 2

    .prologue
    .line 740
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 744
    return-void

    .line 741
    :catch_0
    move-exception v0

    .line 742
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMarkerDragListener(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    .locals 2

    .prologue
    .line 754
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 758
    return-void

    .line 755
    :catch_0
    move-exception v0

    .line 756
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final setOnMyLocationChangeListener(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    .locals 2

    .prologue
    .line 706
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 708
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public setTrafficEnabled(Z)V
    .locals 2

    .prologue
    .line 494
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->f(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 498
    return-void

    .line 495
    :catch_0
    move-exception v0

    .line 496
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final stopAnimation()V
    .locals 2

    .prologue
    .line 328
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/maps/AMap;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->q()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    return-void

    .line 329
    :catch_0
    move-exception v0

    .line 330
    new-instance v1, Lcom/amap/api/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
