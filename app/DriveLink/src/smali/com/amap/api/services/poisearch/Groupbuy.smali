.class public final Lcom/amap/api/services/poisearch/Groupbuy;
.super Ljava/lang/Object;
.source "Groupbuy.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/amap/api/services/poisearch/Groupbuy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/Date;

.field private e:Ljava/util/Date;

.field private f:I

.field private g:I

.field private h:F

.field private i:F

.field private j:F

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/amap/api/services/poisearch/b;

    invoke-direct {v0}, Lcom/amap/api/services/poisearch/b;-><init>()V

    sput-object v0, Lcom/amap/api/services/poisearch/Groupbuy;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->a:Ljava/lang/String;

    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->b:Ljava/lang/String;

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->c:Ljava/lang/String;

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/services/core/e;->e(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->d:Ljava/util/Date;

    .line 194
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/services/core/e;->e(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->e:Ljava/util/Date;

    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->f:I

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->g:I

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->h:F

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->i:F

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->j:F

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->k:Ljava/lang/String;

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->l:Ljava/lang/String;

    .line 202
    sget-object v0, Lcom/amap/api/services/poisearch/Photo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->n:Ljava/lang/String;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->o:Ljava/lang/String;

    .line 205
    return-void
.end method


# virtual methods
.method protected addPhotos(Lcom/amap/api/services/poisearch/Photo;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->f:I

    return v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscount()F
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->j:F

    return v0
.end method

.method public getEndTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->e:Ljava/util/Date;

    return-object v0
.end method

.method public getGroupbuyPrice()F
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->i:F

    return v0
.end method

.method public getOriginalPrice()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->h:F

    return v0
.end method

.method public getPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    return-object v0
.end method

.method public getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getSoldCount()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->g:I

    return v0
.end method

.method public getStartTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->d:Ljava/util/Date;

    return-object v0
.end method

.method public getTicketAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketTel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeDes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->n:Ljava/lang/String;

    return-object v0
.end method

.method protected initPhotos(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/services/poisearch/Photo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 145
    :cond_0
    return-void

    .line 141
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 142
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/poisearch/Photo;

    .line 143
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected setCount(I)V
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->f:I

    .line 79
    return-void
.end method

.method protected setDetail(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->c:Ljava/lang/String;

    .line 55
    return-void
.end method

.method protected setDiscount(F)V
    .locals 0

    .prologue
    .line 110
    iput p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->j:F

    .line 111
    return-void
.end method

.method protected setEndTime(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->e:Ljava/util/Date;

    .line 71
    return-void
.end method

.method protected setGroupbuyPrice(F)V
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->i:F

    .line 103
    return-void
.end method

.method protected setOriginalPrice(F)V
    .locals 0

    .prologue
    .line 94
    iput p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->h:F

    .line 95
    return-void
.end method

.method protected setProvider(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->o:Ljava/lang/String;

    .line 161
    return-void
.end method

.method protected setSoldCount(I)V
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->g:I

    .line 87
    return-void
.end method

.method protected setStartTime(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->d:Ljava/util/Date;

    .line 63
    return-void
.end method

.method protected setTicketAddress(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->k:Ljava/lang/String;

    .line 119
    return-void
.end method

.method protected setTicketTel(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->l:Ljava/lang/String;

    .line 127
    return-void
.end method

.method protected setTypeCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->a:Ljava/lang/String;

    .line 39
    return-void
.end method

.method protected setTypeDes(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->b:Ljava/lang/String;

    .line 47
    return-void
.end method

.method protected setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/amap/api/services/poisearch/Groupbuy;->n:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->d:Ljava/util/Date;

    invoke-static {v0}, Lcom/amap/api/services/core/e;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->e:Ljava/util/Date;

    invoke-static {v0}, Lcom/amap/api/services/core/e;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 180
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->i:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 181
    iget v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->j:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 182
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->m:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 185
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/amap/api/services/poisearch/Groupbuy;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    return-void
.end method
