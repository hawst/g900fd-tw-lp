.class public Lcom/amap/api/services/poisearch/g;
.super Lcom/amap/api/services/poisearch/d;
.source "PoiSearchIdHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amap/api/services/poisearch/d",
        "<",
        "Ljava/lang/String;",
        "Lcom/amap/api/services/poisearch/PoiItemDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/net/Proxy;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/d;-><init>(Ljava/lang/Object;Ljava/net/Proxy;)V

    .line 18
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    const-string/jumbo v0, "id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/amap/api/services/poisearch/g;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string/jumbo v0, "&output=json"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string/jumbo v0, "&extensions=all"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "&key="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/amap/api/services/core/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/amap/api/services/poisearch/Discount;Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 154
    const-string/jumbo v0, "photos"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    :try_start_0
    const-string/jumbo v0, "photos"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 159
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 160
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 161
    new-instance v3, Lcom/amap/api/services/poisearch/Photo;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Photo;-><init>()V

    .line 162
    const-string/jumbo v4, "title"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Photo;->setTitle(Ljava/lang/String;)V

    .line 163
    const-string/jumbo v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/amap/api/services/poisearch/Photo;->setUrl(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/Discount;->addPhotos(Lcom/amap/api/services/poisearch/Photo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 166
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Lcom/amap/api/services/poisearch/Groupbuy;Lorg/json/JSONObject;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 110
    const-string/jumbo v0, "photos"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    :try_start_0
    const-string/jumbo v0, "photos"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 115
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 116
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 117
    new-instance v3, Lcom/amap/api/services/poisearch/Photo;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Photo;-><init>()V

    .line 118
    const-string/jumbo v4, "title"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Photo;->setTitle(Ljava/lang/String;)V

    .line 119
    const-string/jumbo v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/amap/api/services/poisearch/Photo;->setUrl(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/Groupbuy;->addPhotos(Lcom/amap/api/services/poisearch/Photo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 122
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiItemDetail;->isGroupbuyInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/g;->b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/services/poisearch/PoiItemDetail;->isDiscountInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/poisearch/g;->c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    .line 70
    :cond_1
    return-void
.end method

.method private b(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 45
    const-string/jumbo v1, "pois"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-object v0

    .line 48
    :cond_1
    const-string/jumbo v1, "pois"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 53
    invoke-virtual {p0, v1}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;

    move-result-object v0

    .line 54
    const-string/jumbo v2, "rich_content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const-string/jumbo v2, "rich_content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 56
    invoke-direct {p0, v0, v1}, Lcom/amap/api/services/poisearch/g;->a(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V

    goto :goto_0
.end method

.method private b(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    const-string/jumbo v0, "groupbuys"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    return-void

    .line 77
    :cond_1
    const-string/jumbo v0, "groupbuys"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 78
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 79
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 80
    new-instance v3, Lcom/amap/api/services/poisearch/Groupbuy;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Groupbuy;-><init>()V

    .line 81
    const-string/jumbo v4, "typecode"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTypeCode(Ljava/lang/String;)V

    .line 82
    const-string/jumbo v4, "type"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTypeDes(Ljava/lang/String;)V

    .line 83
    const-string/jumbo v4, "detail"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setDetail(Ljava/lang/String;)V

    .line 84
    const-string/jumbo v4, "start_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/e;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 86
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setStartTime(Ljava/util/Date;)V

    .line 87
    const-string/jumbo v4, "end_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/e;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 89
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setEndTime(Ljava/util/Date;)V

    .line 91
    const-string/jumbo v4, "num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setCount(I)V

    .line 92
    const-string/jumbo v4, "sold_num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setSoldCount(I)V

    .line 93
    const-string/jumbo v4, "original_price"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setOriginalPrice(F)V

    .line 95
    const-string/jumbo v4, "groupbuy_price"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setGroupbuyPrice(F)V

    .line 97
    const-string/jumbo v4, "discount"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->g(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setDiscount(F)V

    .line 98
    const-string/jumbo v4, "ticket_address"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTicketAddress(Ljava/lang/String;)V

    .line 99
    const-string/jumbo v4, "ticket_tel"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setTicketTel(Ljava/lang/String;)V

    .line 100
    const-string/jumbo v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setUrl(Ljava/lang/String;)V

    .line 101
    const-string/jumbo v4, "provider"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Groupbuy;->setProvider(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, v3, v2}, Lcom/amap/api/services/poisearch/g;->a(Lcom/amap/api/services/poisearch/Groupbuy;Lorg/json/JSONObject;)V

    .line 103
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/PoiItemDetail;->addGroupbuy(Lcom/amap/api/services/poisearch/Groupbuy;)V

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method private c(Lcom/amap/api/services/poisearch/PoiItemDetail;Lorg/json/JSONObject;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 129
    const-string/jumbo v0, "discounts"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    return-void

    .line 132
    :cond_1
    const-string/jumbo v0, "discounts"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 133
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 134
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 135
    new-instance v3, Lcom/amap/api/services/poisearch/Discount;

    invoke-direct {v3}, Lcom/amap/api/services/poisearch/Discount;-><init>()V

    .line 136
    const-string/jumbo v4, "title"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setTitle(Ljava/lang/String;)V

    .line 137
    const-string/jumbo v4, "detail"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setDetail(Ljava/lang/String;)V

    .line 138
    const-string/jumbo v4, "start_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/e;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 140
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setStartTime(Ljava/util/Date;)V

    .line 141
    const-string/jumbo v4, "end_time"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/amap/api/services/core/e;->c(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    .line 143
    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setEndTime(Ljava/util/Date;)V

    .line 144
    const-string/jumbo v4, "sold_num"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/amap/api/services/poisearch/g;->f(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setSoldCount(I)V

    .line 145
    const-string/jumbo v4, "url"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setUrl(Ljava/lang/String;)V

    .line 146
    const-string/jumbo v4, "provider"

    invoke-virtual {p0, v2, v4}, Lcom/amap/api/services/poisearch/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/amap/api/services/poisearch/Discount;->setProvider(Ljava/lang/String;)V

    .line 147
    invoke-direct {p0, v3, v2}, Lcom/amap/api/services/poisearch/g;->a(Lcom/amap/api/services/poisearch/Discount;Lorg/json/JSONObject;)V

    .line 148
    invoke-virtual {p1, v3}, Lcom/amap/api/services/poisearch/PoiItemDetail;->addDiscount(Lcom/amap/api/services/poisearch/Discount;)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public synthetic b(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/services/core/AMapException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/amap/api/services/poisearch/g;->c(Ljava/io/InputStream;)Lcom/amap/api/services/poisearch/PoiItemDetail;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/io/InputStream;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/services/core/AMapException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 27
    .line 28
    invoke-virtual {p0, p1}, Lcom/amap/api/services/poisearch/g;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 29
    if-eqz v1, :cond_0

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    invoke-static {v1}, Lcom/amap/api/services/core/e;->b(Ljava/lang/String;)V

    .line 34
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, v2}, Lcom/amap/api/services/poisearch/g;->b(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 36
    :catch_0
    move-exception v1

    .line 37
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 38
    :catch_1
    move-exception v1

    .line 39
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected c()[B
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/amap/api/services/poisearch/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/amap/api/services/core/i;->a()Lcom/amap/api/services/core/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/services/core/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/place/detail?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
