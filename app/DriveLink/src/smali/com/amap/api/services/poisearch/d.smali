.class public abstract Lcom/amap/api/services/poisearch/d;
.super Lcom/amap/api/services/core/k;
.source "PoiHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/amap/api/services/core/k",
        "<TT;TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/net/Proxy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/net/Proxy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/amap/api/services/core/k;-><init>(Ljava/lang/Object;Ljava/net/Proxy;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected a(Lorg/json/JSONObject;)Lcom/amap/api/services/poisearch/PoiItemDetail;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 28
    const-string/jumbo v0, "id"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    const-string/jumbo v1, "location"

    invoke-virtual {p0, p1, v1}, Lcom/amap/api/services/poisearch/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v1

    .line 30
    const-string/jumbo v2, "name"

    invoke-virtual {p0, p1, v2}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 31
    const-string/jumbo v3, "address"

    invoke-virtual {p0, p1, v3}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 32
    new-instance v4, Lcom/amap/api/services/poisearch/PoiItemDetail;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/amap/api/services/poisearch/PoiItemDetail;-><init>(Ljava/lang/String;Lcom/amap/api/services/core/LatLonPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string/jumbo v0, "adcode"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setAdCode(Ljava/lang/String;)V

    .line 34
    const-string/jumbo v0, "citycode"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setCityCode(Ljava/lang/String;)V

    .line 35
    const-string/jumbo v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const-string/jumbo v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-virtual {p0, v0}, Lcom/amap/api/services/poisearch/d;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setDistance(I)V

    .line 39
    invoke-virtual {v4}, Lcom/amap/api/services/poisearch/PoiItemDetail;->getDistance()I

    move-result v0

    if-nez v0, :cond_0

    .line 40
    const/4 v0, -0x1

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setDistance(I)V

    .line 44
    :cond_0
    const-string/jumbo v0, "tel"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setTel(Ljava/lang/String;)V

    .line 54
    const-string/jumbo v0, "type"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setTypeDes(Ljava/lang/String;)V

    .line 55
    const-string/jumbo v0, "entr_location"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setEnter(Lcom/amap/api/services/core/LatLonPoint;)V

    .line 56
    const-string/jumbo v0, "exit_location"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/amap/api/services/core/LatLonPoint;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setExit(Lcom/amap/api/services/core/LatLonPoint;)V

    .line 57
    const-string/jumbo v0, "website"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setWebsite(Ljava/lang/String;)V

    .line 58
    const-string/jumbo v0, "citycode"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setPostcode(Ljava/lang/String;)V

    .line 59
    const-string/jumbo v0, "email"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setEmail(Ljava/lang/String;)V

    .line 60
    const-string/jumbo v0, "groupbuy_num"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-virtual {p0, v0}, Lcom/amap/api/services/poisearch/d;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v4, v5}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setGroupbuyInfo(Z)V

    .line 66
    :goto_0
    const-string/jumbo v0, "discount_num"

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/services/poisearch/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {p0, v0}, Lcom/amap/api/services/poisearch/d;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    invoke-virtual {v4, v5}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setDiscountInfo(Z)V

    .line 72
    :goto_1
    return-object v4

    .line 64
    :cond_1
    invoke-virtual {v4, v6}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setGroupbuyInfo(Z)V

    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {v4, v6}, Lcom/amap/api/services/poisearch/PoiItemDetail;->setDiscountInfo(Z)V

    goto :goto_1
.end method

.method protected a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 18
    const/4 v1, 0x0

    .line 20
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Lcom/amap/api/services/core/b;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    return-object v0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method protected e(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 76
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "[]"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
