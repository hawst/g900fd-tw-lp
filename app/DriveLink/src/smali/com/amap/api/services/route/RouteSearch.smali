.class public Lcom/amap/api/services/route/RouteSearch;
.super Ljava/lang/Object;
.source "RouteSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;,
        Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;,
        Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;,
        Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;,
        Lcom/amap/api/services/route/RouteSearch$FromAndTo;
    }
.end annotation


# static fields
.field public static final BusComfortable:I = 0x4

.field public static final BusDefault:I = 0x0

.field public static final BusLeaseChange:I = 0x2

.field public static final BusLeaseWalk:I = 0x3

.field public static final BusNoSubway:I = 0x5

.field public static final BusSaveMoney:I = 0x1

.field public static final DrivingAvoidCongestion:I = 0x4

.field public static final DrivingDefault:I = 0x0

.field public static final DrivingMultiStrategy:I = 0x5

.field public static final DrivingNoExpressways:I = 0x3

.field public static final DrivingNoHighAvoidCongestionSaveMoney:I = 0x9

.field public static final DrivingNoHighWay:I = 0x6

.field public static final DrivingNoHighWaySaveMoney:I = 0x7

.field public static final DrivingSaveMoney:I = 0x1

.field public static final DrivingSaveMoneyAvoidCongestion:I = 0x8

.field public static final DrivingShortDistance:I = 0x2

.field public static final WalkDefault:I = 0x0

.field public static final WalkMultipath:I = 0x1


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    new-instance v0, Lcom/amap/api/services/route/r;

    invoke-direct {v0, p0}, Lcom/amap/api/services/route/r;-><init>(Lcom/amap/api/services/route/RouteSearch;)V

    iput-object v0, p0, Lcom/amap/api/services/route/RouteSearch;->a:Landroid/os/Handler;

    .line 41
    iput-object p1, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    .line 42
    invoke-static {p1}, Lcom/amap/api/services/core/e;->b(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/amap/api/services/route/RouteSearch;)Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/amap/api/services/route/RouteSearch;->b:Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 14
    invoke-static {p0, p1}, Lcom/amap/api/services/route/RouteSearch;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 222
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 223
    const/4 v0, 0x1

    .line 228
    :goto_0
    return v0

    .line 225
    :cond_0
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 226
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 228
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public calculateBusRoute(Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;)Lcom/amap/api/services/route/BusRouteResult;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/services/core/AMapException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Lcom/amap/api/services/core/c;

    .line 89
    invoke-virtual {p1}, Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;->clone()Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;

    move-result-object v1

    .line 90
    new-instance v0, Lcom/amap/api/services/route/b;

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;->getFromAndTo()Lcom/amap/api/services/route/RouteSearch$FromAndTo;

    move-result-object v2

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;->getMode()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;->getNightFlag()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/services/route/b;-><init>(Lcom/amap/api/services/route/RouteSearch$FromAndTo;ILjava/lang/String;Ljava/lang/String;)V

    .line 92
    new-instance v2, Lcom/amap/api/services/route/d;

    iget-object v3, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/amap/api/services/core/e;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/amap/api/services/route/d;-><init>(Lcom/amap/api/services/route/b;Ljava/net/Proxy;)V

    .line 94
    invoke-virtual {v2}, Lcom/amap/api/services/route/d;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/route/BusRouteResult;

    .line 95
    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {v0, v1}, Lcom/amap/api/services/route/BusRouteResult;->setBusQuery(Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;)V

    .line 98
    :cond_0
    return-object v0
.end method

.method public calculateBusRouteAsyn(Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;)V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/amap/api/services/route/RouteSearch$2;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/services/route/RouteSearch$2;-><init>(Lcom/amap/api/services/route/RouteSearch;Lcom/amap/api/services/route/RouteSearch$BusRouteQuery;)V

    invoke-virtual {v0}, Lcom/amap/api/services/route/RouteSearch$2;->start()V

    .line 121
    return-void
.end method

.method public calculateDriveRoute(Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;)Lcom/amap/api/services/route/DriveRouteResult;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/services/core/AMapException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Lcom/amap/api/services/core/c;

    .line 126
    invoke-virtual {p1}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->clone()Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;

    move-result-object v6

    .line 127
    new-instance v0, Lcom/amap/api/services/route/i;

    invoke-virtual {v6}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->getFromAndTo()Lcom/amap/api/services/route/RouteSearch$FromAndTo;

    move-result-object v1

    invoke-virtual {v6}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->getMode()I

    move-result v2

    invoke-virtual {v6}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->getPassedByPoints()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v6}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->getAvoidpolygons()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v6}, Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;->getAvoidRoad()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/services/route/i;-><init>(Lcom/amap/api/services/route/RouteSearch$FromAndTo;ILjava/util/List;Ljava/util/List;Ljava/lang/String;)V

    .line 130
    new-instance v1, Lcom/amap/api/services/route/k;

    iget-object v2, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/amap/api/services/core/e;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/amap/api/services/route/k;-><init>(Lcom/amap/api/services/route/i;Ljava/net/Proxy;)V

    .line 132
    invoke-virtual {v1}, Lcom/amap/api/services/route/k;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/route/DriveRouteResult;

    .line 133
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0, v6}, Lcom/amap/api/services/route/DriveRouteResult;->setDriveQuery(Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;)V

    .line 136
    :cond_0
    return-object v0
.end method

.method public calculateDriveRouteAsyn(Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;)V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/amap/api/services/route/RouteSearch$3;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/services/route/RouteSearch$3;-><init>(Lcom/amap/api/services/route/RouteSearch;Lcom/amap/api/services/route/RouteSearch$DriveRouteQuery;)V

    invoke-virtual {v0}, Lcom/amap/api/services/route/RouteSearch$3;->start()V

    .line 160
    return-void
.end method

.method public calculateWalkRoute(Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;)Lcom/amap/api/services/route/WalkRouteResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/services/core/AMapException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/services/core/c;->a(Landroid/content/Context;)Lcom/amap/api/services/core/c;

    .line 52
    invoke-virtual {p1}, Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;->clone()Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;

    move-result-object v1

    .line 53
    new-instance v0, Lcom/amap/api/services/route/w;

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;->getFromAndTo()Lcom/amap/api/services/route/RouteSearch$FromAndTo;

    move-result-object v2

    invoke-virtual {v1}, Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;->getMode()I

    move-result v3

    const-string/jumbo v4, "0"

    invoke-direct {v0, v2, v3, v4}, Lcom/amap/api/services/route/w;-><init>(Lcom/amap/api/services/route/RouteSearch$FromAndTo;ILjava/lang/String;)V

    .line 55
    new-instance v2, Lcom/amap/api/services/route/y;

    iget-object v3, p0, Lcom/amap/api/services/route/RouteSearch;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/amap/api/services/core/e;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/amap/api/services/route/y;-><init>(Lcom/amap/api/services/route/w;Ljava/net/Proxy;)V

    .line 57
    invoke-virtual {v2}, Lcom/amap/api/services/route/y;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/route/WalkRouteResult;

    .line 58
    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v0, v1}, Lcom/amap/api/services/route/WalkRouteResult;->setWalkQuery(Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;)V

    .line 61
    :cond_0
    return-object v0
.end method

.method public calculateWalkRouteAsyn(Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;)V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/amap/api/services/route/RouteSearch$1;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/services/route/RouteSearch$1;-><init>(Lcom/amap/api/services/route/RouteSearch;Lcom/amap/api/services/route/RouteSearch$WalkRouteQuery;)V

    invoke-virtual {v0}, Lcom/amap/api/services/route/RouteSearch$1;->start()V

    .line 84
    return-void
.end method

.method public setRouteSearchListener(Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/amap/api/services/route/RouteSearch;->b:Lcom/amap/api/services/route/RouteSearch$OnRouteSearchListener;

    .line 47
    return-void
.end method
