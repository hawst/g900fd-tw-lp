.class Lcom/amap/api/mapcore/af;
.super Landroid/view/View;
.source "MapOverlayImageView.java"


# instance fields
.field a:Lcom/amap/api/mapcore/q;

.field private b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/mapcore/t;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/autonavi/amap/mapcore/IPoint;

.field private d:Lcom/amap/api/mapcore/t;

.field private final e:Landroid/os/Handler;

.field private f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/mapcore/q;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 124
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/af;->e:Landroid/os/Handler;

    .line 138
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/af;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 45
    iput-object p3, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/af;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/amap/api/mapcore/af;->g()V

    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 200
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 201
    iget-object v2, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    invoke-interface {v2}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->b()Landroid/graphics/Rect;

    move-result-object v2

    .line 203
    new-instance v3, Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->q()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-direct {v3, v0, v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v3, p0, Lcom/amap/api/mapcore/af;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 205
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->m()V

    goto :goto_0

    .line 208
    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lcom/amap/api/mapcore/q;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    return-object v0
.end method

.method public a(Landroid/view/MotionEvent;)Lcom/amap/api/mapcore/t;
    .locals 6

    .prologue
    .line 219
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 220
    const/4 v1, 0x0

    move v2, v0

    .line 221
    :goto_0
    if-ltz v2, :cond_1

    .line 222
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 223
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->b()Landroid/graphics/Rect;

    move-result-object v3

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/amap/api/mapcore/af;->a(Landroid/graphics/Rect;II)Z

    move-result v3

    .line 225
    if-eqz v3, :cond_0

    .line 230
    :goto_1
    return-object v0

    .line 221
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lcom/amap/api/mapcore/t;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 50
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/t;)V
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 75
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method a(Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 153
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/h;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 157
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->e:Landroid/os/Handler;

    new-instance v1, Lcom/amap/api/mapcore/af$1;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/af$1;-><init>(Lcom/amap/api/mapcore/af;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 163
    iget-object v2, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0, p1, v2}, Lcom/amap/api/mapcore/t;->a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/q;)V

    goto :goto_1

    .line 165
    :cond_1
    return-void
.end method

.method public a(Landroid/graphics/Rect;II)Z
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 255
    const/4 v2, 0x0

    .line 256
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 257
    :goto_0
    if-ltz v3, :cond_1

    .line 258
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 259
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->b()Landroid/graphics/Rect;

    move-result-object v4

    .line 260
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v4, v1, v5}, Lcom/amap/api/mapcore/af;->a(Landroid/graphics/Rect;II)Z

    move-result v1

    .line 261
    if-eqz v1, :cond_0

    .line 263
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v4, Landroid/graphics/Rect;->left:I

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->q()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-direct {v2, v3, v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v2, p0, Lcom/amap/api/mapcore/af;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 265
    iput-object v0, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    move v0, v1

    .line 287
    :goto_1
    return v0

    .line 257
    :cond_0
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public b(Lcom/amap/api/mapcore/t;)Z
    .locals 1

    .prologue
    .line 80
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->e()I

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/af;->a(Ljava/lang/Integer;)V

    .line 83
    :cond_0
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 84
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 67
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/af;->a(Ljava/lang/Integer;)V

    .line 68
    iget-object v2, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->e()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/amap/api/mapcore/q;->c(I)V

    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 71
    return-void
.end method

.method public c(Lcom/amap/api/mapcore/t;)V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 89
    iget-object v1, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 91
    iget-object v2, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v3, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method public d()Lcom/amap/api/mapcore/t;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    return-object v0
.end method

.method public d(Lcom/amap/api/mapcore/t;)V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->c:Lcom/autonavi/amap/mapcore/IPoint;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/af;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 103
    :cond_0
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 104
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->q()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>(II)V

    iput-object v1, p0, Lcom/amap/api/mapcore/af;->c:Lcom/autonavi/amap/mapcore/IPoint;

    .line 105
    iput-object p1, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/af;->d()Lcom/amap/api/mapcore/t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/mapcore/t;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_0
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 109
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    .line 240
    if-eqz v0, :cond_0

    .line 241
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->n()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 248
    const-string/jumbo v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "MapOverlayImageView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :goto_1
    return-void

    .line 245
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/amap/api/mapcore/af;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public e(Lcom/amap/api/mapcore/t;)V
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/af;->f(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->A()V

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/af;->d:Lcom/amap/api/mapcore/t;

    .line 118
    :cond_0
    return-void
.end method

.method public f()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 295
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 296
    new-instance v8, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->j()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v2}, Lcom/amap/api/mapcore/q;->k()I

    move-result v2

    invoke-direct {v8, v0, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 298
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    move v6, v0

    .line 299
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 301
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    iget-wide v1, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/mapcore/q;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 302
    iget v0, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {p0, v8, v0, v1}, Lcom/amap/api/mapcore/af;->a(Landroid/graphics/Rect;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    iget-object v0, p0, Lcom/amap/api/mapcore/af;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/t;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 306
    :cond_1
    return-object v7
.end method

.method public f(Lcom/amap/api/mapcore/t;)Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/q;->b(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    return v0
.end method
