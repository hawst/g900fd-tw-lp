.class Lcom/amap/api/mapcore/ag;
.super Landroid/view/ViewGroup;
.source "MapOverlayViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/ag$a;
    }
.end annotation


# instance fields
.field private a:Lcom/amap/api/mapcore/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/q;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p2, p0, Lcom/amap/api/mapcore/ag;->a:Lcom/amap/api/mapcore/q;

    .line 20
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/ag;->setBackgroundColor(I)V

    .line 21
    return-void
.end method

.method private a(Landroid/view/View;IIIII)V
    .locals 3

    .prologue
    .line 170
    .line 172
    and-int/lit8 v0, p6, 0x7

    .line 173
    and-int/lit8 v1, p6, 0x70

    .line 175
    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 176
    sub-int/2addr p4, p2

    .line 181
    :cond_0
    :goto_0
    const/16 v0, 0x50

    if-ne v1, v0, :cond_3

    .line 182
    sub-int/2addr p5, p3

    .line 186
    :cond_1
    :goto_1
    add-int v0, p4, p2

    add-int v1, p5, p3

    invoke-virtual {p1, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 187
    return-void

    .line 177
    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 178
    div-int/lit8 v0, p2, 0x2

    sub-int/2addr p4, v0

    goto :goto_0

    .line 183
    :cond_3
    const/16 v0, 0x10

    if-ne v1, v0, :cond_1

    .line 184
    div-int/lit8 v0, p3, 0x2

    sub-int/2addr p5, v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 142
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    aput v1, p4, v2

    .line 144
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    aput v0, p4, v3

    .line 147
    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 148
    :cond_1
    invoke-virtual {p1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 150
    :cond_2
    if-ne p2, v4, :cond_3

    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    .line 158
    :goto_0
    if-ne p3, v4, :cond_5

    .line 159
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    .line 166
    :goto_1
    return-void

    .line 152
    :cond_3
    if-ne p2, v5, :cond_4

    .line 153
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    goto :goto_0

    .line 155
    :cond_4
    aput p2, p4, v2

    goto :goto_0

    .line 160
    :cond_5
    if-ne p3, v5, :cond_6

    .line 161
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    goto :goto_1

    .line 163
    :cond_6
    aput p3, p4, v3

    goto :goto_1
.end method

.method private a(Landroid/view/View;Lcom/amap/api/mapcore/ag$a;)V
    .locals 7

    .prologue
    .line 108
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 109
    iget v1, p2, Lcom/amap/api/mapcore/ag$a;->width:I

    iget v2, p2, Lcom/amap/api/mapcore/ag$a;->height:I

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;II[I)V

    .line 110
    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v1, 0x1

    aget v3, v0, v1

    iget v4, p2, Lcom/amap/api/mapcore/ag$a;->c:I

    iget v5, p2, Lcom/amap/api/mapcore/ag$a;->d:I

    iget v6, p2, Lcom/amap/api/mapcore/ag$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;IIIII)V

    .line 112
    return-void
.end method

.method private b(Landroid/view/View;Lcom/amap/api/mapcore/ag$a;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 115
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 116
    iget v1, p2, Lcom/amap/api/mapcore/ag$a;->width:I

    iget v2, p2, Lcom/amap/api/mapcore/ag$a;->height:I

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;II[I)V

    .line 117
    instance-of v1, p1, Lcom/amap/api/mapcore/as;

    if-eqz v1, :cond_1

    .line 118
    aget v2, v0, v4

    aget v3, v0, v11

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getWidth()I

    move-result v1

    aget v0, v0, v4

    sub-int v4, v1, v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getHeight()I

    move-result v5

    iget v6, p2, Lcom/amap/api/mapcore/ag$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;IIIII)V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    instance-of v1, p1, Lcom/amap/api/mapcore/ab;

    if-eqz v1, :cond_2

    .line 122
    aget v2, v0, v4

    aget v3, v0, v11

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getWidth()I

    move-result v1

    aget v4, v0, v4

    sub-int v4, v1, v4

    aget v5, v0, v11

    iget v6, p2, Lcom/amap/api/mapcore/ag$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 125
    :cond_2
    instance-of v1, p1, Lcom/amap/api/mapcore/k;

    if-eqz v1, :cond_3

    .line 126
    aget v2, v0, v4

    aget v3, v0, v11

    iget v6, p2, Lcom/amap/api/mapcore/ag$a;->e:I

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 128
    :cond_3
    iget-object v1, p2, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    if-eqz v1, :cond_0

    .line 129
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 130
    iget-object v5, p0, Lcom/amap/api/mapcore/ag;->a:Lcom/amap/api/mapcore/q;

    iget-object v1, p2, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v1, p2, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v5 .. v10}, Lcom/amap/api/mapcore/q;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 132
    iget v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v2, p2, Lcom/amap/api/mapcore/ag$a;->c:I

    add-int/2addr v1, v2

    iput v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 133
    iget v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v2, p2, Lcom/amap/api/mapcore/ag$a;->d:I

    add-int/2addr v1, v2

    iput v1, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 135
    aget v2, v0, v4

    aget v3, v0, v11

    iget v4, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, p2, Lcom/amap/api/mapcore/ag$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;IIIII)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 104
    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/mapcore/ag;->onLayout(ZIIII)V

    .line 105
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ag;->getChildCount()I

    move-result v2

    .line 78
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 79
    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/ag;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 80
    if-nez v3, :cond_0

    .line 78
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/amap/api/mapcore/ag$a;

    if-eqz v0, :cond_1

    .line 84
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ag$a;

    .line 86
    invoke-direct {p0, v3, v0}, Lcom/amap/api/mapcore/ag;->b(Landroid/view/View;Lcom/amap/api/mapcore/ag$a;)V

    goto :goto_1

    .line 95
    :cond_1
    new-instance v0, Lcom/amap/api/mapcore/ag$a;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/amap/api/mapcore/ag$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    invoke-direct {p0, v3, v0}, Lcom/amap/api/mapcore/ag;->a(Landroid/view/View;Lcom/amap/api/mapcore/ag$a;)V

    goto :goto_1

    .line 101
    :cond_2
    return-void
.end method
