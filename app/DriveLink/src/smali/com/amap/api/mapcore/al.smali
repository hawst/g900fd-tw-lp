.class Lcom/amap/api/mapcore/al;
.super Ljava/lang/Object;
.source "PolylineDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/w;


# instance fields
.field a:F

.field b:F

.field c:F

.field d:F

.field e:[F

.field f:[F

.field g:[F

.field private h:Lcom/amap/api/mapcore/q;

.field private i:F

.field private j:I

.field private k:F

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/nio/FloatBuffer;

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:Lcom/amap/api/maps/model/LatLngBounds;


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/q;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/mapcore/al;->i:F

    .line 34
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/amap/api/mapcore/al;->j:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/al;->k:F

    .line 36
    iput-boolean v2, p0, Lcom/amap/api/mapcore/al;->l:Z

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    .line 40
    iput v1, p0, Lcom/amap/api/mapcore/al;->p:I

    .line 41
    iput-boolean v1, p0, Lcom/amap/api/mapcore/al;->q:Z

    .line 42
    iput-boolean v1, p0, Lcom/amap/api/mapcore/al;->r:Z

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->s:Ljava/util/List;

    .line 52
    iput-boolean v2, p0, Lcom/amap/api/mapcore/al;->t:Z

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->u:Lcom/amap/api/maps/model/LatLngBounds;

    .line 414
    const/16 v0, 0x1e0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->f:[F

    .line 415
    const/16 v0, 0x3c

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->g:[F

    .line 89
    iput-object p1, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    .line 91
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->m:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private m()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 261
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 262
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 263
    if-eqz v0, :cond_0

    .line 264
    new-instance v3, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 265
    iget-object v4, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-interface {v4, v5, v0, v3}, Lcom/amap/api/mapcore/q;->b(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 266
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 271
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method a(Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;DI)Lcom/autonavi/amap/mapcore/IPoint;
    .locals 9

    .prologue
    .line 131
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 132
    iget v1, p2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v2, p1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v1, v2

    int-to-double v1, v1

    .line 133
    iget v3, p2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, p1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v4

    int-to-double v3, v3

    .line 134
    mul-double v5, v3, v3

    mul-double v7, v1, v1

    div-double/2addr v5, v7

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    add-double/2addr v5, v7

    .line 135
    int-to-double v7, p6

    mul-double/2addr v7, p4

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    div-double v5, v7, v5

    iget v7, p3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v7, v7

    add-double/2addr v5, v7

    double-to-int v5, v5

    iput v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 136
    iget v5, p3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v5, v6

    int-to-double v5, v5

    mul-double/2addr v3, v5

    div-double v1, v3, v1

    iget v3, p3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v3, v3

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 138
    return-object v0
.end method

.method public a(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 302
    iput p1, p0, Lcom/amap/api/mapcore/al;->k:F

    .line 303
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 304
    return-void
.end method

.method public a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 287
    iput p1, p0, Lcom/amap/api/mapcore/al;->j:I

    .line 288
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/al;->a:F

    .line 289
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/al;->b:F

    .line 290
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/al;->c:F

    .line 291
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/al;->d:F

    .line 292
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 293
    return-void
.end method

.method a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;Ljava/util/List;Lcom/amap/api/maps/model/LatLngBounds$Builder;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amap/api/maps/model/LatLng;",
            "Lcom/amap/api/maps/model/LatLng;",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;",
            "Lcom/amap/api/maps/model/LatLngBounds$Builder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 189
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double v20, v2, v4

    .line 191
    new-instance v17, Lcom/amap/api/maps/model/LatLng;

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 195
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    .line 198
    move-object/from16 v0, v17

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const/16 v22, -0x1

    .line 200
    :goto_0
    new-instance v7, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v2 .. v7}, Lcom/amap/api/mapcore/q;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 202
    new-instance v13, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v13}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 203
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    move-object/from16 v0, p2

    iget-wide v9, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, p2

    iget-wide v11, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v8 .. v13}, Lcom/amap/api/mapcore/q;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 204
    new-instance v19, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v19 .. v19}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 205
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    move-object/from16 v0, v17

    iget-wide v15, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-wide/from16 v17, v0

    invoke-interface/range {v14 .. v19}, Lcom/amap/api/mapcore/q;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 209
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double v2, v2, v20

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 211
    iget v4, v7, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v5, v13, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    iget v6, v7, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v8, v13, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v6, v8

    int-to-double v8, v6

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v8, v8, v20

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    mul-double v20, v4, v8

    move-object/from16 v16, p0

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    .line 216
    invoke-virtual/range {v16 .. v22}, Lcom/amap/api/mapcore/al;->a(Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;Lcom/autonavi/amap/mapcore/IPoint;DI)Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v4

    .line 219
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 220
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    invoke-interface {v5, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/amap/api/mapcore/al;->a(Ljava/util/List;Ljava/util/List;D)V

    .line 226
    return-void

    .line 198
    :cond_0
    const/16 v22, 0x1

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->q:Z

    if-eqz v0, :cond_0

    .line 245
    iput-object p1, p0, Lcom/amap/api/mapcore/al;->s:Ljava/util/List;

    .line 247
    :cond_0
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/al;->b(Ljava/util/List;)V

    .line 248
    return-void
.end method

.method a(Ljava/util/List;Ljava/util/List;D)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;D)V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 181
    :cond_0
    return-void

    .line 160
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    .line 162
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v2, 0xa

    if-gt v3, v2, :cond_0

    .line 163
    int-to-float v2, v3

    const/high16 v5, 0x41200000    # 10.0f

    div-float v5, v2, v5

    .line 164
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 165
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    float-to-double v9, v5

    sub-double/2addr v7, v9

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    float-to-double v11, v5

    sub-double/2addr v9, v11

    mul-double/2addr v7, v9

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v9, v2

    mul-double/2addr v7, v9

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v9, v2

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    mul-double/2addr v9, v11

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v11, v2

    mul-double/2addr v9, v11

    mul-double v9, v9, p3

    add-double/2addr v7, v9

    mul-float v9, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v9

    float-to-double v9, v2

    add-double/2addr v7, v9

    .line 168
    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    float-to-double v11, v5

    sub-double/2addr v9, v11

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    mul-double/2addr v9, v11

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v11, v2

    mul-double/2addr v9, v11

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v11, v2

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    mul-double/2addr v11, v13

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v13, v2

    mul-double/2addr v11, v13

    mul-double v11, v11, p3

    add-double/2addr v9, v11

    mul-float v11, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v11

    float-to-double v11, v2

    add-double/2addr v9, v11

    .line 172
    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    mul-double/2addr v11, v13

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v13, v2

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v17, v0

    sub-double v15, v15, v17

    mul-double/2addr v13, v15

    mul-double v13, v13, p3

    add-double/2addr v11, v13

    mul-float v2, v5, v5

    float-to-double v13, v2

    add-double/2addr v11, v13

    .line 173
    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v17, v0

    sub-double v15, v15, v17

    mul-double/2addr v13, v15

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v15, v2

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v19, v0

    sub-double v17, v17, v19

    mul-double v15, v15, v17

    mul-double v15, v15, p3

    add-double/2addr v13, v15

    mul-float v2, v5, v5

    float-to-double v15, v2

    add-double/2addr v13, v15

    .line 175
    div-double/2addr v7, v11

    double-to-int v2, v7

    iput v2, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 176
    div-double v7, v9, v13

    double-to-int v2, v7

    iput v2, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 178
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    int-to-float v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    move v3, v2

    goto/16 :goto_0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 454
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/al;->i:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget v0, p0, Lcom/amap/api/mapcore/al;->p:I

    if-nez v0, :cond_2

    .line 461
    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->g()V

    .line 463
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/al;->p:I

    if-lez v0, :cond_0

    .line 464
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->t:Z

    if-eqz v0, :cond_4

    .line 501
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->h()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v2

    .line 503
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapLenWithWin(I)F

    move-result v8

    .line 508
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->r:Z

    if-eqz v0, :cond_3

    .line 509
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->l()I

    move-result v3

    .line 513
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    iget-object v1, p0, Lcom/amap/api/mapcore/al;->e:[F

    array-length v1, v1

    iget v4, p0, Lcom/amap/api/mapcore/al;->b:F

    iget v5, p0, Lcom/amap/api/mapcore/al;->c:F

    iget v6, p0, Lcom/amap/api/mapcore/al;->d:F

    iget v7, p0, Lcom/amap/api/mapcore/al;->a:F

    iget-boolean v9, p0, Lcom/amap/api/mapcore/al;->r:Z

    invoke-static/range {v0 .. v9}, Lcom/amap/api/maps/AMapNativeRenderer;->nativeDrawLineByTextureID([FIFIFFFFFZ)V

    goto :goto_0

    .line 511
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->c()I

    move-result v3

    goto :goto_1

    .line 611
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    if-eqz v0, :cond_5

    .line 612
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    .line 614
    :cond_5
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->i()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->h()F

    move-result v4

    iget v5, p0, Lcom/amap/api/mapcore/al;->p:I

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/amap/api/mapcore/n;->a(Ljavax/microedition/khronos/opengles/GL10;IILjava/nio/FloatBuffer;FI)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 313
    iput-boolean p1, p0, Lcom/amap/api/mapcore/al;->l:Z

    .line 314
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 315
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 369
    iget-object v2, p0, Lcom/amap/api/mapcore/al;->u:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 376
    :cond_0
    :goto_0
    return v0

    .line 372
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v2}, Lcom/amap/api/mapcore/q;->D()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v2

    .line 373
    if-nez v2, :cond_2

    move v0, v1

    .line 374
    goto :goto_0

    .line 376
    :cond_2
    iget-object v3, p0, Lcom/amap/api/mapcore/al;->u:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps/model/LatLngBounds;->contains(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/mapcore/al;->u:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->intersects(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/u;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 325
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/u;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    :cond_0
    const/4 v0, 0x1

    .line 329
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/al;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Ljava/lang/String;)Z

    .line 231
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 232
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 276
    iput p1, p0, Lcom/amap/api/mapcore/al;->i:F

    .line 277
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 278
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 98
    .line 99
    invoke-static {}, Lcom/amap/api/maps/model/LatLngBounds;->builder()Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v7

    .line 100
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 101
    if-eqz p1, :cond_3

    .line 102
    const/4 v6, 0x0

    .line 103
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v6

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/amap/api/maps/model/LatLng;

    .line 104
    if-eqz v6, :cond_0

    invoke-virtual {v6, v1}, Lcom/amap/api/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->q:Z

    if-nez v0, :cond_2

    .line 108
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    iget-wide v1, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v6, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/mapcore/q;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 110
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-virtual {v7, v6}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    :cond_1
    :goto_1
    move-object v1, v6

    .line 117
    goto :goto_0

    .line 113
    :cond_2
    if-eqz v1, :cond_1

    .line 114
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-virtual {p0, v1, v6, v0, v7}, Lcom/amap/api/mapcore/al;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;Ljava/util/List;Lcom/amap/api/maps/model/LatLngBounds$Builder;)V

    goto :goto_1

    .line 120
    :cond_3
    invoke-virtual {v7}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->u:Lcom/amap/api/maps/model/LatLngBounds;

    .line 121
    iput v9, p0, Lcom/amap/api/mapcore/al;->p:I

    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    invoke-interface {v0, v9}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 124
    return-void
.end method

.method public b(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/amap/api/mapcore/al;->q:Z

    .line 62
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 63
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 237
    const-string/jumbo v0, "Polyline"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->m:Ljava/lang/String;

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->m:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/amap/api/mapcore/al;->t:Z

    .line 57
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 58
    return-void
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 308
    iget v0, p0, Lcom/amap/api/mapcore/al;->k:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->l:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 334
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 394
    const/4 v0, 0x0

    .line 395
    new-instance v2, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 397
    iget-object v1, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/amap/api/mapcore/al;->e:[F

    .line 398
    iget-object v1, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 400
    iget-object v4, p0, Lcom/amap/api/mapcore/al;->h:Lcom/amap/api/mapcore/q;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-interface {v4, v5, v0, v2}, Lcom/amap/api/mapcore/q;->a(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 401
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    mul-int/lit8 v4, v1, 0x3

    iget v5, v2, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v5, v0, v4

    .line 402
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x1

    iget v5, v2, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v5, v0, v4

    .line 403
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    mul-int/lit8 v4, v1, 0x3

    add-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    aput v5, v0, v4

    .line 404
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 406
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->t:Z

    if-nez v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/al;->p:I

    .line 410
    return-void
.end method

.method public h()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 282
    iget v0, p0, Lcom/amap/api/mapcore/al;->i:F

    return v0
.end method

.method public i()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 297
    iget v0, p0, Lcom/amap/api/mapcore/al;->j:I

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->q:Z

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->s:Ljava/util/List;

    .line 255
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/al;->m()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->q:Z

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/amap/api/mapcore/al;->r:Z

    return v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 659
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    if-eqz v0, :cond_0

    .line 660
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->e:[F

    .line 662
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 664
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/al;->o:Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 670
    :cond_1
    :goto_0
    return-void

    .line 666
    :catch_0
    move-exception v0

    .line 667
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 668
    const-string/jumbo v0, "destroy erro"

    const-string/jumbo v1, "PolylineDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
