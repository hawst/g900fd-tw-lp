.class Lcom/amap/api/mapcore/b$e;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/b;

.field private b:F


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    .line 1988
    iput-object p1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->b:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V
    .locals 0

    .prologue
    .line 1988
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$e;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2000
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2011
    :cond_0
    :goto_0
    return v5

    .line 2003
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 2004
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_2

    float-to-double v1, v0

    const-wide v3, 0x3ff147ae147ae148L    # 1.08

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_2

    float-to-double v1, v0

    const-wide v3, 0x3fed70a3d70a3d71L    # 0.92

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    .line 2005
    :cond_2
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2006
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {}, Lcom/amap/api/mapcore/b;->J()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 2007
    iget v1, p0, Lcom/amap/api/mapcore/b$e;->b:F

    add-float/2addr v0, v1

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->a(F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2009
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2020
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/aa;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 2035
    :cond_0
    :goto_0
    return v0

    .line 2023
    :catch_0
    move-exception v1

    .line 2024
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2029
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;I)I

    .line 2030
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2033
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2034
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->b:F

    .line 2035
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    .line 2040
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$e;->b:F

    .line 2041
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;Z)Z

    .line 2043
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2045
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 2046
    iget-object v1, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 2057
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$e;->a:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)V

    .line 2058
    return-void
.end method
