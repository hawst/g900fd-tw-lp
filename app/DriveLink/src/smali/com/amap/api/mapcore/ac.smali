.class public Lcom/amap/api/mapcore/ac;
.super Ljava/lang/Object;
.source "MapFragmentDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/s;


# static fields
.field public static a:Landroid/content/Context;


# instance fields
.field private b:Lcom/amap/api/mapcore/q;

.field private c:Lcom/amap/api/maps/AMapOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-nez v0, :cond_2

    .line 93
    sget-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 94
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    sput-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    .line 97
    :cond_0
    sget-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Context \u4e3a null \u8bf7\u5728\u5730\u56fe\u8c03\u7528\u4e4b\u524d \u4f7f\u7528 MapsInitializer.initialize(Context paramContext) \u6765\u8bbe\u7f6eContext"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_1
    sget-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 103
    const/16 v1, 0x78

    if-gt v0, v1, :cond_4

    .line 104
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    .line 117
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/b;

    sget-object v1, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    if-nez v0, :cond_3

    if-eqz p3, :cond_3

    .line 123
    const-string/jumbo v0, "MapOptions"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/AMapOptions;

    iput-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/ac;->b(Lcom/amap/api/maps/AMapOptions;)V

    .line 128
    const-string/jumbo v0, "MapFragmentDelegateImp"

    const-string/jumbo v1, "onCreateView"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/f;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 130
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->z()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 105
    :cond_4
    const/16 v1, 0xa0

    if-gt v0, v1, :cond_5

    .line 106
    const v0, 0x3f19999a    # 0.6f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 107
    :cond_5
    const/16 v1, 0xf0

    if-gt v0, v1, :cond_6

    .line 108
    const v0, 0x3f5eb852    # 0.87f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 109
    :cond_6
    const/16 v1, 0x140

    if-gt v0, v1, :cond_7

    .line 110
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 111
    :cond_7
    const/16 v1, 0x1e0

    if-gt v0, v1, :cond_8

    .line 112
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 114
    :cond_8
    const v0, 0x3f666666    # 0.9f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0
.end method

.method public a()Lcom/amap/api/mapcore/q;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-nez v0, :cond_1

    .line 39
    sget-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Context \u4e3a null \u8bf7\u5728\u5730\u56fe\u8c03\u7528\u4e4b\u524d \u4f7f\u7528 MapsInitializer.initialize(Context paramContext) \u6765\u8bbe\u7f6eContext"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    sget-object v0, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 45
    const/16 v1, 0x78

    if-gt v0, v1, :cond_2

    .line 46
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    .line 59
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/b;

    sget-object v1, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    return-object v0

    .line 47
    :cond_2
    const/16 v1, 0xa0

    if-gt v0, v1, :cond_3

    .line 48
    const v0, 0x3f4ccccd    # 0.8f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 49
    :cond_3
    const/16 v1, 0xf0

    if-gt v0, v1, :cond_4

    .line 50
    const v0, 0x3f5eb852    # 0.87f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 51
    :cond_4
    const/16 v1, 0x140

    if-gt v0, v1, :cond_5

    .line 52
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 53
    :cond_5
    const/16 v1, 0x1e0

    if-gt v0, v1, :cond_6

    .line 54
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0

    .line 56
    :cond_6
    const v0, 0x3f666666    # 0.9f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 23
    sput-object p1, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    .line 24
    return-void
.end method

.method public a(Landroid/app/Activity;Lcom/amap/api/maps/AMapOptions;Landroid/os/Bundle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    sput-object p1, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 72
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    const-string/jumbo v0, "MapFragmentDelegateImp"

    const-string/jumbo v1, "onCreate"

    const/16 v2, 0x71

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/f;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMapOptions;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 28
    return-void
.end method

.method public b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->onResume()V

    .line 158
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Lcom/amap/api/maps/AMapOptions;

    invoke-direct {v0}, Lcom/amap/api/maps/AMapOptions;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ac;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->n()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMapOptions;->camera(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/maps/AMapOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 228
    const-string/jumbo v0, "MapOptions"

    iget-object v1, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 231
    :cond_1
    return-void
.end method

.method b(Lcom/amap/api/maps/AMapOptions;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 134
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getCamera()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    iget-object v1, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    iget-object v2, v0, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget v3, v0, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    iget v4, v0, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    iget v0, v0, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v2, v3, v4, v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/mapcore/i;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    .line 142
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getRotateGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->h(Z)V

    .line 143
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getScrollGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->e(Z)V

    .line 144
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getTiltGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->g(Z)V

    .line 145
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZoomControlsEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->b(Z)V

    .line 146
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZoomGesturesEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->f(Z)V

    .line 147
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getCompassEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->c(Z)V

    .line 148
    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getScaleControlsEnabled()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/aa;->a(Z)V

    .line 149
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    invoke-virtual {p1}, Lcom/amap/api/maps/AMapOptions;->getZOrderOnTop()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->setZOrderOnTop(Z)V

    .line 151
    :cond_1
    return-void
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->onPause()V

    .line 182
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 198
    return-void
.end method

.method public e()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ac;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ac;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->f()V

    .line 206
    :cond_0
    iput-object v1, p0, Lcom/amap/api/mapcore/ac;->b:Lcom/amap/api/mapcore/q;

    .line 207
    iput-object v1, p0, Lcom/amap/api/mapcore/ac;->c:Lcom/amap/api/maps/AMapOptions;

    .line 209
    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    return-void

    .line 210
    :catch_0
    move-exception v0

    .line 211
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 217
    const-string/jumbo v0, "onLowMemory"

    const-string/jumbo v1, "onLowMemory run"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-void
.end method
