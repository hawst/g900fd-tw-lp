.class Lcom/amap/api/mapcore/as;
.super Landroid/widget/LinearLayout;
.source "ZoomControllerView.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Bitmap;

.field private f:Landroid/graphics/Bitmap;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Lcom/amap/api/mapcore/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/ae;Lcom/amap/api/mapcore/q;)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 52
    iput-object p3, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    .line 55
    :try_start_0
    const-string/jumbo v0, "zoomin_selected.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    .line 56
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    .line 58
    const-string/jumbo v0, "zoomin_unselected.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    .line 59
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    .line 62
    const-string/jumbo v0, "zoomout_selected.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    .line 63
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    .line 65
    const-string/jumbo v0, "zoomout_unselected.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    .line 66
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    .line 69
    const-string/jumbo v0, "zoomin_pressed.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    .line 70
    const-string/jumbo v0, "zoomout_pressed.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;

    .line 72
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    .line 74
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;

    sget v1, Lcom/amap/api/mapcore/l;->a:F

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    .line 82
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/mapcore/as$1;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/as$1;-><init>(Lcom/amap/api/mapcore/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    .line 99
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/mapcore/as$2;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/as$2;-><init>(Lcom/amap/api/mapcore/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/mapcore/as$3;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/as$3;-><init>(Lcom/amap/api/mapcore/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 139
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/mapcore/as$4;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/as$4;-><init>(Lcom/amap/api/mapcore/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 169
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    const/4 v1, -0x2

    invoke-virtual {v0, v2, v2, v3, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 170
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v2, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 171
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/as;->setOrientation(I)V

    .line 173
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/as;->addView(Landroid/view/View;)V

    .line 174
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/as;->addView(Landroid/view/View;)V

    .line 176
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/mapcore/as;)Lcom/amap/api/mapcore/q;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    return-object v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/as;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic f(Lcom/amap/api/mapcore/as;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 30
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 31
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 32
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 33
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 34
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->e:Landroid/graphics/Bitmap;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/as;->f:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->o()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->p()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 182
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->p()F

    move-result v0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 185
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->o()F

    move-result v0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 188
    iget-object v0, p0, Lcom/amap/api/mapcore/as;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/mapcore/as;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
