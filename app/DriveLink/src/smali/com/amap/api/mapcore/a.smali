.class Lcom/amap/api/mapcore/a;
.super Lcom/autonavi/amap/mapcore/BaseMapCallImplement;
.source "AMapCallback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/a$2;
    }
.end annotation


# instance fields
.field a:Lcom/autonavi/amap/mapcore/IPoint;

.field b:F

.field c:F

.field d:F

.field e:Lcom/autonavi/amap/mapcore/IPoint;

.field private f:Lcom/amap/api/mapcore/b;

.field private final g:Landroid/os/Handler;

.field private h:F


# direct methods
.method public constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;-><init>()V

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/a;->g:Landroid/os/Handler;

    .line 56
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/amap/api/mapcore/a;->h:F

    .line 112
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    .line 116
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    .line 36
    iput-object p1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/a;)Lcom/amap/api/mapcore/b;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    return-object v0
.end method

.method private b(Lcom/amap/api/mapcore/i;)V
    .locals 19

    .prologue
    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/b;->b()Lcom/autonavi/amap/mapcore/MapCore;

    move-result-object v5

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/b;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v6

    .line 196
    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 198
    :goto_0
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 199
    move-object/from16 v0, p1

    iget v8, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 200
    move-object/from16 v0, p1

    iget v9, v0, Lcom/amap/api/mapcore/i;->l:I

    .line 201
    move-object/from16 v0, p1

    iget v10, v0, Lcom/amap/api/mapcore/i;->j:I

    .line 202
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v11, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v13, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v3, v11, v12, v13, v14}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    invoke-static {v3, v4}, Lcom/amap/api/maps/AMapUtils;->calculateLineDistance(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)F

    move-result v11

    .line 205
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v12, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v14, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v3, v12, v13, v14, v15}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    invoke-static {v3, v4}, Lcom/amap/api/maps/AMapUtils;->calculateLineDistance(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)F

    move-result v12

    .line 208
    const/4 v3, 0x0

    cmpl-float v3, v11, v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    cmpl-float v3, v12, v3

    if-nez v3, :cond_2

    .line 272
    :goto_1
    return-void

    .line 196
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 210
    :cond_2
    const/4 v3, 0x0

    .line 211
    sget-object v4, Lcom/amap/api/mapcore/b/h;->b:[D

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    :goto_2
    if-ltz v4, :cond_3

    .line 212
    float-to-double v13, v11

    sget-object v15, Lcom/amap/api/mapcore/b/h;->b:[D

    aget-wide v15, v15, v4

    mul-int/lit8 v17, v10, 0x2

    sub-int v17, v8, v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    mul-double v15, v15, v17

    cmpg-double v13, v13, v15

    if-gtz v13, :cond_4

    float-to-double v13, v12

    sget-object v15, Lcom/amap/api/mapcore/b/h;->b:[D

    aget-wide v15, v15, v4

    mul-int/lit8 v17, v10, 0x2

    sub-int v17, v9, v17

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    mul-double v15, v15, v17

    cmpg-double v13, v13, v15

    if-gtz v13, :cond_4

    .line 214
    sget-object v3, Lcom/amap/api/mapcore/b/h;->a:[I

    aget v3, v3, v4

    .line 219
    :cond_3
    if-nez v2, :cond_5

    .line 220
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 221
    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v10, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v10, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v4, v7, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v12, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v12

    invoke-static {v8, v9, v10, v11, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 256
    iget v4, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v6, v4, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 257
    int-to-float v2, v3

    invoke-virtual {v6, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 258
    invoke-virtual {v5, v6}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto :goto_1

    .line 211
    :cond_4
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 260
    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 261
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->setCameraHeaderAngle(F)V

    .line 262
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapAngle(F)V

    .line 263
    invoke-virtual {v5, v6}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    .line 264
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 265
    invoke-static {v7, v8, v9, v10}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLngBounds;III)Lcom/amap/api/mapcore/i;

    move-result-object v3

    .line 267
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/amap/api/mapcore/i;->o:Z

    .line 268
    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 269
    const/16 v3, 0xc

    iput v3, v2, Landroid/os/Message;->what:I

    .line 270
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget-object v3, v3, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public OnMapDestory(Ljavax/microedition/khronos/opengles/GL10;Lcom/autonavi/amap/mapcore/MapCore;)V
    .locals 0

    .prologue
    .line 431
    invoke-super {p0, p2}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->OnMapDestory(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 432
    return-void
.end method

.method public OnMapLoaderError(I)V
    .locals 3

    .prologue
    .line 472
    const-string/jumbo v0, "MapCore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnMapLoaderError="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x70

    invoke-static {v0, v1, v2}, Lcom/amap/api/mapcore/b/f;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 474
    return-void
.end method

.method public OnMapProcessEvent(Lcom/autonavi/amap/mapcore/MapCore;)V
    .locals 12

    .prologue
    const/16 v1, 0x7da

    const/16 v11, 0x7db

    const/4 v3, 0x0

    .line 60
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->B()F

    move-result v10

    .line 61
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/a;->a(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 63
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ae;->a()Lcom/amap/api/mapcore/ad;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 64
    iget v0, v6, Lcom/amap/api/mapcore/ad;->a:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 65
    invoke-virtual {v6}, Lcom/amap/api/mapcore/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const/4 v2, 0x4

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 75
    :cond_1
    :goto_1
    iget v0, v6, Lcom/amap/api/mapcore/ad;->a:I

    if-ne v0, v11, :cond_0

    .line 76
    iget-boolean v0, v6, Lcom/amap/api/mapcore/ad;->b:Z

    if-eqz v0, :cond_3

    .line 77
    const/4 v6, 0x3

    move-object v4, p1

    move v5, v11

    move v7, v3

    move v8, v3

    move v9, v3

    invoke-virtual/range {v4 .. v9}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    :cond_2
    move-object v0, p1

    move v2, v3

    move v4, v3

    move v5, v3

    .line 70
    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_1

    :cond_3
    move-object v4, p1

    move v5, v11

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    .line 88
    invoke-virtual/range {v4 .. v9}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 98
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    .line 99
    iget v0, p0, Lcom/amap/api/mapcore/a;->b:F

    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->p()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    iget v0, p0, Lcom/amap/api/mapcore/a;->h:F

    cmpl-float v0, v0, v10

    if-eqz v0, :cond_5

    .line 100
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->g:Landroid/os/Handler;

    new-instance v1, Lcom/amap/api/mapcore/a$1;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/a$1;-><init>(Lcom/amap/api/mapcore/a;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 109
    :cond_5
    iput v10, p0, Lcom/amap/api/mapcore/a;->h:F

    .line 110
    return-void
.end method

.method public OnMapReferencechanged(Lcom/autonavi/amap/mapcore/MapCore;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 439
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->g()V

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->h()V

    .line 445
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b;->h(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->c()V

    .line 450
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->a()Lcom/amap/api/maps/CustomRenderer;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_2

    .line 452
    invoke-interface {v0}, Lcom/amap/api/maps/CustomRenderer;->OnMapReferencechanged()V

    .line 454
    :cond_2
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 447
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public OnMapSufaceChanged(Ljavax/microedition/khronos/opengles/GL10;Lcom/autonavi/amap/mapcore/MapCore;II)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public OnMapSurfaceCreate(Ljavax/microedition/khronos/opengles/GL10;Lcom/autonavi/amap/mapcore/MapCore;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p2}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->OnMapSurfaceCreate(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 42
    return-void
.end method

.method public OnMapSurfaceRenderer(Ljavax/microedition/khronos/opengles/GL10;Lcom/autonavi/amap/mapcore/MapCore;I)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method a(Lcom/amap/api/mapcore/i;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 282
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->b()Lcom/autonavi/amap/mapcore/MapCore;

    move-result-object v1

    .line 283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/b;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    .line 284
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/b;->b(F)F

    move-result v3

    move-object/from16 v0, p1

    iput v3, v0, Lcom/amap/api/mapcore/i;->d:F

    .line 286
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->f:F

    invoke-static {v3}, Lcom/amap/api/mapcore/b/h;->a(F)F

    move-result v3

    move-object/from16 v0, p1

    iput v3, v0, Lcom/amap/api/mapcore/i;->f:F

    .line 289
    sget-object v3, Lcom/amap/api/mapcore/a$2;->a:[I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    invoke-virtual {v4}, Lcom/amap/api/mapcore/i$a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 424
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    .line 427
    :goto_0
    return-void

    .line 291
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 293
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto :goto_0

    .line 297
    :pswitch_1
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->g:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapAngle(F)V

    .line 298
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto :goto_0

    .line 302
    :pswitch_2
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->f:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setCameraHeaderAngle(F)V

    .line 303
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto :goto_0

    .line 307
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 309
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 310
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto :goto_0

    .line 314
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    iget-object v3, v3, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    .line 315
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 316
    iget-wide v5, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v7, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v5, v6, v7, v8, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 318
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    iget v3, v3, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    invoke-static {v3}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v3

    .line 320
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    iget v5, v5, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    .line 321
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    iget v6, v6, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v6}, Lcom/amap/api/mapcore/b/h;->a(F)F

    move-result v6

    .line 324
    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v7, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 325
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 326
    invoke-virtual {v2, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapAngle(F)V

    .line 327
    invoke-virtual {v2, v6}, Lcom/autonavi/amap/mapcore/MapProjection;->setCameraHeaderAngle(F)V

    .line 328
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 332
    :pswitch_5
    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    .line 333
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v4, v3}, Lcom/amap/api/mapcore/b;->b(F)F

    move-result v3

    .line 334
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 335
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 339
    :pswitch_6
    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    .line 340
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v4, v3}, Lcom/amap/api/mapcore/b;->b(F)F

    move-result v3

    .line 341
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 342
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 346
    :pswitch_7
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->d:F

    .line 347
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 348
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 352
    :pswitch_8
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->e:F

    .line 353
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    add-float/2addr v3, v5

    invoke-virtual {v4, v3}, Lcom/amap/api/mapcore/b;->b(F)F

    move-result v3

    .line 354
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 355
    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    sub-float v5, v3, v5

    .line 357
    if-eqz v4, :cond_0

    .line 358
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 359
    new-instance v7, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 360
    invoke-virtual {v2, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 361
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget v9, v4, Landroid/graphics/Point;->x:I

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v8, v9, v4, v6}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 362
    iget v4, v7, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v8, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v4, v8

    .line 363
    iget v7, v7, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v8, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v7, v8

    .line 364
    iget v8, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v8, v8

    int-to-double v10, v4

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    float-to-double v14, v5

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    div-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-int v4, v8

    .line 365
    iget v6, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v8, v6

    int-to-double v6, v7

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    float-to-double v12, v5

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    div-double v5, v6, v10

    add-double/2addr v5, v8

    double-to-int v5, v5

    .line 366
    invoke-virtual {v2, v4, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 367
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 371
    :goto_1
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 369
    :cond_0
    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    goto :goto_1

    .line 375
    :pswitch_9
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->b:F

    .line 376
    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/mapcore/i;->c:F

    .line 377
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v5}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v3, v5

    .line 378
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v5}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    .line 379
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 380
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {v6, v3, v4, v5}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 381
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 382
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 386
    :pswitch_a
    sget-object v1, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 387
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v1

    move-object/from16 v0, p1

    iput v1, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 388
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v1

    move-object/from16 v0, p1

    iput v1, v0, Lcom/amap/api/mapcore/i;->l:I

    .line 389
    invoke-direct/range {p0 .. p1}, Lcom/amap/api/mapcore/a;->b(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_0

    .line 393
    :pswitch_b
    invoke-direct/range {p0 .. p1}, Lcom/amap/api/mapcore/a;->b(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_0

    .line 397
    :pswitch_c
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->setGeoCenter(II)V

    .line 399
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapZoomer(F)V

    .line 400
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->g:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setMapAngle(F)V

    .line 401
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->f:F

    invoke-virtual {v2, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->setCameraHeaderAngle(F)V

    .line 402
    invoke-virtual {v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setMapstate(Lcom/autonavi/amap/mapcore/MapProjection;)V

    goto/16 :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method a(Lcom/autonavi/amap/mapcore/MapCore;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v4

    .line 120
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 121
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v6

    .line 122
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v7

    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v4, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    move v0, v1

    .line 128
    :goto_0
    iget-object v2, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget-object v2, v2, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/ae;->c()Lcom/amap/api/mapcore/i;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 130
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/a;->a(Lcom/amap/api/mapcore/i;)V

    .line 131
    iget-boolean v2, v2, Lcom/amap/api/mapcore/i;->o:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    or-int/2addr v0, v2

    goto :goto_0

    .line 141
    :catch_0
    move-exception v2

    .line 142
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 145
    :cond_0
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v2

    iput v2, p0, Lcom/amap/api/mapcore/a;->b:F

    .line 146
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v2

    iput v2, p0, Lcom/amap/api/mapcore/a;->c:F

    .line 147
    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    iput v2, p0, Lcom/amap/api/mapcore/a;->d:F

    .line 148
    iget-object v2, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v4, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 149
    iget v2, p0, Lcom/amap/api/mapcore/a;->b:F

    cmpl-float v2, v5, v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/amap/api/mapcore/a;->c:F

    cmpl-float v2, v2, v6

    if-nez v2, :cond_1

    iget v2, p0, Lcom/amap/api/mapcore/a;->d:F

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget-object v4, p0, Lcom/amap/api/mapcore/a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget-object v4, p0, Lcom/amap/api/mapcore/a;->e:Lcom/autonavi/amap/mapcore/IPoint;

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-eq v2, v4, :cond_2

    :cond_1
    move v1, v3

    .line 155
    :cond_2
    if-eqz v1, :cond_9

    .line 156
    :try_start_1
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 157
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->y()Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    move-result-object v1

    .line 159
    if-eqz v1, :cond_3

    .line 160
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 161
    iget-object v2, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget-object v3, p0, Lcom/amap/api/mapcore/a;->a:Lcom/autonavi/amap/mapcore/IPoint;

    iget v3, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v2, v3, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 162
    new-instance v2, Lcom/amap/api/maps/model/CameraPosition;

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v8, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v10, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v8, v9, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    iget v1, p0, Lcom/amap/api/mapcore/a;->b:F

    iget v4, p0, Lcom/amap/api/mapcore/a;->c:F

    iget v8, p0, Lcom/amap/api/mapcore/a;->d:F

    invoke-direct {v2, v3, v1, v4, v8}, Lcom/amap/api/maps/model/CameraPosition;-><init>(Lcom/amap/api/maps/model/LatLng;FFF)V

    .line 164
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/maps/model/CameraPosition;)V

    .line 166
    :cond_3
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->C()V

    .line 167
    if-eqz v0, :cond_8

    .line 168
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/b;->h(Z)V

    .line 174
    :goto_1
    if-eqz v0, :cond_4

    .line 175
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 176
    const/16 v1, 0x11

    iput v1, v0, Landroid/os/Message;->what:I

    .line 177
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 179
    :cond_4
    iget v0, p0, Lcom/amap/api/mapcore/a;->c:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_5

    iget v0, p0, Lcom/amap/api/mapcore/a;->d:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 181
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->g()V

    .line 183
    :cond_6
    iget v0, p0, Lcom/amap/api/mapcore/a;->b:F

    cmpl-float v0, v5, v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 185
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->h()V

    .line 190
    :cond_7
    :goto_2
    return-void

    .line 170
    :cond_8
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/b;->h(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 187
    :catch_1
    move-exception v0

    .line 188
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 172
    :cond_9
    :try_start_2
    iget-object v1, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/b;->e(Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getMapSvrAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string/jumbo v0, "http://m.amap.com"

    return-object v0
.end method

.method public isMapEngineValid()Z
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->b()Lcom/autonavi/amap/mapcore/MapCore;

    move-result-object v0

    if-nez v0, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 467
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSetParameter(Lcom/autonavi/amap/mapcore/MapCore;)V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/amap/api/mapcore/a;->f:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 486
    return-void
.end method
