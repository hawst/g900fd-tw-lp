.class Lcom/amap/api/mapcore/as$4;
.super Ljava/lang/Object;
.source "ZoomControllerView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/mapcore/as;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ae;Lcom/amap/api/mapcore/q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/mapcore/as;


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/as;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v0}, Lcom/amap/api/mapcore/as;->a(Lcom/amap/api/mapcore/as;)Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->B()F

    move-result v0

    iget-object v1, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v1}, Lcom/amap/api/mapcore/as;->a(Lcom/amap/api/mapcore/as;)Lcom/amap/api/mapcore/q;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->p()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v2

    .line 145
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v0}, Lcom/amap/api/mapcore/as;->f(Lcom/amap/api/mapcore/as;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v1}, Lcom/amap/api/mapcore/as;->e(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v0}, Lcom/amap/api/mapcore/as;->f(Lcom/amap/api/mapcore/as;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v1}, Lcom/amap/api/mapcore/as;->g(Lcom/amap/api/mapcore/as;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/as$4;->a:Lcom/amap/api/mapcore/as;

    invoke-static {v0}, Lcom/amap/api/mapcore/as;->a(Lcom/amap/api/mapcore/as;)Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-static {}, Lcom/amap/api/mapcore/i;->c()Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->b(Lcom/amap/api/mapcore/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
