.class Lcom/amap/api/mapcore/ah;
.super Ljava/lang/Object;
.source "MarkerDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/t;


# static fields
.field private static a:I


# instance fields
.field private b:F

.field private c:Ljava/nio/FloatBuffer;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Lcom/amap/api/maps/model/LatLng;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/amap/api/maps/model/BitmapDescriptor;

.field private j:F

.field private k:F

.field private l:Z

.field private m:Z

.field private n:Lcom/amap/api/mapcore/af;

.field private o:Ljava/nio/FloatBuffer;

.field private p:Ljava/lang/Object;

.field private q:Z

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/amap/api/mapcore/ah;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/af;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/ah;->b:F

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    .line 91
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/amap/api/mapcore/ah;->j:F

    .line 92
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/mapcore/ah;->k:F

    .line 93
    iput-boolean v1, p0, Lcom/amap/api/mapcore/ah;->l:Z

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ah;->m:Z

    .line 98
    iput-boolean v1, p0, Lcom/amap/api/mapcore/ah;->q:Z

    .line 390
    iput-boolean v1, p0, Lcom/amap/api/mapcore/ah;->r:Z

    .line 101
    iput-object p2, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    .line 102
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Lcom/amap/api/maps/model/LatLng;

    .line 105
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getAnchorU()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/ah;->j:F

    .line 106
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getAnchorV()F

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/ah;->k:F

    .line 107
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getIcon()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->clone()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 108
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ah;->m:Z

    .line 109
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getSnippet()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->h:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->g:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isDraggable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ah;->l:Z

    .line 112
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Lcom/amap/api/maps/model/MarkerOptions;->isPerspective()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ah;->q:Z

    .line 116
    return-void
.end method

.method private a(IILcom/autonavi/amap/mapcore/IPoint;)V
    .locals 9

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->q()I

    move-result v0

    .line 565
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->t()I

    move-result v1

    .line 567
    div-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 568
    div-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    .line 569
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    iget v4, p0, Lcom/amap/api/mapcore/ah;->b:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    .line 570
    int-to-double v3, v0

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    int-to-double v5, v1

    float-to-double v7, v2

    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-int v3, v3

    iput v3, p3, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 571
    int-to-double v3, v1

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    int-to-double v0, v0

    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v0, v5

    sub-double v0, v3, v0

    double-to-int v0, v0

    iput v0, p3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 572
    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V
    .locals 7

    .prologue
    const/16 v6, 0x1406

    const/16 v5, 0xbe2

    const/16 v4, 0xde1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 583
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 587
    const/4 v0, 0x1

    const/16 v1, 0x303

    invoke-interface {p1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 591
    invoke-interface {p1, v2, v2, v2, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 594
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 595
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 596
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glEnableClientState(I)V

    .line 597
    invoke-interface {p1, v4, p2}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    .line 602
    const/4 v0, 0x3

    invoke-interface {p1, v0, v6, v3, p3}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 603
    const/4 v0, 0x2

    invoke-interface {p1, v0, v6, v3, p4}, Ljavax/microedition/khronos/opengles/GL10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    .line 604
    const/4 v0, 0x6

    const/4 v1, 0x4

    invoke-interface {p1, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 606
    const v0, 0x8074

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 607
    const v0, 0x8078

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisableClientState(I)V

    .line 608
    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 609
    invoke-interface {p1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    sget v0, Lcom/amap/api/mapcore/ah;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/amap/api/mapcore/ah;->a:I

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/amap/api/mapcore/ah;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    iget-object v0, v0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    iget-object v0, v0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 217
    :cond_0
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 36
    neg-float v0, p1

    rem-float/2addr v0, v1

    add-float/2addr v0, v1

    rem-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/ah;->b:F

    .line 38
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/amap/api/mapcore/ah;->j:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/ah;->k:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 335
    :cond_0
    iput p1, p0, Lcom/amap/api/mapcore/ah;->j:F

    .line 336
    iput p2, p0, Lcom/amap/api/mapcore/ah;->k:F

    .line 337
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 339
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->d(Lcom/amap/api/mapcore/t;)V

    .line 341
    :cond_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/BitmapDescriptor;)V
    .locals 1

    .prologue
    .line 269
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ah;->r:Z

    .line 271
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    .line 275
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 277
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->d(Lcom/amap/api/mapcore/t;)V

    .line 279
    :cond_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 280
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->f:Lcom/amap/api/maps/model/LatLng;

    .line 235
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 236
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 670
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->p:Ljava/lang/Object;

    .line 671
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->g:Ljava/lang/String;

    .line 241
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 242
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Lcom/amap/api/mapcore/q;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 619
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ah;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ah;->r:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/ah;->e:I

    if-nez v0, :cond_6

    .line 634
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 635
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 636
    iget v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    if-eqz v1, :cond_5

    .line 638
    new-array v1, v4, [I

    iget v2, p0, Lcom/amap/api/mapcore/ah;->e:I

    aput v2, v1, v3

    invoke-interface {p1, v4, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    .line 647
    :cond_3
    :goto_1
    iget v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    invoke-static {p1, v1, v0}, Lcom/amap/api/mapcore/b/h;->a(Ljavax/microedition/khronos/opengles/GL10;ILandroid/graphics/Bitmap;)V

    .line 658
    :cond_4
    iput-boolean v4, p0, Lcom/amap/api/mapcore/ah;->r:Z

    goto :goto_0

    .line 640
    :cond_5
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    iget-object v1, v1, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->H()I

    move-result v1

    iput v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    .line 641
    iget v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    if-nez v1, :cond_3

    .line 642
    new-array v1, v4, [I

    aput v3, v1, v3

    .line 643
    invoke-interface {p1, v4, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 644
    aget v1, v1, v3

    iput v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    goto :goto_1

    .line 663
    :cond_6
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    iget v0, p0, Lcom/amap/api/mapcore/ah;->e:I

    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/amap/api/mapcore/ah;->a(Ljavax/microedition/khronos/opengles/GL10;ILjava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/amap/api/mapcore/ah;->l:Z

    .line 263
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 264
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 209
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    iget-object v0, v0, Lcom/amap/api/mapcore/af;->a:Lcom/amap/api/mapcore/q;

    iget v1, p0, Lcom/amap/api/mapcore/ah;->e:I

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->c(I)V

    .line 210
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->b(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/amap/api/mapcore/t;)Z
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    :cond_0
    const/4 v0, 0x1

    .line 360
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->u()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v1

    .line 198
    if-nez v1, :cond_0

    .line 199
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 203
    :goto_0
    return-object v0

    .line 201
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget v2, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->q()I

    move-result v5

    add-int/2addr v4, v5

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->t()I

    move-result v5

    add-int/2addr v1, v5

    invoke-direct {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/amap/api/mapcore/ah;->h:Ljava/lang/String;

    .line 252
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 253
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/amap/api/mapcore/ah;->m:Z

    .line 320
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 323
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 324
    return-void
.end method

.method public c()Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 680
    iput-boolean p1, p0, Lcom/amap/api/mapcore/ah;->q:Z

    .line 681
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 227
    const-string/jumbo v0, "Marker"

    invoke-static {v0}, Lcom/amap/api/mapcore/ah;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Ljava/lang/String;

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/amap/api/mapcore/ah;->e:I

    return v0
.end method

.method public f()Lcom/autonavi/amap/mapcore/FPoint;
    .locals 3

    .prologue
    .line 165
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->q()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/amap/api/mapcore/ah;->j:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 168
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->t()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/amap/api/mapcore/ah;->k:F

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 170
    :cond_0
    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ah;->l:Z

    return v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->d(Lcom/amap/api/mapcore/t;)V

    .line 301
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    goto :goto_0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->e(Lcom/amap/api/mapcore/t;)V

    .line 308
    invoke-direct {p0}, Lcom/amap/api/mapcore/ah;->z()V

    .line 310
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p0}, Lcom/amap/api/mapcore/af;->f(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ah;->m:Z

    return v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 47
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->a()Z

    .line 48
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    .line 64
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->f:Lcom/amap/api/maps/model/LatLng;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->p:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 74
    const-string/jumbo v0, "destroy erro"

    const-string/jumbo v1, "MarkerDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 365
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public p()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->p:Ljava/lang/Object;

    return-object v0
.end method

.method public q()I
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 129
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 130
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/af;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    .line 131
    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    .line 132
    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v2

    .line 133
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->s()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v3

    .line 134
    iget-boolean v4, p0, Lcom/amap/api/mapcore/ah;->q:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    const v4, 0x358637bd    # 1.0E-6f

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 138
    :cond_1
    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->k()I

    move-result v1

    .line 139
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 140
    int-to-double v6, v0

    iget v2, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    div-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v2, v6

    div-int/lit8 v1, v1, 0x2

    int-to-double v6, v1

    div-double v1, v2, v6

    div-double v3, v8, v4

    sub-double/2addr v3, v8

    mul-double/2addr v1, v3

    const-wide v3, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int/2addr v0, v1

    .line 142
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 685
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ah;->q:Z

    return v0
.end method

.method public s()Lcom/autonavi/amap/mapcore/IPoint;
    .locals 6

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    .line 120
    const/4 v5, 0x0

    .line 125
    :goto_0
    return-object v5

    .line 122
    :cond_0
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iget-wide v1, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    iget-wide v3, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/mapcore/q;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    goto :goto_0
.end method

.method public t()I
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 147
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getHeight()I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/af;->a()Lcom/amap/api/mapcore/q;

    move-result-object v1

    .line 149
    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    .line 150
    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v2

    .line 151
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->s()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v3

    .line 152
    iget-boolean v4, p0, Lcom/amap/api/mapcore/ah;->q:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    const v4, 0x358637bd    # 1.0E-6f

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    invoke-interface {v1}, Lcom/amap/api/mapcore/q;->k()I

    move-result v1

    .line 157
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 158
    int-to-double v6, v0

    iget v2, v3, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    div-int/lit8 v3, v1, 0x2

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v2, v6

    div-int/lit8 v1, v1, 0x2

    int-to-double v6, v1

    div-double v1, v2, v6

    div-double v3, v8, v4

    sub-double/2addr v3, v8

    mul-double/2addr v1, v3

    const-wide v3, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v1, v3

    double-to-int v1, v1

    add-int/2addr v0, v1

    .line 160
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Lcom/autonavi/amap/mapcore/IPoint;
    .locals 4

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->s()Lcom/autonavi/amap/mapcore/IPoint;

    move-result-object v0

    .line 184
    if-nez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->f()Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v1

    .line 188
    iget v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v2, v2

    iget v3, v1, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    .line 189
    iget v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v2, v2

    iget v1, v1, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sub-float v1, v2, v1

    float-to-int v1, v1

    iput v1, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    goto :goto_0
.end method

.method public v()Lcom/amap/api/maps/model/BitmapDescriptor;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    if-nez v0, :cond_0

    .line 285
    invoke-static {}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->defaultMarker()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->i:Lcom/amap/api/maps/model/BitmapDescriptor;

    return-object v0
.end method

.method public w()F
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/amap/api/mapcore/ah;->j:F

    return v0
.end method

.method public x()F
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/amap/api/mapcore/ah;->k:F

    return v0
.end method

.method public y()Z
    .locals 14

    .prologue
    .line 435
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    if-nez v0, :cond_0

    .line 436
    const/4 v0, 0x0

    .line 531
    :goto_0
    return v0

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ah;->n:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->a()Lcom/amap/api/mapcore/q;

    move-result-object v0

    .line 439
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 440
    if-nez v3, :cond_1

    .line 441
    const/4 v0, 0x0

    goto :goto_0

    .line 443
    :cond_1
    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v6

    .line 444
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 445
    iget-wide v1, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/mapcore/q;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 446
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->q()I

    move-result v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->t()I

    move-result v2

    .line 448
    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-float v3, v3

    int-to-float v4, v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->w()F

    move-result v7

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-float v4, v4

    int-to-float v5, v2

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->x()F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 455
    sub-int v5, v3, v1

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->j()I

    move-result v7

    if-gt v5, v7, :cond_2

    neg-int v5, v1

    mul-int/lit8 v5, v5, 0x2

    if-lt v3, v5, :cond_2

    neg-int v5, v2

    mul-int/lit8 v5, v5, 0x2

    if-lt v4, v5, :cond_2

    sub-int v5, v4, v2

    invoke-interface {v0}, Lcom/amap/api/mapcore/q;->k()I

    move-result v0

    if-le v5, v0, :cond_3

    .line 458
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 465
    :cond_3
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/BitmapDescriptor;->getWidth()I

    move-result v0

    .line 466
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/amap/api/maps/model/BitmapDescriptor;->getHeight()I

    move-result v5

    .line 467
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 468
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ah;->v()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/amap/api/maps/model/BitmapDescriptor;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 469
    int-to-float v0, v0

    int-to-float v8, v8

    div-float/2addr v0, v8

    .line 470
    int-to-float v5, v5

    int-to-float v7, v7

    div-float/2addr v5, v7

    .line 471
    iget-object v7, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    if-nez v7, :cond_4

    .line 472
    const/16 v7, 0x8

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    aput v5, v7, v8

    const/4 v8, 0x2

    aput v0, v7, v8

    const/4 v8, 0x3

    aput v5, v7, v8

    const/4 v5, 0x4

    aput v0, v7, v5

    const/4 v0, 0x5

    const/4 v5, 0x0

    aput v5, v7, v0

    const/4 v0, 0x6

    const/4 v5, 0x0

    aput v5, v7, v0

    const/4 v0, 0x7

    const/4 v5, 0x0

    aput v5, v7, v0

    invoke-static {v7}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->o:Ljava/nio/FloatBuffer;

    .line 476
    :cond_4
    const/16 v0, 0xc

    new-array v0, v0, [F

    .line 478
    new-instance v5, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 480
    new-instance v7, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 482
    new-instance v8, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 484
    new-instance v9, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v9}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 486
    iget v10, p0, Lcom/amap/api/mapcore/ah;->b:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-nez v10, :cond_5

    .line 487
    invoke-virtual {v6, v3, v4, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 488
    add-int v10, v3, v1

    invoke-virtual {v6, v10, v4, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 489
    add-int/2addr v1, v3

    sub-int v10, v4, v2

    invoke-virtual {v6, v1, v10, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 490
    sub-int v1, v4, v2

    invoke-virtual {v6, v3, v1, v9}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 507
    :goto_1
    const/4 v1, 0x0

    iget v2, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v2, v0, v1

    .line 508
    const/4 v1, 0x1

    iget v2, v5, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v2, v0, v1

    .line 509
    const/4 v1, 0x2

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 511
    const/4 v1, 0x3

    iget v2, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v2, v0, v1

    .line 512
    const/4 v1, 0x4

    iget v2, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v2, v0, v1

    .line 513
    const/4 v1, 0x5

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 515
    const/4 v1, 0x6

    iget v2, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v2, v0, v1

    .line 516
    const/4 v1, 0x7

    iget v2, v8, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v2, v0, v1

    .line 517
    const/16 v1, 0x8

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 519
    const/16 v1, 0x9

    iget v2, v9, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v2, v0, v1

    .line 520
    const/16 v1, 0xa

    iget v2, v9, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v2, v0, v1

    .line 521
    const/16 v1, 0xb

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 525
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    if-nez v1, :cond_6

    .line 526
    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    .line 531
    :goto_2
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 492
    :cond_5
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 493
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {p0, v11, v12, v10}, Lcom/amap/api/mapcore/ah;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 494
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v3

    iget v12, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    add-int/2addr v11, v12

    div-int/lit8 v12, v2, 0x2

    sub-int v12, v4, v12

    iget v13, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v12, v13

    invoke-virtual {v6, v11, v12, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 496
    const/4 v11, 0x0

    invoke-direct {p0, v1, v11, v10}, Lcom/amap/api/mapcore/ah;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 497
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v3

    iget v12, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    add-int/2addr v11, v12

    div-int/lit8 v12, v2, 0x2

    sub-int v12, v4, v12

    iget v13, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v12, v13

    invoke-virtual {v6, v11, v12, v7}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 499
    invoke-direct {p0, v1, v2, v10}, Lcom/amap/api/mapcore/ah;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 500
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v3

    iget v12, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    add-int/2addr v11, v12

    div-int/lit8 v12, v2, 0x2

    sub-int v12, v4, v12

    iget v13, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v12, v13

    invoke-virtual {v6, v11, v12, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 502
    const/4 v11, 0x0

    invoke-direct {p0, v11, v2, v10}, Lcom/amap/api/mapcore/ah;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 503
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    iget v3, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    add-int/2addr v1, v3

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v4, v2

    iget v3, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v2, v3

    invoke-virtual {v6, v1, v2, v9}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    goto/16 :goto_1

    .line 528
    :cond_6
    iget-object v1, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a([FLjava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ah;->c:Ljava/nio/FloatBuffer;

    goto :goto_2
.end method
