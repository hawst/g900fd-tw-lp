.class Lcom/amap/api/mapcore/b;
.super Landroid/opengl/GLSurfaceView;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/amap/api/mapcore/q;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/b$a;,
        Lcom/amap/api/mapcore/b$d;,
        Lcom/amap/api/mapcore/b$b;,
        Lcom/amap/api/mapcore/b$c;,
        Lcom/amap/api/mapcore/b$e;,
        Lcom/amap/api/mapcore/b$f;
    }
.end annotation


# static fields
.field private static final aj:D


# instance fields
.field private A:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

.field private B:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

.field private C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

.field private D:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

.field private E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

.field private F:Lcom/amap/api/maps/AMap$OnMapClickListener;

.field private G:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

.field private H:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

.field private I:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

.field private J:Landroid/view/View;

.field private K:Lcom/amap/api/maps/model/Marker;

.field private L:Lcom/amap/api/mapcore/x;

.field private M:Lcom/amap/api/mapcore/aa;

.field private N:Lcom/amap/api/maps/LocationSource;

.field private O:Landroid/graphics/Rect;

.field private P:Z

.field private Q:Z

.field private R:Lcom/amap/api/mapcore/f;

.field private S:Lcom/amap/api/mapcore/a/b;

.field private T:Lcom/amap/api/mapcore/ai;

.field private U:Lcom/amap/api/mapcore/g;

.field private V:Lcom/amap/api/mapcore/h;

.field private W:I

.field private X:I

.field private Y:Lcom/amap/api/maps/AMap$CancelableCallback;

.field private Z:Z

.field a:Lcom/amap/api/mapcore/ae;

.field private aA:Z

.field private aB:I

.field private aC:Z

.field private aD:Ljava/lang/Thread;

.field private aE:Lcom/amap/api/maps/model/LatLngBounds;

.field private aF:Z

.field private aa:Z

.field private ab:Z

.field private ac:Landroid/graphics/drawable/Drawable;

.field private ad:Landroid/location/Location;

.field private final ae:[I

.field private af:Z

.field private ag:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

.field private ah:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

.field private ai:Ljava/util/Timer;

.field private ak:Lcom/amap/api/maps/CustomRenderer;

.field private al:Lcom/amap/api/mapcore/m;

.field private am:Z

.field private an:Z

.field private ao:Landroid/os/Handler;

.field private ap:Ljava/lang/Runnable;

.field private aq:Z

.field private ar:Landroid/graphics/Bitmap;

.field private as:Landroid/graphics/Bitmap;

.field private at:Z

.field private au:Z

.field private av:Z

.field private aw:Z

.field private ax:Lcom/amap/api/maps/model/Marker;

.field private ay:Z

.field private az:Z

.field b:Lcom/amap/api/mapcore/as;

.field c:Z

.field d:Lcom/amap/api/mapcore/p;

.field final e:Landroid/os/Handler;

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Lcom/autonavi/amap/mapcore/MapCore;

.field private m:Landroid/content/Context;

.field private n:Lcom/amap/api/mapcore/a;

.field private o:Lcom/autonavi/amap/mapcore/MapProjection;

.field private p:Landroid/view/GestureDetector;

.field private q:Landroid/view/ScaleGestureDetector;

.field private r:Lcom/amap/api/mapcore/a/c;

.field private s:Landroid/view/SurfaceHolder;

.field private t:Lcom/amap/api/mapcore/ag;

.field private u:Lcom/amap/api/mapcore/af;

.field private v:Lcom/amap/api/mapcore/ar;

.field private w:Lcom/amap/api/mapcore/ab;

.field private x:Lcom/amap/api/mapcore/k;

.field private y:Lcom/amap/api/mapcore/an;

.field private z:Lcom/amap/api/mapcore/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 184
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/amap/api/mapcore/b;->aj:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/amap/api/mapcore/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 236
    sput-object p1, Lcom/amap/api/mapcore/ac;->a:Landroid/content/Context;

    .line 237
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    .line 238
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->s:Landroid/view/SurfaceHolder;

    .line 239
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->s:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v9, 0xff

    const/4 v8, 0x0

    const/16 v7, 0xeb

    const/4 v4, 0x0

    .line 274
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->f:I

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->g:I

    .line 113
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->h:Z

    .line 114
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 115
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 120
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/b;->k:I

    .line 123
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    .line 128
    new-instance v0, Lcom/amap/api/mapcore/ae;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ae;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    .line 129
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->s:Landroid/view/SurfaceHolder;

    .line 154
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->O:Landroid/graphics/Rect;

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->P:Z

    .line 156
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->Q:Z

    .line 157
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->c:Z

    .line 165
    iput v4, p0, Lcom/amap/api/mapcore/b;->W:I

    .line 166
    iput v4, p0, Lcom/amap/api/mapcore/b;->X:I

    .line 167
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 168
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->Z:Z

    .line 169
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aa:Z

    .line 170
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->ab:Z

    .line 171
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    .line 174
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ae:[I

    .line 177
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->af:Z

    .line 178
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->ag:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 179
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->ah:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 200
    new-instance v0, Lcom/amap/api/mapcore/p;

    invoke-direct {v0}, Lcom/amap/api/mapcore/p;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    .line 417
    new-instance v0, Lcom/amap/api/mapcore/m;

    invoke-direct {v0}, Lcom/amap/api/mapcore/m;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->al:Lcom/amap/api/mapcore/m;

    .line 786
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->am:Z

    .line 787
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->an:Z

    .line 788
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Landroid/os/Handler;

    .line 789
    new-instance v0, Lcom/amap/api/mapcore/c;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/c;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/lang/Runnable;

    .line 1025
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aq:Z

    .line 1073
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    .line 1074
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->as:Landroid/graphics/Bitmap;

    .line 1643
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 1902
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->au:Z

    .line 1906
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->av:Z

    .line 1908
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1909
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    .line 1910
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 1911
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->az:Z

    .line 1912
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aA:Z

    .line 1956
    iput v4, p0, Lcom/amap/api/mapcore/b;->aB:I

    .line 1960
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aC:Z

    .line 2631
    new-instance v0, Lcom/amap/api/mapcore/d;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/d;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->aD:Ljava/lang/Thread;

    .line 2649
    iput-object v8, p0, Lcom/amap/api/mapcore/b;->aE:Lcom/amap/api/maps/model/LatLngBounds;

    .line 2660
    new-instance v0, Lcom/amap/api/mapcore/e;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/e;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    .line 2863
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aF:Z

    .line 275
    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 276
    new-instance v0, Lcom/amap/api/mapcore/g;

    invoke-direct {v0}, Lcom/amap/api/mapcore/g;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->U:Lcom/amap/api/mapcore/g;

    .line 277
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    .line 278
    new-instance v0, Lcom/autonavi/amap/mapcore/MapCore;

    invoke-direct {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    .line 279
    new-instance v0, Lcom/amap/api/mapcore/a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/a;-><init>(Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    .line 280
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0, v1}, Lcom/autonavi/amap/mapcore/MapCore;->setMapCallback(Lcom/autonavi/amap/mapcore/IMapCallback;)V

    .line 282
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 292
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    const-wide v2, 0x4043f64cb5bb3850L    # 39.924216

    const-wide v5, 0x405d1976a004eda6L    # 116.3978653

    invoke-direct {v1, v2, v3, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v5}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 295
    new-instance v0, Lcom/amap/api/mapcore/am;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/am;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/mapcore/x;

    .line 296
    new-instance v0, Lcom/amap/api/mapcore/f;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/f;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/mapcore/f;

    .line 297
    new-instance v0, Lcom/amap/api/mapcore/ap;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ap;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/mapcore/aa;

    .line 306
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$c;

    invoke-direct {v1, p0, v8}, Lcom/amap/api/mapcore/b$c;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->p:Landroid/view/GestureDetector;

    .line 311
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->p:Landroid/view/GestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$b;

    invoke-direct {v1, p0, v8}, Lcom/amap/api/mapcore/b$b;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 312
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->p:Landroid/view/GestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 317
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/amap/api/mapcore/b$e;

    invoke-direct {v1, p0, v8}, Lcom/amap/api/mapcore/b$e;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->q:Landroid/view/ScaleGestureDetector;

    .line 323
    new-instance v0, Lcom/amap/api/mapcore/a/c;

    new-instance v1, Lcom/amap/api/mapcore/b$d;

    invoke-direct {v1, p0, v8}, Lcom/amap/api/mapcore/b$d;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/a/c;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/a/c$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->r:Lcom/amap/api/mapcore/a/c;

    .line 329
    new-instance v0, Lcom/amap/api/mapcore/a/b;

    new-instance v1, Lcom/amap/api/mapcore/b$a;

    invoke-direct {v1, p0, v8}, Lcom/amap/api/mapcore/b$a;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/a/b;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/a/b$a;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->S:Lcom/amap/api/mapcore/a/b;

    .line 334
    new-instance v0, Lcom/amap/api/mapcore/ag;

    invoke-direct {v0, p1, p0}, Lcom/amap/api/mapcore/ag;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    .line 335
    new-instance v0, Lcom/amap/api/mapcore/ar;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ar;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    .line 336
    new-instance v0, Lcom/amap/api/mapcore/an;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/an;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/b;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    .line 337
    new-instance v0, Lcom/amap/api/mapcore/ao;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/mapcore/ao;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    .line 338
    new-instance v0, Lcom/amap/api/mapcore/as;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/as;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ae;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    .line 340
    new-instance v0, Lcom/amap/api/mapcore/ab;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/ab;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ae;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    .line 341
    new-instance v0, Lcom/amap/api/mapcore/k;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-direct {v0, v1, v2, p0}, Lcom/amap/api/mapcore/k;-><init>(Landroid/content/Context;Lcom/amap/api/mapcore/ae;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    .line 342
    new-instance v0, Lcom/amap/api/mapcore/af;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0}, Lcom/amap/api/mapcore/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    .line 344
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 345
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->setBackgroundColor(I)V

    .line 346
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->setBackgroundColor(I)V

    .line 347
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ao;->setBackgroundColor(I)V

    .line 348
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/as;->setBackgroundColor(I)V

    .line 349
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/af;->setBackgroundColor(I)V

    .line 350
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setBackgroundColor(I)V

    .line 352
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 355
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v1, p0, v4, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 357
    new-instance v1, Lcom/amap/api/mapcore/ag$a;

    invoke-direct {v1, v0}, Lcom/amap/api/mapcore/ag$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v2, v3, v1}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 362
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 364
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 366
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    new-instance v0, Lcom/amap/api/mapcore/ag$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v3, v10, v11, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/16 v6, 0x53

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 372
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 374
    new-instance v0, Lcom/amap/api/mapcore/ag$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v3, v10, v11, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/16 v6, 0x53

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 379
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->w()Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setVisibility(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :cond_0
    :goto_0
    new-instance v0, Lcom/amap/api/mapcore/ag$a;

    const/4 v1, -0x2

    const/4 v2, -0x2

    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    invoke-direct {v3, v10, v11, v10, v11}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    const/16 v6, 0x33

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 393
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 396
    new-instance v0, Lcom/amap/api/mapcore/h;

    invoke-direct {v0, p1}, Lcom/amap/api/mapcore/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    .line 398
    new-instance v0, Lcom/amap/api/mapcore/ai;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ai;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    .line 400
    invoke-virtual {p0, p0}, Lcom/amap/api/mapcore/b;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 403
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ZoomControllerViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/as;->setId(I)V

    .line 404
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->ScaleControlsViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->setId(I)V

    .line 405
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->MyLocationViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setId(I)V

    .line 406
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    sget v1, Lcom/amap/api/mapcore/AutoTestConfig;->CompassViewId:I

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setId(I)V

    .line 415
    return-void

    .line 384
    :catch_0
    move-exception v0

    .line 385
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 174
    nop

    :array_0
    .array-data 4
        0x989680
        0x4c4b40
        0x1e8480
        0xf4240
        0x7a120
        0x30d40
        0x186a0
        0xc350
        0x7530
        0x4e20
        0x2710
        0x1388
        0x7d0
        0x3e8
        0x1f4
        0xc8
        0x64
        0x32
        0x19
        0xa
        0x5
    .end array-data
.end method

.method static synthetic A(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method static synthetic B(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLoadedListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    return-object v0
.end method

.method static synthetic C(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/k;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    return-object v0
.end method

.method static synthetic D(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ar;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    return-object v0
.end method

.method static synthetic E(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    return-object v0
.end method

.method static synthetic F(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ag:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object v0
.end method

.method static synthetic G(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ah:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object v0
.end method

.method static synthetic H(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->Z:Z

    return v0
.end method

.method static synthetic I(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ab:Z

    return v0
.end method

.method static synthetic J()D
    .locals 2

    .prologue
    .line 88
    sget-wide v0, Lcom/amap/api/mapcore/b;->aj:D

    return-wide v0
.end method

.method private declared-synchronized K()V
    .locals 6

    .prologue
    .line 1009
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1010
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->L()V

    .line 1012
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 1013
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    .line 1015
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    new-instance v1, Lcom/amap/api/mapcore/b$f;

    invoke-direct {v1, p0, p0}, Lcom/amap/api/mapcore/b$f;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/b;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1016
    monitor-exit p0

    return-void

    .line 1009
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized L()V
    .locals 1

    .prologue
    .line 1019
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1021
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ai:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1023
    :cond_0
    monitor-exit p0

    return-void

    .line 1019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private M()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1031
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aq:Z

    if-nez v0, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "bk.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1036
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1037
    const/16 v1, 0xf0

    if-lt v0, v1, :cond_1

    .line 1038
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v1, "icn_h.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a([B)[B

    move-result-object v0

    .line 1040
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v1, v0, v3}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1047
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "roadarrow.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1050
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "LineRound.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1053
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "tgl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1056
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "trl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1059
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "tyl.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1062
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v2, "dash.pvr"

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    .line 1066
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->aq:Z

    .line 1068
    :cond_0
    return-void

    .line 1042
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v1, "icn.data"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a([B)[B

    move-result-object v0

    .line 1044
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v1, v0, v3}, Lcom/autonavi/amap/mapcore/MapCore;->setInternaltexture([BI)V

    goto :goto_0
.end method

.method private N()Lcom/amap/api/maps/model/LatLng;
    .locals 6

    .prologue
    .line 1878
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 1879
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1880
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1881
    iget v2, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v2, v1, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 1882
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 1883
    return-object v1
.end method

.method private O()Lcom/amap/api/maps/model/CameraPosition;
    .locals 2

    .prologue
    .line 1887
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->N()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 1888
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->bearing(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->tilt(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method private P()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1917
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aC:Z

    if-eqz v0, :cond_0

    .line 1918
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aC:Z

    .line 1919
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->ay:Z

    if-eqz v0, :cond_1

    .line 1920
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->ay:Z

    .line 1921
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 1923
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 1924
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 1926
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->au:Z

    if-eqz v0, :cond_2

    .line 1927
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->au:Z

    .line 1928
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 1930
    iput-boolean v3, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 1931
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 1933
    :cond_2
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->av:Z

    .line 1934
    iput-boolean v2, p0, Lcom/amap/api/mapcore/b;->aw:Z

    .line 1935
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_3

    .line 1936
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDragEnd(Lcom/amap/api/maps/model/Marker;)V

    .line 1937
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    .line 1939
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/amap/api/mapcore/b;->aB:I

    return p1
.end method

.method public static a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 2888
    mul-int v0, p2, p3

    new-array v9, v0, [I

    .line 2889
    mul-int v0, p2, p3

    new-array v10, v0, [I

    .line 2890
    invoke-static {v9}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v7

    .line 2891
    invoke-virtual {v7, v8}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 2892
    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object v0, p4

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    move v1, v8

    .line 2898
    :goto_0
    if-ge v1, p3, :cond_1

    move v0, v8

    .line 2899
    :goto_1
    if-ge v0, p2, :cond_0

    .line 2900
    mul-int v2, v1, p2

    add-int/2addr v2, v0

    aget v2, v9, v2

    .line 2901
    shr-int/lit8 v3, v2, 0x10

    and-int/lit16 v3, v3, 0xff

    .line 2902
    shl-int/lit8 v4, v2, 0x10

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    .line 2903
    const v5, -0xff0100

    and-int/2addr v2, v5

    or-int/2addr v2, v4

    or-int/2addr v2, v3

    .line 2904
    sub-int v3, p3, v1

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p2

    add-int/2addr v3, v0

    aput v2, v10, v3

    .line 2899
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2898
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2910
    :cond_1
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2911
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v10

    move v3, p2

    move v6, p2

    move v7, p3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2916
    :goto_2
    return-object v0

    .line 2912
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2913
    const/4 v0, 0x0

    .line 2914
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps/AMap$OnMapScreenShotListener;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ah:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)Lcom/amap/api/maps/AMap$onMapPrintScreenListener;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ag:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;)Lcom/amap/api/maps/model/LatLngBounds;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->aE:Lcom/amap/api/maps/model/LatLngBounds;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/Marker;)Lcom/amap/api/maps/model/Marker;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    return-object p1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 1942
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 1943
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 1944
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    const/high16 v2, 0x42700000    # 60.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 1945
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 1946
    invoke-virtual {p0, v0, v1, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 1947
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v3, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v1, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 1948
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/model/Marker;->setPosition(Lcom/amap/api/maps/model/LatLng;)V

    .line 1949
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDrag(Lcom/amap/api/maps/model/Marker;)V

    .line 1951
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->an:Z

    return v0
.end method

.method static synthetic a(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->am:Z

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/amap/api/mapcore/b;->W:I

    return p1
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->au:Z

    return v0
.end method

.method static synthetic b(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->an:Z

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/amap/api/mapcore/b;->X:I

    return p1
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->av:Z

    return v0
.end method

.method static synthetic c(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->av:Z

    return p1
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/mapcore/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ay:Z

    return p1
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aA:Z

    return p1
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->P()V

    return-void
.end method

.method static synthetic f(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aC:Z

    return p1
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aA:Z

    return v0
.end method

.method static synthetic g(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aw:Z

    return p1
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    return-object v0
.end method

.method static synthetic h(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->az:Z

    return p1
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    return-object v0
.end method

.method static synthetic i(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->au:Z

    return p1
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->S:Lcom/amap/api/mapcore/a/b;

    return-object v0
.end method

.method static synthetic j(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->aa:Z

    return p1
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/amap/api/mapcore/b;->W:I

    return v0
.end method

.method static synthetic k(Lcom/amap/api/mapcore/b;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->ab:Z

    return p1
.end method

.method static synthetic l(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/amap/api/mapcore/b;->X:I

    return v0
.end method

.method static synthetic m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ao;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    return-object v0
.end method

.method static synthetic n(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLongClickListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    return-object v0
.end method

.method static synthetic o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    return-object v0
.end method

.method static synthetic p(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerDragListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    return-object v0
.end method

.method static synthetic q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ax:Lcom/amap/api/maps/model/Marker;

    return-object v0
.end method

.method static synthetic r(Lcom/amap/api/mapcore/b;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/amap/api/mapcore/b;->aB:I

    return v0
.end method

.method static synthetic s(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aC:Z

    return v0
.end method

.method static synthetic t(Lcom/amap/api/mapcore/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    return-object v0
.end method

.method static synthetic u(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    return-object v0
.end method

.method static synthetic v(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerClickListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    return-object v0
.end method

.method static synthetic w(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapClickListener;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMapClickListener;

    return-object v0
.end method

.method static synthetic x(Lcom/amap/api/mapcore/b;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->az:Z

    return v0
.end method

.method static synthetic y(Lcom/amap/api/mapcore/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic z(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/g;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->U:Lcom/amap/api/mapcore/g;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2394
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2395
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2396
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->removeView(Landroid/view/View;)V

    .line 2397
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2398
    if-eqz v0, :cond_0

    .line 2399
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2402
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    .line 2404
    :cond_1
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    .line 2405
    return-void
.end method

.method public B()F
    .locals 1

    .prologue
    .line 2628
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    return v0
.end method

.method C()V
    .locals 2

    .prologue
    .line 2652
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2653
    return-void
.end method

.method public D()Lcom/amap/api/maps/model/LatLngBounds;
    .locals 1

    .prologue
    .line 2657
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aE:Lcom/amap/api/maps/model/LatLngBounds;

    return-object v0
.end method

.method E()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2869
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    if-nez v0, :cond_0

    .line 2870
    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->setBackgroundColor(I)V

    .line 2871
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ar;->setBackgroundColor(I)V

    .line 2872
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->setBackgroundColor(I)V

    .line 2873
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ag;->setBackgroundColor(I)V

    .line 2874
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ao;->setBackgroundColor(I)V

    .line 2875
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/as;->setBackgroundColor(I)V

    .line 2876
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/af;->setBackgroundColor(I)V

    .line 2877
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setBackgroundColor(I)V

    .line 2878
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->aF:Z

    .line 2879
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->postInvalidate()V

    .line 2881
    :cond_0
    return-void
.end method

.method F()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 2884
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->c()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public G()F
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2972
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 2973
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2974
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2975
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2976
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2977
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/b/h;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v1

    .line 2980
    int-to-double v3, v0

    div-double v0, v1, v3

    double-to-float v0, v0

    return v0
.end method

.method public H()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3021
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 3022
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3023
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3024
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3025
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3027
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public I()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3032
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "\u5730\u56fe\u672a\u521d\u59cb\u5316\u5b8c\u6210\uff01"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 3034
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->f()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3032
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/r;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1531
    new-instance v0, Lcom/amap/api/mapcore/j;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/j;-><init>(Lcom/amap/api/mapcore/q;)V

    .line 1532
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(I)V

    .line 1533
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getCenter()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 1534
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(Z)V

    .line 1535
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->b(F)V

    .line 1536
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(F)V

    .line 1537
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/j;->a(I)V

    .line 1538
    invoke-virtual {p1}, Lcom/amap/api/maps/model/CircleOptions;->getRadius()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/mapcore/j;->a(D)V

    .line 1539
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/u;)V

    .line 1540
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1541
    return-object v0
.end method

.method public a(Lcom/amap/api/maps/model/PolygonOptions;)Lcom/amap/api/mapcore/v;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1516
    new-instance v0, Lcom/amap/api/mapcore/ak;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ak;-><init>(Lcom/amap/api/mapcore/q;)V

    .line 1517
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->a(I)V

    .line 1518
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->a(Ljava/util/List;)V

    .line 1519
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->a(Z)V

    .line 1520
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->b(F)V

    .line 1521
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->a(F)V

    .line 1522
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolygonOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ak;->b(I)V

    .line 1523
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/u;)V

    .line 1524
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1525
    return-object v0
.end method

.method public a(Lcom/amap/api/maps/model/PolylineOptions;)Lcom/amap/api/mapcore/w;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1499
    new-instance v0, Lcom/amap/api/mapcore/al;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/al;-><init>(Lcom/amap/api/mapcore/q;)V

    .line 1500
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->a(I)V

    .line 1501
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isGeodesic()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->b(Z)V

    .line 1503
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->a(Ljava/util/List;)V

    .line 1504
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->a(Z)V

    .line 1505
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->b(F)V

    .line 1506
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->a(F)V

    .line 1507
    invoke-virtual {p1}, Lcom/amap/api/maps/model/PolylineOptions;->isUseTexture()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/al;->c(Z)V

    .line 1508
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/p;->a(Lcom/amap/api/mapcore/u;)V

    .line 1509
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1510
    return-object v0
.end method

.method a()Lcom/amap/api/maps/CustomRenderer;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    return-object v0
.end method

.method public a(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1546
    new-instance v0, Lcom/amap/api/mapcore/ah;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/ah;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/af;)V

    .line 1548
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/af;->a(Lcom/amap/api/mapcore/t;)V

    .line 1549
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1550
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    return-object v1
.end method

.method public a(DDLcom/autonavi/amap/mapcore/FPoint;)V
    .locals 3

    .prologue
    .line 1823
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1825
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1826
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v1, v2, v0, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 1827
    return-void
.end method

.method public a(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 0

    .prologue
    .line 1832
    invoke-static {p3, p4, p1, p2, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1833
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 581
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 582
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ai;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ai;->a(F)V

    .line 584
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x7db

    const/4 v0, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1620
    if-ne p1, v0, :cond_0

    .line 1621
    iput v0, p0, Lcom/amap/api/mapcore/b;->k:I

    .line 1622
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    new-instance v1, Lcom/amap/api/mapcore/ad;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/ad;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/ad;->a(Z)Lcom/amap/api/mapcore/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/ad;)V

    .line 1625
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ar;->a(Z)V

    .line 1633
    :goto_0
    return-void

    .line 1627
    :cond_0
    iput v2, p0, Lcom/amap/api/mapcore/b;->k:I

    .line 1628
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    new-instance v1, Lcom/amap/api/mapcore/ad;

    invoke-direct {v1, v4}, Lcom/amap/api/mapcore/ad;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/amap/api/mapcore/ad;->a(Z)Lcom/amap/api/mapcore/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/ad;)V

    .line 1631
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, v3}, Lcom/amap/api/mapcore/ar;->a(Z)V

    goto :goto_0
.end method

.method public a(IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 996
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x899

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1001
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v1, 0x89a

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1006
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 4

    .prologue
    .line 1806
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 1808
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 1809
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1810
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1811
    iget v0, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-static {v0, v1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 1812
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/FPoint;)V
    .locals 1

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0, p2, p1, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 1845
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore/IPoint;)V
    .locals 3

    .prologue
    .line 1816
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 1817
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v1, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 1818
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v1, v2, v0, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1819
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 593
    if-nez p1, :cond_0

    .line 629
    :goto_0
    return-void

    .line 596
    :cond_0
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 599
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    if-nez v0, :cond_5

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->a()V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 616
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/amap/api/mapcore/ai;->a(Lcom/amap/api/maps/model/LatLng;D)V

    .line 618
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    if-eqz v0, :cond_4

    .line 619
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_4

    .line 624
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    invoke-interface {v0, p1}, Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;->onMyLocationChange(Landroid/location/Location;)V

    .line 627
    :cond_4
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    .line 628
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_0

    .line 604
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 605
    :cond_6
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    if-nez v0, :cond_7

    .line 606
    new-instance v0, Lcom/amap/api/mapcore/ai;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ai;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    .line 608
    :cond_7
    if-eqz v1, :cond_2

    .line 609
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v0

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public a(Lcom/amap/api/mapcore/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1181
    iget-object v0, p1, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v2, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-ne v0, v2, :cond_0

    .line 1182
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string/jumbo v2, "the map must have a size"

    invoke-static {v0, v2}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 1185
    :cond_0
    iput-boolean v1, p1, Lcom/amap/api/mapcore/i;->o:Z

    .line 1186
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 1187
    return-void

    .line 1182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1209
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_0

    .line 1210
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    const-string/jumbo v3, "the map must have a size"

    invoke-static {v2, v3}, Lcom/amap/api/mapcore/b/a;->a(ZLjava/lang/Object;)V

    .line 1213
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    invoke-virtual {v2}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v2, :cond_1

    .line 1216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v2}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1218
    :cond_1
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1219
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/mapcore/b;->aa:Z

    if-eqz v2, :cond_2

    .line 1220
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->ab:Z

    .line 1222
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->Z:Z

    .line 1223
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->j:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_5

    .line 1224
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->c:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    .line 1226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 1480
    :goto_1
    return-void

    .line 1210
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1229
    :cond_4
    new-instance v9, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v9}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1230
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v9}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1231
    new-instance v10, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v10}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1232
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->b:F

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/mapcore/i;->c:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v10}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v9, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v9, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    iget v8, v10, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v11, v9, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v8, v11

    iget v10, v10, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v9, v9, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v9, v10, v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    .line 1479
    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    goto :goto_1

    .line 1241
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->b:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_7

    .line 1242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 1243
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, v5

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    sub-float v10, v2, v5

    .line 1244
    const/4 v2, 0x0

    cmpl-float v2, v10, v2

    if-nez v2, :cond_6

    .line 1245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1248
    :cond_6
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto :goto_2

    .line 1254
    :cond_7
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->g:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_9

    .line 1255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 1256
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v5, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    sub-float v10, v2, v5

    .line 1257
    const/4 v2, 0x0

    cmpl-float v2, v10, v2

    if-nez v2, :cond_8

    .line 1258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1261
    :cond_8
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1267
    :cond_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->h:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_b

    .line 1268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 1269
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->d:F

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    sub-float v10, v2, v5

    .line 1271
    const/4 v2, 0x0

    cmpl-float v2, v10, v2

    if-nez v2, :cond_a

    .line 1272
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1275
    :cond_a
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1281
    :cond_b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->i:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_e

    .line 1282
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->e:F

    .line 1283
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 1284
    add-float v3, v5, v2

    invoke-static {v3}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v3

    sub-float v10, v3, v5

    .line 1285
    const/4 v3, 0x0

    cmpl-float v3, v10, v3

    if-nez v3, :cond_c

    .line 1286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1289
    :cond_c
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 1290
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1291
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1292
    const/4 v8, 0x0

    .line 1293
    const/4 v9, 0x0

    .line 1294
    if-eqz v3, :cond_d

    .line 1295
    new-instance v6, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1296
    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v3, v6}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/IPoint;)V

    .line 1297
    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v6, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v3, v7

    .line 1298
    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, v6, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v6, v7, v6

    .line 1299
    int-to-double v7, v3

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    float-to-double v13, v2

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    div-double/2addr v7, v11

    int-to-double v11, v3

    sub-double/2addr v7, v11

    double-to-int v8, v7

    .line 1300
    int-to-double v11, v6

    const-wide/high16 v13, 0x4000000000000000L    # 2.0

    float-to-double v2, v2

    invoke-static {v13, v14, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    div-double v2, v11, v2

    int-to-double v6, v6

    sub-double/2addr v2, v6

    double-to-int v9, v2

    .line 1302
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1303
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1306
    :cond_e
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->k:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_11

    .line 1307
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1309
    new-instance v2, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1310
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    .line 1311
    iget-object v5, v3, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v5, v5, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v7, v3, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v7, v7, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v5, v6, v7, v8, v2}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1313
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    .line 1314
    iget v6, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v7, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v8, v6, v7

    .line 1315
    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v6, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v9, v2, v6

    .line 1316
    iget v2, v3, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    sub-float v10, v2, v5

    .line 1317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    .line 1318
    iget v2, v3, Lcom/amap/api/maps/model/CameraPosition;->bearing:F

    const/high16 v7, 0x43b40000    # 360.0f

    rem-float/2addr v2, v7

    const/high16 v7, 0x43b40000    # 360.0f

    rem-float v7, v6, v7

    sub-float v11, v2, v7

    .line 1319
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v7, 0x43340000    # 180.0f

    cmpl-float v2, v2, v7

    if-ltz v2, :cond_f

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v7, 0x43b40000    # 360.0f

    mul-float/2addr v2, v7

    sub-float/2addr v11, v2

    .line 1321
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    .line 1322
    iget v2, v3, Lcom/amap/api/maps/model/CameraPosition;->tilt:F

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->a(F)F

    move-result v2

    sub-float v12, v2, v7

    .line 1323
    if-nez v8, :cond_10

    if-nez v9, :cond_10

    const/4 v2, 0x0

    cmpl-float v2, v10, v2

    if-nez v2, :cond_10

    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_10

    const/4 v2, 0x0

    cmpl-float v2, v12, v2

    if-nez v2, :cond_10

    .line 1325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1328
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1331
    :cond_11
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->e:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_14

    .line 1332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    .line 1333
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->g:F

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v3, v6, v3

    sub-float v11, v2, v3

    .line 1335
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v11, v2

    .line 1337
    :cond_12
    const/4 v2, 0x0

    cmpl-float v2, v11, v2

    if-nez v2, :cond_13

    .line 1338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1341
    :cond_13
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1347
    :cond_14
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->d:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_16

    .line 1348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    .line 1349
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->f:F

    sub-float v12, v2, v7

    .line 1350
    const/4 v2, 0x0

    cmpl-float v2, v12, v2

    if-nez v2, :cond_15

    .line 1351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1354
    :cond_15
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1360
    :cond_16
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->c:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_18

    .line 1361
    new-instance v4, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1363
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v8, v2, v3

    .line 1364
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v9, v2, v3

    .line 1365
    if-nez v8, :cond_17

    if-nez v9, :cond_17

    .line 1366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1369
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    iget v3, v4, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v4, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v5}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v7}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1374
    :cond_18
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-eq v2, v3, :cond_19

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_22

    .line 1378
    :cond_19
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    sget-object v3, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    if-ne v2, v3, :cond_1b

    .line 1379
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v3

    .line 1380
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v2

    move v14, v2

    move v15, v3

    .line 1385
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float v17, v2, v3

    .line 1386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v18

    .line 1387
    const/4 v2, 0x0

    cmpl-float v2, v17, v2

    if-nez v2, :cond_1a

    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-eqz v2, :cond_1c

    :cond_1a
    const/4 v2, 0x1

    .line 1388
    :goto_4
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 1389
    move-object/from16 v0, p1

    iget v0, v0, Lcom/amap/api/mapcore/i;->j:I

    move/from16 v19, v0

    .line 1390
    new-instance v20, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v20 .. v20}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1391
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->getGeoCenter(Lcom/autonavi/amap/mapcore/IPoint;)V

    .line 1392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v16

    .line 1393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1394
    if-nez v2, :cond_20

    .line 1395
    new-instance v21, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct/range {v21 .. v21}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1396
    iget-object v2, v8, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-object v4, v8, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->longitude:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    iget-object v4, v8, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v4, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v6, v8, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v6, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, v21

    invoke-static {v2, v3, v4, v5, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1401
    new-instance v7, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1402
    new-instance v13, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v13}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1403
    iget-object v2, v8, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v3, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v8, Lcom/amap/api/maps/model/LatLngBounds;->northeast:Lcom/amap/api/maps/model/LatLng;

    iget-wide v5, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1405
    iget-object v2, v8, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v9, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v2, v8, Lcom/amap/api/maps/model/LatLngBounds;->southwest:Lcom/amap/api/maps/model/LatLng;

    iget-wide v11, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    move-object/from16 v8, p0

    invoke-virtual/range {v8 .. v13}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1408
    iget v2, v7, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v13, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int/2addr v2, v3

    .line 1409
    iget v3, v13, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v4, v7, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int/2addr v3, v4

    .line 1411
    if-nez v2, :cond_1d

    if-nez v3, :cond_1d

    .line 1412
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1382
    :cond_1b
    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 1383
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/mapcore/i;->l:I

    move v14, v2

    move v15, v3

    goto/16 :goto_3

    .line 1387
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1416
    :cond_1d
    int-to-float v2, v2

    mul-int/lit8 v4, v19, 0x2

    sub-int v4, v15, v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    .line 1418
    int-to-float v3, v3

    mul-int/lit8 v4, v19, 0x2

    sub-int v4, v14, v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 1421
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1423
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1e

    .line 1424
    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->c(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v16, v2

    .line 1426
    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    .line 1432
    :goto_5
    move-object/from16 v0, v21

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    sub-int v8, v3, v4

    .line 1433
    move-object/from16 v0, v21

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    sub-int v9, v3, v4

    .line 1434
    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    sub-float v10, v2, v16

    .line 1435
    if-nez v8, :cond_1f

    if-nez v9, :cond_1f

    const/4 v2, 0x0

    cmpl-float v2, v10, v2

    if-nez v2, :cond_1f

    .line 1436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 1427
    :cond_1e
    float-to-double v3, v2

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v3, v5

    if-gez v3, :cond_23

    .line 1428
    const/high16 v3, 0x3f800000    # 1.0f

    div-float v2, v3, v2

    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->c(F)I

    move-result v2

    int-to-float v2, v2

    add-float v2, v2, v16

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    .line 1430
    invoke-static {v2}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v2

    goto :goto_5

    .line 1439
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/mapcore/h;->a(Landroid/view/animation/Interpolator;)V

    .line 1440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v20

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    move/from16 v5, v16

    move/from16 v6, v17

    move/from16 v7, v18

    move-wide/from16 v13, p2

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1443
    :cond_20
    move/from16 v0, v17

    neg-float v11, v0

    .line 1444
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_21

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    sub-float/2addr v11, v2

    .line 1446
    :cond_21
    move/from16 v0, v18

    neg-float v12, v0

    .line 1448
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1449
    new-instance v2, Lcom/amap/api/mapcore/b$2;

    move-object/from16 v3, p0

    move-object v4, v8

    move v5, v15

    move v6, v14

    move/from16 v7, v19

    move-wide/from16 v8, p2

    invoke-direct/range {v2 .. v10}, Lcom/amap/api/mapcore/b$2;-><init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/LatLngBounds;IIIJLcom/amap/api/maps/AMap$CancelableCallback;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1470
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/mapcore/b;->Z:Z

    .line 1471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    move-object/from16 v0, v20

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    move-object/from16 v0, v20

    iget v4, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v13, 0xfa

    move/from16 v5, v16

    move/from16 v6, v17

    move/from16 v7, v18

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/mapcore/h;->a(IIFFFIIFFFJ)V

    goto/16 :goto_2

    .line 1476
    :cond_22
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 1477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_2

    :cond_23
    move/from16 v2, v16

    goto/16 :goto_5
.end method

.method public a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1200
    const-wide/16 v0, 0xfa

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;JLcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1202
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/o;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->al:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Lcom/amap/api/mapcore/o;)Z

    .line 426
    return-void
.end method

.method public a(Lcom/amap/api/mapcore/t;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x2

    const/high16 v5, -0x1000000

    .line 2293
    .line 2294
    if-nez p1, :cond_1

    .line 2376
    :cond_0
    :goto_0
    return-void

    .line 2297
    :cond_1
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2301
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    if-eqz v0, :cond_0

    .line 2302
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 2303
    new-instance v7, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v7, p1}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    .line 2304
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v7}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoWindow(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    .line 2307
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 2308
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v1, "infowindow_bg.9.png"

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/aj;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2316
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    if-nez v0, :cond_4

    .line 2317
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    invoke-interface {v0, v7}, Lcom/amap/api/maps/AMap$InfoWindowAdapter;->getInfoContents(Lcom/amap/api/maps/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    .line 2319
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 2320
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2328
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2358
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2359
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 2360
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setDrawingCacheQuality(I)V

    .line 2361
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->f()Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v5

    .line 2364
    if-eqz v0, :cond_7

    .line 2365
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2366
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2368
    :goto_3
    new-instance v0, Lcom/amap/api/mapcore/ag$a;

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    iget v4, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    float-to-int v4, v4

    neg-int v4, v4

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->q()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    iget v5, v5, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    float-to-int v5, v5

    neg-int v5, v5

    add-int/lit8 v5, v5, 0x2

    const/16 v6, 0x51

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/mapcore/ag$a;-><init>(IILcom/amap/api/maps/model/LatLng;III)V

    .line 2373
    iput-object v7, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    .line 2374
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    iget-object v2, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/mapcore/ag;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2312
    :catch_0
    move-exception v0

    .line 2313
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2332
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2334
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2335
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2336
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2337
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2338
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2339
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2340
    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2341
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2342
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2343
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2345
    iput-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public a(Lcom/amap/api/maps/AMap$InfoWindowAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1785
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->I:Lcom/amap/api/maps/AMap$InfoWindowAdapter;

    .line 1786
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnCameraChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1729
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    .line 1730
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1779
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->H:Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    .line 1780
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1749
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->F:Lcom/amap/api/maps/AMap$OnMapClickListener;

    .line 1750
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLoadedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1773
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->D:Lcom/amap/api/maps/AMap$OnMapLoadedListener;

    .line 1774
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1755
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->G:Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    .line 1756
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 2946
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ah:Lcom/amap/api/maps/AMap$OnMapScreenShotListener;

    .line 2947
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->af:Z

    .line 2948
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1761
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->B:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    .line 1762
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMarkerDragListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1767
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->C:Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    .line 1768
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->A:Lcom/amap/api/maps/AMap$OnMyLocationChangeListener;

    .line 536
    return-void
.end method

.method public a(Lcom/amap/api/maps/AMap$onMapPrintScreenListener;)V
    .locals 1

    .prologue
    .line 2940
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ag:Lcom/amap/api/maps/AMap$onMapPrintScreenListener;

    .line 2941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->af:Z

    .line 2942
    return-void
.end method

.method public a(Lcom/amap/api/maps/CustomRenderer;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    .line 194
    return-void
.end method

.method public a(Lcom/amap/api/maps/LocationSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1708
    iput-object p1, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    .line 1709
    if-eqz p1, :cond_0

    .line 1710
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->a(Z)V

    .line 1714
    :goto_0
    return-void

    .line 1712
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->a(Z)V

    goto :goto_0
.end method

.method a(Lcom/amap/api/maps/model/CameraPosition;)V
    .locals 2

    .prologue
    .line 1734
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1735
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1736
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1737
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1738
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 574
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 575
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->e()Lcom/amap/api/mapcore/ai;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ai;->a(Lcom/amap/api/maps/model/MyLocationStyle;)V

    .line 577
    :cond_0
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/16 v2, 0x1f4

    const/4 v0, 0x0

    .line 218
    iget-boolean v1, p0, Lcom/amap/api/mapcore/b;->h:Z

    if-eqz v1, :cond_0

    .line 226
    :goto_0
    return-void

    .line 220
    :cond_0
    new-array v1, v2, [I

    .line 221
    invoke-interface {p1, v2, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    .line 222
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 223
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    aget v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 225
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->h:Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 633
    if-eqz p1, :cond_0

    .line 634
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/as;->setVisibility(I)V

    .line 638
    :goto_0
    return-void

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/as;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(ZLcom/amap/api/maps/model/CameraPosition;)V
    .locals 1

    .prologue
    .line 2990
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    if-nez v0, :cond_1

    .line 3010
    :cond_0
    :goto_0
    return-void

    .line 2993
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2996
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2999
    if-nez p2, :cond_2

    .line 3001
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->n()Lcom/amap/api/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 3006
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    invoke-interface {v0, p2}, Lcom/amap/api/maps/AMap$OnCameraChangeListener;->onCameraChangeFinish(Lcom/amap/api/maps/model/CameraPosition;)V

    goto :goto_0

    .line 3002
    :catch_0
    move-exception v0

    .line 3003
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 774
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 775
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/p;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(F)F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1795
    invoke-static {p1}, Lcom/amap/api/mapcore/b/h;->b(F)F

    move-result v0

    return v0
.end method

.method public b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1556
    new-instance v0, Lcom/amap/api/mapcore/ah;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/mapcore/ah;-><init>(Lcom/amap/api/maps/model/MarkerOptions;Lcom/amap/api/mapcore/af;)V

    .line 1558
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/af;->a(Lcom/amap/api/mapcore/t;)V

    .line 1559
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1560
    return-object v0
.end method

.method public b()Lcom/autonavi/amap/mapcore/MapCore;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    return-object v0
.end method

.method public b(DDLcom/autonavi/amap/mapcore/IPoint;)V
    .locals 4

    .prologue
    .line 1861
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 1862
    new-instance v1, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 1864
    invoke-static {p3, p4, p1, p2, v0}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1865
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v2, v3, v0, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 1866
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    iget v2, v1, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v1, v1, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    invoke-virtual {v0, v2, v1, p5}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Win(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 1869
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 2961
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    if-eqz v0, :cond_0

    .line 2962
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ar;->a(I)V

    .line 2963
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->invalidate()V

    .line 2964
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/an;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2965
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/an;->invalidate()V

    .line 2968
    :cond_0
    return-void
.end method

.method public b(IILcom/autonavi/amap/mapcore/DPoint;)V
    .locals 0

    .prologue
    .line 1856
    invoke-static {p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2LonLat(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 1857
    return-void
.end method

.method public b(Lcom/amap/api/mapcore/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1193
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;Lcom/amap/api/maps/AMap$CancelableCallback;)V

    .line 1194
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 694
    if-eqz p1, :cond_0

    .line 695
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setVisibility(I)V

    .line 699
    :goto_0
    return-void

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Lcom/amap/api/mapcore/t;)Z
    .locals 2

    .prologue
    .line 2386
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2387
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v0}, Lcom/amap/api/maps/model/Marker;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2389
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1597
    const/4 v2, 0x0

    .line 1599
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/af;->a(Ljava/lang/String;)Lcom/amap/api/mapcore/t;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1603
    :goto_0
    if-eqz v1, :cond_0

    .line 1604
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1605
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/af;->b(Lcom/amap/api/mapcore/t;)Z

    move-result v0

    .line 1607
    :cond_0
    return v0

    .line 1600
    :catch_0
    move-exception v1

    .line 1601
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v2

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/amap/api/mapcore/b;->f:I

    return v0
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 3013
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3014
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 3015
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->j:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    .line 3017
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 703
    if-eqz p1, :cond_0

    .line 707
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    .line 708
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->b()V

    .line 713
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/k;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Lcom/autonavi/amap/mapcore/MapProjection;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapCore;->getMapstate()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    return-object v0
.end method

.method public d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 732
    if-eqz p1, :cond_0

    .line 733
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/an;->setVisibility(I)V

    .line 734
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->h()V

    .line 740
    :goto_0
    return-void

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Ljava/lang/String;)V

    .line 737
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/an;->a(I)V

    .line 738
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()Lcom/amap/api/mapcore/ai;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    return-object v0
.end method

.method public declared-synchronized e(Z)V
    .locals 4

    .prologue
    .line 806
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 807
    :try_start_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->an:Z

    if-nez v0, :cond_0

    .line 808
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->an:Z

    .line 809
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 814
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->an:Z

    .line 815
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ao:Landroid/os/Handler;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ap:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 816
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 645
    :try_start_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->L()V

    .line 646
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->b:Lcom/amap/api/mapcore/as;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/as;->a()V

    .line 647
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/an;->a()V

    .line 648
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->v:Lcom/amap/api/mapcore/ar;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ar;->a()V

    .line 649
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ab;->a()V

    .line 650
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->x:Lcom/amap/api/mapcore/k;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/k;->a()V

    .line 651
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ao;->a()V

    .line 652
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->b()V

    .line 653
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->e()V

    .line 654
    sget-object v0, Lcom/amap/api/mapcore/g;->d:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 655
    sget-object v0, Lcom/amap/api/mapcore/g;->d:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aD:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 658
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aD:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_2

    .line 661
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/a;->OnMapDestory(Lcom/autonavi/amap/mapcore/MapCore;)V

    .line 663
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 664
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ac:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ag;->removeAllViews()V

    .line 668
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :goto_0
    return-void

    .line 687
    :catch_0
    move-exception v0

    .line 688
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1650
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->at:Z

    .line 1651
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    new-instance v1, Lcom/amap/api/mapcore/ad;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/amap/api/mapcore/ad;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/amap/api/mapcore/ad;->a(Z)Lcom/amap/api/mapcore/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/ad;)V

    .line 1653
    return-void
.end method

.method g()V
    .locals 2

    .prologue
    .line 716
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 717
    return-void
.end method

.method public g(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1672
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_4

    .line 1673
    if-eqz p1, :cond_2

    .line 1674
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    iget-object v1, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/mapcore/f;

    invoke-interface {v0, v1}, Lcom/amap/api/maps/LocationSource;->activate(Lcom/amap/api/maps/LocationSource$OnLocationChangedListener;)V

    .line 1675
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ab;->a(Z)V

    .line 1676
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    if-nez v0, :cond_0

    .line 1677
    new-instance v0, Lcom/amap/api/mapcore/ai;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ai;-><init>(Lcom/amap/api/mapcore/q;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    .line 1690
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    .line 1691
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/mapcore/aa;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/aa;->d(Z)V

    .line 1693
    :cond_1
    iput-boolean p1, p0, Lcom/amap/api/mapcore/b;->P:Z

    .line 1694
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1695
    return-void

    .line 1680
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    if-eqz v0, :cond_3

    .line 1681
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ai;->a()V

    .line 1682
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->T:Lcom/amap/api/mapcore/ai;

    .line 1684
    :cond_3
    iput-object v1, p0, Lcom/amap/api/mapcore/b;->ad:Landroid/location/Location;

    .line 1685
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    invoke-interface {v0}, Lcom/amap/api/maps/LocationSource;->deactivate()V

    goto :goto_0

    .line 1688
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->w:Lcom/amap/api/mapcore/ab;

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/ab;->a(Z)V

    goto :goto_0
.end method

.method h()V
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 744
    return-void
.end method

.method h(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2984
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v3, 0x14

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2986
    return-void

    :cond_0
    move v0, v1

    .line 2984
    goto :goto_0
.end method

.method i()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 747
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    if-nez v0, :cond_0

    .line 764
    :goto_0
    return-void

    .line 750
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    .line 751
    new-instance v1, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 752
    new-instance v2, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 753
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 754
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 755
    new-instance v3, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/mapcore/b/h;->a(Lcom/amap/api/maps/model/LatLng;Lcom/amap/api/maps/model/LatLng;)D

    move-result-wide v1

    .line 758
    iget-object v3, p0, Lcom/amap/api/mapcore/b;->o:Lcom/autonavi/amap/mapcore/MapProjection;

    invoke-virtual {v3}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapZoomer()F

    move-result v3

    float-to-int v3, v3

    .line 759
    iget-object v4, p0, Lcom/amap/api/mapcore/b;->ae:[I

    aget v4, v4, v3

    mul-int/2addr v0, v4

    int-to-double v4, v0

    div-double v0, v4, v1

    double-to-int v0, v0

    .line 760
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->ae:[I

    aget v1, v1, v3

    invoke-static {v1}, Lcom/amap/api/mapcore/b/h;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 761
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v2, v0}, Lcom/amap/api/mapcore/an;->a(I)V

    .line 762
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/an;->a(Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->y:Lcom/amap/api/mapcore/an;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/an;->invalidate()V

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->O:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 1138
    iget v0, p0, Lcom/amap/api/mapcore/b;->g:I

    return v0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    if-eqz v0, :cond_1

    .line 1147
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/ag$a;

    .line 1149
    if-eqz v0, :cond_0

    .line 1150
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->K:Lcom/amap/api/maps/model/Marker;

    invoke-virtual {v1}, Lcom/amap/api/maps/model/Marker;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iput-object v1, v0, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 1152
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ag;->a()V

    .line 1153
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1155
    :cond_1
    return-void
.end method

.method public n()Lcom/amap/api/maps/model/CameraPosition;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1164
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->O()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public o()F
    .locals 1

    .prologue
    .line 1169
    const/high16 v0, 0x41a00000    # 20.0f

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 855
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->af:Z

    if-eqz v0, :cond_0

    .line 856
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->getHeight()I

    move-result v1

    invoke-static {v4, v4, v0, v1, p1}, Lcom/amap/api/mapcore/b;->a(IIIILjavax/microedition/khronos/opengles/GL10;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 857
    iget-object v1, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 858
    iput-boolean v4, p0, Lcom/amap/api/mapcore/b;->af:Z

    .line 860
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->am:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->an:Z

    if-nez v0, :cond_2

    .line 906
    :cond_1
    :goto_0
    return-void

    .line 886
    :cond_2
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-interface {p1, v3, v3, v3, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 887
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 888
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->drawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 889
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/ao;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 890
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/p;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 891
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 892
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/af;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 893
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->al:Lcom/amap/api/mapcore/m;

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/m;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 896
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 897
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 899
    :cond_3
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->Q:Z

    if-nez v0, :cond_4

    .line 900
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 901
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->Q:Z

    .line 903
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_1

    .line 904
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1}, Lcom/amap/api/maps/CustomRenderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 560
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->L()V

    .line 564
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onPause()V

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ao;->b()V

    .line 570
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 540
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->K()V

    .line 541
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->n:Lcom/amap/api/mapcore/a;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a;->onResume()V

    .line 543
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ao;->c()V

    .line 556
    :cond_1
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/16 v2, 0x64

    const/4 v4, 0x1

    const/16 v1, 0x803

    const/4 v5, 0x0

    .line 928
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v5, v5, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->O:Landroid/graphics/Rect;

    .line 929
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 930
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1, p2, p3}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceChange(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 931
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 932
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->M()V

    .line 941
    const/16 v3, 0x78

    if-gt v0, v3, :cond_1

    .line 942
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x32

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 971
    :goto_0
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3fd

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 972
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v7, 0x3fe

    move v8, v5

    move v9, v5

    move v10, v5

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 973
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x3ff

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 980
    invoke-virtual {p0, v5}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 983
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->e:Landroid/os/Handler;

    new-instance v1, Lcom/amap/api/mapcore/b$1;

    invoke-direct {v1, p0}, Lcom/amap/api/mapcore/b$1;-><init>(Lcom/amap/api/mapcore/b;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 989
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_0

    .line 990
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2, p3}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 992
    :cond_0
    return-void

    .line 943
    :cond_1
    const/16 v3, 0xa0

    if-gt v0, v3, :cond_3

    .line 944
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x1e0

    if-gt v0, v3, :cond_2

    .line 945
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v2, 0x78

    const/16 v3, 0x3c

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 948
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 952
    :cond_3
    const/16 v3, 0xd7

    if-gt v0, v3, :cond_4

    .line 953
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v3, 0x50

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 954
    :cond_4
    const/16 v3, 0xf0

    if-gt v0, v3, :cond_5

    .line 955
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v9, 0x5a

    move v7, v1

    move v8, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 956
    :cond_5
    const/16 v3, 0x140

    if-gt v0, v3, :cond_8

    .line 957
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v3, 0x500

    if-gt v0, v3, :cond_6

    .line 958
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x5a

    move v7, v1

    move v9, v2

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 959
    :cond_6
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/16 v2, 0xa00

    if-lt v0, v2, :cond_7

    .line 960
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    const/16 v9, 0xb4

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 962
    :cond_7
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 964
    :cond_8
    const/16 v2, 0x1e0

    if-gt v0, v2, :cond_9

    .line 965
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x46

    const/16 v9, 0x96

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0

    .line 967
    :cond_9
    iget-object v6, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    const/16 v8, 0x3c

    const/16 v9, 0xb4

    const/4 v10, 0x3

    move v7, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto/16 :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 10

    .prologue
    const/16 v1, 0x3e9

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 1079
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->setRenderMode(I)V

    .line 1080
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->K()V

    .line 1081
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->setGL(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1083
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v4, "style_v3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1085
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    iget-object v3, p0, Lcom/amap/api/mapcore/b;->m:Landroid/content/Context;

    const-string/jumbo v4, "style_sv3.data"

    invoke-static {v3, v4}, Lcom/amap/api/mapcore/b/h;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v3, v9}, Lcom/autonavi/amap/mapcore/MapCore;->setStyleData([BI)V

    .line 1088
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/MapCore;->surfaceCreate(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1089
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1090
    if-eqz v0, :cond_1

    .line 1091
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1092
    const-string/jumbo v3, "adreno (tm)"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-gt v3, v4, :cond_0

    const-string/jumbo v3, "adreno"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    .line 1100
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1101
    :cond_2
    const-string/jumbo v0, "lineTexture.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    .line 1103
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->as:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/amap/api/mapcore/b;->as:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1104
    :cond_4
    const-string/jumbo v0, "lineDashTexture.png"

    invoke-static {v0}, Lcom/amap/api/mapcore/b/h;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->as:Landroid/graphics/Bitmap;

    .line 1110
    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/h;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->f:I

    .line 1111
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->as:Landroid/graphics/Bitmap;

    invoke-static {p1, v0, v9}, Lcom/amap/api/mapcore/b/h;->a(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/Bitmap;Z)I

    move-result v0

    iput v0, p0, Lcom/amap/api/mapcore/b;->g:I

    .line 1113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/b;->ar:Landroid/graphics/Bitmap;

    .line 1122
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->M()V

    .line 1123
    invoke-virtual {p0, v2}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1124
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->c:Z

    if-nez v0, :cond_6

    .line 1126
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aD:Ljava/lang/Thread;

    const-string/jumbo v1, "AuthThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1127
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->aD:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/b;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1133
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    if-eqz v0, :cond_7

    .line 1134
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->ak:Lcom/amap/api/maps/CustomRenderer;

    invoke-interface {v0, p1, p2}, Lcom/amap/api/maps/CustomRenderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1136
    :cond_7
    return-void

    .line 1095
    :cond_8
    iget-object v3, p0, Lcom/amap/api/mapcore/b;->l:Lcom/autonavi/amap/mapcore/MapCore;

    move v4, v1

    move v5, v9

    move v6, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/autonavi/amap/mapcore/MapCore;->setParameter(IIIII)V

    goto :goto_0

    .line 1129
    :catch_0
    move-exception v0

    .line 1130
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1968
    iget-boolean v2, p0, Lcom/amap/api/mapcore/b;->aF:Z

    if-nez v2, :cond_0

    .line 1985
    :goto_0
    return v0

    .line 1972
    :cond_0
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1973
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->p:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1974
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->q:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1975
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->r:Lcom/amap/api/mapcore/a/c;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/a/c;->a(Landroid/view/MotionEvent;)Z

    .line 1976
    iget-object v2, p0, Lcom/amap/api/mapcore/b;->S:Lcom/amap/api/mapcore/a/b;

    invoke-virtual {v2, p1}, Lcom/amap/api/mapcore/a/b;->a(Landroid/view/MotionEvent;)Z

    .line 1977
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1978
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1979
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b;->a(Landroid/view/MotionEvent;)V

    .line 1981
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 1982
    invoke-direct {p0}, Lcom/amap/api/mapcore/b;->P()V

    .line 1984
    :cond_2
    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    move v0, v1

    .line 1985
    goto :goto_0
.end method

.method public p()F
    .locals 1

    .prologue
    .line 1174
    const/high16 v0, 0x40800000    # 4.0f

    return v0
.end method

.method public q()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1484
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1485
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->V:Lcom/amap/api/mapcore/h;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 1486
    invoke-virtual {p0, v1, v2}, Lcom/amap/api/mapcore/b;->a(ZLcom/amap/api/maps/model/CameraPosition;)V

    .line 1487
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 1488
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 1490
    :cond_0
    iput-object v2, p0, Lcom/amap/api/mapcore/b;->Y:Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 1492
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V

    .line 1493
    return-void
.end method

.method public r()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1583
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/b;->A()V

    .line 1584
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->d:Lcom/amap/api/mapcore/p;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/p;->a()V

    .line 1585
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->z:Lcom/amap/api/mapcore/ao;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/ao;->a()V

    .line 1586
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->u:Lcom/amap/api/mapcore/af;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->c()V

    .line 1587
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amap/api/mapcore/b;->e(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1593
    :goto_0
    return-void

    .line 1588
    :catch_0
    move-exception v0

    .line 1589
    const-string/jumbo v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "AMapDelegateImpGLSurfaceView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1591
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public s()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1612
    iget v0, p0, Lcom/amap/api/mapcore/b;->k:I

    return v0
.end method

.method public setZOrderOnTop(Z)V
    .locals 0

    .prologue
    .line 1159
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->setZOrderOnTop(Z)V

    .line 1160
    return-void
.end method

.method public t()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1640
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->at:Z

    return v0
.end method

.method public u()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1667
    iget-boolean v0, p0, Lcom/amap/api/mapcore/b;->P:Z

    return v0
.end method

.method public v()Landroid/location/Location;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1699
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->N:Lcom/amap/api/maps/LocationSource;

    if-eqz v0, :cond_0

    .line 1700
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->R:Lcom/amap/api/mapcore/f;

    iget-object v0, v0, Lcom/amap/api/mapcore/f;->a:Landroid/location/Location;

    .line 1702
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Lcom/amap/api/mapcore/aa;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->M:Lcom/amap/api/mapcore/aa;

    return-object v0
.end method

.method public x()Lcom/amap/api/mapcore/x;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->L:Lcom/amap/api/mapcore/x;

    return-object v0
.end method

.method public y()Lcom/amap/api/maps/AMap$OnCameraChangeListener;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1743
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->E:Lcom/amap/api/maps/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method public z()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1790
    iget-object v0, p0, Lcom/amap/api/mapcore/b;->t:Lcom/amap/api/mapcore/ag;

    return-object v0
.end method
