.class Lcom/amap/api/mapcore/b$a;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Lcom/amap/api/mapcore/a/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/Float;

.field b:Ljava/lang/Float;

.field c:F

.field d:Lcom/amap/api/mapcore/i;

.field final synthetic e:Lcom/amap/api/mapcore/b;

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2547
    iput-object p1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554
    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2555
    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2570
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$a;->c:F

    .line 2571
    iget v0, p0, Lcom/amap/api/mapcore/b$a;->c:F

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->c(F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$a;->d:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V
    .locals 0

    .prologue
    .line 2547
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$a;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public a(FFFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2560
    iput p2, p0, Lcom/amap/api/mapcore/b$a;->f:F

    .line 2561
    iput p3, p0, Lcom/amap/api/mapcore/b$a;->h:F

    .line 2562
    iput p4, p0, Lcom/amap/api/mapcore/b$a;->g:F

    .line 2563
    iput p5, p0, Lcom/amap/api/mapcore/b$a;->i:F

    .line 2564
    iget v0, p0, Lcom/amap/api/mapcore/b$a;->i:F

    iget v1, p0, Lcom/amap/api/mapcore/b$a;->h:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/amap/api/mapcore/b$a;->g:F

    iget v2, p0, Lcom/amap/api/mapcore/b$a;->f:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/mapcore/b$a;->j:F

    .line 2566
    iput-object v3, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2567
    iput-object v3, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2568
    return-void
.end method

.method public a(Landroid/view/MotionEvent;FFFF)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 2578
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/aa;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 2622
    :cond_0
    :goto_0
    return v0

    .line 2581
    :catch_0
    move-exception v1

    .line 2582
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2586
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2590
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    if-nez v1, :cond_2

    .line 2591
    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    .line 2593
    :cond_2
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    if-nez v1, :cond_3

    .line 2594
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2597
    :cond_3
    iget v1, p0, Lcom/amap/api/mapcore/b$a;->h:F

    sub-float/2addr v1, p3

    .line 2598
    iget v2, p0, Lcom/amap/api/mapcore/b$a;->i:F

    sub-float/2addr v2, p5

    .line 2599
    iget v3, p0, Lcom/amap/api/mapcore/b$a;->f:F

    sub-float/2addr v3, p2

    .line 2600
    iget v4, p0, Lcom/amap/api/mapcore/b$a;->g:F

    sub-float/2addr v4, p4

    .line 2601
    sub-float v5, p5, p3

    sub-float v6, p4, p2

    div-float/2addr v5, v6

    .line 2602
    iget v6, p0, Lcom/amap/api/mapcore/b$a;->j:F

    sub-float v5, v6, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-double v5, v5

    const-wide v7, 0x3fc999999999999aL    # 0.2

    cmpg-double v5, v5, v7

    if-gez v5, :cond_8

    cmpl-float v5, v1, v9

    if-lez v5, :cond_4

    cmpl-float v5, v2, v9

    if-gtz v5, :cond_5

    :cond_4
    cmpg-float v1, v1, v9

    if-gez v1, :cond_8

    cmpg-float v1, v2, v9

    if-gez v1, :cond_8

    :cond_5
    cmpl-float v1, v3, v9

    if-ltz v1, :cond_6

    cmpl-float v1, v4, v9

    if-gez v1, :cond_7

    :cond_6
    cmpg-float v1, v3, v9

    if-gtz v1, :cond_8

    cmpg-float v1, v4, v9

    if-gtz v1, :cond_8

    .line 2605
    :cond_7
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float/2addr v1, p3

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    .line 2607
    iget-object v2, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;Z)Z

    .line 2610
    iget-object v2, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getCameraHeaderAngle()F

    move-result v2

    sub-float v1, v2, v1

    iput v1, p0, Lcom/amap/api/mapcore/b$a;->c:F

    .line 2614
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->d:Lcom/amap/api/mapcore/i;

    iget v2, p0, Lcom/amap/api/mapcore/b$a;->c:F

    iput v2, v1, Lcom/amap/api/mapcore/i;->f:F

    .line 2615
    iget-object v1, p0, Lcom/amap/api/mapcore/b$a;->e:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$a;->d:Lcom/amap/api/mapcore/i;

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 2617
    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/b$a;->a:Ljava/lang/Float;

    .line 2618
    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/mapcore/b$a;->b:Ljava/lang/Float;

    goto/16 :goto_0

    .line 2622
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
