.class Lcom/amap/api/mapcore/j;
.super Ljava/lang/Object;
.source "CircleDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/r;


# static fields
.field private static n:F

.field private static o:I

.field private static p:I


# instance fields
.field private a:Lcom/amap/api/maps/model/LatLng;

.field private b:D

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/amap/api/mapcore/q;

.field private j:Ljava/nio/FloatBuffer;

.field private k:Ljava/nio/FloatBuffer;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 264
    const v0, 0x4c18dfc2    # 4.0075016E7f

    sput v0, Lcom/amap/api/mapcore/j;->n:F

    .line 265
    const/16 v0, 0x100

    sput v0, Lcom/amap/api/mapcore/j;->o:I

    .line 266
    const/16 v0, 0x14

    sput v0, Lcom/amap/api/mapcore/j;->p:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/mapcore/q;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->a:Lcom/amap/api/maps/model/LatLng;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/mapcore/j;->b:D

    .line 21
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/mapcore/j;->c:F

    .line 22
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/amap/api/mapcore/j;->d:I

    .line 23
    iput v2, p0, Lcom/amap/api/mapcore/j;->e:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/j;->f:F

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/j;->g:Z

    .line 30
    iput v2, p0, Lcom/amap/api/mapcore/j;->l:I

    iput v2, p0, Lcom/amap/api/mapcore/j;->m:I

    .line 34
    iput-object p1, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    .line 36
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->h:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private b(D)F
    .locals 4

    .prologue
    .line 272
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, p1

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    sget v2, Lcom/amap/api/mapcore/j;->n:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sget v2, Lcom/amap/api/mapcore/j;->o:I

    sget v3, Lcom/amap/api/mapcore/j;->p:I

    shl-int/2addr v2, v3

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private c(D)D
    .locals 4

    .prologue
    .line 276
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, p1, p2}, Lcom/amap/api/mapcore/j;->b(D)F

    move-result v2

    float-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(D)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 223
    iput-wide p1, p0, Lcom/amap/api/mapcore/j;->b:D

    .line 224
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->h()V

    .line 225
    return-void
.end method

.method public a(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    iput p1, p0, Lcom/amap/api/mapcore/j;->f:F

    .line 62
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 63
    return-void
.end method

.method public a(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 245
    iput p1, p0, Lcom/amap/api/mapcore/j;->d:I

    .line 246
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 212
    iput-object p1, p0, Lcom/amap/api/mapcore/j;->a:Lcom/amap/api/maps/model/LatLng;

    .line 213
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->h()V

    .line 214
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->i()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/amap/api/mapcore/j;->b:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/j;->l:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/j;->m:I

    if-nez v0, :cond_3

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->g()V

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/j;->l:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/j;->m:I

    if-lez v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->m()I

    move-result v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->l()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->k()F

    move-result v4

    iget-object v5, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    iget v6, p0, Lcom/amap/api/mapcore/j;->l:I

    iget v7, p0, Lcom/amap/api/mapcore/j;->m:I

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/amap/api/mapcore/n;->b(Ljavax/microedition/khronos/opengles/GL10;IILjava/nio/FloatBuffer;FLjava/nio/FloatBuffer;II)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/amap/api/mapcore/j;->g:Z

    .line 73
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 74
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/amap/api/mapcore/u;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/u;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Ljava/lang/String;)Z

    .line 48
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 49
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 234
    iput p1, p0, Lcom/amap/api/mapcore/j;->c:F

    .line 235
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 236
    return-void
.end method

.method public b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 255
    iput p1, p0, Lcom/amap/api/mapcore/j;->e:I

    .line 256
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 257
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 54
    const-string/jumbo v0, "Circle"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->h:Ljava/lang/String;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->h:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 67
    iget v0, p0, Lcom/amap/api/mapcore/j;->f:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/amap/api/mapcore/j;->g:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->i()Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_2

    .line 112
    const/16 v1, 0x168

    new-array v2, v1, [Lcom/autonavi/amap/mapcore/FPoint;

    .line 115
    array-length v1, v2

    mul-int/lit8 v1, v1, 0x3

    new-array v3, v1, [F

    .line 116
    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->i()Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    iget-wide v4, v1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-direct {p0, v4, v5}, Lcom/amap/api/mapcore/j;->c(D)D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/amap/api/mapcore/j;->j()D

    move-result-wide v6

    mul-double/2addr v4, v6

    .line 118
    new-instance v1, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 119
    iget-object v6, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v6}, Lcom/amap/api/mapcore/q;->d()Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v6

    .line 120
    iget-wide v7, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    iget-wide v9, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v7, v8, v9, v10, v1}, Lcom/autonavi/amap/mapcore/MapProjection;->lonlat2Geo(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 121
    const/4 v0, 0x0

    :goto_0
    const/16 v7, 0x168

    if-ge v0, v7, :cond_0

    .line 122
    int-to-double v7, v0

    const-wide v9, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v7, v9

    const-wide v9, 0x4066800000000000L    # 180.0

    div-double/2addr v7, v9

    .line 123
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    mul-double/2addr v9, v4

    .line 124
    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double/2addr v7, v4

    .line 126
    iget v11, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    int-to-double v11, v11

    add-double/2addr v9, v11

    double-to-int v9, v9

    .line 127
    iget v10, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    int-to-double v10, v10

    add-double/2addr v7, v10

    double-to-int v7, v7

    .line 128
    new-instance v8, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v8}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 129
    invoke-virtual {v6, v9, v7, v8}, Lcom/autonavi/amap/mapcore/MapProjection;->geo2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 130
    aput-object v8, v2, v0

    .line 132
    mul-int/lit8 v7, v0, 0x3

    aget-object v8, v2, v0

    iget v8, v8, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v8, v3, v7

    .line 133
    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x1

    aget-object v8, v2, v0

    iget v8, v8, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v8, v3, v7

    .line 134
    mul-int/lit8 v7, v0, 0x3

    add-int/lit8 v7, v7, 0x2

    const/4 v8, 0x0

    aput v8, v3, v7

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    invoke-static {v2}, Lcom/amap/api/mapcore/ak;->a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v4

    .line 138
    const/4 v1, 0x0

    .line 139
    array-length v0, v4

    mul-int/lit8 v0, v0, 0x3

    new-array v5, v0, [F

    .line 140
    array-length v6, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v4, v0

    .line 141
    mul-int/lit8 v8, v1, 0x3

    iget v9, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v9, v5, v8

    .line 142
    mul-int/lit8 v8, v1, 0x3

    add-int/lit8 v8, v8, 0x1

    iget v7, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v7, v5, v8

    .line 143
    mul-int/lit8 v7, v1, 0x3

    add-int/lit8 v7, v7, 0x2

    const/4 v8, 0x0

    aput v8, v5, v7

    .line 144
    add-int/lit8 v1, v1, 0x1

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 147
    :cond_1
    array-length v0, v2

    iput v0, p0, Lcom/amap/api/mapcore/j;->l:I

    .line 148
    array-length v0, v4

    iput v0, p0, Lcom/amap/api/mapcore/j;->m:I

    .line 150
    invoke-static {v3}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    .line 151
    invoke-static {v5}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    .line 157
    :cond_2
    return-void
.end method

.method h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    iput v1, p0, Lcom/amap/api/mapcore/j;->l:I

    .line 200
    iput v1, p0, Lcom/amap/api/mapcore/j;->m:I

    .line 201
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->i:Lcom/amap/api/mapcore/q;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 208
    return-void
.end method

.method public i()Lcom/amap/api/maps/model/LatLng;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->a:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method public j()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 229
    iget-wide v0, p0, Lcom/amap/api/mapcore/j;->b:D

    return-wide v0
.end method

.method public k()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 240
    iget v0, p0, Lcom/amap/api/mapcore/j;->c:F

    return v0
.end method

.method public l()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 250
    iget v0, p0, Lcom/amap/api/mapcore/j;->d:I

    return v0
.end method

.method public m()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 261
    iget v0, p0, Lcom/amap/api/mapcore/j;->e:I

    return v0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 285
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/amap/api/mapcore/j;->a:Lcom/amap/api/maps/model/LatLng;

    .line 287
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/amap/api/mapcore/j;->k:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/j;->j:Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :cond_1
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 297
    const-string/jumbo v0, "destroy erro"

    const-string/jumbo v1, "CircleDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
