.class Lcom/amap/api/mapcore/b$d;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Lcom/amap/api/mapcore/a/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field a:F

.field b:F

.field c:Lcom/amap/api/mapcore/i;

.field final synthetic d:Lcom/amap/api/mapcore/b;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2475
    iput-object p1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2477
    iput v0, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2478
    iput v0, p0, Lcom/amap/api/mapcore/b$d;->b:F

    .line 2479
    iget v0, p0, Lcom/amap/api/mapcore/b$d;->b:F

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->d(F)Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V
    .locals 0

    .prologue
    .line 2475
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$d;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/amap/api/mapcore/a/c;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2484
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2485
    const/4 v0, 0x0

    .line 2500
    :cond_0
    :goto_0
    return v0

    .line 2487
    :cond_1
    invoke-virtual {p1}, Lcom/amap/api/mapcore/a/c;->b()F

    move-result v1

    .line 2488
    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    add-float/2addr v2, v1

    iput v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2489
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Z

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_2

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->a:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x43af0000    # 350.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 2490
    :cond_2
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2491
    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapAngle()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->b:F

    .line 2495
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/amap/api/mapcore/i;

    iget v2, p0, Lcom/amap/api/mapcore/b$d;->b:F

    iput v2, v1, Lcom/amap/api/mapcore/i;->g:F

    .line 2496
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    iget-object v2, p0, Lcom/amap/api/mapcore/b$d;->c:Lcom/amap/api/mapcore/i;

    invoke-virtual {v1, v2}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 2497
    const/4 v1, 0x0

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->a:F

    goto :goto_0
.end method

.method public b(Lcom/amap/api/mapcore/a/c;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2507
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/mapcore/aa;->h()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 2523
    :cond_0
    :goto_0
    return v0

    .line 2510
    :catch_0
    move-exception v1

    .line 2511
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2514
    :cond_1
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1, v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2515
    const/4 v1, 0x0

    iput v1, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2516
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;I)I

    .line 2517
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2520
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/amap/api/mapcore/a/c;->c()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 2521
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Lcom/amap/api/mapcore/a/c;)V
    .locals 2

    .prologue
    .line 2529
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/b$d;->a:F

    .line 2530
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->x(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2531
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;Z)Z

    .line 2532
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 2534
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/mapcore/i;->o:Z

    .line 2535
    iget-object v1, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    iget-object v1, v1, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    invoke-virtual {v1, v0}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    .line 2537
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$d;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;)V

    .line 2538
    return-void
.end method
