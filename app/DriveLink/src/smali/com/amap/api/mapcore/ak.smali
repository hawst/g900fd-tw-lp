.class Lcom/amap/api/mapcore/ak;
.super Ljava/lang/Object;
.source "PolygonDelegateImp.java"

# interfaces
.implements Lcom/amap/api/mapcore/v;


# static fields
.field private static n:F


# instance fields
.field private a:Lcom/amap/api/mapcore/q;

.field private b:F

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:F

.field private f:I

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore/IPoint;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/nio/FloatBuffer;

.field private j:Ljava/nio/FloatBuffer;

.field private k:I

.field private l:I

.field private m:Lcom/amap/api/maps/model/LatLngBounds;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 325
    const v0, 0x501502f9    # 1.0E10f

    sput v0, Lcom/amap/api/mapcore/ak;->n:F

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/mapcore/q;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/mapcore/ak;->b:F

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/mapcore/ak;->c:Z

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    .line 35
    iput v1, p0, Lcom/amap/api/mapcore/ak;->k:I

    iput v1, p0, Lcom/amap/api/mapcore/ak;->l:I

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->m:Lcom/amap/api/maps/model/LatLngBounds;

    .line 39
    iput-object p1, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    .line 41
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 328
    array-length v2, p0

    .line 329
    new-array v3, v2, [Lcom/autonavi/amap/mapcore/FPoint;

    move v1, v0

    .line 330
    :goto_0
    if-ge v1, v2, :cond_0

    .line 331
    new-instance v4, Lcom/autonavi/amap/mapcore/FPoint;

    aget-object v5, p0, v1

    iget v5, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sget v6, Lcom/amap/api/mapcore/ak;->n:F

    mul-float/2addr v5, v6

    aget-object v6, p0, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sget v7, Lcom/amap/api/mapcore/ak;->n:F

    mul-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Lcom/autonavi/amap/mapcore/FPoint;-><init>(FF)V

    aput-object v4, v3, v1

    .line 330
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 334
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 335
    invoke-static {v3, v2}, Lcom/amap/api/mapcore/b/g;->a([Lcom/autonavi/amap/mapcore/FPoint;Ljava/util/List;)Z

    .line 336
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 337
    new-array v4, v3, [Lcom/autonavi/amap/mapcore/FPoint;

    move v1, v0

    .line 338
    :goto_1
    if-ge v1, v3, :cond_1

    .line 339
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    aput-object v0, v4, v1

    .line 340
    aget-object v5, v4, v1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/FPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sget v6, Lcom/amap/api/mapcore/ak;->n:F

    div-float/2addr v0, v6

    iput v0, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 341
    aget-object v5, v4, v1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/FPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sget v6, Lcom/amap/api/mapcore/ak;->n:F

    div-float/2addr v0, v6

    iput v0, v5, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 338
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 343
    :cond_1
    return-object v4
.end method


# virtual methods
.method public a(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 74
    iput p1, p0, Lcom/amap/api/mapcore/ak;->b:F

    .line 75
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 76
    return-void
.end method

.method public a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 283
    iput p1, p0, Lcom/amap/api/mapcore/ak;->f:I

    .line 284
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 285
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/amap/api/mapcore/ak;->b(Ljava/util/List;)V

    .line 64
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 65
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/ak;->k:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/amap/api/mapcore/ak;->l:I

    if-nez v0, :cond_3

    .line 259
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->g()V

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/ak;->k:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/amap/api/mapcore/ak;->l:I

    if-lez v0, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->i()I

    move-result v1

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->k()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->h()F

    move-result v4

    iget-object v5, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    iget v6, p0, Lcom/amap/api/mapcore/ak;->k:I

    iget v7, p0, Lcom/amap/api/mapcore/ak;->l:I

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/amap/api/mapcore/n;->a(Ljavax/microedition/khronos/opengles/GL10;IILjava/nio/FloatBuffer;FLjava/nio/FloatBuffer;II)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/amap/api/mapcore/ak;->c:Z

    .line 86
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 87
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 237
    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->m:Lcom/amap/api/maps/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 240
    :cond_1
    iget-object v2, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v2}, Lcom/amap/api/mapcore/q;->D()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v2

    .line 241
    if-nez v2, :cond_2

    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_2
    iget-object v3, p0, Lcom/amap/api/mapcore/ak;->m:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->contains(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/mapcore/ak;->m:Lcom/amap/api/maps/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps/model/LatLngBounds;->intersects(Lcom/amap/api/maps/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/mapcore/u;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/mapcore/u;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Ljava/lang/String;)Z

    .line 50
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 51
    return-void
.end method

.method public b(F)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 272
    iput p1, p0, Lcom/amap/api/mapcore/ak;->e:F

    .line 273
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 274
    return-void
.end method

.method public b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 294
    iput p1, p0, Lcom/amap/api/mapcore/ak;->g:I

    .line 295
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 296
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 105
    .line 106
    invoke-static {}, Lcom/amap/api/maps/model/LatLngBounds;->builder()Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-result-object v7

    .line 107
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 108
    if-eqz p1, :cond_2

    .line 109
    const/4 v6, 0x0

    .line 110
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v6

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/amap/api/maps/model/LatLng;

    .line 111
    invoke-virtual {v6, v1}, Lcom/amap/api/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 114
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    iget-wide v1, v6, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v6, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/mapcore/q;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 115
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-virtual {v7, v6}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLngBounds$Builder;

    move-object v1, v6

    .line 118
    goto :goto_0

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 120
    const/4 v0, 0x1

    if-le v2, v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 122
    iget-object v1, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    add-int/lit8 v3, v2, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/autonavi/amap/mapcore/IPoint;

    .line 123
    iget v3, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v4, v1, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    if-ne v3, v4, :cond_2

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v1, v1, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    if-ne v0, v1, :cond_2

    .line 124
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 128
    :cond_2
    invoke-virtual {v7}, Lcom/amap/api/maps/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->m:Lcom/amap/api/maps/model/LatLngBounds;

    .line 129
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 132
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_4

    .line 133
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 135
    :cond_4
    iput v9, p0, Lcom/amap/api/mapcore/ak;->k:I

    .line 136
    iput v9, p0, Lcom/amap/api/mapcore/ak;->l:I

    .line 137
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    invoke-interface {v0, v9}, Lcom/amap/api/mapcore/q;->e(Z)V

    .line 138
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 56
    const-string/jumbo v0, "Polygon"

    invoke-static {v0}, Lcom/amap/api/mapcore/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Ljava/lang/String;

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    iget v0, p0, Lcom/amap/api/mapcore/ak;->b:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/amap/api/mapcore/ak;->c:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 202
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const v9, 0x501502f9    # 1.0E10f

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 162
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcom/autonavi/amap/mapcore/FPoint;

    .line 163
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    new-array v4, v0, [F

    .line 165
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 166
    new-instance v6, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    aput-object v6, v3, v1

    .line 167
    iget-object v6, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    iget v7, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    aget-object v8, v3, v1

    invoke-interface {v6, v7, v0, v8}, Lcom/amap/api/mapcore/q;->a(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 168
    mul-int/lit8 v0, v1, 0x3

    aget-object v6, v3, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v6, v4, v0

    .line 169
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x1

    aget-object v6, v3, v1

    iget v6, v6, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v6, v4, v0

    .line 170
    mul-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, 0x2

    aput v10, v4, v0

    .line 171
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 174
    :cond_0
    invoke-static {v3}, Lcom/amap/api/mapcore/ak;->a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v0

    .line 175
    array-length v1, v0

    if-nez v1, :cond_1

    .line 176
    sget v0, Lcom/amap/api/mapcore/ak;->n:F

    cmpl-float v0, v0, v9

    if-nez v0, :cond_2

    .line 177
    const v0, 0x4cbebc20    # 1.0E8f

    sput v0, Lcom/amap/api/mapcore/ak;->n:F

    .line 181
    :goto_1
    invoke-static {v3}, Lcom/amap/api/mapcore/ak;->a([Lcom/autonavi/amap/mapcore/FPoint;)[Lcom/autonavi/amap/mapcore/FPoint;

    move-result-object v0

    .line 183
    :cond_1
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [F

    .line 185
    array-length v6, v0

    move v1, v2

    :goto_2
    if-ge v2, v6, :cond_3

    aget-object v7, v0, v2

    .line 186
    mul-int/lit8 v8, v1, 0x3

    iget v9, v7, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    aput v9, v5, v8

    .line 187
    mul-int/lit8 v8, v1, 0x3

    add-int/lit8 v8, v8, 0x1

    iget v7, v7, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    aput v7, v5, v8

    .line 188
    mul-int/lit8 v7, v1, 0x3

    add-int/lit8 v7, v7, 0x2

    aput v10, v5, v7

    .line 189
    add-int/lit8 v1, v1, 0x1

    .line 185
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 179
    :cond_2
    sput v9, Lcom/amap/api/mapcore/ak;->n:F

    goto :goto_1

    .line 192
    :cond_3
    array-length v1, v3

    iput v1, p0, Lcom/amap/api/mapcore/ak;->k:I

    .line 193
    array-length v0, v0

    iput v0, p0, Lcom/amap/api/mapcore/ak;->l:I

    .line 195
    invoke-static {v4}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    .line 196
    invoke-static {v5}, Lcom/amap/api/mapcore/b/h;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    .line 198
    return-void
.end method

.method public h()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 278
    iget v0, p0, Lcom/amap/api/mapcore/ak;->e:F

    return v0
.end method

.method public i()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 289
    iget v0, p0, Lcom/amap/api/mapcore/ak;->f:I

    return v0
.end method

.method public j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ak;->l()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 300
    iget v0, p0, Lcom/amap/api/mapcore/ak;->g:I

    return v0
.end method

.method l()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 143
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/IPoint;

    .line 144
    if-eqz v0, :cond_0

    .line 145
    new-instance v3, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 146
    iget-object v4, p0, Lcom/amap/api/mapcore/ak;->a:Lcom/amap/api/mapcore/q;

    iget v5, v0, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-interface {v4, v5, v0, v3}, Lcom/amap/api/mapcore/q;->b(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 147
    new-instance v0, Lcom/amap/api/maps/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 152
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public n()V
    .locals 2

    .prologue
    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 359
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->i:Ljava/nio/FloatBuffer;

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ak;->j:Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :cond_1
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v0

    .line 365
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 366
    const-string/jumbo v0, "destroy erro"

    const-string/jumbo v1, "PolygonDelegateImp destroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
