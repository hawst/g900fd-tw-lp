.class public Lcom/amap/api/mapcore/l;
.super Ljava/lang/Object;
.source "ConfigableConst.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/l$a;
    }
.end annotation


# static fields
.field public static a:F

.field public static b:F

.field public static final c:Ljava/lang/String;

.field public static final d:Lcom/amap/api/mapcore/l$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 15
    const v0, 0x3f666666    # 0.9f

    sput v0, Lcom/amap/api/mapcore/l;->a:F

    .line 16
    const/high16 v0, 0x41900000    # 18.0f

    sput v0, Lcom/amap/api/mapcore/l;->b:F

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "AMAP SDK Android Map "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "V2.1.3"

    const/4 v2, 0x1

    const-string/jumbo v3, "V2.1.3"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    .line 25
    sget-object v0, Lcom/amap/api/mapcore/l$a;->a:Lcom/amap/api/mapcore/l$a;

    sput-object v0, Lcom/amap/api/mapcore/l;->d:Lcom/amap/api/mapcore/l$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method
