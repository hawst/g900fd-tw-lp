.class Lcom/amap/api/mapcore/ai;
.super Ljava/lang/Object;
.source "MyLocationOverlay.java"


# instance fields
.field private a:Lcom/amap/api/mapcore/q;

.field private b:Lcom/amap/api/mapcore/t;

.field private c:Lcom/amap/api/mapcore/r;

.field private d:Lcom/amap/api/maps/model/MyLocationStyle;

.field private e:Lcom/amap/api/maps/model/LatLng;

.field private f:D


# direct methods
.method constructor <init>(Lcom/amap/api/mapcore/q;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    .line 30
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    if-nez v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/amap/api/mapcore/ai;->c()V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/mapcore/ai;->d()V

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    new-instance v1, Lcom/amap/api/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/CircleOptions;-><init>()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0x64

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xb4

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xdc

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->center(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    .line 108
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    const-wide/high16 v1, 0x4069000000000000L    # 200.0

    invoke-interface {v0, v1, v2}, Lcom/amap/api/mapcore/r;->a(D)V

    .line 109
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/amap/api/mapcore/y$a;->c:Lcom/amap/api/mapcore/y$a;

    invoke-virtual {v3}, Lcom/amap/api/mapcore/y$a;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromAsset(Ljava/lang/String;)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private d()V
    .locals 7

    .prologue
    .line 122
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    new-instance v1, Lcom/amap/api/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/CircleOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getStrokeWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getRadiusFillColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getStrokeColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/CircleOptions;->center(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Lcom/amap/api/maps/model/CircleOptions;)Lcom/amap/api/mapcore/r;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    .line 131
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->e:Lcom/amap/api/maps/model/LatLng;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    iget-object v1, p0, Lcom/amap/api/mapcore/ai;->e:Lcom/amap/api/maps/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    iget-wide v1, p0, Lcom/amap/api/mapcore/ai;->f:D

    invoke-interface {v0, v1, v2}, Lcom/amap/api/mapcore/r;->a(D)V

    .line 135
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getAnchorU()F

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v3}, Lcom/amap/api/maps/model/MyLocationStyle;->getAnchorV()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps/model/MyLocationStyle;->getMyLocationIcon()Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->b(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/mapcore/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    .line 139
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->e:Lcom/amap/api/maps/model/LatLng;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    iget-object v1, p0, Lcom/amap/api/mapcore/ai;->e:Lcom/amap/api/maps/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/t;->a(Lcom/amap/api/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    iget-object v1, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v1}, Lcom/amap/api/mapcore/r;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->a(Ljava/lang/String;)Z

    .line 74
    iput-object v2, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->a:Lcom/amap/api/mapcore/q;

    iget-object v1, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    invoke-interface {v1}, Lcom/amap/api/mapcore/t;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/mapcore/q;->b(Ljava/lang/String;)Z

    .line 78
    iput-object v2, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    .line 80
    :cond_1
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(F)V

    .line 151
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/maps/model/LatLng;D)V
    .locals 2

    .prologue
    .line 47
    iput-object p1, p0, Lcom/amap/api/mapcore/ai;->e:Lcom/amap/api/maps/model/LatLng;

    .line 48
    iput-wide p2, p0, Lcom/amap/api/mapcore/ai;->f:D

    .line 49
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    if-nez v0, :cond_0

    .line 50
    invoke-direct {p0}, Lcom/amap/api/mapcore/ai;->b()V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/t;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/r;->a(Lcom/amap/api/maps/model/LatLng;)V

    .line 55
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    invoke-interface {v0, p2, p3}, Lcom/amap/api/mapcore/r;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :cond_1
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 33
    iput-object p1, p0, Lcom/amap/api/mapcore/ai;->d:Lcom/amap/api/maps/model/MyLocationStyle;

    .line 34
    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->b:Lcom/amap/api/mapcore/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/ai;->c:Lcom/amap/api/mapcore/r;

    if-nez v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 38
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/mapcore/ai;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_1
    invoke-direct {p0}, Lcom/amap/api/mapcore/ai;->d()V

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
