.class Lcom/amap/api/mapcore/b$c;
.super Ljava/lang/Object;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field a:Lcom/autonavi/amap/mapcore/FPoint;

.field b:Lcom/autonavi/amap/mapcore/IPoint;

.field c:Lcom/amap/api/mapcore/i;

.field final synthetic d:Lcom/amap/api/mapcore/b;


# direct methods
.method private constructor <init>(Lcom/amap/api/mapcore/b;)V
    .locals 1

    .prologue
    .line 2067
    iput-object p1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068
    new-instance v0, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b$c;->a:Lcom/autonavi/amap/mapcore/FPoint;

    .line 2209
    new-instance v0, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/b$c;->b:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2210
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->b:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->a(Lcom/autonavi/amap/mapcore/IPoint;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/mapcore/b$c;->c:Lcom/amap/api/mapcore/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/mapcore/b;Lcom/amap/api/mapcore/c;)V
    .locals 0

    .prologue
    .line 2067
    invoke-direct {p0, p1}, Lcom/amap/api/mapcore/b$c;-><init>(Lcom/amap/api/mapcore/b;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2072
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2074
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->g(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2075
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/amap/api/mapcore/h;->a(Z)V

    .line 2076
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v2, v3}, Lcom/amap/api/mapcore/b;->a(ZLcom/amap/api/maps/model/CameraPosition;)V

    .line 2077
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2078
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->i(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$CancelableCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/maps/AMap$CancelableCallback;->onCancel()V

    .line 2079
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v3}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 2081
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;Z)Z

    .line 2082
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;I)I

    .line 2083
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->a:Lcom/autonavi/amap/mapcore/FPoint;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 2084
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->a:Lcom/autonavi/amap/mapcore/FPoint;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, v0, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 2085
    return v2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2091
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2093
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    .line 2118
    :cond_0
    :goto_0
    return v9

    .line 2096
    :catch_0
    move-exception v0

    .line 2097
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2100
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/mapcore/a/b;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 2105
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0}, Lcom/amap/api/mapcore/b;->j()I

    move-result v0

    .line 2106
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {v1}, Lcom/amap/api/mapcore/b;->k()I

    move-result v1

    .line 2107
    mul-int/lit8 v6, v0, 0x2

    .line 2108
    mul-int/lit8 v8, v1, 0x2

    .line 2109
    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    div-int/lit8 v0, v0, 0x2

    invoke-static {v2, v0}, Lcom/amap/api/mapcore/b;->b(Lcom/amap/api/mapcore/b;I)I

    .line 2110
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->c(Lcom/amap/api/mapcore/b;I)I

    .line 2111
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/AMap$CancelableCallback;)Lcom/amap/api/maps/AMap$CancelableCallback;

    .line 2112
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->k(Lcom/amap/api/mapcore/b;)I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->l(Lcom/amap/api/mapcore/b;)I

    move-result v2

    neg-float v3, p3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x5

    neg-float v4, p4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x5

    neg-int v5, v6

    neg-int v7, v8

    invoke-virtual/range {v0 .. v8}, Lcom/amap/api/mapcore/h;->a(IIIIIIII)V

    .line 2115
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ao;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2116
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->m(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/ao;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/amap/api/mapcore/ao;->b(Z)V

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2123
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2124
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->n(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2125
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2126
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2127
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->n(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapLongClickListener;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    iget-wide v3, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v5, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, Lcom/amap/api/maps/AMap$OnMapLongClickListener;->onMapLongClick(Lcom/amap/api/maps/model/LatLng;)V

    .line 2128
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;Z)Z

    .line 2130
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/af;->a(Landroid/view/MotionEvent;)Lcom/amap/api/mapcore/t;

    move-result-object v0

    .line 2132
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->p(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2134
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    new-instance v2, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v2, v0}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    invoke-static {v1, v2}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/b;Lcom/amap/api/maps/model/Marker;)Lcom/amap/api/maps/model/Marker;

    .line 2135
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/Marker;->getPosition()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 2136
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2137
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    iget-wide v1, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/mapcore/b;->b(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2138
    iget v0, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    add-int/lit8 v0, v0, -0x3c

    iput v0, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    .line 2139
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2140
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    iget v2, v5, Lcom/autonavi/amap/mapcore/IPoint;->x:I

    iget v3, v5, Lcom/autonavi/amap/mapcore/IPoint;->y:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2141
    new-instance v1, Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    .line 2142
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/model/Marker;->setPosition(Lcom/amap/api/maps/model/LatLng;)V

    .line 2143
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->p(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerDragListener;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->q(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/model/Marker;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnMarkerDragListener;->onMarkerDragStart(Lcom/amap/api/maps/model/Marker;)V

    .line 2144
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v7}, Lcom/amap/api/mapcore/b;->g(Lcom/amap/api/mapcore/b;Z)Z

    .line 2146
    :cond_1
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 2154
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v8}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2155
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->h(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/h;->j()I

    move-result v0

    if-eq v0, v8, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->j(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/mapcore/a/b;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 2159
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v4}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2206
    :goto_0
    return v8

    .line 2166
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    .line 2167
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v4}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    goto :goto_0

    .line 2171
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/aa;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/mapcore/aa;->e()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2172
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2175
    :catch_0
    move-exception v0

    .line 2176
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2179
    :cond_4
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->r(Lcom/amap/api/mapcore/b;)I

    move-result v0

    if-le v0, v8, :cond_5

    .line 2180
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v4}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    goto :goto_0

    .line 2183
    :cond_5
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->a:Lcom/autonavi/amap/mapcore/FPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    .line 2184
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->a:Lcom/autonavi/amap/mapcore/FPoint;

    iget v1, v1, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    .line 2186
    sub-float v2, v0, p3

    .line 2187
    sub-float v3, v1, p4

    .line 2189
    new-instance v4, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v4}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2190
    new-instance v5, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2191
    new-instance v6, Lcom/autonavi/amap/mapcore/FPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore/FPoint;-><init>()V

    .line 2192
    iget-object v7, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v7}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v7

    float-to-int v0, v0

    float-to-int v1, v1

    invoke-virtual {v7, v0, v1, v4}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2193
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v0

    float-to-int v1, v2

    float-to-int v2, v3

    invoke-virtual {v0, v1, v2, v5}, Lcom/autonavi/amap/mapcore/MapProjection;->win2Map(IILcom/autonavi/amap/mapcore/FPoint;)V

    .line 2195
    iget v0, v5, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    iget v1, v4, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sub-float/2addr v0, v1

    .line 2196
    iget v1, v5, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    iget v2, v4, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sub-float/2addr v1, v2

    .line 2197
    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/autonavi/amap/mapcore/MapProjection;->getMapCenter(Lcom/autonavi/amap/mapcore/FPoint;)V

    .line 2199
    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->e(Lcom/amap/api/mapcore/b;)Lcom/autonavi/amap/mapcore/MapProjection;

    move-result-object v2

    iget v3, v6, Lcom/autonavi/amap/mapcore/FPoint;->x:F

    sub-float v0, v3, v0

    iget v3, v6, Lcom/autonavi/amap/mapcore/FPoint;->y:F

    sub-float v1, v3, v1

    iget-object v3, p0, Lcom/amap/api/mapcore/b$c;->b:Lcom/autonavi/amap/mapcore/IPoint;

    invoke-virtual {v2, v0, v1, v3}, Lcom/autonavi/amap/mapcore/MapProjection;->map2Geo(FFLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2203
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->c:Lcom/amap/api/mapcore/i;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->b:Lcom/autonavi/amap/mapcore/IPoint;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    .line 2204
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    iget-object v0, v0, Lcom/amap/api/mapcore/b;->a:Lcom/amap/api/mapcore/ae;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->c:Lcom/amap/api/mapcore/i;

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/ae;->a(Lcom/amap/api/mapcore/i;)V

    goto/16 :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2216
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 2220
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->d(Lcom/amap/api/mapcore/b;Z)Z

    .line 2221
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->s(Lcom/amap/api/mapcore/b;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2222
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0, v1}, Lcom/amap/api/mapcore/b;->f(Lcom/amap/api/mapcore/b;Z)Z

    .line 2287
    :cond_0
    :goto_0
    return v7

    .line 2228
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->t(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2231
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->t(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v2}, Lcom/amap/api/mapcore/b;->t(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v3}, Lcom/amap/api/mapcore/b;->t(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v4}, Lcom/amap/api/mapcore/b;->t(Lcom/amap/api/mapcore/b;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2235
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/amap/api/mapcore/af;->a(Landroid/graphics/Rect;II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->u(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2237
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->d()Lcom/amap/api/mapcore/t;

    move-result-object v0

    .line 2239
    invoke-interface {v0}, Lcom/amap/api/mapcore/t;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2242
    new-instance v1, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v1, v0}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    .line 2243
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->u(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/amap/api/maps/AMap$OnInfoWindowClickListener;->onInfoWindowClick(Lcom/amap/api/maps/model/Marker;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2283
    :catch_0
    move-exception v0

    .line 2284
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2247
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/mapcore/af;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2248
    if-eqz v0, :cond_7

    .line 2249
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->d()Lcom/amap/api/mapcore/t;

    move-result-object v6

    .line 2250
    if-eqz v6, :cond_0

    invoke-interface {v6}, Lcom/amap/api/mapcore/t;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2253
    new-instance v0, Lcom/amap/api/maps/model/Marker;

    invoke-direct {v0, v6}, Lcom/amap/api/maps/model/Marker;-><init>(Lcom/amap/api/mapcore/t;)V

    .line 2254
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->v(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2255
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->v(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/amap/api/maps/AMap$OnMarkerClickListener;->onMarkerClick(Lcom/amap/api/maps/model/Marker;)Z

    move-result v0

    .line 2257
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->b()I

    move-result v0

    if-gtz v0, :cond_4

    .line 2258
    :cond_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/amap/api/mapcore/af;->c(Lcom/amap/api/mapcore/t;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 2262
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/mapcore/af;->d()Lcom/amap/api/mapcore/t;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2263
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {v0, v6}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/t;)V

    .line 2264
    :cond_5
    invoke-interface {v6}, Lcom/amap/api/mapcore/t;->c()Lcom/amap/api/maps/model/LatLng;

    move-result-object v3

    .line 2265
    if-eqz v3, :cond_6

    .line 2266
    new-instance v5, Lcom/autonavi/amap/mapcore/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore/IPoint;-><init>()V

    .line 2267
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    iget-wide v1, v3, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v3, v3, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/mapcore/b;->a(DDLcom/autonavi/amap/mapcore/IPoint;)V

    .line 2268
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v5}, Lcom/amap/api/mapcore/i;->a(Lcom/autonavi/amap/mapcore/IPoint;)Lcom/amap/api/mapcore/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/mapcore/b;->a(Lcom/amap/api/mapcore/i;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2275
    :cond_6
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->o(Lcom/amap/api/mapcore/b;)Lcom/amap/api/mapcore/af;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/amap/api/mapcore/af;->c(Lcom/amap/api/mapcore/t;)V

    goto/16 :goto_0

    .line 2271
    :catch_1
    move-exception v0

    .line 2272
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 2278
    :cond_7
    iget-object v0, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v0}, Lcom/amap/api/mapcore/b;->w(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2279
    new-instance v0, Lcom/autonavi/amap/mapcore/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore/DPoint;-><init>()V

    .line 2280
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/amap/api/mapcore/b;->a(IILcom/autonavi/amap/mapcore/DPoint;)V

    .line 2281
    iget-object v1, p0, Lcom/amap/api/mapcore/b$c;->d:Lcom/amap/api/mapcore/b;

    invoke-static {v1}, Lcom/amap/api/mapcore/b;->w(Lcom/amap/api/mapcore/b;)Lcom/amap/api/maps/AMap$OnMapClickListener;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    iget-wide v3, v0, Lcom/autonavi/amap/mapcore/DPoint;->y:D

    iget-wide v5, v0, Lcom/autonavi/amap/mapcore/DPoint;->x:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, Lcom/amap/api/maps/AMap$OnMapClickListener;->onMapClick(Lcom/amap/api/maps/model/LatLng;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method
