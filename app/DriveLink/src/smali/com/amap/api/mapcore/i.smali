.class public Lcom/amap/api/mapcore/i;
.super Ljava/lang/Object;
.source "CameraUpdateFactoryDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/i$a;
    }
.end annotation


# instance fields
.field a:Lcom/amap/api/mapcore/i$a;

.field b:F

.field c:F

.field d:F

.field e:F

.field f:F

.field g:F

.field h:Lcom/amap/api/maps/model/CameraPosition;

.field i:Lcom/amap/api/maps/model/LatLngBounds;

.field j:I

.field k:I

.field l:I

.field m:Landroid/graphics/Point;

.field n:Lcom/autonavi/amap/mapcore/IPoint;

.field o:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, Lcom/amap/api/mapcore/i$a;->a:Lcom/amap/api/mapcore/i$a;

    iput-object v0, p0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/mapcore/i;->o:Z

    .line 13
    return-void
.end method

.method public static a()Lcom/amap/api/mapcore/i;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/amap/api/mapcore/i;

    invoke-direct {v0}, Lcom/amap/api/mapcore/i;-><init>()V

    .line 62
    return-object v0
.end method

.method public static a(F)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 131
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 133
    sget-object v1, Lcom/amap/api/mapcore/i$a;->h:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 134
    iput p0, v0, Lcom/amap/api/mapcore/i;->d:F

    .line 135
    return-object v0
.end method

.method public static a(FF)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 98
    sget-object v1, Lcom/amap/api/mapcore/i$a;->j:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 99
    iput p0, v0, Lcom/amap/api/mapcore/i;->b:F

    .line 100
    iput p1, v0, Lcom/amap/api/mapcore/i;->c:F

    .line 101
    return-object v0
.end method

.method public static a(FLandroid/graphics/Point;)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 143
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 145
    sget-object v1, Lcom/amap/api/mapcore/i$a;->i:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 146
    iput p0, v0, Lcom/amap/api/mapcore/i;->e:F

    .line 147
    iput-object p1, v0, Lcom/amap/api/mapcore/i;->m:Landroid/graphics/Point;

    .line 148
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 155
    sget-object v1, Lcom/amap/api/mapcore/i$a;->k:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 156
    iput-object p0, v0, Lcom/amap/api/mapcore/i;->h:Lcom/amap/api/maps/model/CameraPosition;

    .line 157
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/mapcore/i;
    .locals 1

    .prologue
    .line 220
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/mapcore/i;
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/LatLng;FFF)Lcom/amap/api/mapcore/i;
    .locals 1

    .prologue
    .line 232
    invoke-static {}, Lcom/amap/api/maps/model/CameraPosition;->builder()Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->target(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/amap/api/maps/model/CameraPosition$Builder;->bearing(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/amap/api/maps/model/CameraPosition$Builder;->tilt(F)Lcom/amap/api/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps/model/CameraPosition$Builder;->build()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/mapcore/i;->a(Lcom/amap/api/maps/model/CameraPosition;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/LatLngBounds;I)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 250
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 252
    sget-object v1, Lcom/amap/api/mapcore/i$a;->l:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 253
    iput-object p0, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 254
    iput p1, v0, Lcom/amap/api/mapcore/i;->j:I

    .line 255
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps/model/LatLngBounds;III)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 260
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 262
    sget-object v1, Lcom/amap/api/mapcore/i$a;->m:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 263
    iput-object p0, v0, Lcom/amap/api/mapcore/i;->i:Lcom/amap/api/maps/model/LatLngBounds;

    .line 264
    iput p3, v0, Lcom/amap/api/mapcore/i;->j:I

    .line 265
    iput p1, v0, Lcom/amap/api/mapcore/i;->k:I

    .line 266
    iput p2, v0, Lcom/amap/api/mapcore/i;->l:I

    .line 267
    return-object v0
.end method

.method public static a(Lcom/autonavi/amap/mapcore/IPoint;)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 167
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 169
    sget-object v1, Lcom/amap/api/mapcore/i$a;->c:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 170
    iput-object p0, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    .line 171
    return-object v0
.end method

.method static a(Lcom/autonavi/amap/mapcore/IPoint;FFF)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 240
    sget-object v1, Lcom/amap/api/mapcore/i$a;->n:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 241
    iput-object p0, v0, Lcom/amap/api/mapcore/i;->n:Lcom/autonavi/amap/mapcore/IPoint;

    .line 242
    iput p1, v0, Lcom/amap/api/mapcore/i;->d:F

    .line 243
    iput p2, v0, Lcom/amap/api/mapcore/i;->g:F

    .line 244
    iput p3, v0, Lcom/amap/api/mapcore/i;->f:F

    .line 245
    return-object v0
.end method

.method public static b()Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 68
    sget-object v1, Lcom/amap/api/mapcore/i$a;->b:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 69
    return-object v0
.end method

.method public static b(F)Lcom/amap/api/mapcore/i;
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/amap/api/mapcore/i;->a(FLandroid/graphics/Point;)Lcom/amap/api/mapcore/i;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/amap/api/mapcore/i$a;->g:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 91
    return-object v0
.end method

.method public static c(F)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 183
    sget-object v1, Lcom/amap/api/mapcore/i$a;->d:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 184
    iput p0, v0, Lcom/amap/api/mapcore/i;->f:F

    .line 185
    return-object v0
.end method

.method public static d(F)Lcom/amap/api/mapcore/i;
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Lcom/amap/api/mapcore/i;->a()Lcom/amap/api/mapcore/i;

    move-result-object v0

    .line 197
    sget-object v1, Lcom/amap/api/mapcore/i$a;->e:Lcom/amap/api/mapcore/i$a;

    iput-object v1, v0, Lcom/amap/api/mapcore/i;->a:Lcom/amap/api/mapcore/i$a;

    .line 198
    iput p0, v0, Lcom/amap/api/mapcore/i;->g:F

    .line 199
    return-object v0
.end method
