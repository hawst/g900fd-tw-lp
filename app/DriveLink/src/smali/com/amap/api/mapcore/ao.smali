.class public Lcom/amap/api/mapcore/ao;
.super Landroid/view/View;
.source "TileOverlayView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/mapcore/ao$a;
    }
.end annotation


# instance fields
.field a:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/mapcore/z;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/amap/api/mapcore/ao$a;

.field c:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/amap/api/mapcore/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/mapcore/q;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 17
    new-instance v0, Lcom/amap/api/mapcore/ao$a;

    invoke-direct {v0, p0}, Lcom/amap/api/mapcore/ao$a;-><init>(Lcom/amap/api/mapcore/ao;)V

    iput-object v0, p0, Lcom/amap/api/mapcore/ao;->b:Lcom/amap/api/mapcore/ao$a;

    .line 18
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/mapcore/ao;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 42
    iput-object p2, p0, Lcom/amap/api/mapcore/ao;->d:Lcom/amap/api/mapcore/q;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-interface {v0}, Lcom/amap/api/mapcore/z;->a()V

    goto :goto_0

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 77
    return-void
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 51
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Lcom/amap/api/mapcore/b/h;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 54
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 55
    invoke-interface {v0}, Lcom/amap/api/mapcore/z;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/z;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_1

    .line 59
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 100
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/mapcore/z;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/z;->b(Z)V

    goto :goto_0

    .line 104
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v0}, Lcom/amap/api/mapcore/z;->g()V

    goto :goto_0

    .line 112
    :cond_1
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 124
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {v0, p1}, Lcom/amap/api/mapcore/z;->c(Z)V

    goto :goto_0

    .line 128
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/amap/api/mapcore/ao;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/mapcore/z;

    .line 116
    if-eqz v0, :cond_0

    .line 117
    invoke-interface {v0}, Lcom/amap/api/mapcore/z;->h()V

    goto :goto_0

    .line 120
    :cond_1
    return-void
.end method
