.class public Lcom/amap/api/mapcore/ag$a;
.super Landroid/view/ViewGroup$LayoutParams;
.source "MapOverlayViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/mapcore/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/amap/api/maps/model/LatLng;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(IILcom/amap/api/maps/model/LatLng;III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->a:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 40
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->c:I

    .line 41
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->d:I

    .line 42
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->e:I

    .line 51
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->a:I

    .line 52
    iput-object p3, p0, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 53
    iput p4, p0, Lcom/amap/api/mapcore/ag$a;->c:I

    .line 54
    iput p5, p0, Lcom/amap/api/mapcore/ag$a;->d:I

    .line 55
    iput p6, p0, Lcom/amap/api/mapcore/ag$a;->e:I

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->a:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 40
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->c:I

    .line 41
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->d:I

    .line 42
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->e:I

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->a:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/mapcore/ag$a;->b:Lcom/amap/api/maps/model/LatLng;

    .line 40
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->c:I

    .line 41
    iput v1, p0, Lcom/amap/api/mapcore/ag$a;->d:I

    .line 42
    const/16 v0, 0x33

    iput v0, p0, Lcom/amap/api/mapcore/ag$a;->e:I

    .line 71
    return-void
.end method
