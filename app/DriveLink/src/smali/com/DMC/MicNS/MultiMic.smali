.class public Lcom/DMC/MicNS/MultiMic;
.super Ljava/lang/Object;
.source "MultiMic.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init()V
    .locals 3

    .prologue
    .line 18
    :try_start_0
    const-string/jumbo v1, "DMC_2CH_NS"

    const-string/jumbo v2, "Trying to load libDMC_2CH_NS.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    const-string/jumbo v1, "DMC_2CH_NS"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 20
    const-string/jumbo v1, "DMC_2CH_NS"

    const-string/jumbo v2, "Loading libDMC_2CH_NS.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 23
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 25
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "DMC_2CH_NS"

    const-string/jumbo v2, "WARNING: Could not load libDMC_2CH_NS.so"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public native GetVersion()I
.end method

.method public native NSFree()I
.end method

.method public native NSInit(III)I
.end method

.method public native processNSFrame([S[SS)I
.end method
