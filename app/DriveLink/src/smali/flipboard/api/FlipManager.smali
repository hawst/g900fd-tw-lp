.class public Lflipboard/api/FlipManager;
.super Ljava/lang/Object;
.source "FlipManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflipboard/api/FlipManager$1;,
        Lflipboard/api/FlipManager$ErrorMessage;,
        Lflipboard/api/FlipManager$ContentType;,
        Lflipboard/api/FlipManager$AppType;
    }
.end annotation


# static fields
.field private static final FLIPBOARD_CONTENT_URI:Ljava/lang/String; = "content://flipboard.remoteservice.feeds/feeditems"

.field private static final FLIPBOARD_CONTENT_URI_CHINA:Ljava/lang/String; = "content://flipboard.remoteservice.feeds.china/feeditems"

.field private static final FLIPBOARD_CONTENT_URI_DEBUG:Ljava/lang/String; = "content://flipboard.remoteservice.feeds.debug/feeditems"

.field static final INSTANCE:Lflipboard/api/FlipManager;

.field public static final TAG:Ljava/lang/String; = "FlipboardLibrary"

.field static mContext:Landroid/content/Context;

.field private static mIsFlipboardCompatible:Z

.field private static mMessenger:Lflipboard/api/MessengerProvider;


# instance fields
.field appType:Lflipboard/api/FlipManager$AppType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lflipboard/api/FlipManager;

    invoke-direct {v0}, Lflipboard/api/FlipManager;-><init>()V

    sput-object v0, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    .line 79
    const/4 v0, 0x0

    sput-boolean v0, Lflipboard/api/FlipManager;->mIsFlipboardCompatible:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lflipboard/api/FlipManager$AppType;->NONE:Lflipboard/api/FlipManager$AppType;

    iput-object v0, p0, Lflipboard/api/FlipManager;->appType:Lflipboard/api/FlipManager$AppType;

    .line 84
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lflipboard/api/FlipManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lflipboard/api/FlipManager;->mContext:Landroid/content/Context;

    .line 95
    sget-object v0, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    .line 100
    :goto_0
    return-object v0

    .line 98
    :cond_0
    new-instance v0, Lflipboard/api/MessengerProvider;

    sget-object v1, Lflipboard/api/FlipManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lflipboard/api/MessengerProvider;-><init>(Landroid/content/Context;)V

    sput-object v0, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    .line 100
    sget-object v0, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    goto :goto_0
.end method


# virtual methods
.method public fetchFlipboardItems(Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V
    .locals 7
    .param p1, "authToken"    # Ljava/lang/String;
    .param p2, "categoryId"    # Ljava/lang/String;
    .param p3, "contentType"    # Lflipboard/api/FlipManager$ContentType;
    .param p4, "locale"    # Ljava/util/Locale;
    .param p5, "listener"    # Lflipboard/api/FlipFetchListener;

    .prologue
    .line 119
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    invoke-virtual {p4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 120
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 123
    :cond_1
    sget-object v0, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lflipboard/api/MessengerProvider;->getFlipFeeds(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V

    .line 124
    return-void
.end method

.method public fetchMoreFlipboardItems(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V
    .locals 7
    .param p1, "authToken"    # Ljava/lang/String;
    .param p2, "categoryId"    # Ljava/lang/String;
    .param p3, "pageKey"    # Ljava/lang/String;
    .param p4, "contentType"    # Lflipboard/api/FlipManager$ContentType;
    .param p5, "locale"    # Ljava/util/Locale;
    .param p6, "listener"    # Lflipboard/api/FlipFetchListener;

    .prologue
    .line 144
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    invoke-virtual {p5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 147
    :cond_1
    sget-object v0, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lflipboard/api/MessengerProvider;->getFlipFeeds(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V

    .line 148
    return-void
.end method

.method public getItemsForFeed(Ljava/lang/String;Ljava/util/Locale;Lflipboard/api/FlipManager$ContentType;)Ljava/util/List;
    .locals 19
    .param p1, "categoryId"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "contentType"    # Lflipboard/api/FlipManager$ContentType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Lflipboard/api/FlipManager$ContentType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lflipboard/api/FlipboardItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    const-string/jumbo v8, ""

    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    .line 164
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 169
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lflipboard/api/FlipManager;->appType:Lflipboard/api/FlipManager$AppType;

    invoke-virtual {v8}, Lflipboard/api/FlipManager$AppType;->getContentUri()Ljava/lang/String;

    move-result-object v18

    .line 170
    .local v18, "uriString":Ljava/lang/String;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v16, "flitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lflipboard/api/FlipboardItem;>;"
    if-eqz v18, :cond_3

    .line 172
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 173
    .local v2, "flipboardFeedItemUri":Landroid/net/Uri;
    sget-object v8, Lflipboard/api/FlipManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    .line 176
    .local v1, "cpClient":Landroid/content/ContentProviderClient;
    if-eqz v1, :cond_3

    .line 178
    const/4 v3, 0x0

    :try_start_0
    const-string/jumbo v4, "sectionId = ? AND locale = ? AND country = ? AND contentType = ? "

    const/4 v8, 0x4

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v8

    const/4 v8, 0x2

    invoke-virtual/range {p2 .. p2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v8

    const/4 v8, 0x3

    invoke-virtual/range {p3 .. p3}, Lflipboard/api/FlipManager$ContentType;->name()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v8

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 179
    .local v14, "cursor":Landroid/database/Cursor;
    if-eqz v14, :cond_3

    .line 180
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_3

    .line 184
    const/4 v8, 0x0

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 185
    .local v4, "id":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 186
    .local v5, "title":Ljava/lang/String;
    const/4 v8, 0x2

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 187
    .local v6, "text":Ljava/lang/String;
    const/4 v8, 0x3

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 189
    .local v7, "author":Ljava/lang/String;
    const/4 v8, 0x4

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 190
    .local v17, "type":Ljava/lang/String;
    const/4 v8, 0x5

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 191
    .local v11, "sourceUrl":Ljava/lang/String;
    const/4 v8, 0x6

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 192
    .local v12, "pageKey":Ljava/lang/String;
    const/4 v8, 0x7

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 194
    .local v9, "timestamp":J
    sget-object v8, Lflipboard/api/FlipManager$ContentType;->TEXT:Lflipboard/api/FlipManager$ContentType;

    move-object/from16 v0, p3

    if-ne v0, v8, :cond_4

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_4

    .line 195
    :cond_2
    const-string/jumbo v8, "FlipboardLibrary"

    const-string/jumbo v13, "Skipping article because text is missing"

    invoke-static {v8, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "text":Ljava/lang/String;
    .end local v7    # "author":Ljava/lang/String;
    .end local v9    # "timestamp":J
    .end local v11    # "sourceUrl":Ljava/lang/String;
    .end local v12    # "pageKey":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v17    # "type":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 207
    .local v15, "e":Landroid/os/RemoteException;
    const-string/jumbo v8, "FlipboardLibrary"

    invoke-virtual {v15}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-static {v8, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    .end local v1    # "cpClient":Landroid/content/ContentProviderClient;
    .end local v2    # "flipboardFeedItemUri":Landroid/net/Uri;
    .end local v15    # "e":Landroid/os/RemoteException;
    :cond_3
    return-object v16

    .line 196
    .restart local v1    # "cpClient":Landroid/content/ContentProviderClient;
    .restart local v2    # "flipboardFeedItemUri":Landroid/net/Uri;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v6    # "text":Ljava/lang/String;
    .restart local v7    # "author":Ljava/lang/String;
    .restart local v9    # "timestamp":J
    .restart local v11    # "sourceUrl":Ljava/lang/String;
    .restart local v12    # "pageKey":Ljava/lang/String;
    .restart local v14    # "cursor":Landroid/database/Cursor;
    .restart local v17    # "type":Ljava/lang/String;
    :cond_4
    :try_start_1
    sget-object v8, Lflipboard/api/FlipManager$ContentType;->AUDIO:Lflipboard/api/FlipManager$ContentType;

    move-object/from16 v0, p3

    if-ne v0, v8, :cond_6

    if-eqz v11, :cond_5

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_6

    .line 197
    :cond_5
    const-string/jumbo v8, "FlipboardLibrary"

    const-string/jumbo v13, "Skipping audio because url is missing"

    invoke-static {v8, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 199
    :cond_6
    new-instance v3, Lflipboard/api/FlipboardItem;

    move-object/from16 v8, p3

    move-object/from16 v13, p1

    invoke-direct/range {v3 .. v13}, Lflipboard/api/FlipboardItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .local v3, "item":Lflipboard/api/FlipboardItem;
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public isFlipboardAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 220
    sget-boolean v1, Lflipboard/api/FlipManager;->mIsFlipboardCompatible:Z

    if-nez v1, :cond_0

    .line 221
    sget-object v1, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    invoke-virtual {v1}, Lflipboard/api/MessengerProvider;->connectServer()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    sget-object v1, Lflipboard/api/FlipManager;->mMessenger:Lflipboard/api/MessengerProvider;

    invoke-virtual {v1}, Lflipboard/api/MessengerProvider;->disconnectServer()Z

    .line 223
    sput-boolean v0, Lflipboard/api/FlipManager;->mIsFlipboardCompatible:Z

    .line 224
    sget-boolean v0, Lflipboard/api/FlipManager;->mIsFlipboardCompatible:Z

    .line 228
    :cond_0
    :goto_0
    return v0

    .line 226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
