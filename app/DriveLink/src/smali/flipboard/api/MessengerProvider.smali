.class Lflipboard/api/MessengerProvider;
.super Ljava/lang/Object;
.source "MessengerProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflipboard/api/MessengerProvider$IncomingHandler;,
        Lflipboard/api/MessengerProvider$Request;
    }
.end annotation


# static fields
.field private static final LIB_VERSION:Ljava/lang/String; = "1.861"

.field static final MSG_REGISTER_CLIENT:I = 0x1

.field static final MSG_TRIGGER_UPDATE:I = 0x3

.field static final MSG_UNREGISTER_CLIENT:I = 0x2

.field private static final REMOTE_SERVICE:Ljava/lang/String; = "flipboard.service.MessengerService"

.field private static final REMOTE_SERVICE_CHINA:Ljava/lang/String; = "flipboard.service.MessengerService.china"

.field private static final REMOTE_SERVICE_DEBUG:Ljava/lang/String; = "flipboard.service.MessengerService.debug"

.field private static final TAG:Ljava/lang/String; = "FlipboardLibrary"

.field static mFeedUpdateListener:Lflipboard/api/FlipFetchListener;


# instance fields
.field private mConnection:Landroid/content/ServiceConnection;

.field final mContext:Landroid/content/Context;

.field mCurrentRequest:Landroid/os/Bundle;

.field mIsBound:Z

.field final mMessenger:Landroid/os/Messenger;

.field mService:Landroid/os/Messenger;

.field final userRequests:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/api/MessengerProvider$Request;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lflipboard/api/MessengerProvider;->mFeedUpdateListener:Lflipboard/api/FlipFetchListener;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    .line 135
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lflipboard/api/MessengerProvider$IncomingHandler;

    invoke-direct {v1, p0}, Lflipboard/api/MessengerProvider$IncomingHandler;-><init>(Lflipboard/api/MessengerProvider;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lflipboard/api/MessengerProvider;->mMessenger:Landroid/os/Messenger;

    .line 140
    new-instance v0, Lflipboard/api/MessengerProvider$1;

    invoke-direct {v0, p0}, Lflipboard/api/MessengerProvider$1;-><init>(Lflipboard/api/MessengerProvider;)V

    iput-object v0, p0, Lflipboard/api/MessengerProvider;->mConnection:Landroid/content/ServiceConnection;

    .line 74
    iput-object p1, p0, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lflipboard/api/MessengerProvider;ILandroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lflipboard/api/MessengerProvider;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lflipboard/api/MessengerProvider;->sendMessage(ILandroid/os/Bundle;)V

    return-void
.end method

.method private isValidToken(Ljava/lang/String;)Z
    .locals 1
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method private sendMessage(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 223
    iget-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-nez v1, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 229
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 230
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mMessenger:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 232
    if-eqz p2, :cond_2

    .line 233
    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 236
    :cond_2
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 237
    .end local v0    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method connectServer()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 177
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "flipboard.service.MessengerService.debug"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v0, "remoteMessengerService":Landroid/content/Intent;
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lflipboard/api/MessengerProvider;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    .line 179
    iget-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-nez v1, :cond_2

    .line 180
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "remoteMessengerService":Landroid/content/Intent;
    const-string/jumbo v1, "flipboard.service.MessengerService.china"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .restart local v0    # "remoteMessengerService":Landroid/content/Intent;
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lflipboard/api/MessengerProvider;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    .line 182
    iget-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-nez v1, :cond_1

    .line 183
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "remoteMessengerService":Landroid/content/Intent;
    const-string/jumbo v1, "flipboard.service.MessengerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 184
    .restart local v0    # "remoteMessengerService":Landroid/content/Intent;
    iget-object v1, p0, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lflipboard/api/MessengerProvider;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    .line 185
    iget-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-eqz v1, :cond_0

    .line 186
    sget-object v1, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    sget-object v2, Lflipboard/api/FlipManager$AppType;->REGULAR:Lflipboard/api/FlipManager$AppType;

    iput-object v2, v1, Lflipboard/api/FlipManager;->appType:Lflipboard/api/FlipManager$AppType;

    .line 194
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    return v1

    .line 189
    :cond_1
    sget-object v1, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    sget-object v2, Lflipboard/api/FlipManager$AppType;->CHINA:Lflipboard/api/FlipManager$AppType;

    iput-object v2, v1, Lflipboard/api/FlipManager;->appType:Lflipboard/api/FlipManager$AppType;

    goto :goto_0

    .line 192
    :cond_2
    sget-object v1, Lflipboard/api/FlipManager;->INSTANCE:Lflipboard/api/FlipManager;

    sget-object v2, Lflipboard/api/FlipManager$AppType;->DEBUG:Lflipboard/api/FlipManager$AppType;

    iput-object v2, v1, Lflipboard/api/FlipManager;->appType:Lflipboard/api/FlipManager$AppType;

    goto :goto_0
.end method

.method disconnectServer()Z
    .locals 5

    .prologue
    .line 199
    iget-object v3, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    monitor-enter v3

    .line 200
    :try_start_0
    iget-boolean v2, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-eqz v2, :cond_0

    .line 203
    const/4 v2, 0x2

    const/4 v4, 0x0

    invoke-direct {p0, v2, v4}, Lflipboard/api/MessengerProvider;->sendMessage(ILandroid/os/Bundle;)V

    .line 205
    iget-object v2, p0, Lflipboard/api/MessengerProvider;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lflipboard/api/MessengerProvider;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 206
    const/4 v2, 0x0

    iput-boolean v2, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    .line 207
    const-string/jumbo v2, "FlipboardLibrary"

    const-string/jumbo v4, "Unbinding"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    iget-object v2, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/api/MessengerProvider$Request;

    .line 210
    .local v1, "request":Lflipboard/api/MessengerProvider$Request;
    iget-object v2, v1, Lflipboard/api/MessengerProvider$Request;->mListener:Lflipboard/api/FlipFetchListener;

    sget-object v4, Lflipboard/api/FlipManager$ErrorMessage;->DISCONNECTED_FROM_FLIPBOARD:Lflipboard/api/FlipManager$ErrorMessage;

    invoke-interface {v2, v4}, Lflipboard/api/FlipFetchListener;->onFailure(Lflipboard/api/FlipManager$ErrorMessage;)V

    goto :goto_0

    .line 215
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "request":Lflipboard/api/MessengerProvider$Request;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 212
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, p0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    .line 213
    iget-object v2, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 214
    iget-boolean v2, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v2
.end method

.method getFlipFeeds(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;Ljava/util/Locale;Lflipboard/api/FlipFetchListener;)V
    .locals 8
    .param p1, "token"    # Ljava/lang/String;
    .param p2, "categoryId"    # Ljava/lang/String;
    .param p3, "pageKey"    # Ljava/lang/String;
    .param p4, "contentType"    # Lflipboard/api/FlipManager$ContentType;
    .param p5, "locale"    # Ljava/util/Locale;
    .param p6, "listener"    # Lflipboard/api/FlipFetchListener;

    .prologue
    .line 256
    iget-object v6, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    monitor-enter v6

    .line 257
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, "_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, "_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p4}, Lflipboard/api/FlipManager$ContentType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 258
    .local v2, "key":Ljava/lang/String;
    iget-object v5, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 259
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 261
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v5, "token"

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string/jumbo v5, "categoryId"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string/jumbo v5, "pageKey"

    invoke-virtual {v0, v5, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string/jumbo v5, "contentType"

    invoke-virtual {p4}, Lflipboard/api/FlipManager$ContentType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string/jumbo v5, "language"

    invoke-virtual {p5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string/jumbo v5, "country"

    invoke-virtual {p5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v4, Lflipboard/api/MessengerProvider$Request;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v0, p6, v5}, Lflipboard/api/MessengerProvider$Request;-><init>(Lflipboard/api/MessengerProvider;Landroid/os/Bundle;Lflipboard/api/FlipFetchListener;Z)V

    .line 270
    .local v4, "request":Lflipboard/api/MessengerProvider$Request;
    iget-object v5, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    iget-object v5, p0, Lflipboard/api/MessengerProvider;->mService:Landroid/os/Messenger;

    if-nez v5, :cond_1

    .line 273
    invoke-virtual {p0}, Lflipboard/api/MessengerProvider;->connectServer()Z

    .line 274
    iget-boolean v5, p0, Lflipboard/api/MessengerProvider;->mIsBound:Z

    if-eqz v5, :cond_0

    .line 275
    const-string/jumbo v5, "FlipboardLibrary"

    const-string/jumbo v7, "Binding"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v4    # "request":Lflipboard/api/MessengerProvider$Request;
    :cond_0
    :goto_0
    monitor-exit v6

    .line 291
    return-void

    .line 279
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v4    # "request":Lflipboard/api/MessengerProvider$Request;
    :cond_1
    iget-object v5, p0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/api/MessengerProvider$Request;

    .line 280
    .local v3, "req":Lflipboard/api/MessengerProvider$Request;
    iget-boolean v5, v3, Lflipboard/api/MessengerProvider$Request;->mIsSent:Z

    if-nez v5, :cond_2

    .line 281
    const/4 v5, 0x3

    iget-object v7, v3, Lflipboard/api/MessengerProvider$Request;->mBundle:Landroid/os/Bundle;

    invoke-direct {p0, v5, v7}, Lflipboard/api/MessengerProvider;->sendMessage(ILandroid/os/Bundle;)V

    .line 282
    const/4 v5, 0x1

    iput-boolean v5, v3, Lflipboard/api/MessengerProvider$Request;->mIsSent:Z

    goto :goto_1

    .line 290
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "req":Lflipboard/api/MessengerProvider$Request;
    .end local v4    # "request":Lflipboard/api/MessengerProvider$Request;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 288
    .restart local v2    # "key":Ljava/lang/String;
    :cond_3
    :try_start_1
    sget-object v5, Lflipboard/api/FlipManager$ErrorMessage;->DUPLICATE_REQUEST:Lflipboard/api/FlipManager$ErrorMessage;

    invoke-interface {p6, v5}, Lflipboard/api/FlipFetchListener;->onFailure(Lflipboard/api/FlipManager$ErrorMessage;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
