.class public Lflipboard/api/FlipboardItem;
.super Ljava/lang/Object;
.source "FlipboardItem.java"


# instance fields
.field private author:Ljava/lang/String;

.field private categoryId:Ljava/lang/String;

.field private contentType:Lflipboard/api/FlipManager$ContentType;

.field private id:Ljava/lang/String;

.field private pageKey:Ljava/lang/String;

.field private sourceUrl:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private timeCreated:J

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/api/FlipManager$ContentType;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "contentType"    # Lflipboard/api/FlipManager$ContentType;
    .param p6, "timeCreated"    # J
    .param p8, "sourceUrl"    # Ljava/lang/String;
    .param p9, "pageKey"    # Ljava/lang/String;
    .param p10, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lflipboard/api/FlipboardItem;->id:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lflipboard/api/FlipboardItem;->title:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lflipboard/api/FlipboardItem;->text:Ljava/lang/String;

    .line 24
    iput-object p4, p0, Lflipboard/api/FlipboardItem;->author:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lflipboard/api/FlipboardItem;->contentType:Lflipboard/api/FlipManager$ContentType;

    .line 26
    iput-wide p6, p0, Lflipboard/api/FlipboardItem;->timeCreated:J

    .line 27
    iput-object p8, p0, Lflipboard/api/FlipboardItem;->sourceUrl:Ljava/lang/String;

    .line 28
    iput-object p9, p0, Lflipboard/api/FlipboardItem;->pageKey:Ljava/lang/String;

    .line 29
    iput-object p10, p0, Lflipboard/api/FlipboardItem;->categoryId:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->author:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Lflipboard/api/FlipManager$ContentType;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->contentType:Lflipboard/api/FlipManager$ContentType;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getPageKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->pageKey:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->sourceUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lflipboard/api/FlipboardItem;->timeCreated:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/api/FlipboardItem;->title:Ljava/lang/String;

    return-object v0
.end method
