.class Lflipboard/api/MessengerProvider$IncomingHandler;
.super Landroid/os/Handler;
.source "MessengerProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflipboard/api/MessengerProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lflipboard/api/MessengerProvider;


# direct methods
.method constructor <init>(Lflipboard/api/MessengerProvider;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private onFeedItemUpdate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 108
    iget-object v5, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v6, v5, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    monitor-enter v6

    .line 110
    :try_start_0
    const-string/jumbo v5, "result"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 111
    .local v4, "result":Z
    const-string/jumbo v5, "FlipboardLibrary"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Received from service: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v5, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v5, v5, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    const-string/jumbo v7, "responseKey"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/api/MessengerProvider$Request;

    .line 113
    .local v3, "request":Lflipboard/api/MessengerProvider$Request;
    if-nez v3, :cond_0

    .line 114
    monitor-exit v6

    .line 129
    :goto_0
    return-void

    .line 116
    :cond_0
    if-eqz v4, :cond_1

    .line 117
    iget-object v5, v3, Lflipboard/api/MessengerProvider$Request;->mListener:Lflipboard/api/FlipFetchListener;

    invoke-interface {v5, v4}, Lflipboard/api/FlipFetchListener;->onSuccess(Z)V

    .line 128
    :goto_1
    monitor-exit v6

    goto :goto_0

    .end local v3    # "request":Lflipboard/api/MessengerProvider$Request;
    .end local v4    # "result":Z
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 119
    .restart local v3    # "request":Lflipboard/api/MessengerProvider$Request;
    .restart local v4    # "result":Z
    :cond_1
    :try_start_1
    const-string/jumbo v5, "errorMessage"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "errorMessage":Ljava/lang/String;
    sget-object v2, Lflipboard/api/FlipManager$ErrorMessage;->UNKNOWN:Lflipboard/api/FlipManager$ErrorMessage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    .local v2, "msg":Lflipboard/api/FlipManager$ErrorMessage;
    :try_start_2
    invoke-static {v1}, Lflipboard/api/FlipManager$ErrorMessage;->valueOf(Ljava/lang/String;)Lflipboard/api/FlipManager$ErrorMessage;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 126
    :goto_2
    :try_start_3
    iget-object v5, v3, Lflipboard/api/MessengerProvider$Request;->mListener:Lflipboard/api/FlipFetchListener;

    invoke-interface {v5, v2}, Lflipboard/api/FlipFetchListener;->onFailure(Lflipboard/api/FlipManager$ErrorMessage;)V

    goto :goto_1

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "FlipboardLibrary"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Invalid error code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", using UNKNOWN instead"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 86
    iget-object v0, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v1, v0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    monitor-enter v1

    .line 87
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 96
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 98
    :cond_0
    :goto_0
    monitor-exit v1

    .line 99
    return-void

    .line 89
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lflipboard/api/MessengerProvider$IncomingHandler;->onFeedItemUpdate(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v0, v0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "responseKey"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    iget-object v0, v0, Lflipboard/api/MessengerProvider;->userRequests:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lflipboard/api/MessengerProvider$IncomingHandler;->this$0:Lflipboard/api/MessengerProvider;

    invoke-virtual {v0}, Lflipboard/api/MessengerProvider;->disconnectServer()Z

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
