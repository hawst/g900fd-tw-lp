﻿<b>CONDIZIONI PER L'UTILIZZO DEL SERVIZIO MODALITÀ AUTO</b>
<br/>
<br/>
<br/>PRIMA DI UTILIZZARE IL SERVIZIO LEGGERE ATTENTAMENTE LE SEGUENTI CONDIZIONI PER L'UTILIZZO DEL SERVIZIO.
<br/>Il Servizio modalità Auto ("Servizio") offre un servizio per l'uso in automobile che aiuta gli utenti a utilizzare le funzionalità degli smartphone come chiamate, messaggi, navigazione e ascolto di musica in sicurezza e comodità. Le presenti Condizioni per l'utilizzo del servizio regolano il rapporto giuridico tra Samsung Electronics Co., Ltd e le sue controllate (collettivamente denominate “Samsung“ o “noi“) e l'utente del Servizio. 
<br/>
<br/>1. Accettazione delle Condizioni per l'utilizzo del servizio modalità Auto
<br/>
<br/>1.1	UTILIZZANDO IL SERVIZIO, L'UTENTE ACCETTA DI ESSERE VINCOLATO ALLE PRESENTI CONDIZIONI PER L'UTILIZZO DEL SERVIZIO. SE NON SI ACCETTANO LE CONDIZIONI PER L'UTILIZZO DEL SERVIZIO, NON UTILIZZARE IL SERVIZIO. 
<br/>
<br/>1.2 I SERVIZI NON SONO STATI PROGETTATI PER L'UTILIZZO DA PARTE DI SOGGETTI DI ETÀ INFERIORE A 13 ANNI. AGLI UTENTI DI ETÀ INFERIORE A 13 ANNI NON È CONSENTITO UTILIZZARE I SERVIZI O FORNIRE A SAMSUNG INFORMAZIONI PERSONALI CHE POSSANO IDENTIFICARE L'UTENTE. Nel caso in cui l'utente abbia 13 anni o un'età maggiore, ma al di sotto dei 18 anni, è necessario dichiarare di aver preso atto dei presenti Termini e condizioni insieme ai propri genitori o con un tutore legale, al fine di confermare che l'utente e i suoi genitori o il suo tutore legale comprendano i presenti Termini e condizioni. Nel caso in cui un genitore e/o un tutore legale permettano a una persona minorenne nel proprio Paese di residenza (un "minore") di utilizzare i Servizi, gli stessi accettano di: (i) esercitare la supervisione sull'utilizzo dei Servizi da parte del minore; (ii) assumersi ogni rischio associato all'utilizzo da parte del minore dei Contenuti o di altri contenuti ricevuti attraverso i Servizi, compresa la trasmissione di materiali, contenuti o altre informazioni verso e da terze parti tramite Internet; (iii) assumersi le responsabilità derivanti dall'utilizzo dei Servizi da parte del minore, compresi gli obblighi relativi ai pagamenti; e (iv) di assicurarsi della veridicità e dell'accuratezza di tutte le informazioni fornite dal minore.
<br/>
<br/>2. Fornitura dei Servizi di Samsung
<br/>
<br/>2.1 Samsung concede all'utente licenza e diritti non esclusivi, non trasferibili e limitati per l'accesso ai Servizi e il relativo utilizzo a scopo strettamente personale, a patto che l'utente rispetti appieno le Condizioni per l'utilizzo del servizio. All'utente non è consentito interferire o tentare di interferire con il funzionamento o l'utilizzo dei Servizi da parte di terzi in qualsiasi modo o con qualsiasi mezzo o dispositivo inclusi, in via esemplificativa, lo spamming, l'intrusione, il caricamento di virus informatici o sistemi di disattivazione a tempo, o con qualsiasi altro mezzo. SAMSUNG SI RISERVA IL DIRITTO, A SUA ESCLUSIVA DISCREZIONE, DI CAMBIARE, MODIFICARE, AGGIORNARE, AGGIUNGERE, SOSPENDERE, RIMUOVERE, REVISIONARE, CANCELLARE O MODIFICARE IN ALTRO MODO QUALUNQUE PARTE DELLE PRESENTI CONDIZIONI PER L'UTILIZZO DEL SERVIZIO MODALITÀ AUTO, CON O SENZA PREAVVISO ALL'UTENTE. L'UTILIZZO CONTINUATO DEI SERVIZI SAMSUNG SI RITERRÀ COSTITUIRE L'ACCETTAZIONE DA PARTE DELL'UTENTE DI TALI MODIFICHE. 
<br/>
<br/>2.2 Qualora l'utente non si attenga, o Samsung ne abbia sospetto, a una qualunque delle clausole di queste Condizioni per l'utilizzo del servizio, senza limitare i diritti e i rimedi giuridici di Samsung, ciascuno dei quali è espressamente riservato, Samsung, a sua sola discrezione e senza alcun preavviso all'utente, può: (i) sospendere l'utilizzo dei Servizi da parte dell'utente, il quale rimane responsabile di tutti gli importi dovuti fino alla data di risoluzione inclusa; e/o (ii) sospendere le licenze concesse in virtù del presente documento; e/o (iii) impedire l'accesso ai Servizi e/o ai Contenuti, per intero o in parte.
<br/>
<br/>2.3 L'utente dichiara di essere a conoscenza e di accettare che i Servizi possono essere forniti da società affiliate o subappaltatori di Samsung in nome e per conto di Samsung. L'utente accetta che qualunque terzo che fornisca a Samsung i contenuti per la distribuzione attraverso i Servizi sia ritenuto con il presente un beneficiario terzo autorizzato delle presente Condizioni per l'utilizzo del servizio, e che avrà il diritto di applicare tutti gli obblighi degli utenti dei Servizi ai sensi delle presenti Condizioni per l'utilizzo del servizio nella stessa misura nel caso in cui tale fornitore di contenuti terzo fosse una parte delle presenti Condizioni per l'utilizzo del servizio e a tutti gli effetti, ai sensi delle norme di legge o secondo equità. 
<br/>
<br/>2.4 L'utilizzo dei Servizi richiede uno o più dispositivi compatibili, l'accesso o la connessione alla rete mobile o a Internet (il che potrebbe comportare l'applicazione delle tariffe del caso) e a determinati software (il che potrebbe comportare l'applicazione delle tariffe del caso); potrebbe, inoltre, essere necessario effettuare upgrade e aggiornamenti periodici. Poiché l'utilizzo dei Servizi coinvolge hardware, software e accesso a Internet, la capacità dell'utente di utilizzare i Servizi potrebbe essere influenzata dal funzionamento di tali fattori e dal proprio ambiente. L'utente dichiara di essere a conoscenza e di accettare che tali requisiti di sistema, che potrebbero essere modificati periodicamente, sono di sua responsabilità. 
<br/>
<br/>3. Utilizzo dei Servizi da parte dell'utente
<br/>
<br/>3.1 L'UTENTE GARANTISCE CHE RISPETTERÀ SEMPRE TUTTE LE LEGGI E LE NORMATIVE IN MATERIA DI UTILIZZO DI DISPOSITIVI MOBILI DURANTE LA GUIDA. L'UTENTE DICHIARA DI ESSERE A CONOSCENZA E DI ACCETTARE CHE LA SICUREZZA È IL SUO DOVERE PIÙ IMPORTANTE. L'UTENTE DICHIARA DI ESSERE A CONOSCENZA CHE L'UTILIZZO DI QUESTA APPLICAZIONE DURANTE LA GUIDA POTREBBE CAUSARE GRAVI LESIONI O DANNI A SE STESSO E AGLI ALTRI. NOI NON SAREMO RESPONSABILI DI EVENTUALI PERDITE O DANNI DERIVANTI DALL'INOSSERVANZA DI LEGGI E NORMATIVE APPLICABILI E DI LINEE DI CONDOTTA RELATIVE ALLA GUIDA APPLICABILI ALL'UTENTE. L'UTENTE GARANTISCE ALTRESÌ CHE:
<br/>
<br/>(a) Utilizzerà il vivavoce/la modalità auricolare attivata durante la guida.
<br/>(b) Posizionerà il proprio smartphone a una distanza facilmente raggiungibile.
<br/>(c) Comunicherà alla persona con cui è in corso una conversazione di trovarsi alla guida; se necessario, terminerà la chiamata in condizioni di traffico intenso o condizioni atmosferiche pericolose.
<br/>(d) Non prenderà appunti, non cercherà numeri di telefono, né leggerà/scriverà e-mail o navigherà in Internet durante la guida.
<br/>(e) Non intraprenderà conversazioni stressanti o emotivamente coinvolgenti che possono distogliere la sua attenzione dalla strada.
<br/>(f) Rispetterà tutte le leggi, normative e linee di condotta relative alla guida applicabili. 
<br/>
<br/>3.2 Modalità Auto offre un servizio per l'uso in automobile che aiuta gli utenti a utilizzare le funzionalità degli smartphone come chiamate, messaggi, navigazione e ascolto di musica tramite inserimento vocale e/o inserimento con tocco (potrebbero essere disattivati in conformità alle leggi e normative dello stato dell'utente o alle linee di condotta applicabili). 
<br/>
<br/>1)	Gli utenti possono utilizzare le seguenti funzionalità: chiamate, navigazione, musica, messaggi.
<br/>2)	Alcune funzionalità e l'inserimento con tocco potrebbero essere disattivati mentre l'automobile è in movimento per garantire la guida sicura.
<br/>3)	Attivando i Servizi, il Bluetooth e il GPS dell'utente potrebbero essere accesi automaticamente. Connessione dati, WLAN e WiFi verranno attivati previo consenso dell'utente.
<br/>
<br/>3.3 L'utente non può utilizzare i Servizi per finalità illecite. Si fa espresso divieto all'utente di utilizzare i Servizi in connessione a qualunque attività che vìoli i diritti di terzi, compresi i diritti di proprietà intellettuale. Samsung può rispondere alle comunicazioni di presunta violazione dei diritti di copyright nel rispetto delle leggi e dei regolamenti applicabili in materia di proprietà intellettuale. 
<br/>
<br/>3.4 Il software di Samsung, utilizzato dall'utente per i Servizi, potrebbe periodicamente scaricare e installare aggiornamenti in maniera automatica. L'utente acconsente alle operazione di download e di installazione degli aggiornamenti, come parte dell'utilizzo dei Servizi.
<br/>
<br/>4. Tutela della Privacy 
<br/>
<br/>4.1 Samsung si impegna nella protezione dei dati personali dell'utente per i Servizi. L'utente è tenuto a leggere attentamente la presente Sezione per assicurarsi di rispettare l'informativa sulla privacy di modalità Auto.
<br/>
<br/>Le informazioni da noi raccolte possono comprendere, in via esemplificativa: 
<br/>
<br/>(a)	Informazioni personali (per es., l'inserimento vocale dell'utente per controllare chiamate, navigazione, messaggi, funzioni musicali, elenco di contatti e titolo di tracce musicali);  
<br/>(b)	Informazioni relative al dispositivo (per es., il numero di modello del dispositivo, il codice Paese e le impostazioni relative alla lingua del dispositivo dell'utente); oppure
<br/>(c)	Informazioni relative all'accesso e informazioni relative all'utilizzo (per es., dettagli sulla modalità di utilizzo da parte dell'utente del servizio o dei dispositivi hardware applicabili, come ora e durata dell'utilizzo del servizio da parte dell'utente, nelle modalità specificate in una sezione separata sottostante).
<br/>
<br/>Le informazioni relative all'utente possono essere da noi utilizzate per finalità che comprendono, in via esemplificativa, le seguenti:
<br/>
<br/>(a)	per valutare e analizzare i nostri mercati, clienti, prodotti e servizi (richiedendo altresì all'utente un parere in merito ai nostri prodotti e servizi e svolgendo sondaggi tra i clienti);
<br/>(b)	per consentirci di monitorare, revisionare e migliorare i prodotti e i servizi da noi offerti;
<br/>(c)	per raccomandare servizi o contenuti che potrebbero essere di interesse per l'utente;
<br/>(d)	per finalità di contabilità interna; oppure
<br/>(e)	per finalità di marketing.
<br/>
<br/>Periodicamente, possiamo trasferire o unire le informazioni raccolte riportate in precedenza all'interno delle nostre banche dati online e combinarle alle informazioni che otteniamo da altre fonti, incluse le informazioni ricevute dalle nostre società affiliate, società di marketing o dai nostri agenti pubblicitari. 
<br/>
<br/>Le informazioni raccolte riportate in precedenza possono essere da noi rivelate: 
<br/>
<br/>(a)	a una società affiliata di Samsung;
<br/>(b)	a fornitori di servizi che ci aiutano ad erogare i servizi all'utente nella misura necessaria per assolvere ai loro doveri e alle loro funzioni;
<br/>(c)	ai nostri consulenti professionali;
<br/>(d)	ogniqualvolta siamo tenuti per legge a procedere in tal senso, su richiesta delle autorità governative nell'ambito dello svolgimento di un'inchiesta o per verificare o applicare la conformità alle politiche che disciplinano i nostri servizi e alle leggi applicabili. Possiamo inoltre rivelare tali informazioni quando riteniamo in buona fede che la rivelazione sia necessaria per proteggere i diritti, i beni o la sicurezza di Samsung, o di nostre società affiliate, nostri partner d'affari, clienti o terzi nelle modalità richieste o consentite dalla legge;
<br/>(e)	all'acquirente o al potenziale acquirente dei nostri diritti e obblighi in relazione ai nostri servizi, nel qual caso i dati personali da noi detenuti relativi ai nostri clienti costituiranno una risorsa oggetto di trasferimento di qualsiasi acquisto, e potremo trasferire i dati personali dell'utente a una tale terza parte; oppure
<br/>(f)	laddove ciò sia necessario per perseguire i nostri interessi legittimi, o qualora siamo tenuti o autorizzati per legge a procedere in tal senso.
<br/>
<br/>4.2 L'utente accetta e dichiara di essere a conoscenza che determinate informazioni concernenti l'utilizzo da parte sua dei Servizi compresi, in via esemplificativa, elenco di contatti, titolo di tracce musicali, numero di modello, codice Paese, impostazioni relative alla lingua del dispositivo, potrebbero essere elaborate automaticamente per fornire il controllo vocale del telefono, della posizione e della musica nell'ambito dell'utilizzo, da parte sua, dei Servizi.
<br/>
<br/>5. Tariffe
<br/>
<br/>5.1 Inizialmente, i Servizi saranno offerti all'utente esclusivamente attraverso funzionalità gratuite, tuttavia, Samsung si riserva il diritto di aggiungere caratteristiche e funzionalità a pagamento, in futuro. 
<br/>	
<br/>5.2 All'utente potrebbero essere addebitate tariffe per il trasferimento dati, per l'utilizzo del Servizio o per l'aggiornamento del Servizio. La tariffa varierà in base ai fornitori dei servizi di rete dell'utente.
<br/>
<br/>6. Dichiarazione di non responsabilità
<br/>
<br/>I SERVIZI SONO FORNITI "COSÌ COME SONO" E "SECONDO DISPONIBILITÀ" SENZA GARANZIE DI ALCUN TIPO, NÉ ESPLICITE NÉ IMPLICITE. SAMSUNG ESCLUDE TUTTE LE GARANZIE, ESPLICITE O IMPLICITE, COMPRESE, IN VIA ESEMPLIFICATIVA, GARANZIE IMPLICITE DI COMMERCIABILITÀ, IDONEITÀ PER UN UTILIZZO PARTICOLARE, TITOLARITÀ E NON VIOLAZIONE DEI DIRITTI DI TERZE PARTI CON RIFERIMENTO A INFORMAZIONI, CONTENUTI O MATERIALI DEI SERVIZI. SAMSUNG NON DICHIARA NÉ GARANTISCE CHE I MATERIALI DI SAMSUNG, COMPRESE LE INFORMAZIONI DISPONIBILI NEI SERVIZI, SIANO ACCURATE, COMPLETE, AFFIDABILI, AGGIORNATE O PRIVE DI ERRORI. SAMSUNG NON DICHIARA NÉ GARANTISCE CHE I SERVIZI O I PROPRI SERVER SIANO ESENTI DA VIRUS O ALTRE COMPONENTI DANNOSE. L'UTENTE È TENUTO A VERIFICARE IN MANIERA INDIPENDENTE L'ACCURATEZZA DI QUALSIASI INFORMAZIONE OTTENUTA DAI SERVIZI PRIMA DI FARNE USO. L'UTENTE ACCETTA DI ESSERE L'UNICO RESPONSABILE DELL'UTILIZZO DA PARTE SUA DEI CONTENUTI FORNITI DAI SERVIZI. SAMSUNG NON È RESPONSABILE DI ERRORI O OMISSIONI IN QUALSIASI INFORMAZIONE O IN MATERIALI FORNITI ALL'UTENTE ATTRAVERSO I SERVIZI. 
<br/>
<br/>I SERVIZI POSSONO INOLTRE CONTENERE INFORMAZIONI O UN COLLEGAMENTO A DETERMINATE APPLICAZIONI E DETERMINATI SERVIZI FORNITI O OFFERTI DA TERZI (COLLETTIVAMENTE I "SERVIZI DI TERZI "). SAMSUNG NON RACCOMANDA NÉ SI ASSUME LA RESPONSABILITÀ DI SERVIZI DI TERZI NÉ MONITORA O HA IL CONTROLLO SU TALI SERVIZI DI TERZI. DI CONSEGUENZA, SAMSUNG NON RILASCIA ALCUNA ASSICURAZIONE, DICHIARAZIONE O GARANZIA DI ALCUN TIPO CON RIFERIMENTO A QUALITÀ, COMPETENZA, VALORE, AFFIDABILITÀ, REATTIVITÀ, ACCURATEZZA O COMPLETEZZA DI TALI SERVIZI DI TERZI O DEI RISULTATI DA ESSI OTTENUTI, E SAMSUNG NON SI ASSUME ALCUNA RESPONSABILITÀ O OBBLIGAZIONE IN RELAZIONE AI SERVIZI DI TERZI O ALLE AZIONI O ALL'INADEMPIENZA DI COLORO CHE FORNISCONO TALI SERVIZI DI TERZI. L'UTENTE ASSUME LA PIENA RESPONSABILITÀ DELL'UTILIZZO, DA PARTE SUA, DI TALI SERVIZI DI TERZI E SAMSUNG NON È RESPONSABILE NÉ OBBLIGATA IN RELAZIONE AI SERVIZI DI TERZI.
<br/>
<br/>7. Limitazione della responsabilità 
<br/>
<br/>IN NESSUN CASO SAMSUNG O UNA DELLE RELATIVE SOCIETÀ AFFILIATE, UNO DEI RELATIVI APPALTATORI INDIPENDENTI, FORNITORI DI SERVIZI O CONSULENTI, O UNO DEI RISPETTIVI AMMINISTRATORI, DIPENDENTI E AGENTI, SARÀ RESPONSABILE DI DANNI DIRETTI, , O DI ALTRI DANNI DI QUALSIASI TIPO, COMPRESI, IN VIA ESEMPLIFICATIVA, PERDITA DI UTILIZZO, LUCRO CESSANTE O PERDITA DI DATI, DERIVANTI DA UN'AZIONE DI NATURA CONTRATTUALE O EXTRA-CONTRATTUALE  O ALTRIMENTI, DERIVANTI DA O IN QUALSIASI MODO CORRELATI O CONNESSI A QUALSIASI UTILIZZO DEI SERVIZI, DEI CONTENUTI O DEI MATERIALI INCLUSI NEI SERVIZI O A CUI È STATO POSSIBILE ACCEDERE ATTRAVERSO GLI STESSI, COMPRESI, IN VIA ESEMPLIFICATIVA, DANNI, PERDITA O LESIONI CAUSATI DA O DERIVANTI DALL'AVER FATTO AFFIDAMENTO, DA PARTE DELL'UTENTE, SU QUALSIASI INFORMAZIONE OTTENUTA DAI SERVIZI, O CHE DERIVANO DA ERRORI, OMISSIONI, INTERRUZIONI, ELIMINAZIONE DI FILE O E-MAIL, ERRORI, DIFETTI, VIRUS, RITARDI NEL FUNZIONAMENTO O NELLA TRASMISSIONE O QUALSIASI MANCATA PRESTAZIONE, DERIVANTI O MENO DA EVENTI DI FORZA MAGGIORE, MANCATA COMUNICAZIONE, FURTO, DISTRUZIONE O ACCESSO NON AUTORIZZATO AI RECORD, AI PROGRAMMI O AI SERVIZI DI SAMSUNG. LA RESPONSABILITÀ COMPLESSIVA DI SAMSUNG, SIA DERIVANTE DAL CONTRATTO, DA UNA GARANZIA, DA ILLECITO CIVILE, RESPONSABILITÀ DI PRODOTTO, RESPONSABILITÀ INCONDIZIONATA O ALTRA TEORIA DI RESPONSABILITÀ, DERIVANTE DA O CORRELATA IN QUALSIASI MANIERA ALL'UTILIZZO DEI SERVIZI NON ECCEDERÀ QUALSIASI CORRISPETTIVO VERSATO DALL'UTENTE, SE PRESENTE, A SAMSUNG PER L'UTILIZZO DEI SERVIZI.
<br/>
<br/>8. Risarcimento 
<br/>
<br/>L'utente accetta di manlevare e tenere indenne Samsung, le relative società affiliate, i relativi appaltatori indipendenti, fornitori di servizi e consulenti, e ognuno dei rispettivi amministratori, dipendenti e agenti, da e contro richieste di risarcimento danni, danni, costi, responsabilità e spese (compresi, in via esemplificativa, onorari ragionevolmente corrisposti ad avvocati) derivanti da o correlati (i) all'utilizzo o all'abuso, da parte sua, dei Servizi; (ii) alla condotta e la violazione da parte dell'utente delle Condizioni per l'utilizzo del servizio o alla violazione da parte dell'utente dei diritti di un terzo.
<br/> 
<br/>9. Diritto applicabile e foro competente 
<br/>
<br/>Le presenti Condizioni per l'utilizzo del servizio e l'utilizzo da parte dell'utente dei Servizi saranno disciplinati e interpretati in base alle leggi della Repubblica di Corea, con l'esclusione delle relative disposizioni in materia di conflitti di legge. Qualsiasi controversia o rivendicazione derivante da o correlata in qualsiasi modo alle presenti Condizioni per l'utilizzo del servizio, o alla relativa violazione, o all'utilizzo, da parte dell'utente, dei Servizi, sarà composta esclusivamente mediante arbitrato gestito dalla Camera di commercio internazionale (la "CCI") in conformità alle relative norme in materia di arbitrato internazionale. La sede dell'arbitrato sarà Seoul, in Corea, e il procedimento si svolgerà in lingua inglese.
<br/>
<br/>10. Indipendenza delle previsioni contrattuali
<br/>
<br/>Qualora una delle disposizioni delle presenti Condizioni per l'utilizzo del servizio dovesse essere ritenuta illecita, nulla o per qualsiasi motivo inapplicabile nella giurisdizione dell'utente, la disposizione specifica sarà ritenuta indipendente dalle presenti Condizioni per l'utilizzo del servizio e non pregiudicherà la validità e l'applicabilità delle rimanenti disposizioni.
<br/>
<br/>11. Domande e informazioni di contatto 
<br/>
<br/>Domande o commenti possono essere indirizzati a Samsung all'indirizzo e-mail carmode@samsung.com.
<br/>
<br/>『L'utente dichiara di essere a conoscenza e di accettare che i requisiti legali per le leggi e le normative applicabili (comprese, in via esemplificativa, l'informativa sulla privacy e le leggi in materia di tutela dei consumatori) possono variare in base a ogni regione e/o Paese. Di conseguenza, le seguenti Condizioni per l'utilizzo del servizio regionali sono aggiunte nella misura applicabile per poter riflettere determinati requisiti che non sono specificamente presi in esame nelle Condizioni per l'utilizzo del servizio riportate in precedenza o non sono coerenti con queste. Nella misura in cui sussista un conflitto tra le Condizioni per l'utilizzo del servizio riportate in precedenza e le Condizioni per l'utilizzo del servizio regionali, prevarranno le Condizioni per l'utilizzo del servizio regionali.』
<br/>
<br/>■ Condizioni per l'utilizzo del servizio regionali
<br/>Samsung, con le presenti Condizioni per l’utilizzo del servizio regionali applicabili esclusivamente in Italia, comunica che le clausole 6 7 e 8 delle Condizioni per l'utilizzo del servizio modalità auto sopra riportate non si applicano agli utenti italiani.
<br/>Inoltre per gli utenti residenti in Italia, il foro esclusivamente competente a decidere qualsiasi controversia relativo al Servizio sarà il foro del luogo di residenza dell'utente. 
<br/>
<br/>