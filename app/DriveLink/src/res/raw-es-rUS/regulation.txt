﻿<b>CONDICIONES DEL SERVICIO DEL CAR MODE</b>
<br/>
<br/>
<br/>LEA DETENIDAMENTE ESTAS CONDICIONES DEL SERVICIO ANTES DE USAR EL SERVICIO.
<br/>El Servicio Car mode (en adelante, el “Servicio”) ofrece un servicio para usar en el interior del coche y que ayuda a los usuarios a utilizar funciones del smartphone tales como el teléfono, los mensajes, la navegación y la reproducción de música de un modo seguro y cómodo. Las presentes Condiciones del servicio rigen y regulan la relación legal entre Samsung Electronics Co., Ltd. y sus empresas subsidiarias (denominadas, en conjunto, "Samsung" o "nosotros") y usted en calidad de usuario del Servicio. 
<br/>
<br/>1. Aceptación de las Condiciones del Servicio Car mode
<br/>
<br/>1.1	AL UTILIZAR EL SERVICIO, ACEPTA QUEDAR OBLIGADO POR LAS CONDICIONES DEL SERVICIO. SI NO ACEPTA LAS CONDICIONES DE SERVICIO, NO LO UTILICE. 
<br/>
<br/>1.2 LOS SERVICIOS NO PODRÁN SER UTILIZADOS POR PERSONAS MENORES DE 13 AÑOS. SI TIENE MENOS DE 13 AÑOS, NO PODRÁ UTILIZAR EL SERVICIO NI FACILITARLE A SAMSUNG NINGÚN TIPO DE INFORMACIÓN QUE PUEDA IDENTIFICARLO PERSONALMENTE. Si tiene 13 años o más, pero menos de 18, declara haber revisado los presentes términos y condiciones con su padre o tutor legal para asegurarse de que tanto usted como su padre o tutor legal los comprenden. Si usted es un padre o tutor que permite que una persona menor de edad en su país de residencia (un "menor") utilice los Servicios, acepta: (i) supervisar el uso que el menor haga de los Servicios; (ii) asumir todos los riesgos asociados a la utilización que el menor haga de los Contenidos o de cualquier otra información que se haya recibido a través de los Servicios, incluida la transmisión de materiales, contenido u otra información a terceros o que proceda de ellos a través de Internet; (iii) asumir las responsabilidades derivadas de la utilización de los Servicios por parte del menor, incluidas las obligaciones de pago; y (iv) garantizar la exactitud y veracidad de toda la información proporcionada por el menor.
<br/>
<br/>2. Prestación de los servicios por parte de Samsung
<br/>
<br/>2.1 Samsung le concede un derecho y una licencia no exclusivos, intransferibles y limitados para acceder y utilizar los Servicios solo para uso personal, siempre que cumpla íntegramente estas Condiciones del servicio. No debe interferir ni intentar interferir en el funcionamiento o uso de los Servicios que hagan otras personas de ningún modo posible ni mediante ningún medio ni dispositivo, incluidos, sin limitarse a ellos, correo no deseado, piratería informática, carga de virus informáticos, bombas de tiempo ni ningún otro medio. SAMSUNG SE RESERVA EL DERECHO, A SU ENTERA DISCRECIÓN, DE CAMBIAR, MODIFICAR, ACTUALIZAR, AÑADIR, INTERRUMPIR, QUITAR, REVISAR, ELIMINAR O CAMBIAR DE ALGUNA MANERA CUALQUIER PARTE DE LAS CONDICIONES DEL SERVICIO DEL CAR MODE EN CUALQUIER MOMENTO QUE CONSIDERE OPORTUNO CON O SIN NOTIFICACIÓN PREVIA. SU USO CONTINUADO DE ESTOS SERVICIOS DE SAMSUNG IMPLICARÁ LA ACEPTACIÓN DE DICHOS CAMBIOS. 
<br/>
<br/>2.2 Si incumple o Samsung sospecha que ha incumplido cualquiera de las disposiciones de las presentes Condiciones del servicio, Samsung podrá, sin limitación de sus otros derechos o recursos, los cuales quedan expresamente reservados, a su entera discreción y sin notificación previa: (i) dar por terminado su uso de los Servicios, y usted continuará siendo responsable de todas las cantidades pendientes de pago hasta la fecha de finalización (incluido el mismo día de la finalización); (ii) rescindir las licencias que se otorgan en virtud del presente documento; y/o (iii) impedir el acceso en parte o en su totalidad a los Servicios y/o los Contenidos.
<br/>
<br/>2.3 Usted reconoce y acepta que los Servicios pueden ser proporcionados por empresas afiliadas o subcontratistas de Samsung en representación de Samsung. Usted acepta que cualquier tercero que proporcione contenidos a Samsung para distribuirlos a través de los Servicios será considerado, en virtud del presente documento, como tercero beneficiario intencionado de las presentes Condiciones del servicio y tendrá derecho a exigir el cumplimiento de todas y cada una de las obligaciones de los usuarios del Servicio recogidas en las presentes Condiciones del servicio como si dicho proveedor de contenidos de terceros fuera parte de las presentes Condiciones del servicio, y según lo dispuesto en la legislación vigente o según los preceptos de equidad ("Equity") del orden jurídico anglosajón. 
<br/>
<br/>2.4 La utilización del Servicio exige el uso de uno o más dispositivos compatibles, acceso o conexión a una red móvil o Internet (puede conllevar costos adicionales) y un software concreto (puede conllevar costos adicionales), y puede ser necesaria la obtención de actualizaciones o mejoras en algún momento. Dado que la utilización del Servicio implica hardware, software y acceso a Internet, su capacidad para utilizar dicho Servicio se puede ver afectada por el rendimiento de estos factores y el entorno que lo rodea. Reconoce y acepta que dichos requisitos del sistema, que pueden variar en algún momento, son responsabilidad suya. 
<br/>
<br/>3. El Uso de los servicios
<br/>
<br/>3.1 DEBERÁ ASEGURARSE DE CUMPLIR TODA LA LEGISLACIÓN Y NORMATIVA EN VIGOR RELACIONADA AL USO DE DISPOSITIVOS MÓVILES DURANTE LA CONDUCCIÓN. POR LA PRESENTE, RECONOCE Y ACEPTA QUE LA SEGURIDAD SERÁ SIEMPRE LO MÁS IMPORTANTE. RECONOCE QUE EL USO DE ESTA APLICACIÓN DURANTE LA CONDUCCIÓN PUEDE PROVOCARLE LESIONES O DAÑOS GRAVES A USTED Y A TERCEROS. NO ASUMIREMOS NINGUNA RESPONSABILIDAD RELACIONADA CON CUALQUIER PÉRDIDA O DAÑO DERIVADO DEL INCUMPLIMIENTO DE CUALQUIER LEY, NORMATIVA O DIRECTRICES SOBRE CONDUCCIÓN VIGENTES QUE RECAIGAN SOBRE USTED. DEBERÁ ASEGURARSE DE CUMPLIR CON LOS SIGUIENTES REQUISITOS:
<br/>
<br/>(a) Utilizar el modo de manos libres activado por voz mientras conduce.
<br/>(b) Colocar el smartphone a su alcance.
<br/>(c) Comunicar a la persona con la que está hablando que está conduciendo; si resultara necesario, suspender la llamada en caso de tráfico denso o condiciones atmosféricas que puedan ponerlo en peligro.
<br/>(d) No tomar notas, buscar números de teléfono, leer o escribir mensajes de correo electrónico ni navegar por Internet mientras conduce.
<br/>(e) No mantener conversaciones estresantes o con carga emocional que puedan distraer su atención de la carretera.
<br/>(f) Cumplir todas las leyes, normativas y directrices aplicables sobre conducción, así como los Reglamentos de Tránsito correspondientes. 
<br/>
<br/>3.2 El Car mode ofrece un servicio para usar en el interior del coche que ayuda a los usuarios a utilizar determinadas funciones del smartphone como el teléfono, los mensajes, la navegación o la reproducción de música mediante la voz o funciones táctiles (estas últimas pueden estar desactivadas de acuerdo con lo establecido en las leyes, normativas y directrices aplicables). 
<br/>
<br/>1)	Los usuarios pueden utilizar las siguientes funciones: Llamar, Navegar, Música, Mensajes.
<br/>2)	Algunas funciones, como las táctiles, pueden estar desactivadas mientras el coche está en movimiento para garantizar la conducción segura.
<br/>3)	Al activar los Servicios, las funciones de Bluetooth y GPS pueden activarse de forma automática. Otras, como los datos móviles, WLAN o WiFi se activarán con su aprobación.
<br/>
<br/>3.3 No podrá utilizar los Servicios con fines ilícitos. Queda expresamente prohibido usar los Servicios relacionados con cualquier actividad que suponga el incumplimiento de los derechos de terceros, incluidos los derechos de propiedad intelectual. Samsung puede responder a las notificaciones que aleguen una infracción de los derechos de autor cumpliendo las leyes y regulaciones aplicables en materia de propiedad intelectual. 
<br/>
<br/>2.4 El software de Samsung que utiliza para el Servicio puede descargar e instalar automáticamente actualizaciones en cualquier momento. Usted acepta dicha descarga e instalación de actualizaciones como parte de la utilización del Servicio.
<br/>
<br/>4. Protección de la privacidad 
<br/>
<br/>4.1 Samsung se compromete a la protección de sus datos personales en relación con los Servicios. Lea detenidamente esta Sección para asegurarse de que cumple con la política de privacidad del Car mode.
<br/>
<br/>La información que podemos recopilar puede incluir, sin limitación, los siguientes datos: 
<br/>
<br/>(a)	Información personal (por ejemplo, su entrada de voz para controlar el teléfono, la navegación, los mensajes, las funciones de música, la lista de contactos y el título de las canciones);  
<br/>(b)	información del dispositivo (por ejemplo, el número de modelo del dispositivo, el código de país y la configuración de idioma del dispositivo ); o bien
<br/>(c)	la información de registro y de uso (por ejemplo, detalles sobre el modo en que usa el servicio o los dispositivos de hardware aplicables, como el tiempo y la duración del uso del servicio, tal y como se explica en una sección más adelante).
<br/>
<br/>Podemos utilizar información sobre usted para los propósitos indicados a continuación, entre los que se incluyen, sin limitarse a ellos:
<br/>
<br/>(a)	para evaluar y llevar a cabo un análisis del mercado, los clientes, los productos y los servicios (solicitar su opinión sobre nuestros productos y servicios, así como realizar encuestas a clientes);
<br/>(b)	para controlar, revisar y mejorar los productos y servicios que ofrecemos;
<br/>(c)	para recomendar los servicios y contenidos que puedan resultarle de interés;
<br/>(d)	para conservar registros internos; o bien
<br/>(e)	con fines de marketing.
<br/>
<br/>En alguna ocasión, podemos transferir o fusionar cualquiera de los datos recopilados en nuestras bases de datos en línea y combinarlos con la información que obtengamos de otras fuentes, entre las que se incluye la información recibida de filiales, empresas de marketing o anunciantes. 
<br/>
<br/>Podemos revelar la información que recopilemos por los medios indicados anteriormente a las siguientes entidades: 
<br/>
<br/>(a)	a una filial de Samsung;
<br/>(b)	a proveedores de servicios que nos ayuden a proporcionarle servicios en la medida en que la necesiten para llevar a cabo su cometido y sus funciones;
<br/>(c)	a nuestros consejeros profesionales;
<br/>(d)	cuando sea necesario por motivos legales, si así lo exigen las autoridades gubernamentales que estén llevando a cabo una investigación o para verificar o exigir el cumplimiento de las normas que rigen nuestros servicios y de la legislación aplicable. También podemos revelar dicha información cuando consideremos de buena fe que la revelación resulta necesaria para proteger los derechos, la propiedad o la seguridad de Samsung o de cualquiera de sus filiales, socios de negocio, clientes u otras entidades del modo en que lo exija o lo permita la ley;
<br/>(e)	al comprador o comprador potencial de nuestros derechos y obligaciones relacionados con los servicios, en cuyo caso los datos personales que guardamos de nuestros clientes se considerarán un activo transferido de la compra, por lo que podemos transferir su información personal a dicho tercero; o bien
<br/>(f)	cuando resulte necesario para hacer valer nuestros intereses legítimos o en el caso en que la ley nos exija o nos permita hacerlo.
<br/>
<br/>4.2 Usted acepta y reconoce que determinada información acerca del uso de los Servicios, entre la que se incluye, sin limitarse a ella, la lista de contactos, los títulos de las canciones, el número de modelo, el código del país o la configuración de idioma del dispositivo, pueden procesarse de forma automática para proporcionar el control de voz del teléfono, la ubicación y la música como parte del uso que hace de los Servicios.
<br/>
<br/>5. Tarifas
<br/>
<br/>5.1 Aunque el Servicio se ofrezca en un principio solo con funciones gratuitas, Samsung se reserva el derecho de agregar en el futuro funciones y funcionalidades que puedan conllevar costos adicionales. 
<br/>	
<br/>5.2 Pueden aplicarse tarifas de transferencia de datos por el uso o la actualización del Servicio. La tarifa variará dependiendo del proveedor de red, sin responsabilidad alguna para Samsung..
<br/>
<br/>6. Exención de responsabilidad
<br/>
<br/>LOS SERVICIOS SE PROPORCIONAN "TAL CUAL" O "SEGÚN SU DISPONIBILIDAD" SIN OFRECER NINGUNA GARANTÍA DE NINGÚN TIPO, NI EXPRESA NI IMPLÍCITA. SAMSUNG RECHAZA EXPRESAMENTE CUALESQUIERA GARANTÍAS, EXPRESAS, IMPLÍCITAS, INCLUIDAS, SIN LIMITARSE A ELLAS, LAS GARANTÍAS IMPLÍCITAS DE COMERCIABILIDAD, IDONEIDAD PARA UN FIN CONCRETO, TITULARIDAD Y NO VULNERACIÓN DE LA INFORMACIÓN, EL CONTENIDO O LOS MATERIALES DE LOS SERVICIOS. SAMSUNG NO REPRESENTA NI GARANTIZA QUE LOS MATERIALES DE SAMSUNG, INCLUIDA LA INFORMACIÓN DISPONIBLE EN LOS SERVICIOS, SEAN PRECISOS, COMPLETOS, FIABLES, ACTUALES O ESTÉN LIBRES DE ERRORES. SAMSUNG NO REPRESENTA NI GARANTIZA QUE LOS SERVICIOS O SUS SERVIDORES ESTÉN LIBRES DE VIRUS U OTROS COMPONENTES DAÑINOS. USTED DEBERÍA COMPROBAR POR SU CUENTA LA PRECISIÓN DE TODA LA INFORMACIÓN QUE RECIBA DE LOS SERVICIOS ANTES DE USARLOS. USTED ACEPTA SER EL ÚNICO RESPONSABLE DEL USO QUE HAGA DEL CONTENIDO QUE LE PROPORCIONEN LOS SERVICIOS. SAMSUNG NO SERÁ RESPONSABLE DE LOS ERRORES U OMISIONES DE LA INFORMACIÓN O LOS MATERIALES QUE SE LE PROPORCIONEN A TRAVÉS DE LOS SERVICIOS. 
<br/>
<br/>LOS SERVICIOS TAMBIÉN PUEDEN CONTENER INFORMACIÓN O VÍNCULOS A DETERMINADOS SERVICIOS Y APLICACIONES PROPORCIONADOS U OFRECIDOS POR TERCEROS (DENOMINADOS, EN CONJUNTO, "LOS SERVICIOS DE TERCEROS"). SAMSUNG NO RECOMIENDA NI APOYA NINGÚN SERVICIO DE TERCEROS NI EJERCE NINGÚN CONTROL SOBRE DICHOS SERVICIOS DE TERCEROS. POR LO TANTO, SAMSUNG NO OFRECE NINGUNA GARANTÍA NI REALIZA NINGUNA DECLARACIÓN DE NINGÚN TIPO CON RESPECTO A LA CALIDAD, COMPETENCIA, VALOR, FIABILIDAD, CAPACIDAD DE RESPUESTA, PRECISIÓN O TOTALIDAD DE NINGÚN SERVICIO DE TERCEROS NI DE LOS RESULTADOS QUE SE OBTENGAN DE ELLOS. ADEMÁS, SAMSUNG NO ASUME NINGUNA RESPONSABILIDAD RELACIONADA CON NINGÚN SERVICIO DE TERCEROS NI CON LAS ACCIONES U OMISIONES DE LAS ENTIDADES QUE PROPORCIONAN DICHOS SERVICIOS DE TERCEROS. USTED ASUME TODA LA RESPONSABILIDAD DEL USO QUE HAGA DE CUALQUIERA DE LOS SERVICIOS DE TERCEROS Y SAMSUNG NO SERÁ RESPONSABLE DE NINGÚN SERVICIO DE TERCEROS.
<br/>
<br/>7. Limitación de la responsabilidad 
<br/>
<br/>EN NINGÚN CASO, SAMSUNG O CUALQUIERA DE SUS ENTIDADES AFILIADAS, CONTRATISTAS INDEPENDIENTES, PROVEEDORES DE SERVICIOS O CONSULTORES, O CUALQUIERA DE SUS RESPECTIVOS DIRECTIVOS, EMPLEADOS Y AGENTES, ACEPTARÁN RESPONSABILIDAD ALGUNA POR CUALESQUIERA DAÑOS DIRECTOS, ESPECIALES, INDIRECTOS O CONSECUENTES, NI DE NINGÚN OTRO DAÑO DE CUALQUIER OTRA CLASE, INCLUIDOS, SIN LIMITARSE A ELLOS, PÉRDIDA DE USO, LUCRO CESANTE O PÉRDIDA DE DATOS, CONTRACTUAL NI EXTRACONTRACTUAL (INCLUIDO, SIN LIMITARSE A ÉL, LA NEGLIGENCIA) O CUALQUIER OTRA CIRCUNSTANCIA QUE DERIVE DEL USO DE LOS SERVICIOS, EL CONTENIDO O LOS MATERIALES CONTENIDOS EN LOS SERVICIOS O A LOS QUE SE ACCEDA A TRAVÉS DE ELLOS O QUE ESTÉ RELACIONADA CON ELLOS, INCLUIDOS, SIN LIMITARSE A ELLOS, LOS DAÑOS, LAS PÉRDIDAS O LAS LESIONES QUE DERIVEN O SEAN CONSECUENCIA DE LA CONFIANZA DEPOSITADA POR EL USUARIO EN CUALQUIER INFORMACIÓN QUE SE HAYA OBTENIDO DE LOS SERVICIOS O QUE RESULTE EN ERRORES, OMISIONES, INTERRUPCIONES, ELIMINACIÓN DE ARCHIVOS O MENSAJES DE CORREO ELECTRÓNICO, DEFECTOS, VIRUS, RETRASOS EN EL FUNCIONAMIENTO O LA TRANSMISIÓN O CUALQUIER FALLO EN EL FUNCIONAMIENTO, INDEPENDIENTEMENTE DE QUE SE DEBA A MOTIVOS DE FUERZA MAYOR, FALLOS EN LAS COMUNICACIONES, ROBO, DESTRUCCIÓN O ACCESO NO AUTORIZADO A LOS REGISTROS, PROGRAMAS O SERVICIOS DE SAMSUNG. LA RESPONSABILIDAD GLOBAL DE SAMSUNG, YA SEA EN CONTRATO, GARANTÍA, ILÍCITO CIVIL EXTRACONTRACTUAL (INCLUIDA LA NEGLIGENCIA, YA SEA ACTIVA, PASIVA O IMPUTADA), LA RESPONSABILIDAD POR PRODUCTOS, RESPONSABILIDAD OBJETIVA U OTRO PRINCIPIO QUE RESULTE DEL USO DE LOS SERVICIOS O QUE ESTÉ RELACIONADO DE CUALQUIER FORMA POSIBLE CON ELLOS, NO DEBERÁ SUPERAR CUALQUIER COMPENSACIÓN QUE HAYA PAGADO A SAMSUNG, SI LA HUBIERA, POR EL USO DE LOS SERVICIOS.
<br/>
<br/>8. Indemnización 
<br/>
<br/>Usted acepta defender, indemnizar y mantener indemne a Samsung, sus entidades afiliadas, contratistas independientes, proveedores de servicios y consultores, así como a sus respectivos directivos, empleados y agentes, frente a cualquier demanda, daños, costos, responsabilidades y gastos (incluidos, sin limitarse a ellos, los honorarios razonables de abogados) que se deriven de los siguientes supuestos o estén relacionados con ellos: (i) uso o mal uso de los Servicios; (ii) su conducta o el incumplimiento de las Condiciones del servicio o de los derechos de cualquier tercero.
<br/>
<br/>9. Legislación aplicable y jurisdicción 
<br/>
<br/>Estas Condiciones del servicio y el uso de los Servicios se regirán y se interpretarán según lo establecido en las leyes de la República de Corea sin que sean de aplicación sus disposiciones sobre conflictos de leyes. Cualquier controversia o queja que surja como consecuencia de estas Condiciones del servicio o en relación con ellas, o el incumplimiento de las mismas, o con el uso de los Servicios se dirimirán de forma exclusiva mediante arbitraje que llevará a cabo la Cámara Internacional de Comercio al amparo Reglamento de arbitraje de la Cámara Internacional de Comercio (Rules of Arbitration of the International Chamber of Commerce). El arbitraje se celebrará en Seúl, Corea, y se llevará a cabo en inglés.
<br/>
<br/>10. Divisibilidad
<br/>
<br/>Si alguna disposición de estas Condiciones del servicio se considera ilegal, inválida o no ejecutable por cualquier motivo en su jurisdicción, esa disposición se considerará independiente de estas Condiciones del servicio y no afectará a la validez o al cumplimiento de ninguna de las disposiciones restantes.
<br/>
<br/>11. Preguntas e información de contacto 
<br/>
<br/>Las preguntas o comentarios deberán dirigirse a Samsung a la dirección de correo electrónico carmode@samsung.com.
<br/>
<br/>『Usted reconoce y acepta que los requisitos legales de las leyes y normativas aplicables (incluidas, sin limitarse a ellas, la ley sobre privacidad y las leyes sobre la protección de los clientes) pueden variar dependiendo de cada región y/o país. Por tanto, las siguientes Condiciones del servicio regionales se han añadido en la medida en que proceda para reflejar requisitos específicos de determinados países que no quedan expresamente reflejados en las siguientes Condiciones del servicio o que no son coherentes con ellos. En el caso en que se produzca algún conflicto entre las siguientes Condiciones del servicio y las Condiciones del servicio regionales, prevalecerá lo dispuesto en las Condiciones del servicio regionales.』
<br/>
<br/>■ Condiciones del servicio regionales
<br/>
<br/>