.class public Lcom/LocalFota/XLFBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "XLFBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;
    }
.end annotation


# static fields
.field static final handler:Landroid/os/Handler;


# instance fields
.field serviceStartThread:Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/LocalFota/XLFBroadcastReceiver$1;

    invoke-direct {v0}, Lcom/LocalFota/XLFBroadcastReceiver$1;-><init>()V

    sput-object v0, Lcom/LocalFota/XLFBroadcastReceiver;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 113
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    .line 27
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1

    .line 28
    const-string v9, "DBG_WSS_LF"

    const-string v10, "The Intent is NULL."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    const-string v10, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 34
    const-string v9, "DBG_WSS_LF"

    const-string v10, "*** boot complete ***"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    sget-boolean v9, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z

    if-nez v9, :cond_2

    .line 37
    new-instance v9, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;

    sget-object v10, Lcom/LocalFota/XLFBroadcastReceiver;->handler:Landroid/os/Handler;

    invoke-direct {v9, v10, v11, p2}, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;-><init>(Landroid/os/Handler;ILandroid/content/Intent;)V

    iput-object v9, p0, Lcom/LocalFota/XLFBroadcastReceiver;->serviceStartThread:Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;

    .line 38
    iget-object v9, p0, Lcom/LocalFota/XLFBroadcastReceiver;->serviceStartThread:Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;

    invoke-virtual {v9}, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->start()V

    goto :goto_0

    .line 42
    :cond_2
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetLFStatus()I

    move-result v5

    .line 43
    .local v5, "nStatus":I
    const-string v9, "DBG_WSS_LF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "nStatus : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    if-nez v5, :cond_0

    .line 46
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFDeltaAllClear()V

    .line 47
    const-string v9, "DBG_WSS_LF"

    const-string v10, "Delete Delta File"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    .end local v5    # "nStatus":I
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    const-string v10, "android.intent.action.USER_INITIALIZE"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 50
    const-string v9, "DBG_WSS_LF"

    const-string v10, "*** User initialize ***"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v3, 0x0

    .line 52
    .local v3, "is":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 54
    .local v6, "os":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v9, "/data/data/com.LocalFota/libdprwl.so"

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_6

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    const-string v10, "libdprwl.so"

    invoke-virtual {v9, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 58
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v8

    .line 59
    .local v8, "size":I
    new-array v0, v8, [B

    .line 60
    .local v0, "buffer":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 61
    .local v4, "len":I
    const/4 v9, -0x1

    if-ne v4, v9, :cond_5

    .line 73
    if-eqz v6, :cond_4

    .line 74
    :try_start_1
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 75
    :cond_4
    if-eqz v3, :cond_0

    .line 76
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/io/IOException;
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 64
    .end local v1    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_2
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 65
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .local v7, "os":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual {v7, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 66
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 67
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v6, v7

    .line 73
    .end local v0    # "buffer":[B
    .end local v4    # "len":I
    .end local v7    # "os":Ljava/io/FileOutputStream;
    .end local v8    # "size":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :cond_6
    if-eqz v6, :cond_7

    .line 74
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 75
    :cond_7
    if-eqz v3, :cond_0

    .line 76
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 77
    :catch_1
    move-exception v1

    .line 78
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 69
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    :catch_2
    move-exception v1

    .line 70
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_5
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 73
    if-eqz v6, :cond_8

    .line 74
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 75
    :cond_8
    if-eqz v3, :cond_0

    .line 76
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 77
    :catch_3
    move-exception v1

    .line 78
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 72
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 73
    :goto_2
    if-eqz v6, :cond_9

    .line 74
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 75
    :cond_9
    if-eqz v3, :cond_a

    .line 76
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 79
    :cond_a
    :goto_3
    throw v9

    .line 77
    :catch_4
    move-exception v1

    .line 78
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v10, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 72
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "len":I
    .restart local v7    # "os":Ljava/io/FileOutputStream;
    .restart local v8    # "size":I
    :catchall_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 69
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v7    # "os":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v1

    move-object v6, v7

    .end local v7    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
