.class public Lcom/LocalFota/ui/XUiTempNFCActivity;
.super Landroid/app/Activity;
.source "XUiTempNFCActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;
    }
.end annotation


# static fields
.field static final handler:Landroid/os/Handler;

.field static m_Context:Landroid/content/Context;


# instance fields
.field serviceStartThread:Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/LocalFota/ui/XUiTempNFCActivity$1;

    invoke-direct {v0}, Lcom/LocalFota/ui/XUiTempNFCActivity$1;-><init>()V

    sput-object v0, Lcom/LocalFota/ui/XUiTempNFCActivity;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 110
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const-string v2, "DBG_WSS_LF"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    sput-object p0, Lcom/LocalFota/ui/XUiTempNFCActivity;->m_Context:Landroid/content/Context;

    .line 36
    sget-boolean v2, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z

    if-nez v2, :cond_0

    .line 38
    new-instance v2, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;

    sget-object v3, Lcom/LocalFota/ui/XUiTempNFCActivity;->handler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v2, v3, v5, v4}, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;-><init>(Landroid/os/Handler;ILandroid/content/Intent;)V

    iput-object v2, p0, Lcom/LocalFota/ui/XUiTempNFCActivity;->serviceStartThread:Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;

    .line 39
    iget-object v2, p0, Lcom/LocalFota/ui/XUiTempNFCActivity;->serviceStartThread:Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;

    invoke-virtual {v2}, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->start()V

    .line 40
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->finish()V

    .line 66
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-static {}, Lcom/LocalFota/cust/XLFTask;->GetSyncMode()Z

    move-result v0

    .line 45
    .local v0, "bInSync":Z
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bInSync : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    if-nez v0, :cond_2

    .line 49
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/LocalFota/ui/XUiAdminActivity;->resolveIntent(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/LocalFota/ui/XUiAdminActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v1, "i":Landroid/content/Intent;
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0, v1}, Lcom/LocalFota/ui/XUiTempNFCActivity;->startActivity(Landroid/content/Intent;)V

    .line 54
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->finish()V

    goto :goto_0

    .line 58
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->finish()V

    goto :goto_0

    .line 63
    :cond_2
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040010

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 64
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiTempNFCActivity;->finish()V

    goto :goto_0
.end method
