.class public Lcom/LocalFota/ui/XUiDownloadProgress;
.super Landroid/app/AlertDialog;
.source "XUiDownloadProgress.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# static fields
.field private static lBeforeDownTime:J

.field private static lOneTermDownTime:J

.field private static mDeltaDownloadSize:J

.field private static mDeltaDownloadchecksize:J

.field private static mDeltaPackageTotalSize:J

.field private static mDownloadPercent:I

.field private static mDownloadRemainTime:Ljava/lang/String;

.field private static mDownloadSizeInfo:Ljava/lang/String;

.field private static nBeforeDownSize:J

.field private static nBeforeOneTermDownSize:J

.field private static nDownloadCount:I

.field private static nOneTermDownSize:J


# instance fields
.field private final mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mDownloadPercentageText:Landroid/widget/TextView;

.field private mDownloadProgressbar:Landroid/widget/ProgressBar;

.field private mDownloadSizeText:Landroid/widget/TextView;

.field private mDownloadTimeText:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 45
    sput v1, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    .line 46
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    .line 48
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    .line 49
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadchecksize:J

    .line 51
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 52
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    .line 54
    sput v1, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    .line 55
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeDownSize:J

    .line 56
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->lBeforeDownTime:J

    .line 57
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    .line 58
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    .line 59
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->lOneTermDownTime:J

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clickListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object v0, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    .line 41
    iput-object v0, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercentageText:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeText:Landroid/widget/TextView;

    .line 43
    iput-object v0, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadTimeText:Landroid/widget/TextView;

    .line 63
    new-instance v0, Lcom/LocalFota/ui/XUiDownloadProgress$1;

    invoke-direct {v0, p0}, Lcom/LocalFota/ui/XUiDownloadProgress$1;-><init>(Lcom/LocalFota/ui/XUiDownloadProgress;)V

    iput-object v0, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mHandler:Landroid/os/Handler;

    .line 96
    iput-object p2, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 97
    iput-object p3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/LocalFota/ui/XUiDownloadProgress;)V
    .locals 0
    .param p0, "x0"    # Lcom/LocalFota/ui/XUiDownloadProgress;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiUpdateDownloadProgressBar()V

    return-void
.end method

.method private setEnableButton(IZ)V
    .locals 3
    .param p1, "whichButton"    # I
    .param p2, "bEnable"    # Z

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lcom/LocalFota/ui/XUiDownloadProgress;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 162
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_0
    const-string v1, "DBG_WSS_LF"

    const-string v2, "Button is null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static wssLFUiDownloadProgressInit()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 278
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    .line 281
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    .line 282
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    .line 283
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadchecksize:J

    .line 285
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 286
    const-string v0, "00:00:00"

    sput-object v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public static wssLFUiDownloadTimeInit()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 265
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v0, "00:00:00"

    sput-object v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    .line 268
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    .line 269
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeDownSize:J

    .line 270
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->lBeforeDownTime:J

    .line 271
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    .line 272
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    .line 273
    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->lOneTermDownTime:J

    .line 274
    return-void
.end method

.method public static wssLFUiGetDownloadPercent()I
    .locals 1

    .prologue
    .line 349
    sget v0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    return v0
.end method

.method public static wssLFUiSetDeltaTotalSize(J)V
    .locals 4
    .param p0, "deltaPackageTotalSize"    # J

    .prologue
    .line 291
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deltaPackageTotalSize : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    sput-wide p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    .line 293
    return-void
.end method

.method private static wssLFUiSetDownloadTime(J)V
    .locals 16
    .param p0, "nDownloadSize"    # J

    .prologue
    .line 199
    const/4 v2, 0x6

    .line 200
    .local v2, "DOWNLOADTIME_CHECK_START_COUNT":I
    const/16 v0, 0x14

    .line 201
    .local v0, "DOWNLOADTIME_CHECK_END_COUNT":I
    const/16 v1, 0x32

    .line 203
    .local v1, "DOWNLOADTIME_CHECK_RESET_COUNT":I
    const-wide/16 v6, 0x0

    .line 204
    .local v6, "lDownTime":J
    const-wide/16 v8, 0x0

    .line 205
    .local v8, "nDownRemainData":J
    const/4 v10, 0x0

    .line 206
    .local v10, "nDownTermCount":I
    const/4 v5, 0x0

    .line 208
    .local v5, "nChangePercent":I
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    .line 209
    .local v3, "calendar":Ljava/util/GregorianCalendar;
    sget v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    add-int/lit8 v11, v11, 0x1

    sput v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    .line 211
    sget v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    const/4 v12, 0x6

    if-ne v11, v12, :cond_2

    .line 213
    invoke-virtual {v3}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    sput-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->lBeforeDownTime:J

    .line 214
    sput-wide p0, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeDownSize:J

    .line 254
    :cond_0
    :goto_0
    sget v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    const/16 v12, 0x32

    if-le v11, v12, :cond_1

    .line 256
    const/4 v11, 0x0

    sput v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    .line 261
    :cond_1
    return-void

    .line 216
    :cond_2
    sget v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    const/16 v12, 0x14

    if-ne v11, v12, :cond_5

    .line 218
    invoke-virtual {v3}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    sget-wide v14, Lcom/LocalFota/ui/XUiDownloadProgress;->lBeforeDownTime:J

    sub-long/2addr v12, v14

    sput-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->lOneTermDownTime:J

    .line 219
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeDownSize:J

    sub-long v12, p0, v12

    sput-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    .line 221
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-nez v11, :cond_3

    .line 223
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    sput-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    goto :goto_0

    .line 227
    :cond_3
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_0

    .line 229
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    long-to-int v11, v12

    mul-int/lit8 v11, v11, 0x64

    int-to-long v12, v11

    sget-wide v14, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    div-long/2addr v12, v14

    long-to-int v5, v12

    .line 231
    const-string v11, "DBG_WSS_LF"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "nBeforeOneTermDownSize : ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-wide v14, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] nOneTermDownSize : ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-wide v14, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] nChangePercent : ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/16 v11, 0x50

    if-lt v5, v11, :cond_4

    const/16 v11, 0x78

    if-le v5, v11, :cond_0

    .line 234
    :cond_4
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nOneTermDownSize:J

    sput-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    goto :goto_0

    .line 239
    :cond_5
    sget v11, Lcom/LocalFota/ui/XUiDownloadProgress;->nDownloadCount:I

    const/16 v12, 0x14

    if-le v11, v12, :cond_0

    .line 241
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    sub-long v8, v12, p0

    .line 242
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_6

    .line 244
    sget-wide v12, Lcom/LocalFota/ui/XUiDownloadProgress;->nBeforeOneTermDownSize:J

    div-long v12, v8, v12

    long-to-int v10, v12

    .line 245
    int-to-long v12, v10

    sget-wide v14, Lcom/LocalFota/ui/XUiDownloadProgress;->lOneTermDownTime:J

    mul-long v6, v12, v14

    .line 247
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v11, "HH:mm:ss"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v11, v12}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 248
    .local v4, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v11, "GMT+0"

    invoke-static {v11}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 249
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    sput-object v11, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    .line 251
    .end local v4    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_6
    const-string v11, "DBG_WSS_LF"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "nDownRemainData : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static wssLFUiUpdateDownloadInfo()V
    .locals 12

    .prologue
    .line 302
    const-wide/16 v4, 0x0

    .line 303
    .local v4, "nDownloadSize":J
    const/4 v3, 0x0

    .line 305
    .local v3, "nPercentage":I
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaPkgFile()Ljava/io/File;

    move-result-object v2

    .line 306
    .local v2, "local":Ljava/io/File;
    if-nez v2, :cond_0

    .line 343
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 311
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 314
    :cond_1
    sget-wide v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-eqz v8, :cond_2

    .line 316
    const-wide/16 v8, 0x64

    mul-long/2addr v8, v4

    sget-wide v10, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    div-long/2addr v8, v10

    long-to-int v3, v8

    .line 322
    :cond_2
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_3

    sget-wide v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_3

    .line 324
    new-instance v0, Ljava/math/BigDecimal;

    long-to-double v8, v4

    const-wide/high16 v10, 0x4130000000000000L    # 1048576.0

    div-double/2addr v8, v10

    invoke-direct {v0, v8, v9}, Ljava/math/BigDecimal;-><init>(D)V

    .line 325
    .local v0, "bd_recv":Ljava/math/BigDecimal;
    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    .line 326
    const-string v8, "%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 330
    .local v6, "szDownloadRecvSize":Ljava/lang/String;
    new-instance v1, Ljava/math/BigDecimal;

    sget-wide v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    long-to-double v8, v8

    const-wide/high16 v10, 0x4130000000000000L    # 1048576.0

    div-double/2addr v8, v10

    invoke-direct {v1, v8, v9}, Ljava/math/BigDecimal;-><init>(D)V

    .line 331
    .local v1, "bd_total":Ljava/math/BigDecimal;
    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v1

    .line 332
    const-string v8, "%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 334
    .local v7, "szDownloadTotalSize":Ljava/lang/String;
    sput-object v6, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 335
    sget-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    const-string v9, "MB"

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 336
    sget-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 337
    sget-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 338
    sget-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    const-string v9, "MB"

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    .line 340
    .end local v0    # "bd_recv":Ljava/math/BigDecimal;
    .end local v1    # "bd_total":Ljava/math/BigDecimal;
    .end local v6    # "szDownloadRecvSize":Ljava/lang/String;
    .end local v7    # "szDownloadTotalSize":Ljava/lang/String;
    :cond_3
    invoke-static {v4, v5}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiSetDownloadTime(J)V

    .line 341
    sput-wide v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    .line 342
    sput v3, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    goto/16 :goto_0
.end method

.method private wssLFUiUpdateDownloadProgressBar()V
    .locals 6

    .prologue
    .line 170
    const v0, 0x4b000

    .line 174
    .local v0, "INDICATOR_CHECK_SIZE":I
    :try_start_0
    iget-object v2, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadTimeText:Landroid/widget/TextView;

    sget-object v3, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    sget-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    sget-wide v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadchecksize:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    sget-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    sget-wide v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 178
    :cond_0
    sget-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadchecksize:J

    const-wide/32 v4, 0x4b000

    add-long/2addr v2, v4

    sput-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadchecksize:J

    .line 180
    iget-object v2, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    sget v3, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 181
    iget-object v2, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercentageText:Landroid/widget/TextView;

    sget v3, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v2, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeText:Landroid/widget/TextView;

    sget-object v3, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    sget-wide v2, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaDownloadSize:J

    sget-wide v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDeltaPackageTotalSize:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 187
    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/LocalFota/ui/XUiDownloadProgress;->setEnableButton(IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_1
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    const-string v3, "DBG_WSS_LF"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiUpdateDownloadInfo()V

    .line 106
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiDownloadProgress;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 107
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 108
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const/high16 v3, 0x7f020000

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 109
    .local v1, "downloadView":Landroid/view/View;
    const/high16 v3, 0x7f060000

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    .line 110
    const v3, 0x7f060001

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercentageText:Landroid/widget/TextView;

    .line 111
    const v3, 0x7f060002

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeText:Landroid/widget/TextView;

    .line 112
    const v3, 0x7f060003

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadTimeText:Landroid/widget/TextView;

    .line 114
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    .line 115
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 116
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadProgressbar:Landroid/widget/ProgressBar;

    sget v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 121
    :goto_0
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercentageText:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 122
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercentageText:Landroid/widget/TextView;

    sget v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadPercent:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    :goto_1
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadTimeText:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 128
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadTimeText:Landroid/widget/TextView;

    sget-object v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadRemainTime:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :goto_2
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeText:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    .line 134
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeText:Landroid/widget/TextView;

    sget-object v4, Lcom/LocalFota/ui/XUiDownloadProgress;->mDownloadSizeInfo:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :goto_3
    const v3, 0x7f040012

    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiDownloadProgress;->setTitle(I)V

    .line 140
    invoke-virtual {p0, v1}, Lcom/LocalFota/ui/XUiDownloadProgress;->setView(Landroid/view/View;)V

    .line 141
    const/4 v3, -0x1

    const-string v4, "Hide"

    iget-object v5, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v3, v4, v5}, Lcom/LocalFota/ui/XUiDownloadProgress;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 142
    const/4 v3, -0x2

    const-string v4, "cancel"

    iget-object v5, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v3, v4, v5}, Lcom/LocalFota/ui/XUiDownloadProgress;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 143
    iget-object v3, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiDownloadProgress;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 144
    new-instance v3, Lcom/LocalFota/ui/XUiDownloadProgress$2;

    invoke-direct {v3, p0}, Lcom/LocalFota/ui/XUiDownloadProgress$2;-><init>(Lcom/LocalFota/ui/XUiDownloadProgress;)V

    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiDownloadProgress;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 155
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 156
    return-void

    .line 118
    :cond_0
    const-string v3, "DBG_WSS_LF"

    const-string v4, "mDownloadProgressbar is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :cond_1
    const-string v3, "DBG_WSS_LF"

    const-string v4, "mDownloadPercentageText is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 130
    :cond_2
    const-string v3, "DBG_WSS_LF"

    const-string v4, "mDownloadTimeText is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 136
    :cond_3
    const-string v3, "DBG_WSS_LF"

    const-string v4, "mDownloadSizeText is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public sendMessage(I)V
    .locals 3
    .param p1, "msg"    # I

    .prologue
    .line 85
    :try_start_0
    iget-object v1, p0, Lcom/LocalFota/ui/XUiDownloadProgress;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
