.class Lcom/LocalFota/ui/XUiAdminActivity$3;
.super Landroid/content/BroadcastReceiver;
.source "XUiAdminActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/LocalFota/ui/XUiAdminActivity;->registerReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/LocalFota/ui/XUiAdminActivity;


# direct methods
.method constructor <init>(Lcom/LocalFota/ui/XUiAdminActivity;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/LocalFota/ui/XUiAdminActivity$3;->this$0:Lcom/LocalFota/ui/XUiAdminActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 246
    const-string v1, "DBG_WSS_LF"

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 252
    :cond_0
    invoke-static {}, Lcom/LocalFota/cust/XLFTask;->GetSyncMode()Z

    move-result v0

    .line 253
    .local v0, "bInSync":Z
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive_bInSync : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->endTimer()V

    .line 256
    sget-boolean v1, Lcom/LocalFota/ui/XUiAdminActivity;->m_bWificonnectPopup:Z

    if-eqz v1, :cond_1

    .line 258
    const/16 v1, 0xd

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 259
    const/4 v1, 0x0

    sput-boolean v1, Lcom/LocalFota/ui/XUiAdminActivity;->m_bWificonnectPopup:Z

    .line 262
    :cond_1
    if-nez v0, :cond_4

    .line 264
    sget-boolean v1, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    if-nez v1, :cond_3

    .line 266
    const-string v1, "DBG_WSS_LF"

    const-string v2, "----------- XMSG_LF_INIT WIFI ok"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    sput-boolean v5, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    .line 268
    const/4 v1, 0x2

    invoke-static {v1, v4, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 280
    .end local v0    # "bInSync":Z
    :cond_2
    :goto_0
    return-void

    .line 270
    .restart local v0    # "bInSync":Z
    :cond_3
    sget-boolean v1, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    if-eqz v1, :cond_2

    .line 272
    # invokes: Lcom/LocalFota/ui/XUiAdminActivity;->xuiCallUiStart()V
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->access$000()V

    goto :goto_0

    .line 277
    :cond_4
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040010

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method
