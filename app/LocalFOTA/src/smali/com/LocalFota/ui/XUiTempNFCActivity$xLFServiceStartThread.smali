.class Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;
.super Ljava/lang/Thread;
.source "XUiTempNFCActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/ui/XUiTempNFCActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "xLFServiceStartThread"
.end annotation


# instance fields
.field mHandler:Landroid/os/Handler;

.field msg:Landroid/os/Message;


# direct methods
.method constructor <init>(Landroid/os/Handler;ILandroid/content/Intent;)V
    .locals 2
    .param p1, "h"    # Landroid/os/Handler;
    .param p2, "category"    # I
    .param p3, "receivedintent"    # Landroid/content/Intent;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    .line 117
    iput-object p1, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->mHandler:Landroid/os/Handler;

    .line 118
    iget-object v0, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    if-nez v0, :cond_0

    .line 120
    const-string v0, "DBG_WSS_LF"

    const-string v1, "msg is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object v0, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    iput p2, v0, Landroid/os/Message;->what:I

    .line 124
    iget-object v0, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 125
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 129
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Run Start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0x14

    if-ge v1, v2, :cond_0

    .line 133
    sget-boolean v2, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z

    if-eqz v2, :cond_1

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/LocalFota/ui/XUiTempNFCActivity$xLFServiceStartThread;->msg:Landroid/os/Message;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 148
    return-void

    .line 138
    :cond_1
    :try_start_0
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Waiting for LF Application Start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
