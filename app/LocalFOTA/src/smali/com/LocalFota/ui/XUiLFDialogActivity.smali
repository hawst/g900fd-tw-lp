.class public Lcom/LocalFota/ui/XUiLFDialogActivity;
.super Landroid/app/Activity;
.source "XUiLFDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    }
.end annotation


# static fields
.field private static mDialogActivity:Landroid/app/Activity;

.field public static mDialogId:I

.field public static mDownloadProgress:Lcom/LocalFota/ui/XUiDownloadProgress;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 914
    return-void
.end method

.method public static wssDmUiUpdateDownloadProgress()V
    .locals 2

    .prologue
    .line 903
    sget-object v0, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDownloadProgress:Lcom/LocalFota/ui/XUiDownloadProgress;

    if-eqz v0, :cond_0

    .line 905
    sget-object v0, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDownloadProgress:Lcom/LocalFota/ui/XUiDownloadProgress;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/LocalFota/ui/XUiDownloadProgress;->sendMessage(I)V

    .line 911
    :goto_0
    return-void

    .line 909
    :cond_0
    const-string v0, "DBG_WSS_LF"

    const-string v1, "mDownloadProgressDialog is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public LFUiPopUp(I)Z
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/16 v7, 0x12

    const/16 v6, 0xb

    const/4 v5, 0x3

    const/16 v4, 0x10

    const/4 v3, 0x1

    .line 77
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    packed-switch p1, :pswitch_data_0

    .line 175
    :goto_0
    :pswitch_0
    return v3

    .line 81
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 82
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 85
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 86
    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 87
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 90
    :pswitch_3
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 91
    invoke-virtual {p0, v5}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 94
    :pswitch_4
    invoke-virtual {p0, v5}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 95
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 98
    :pswitch_5
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 99
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 102
    :pswitch_6
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 103
    invoke-virtual {p0, v5}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 104
    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 105
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 108
    :pswitch_7
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 109
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 112
    :pswitch_8
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 113
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 116
    :pswitch_9
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 117
    const/16 v0, 0x17

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 120
    :pswitch_a
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 121
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 124
    :pswitch_b
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 125
    invoke-virtual {p0, v5}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 126
    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 129
    :pswitch_c
    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 130
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 133
    :pswitch_d
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 136
    :pswitch_e
    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 137
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 140
    :pswitch_f
    invoke-virtual {p0, v7}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 141
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto :goto_0

    .line 144
    :pswitch_10
    invoke-virtual {p0, v5}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 145
    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 146
    invoke-virtual {p0, v7}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 149
    :pswitch_11
    invoke-virtual {p0, v7}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 150
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 153
    :pswitch_12
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 156
    :pswitch_13
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 157
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 160
    :pswitch_14
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 161
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 164
    :pswitch_15
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 165
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 168
    :pswitch_16
    invoke-virtual {p0, v3}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 169
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_d
        :pswitch_11
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_9
        :pswitch_16
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    sput-object p0, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogActivity:Landroid/app/Activity;

    .line 45
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "sid":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    .line 47
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 12
    .param p1, "id"    # I

    .prologue
    const/4 v11, 0x1

    const v10, 0x7f040012

    const v9, 0x108009b

    const v7, 0x1010355

    const/4 v8, 0x0

    .line 180
    packed-switch p1, :pswitch_data_0

    .line 896
    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    :goto_0
    return-object v2

    .line 184
    :pswitch_1
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 185
    .local v2, "prgDialog":Landroid/app/ProgressDialog;
    invoke-virtual {v2, v9}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 186
    invoke-virtual {v2, v7}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 187
    invoke-virtual {v2, v10}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 188
    const v6, 0x7f040005

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 189
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$1;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$1;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 200
    const v6, 0x7f040003

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$2;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$2;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 211
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$3;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$3;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0

    .line 225
    .end local v2    # "prgDialog":Landroid/app/ProgressDialog;
    :pswitch_2
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 227
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040006

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$6;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$6;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$5;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$5;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$4;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$4;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 260
    :pswitch_3
    const-string v6, "DBG_WSS_LF"

    const-string v7, "LF_EVENT_UI_DOWNLOAD_PROGRESS"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    new-instance v2, Lcom/LocalFota/ui/XUiDownloadProgress;

    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$7;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$7;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$8;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$8;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-direct {v2, p0, v6, v7}, Lcom/LocalFota/ui/XUiDownloadProgress;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    sput-object v2, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDownloadProgress:Lcom/LocalFota/ui/XUiDownloadProgress;

    goto/16 :goto_0

    .line 301
    :pswitch_4
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040018

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$11;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$11;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$10;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$10;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$9;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$9;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 335
    :pswitch_5
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040011

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$15;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$15;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040003

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$14;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$14;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$13;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$13;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$12;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$12;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 384
    :pswitch_6
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 386
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040014

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$18;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$18;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$17;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$17;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$16;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$16;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 421
    :pswitch_7
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 422
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040016

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$21;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$21;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$20;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$20;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$19;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$19;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 455
    :pswitch_8
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 456
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040009

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$25;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$25;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040003

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$24;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$24;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$23;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$23;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$22;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$22;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 497
    :pswitch_9
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 498
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040015

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$28;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$28;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$27;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$27;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$26;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$26;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 531
    :pswitch_a
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 532
    .restart local v2    # "prgDialog":Landroid/app/ProgressDialog;
    invoke-virtual {v2, v9}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 533
    invoke-virtual {v2, v7}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 534
    invoke-virtual {v2, v10}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 535
    const v6, 0x7f040007

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 536
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$29;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$29;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 546
    const v6, 0x7f040003

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$30;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$30;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 556
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$31;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$31;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto/16 :goto_0

    .line 572
    .end local v2    # "prgDialog":Landroid/app/ProgressDialog;
    :pswitch_b
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 573
    .restart local v2    # "prgDialog":Landroid/app/ProgressDialog;
    invoke-virtual {v2, v9}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 574
    invoke-virtual {v2, v7}, Landroid/app/ProgressDialog;->setIconAttribute(I)V

    .line 575
    invoke-virtual {v2, v10}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 576
    const v6, 0x7f04001c

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 577
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$32;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$32;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 587
    const v6, 0x7f040003

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$33;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$33;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 597
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$34;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$34;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto/16 :goto_0

    .line 612
    .end local v2    # "prgDialog":Landroid/app/ProgressDialog;
    :pswitch_c
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 614
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040008

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$37;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$37;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$36;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$36;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$35;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$35;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 651
    :pswitch_d
    const/4 v4, 0x0

    .line 652
    .local v4, "t":Ljava/util/Timer;
    new-instance v1, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;

    const/16 v6, 0x11

    invoke-direct {v1, p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;I)V

    .line 653
    .local v1, "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    new-instance v4, Ljava/util/Timer;

    .end local v4    # "t":Ljava/util/Timer;
    invoke-direct {v4, v11}, Ljava/util/Timer;-><init>(Z)V

    .line 654
    .restart local v4    # "t":Ljava/util/Timer;
    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v1, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 701
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 702
    .local v3, "prgDlg":Landroid/app/ProgressDialog;
    const v6, 0x7f04000c

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 703
    invoke-virtual {v3, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 704
    invoke-virtual {v3, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 705
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$38;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$38;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v3

    .line 715
    goto/16 :goto_0

    .line 719
    .end local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    .end local v3    # "prgDlg":Landroid/app/ProgressDialog;
    .end local v4    # "t":Ljava/util/Timer;
    :pswitch_e
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 720
    .restart local v3    # "prgDlg":Landroid/app/ProgressDialog;
    const v6, 0x7f040017

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 721
    invoke-virtual {v3, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 722
    invoke-virtual {v3, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 723
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$39;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$39;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v3

    .line 733
    goto/16 :goto_0

    .line 737
    .end local v3    # "prgDlg":Landroid/app/ProgressDialog;
    :pswitch_f
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 738
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f04000f

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$42;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$42;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$41;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$41;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$40;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$40;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 771
    :pswitch_10
    const/4 v4, 0x0

    .line 772
    .restart local v4    # "t":Ljava/util/Timer;
    new-instance v1, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;

    const/16 v6, 0x14

    invoke-direct {v1, p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;I)V

    .line 773
    .restart local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    new-instance v4, Ljava/util/Timer;

    .end local v4    # "t":Ljava/util/Timer;
    invoke-direct {v4, v11}, Ljava/util/Timer;-><init>(Z)V

    .line 774
    .restart local v4    # "t":Ljava/util/Timer;
    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v1, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 776
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 778
    .local v0, "latestversionprgDlg":Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWipeData()Z

    move-result v6

    if-ne v6, v11, :cond_0

    .line 780
    const v6, 0x7f04000b

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 787
    :goto_1
    invoke-virtual {v0, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 788
    invoke-virtual {v0, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 789
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$43;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$43;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v0

    .line 799
    goto/16 :goto_0

    .line 784
    :cond_0
    const v6, 0x7f04000a

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 804
    .end local v0    # "latestversionprgDlg":Landroid/app/ProgressDialog;
    .end local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    .end local v4    # "t":Ljava/util/Timer;
    :pswitch_11
    const/4 v4, 0x0

    .line 805
    .restart local v4    # "t":Ljava/util/Timer;
    new-instance v1, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;

    const/16 v6, 0x18

    invoke-direct {v1, p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;I)V

    .line 806
    .restart local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    new-instance v4, Ljava/util/Timer;

    .end local v4    # "t":Ljava/util/Timer;
    invoke-direct {v4, v11}, Ljava/util/Timer;-><init>(Z)V

    .line 807
    .restart local v4    # "t":Ljava/util/Timer;
    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v1, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 809
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 810
    .restart local v0    # "latestversionprgDlg":Landroid/app/ProgressDialog;
    const v6, 0x7f040013

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 811
    invoke-virtual {v0, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 812
    invoke-virtual {v0, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 813
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$44;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$44;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v0

    .line 823
    goto/16 :goto_0

    .line 828
    .end local v0    # "latestversionprgDlg":Landroid/app/ProgressDialog;
    .end local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    .end local v4    # "t":Ljava/util/Timer;
    :pswitch_12
    const/4 v4, 0x0

    .line 829
    .restart local v4    # "t":Ljava/util/Timer;
    new-instance v1, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;

    const/16 v6, 0x15

    invoke-direct {v1, p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;I)V

    .line 830
    .restart local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    new-instance v4, Ljava/util/Timer;

    .end local v4    # "t":Ljava/util/Timer;
    invoke-direct {v4, v11}, Ljava/util/Timer;-><init>(Z)V

    .line 831
    .restart local v4    # "t":Ljava/util/Timer;
    const-wide/16 v6, 0xbb8

    invoke-virtual {v4, v1, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 833
    new-instance v5, Landroid/app/ProgressDialog;

    invoke-direct {v5, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 835
    .local v5, "updatesuccessprgDlg":Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWipeData()Z

    move-result v6

    if-ne v6, v11, :cond_1

    .line 837
    const v6, 0x7f04000e

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 844
    :goto_2
    invoke-virtual {v5, v11}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 845
    invoke-virtual {v5, v8}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 846
    new-instance v6, Lcom/LocalFota/ui/XUiLFDialogActivity$45;

    invoke-direct {v6, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$45;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    move-object v2, v5

    .line 856
    goto/16 :goto_0

    .line 841
    :cond_1
    const v6, 0x7f04000d

    invoke-virtual {p0, v6}, Lcom/LocalFota/ui/XUiLFDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 861
    .end local v1    # "mytime":Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
    .end local v4    # "t":Ljava/util/Timer;
    .end local v5    # "updatesuccessprgDlg":Landroid/app/ProgressDialog;
    :pswitch_13
    invoke-static {v8}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 862
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040002

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f040004

    new-instance v8, Lcom/LocalFota/ui/XUiLFDialogActivity$48;

    invoke-direct {v8, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$48;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$47;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$47;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/LocalFota/ui/XUiLFDialogActivity$46;

    invoke-direct {v7, p0}, Lcom/LocalFota/ui/XUiLFDialogActivity$46;-><init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto/16 :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_8
        :pswitch_11
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 53
    const-string v1, "DBG_WSS_LF"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dialogId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :try_start_0
    sget v1, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    if-lez v1, :cond_1

    .line 60
    sget-object v1, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogActivity:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 61
    sget-object v1, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogActivity:Landroid/app/Activity;

    sget v2, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->removeDialog(I)V

    .line 63
    :cond_0
    sget v1, Lcom/LocalFota/ui/XUiLFDialogActivity;->mDialogId:I

    invoke-virtual {p0, v1}, Lcom/LocalFota/ui/XUiLFDialogActivity;->LFUiPopUp(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 72
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
