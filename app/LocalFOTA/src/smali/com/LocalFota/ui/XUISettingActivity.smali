.class public Lcom/LocalFota/ui/XUISettingActivity;
.super Landroid/app/Activity;
.source "XUISettingActivity.java"


# instance fields
.field private mProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/LocalFota/ui/XUISettingActivity;)Lcom/LocalFota/db/XDBProfileInfo;
    .locals 1
    .param p0, "x0"    # Lcom/LocalFota/ui/XUISettingActivity;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/LocalFota/ui/XUISettingActivity;->mProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v1, 0x7f020001

    invoke-virtual {p0, v1}, Lcom/LocalFota/ui/XUISettingActivity;->setContentView(I)V

    .line 25
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetProfileInfo()Lcom/LocalFota/db/XDBProfileInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/LocalFota/ui/XUISettingActivity;->mProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    .line 26
    const v1, 0x7f060005

    invoke-virtual {p0, v1}, Lcom/LocalFota/ui/XUISettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 27
    .local v0, "mServerAddr":Landroid/widget/EditText;
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 28
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 29
    new-instance v1, Lcom/LocalFota/ui/XUISettingActivity$1;

    invoke-direct {v1, p0}, Lcom/LocalFota/ui/XUISettingActivity$1;-><init>(Lcom/LocalFota/ui/XUISettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 51
    const-string v0, "SAVE"

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 52
    const/4 v0, 0x1

    const-string v1, "CANCEL"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x108004c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 60
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 74
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 63
    :pswitch_0
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUISettingActivity;->xuiProfileSave()V

    .line 64
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUISettingActivity;->finish()V

    goto :goto_0

    .line 68
    :pswitch_1
    invoke-virtual {p0}, Lcom/LocalFota/ui/XUISettingActivity;->finish()V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected xuiProfileSave()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/LocalFota/ui/XUISettingActivity;->mProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/LocalFota/db/XDB;->xdbSetServerAddr(Ljava/lang/String;)V

    .line 80
    return-void
.end method
