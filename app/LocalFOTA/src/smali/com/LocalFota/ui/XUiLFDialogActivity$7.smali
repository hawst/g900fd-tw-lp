.class Lcom/LocalFota/ui/XUiLFDialogActivity$7;
.super Ljava/lang/Object;
.source "XUiLFDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/LocalFota/ui/XUiLFDialogActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;


# direct methods
.method constructor <init>(Lcom/LocalFota/ui/XUiLFDialogActivity;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$7;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 265
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "whichButton : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    packed-switch p2, :pswitch_data_0

    .line 287
    :goto_0
    return-void

    .line 271
    :pswitch_0
    const-string v0, "DBG_WSS_LF"

    const-string v1, "Hiden button clicked..."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v0, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$7;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-virtual {v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 278
    :pswitch_1
    const-string v0, "DBG_WSS_LF"

    const-string v1, "Cancel button clicked..."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFSetUsercancelFlag(Z)V

    .line 280
    iget-object v0, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$7;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-virtual {v0}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
