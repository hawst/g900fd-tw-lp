.class public Lcom/LocalFota/ui/XUiAdminActivity;
.super Landroid/preference/PreferenceActivity;
.source "XUiAdminActivity.java"


# static fields
.field public static ResolveValue:Ljava/lang/String;

.field public static S_ID:Ljava/lang/String;

.field public static mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field public static mWiFiManager:Landroid/net/wifi/WifiManager;

.field public static m_MyActivity:Landroid/app/Activity;

.field public static m_NFCTagNumber:Ljava/lang/String;

.field public static m_Password:Ljava/lang/String;

.field public static m_SSID:Ljava/lang/String;

.field public static m_bWificonnectPopup:Z


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    sput-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 69
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    .line 70
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->m_Password:Ljava/lang/String;

    .line 71
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    .line 72
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->S_ID:Ljava/lang/String;

    .line 73
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->ResolveValue:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/LocalFota/ui/XUiAdminActivity;->m_bWificonnectPopup:Z

    .line 77
    sput-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->m_MyActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 60
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->xuiCallUiStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/LocalFota/ui/XUiAdminActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/LocalFota/ui/XUiAdminActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/LocalFota/ui/XUiAdminActivity;->xuiCallUiSetting()V

    return-void
.end method

.method public static connectWiFi()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 296
    :try_start_0
    const-string v4, "wifi"

    invoke-static {v4}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    sput-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    .line 298
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 300
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "WifiManager.getWifiState() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 302
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 305
    :cond_0
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v4, :cond_1

    .line 307
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    const-string v5, "wifilock"

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v4

    sput-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 308
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 309
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 313
    :cond_1
    new-instance v3, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v3}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 315
    .local v3, "wc":Landroid/net/wifi/WifiConfiguration;
    const-string v4, "\""

    sget-object v5, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 316
    const-string v4, "\""

    sget-object v5, Lcom/LocalFota/ui/XUiAdminActivity;->m_Password:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 317
    const/4 v4, 0x2

    iput v4, v3, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 318
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 319
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedGroupCiphers:Ljava/util/BitSet;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 320
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 321
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 322
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedPairwiseCiphers:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 323
    iget-object v4, v3, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 326
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, v3}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v2

    .line 327
    .local v2, "netId":I
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v2, v5}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result v0

    .line 328
    .local v0, "bDisableOtherWifi":Z
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "----------- bDisableOtherWifi : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    .end local v0    # "bDisableOtherWifi":Z
    .end local v2    # "netId":I
    :goto_0
    return-void

    .line 330
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static connectWiFiFail()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xWakeLockRelease()V

    .line 340
    const/16 v0, 0xd

    invoke-static {v1, v0}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 341
    const/16 v0, 0xe

    invoke-static {v1, v0}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 342
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 235
    iget-object v1, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 284
    :goto_0
    return-void

    .line 238
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 239
    .local v0, "theFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 241
    new-instance v1, Lcom/LocalFota/ui/XUiAdminActivity$3;

    invoke-direct {v1, p0}, Lcom/LocalFota/ui/XUiAdminActivity$3;-><init>(Lcom/LocalFota/ui/XUiAdminActivity;)V

    iput-object v1, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 283
    iget-object v1, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/LocalFota/ui/XUiAdminActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static removeWiFiNetwork()V
    .locals 2

    .prologue
    .line 346
    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v0

    .line 348
    .local v0, "nNetid":I
    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 349
    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 350
    return-void
.end method

.method static resolveIntent(Landroid/content/Intent;)Z
    .locals 10
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 355
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    .line 357
    const-string v6, "DBG_WSS_LF"

    const-string v7, "The Intent is NULL."

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :goto_0
    return v5

    .line 361
    :cond_0
    const-string v4, "Useless URI"

    .line 363
    .local v4, "uri":Ljava/lang/String;
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resolveIntent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", data: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v6, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 370
    const-string v5, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 372
    .local v3, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v3, :cond_2

    .line 373
    array-length v5, v3

    new-array v2, v5, [Landroid/nfc/NdefMessage;

    .line 374
    .local v2, "msgs":[Landroid/nfc/NdefMessage;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v3

    if-ge v1, v5, :cond_1

    .line 375
    aget-object v5, v3, v1

    check-cast v5, Landroid/nfc/NdefMessage;

    aput-object v5, v2, v1

    .line 374
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 377
    :cond_1
    invoke-static {v2, v9}, Lcom/LocalFota/ui/XUiAdminActivity;->xLFpreWiFiSetting([Landroid/nfc/NdefMessage;Ljava/lang/String;)Z

    move-result v5

    goto :goto_0

    .line 379
    .end local v1    # "i":I
    .end local v2    # "msgs":[Landroid/nfc/NdefMessage;
    :cond_2
    invoke-static {v9, v4}, Lcom/LocalFota/ui/XUiAdminActivity;->xLFpreWiFiSetting([Landroid/nfc/NdefMessage;Ljava/lang/String;)Z

    move-result v5

    goto :goto_0

    .line 382
    .end local v3    # "rawMsgs":[Landroid/os/Parcelable;
    :cond_3
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown intent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private unregisterReceiver()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/LocalFota/ui/XUiAdminActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiAdminActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 290
    :cond_0
    return-void
.end method

.method public static xLFAdminActivityFinish()V
    .locals 2

    .prologue
    .line 520
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    sget-object v0, Lcom/LocalFota/ui/XUiAdminActivity;->m_MyActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 522
    return-void
.end method

.method static xLFpreDataParsing(Ljava/lang/String;)Z
    .locals 12
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 391
    const/4 v2, 0x0

    .line 392
    .local v2, "bRtn":Z
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 393
    .local v3, "context":Landroid/content/Context;
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 394
    .local v7, "state_Prefs":Landroid/content/SharedPreferences;
    const-string v9, "WipeDataTag"

    const/4 v10, 0x1

    invoke-interface {v7, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 396
    .local v0, "WipeDataTag":Z
    const-string v9, "://"

    invoke-virtual {p0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 397
    .local v5, "firsturl":I
    add-int/lit8 v9, v5, 0x3

    invoke-virtual {p0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 398
    .local v8, "szSub":Ljava/lang/String;
    const-string v9, "DBG_WSS_LF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "szSub URL : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 401
    .local v1, "array":[Ljava/lang/String;
    array-length v9, v1

    if-eqz v9, :cond_6

    .line 403
    const-string v9, "DBG_WSS_LF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "array.length() : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    array-length v9, v1

    if-ge v6, v9, :cond_0

    .line 406
    const-string v9, "DBG_WSS_LF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "array ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v1, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v9, "DBG_WSS_LF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "array ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v1, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 410
    :cond_0
    array-length v9, v1

    const/4 v10, 0x5

    if-ne v9, v10, :cond_2

    .line 413
    const/4 v9, 0x1

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    .line 414
    const/4 v9, 0x2

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_Password:Ljava/lang/String;

    .line 415
    const/4 v9, 0x3

    aget-object v9, v1, v9

    invoke-static {v9}, Lcom/LocalFota/db/XDB;->xdbSetServerAddr(Ljava/lang/String;)V

    .line 416
    const/4 v9, 0x4

    aget-object v9, v1, v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 417
    const-string v9, "0"

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    .line 421
    :goto_1
    const-string v9, "DBG_WSS_LF"

    const-string v10, "array[5] none."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 423
    .local v4, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v9, "WipeDataTag"

    const/4 v10, 0x1

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 424
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 426
    const/4 v2, 0x1

    .end local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v6    # "j":I
    :goto_2
    move v9, v2

    .line 471
    :goto_3
    return v9

    .line 419
    .restart local v6    # "j":I
    :cond_1
    const/4 v9, 0x4

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    goto :goto_1

    .line 428
    :cond_2
    array-length v9, v1

    const/16 v10, 0x8

    if-ne v9, v10, :cond_5

    .line 431
    const/4 v9, 0x1

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    .line 432
    const/4 v9, 0x2

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_Password:Ljava/lang/String;

    .line 433
    const/4 v9, 0x3

    aget-object v9, v1, v9

    invoke-static {v9}, Lcom/LocalFota/db/XDB;->xdbSetServerAddr(Ljava/lang/String;)V

    .line 434
    const/4 v9, 0x4

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    .line 436
    const/4 v9, 0x5

    aget-object v9, v1, v9

    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 437
    const-string v9, "DBG_WSS_LF"

    const-string v10, "Skip Wipe_data."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 439
    .restart local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    const-string v9, "WipeDataTag"

    const/4 v10, 0x0

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 440
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 456
    :goto_4
    const/4 v9, 0x6

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->S_ID:Ljava/lang/String;

    .line 457
    const/4 v9, 0x7

    aget-object v9, v1, v9

    sput-object v9, Lcom/LocalFota/ui/XUiAdminActivity;->ResolveValue:Ljava/lang/String;

    .line 459
    const/4 v2, 0x1

    goto :goto_2

    .line 442
    .end local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    :cond_3
    const/4 v9, 0x5

    aget-object v9, v1, v9

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 444
    const-string v9, "DBG_WSS_LF"

    const-string v10, "Nomal Wipe_data"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 446
    .restart local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    const-string v9, "WipeDataTag"

    const/4 v10, 0x1

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 447
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_4

    .line 451
    .end local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    :cond_4
    const-string v9, "DBG_WSS_LF"

    const-string v10, "array[5] value is error."

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    const/4 v9, 0x0

    const/16 v10, 0x13

    invoke-static {v9, v10}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 453
    const/4 v9, 0x0

    goto :goto_3

    .line 463
    :cond_5
    const/4 v9, 0x0

    const/16 v10, 0x13

    invoke-static {v9, v10}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_2

    .line 468
    .end local v6    # "j":I
    :cond_6
    const/4 v9, 0x0

    const/16 v10, 0x13

    invoke-static {v9, v10}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_2
.end method

.method static xLFpreWiFiSetting([Landroid/nfc/NdefMessage;Ljava/lang/String;)Z
    .locals 8
    .param p0, "msgs"    # [Landroid/nfc/NdefMessage;
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 476
    const/4 v1, 0x0

    .line 480
    .local v1, "bRtn":Z
    if-eqz p0, :cond_0

    :try_start_0
    array-length v5, p0

    if-nez v5, :cond_2

    .line 482
    :cond_0
    if-eqz p1, :cond_1

    .line 484
    invoke-static {p1}, Lcom/LocalFota/ui/XUiAdminActivity;->xLFpreDataParsing(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    move v4, v1

    .line 514
    :cond_1
    :goto_1
    return v4

    .line 500
    :cond_2
    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v5}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v0

    .line 501
    .local v0, "Payload":[B
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    .line 502
    .local v3, "str":Ljava/lang/String;
    const-string v5, "DBG_WSS_LF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "xLFpreWiFiSetting_Payload: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    invoke-static {v3}, Lcom/LocalFota/ui/XUiAdminActivity;->xLFpreDataParsing(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 509
    .end local v0    # "Payload":[B
    .end local v3    # "str":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 512
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    const/4 v5, 0x0

    const/16 v6, 0x13

    invoke-static {v5, v6}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_1
.end method

.method private xuiCallUiSetting()V
    .locals 2

    .prologue
    .line 229
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/LocalFota/ui/XUISettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/LocalFota/ui/XUiAdminActivity;->startActivity(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method private static xuiCallUiStart()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 192
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v1

    if-nez v1, :cond_1

    .line 194
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f04001a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 225
    .local v0, "nStatus":I
    :cond_0
    :goto_0
    return-void

    .line 198
    .end local v0    # "nStatus":I
    :cond_1
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFCurrentStatus()I

    move-result v0

    .line 199
    .restart local v0    # "nStatus":I
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    if-nez v0, :cond_2

    .line 203
    invoke-static {v5, v4, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 205
    :cond_2
    const/16 v1, 0x14

    if-ne v0, v1, :cond_4

    .line 207
    invoke-static {}, Lcom/LocalFota/cust/XLFTask;->DownloadGetDrawingPercentage()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    const/4 v1, 0x3

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 213
    :cond_3
    const/4 v1, 0x6

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 216
    :cond_4
    const/16 v1, 0x32

    if-ne v0, v1, :cond_5

    .line 218
    invoke-static {v5, v4, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 220
    :cond_5
    const/16 v1, 0x3c

    if-ne v0, v1, :cond_0

    .line 222
    const/4 v1, 0x7

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 82
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const-string v4, "DBG_WSS_LF"

    const-string v5, ""

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    sput-object p0, Lcom/LocalFota/ui/XUiAdminActivity;->m_MyActivity:Landroid/app/Activity;

    .line 87
    const/high16 v4, 0x7f030000

    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiAdminActivity;->addPreferencesFromResource(I)V

    .line 89
    const-string v4, "startfota"

    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiAdminActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 90
    .local v1, "SwPref":Landroid/preference/Preference;
    if-eqz v1, :cond_3

    .line 91
    new-instance v4, Lcom/LocalFota/ui/XUiAdminActivity$1;

    invoke-direct {v4, p0}, Lcom/LocalFota/ui/XUiAdminActivity$1;-><init>(Lcom/LocalFota/ui/XUiAdminActivity;)V

    invoke-virtual {v1, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 103
    :goto_0
    const-string v4, "settingfota"

    invoke-virtual {p0, v4}, Lcom/LocalFota/ui/XUiAdminActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 104
    .local v0, "SettingPref":Landroid/preference/Preference;
    if-eqz v0, :cond_4

    .line 105
    new-instance v4, Lcom/LocalFota/ui/XUiAdminActivity$2;

    invoke-direct {v4, p0}, Lcom/LocalFota/ui/XUiAdminActivity$2;-><init>(Lcom/LocalFota/ui/XUiAdminActivity;)V

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 117
    :goto_1
    invoke-direct {p0}, Lcom/LocalFota/ui/XUiAdminActivity;->registerReceiver()V

    .line 119
    const-string v4, "wifi"

    invoke-static {v4}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    sput-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    .line 120
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 122
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "m_SSID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mWiFiManager.getConnectionInfo().getSSID() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 126
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v4

    if-eqz v4, :cond_7

    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/LocalFota/ui/XUiAdminActivity;->m_SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 131
    :cond_0
    invoke-static {}, Lcom/LocalFota/cust/XLFTask;->GetSyncMode()Z

    move-result v2

    .line 132
    .local v2, "bInSync":Z
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isWifiEnabled_bInSync : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->endTimer()V

    .line 135
    if-nez v2, :cond_6

    .line 137
    sget-boolean v4, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    if-nez v4, :cond_5

    sget-boolean v4, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    if-nez v4, :cond_5

    .line 139
    const-string v4, "DBG_WSS_LF"

    const-string v5, "----------- XMSG_LF_INIT WIFI ok"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sput-boolean v7, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    .line 141
    const/4 v4, 0x2

    invoke-static {v4, v8, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 170
    .end local v2    # "bInSync":Z
    :cond_1
    :goto_2
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v4

    if-nez v4, :cond_2

    .line 172
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xWakeLockStart()V

    .line 173
    const/16 v4, 0x10

    invoke-static {v8, v4}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 174
    sput-boolean v7, Lcom/LocalFota/ui/XUiAdminActivity;->m_bWificonnectPopup:Z

    .line 175
    new-instance v4, Lcom/LocalFota/tp/XWiFiConnectTimer;

    invoke-direct {v4, v7}, Lcom/LocalFota/tp/XWiFiConnectTimer;-><init>(Z)V

    .line 176
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->connectWiFi()V

    .line 178
    :cond_2
    return-void

    .line 100
    .end local v0    # "SettingPref":Landroid/preference/Preference;
    :cond_3
    const-string v4, "DBG_WSS_LF"

    const-string v5, "SwPref is null "

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 114
    .restart local v0    # "SettingPref":Landroid/preference/Preference;
    :cond_4
    const-string v4, "DBG_WSS_LF"

    const-string v5, "SettingPref is null "

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 143
    .restart local v2    # "bInSync":Z
    :cond_5
    sget-boolean v4, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    if-eqz v4, :cond_1

    .line 145
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->xuiCallUiStart()V

    goto :goto_2

    .line 150
    :cond_6
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f040010

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_2

    .line 157
    .end local v2    # "bInSync":Z
    :cond_7
    :try_start_0
    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->mWiFiManager:Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 159
    const-string v4, "DBG_WSS_LF"

    const-string v5, "Waiting for mWiFiManager.setWifiEnabled(false)"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 162
    :catch_0
    move-exception v3

    .line 164
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "DBG_WSS_LF"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 184
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 187
    invoke-direct {p0}, Lcom/LocalFota/ui/XUiAdminActivity;->unregisterReceiver()V

    .line 188
    return-void
.end method
