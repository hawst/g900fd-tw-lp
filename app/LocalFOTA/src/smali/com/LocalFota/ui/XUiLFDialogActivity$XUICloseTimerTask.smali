.class public Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;
.super Ljava/util/TimerTask;
.source "XUiLFDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/ui/XUiLFDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XUICloseTimerTask"
.end annotation


# instance fields
.field SelectDialog:I

.field final synthetic this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;


# direct methods
.method public constructor <init>(Lcom/LocalFota/ui/XUiLFDialogActivity;I)V
    .locals 1
    .param p2, "type"    # I

    .prologue
    .line 919
    iput-object p1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 916
    const/4 v0, 0x0

    iput v0, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 920
    iput p2, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->SelectDialog:I

    .line 921
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 932
    iget v1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->SelectDialog:I

    packed-switch v1, :pswitch_data_0

    .line 987
    :goto_0
    :pswitch_0
    return-void

    .line 937
    :pswitch_1
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 938
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->removeWiFiNetwork()V

    .line 939
    const/16 v1, 0xb

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 945
    :goto_1
    iget-object v1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    iget v2, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v1, v2}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 946
    iget-object v1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-virtual {v1}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 941
    :catch_0
    move-exception v0

    .line 943
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 953
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    const/4 v1, 0x0

    :try_start_1
    invoke-static {v1}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 954
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->removeWiFiNetwork()V

    .line 955
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWipeData()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 957
    const/16 v1, 0xe

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 969
    :goto_2
    iget-object v1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    iget v2, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->SelectDialog:I

    invoke-virtual {v1, v2}, Lcom/LocalFota/ui/XUiLFDialogActivity;->removeDialog(I)V

    .line 970
    iget-object v1, p0, Lcom/LocalFota/ui/XUiLFDialogActivity$XUICloseTimerTask;->this$0:Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-virtual {v1}, Lcom/LocalFota/ui/XUiLFDialogActivity;->finish()V

    goto :goto_0

    .line 962
    :cond_0
    const/16 v1, 0x11

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_2
    invoke-static {v1, v2, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 965
    :catch_1
    move-exception v0

    .line 967
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 976
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_3
    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_3
    invoke-static {v1, v2, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 979
    :catch_2
    move-exception v0

    .line 981
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 932
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public scheduledExecutionTime()J
    .locals 2

    .prologue
    .line 926
    invoke-super {p0}, Ljava/util/TimerTask;->scheduledExecutionTime()J

    move-result-wide v0

    return-wide v0
.end method
