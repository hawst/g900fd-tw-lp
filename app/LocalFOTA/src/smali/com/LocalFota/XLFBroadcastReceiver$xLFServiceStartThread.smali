.class Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;
.super Ljava/lang/Thread;
.source "XLFBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/XLFBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "xLFServiceStartThread"
.end annotation


# instance fields
.field mHandler:Landroid/os/Handler;

.field msg:Landroid/os/Message;


# direct methods
.method constructor <init>(Landroid/os/Handler;ILandroid/content/Intent;)V
    .locals 2
    .param p1, "h"    # Landroid/os/Handler;
    .param p2, "category"    # I
    .param p3, "receivedintent"    # Landroid/content/Intent;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    .line 120
    iput-object p1, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->mHandler:Landroid/os/Handler;

    .line 121
    iget-object v0, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    if-nez v0, :cond_0

    .line 123
    const-string v0, "DBG_WSS_LF"

    const-string v1, "msg is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput-object v0, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    iput p2, v0, Landroid/os/Message;->what:I

    .line 127
    iget-object v0, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 128
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 132
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Run Start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0x14

    if-ge v1, v2, :cond_0

    .line 136
    sget-boolean v2, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z

    if-eqz v2, :cond_1

    .line 150
    :cond_0
    iget-object v2, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/LocalFota/XLFBroadcastReceiver$xLFServiceStartThread;->msg:Landroid/os/Message;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 151
    return-void

    .line 141
    :cond_1
    :try_start_0
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Waiting for LF Application Start"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
