.class public Lcom/LocalFota/tp/XNetRetryConnectTimer$RetryConnectTimerTask;
.super Ljava/util/TimerTask;
.source "XNetRetryConnectTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XNetRetryConnectTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RetryConnectTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/LocalFota/tp/XNetRetryConnectTimer$RetryConnectTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    # getter for: Lcom/LocalFota/tp/XNetRetryConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->access$000()I

    move-result v0

    # getter for: Lcom/LocalFota/tp/XNetRetryConnectTimer;->retryAttatchTimer:I
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->access$100()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 66
    const/4 v0, 0x0

    # setter for: Lcom/LocalFota/tp/XNetRetryConnectTimer;->connectcount:I
    invoke-static {v0}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->access$002(I)I

    .line 67
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->endTimer()V

    .line 68
    const-string v0, "DBG_WSS_LF"

    const-string v1, "===Retry Connect Start==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/4 v0, 0x4

    invoke-static {v0, v2, v2}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_0
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "== Retry Connect Timer Count["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/LocalFota/tp/XNetRetryConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    # operator++ for: Lcom/LocalFota/tp/XNetRetryConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->access$008()I

    goto :goto_0
.end method
