.class public Lcom/LocalFota/tp/XLFTpAdapter$UserCertificationAsyncTask;
.super Landroid/os/AsyncTask;
.source "XLFTpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XLFTpAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserCertificationAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 655
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 21
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 676
    const/16 v20, 0x0

    .line 677
    .local v20, "uri":Ljava/net/URI;
    const/4 v7, 0x0

    .line 678
    .local v7, "bRet":Z
    const/4 v13, 0x0

    .line 682
    .local v13, "nReturnCode":I
    :try_start_0
    const-string v1, "http"

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x20c3

    const-string v4, "/auth"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v20

    .line 684
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v1

    sput-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 685
    new-instance v14, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 688
    .local v14, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v1, "Connection"

    const-string v2, "Keep-Alive"

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v16

    .line 690
    .local v16, "szDevID":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/LocalFota/db/XLFBase64;->encode([B)[B

    move-result-object v8

    .line 691
    .local v8, "byteDevID":[B
    new-instance v19, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>([B)V

    .line 692
    .local v19, "szSendData":Ljava/lang/String;
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szSendData "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decode szSendData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v4}, Lcom/LocalFota/db/XLFBase64;->decode([B)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v1, "Source-ID"

    move-object/from16 v0, v19

    invoke-virtual {v14, v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    const-string v1, "Tag-Number"

    sget-object v2, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v1, "Source-Model"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    const-string v1, "Source-AP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetFwV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    const-string v1, "Source-CP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetPhoneV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    const-string v1, "Source-CSC"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetCscV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const-string v1, "Client-AppVer"

    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReleaseVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    new-instance v1, Lcom/LocalFota/tp/XNetworkConnectTimer;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/LocalFota/tp/XNetworkConnectTimer;-><init>(Z)V

    .line 703
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v14}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    .line 704
    .local v15, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 706
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    .line 707
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HTTP Response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v11

    .line 711
    .local v11, "headers":[Lorg/apache/http/Header;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    array-length v1, v11

    if-ge v12, v1, :cond_0

    .line 713
    aget-object v10, v11, v12

    .line 714
    .local v10, "h":Lorg/apache/http/Header;
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Header["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 717
    .end local v10    # "h":Lorg/apache/http/Header;
    :cond_0
    const/16 v1, 0xc8

    if-ne v13, v1, :cond_1

    .line 719
    const-string v1, "Source-ID"

    invoke-interface {v15, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v18

    .line 720
    .local v18, "szReceiveData":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v17

    .line 721
    .local v17, "szDeviceID":Ljava/lang/String;
    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 723
    const-string v1, "DBG_WSS_LF"

    const-string v2, "Auth OK"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 724
    const/4 v7, 0x1

    .line 743
    .end local v17    # "szDeviceID":Ljava/lang/String;
    .end local v18    # "szReceiveData":Ljava/lang/String;
    :cond_1
    :goto_1
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_2

    .line 744
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 745
    :cond_2
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 747
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 750
    .end local v8    # "byteDevID":[B
    .end local v11    # "headers":[Lorg/apache/http/Header;
    .end local v12    # "i":I
    .end local v14    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "resp":Lorg/apache/http/HttpResponse;
    .end local v16    # "szDevID":Ljava/lang/String;
    .end local v19    # "szSendData":Ljava/lang/String;
    :goto_2
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 729
    .restart local v8    # "byteDevID":[B
    .restart local v11    # "headers":[Lorg/apache/http/Header;
    .restart local v12    # "i":I
    .restart local v14    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v15    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v16    # "szDevID":Ljava/lang/String;
    .restart local v17    # "szDeviceID":Ljava/lang/String;
    .restart local v18    # "szReceiveData":Ljava/lang/String;
    .restart local v19    # "szSendData":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    const/16 v2, 0x16

    :try_start_1
    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 733
    .end local v8    # "byteDevID":[B
    .end local v11    # "headers":[Lorg/apache/http/Header;
    .end local v12    # "i":I
    .end local v14    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "resp":Lorg/apache/http/HttpResponse;
    .end local v16    # "szDevID":Ljava/lang/String;
    .end local v17    # "szDeviceID":Ljava/lang/String;
    .end local v18    # "szReceiveData":Ljava/lang/String;
    .end local v19    # "szSendData":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 735
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/LocalFota/tp/XLFTpAdapter$UserCertificationAsyncTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 737
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    const/16 v1, 0xd

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 743
    :cond_4
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_5

    .line 744
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 745
    :cond_5
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 747
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto :goto_2

    .line 743
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_6

    .line 744
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 745
    :cond_6
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 747
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    throw v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 655
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$UserCertificationAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x0

    .line 757
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 759
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 761
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    .line 763
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 765
    const/16 v0, 0x10

    invoke-static {v0, v3, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 767
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 655
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$UserCertificationAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 662
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 664
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiDownloadProgressInit()V

    .line 666
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFSetUsercancelFlag(Z)V

    .line 667
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->endTimer()V

    .line 668
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 669
    return-void
.end method
