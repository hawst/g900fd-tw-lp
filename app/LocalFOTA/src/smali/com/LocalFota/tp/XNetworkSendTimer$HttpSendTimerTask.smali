.class public Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;
.super Ljava/util/TimerTask;
.source "XNetworkSendTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XNetworkSendTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpSendTimerTask"
.end annotation


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 61
    # getter for: Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->access$000()I

    move-result v0

    # getter for: Lcom/LocalFota/tp/XNetworkSendTimer;->SendingTimer:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->access$100()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 63
    const/4 v0, 0x0

    # setter for: Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I
    invoke-static {v0}, Lcom/LocalFota/tp/XNetworkSendTimer;->access$002(I)I

    .line 64
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->endTimer()V

    .line 65
    const-string v0, "DBG_WSS_LF"

    const-string v1, "===Send Fail==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFXXXFail()V

    .line 76
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string v0, "DBG_WSS_LF"

    const-string v1, "============================="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "== send Timer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const-string v0, "DBG_WSS_LF"

    const-string v1, "============================="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    # operator++ for: Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->access$008()I

    goto :goto_0
.end method
