.class public Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;
.super Ljava/util/TimerTask;
.source "XNetworkRecvTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XNetworkRecvTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpRecvTimerTask"
.end annotation


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 62
    # getter for: Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->access$000()I

    move-result v0

    # getter for: Lcom/LocalFota/tp/XNetworkRecvTimer;->ReceiveTimer:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->access$100()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 64
    const/4 v0, 0x0

    # setter for: Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I
    invoke-static {v0}, Lcom/LocalFota/tp/XNetworkRecvTimer;->access$002(I)I

    .line 65
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->endTimer()V

    .line 66
    const-string v0, "DBG_WSS_LF"

    const-string v1, "===Receive Fail==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFXXXFail()V

    .line 77
    :goto_0
    return-void

    .line 72
    :cond_0
    const-string v0, "DBG_WSS_LF"

    const-string v1, "============================="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "== recv Timer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v0, "DBG_WSS_LF"

    const-string v1, "============================="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    # operator++ for: Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->access$008()I

    goto :goto_0
.end method
