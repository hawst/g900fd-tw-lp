.class public Lcom/LocalFota/tp/XWiFiConnectTimer;
.super Ljava/lang/Object;
.source "XWiFiConnectTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;
    }
.end annotation


# static fields
.field private static connectTimer:Ljava/util/Timer;

.field private static connectcount:I

.field private static wifiAttatchTimer:I

.field private static wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    sput-object v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    .line 16
    sput-object v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    .line 17
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    .line 18
    const/16 v0, 0x3c

    sput v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiAttatchTimer:I

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "status"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    invoke-direct {v0}, Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;-><init>()V

    sput-object v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    .line 25
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    .line 26
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->startTimer()V

    .line 27
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 13
    sget v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 13
    sput p0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 13
    sget v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 13
    sget v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiAttatchTimer:I

    return v0
.end method

.method public static endTimer()V
    .locals 3

    .prologue
    .line 38
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I

    .line 40
    sget-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    if-nez v1, :cond_1

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 43
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v1, "DBG_WSS_LF"

    const-string v2, "=====================>> endTimer(WiFi connect)"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sget-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 45
    sget-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    invoke-virtual {v1}, Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;->cancel()Z

    .line 47
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    .line 48
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static startTimer()V
    .locals 6

    .prologue
    .line 31
    sget-object v0, Lcom/LocalFota/tp/XWiFiConnectTimer;->connectTimer:Ljava/util/Timer;

    sget-object v1, Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiConnectTask:Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    const-wide/16 v4, 0x1388

    invoke-virtual {v0, v1, v2, v4, v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 32
    return-void
.end method
