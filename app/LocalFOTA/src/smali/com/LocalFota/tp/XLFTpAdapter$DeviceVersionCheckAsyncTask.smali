.class public Lcom/LocalFota/tp/XLFTpAdapter$DeviceVersionCheckAsyncTask;
.super Landroid/os/AsyncTask;
.source "XLFTpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XLFTpAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceVersionCheckAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 770
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 15
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 789
    const/4 v14, 0x0

    .line 790
    .local v14, "uri":Ljava/net/URI;
    const/4 v6, 0x0

    .line 791
    .local v6, "bRet":Z
    const/4 v11, 0x0

    .line 795
    .local v11, "nReturnCode":I
    :try_start_0
    const-string v0, "http"

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x20c4

    const-string v3, "/check"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v14

    .line 797
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v0

    sput-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 798
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v12, v14}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 801
    .local v12, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v0, "Connection"

    const-string v1, "Keep-Alive"

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    const-string v0, "S-ID"

    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->S_ID:Ljava/lang/String;

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const-string v0, "Source-ID"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    const-string v0, "Source-Model"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    const-string v0, "Source-AP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetFwV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const-string v0, "Source-CP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetPhoneV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    const-string v0, "Source-CSC"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetCscV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    const-string v0, "Tag-Number"

    sget-object v1, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    const-string v0, "Client-AppVer"

    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReleaseVer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    new-instance v0, Lcom/LocalFota/tp/XNetworkConnectTimer;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/LocalFota/tp/XNetworkConnectTimer;-><init>(Z)V

    .line 812
    sget-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 813
    .local v13, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 815
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    .line 816
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP Response : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v9

    .line 819
    .local v9, "headers":[Lorg/apache/http/Header;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    array-length v0, v9

    if-ge v10, v0, :cond_0

    .line 821
    aget-object v8, v9, v10

    .line 822
    .local v8, "h":Lorg/apache/http/Header;
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Header["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 835
    .end local v8    # "h":Lorg/apache/http/Header;
    :cond_0
    sget-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_1

    .line 836
    sget-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 837
    :cond_1
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 839
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 842
    .end local v9    # "headers":[Lorg/apache/http/Header;
    .end local v10    # "i":I
    .end local v12    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v13    # "resp":Lorg/apache/http/HttpResponse;
    :goto_1
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 825
    :catch_0
    move-exception v7

    .line 827
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/LocalFota/tp/XLFTpAdapter$DeviceVersionCheckAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 829
    const-string v0, "DBG_WSS_LF"

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    const/16 v0, 0xd

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    :cond_2
    sget-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_3

    .line 836
    sget-object v0, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 837
    :cond_3
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 839
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto :goto_1

    .line 835
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_4

    .line 836
    sget-object v1, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 837
    :cond_4
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 839
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    throw v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 770
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$DeviceVersionCheckAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x0

    .line 849
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 851
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 853
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    .line 855
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFBatteryLifeCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 857
    const-string v0, "DBG_WSS_LF"

    const-string v1, "Low Battery"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    const-string v0, "Low Battery"

    invoke-static {v0}, Lcom/LocalFota/db/XDB;->xdbSetDownloadStatus(Ljava/lang/String;)V

    .line 860
    const/16 v0, 0x18

    invoke-static {v3, v0}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 877
    :goto_0
    return-void

    .line 865
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 868
    :pswitch_1
    const/4 v0, 0x6

    invoke-static {v0, v3, v3}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 871
    :pswitch_2
    const/16 v0, 0x15

    invoke-static {v3, v0}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 874
    :pswitch_3
    const/16 v0, 0x14

    invoke-static {v3, v0}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 865
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 770
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$DeviceVersionCheckAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 776
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 777
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 780
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->endTimer()V

    .line 781
    return-void
.end method
