.class public Lcom/LocalFota/tp/XNetworkRecvTimer;
.super Ljava/lang/Object;
.source "XNetworkRecvTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;
    }
.end annotation


# static fields
.field private static ReceiveTimer:I

.field private static recvTimer:Ljava/util/Timer;

.field private static recvcount:I

.field private static tprecvtimer:Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-object v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvTimer:Ljava/util/Timer;

    .line 15
    sput-object v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->tprecvtimer:Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;

    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    .line 17
    const/16 v0, 0x3c

    sput v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->ReceiveTimer:I

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 12
    sput p0, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkRecvTimer;->ReceiveTimer:I

    return v0
.end method

.method public static endTimer()V
    .locals 3

    .prologue
    .line 35
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvcount:I

    .line 37
    sget-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->tprecvtimer:Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;

    if-nez v1, :cond_1

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 40
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v1, "DBG_WSS_LF"

    const-string v2, "=====================>> endTimer(recv)"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    sget-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 42
    sget-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->tprecvtimer:Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;

    invoke-virtual {v1}, Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;->cancel()Z

    .line 44
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->recvTimer:Ljava/util/Timer;

    .line 45
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XNetworkRecvTimer;->tprecvtimer:Lcom/LocalFota/tp/XNetworkRecvTimer$HttpRecvTimerTask;

    .line 47
    invoke-static {}, Lcom/LocalFota/tp/XNetworkSendTimer;->endTimer()V

    .line 48
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 52
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
