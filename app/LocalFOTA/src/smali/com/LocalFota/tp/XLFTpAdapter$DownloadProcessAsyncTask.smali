.class public Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;
.super Landroid/os/AsyncTask;
.source "XLFTpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XLFTpAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadProcessAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field bMemcheck:Z

.field nResponse:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 183
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 185
    iput v0, p0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    .line 186
    iput-boolean v0, p0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->bMemcheck:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 33
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 206
    const-string v2, "DBG_WSS_LF"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/16 v32, 0x0

    .line 210
    .local v32, "uri":Ljava/net/URI;
    const/16 v26, 0x0

    .line 211
    .local v26, "nStatus":I
    const/4 v11, 0x0

    .line 212
    .local v11, "fileOutput":Ljava/io/FileOutputStream;
    const/16 v16, 0x0

    .line 213
    .local v16, "istream":Ljava/io/InputStream;
    const/4 v8, 0x0

    .line 214
    .local v8, "bRet":Z
    const-wide/16 v22, 0x0

    .line 218
    .local v22, "nFileSize":J
    :try_start_0
    const-string v2, "http"

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x20c5

    const-string v5, "/download"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v32

    .line 220
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    sput-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 221
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v32

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    sput-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 223
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFCurrentStatus()I

    move-result v26

    .line 224
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdbGetLFCurrentStatus : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Connection"

    const-string v4, "Keep-Alive"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "S-ID"

    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->S_ID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Source-ID"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Source-Model"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetModel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Source-AP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetFwV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Source-CP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetPhoneV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Source-CSC"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetCscV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Tag-Number"

    sget-object v4, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/16 v2, 0x14

    move/from16 v0, v26

    if-ne v0, v2, :cond_0

    .line 238
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v31

    .line 239
    .local v31, "szPath":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 240
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 241
    .local v17, "local":Ljava/io/File;
    invoke-static/range {v31 .. v31}, Lcom/LocalFota/db/XDBAdapter;->xLFFileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 243
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v22

    .line 244
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local file length : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Range"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .end local v17    # "local":Ljava/io/File;
    .end local v31    # "szPath":Ljava/lang/String;
    :cond_0
    :goto_0
    new-instance v2, Lcom/LocalFota/tp/XNetworkConnectTimer;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/LocalFota/tp/XNetworkConnectTimer;-><init>(Z)V

    .line 256
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    sget-object v3, Lcom/LocalFota/tp/XLFTpAdapter;->m_HttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-interface {v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v29

    .line 257
    .local v29, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 259
    const-wide/16 v24, 0x0

    .line 260
    .local v24, "nObjectSize":J
    const-wide/16 v20, 0x0

    .line 261
    .local v20, "nDeltaCRC":J
    const-string v30, ""

    .line 262
    .local v30, "szNewver":Ljava/lang/String;
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    .line 263
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v14

    .line 265
    .local v14, "headers":[Lorg/apache/http/Header;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    array-length v2, v14

    if-ge v15, v2, :cond_6

    .line 267
    aget-object v13, v14, v15

    .line 268
    .local v13, "h":Lorg/apache/http/Header;
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Header["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v13}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v13}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 250
    .end local v13    # "h":Lorg/apache/http/Header;
    .end local v14    # "headers":[Lorg/apache/http/Header;
    .end local v15    # "i":I
    .end local v20    # "nDeltaCRC":J
    .end local v24    # "nObjectSize":J
    .end local v29    # "resp":Lorg/apache/http/HttpResponse;
    .end local v30    # "szNewver":Ljava/lang/String;
    .restart local v17    # "local":Ljava/io/File;
    .restart local v31    # "szPath":Ljava/lang/String;
    :cond_1
    const-string v2, "DBG_WSS_LF"

    const-string v3, "local is not exists!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 386
    .end local v17    # "local":Ljava/io/File;
    .end local v31    # "szPath":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 388
    .local v10, "e":Ljava/lang/RuntimeException;
    :goto_2
    :try_start_1
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 390
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 391
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/16 v2, 0xa

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 394
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 395
    const/16 v2, 0xd

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416
    :cond_2
    :goto_3
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_3

    .line 417
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 418
    :cond_3
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 420
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 424
    if-eqz v11, :cond_4

    .line 426
    :try_start_2
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 436
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :cond_4
    :goto_4
    if-eqz v16, :cond_5

    .line 438
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    .line 446
    :cond_5
    :goto_5
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    .line 270
    .restart local v14    # "headers":[Lorg/apache/http/Header;
    .restart local v15    # "i":I
    .restart local v20    # "nDeltaCRC":J
    .restart local v24    # "nObjectSize":J
    .restart local v29    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v30    # "szNewver":Ljava/lang/String;
    :cond_6
    :try_start_4
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Server Response code : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    move-object/from16 v0, p0

    iget v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1a

    .line 274
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFSaveDeltaFileIndex()I

    move-result v19

    .line 275
    .local v19, "nDeltaFileIndex":I
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xdbGetLFSaveDeltaFileIndex : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStartTime:J

    .line 277
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m_ServerAttachStartTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStartTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-interface/range {v29 .. v29}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v16

    .line 280
    :goto_6
    if-eqz v16, :cond_16

    .line 282
    const/high16 v2, 0x40000

    new-array v9, v2, [B

    .line 283
    .local v9, "bytes":[B
    const/16 v18, 0x0

    .line 284
    .local v18, "nCur":I
    const/16 v27, 0x0

    .line 286
    .local v27, "nTotal":I
    if-nez v26, :cond_13

    .line 289
    const-string v2, "Content-Length"

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    .line 291
    const-string v2, "Delta-CRC"

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 292
    const-string v2, "New-Version"

    move-object/from16 v0, v29

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v30

    .line 293
    const-wide/16 v2, 0x0

    cmp-long v2, v24, v2

    if-lez v2, :cond_7

    .line 295
    invoke-static/range {v24 .. v25}, Lcom/LocalFota/db/XDB;->xdbSetDeltaSize(J)V

    .line 296
    const-wide/32 v2, 0x1400000

    add-long v2, v2, v24

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/db/XDBAdapter;->xLFMemoryCheck(JZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->bMemcheck:Z

    .line 298
    :cond_7
    const-wide/16 v2, 0x0

    cmp-long v2, v20, v2

    if-eqz v2, :cond_8

    .line 300
    invoke-static/range {v20 .. v21}, Lcom/LocalFota/db/XDB;->xdbSetLFDeltaCRC(J)V

    .line 303
    :cond_8
    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 304
    invoke-static/range {v30 .. v30}, Lcom/LocalFota/db/XDB;->xdbSetNewVersionName(Ljava/lang/String;)V

    .line 313
    :cond_9
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->bMemcheck:Z

    if-eqz v2, :cond_19

    .line 315
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v31

    .line 316
    .restart local v31    # "szPath":Ljava/lang/String;
    new-instance v28, Ljava/io/File;

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 317
    .local v28, "path":Ljava/io/File;
    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_a

    .line 319
    invoke-virtual/range {v28 .. v28}, Ljava/io/File;->mkdirs()Z

    .line 321
    :cond_a
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 322
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 323
    .restart local v17    # "local":Ljava/io/File;
    invoke-static/range {v31 .. v31}, Lcom/LocalFota/db/XDBAdapter;->xLFFileExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_14

    .line 325
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "local file length : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    new-instance v12, Ljava/io/FileOutputStream;

    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-direct {v12, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    .end local v11    # "fileOutput":Ljava/io/FileOutputStream;
    .local v12, "fileOutput":Ljava/io/FileOutputStream;
    move-object v11, v12

    .line 334
    .end local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutput":Ljava/io/FileOutputStream;
    :goto_8
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 335
    const/16 v2, 0x14

    invoke-static {v2}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 337
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDeltaSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiSetDeltaTotalSize(J)V

    .line 338
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiDownloadTimeInit()V

    .line 340
    :goto_9
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v2

    if-nez v2, :cond_15

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/io/InputStream;->read([B)I

    move-result v18

    if-lez v18, :cond_15

    .line 342
    const/4 v2, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v9, v2, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 343
    add-int v27, v27, v18

    .line 346
    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_9

    .line 400
    .end local v9    # "bytes":[B
    .end local v14    # "headers":[Lorg/apache/http/Header;
    .end local v15    # "i":I
    .end local v17    # "local":Ljava/io/File;
    .end local v18    # "nCur":I
    .end local v19    # "nDeltaFileIndex":I
    .end local v20    # "nDeltaCRC":J
    .end local v24    # "nObjectSize":J
    .end local v27    # "nTotal":I
    .end local v28    # "path":Ljava/io/File;
    .end local v29    # "resp":Lorg/apache/http/HttpResponse;
    .end local v30    # "szNewver":Ljava/lang/String;
    .end local v31    # "szPath":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 402
    .local v10, "e":Ljava/lang/Exception;
    :goto_a
    :try_start_5
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_b

    .line 404
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 405
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const/16 v2, 0xa

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 408
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 409
    const/16 v2, 0xd

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 416
    :cond_b
    :goto_b
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_c

    .line 417
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 418
    :cond_c
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 420
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 424
    if-eqz v11, :cond_d

    .line 426
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    .line 436
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_d
    :goto_c
    if-eqz v16, :cond_5

    .line 438
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_5

    .line 441
    :catch_2
    move-exception v10

    .line 443
    .local v10, "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 279
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v14    # "headers":[Lorg/apache/http/Header;
    .restart local v15    # "i":I
    .restart local v19    # "nDeltaFileIndex":I
    .restart local v20    # "nDeltaCRC":J
    .restart local v24    # "nObjectSize":J
    .restart local v29    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v30    # "szNewver":Ljava/lang/String;
    :cond_e
    const/16 v16, 0x0

    goto/16 :goto_6

    .line 306
    .restart local v9    # "bytes":[B
    .restart local v18    # "nCur":I
    .restart local v27    # "nTotal":I
    :cond_f
    :try_start_8
    const-string v2, "Do not Know"

    invoke-static {v2}, Lcom/LocalFota/db/XDB;->xdbSetNewVersionName(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_7

    .line 416
    .end local v9    # "bytes":[B
    .end local v14    # "headers":[Lorg/apache/http/Header;
    .end local v15    # "i":I
    .end local v18    # "nCur":I
    .end local v19    # "nDeltaFileIndex":I
    .end local v20    # "nDeltaCRC":J
    .end local v24    # "nObjectSize":J
    .end local v27    # "nTotal":I
    .end local v29    # "resp":Lorg/apache/http/HttpResponse;
    .end local v30    # "szNewver":Ljava/lang/String;
    :catchall_0
    move-exception v2

    :goto_d
    sget-object v3, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v3, :cond_10

    .line 417
    sget-object v3, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 418
    :cond_10
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 420
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 424
    if-eqz v11, :cond_11

    .line 426
    :try_start_9
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 436
    :cond_11
    :goto_e
    if-eqz v16, :cond_12

    .line 438
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 444
    :cond_12
    :goto_f
    throw v2

    .line 308
    .restart local v9    # "bytes":[B
    .restart local v14    # "headers":[Lorg/apache/http/Header;
    .restart local v15    # "i":I
    .restart local v18    # "nCur":I
    .restart local v19    # "nDeltaFileIndex":I
    .restart local v20    # "nDeltaCRC":J
    .restart local v24    # "nObjectSize":J
    .restart local v27    # "nTotal":I
    .restart local v29    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v30    # "szNewver":Ljava/lang/String;
    :cond_13
    const/16 v2, 0x14

    move/from16 v0, v26

    if-ne v0, v2, :cond_9

    .line 310
    :try_start_b
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDeltaSize()J

    move-result-wide v2

    sub-long v2, v2, v22

    const-wide/32 v4, 0x1400000

    add-long/2addr v2, v4

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/LocalFota/db/XDBAdapter;->xLFMemoryCheck(JZ)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->bMemcheck:Z

    goto/16 :goto_7

    .line 330
    .restart local v17    # "local":Ljava/io/File;
    .restart local v28    # "path":Ljava/io/File;
    .restart local v31    # "szPath":Ljava/lang/String;
    :cond_14
    new-instance v12, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 331
    .end local v11    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOutput":Ljava/io/FileOutputStream;
    :try_start_c
    const-string v2, "DBG_WSS_LF"

    const-string v3, "local is not exists!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_b
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move-object v11, v12

    .end local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutput":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .line 349
    :cond_15
    const/4 v8, 0x1

    .line 384
    .end local v9    # "bytes":[B
    .end local v17    # "local":Ljava/io/File;
    .end local v18    # "nCur":I
    .end local v19    # "nDeltaFileIndex":I
    .end local v27    # "nTotal":I
    .end local v28    # "path":Ljava/io/File;
    .end local v31    # "szPath":Ljava/lang/String;
    :cond_16
    :goto_10
    :try_start_d
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "xLFGetUsercancelFlag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 416
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_17

    .line 417
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 418
    :cond_17
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 420
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 424
    if-eqz v11, :cond_18

    .line 426
    :try_start_e
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4

    .line 436
    :cond_18
    :goto_11
    if-eqz v16, :cond_5

    .line 438
    :try_start_f
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3

    goto/16 :goto_5

    .line 441
    :catch_3
    move-exception v10

    .line 443
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 353
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v9    # "bytes":[B
    .restart local v18    # "nCur":I
    .restart local v19    # "nDeltaFileIndex":I
    .restart local v27    # "nTotal":I
    :cond_19
    :try_start_10
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Memory Full"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 355
    const/4 v8, 0x0

    goto :goto_10

    .line 361
    .end local v9    # "bytes":[B
    .end local v18    # "nCur":I
    .end local v19    # "nDeltaFileIndex":I
    .end local v27    # "nTotal":I
    :cond_1a
    if-eqz v26, :cond_1b

    .line 363
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 365
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFDeleteDeltaPkgFile()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 366
    const-string v2, "DBG_WSS_LF"

    const-string v3, "Delete Success.."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :cond_1b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v3, 0x194

    if-ne v2, v3, :cond_1d

    .line 371
    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 382
    :cond_1c
    :goto_12
    const/4 v8, 0x0

    goto :goto_10

    .line 373
    :cond_1d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v3, 0x195

    if-ne v2, v3, :cond_1e

    .line 375
    const/4 v2, 0x0

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_12

    .line 377
    :cond_1e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v3, 0xce

    if-ne v2, v3, :cond_1c

    .line 379
    const/4 v2, 0x0

    const/16 v3, 0x17

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_12

    .line 429
    :catch_4
    move-exception v10

    .line 431
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 397
    .end local v14    # "headers":[Lorg/apache/http/Header;
    .end local v15    # "i":I
    .end local v20    # "nDeltaCRC":J
    .end local v24    # "nObjectSize":J
    .end local v29    # "resp":Lorg/apache/http/HttpResponse;
    .end local v30    # "szNewver":Ljava/lang/String;
    .local v10, "e":Ljava/lang/RuntimeException;
    :cond_1f
    const/4 v2, 0x0

    const/4 v3, 0x2

    :try_start_11
    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_3

    .line 429
    :catch_5
    move-exception v10

    .line 431
    .local v10, "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 441
    .end local v10    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v10

    .line 443
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 411
    .local v10, "e":Ljava/lang/Exception;
    :cond_20
    const/4 v2, 0x0

    const/4 v3, 0x2

    :try_start_12
    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_b

    .line 429
    :catch_7
    move-exception v10

    .line 431
    .local v10, "e":Ljava/io/IOException;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_c

    .line 429
    .end local v10    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v10

    .line 431
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_e

    .line 441
    .end local v10    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v10

    .line 443
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v10}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    .line 416
    .end local v10    # "e":Ljava/io/IOException;
    .end local v11    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v9    # "bytes":[B
    .restart local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v14    # "headers":[Lorg/apache/http/Header;
    .restart local v15    # "i":I
    .restart local v17    # "local":Ljava/io/File;
    .restart local v18    # "nCur":I
    .restart local v19    # "nDeltaFileIndex":I
    .restart local v20    # "nDeltaCRC":J
    .restart local v24    # "nObjectSize":J
    .restart local v27    # "nTotal":I
    .restart local v28    # "path":Ljava/io/File;
    .restart local v29    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v30    # "szNewver":Ljava/lang/String;
    .restart local v31    # "szPath":Ljava/lang/String;
    :catchall_1
    move-exception v2

    move-object v11, v12

    .end local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutput":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .line 400
    .end local v11    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOutput":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v10

    move-object v11, v12

    .end local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutput":Ljava/io/FileOutputStream;
    goto/16 :goto_a

    .line 386
    .end local v11    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOutput":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v10

    move-object v11, v12

    .end local v12    # "fileOutput":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOutput":Ljava/io/FileOutputStream;
    goto/16 :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 183
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 11
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v10, 0x0

    .line 455
    :try_start_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 456
    const-string v6, "DBG_WSS_LF"

    const-string v7, ""

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 460
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v5

    .line 461
    .local v5, "szPath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 462
    .local v4, "path":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    .line 464
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 466
    :cond_0
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 467
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 469
    .local v1, "local":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 470
    .local v2, "filesize":J
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update filesize : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 474
    const/16 v6, 0xa

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 475
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 478
    const-string v6, "User Cancel"

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetDownloadStatus(Ljava/lang/String;)V

    .line 479
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sput-wide v6, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    .line 480
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "m_ServerAttachStopTime : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v8, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    :cond_1
    :goto_0
    invoke-static {v10}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 522
    .end local v1    # "local":Ljava/io/File;
    .end local v2    # "filesize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "szPath":Ljava/lang/String;
    :goto_1
    return-void

    .line 485
    .restart local v1    # "local":Ljava/io/File;
    .restart local v2    # "filesize":J
    .restart local v4    # "path":Ljava/io/File;
    .restart local v5    # "szPath":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_3

    iget v6, p0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_3

    .line 487
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDeltaSize()J

    move-result-wide v6

    cmp-long v6, v2, v6

    if-nez v6, :cond_1

    .line 489
    const/16 v6, 0x28

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 490
    const-string v6, "Download Success"

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetDownloadStatus(Ljava/lang/String;)V

    .line 491
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sput-wide v6, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    .line 492
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "m_ServerAttachStopTime : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v8, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/16 v6, 0xc

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 495
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 497
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 514
    .end local v1    # "local":Ljava/io/File;
    .end local v2    # "filesize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "szPath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 520
    invoke-static {v10}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto :goto_1

    .line 500
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "local":Ljava/io/File;
    .restart local v2    # "filesize":J
    .restart local v4    # "path":Ljava/io/File;
    .restart local v5    # "szPath":Ljava/lang/String;
    :cond_3
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->bMemcheck:Z

    if-nez v6, :cond_1

    iget v6, p0, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->nResponse:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 503
    const-string v6, "Memory Full"

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetDownloadStatus(Ljava/lang/String;)V

    .line 504
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sput-wide v6, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    .line 505
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "m_ServerAttachStopTime : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v8, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    const/16 v6, 0xc

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 508
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 520
    .end local v1    # "local":Ljava/io/File;
    .end local v2    # "filesize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "szPath":Ljava/lang/String;
    :catchall_0
    move-exception v6

    invoke-static {v10}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    throw v6
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 183
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$DownloadProcessAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 193
    const-string v0, "DBG_WSS_LF"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiDownloadProgressInit()V

    .line 196
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFSetUsercancelFlag(Z)V

    .line 198
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->endTimer()V

    .line 200
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 201
    return-void
.end method
