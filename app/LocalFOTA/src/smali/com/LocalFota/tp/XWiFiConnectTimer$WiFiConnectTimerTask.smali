.class public Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;
.super Ljava/util/TimerTask;
.source "XWiFiConnectTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XWiFiConnectTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WiFiConnectTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/LocalFota/tp/XWiFiConnectTimer$WiFiConnectTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 63
    # getter for: Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->access$000()I

    move-result v0

    # getter for: Lcom/LocalFota/tp/XWiFiConnectTimer;->wifiAttatchTimer:I
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->access$100()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 65
    const/4 v0, 0x0

    # setter for: Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I
    invoke-static {v0}, Lcom/LocalFota/tp/XWiFiConnectTimer;->access$002(I)I

    .line 66
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->endTimer()V

    .line 67
    const-string v0, "DBG_WSS_LF"

    const-string v1, "===Wifi Connect Fail==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->connectWiFiFail()V

    .line 80
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->connectWiFi()V

    .line 77
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "== WiFi Connect Timer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    # operator++ for: Lcom/LocalFota/tp/XWiFiConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XWiFiConnectTimer;->access$008()I

    goto :goto_0
.end method
