.class public Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;
.super Landroid/os/AsyncTask;
.source "XLFTpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XLFTpAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateResultReportAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field nResponse:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 525
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 528
    const/4 v0, 0x0

    iput v0, p0, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->nResponse:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 18
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 545
    const/4 v15, 0x0

    .line 546
    .local v15, "uri":Ljava/net/URI;
    const/4 v8, 0x0

    .line 550
    .local v8, "bRet":Z
    :try_start_0
    const-string v2, "http"

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetServerUrl()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x20c6

    const-string v5, "/report"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v15

    .line 552
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    sput-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 553
    new-instance v10, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v10, v15}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 555
    .local v10, "post":Lorg/apache/http/client/methods/HttpPost;
    new-instance v14, Lorg/apache/http/entity/StringEntity;

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDownloadStatus()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v14, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 558
    .local v14, "szEntity":Lorg/apache/http/entity/StringEntity;
    const-string v2, "Connection"

    const-string v3, "Keep-Alive"

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string v2, "S-ID"

    sget-object v3, Lcom/LocalFota/ui/XUiAdminActivity;->S_ID:Ljava/lang/String;

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string v2, "Source-ID"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetFullDeviceID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v2, "Source-Model"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetModel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v2, "Source-AP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetFwV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v2, "Source-CP"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetPhoneV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v2, "Source-CSC"

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetCscV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v2, "New-Version"

    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetNewVersionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v2, "Tag-Number"

    sget-object v3, Lcom/LocalFota/ui/XUiAdminActivity;->m_NFCTagNumber:Ljava/lang/String;

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    sget-wide v2, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStopTime:J

    sget-wide v4, Lcom/LocalFota/tp/XLFTpAdapter;->m_ServerAttachStartTime:J

    sub-long v16, v2, v4

    .line 568
    .local v16, "timerange":J
    const-wide/16 v2, 0x3e8

    rem-long v2, v16, v2

    sub-long v2, v16, v2

    const-wide/16 v4, 0x3e8

    div-long v12, v2, v4

    .line 569
    .local v12, "sendtimerange":J
    const-string v2, "Time-Range"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    invoke-virtual {v10, v14}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 572
    const/16 v2, 0x32

    invoke-static {v2}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 574
    new-instance v2, Lcom/LocalFota/tp/XNetworkConnectTimer;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/LocalFota/tp/XNetworkConnectTimer;-><init>(Z)V

    .line 575
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2, v10}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v11

    .line 576
    .local v11, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 578
    invoke-interface {v11}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->nResponse:I

    .line 579
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HTTP Response : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->nResponse:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    move-object/from16 v0, p0

    iget v2, v0, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->nResponse:I

    if-eqz v2, :cond_0

    .line 584
    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 585
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 588
    :cond_0
    const/4 v8, 0x1

    .line 601
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_1

    .line 602
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 603
    :cond_1
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 605
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 608
    .end local v10    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v11    # "resp":Lorg/apache/http/HttpResponse;
    .end local v12    # "sendtimerange":J
    .end local v14    # "szEntity":Lorg/apache/http/entity/StringEntity;
    .end local v16    # "timerange":J
    :goto_0
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    return-object v2

    .line 590
    :catch_0
    move-exception v9

    .line 592
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 594
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    const/16 v2, 0xd

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601
    :cond_2
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v2, :cond_3

    .line 602
    sget-object v2, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 603
    :cond_3
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 605
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto :goto_0

    .line 601
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v3, :cond_4

    .line 602
    sget-object v3, Lcom/LocalFota/tp/XLFTpAdapter;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 603
    :cond_4
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 605
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    throw v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 525
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 10
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v9, 0x0

    .line 615
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 617
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v5

    .line 618
    .local v5, "szPath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 619
    .local v4, "path":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    .line 621
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 623
    :cond_0
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 624
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 626
    .local v1, "local":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 627
    .local v2, "filesize":J
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update filesize : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    iget v6, p0, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->nResponse:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 631
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDownloadStatus()Ljava/lang/String;

    move-result-object v6

    const-string v7, "CRC Error"

    if-ne v6, v7, :cond_2

    .line 634
    const/4 v6, 0x0

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 635
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFDeleteDeltaPkgFile()Z

    move-result v0

    .line 636
    .local v0, "bChcek":Z
    if-eqz v0, :cond_1

    .line 637
    const-string v6, "DBG_WSS_LF"

    const-string v7, "Delete Success.."

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    .end local v0    # "bChcek":Z
    :cond_1
    :goto_0
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 652
    return-void

    .line 640
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    .line 642
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetDeltaSize()J

    move-result-wide v6

    cmp-long v6, v2, v6

    if-nez v6, :cond_1

    .line 644
    const/16 v6, 0x3c

    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 645
    const/16 v6, 0xc

    invoke-static {v6, v9, v9}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 646
    const/4 v6, 0x7

    invoke-static {v9, v6}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 525
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/LocalFota/tp/XLFTpAdapter$UpdateResultReportAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 534
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 536
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 537
    invoke-static {}, Lcom/LocalFota/tp/XNetRetryConnectTimer;->endTimer()V

    .line 538
    const/4 v0, 0x0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 539
    return-void
.end method
