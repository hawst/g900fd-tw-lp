.class public Lcom/LocalFota/tp/XNetworkSendTimer;
.super Ljava/lang/Object;
.source "XNetworkSendTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;
    }
.end annotation


# static fields
.field private static SendingTimer:I

.field private static sendTimer:Ljava/util/Timer;

.field private static sendcount:I

.field private static tpsendtimer:Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    sput-object v0, Lcom/LocalFota/tp/XNetworkSendTimer;->sendTimer:Ljava/util/Timer;

    .line 15
    sput-object v0, Lcom/LocalFota/tp/XNetworkSendTimer;->tpsendtimer:Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;

    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    .line 17
    const/16 v0, 0x3c

    sput v0, Lcom/LocalFota/tp/XNetworkSendTimer;->SendingTimer:I

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 12
    sput p0, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/LocalFota/tp/XNetworkSendTimer;->SendingTimer:I

    return v0
.end method

.method public static endTimer()V
    .locals 3

    .prologue
    .line 35
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/LocalFota/tp/XNetworkSendTimer;->sendcount:I

    .line 37
    sget-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->sendTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->tpsendtimer:Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;

    if-nez v1, :cond_1

    .line 53
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 40
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v1, "DBG_WSS_LF"

    const-string v2, "=====================>> endTimer(send)"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    sget-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->sendTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 42
    sget-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->tpsendtimer:Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;

    invoke-virtual {v1}, Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;->cancel()Z

    .line 43
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->sendTimer:Ljava/util/Timer;

    .line 44
    const/4 v1, 0x0

    sput-object v1, Lcom/LocalFota/tp/XNetworkSendTimer;->tpsendtimer:Lcom/LocalFota/tp/XNetworkSendTimer$HttpSendTimerTask;

    .line 46
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 47
    invoke-static {}, Lcom/LocalFota/tp/XNetworkRecvTimer;->endTimer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 51
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
