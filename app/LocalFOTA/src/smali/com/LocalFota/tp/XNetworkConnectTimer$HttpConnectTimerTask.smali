.class public Lcom/LocalFota/tp/XNetworkConnectTimer$HttpConnectTimerTask;
.super Ljava/util/TimerTask;
.source "XNetworkConnectTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/LocalFota/tp/XNetworkConnectTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpConnectTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/LocalFota/tp/XNetworkConnectTimer$HttpConnectTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 65
    # getter for: Lcom/LocalFota/tp/XNetworkConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->access$000()I

    move-result v0

    # getter for: Lcom/LocalFota/tp/XNetworkConnectTimer;->netAttatchTimer:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->access$100()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 67
    const/4 v0, 0x0

    # setter for: Lcom/LocalFota/tp/XNetworkConnectTimer;->connectcount:I
    invoke-static {v0}, Lcom/LocalFota/tp/XNetworkConnectTimer;->access$002(I)I

    .line 68
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->endTimer()V

    .line 69
    const-string v0, "DBG_WSS_LF"

    const-string v1, "===Connect Fail==="

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFXXXFail()V

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_0
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "== Connect Timer["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/LocalFota/tp/XNetworkConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    # operator++ for: Lcom/LocalFota/tp/XNetworkConnectTimer;->connectcount:I
    invoke-static {}, Lcom/LocalFota/tp/XNetworkConnectTimer;->access$008()I

    goto :goto_0
.end method
