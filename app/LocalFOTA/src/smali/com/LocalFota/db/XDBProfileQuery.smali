.class public Lcom/LocalFota/db/XDBProfileQuery;
.super Ljava/lang/Object;
.source "XDBProfileQuery.java"


# direct methods
.method public static xdbProfileExistsRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 145
    const/4 v10, 0x0

    .line 146
    .local v10, "bExist":Z
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "address"

    aput-object v1, v3, v4

    .line 150
    .local v3, "From":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 151
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 155
    .local v0, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 156
    const/4 v1, 0x1

    const-string v2, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 158
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    .line 159
    const/4 v10, 0x1

    .line 167
    :cond_0
    if-eqz v11, :cond_1

    .line 168
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 170
    :cond_1
    if-eqz v0, :cond_2

    .line 171
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 174
    :cond_2
    :goto_0
    return v10

    .line 161
    :catch_0
    move-exception v12

    .line 163
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    if-eqz v11, :cond_3

    .line 168
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 170
    :cond_3
    if-eqz v0, :cond_2

    .line 171
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 167
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 168
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 170
    :cond_4
    if-eqz v0, :cond_5

    .line 171
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdbProfileFetchRow(JLcom/LocalFota/db/XDBProfileInfo;)Ljava/lang/Object;
    .locals 12
    .param p0, "rowId"    # J
    .param p2, "profileInfo"    # Lcom/LocalFota/db/XDBProfileInfo;

    .prologue
    const/4 v4, 0x1

    .line 106
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "address"

    aput-object v1, v3, v4

    .line 110
    .local v3, "From":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 111
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 115
    .local v0, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 116
    const/4 v1, 0x1

    const-string v2, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 118
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 120
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v11

    .line 129
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    if-eqz v10, :cond_0

    .line 134
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_0
    if-eqz v0, :cond_1

    .line 137
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 140
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 133
    :cond_2
    if-eqz v10, :cond_3

    .line 134
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_3
    if-eqz v0, :cond_1

    .line 137
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 133
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 134
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 136
    :cond_4
    if-eqz v0, :cond_5

    .line 137
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdbProfileInit(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "Db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 18
    :try_start_0
    const-string v1, "create table if not exists profile (rowid integer primary key autoincrement, address text);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 22
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static xdbProfileInsertRow(Lcom/LocalFota/db/XDBProfileInfo;)V
    .locals 5
    .param p0, "profileInfo"    # Lcom/LocalFota/db/XDBProfileInfo;

    .prologue
    .line 40
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 41
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 45
    .local v1, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "address"

    iget-object v4, p0, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 48
    const-string v3, "profile"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    if-eqz v1, :cond_0

    .line 57
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v2

    .line 52
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    if-eqz v1, :cond_0

    .line 57
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 56
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 57
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdbProfileUpdateRow(JLcom/LocalFota/db/XDBProfileInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "profileInfo"    # Lcom/LocalFota/db/XDBProfileInfo;

    .prologue
    .line 83
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 84
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 88
    .local v1, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "address"

    iget-object v4, p2, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 91
    const-string v3, "profile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    if-eqz v1, :cond_0

    .line 100
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v2

    .line 95
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    if-eqz v1, :cond_0

    .line 100
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 99
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 100
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
