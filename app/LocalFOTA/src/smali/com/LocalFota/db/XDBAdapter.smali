.class public Lcom/LocalFota/db/XDBAdapter;
.super Ljava/lang/Object;
.source "XDBAdapter.java"


# direct methods
.method public static getCRC32Value(Ljava/lang/String;)J
    .locals 9
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 231
    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    .line 232
    .local v1, "crc":Ljava/util/zip/Checksum;
    const/4 v3, 0x0

    .line 236
    .local v3, "in":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .local v4, "in":Ljava/io/BufferedInputStream;
    const v6, 0x8000

    :try_start_1
    new-array v0, v6, [B

    .line 238
    .local v0, "buffer":[B
    const/4 v5, 0x0

    .line 240
    .local v5, "length":I
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v5

    if-ltz v5, :cond_1

    .line 241
    const/4 v6, 0x0

    invoke-interface {v1, v0, v6, v5}, Ljava/util/zip/Checksum;->update([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 243
    .end local v0    # "buffer":[B
    .end local v5    # "length":I
    :catch_0
    move-exception v2

    move-object v3, v4

    .line 245
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    :goto_1
    :try_start_2
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    if-eqz v3, :cond_0

    .line 253
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 262
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v6

    return-wide v6

    .line 251
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v5    # "length":I
    :cond_1
    if-eqz v4, :cond_2

    .line 253
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v3, v4

    .line 259
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 256
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catch_1
    move-exception v2

    .line 258
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 260
    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 256
    .end local v0    # "buffer":[B
    .end local v5    # "length":I
    :catch_2
    move-exception v2

    .line 258
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 249
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 251
    :goto_3
    if-eqz v3, :cond_3

    .line 253
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 259
    :cond_3
    :goto_4
    throw v6

    .line 256
    :catch_3
    move-exception v2

    .line 258
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 249
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/BufferedInputStream;
    .restart local v4    # "in":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedInputStream;
    .restart local v3    # "in":Ljava/io/BufferedInputStream;
    goto :goto_3

    .line 243
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method public static xLFCheckAvailableCacheMemory(J)Z
    .locals 6
    .param p0, "nPkgSize"    # J

    .prologue
    .line 173
    const-wide/16 v2, 0x0

    .line 174
    .local v2, "nFreeSpace":J
    const/4 v0, 0x0

    .line 176
    .local v0, "bRtn":Z
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/LocalFota/db/XDBAdapter;->xLFGetAvailableMemorySize(I)J

    move-result-wide v2

    .line 177
    const-string v1, "DBG_WSS_LF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "freeSpace = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nPkgSize = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    cmp-long v1, v2, p0

    if-ltz v1, :cond_0

    .line 180
    const/4 v0, 0x1

    .line 182
    :cond_0
    return v0
.end method

.method public static xLFCheckAvailableInternalMemory(J)Z
    .locals 12
    .param p0, "nPkgSize"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 316
    const-wide/16 v4, 0x0

    .line 317
    .local v4, "lDataCheckSize":J
    const-wide/16 v0, 0x0

    .line 318
    .local v0, "RemainSize":J
    const-wide/16 v2, 0x0

    .line 320
    .local v2, "TotalSize":J
    invoke-static {v6}, Lcom/LocalFota/db/XDBAdapter;->xLFGetAvailableMemorySize(I)J

    move-result-wide v0

    .line 321
    invoke-static {v6}, Lcom/LocalFota/db/XDBAdapter;->xLFGetTotalMemorySize(I)J

    move-result-wide v2

    .line 322
    const-wide/16 v8, 0xa

    div-long v4, v2, v8

    .line 324
    add-long v8, p0, v4

    cmp-long v8, v8, v0

    if-gtz v8, :cond_0

    .line 326
    const-string v8, "DBG_WSS_LF"

    const-string v9, "Remain size : %d, Total size : %d and Delta Size : %d bytes"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v10, v6

    const/4 v7, 0x2

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v7

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    return v6

    :cond_0
    move v6, v7

    goto :goto_0
.end method

.method public static xLFCheckDeltaCRC()Z
    .locals 9

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    .local v0, "bRtn":Z
    const-wide/16 v4, 0x0

    .line 211
    .local v4, "downloadedfileCRC":J
    const-wide/16 v2, 0x0

    .line 213
    .local v2, "deltaCRC":J
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "szPath":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 215
    invoke-static {v1}, Lcom/LocalFota/db/XDBAdapter;->getCRC32Value(Ljava/lang/String;)J

    move-result-wide v4

    .line 216
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFDeltaCRC()J

    move-result-wide v2

    .line 218
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "downloadedfileCRC : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const-string v6, "DBG_WSS_LF"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deltaCRC : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    cmp-long v6, v4, v2

    if-nez v6, :cond_0

    .line 222
    const/4 v0, 0x1

    .line 226
    :goto_0
    return v0

    .line 224
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xLFDeleteDeltaPkgFile()Z
    .locals 5

    .prologue
    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "bRet":Z
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaPkgFile()Ljava/io/File;

    move-result-object v2

    .line 122
    .local v2, "local":Ljava/io/File;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    const/4 v0, 0x1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 128
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 131
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static xLFDeltaAllClear()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 66
    const/4 v1, 0x2

    .line 67
    .local v1, "nCheckMemoryNum":I
    const-string v2, ""

    .line 69
    .local v2, "szFilePath":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 71
    const-string v2, ""

    .line 72
    packed-switch v0, :pswitch_data_0

    .line 84
    :goto_1
    const-string v3, "DBG_WSS_LF"

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    invoke-static {v2}, Lcom/LocalFota/db/XDBAdapter;->xLFFileExists(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    invoke-static {v2}, Lcom/LocalFota/db/XDBAdapter;->xLFFileDelete(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    const-string v3, "DBG_WSS_LF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileDelete : true ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :pswitch_0
    const-string v3, "%s%s"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "/cache/lfota/"

    aput-object v5, v4, v6

    const-string v5, "update.zip"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 77
    goto :goto_1

    .line 80
    :pswitch_1
    const-string v3, "%s%s"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "/data/data/com.LocalFota/"

    aput-object v5, v4, v6

    const-string v5, "update.zip"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 94
    :cond_1
    return-void

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static xLFFileDelete(Ljava/lang/String;)Z
    .locals 4
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 100
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_0
    const/4 v2, 0x1

    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v2

    .line 104
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static xLFFileExists(Ljava/lang/String;)Z
    .locals 4
    .param p0, "pszFileName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 28
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    const-string v1, "DBG_WSS_LF"

    const-string v2, "File is Exist"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const/4 v1, 0x1

    .line 46
    :goto_0
    return v1

    .line 39
    :cond_0
    const-string v2, "DBG_WSS_LF"

    const-string v3, "File is not Exist"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :cond_1
    const-string v2, "DBG_WSS_LF"

    const-string v3, "File is not Exist"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static xLFGetAvailableMemorySize(I)J
    .locals 8
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 139
    const-string v5, ""

    .line 141
    .local v5, "szPath":Ljava/lang/String;
    invoke-static {p0}, Lcom/LocalFota/db/XDBAdapter;->xLFGetStoragePath(I)Ljava/lang/String;

    move-result-object v5

    .line 142
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 143
    const-wide/16 v6, 0x0

    .line 148
    :goto_0
    return-wide v6

    .line 145
    :cond_0
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 146
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 147
    .local v2, "blockSize":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 148
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    goto :goto_0
.end method

.method public static xLFGetDeltaFileID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "update.zip"

    return-object v0
.end method

.method public static xLFGetDeltaPkgFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 57
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "szPath":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "local":Ljava/io/File;
    return-object v0
.end method

.method private static xLFGetStoragePath(I)Ljava/lang/String;
    .locals 3
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 153
    const-string v0, ""

    .line 155
    .local v0, "szPath":Ljava/lang/String;
    if-nez p0, :cond_2

    .line 157
    const-string v0, "cache"

    .line 164
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    const-string v1, "DBG_WSS_LF"

    const-string v2, "getStoragePath is empty"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_1
    return-object v0

    .line 159
    :cond_2
    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    .line 161
    const-string v0, "data"

    goto :goto_0
.end method

.method public static xLFGetTotalMemorySize(I)J
    .locals 8
    .param p0, "nMemoryArea"    # I

    .prologue
    .line 187
    const-string v3, ""

    .line 189
    .local v3, "szPath":Ljava/lang/String;
    invoke-static {p0}, Lcom/LocalFota/db/XDBAdapter;->xLFGetStoragePath(I)Ljava/lang/String;

    move-result-object v3

    .line 190
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 191
    const-wide/16 v6, 0x0

    .line 196
    :goto_0
    return-wide v6

    .line 193
    :cond_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 194
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 195
    .local v0, "blockSize":J
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v4, v6

    .line 196
    .local v4, "totalBlocks":J
    mul-long v6, v4, v0

    goto :goto_0
.end method

.method public static xLFMemoryCheck(JZ)Z
    .locals 8
    .param p0, "nPkgSize"    # J
    .param p2, "bResumeMode"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "bRtn":Z
    const/4 v2, 0x0

    .line 269
    .local v2, "nStatus":I
    const/4 v1, 0x0

    .line 271
    .local v1, "nDeltaFileIndex":I
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFCurrentStatus()I

    move-result v2

    .line 272
    const-string v5, "DBG_WSS_LF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "xdbGetLFCurrentStatus : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFSaveDeltaFileIndex()I

    move-result v1

    .line 274
    const-string v5, "DBG_WSS_LF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "xdbGetLFSaveDeltaFileIndex : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    if-eqz p2, :cond_1

    .line 278
    invoke-static {p0, p1, v1}, Lcom/LocalFota/db/XDBAdapter;->xLFMemoryCheckForResume(JI)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 280
    const-string v4, "DBG_WSS_LF"

    const-string v5, "Device will resume download"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_0
    :goto_0
    return v3

    .line 286
    :cond_1
    const-string v5, "DBG_WSS_LF"

    const-string v6, "Check cache memory"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-static {p0, p1}, Lcom/LocalFota/db/XDBAdapter;->xLFCheckAvailableCacheMemory(J)Z

    move-result v0

    .line 288
    if-eqz v0, :cond_2

    .line 290
    const-string v5, "DBG_WSS_LF"

    const-string v6, "Cache memory >>> OK..."

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    if-nez v2, :cond_0

    .line 292
    invoke-static {v4}, Lcom/LocalFota/db/XDB;->xdbSetLFSaveDeltaFileIndex(I)V

    goto :goto_0

    .line 298
    :cond_2
    const-string v5, "DBG_WSS_LF"

    const-string v6, "Check internal memory"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {p0, p1}, Lcom/LocalFota/db/XDBAdapter;->xLFCheckAvailableInternalMemory(J)Z

    move-result v0

    .line 300
    if-eqz v0, :cond_3

    .line 302
    const-string v4, "DBG_WSS_LF"

    const-string v5, "Interior memory >>> OK..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    if-nez v2, :cond_0

    .line 304
    invoke-static {v3}, Lcom/LocalFota/db/XDB;->xdbSetLFSaveDeltaFileIndex(I)V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 311
    goto :goto_0
.end method

.method public static xLFMemoryCheckForResume(JI)Z
    .locals 4
    .param p0, "nPkgSize"    # J
    .param p2, "index"    # I

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 337
    .local v0, "bRtn":Z
    packed-switch p2, :pswitch_data_0

    .line 346
    const-string v1, "DBG_WSS_LF"

    const-string v2, "Not define"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :goto_0
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return v0

    .line 340
    :pswitch_0
    invoke-static {p0, p1}, Lcom/LocalFota/db/XDBAdapter;->xLFCheckAvailableCacheMemory(J)Z

    move-result v0

    .line 341
    goto :goto_0

    .line 343
    :pswitch_1
    invoke-static {p0, p1}, Lcom/LocalFota/db/XDBAdapter;->xLFCheckAvailableInternalMemory(J)Z

    move-result v0

    .line 344
    goto :goto_0

    .line 337
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
