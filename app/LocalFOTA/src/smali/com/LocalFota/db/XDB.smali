.class public Lcom/LocalFota/db/XDB;
.super Ljava/lang/Object;
.source "XDB.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

.field public static m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    sput-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    .line 16
    sput-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    return-void
.end method

.method public static xdbDBInit()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 20
    new-instance v0, Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-direct {v0}, Lcom/LocalFota/db/XDBLocalFotaInfo;-><init>()V

    sput-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    .line 21
    new-instance v0, Lcom/LocalFota/db/XDBProfileInfo;

    invoke-direct {v0}, Lcom/LocalFota/db/XDBProfileInfo;-><init>()V

    sput-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    .line 23
    invoke-static {v2, v3}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoExistsRow(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoInsertRow(Lcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 32
    :goto_0
    invoke-static {v2, v3}, Lcom/LocalFota/db/XDBProfileQuery;->xdbProfileExistsRow(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    sget-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    invoke-static {v0}, Lcom/LocalFota/db/XDBProfileQuery;->xdbProfileInsertRow(Lcom/LocalFota/db/XDBProfileInfo;)V

    .line 40
    :goto_1
    return-void

    .line 29
    :cond_0
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v2, v3, v0}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoFetchRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)Ljava/lang/Object;

    goto :goto_0

    .line 38
    :cond_1
    sget-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    invoke-static {v2, v3, v0}, Lcom/LocalFota/db/XDBProfileQuery;->xdbProfileFetchRow(JLcom/LocalFota/db/XDBProfileInfo;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static xdbFileGetDeltaPathFromIndex()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    const/4 v1, 0x0

    .line 180
    .local v1, "szFileName":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFSaveDeltaFileIndex()I

    move-result v0

    .line 181
    .local v0, "nDeltaFileIndex":I
    if-nez v0, :cond_0

    .line 183
    const-string v1, "/cache/lfota/"

    .line 190
    :goto_0
    return-object v1

    .line 188
    :cond_0
    const-string v1, "/data/data/com.LocalFota/"

    goto :goto_0
.end method

.method public static xdbGetDeltaSize()J
    .locals 4

    .prologue
    .line 118
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbGetDeltaSize : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-wide v2, v2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-wide v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    return-wide v0
.end method

.method public static xdbGetDownloadStatus()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbGetDownloadStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-object v2, v2, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-object v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    return-object v0
.end method

.method public static xdbGetLFCurrentStatus()I
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    return v0
.end method

.method public static xdbGetLFDeltaCRC()J
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-wide v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    return-wide v0
.end method

.method public static xdbGetLFSaveDeltaFileIndex()I
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I

    return v0
.end method

.method public static xdbGetNewVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iget-object v0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public static xdbGetProfileInfo()Lcom/LocalFota/db/XDBProfileInfo;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    return-object v0
.end method

.method public static xdbGetServerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    iget-object v0, v0, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static xdbSetDeltaSize(J)V
    .locals 4
    .param p0, "nSize"    # J

    .prologue
    .line 124
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput-wide p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    .line 125
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 126
    return-void
.end method

.method public static xdbSetDownloadStatus(Ljava/lang/String;)V
    .locals 3
    .param p0, "downloadstatus"    # Ljava/lang/String;

    .prologue
    .line 111
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "xdbSetDownloadStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput-object p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    .line 113
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 114
    return-void
.end method

.method public static xdbSetLFCurrentStatus(I)V
    .locals 3
    .param p0, "status"    # I

    .prologue
    .line 135
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    .line 137
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 139
    invoke-static {p0}, Lcom/LocalFota/XLFApplication;->xLFSetLFStatus(I)V

    .line 140
    return-void
.end method

.method public static xdbSetLFDeltaCRC(J)V
    .locals 4
    .param p0, "crc"    # J

    .prologue
    .line 161
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput-wide p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    .line 162
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 163
    return-void
.end method

.method public static xdbSetLFSaveDeltaFileIndex(I)V
    .locals 3
    .param p0, "index"    # I

    .prologue
    .line 149
    const-string v0, "DBG_WSS_LF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I

    .line 151
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 152
    return-void
.end method

.method public static xdbSetNewVersionName(Ljava/lang/String;)V
    .locals 3
    .param p0, "newversion"    # Ljava/lang/String;

    .prologue
    .line 172
    sget-object v0, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    iput-object p0, v0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    .line 173
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_LocalFotaInfo:Lcom/LocalFota/db/XDBLocalFotaInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBLFStatusInfoQuery;->xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V

    .line 174
    return-void
.end method

.method public static xdbSetServerAddr(Ljava/lang/String;)V
    .locals 3
    .param p0, "serverAddr"    # Ljava/lang/String;

    .prologue
    .line 99
    sget-object v0, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    iput-object p0, v0, Lcom/LocalFota/db/XDBProfileInfo;->szServerUrl:Ljava/lang/String;

    .line 100
    const-wide/16 v0, 0x1

    sget-object v2, Lcom/LocalFota/db/XDB;->m_ProfileInfo:Lcom/LocalFota/db/XDBProfileInfo;

    invoke-static {v0, v1, v2}, Lcom/LocalFota/db/XDBProfileQuery;->xdbProfileUpdateRow(JLcom/LocalFota/db/XDBProfileInfo;)V

    .line 101
    return-void
.end method
