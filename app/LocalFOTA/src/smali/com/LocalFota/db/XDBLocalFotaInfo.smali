.class public Lcom/LocalFota/db/XDBLocalFotaInfo;
.super Ljava/lang/Object;
.source "XDBLocalFotaInfo.java"


# instance fields
.field public nCurrentStatus:I

.field public nDeltaCRC:J

.field public nDeltaSize:J

.field public nSaveDeltaFileIndex:I

.field public szDownloadStatus:Ljava/lang/String;

.field public szNewVersionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide v2, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    .line 16
    iput-wide v2, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    .line 19
    iput v1, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    .line 20
    iput v1, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I

    .line 21
    return-void
.end method
