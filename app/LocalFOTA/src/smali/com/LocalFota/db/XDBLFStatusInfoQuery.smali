.class public Lcom/LocalFota/db/XDBLFStatusInfoQuery;
.super Ljava/lang/Object;
.source "XDBLFStatusInfoQuery.java"


# direct methods
.method public static xdbLFStatusInfoExistsRow(J)Z
    .locals 14
    .param p0, "rowId"    # J

    .prologue
    const/4 v4, 0x1

    .line 152
    const/4 v10, 0x0

    .line 153
    .local v10, "bExist":Z
    const/4 v1, 0x7

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "deltasize"

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v2, "deltacrc"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "newversionname"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "downloadstatus"

    aput-object v2, v3, v1

    const/4 v1, 0x5

    const-string v2, "status"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "savedeltafileindex"

    aput-object v2, v3, v1

    .line 162
    .local v3, "From":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 163
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 167
    .local v0, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 168
    const/4 v1, 0x1

    const-string v2, "localfotainfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 170
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_0

    .line 171
    const/4 v10, 0x1

    .line 179
    :cond_0
    if-eqz v11, :cond_1

    .line 180
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_1
    if-eqz v0, :cond_2

    .line 183
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 186
    :cond_2
    :goto_0
    return v10

    .line 173
    :catch_0
    move-exception v12

    .line 175
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    if-eqz v11, :cond_3

    .line 180
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_3
    if-eqz v0, :cond_2

    .line 183
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 179
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v11, :cond_4

    .line 180
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_4
    if-eqz v0, :cond_5

    .line 183
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdbLFStatusInfoFetchRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)Ljava/lang/Object;
    .locals 12
    .param p0, "rowId"    # J
    .param p2, "localfotaInfo"    # Lcom/LocalFota/db/XDBLocalFotaInfo;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 104
    const/4 v1, 0x7

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rowid"

    aput-object v2, v3, v1

    const-string v1, "deltasize"

    aput-object v1, v3, v4

    const-string v1, "deltacrc"

    aput-object v1, v3, v5

    const-string v1, "newversionname"

    aput-object v1, v3, v6

    const-string v1, "downloadstatus"

    aput-object v1, v3, v7

    const/4 v1, 0x5

    const-string v2, "status"

    aput-object v2, v3, v1

    const/4 v1, 0x6

    const-string v2, "savedeltafileindex"

    aput-object v2, v3, v1

    .line 113
    .local v3, "From":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 114
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 118
    .local v0, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 119
    const/4 v1, 0x1

    const-string v2, "localfotainfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 121
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 123
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    .line 126
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    .line 127
    const/4 v1, 0x3

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    .line 128
    const/4 v1, 0x4

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    .line 129
    const/4 v1, 0x5

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    .line 130
    const/4 v1, 0x6

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v11

    .line 136
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    if-eqz v10, :cond_0

    .line 141
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 143
    :cond_0
    if-eqz v0, :cond_1

    .line 144
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 147
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object p2

    .line 140
    :cond_2
    if-eqz v10, :cond_3

    .line 141
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 143
    :cond_3
    if-eqz v0, :cond_1

    .line 144
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    .line 140
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 141
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 143
    :cond_4
    if-eqz v0, :cond_5

    .line 144
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_5
    throw v1
.end method

.method public static xdbLFStatusInfoInit(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "Db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 18
    :try_start_0
    const-string v1, "create table if not exists localfotainfo (rowid integer primary key autoincrement, deltasize integer, deltacrc integer, newversionname text, downloadstatus text, status integer, savedeltafileindex integer);"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    :goto_0
    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 22
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static xdbLFStatusInfoInsertRow(Lcom/LocalFota/db/XDBLocalFotaInfo;)V
    .locals 6
    .param p0, "localfotaInfo"    # Lcom/LocalFota/db/XDBLocalFotaInfo;

    .prologue
    .line 28
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 29
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 33
    .local v1, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "deltasize"

    iget-wide v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 34
    const-string v3, "deltacrc"

    iget-wide v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 35
    const-string v3, "newversionname"

    iget-object v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v3, "downloadstatus"

    iget-object v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v3, "status"

    iget v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 38
    const-string v3, "savedeltafileindex"

    iget v4, p0, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 40
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 41
    const-string v3, "localfotainfo"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    if-eqz v1, :cond_0

    .line 50
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v2

    .line 45
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 49
    if-eqz v1, :cond_0

    .line 50
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 49
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 50
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method

.method public static xdbLFStatusInfoUpdateRow(JLcom/LocalFota/db/XDBLocalFotaInfo;)V
    .locals 6
    .param p0, "rowId"    # J
    .param p2, "localfotaInfo"    # Lcom/LocalFota/db/XDBLocalFotaInfo;

    .prologue
    .line 76
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 77
    .local v0, "ContentValues":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 81
    .local v1, "Db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v3, "deltasize"

    iget-wide v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 82
    const-string v3, "deltacrc"

    iget-wide v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nDeltaCRC:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 83
    const-string v3, "newversionname"

    iget-object v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->szNewVersionName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v3, "downloadstatus"

    iget-object v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->szDownloadStatus:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v3, "status"

    iget v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nCurrentStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    const-string v3, "savedeltafileindex"

    iget v4, p2, Lcom/LocalFota/db/XDBLocalFotaInfo;->nSaveDeltaFileIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 89
    const-string v3, "localfotainfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    if-eqz v1, :cond_0

    .line 98
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v2

    .line 93
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    if-eqz v1, :cond_0

    .line 98
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 97
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 98
    invoke-static {v1}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v3
.end method
