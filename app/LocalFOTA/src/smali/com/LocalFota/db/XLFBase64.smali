.class public Lcom/LocalFota/db/XLFBase64;
.super Ljava/lang/Object;
.source "XLFBase64.java"


# static fields
.field private static base64Alphabet:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 25
    new-array v1, v3, [B

    sput-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    .line 30
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 32
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    const/16 v0, 0x5a

    :goto_1
    const/16 v1, 0x41

    if-lt v0, v1, :cond_1

    .line 36
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    add-int/lit8 v2, v0, -0x41

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 38
    :cond_1
    const/16 v0, 0x7a

    :goto_2
    const/16 v1, 0x61

    if-lt v0, v1, :cond_2

    .line 40
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    add-int/lit8 v2, v0, -0x61

    add-int/lit8 v2, v2, 0x1a

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 38
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 42
    :cond_2
    const/16 v0, 0x39

    :goto_3
    const/16 v1, 0x30

    if-lt v0, v1, :cond_3

    .line 44
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    add-int/lit8 v2, v0, -0x30

    add-int/lit8 v2, v2, 0x34

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 42
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 47
    :cond_3
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    const/16 v2, 0x2b

    const/16 v3, 0x3e

    aput-byte v3, v1, v2

    .line 48
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    const/16 v2, 0x2f

    const/16 v3, 0x3f

    aput-byte v3, v1, v2

    .line 49
    return-void
.end method

.method public static decode([B)[B
    .locals 14
    .param p0, "base64Data"    # [B

    .prologue
    const/4 v13, 0x4

    .line 90
    const/4 v2, 0x0

    .line 92
    .local v2, "buf":[B
    const/4 v8, 0x0

    .line 95
    .local v8, "ncnt":I
    :try_start_0
    const-string v11, "org.apache.commons.codec.binary.Base64"

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 96
    .local v1, "Base64":Ljava/lang/Class;
    const/4 v11, 0x1

    new-array v10, v11, [Ljava/lang/Class;

    const/4 v11, 0x0

    const-class v12, [B

    aput-object v12, v10, v11

    .line 98
    .local v10, "parameterTypes":[Ljava/lang/Class;
    array-length v11, p0

    new-array v6, v11, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .local v6, "myroomedData":[B
    const/4 v5, 0x0

    .local v5, "i":I
    move v9, v8

    .end local v8    # "ncnt":I
    .local v9, "ncnt":I
    :goto_0
    :try_start_1
    array-length v11, p0

    if-ge v5, v11, :cond_0

    .line 101
    aget-byte v11, p0, v5

    invoke-static {v11}, Lcom/LocalFota/db/XLFBase64;->xdsIsBase64(B)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v11

    if-eqz v11, :cond_4

    .line 103
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "ncnt":I
    .restart local v8    # "ncnt":I
    :try_start_2
    aget-byte v11, p0, v5

    aput-byte v11, v6, v9
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 99
    :goto_1
    add-int/lit8 v5, v5, 0x1

    move v9, v8

    .end local v8    # "ncnt":I
    .restart local v9    # "ncnt":I
    goto :goto_0

    .line 107
    :cond_0
    const/4 v7, 0x0

    .line 108
    .local v7, "ncheckcnt":I
    :try_start_3
    array-length v11, v6

    rem-int/lit8 v11, v11, 0x4

    if-nez v11, :cond_2

    .line 110
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v13, :cond_2

    .line 112
    add-int/lit8 v11, v5, 0x1

    sub-int v11, v9, v11

    aget-byte v11, v6, v11

    const/16 v12, 0x3d

    if-ne v11, v12, :cond_1

    .line 114
    add-int/lit8 v7, v7, 0x1

    .line 110
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 118
    :cond_2
    if-ne v7, v13, :cond_3

    .line 120
    add-int/lit8 v11, v9, -0x4

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    .line 121
    add-int/lit8 v11, v9, -0x3

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    .line 122
    add-int/lit8 v11, v9, -0x2

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    .line 123
    add-int/lit8 v11, v9, -0x1

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    .line 126
    :cond_3
    const-string v11, "decodeBase64"

    invoke-virtual {v1, v11, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 127
    .local v3, "decodeBase64":Ljava/lang/reflect/Method;
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {v3, v1, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [B

    move-object v0, v11

    check-cast v0, [B

    move-object v2, v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move v8, v9

    .line 134
    .end local v1    # "Base64":Ljava/lang/Class;
    .end local v3    # "decodeBase64":Ljava/lang/reflect/Method;
    .end local v5    # "i":I
    .end local v6    # "myroomedData":[B
    .end local v7    # "ncheckcnt":I
    .end local v9    # "ncnt":I
    .end local v10    # "parameterTypes":[Ljava/lang/Class;
    .restart local v8    # "ncnt":I
    :goto_3
    return-object v2

    .line 129
    :catch_0
    move-exception v4

    .line 131
    .local v4, "e":Ljava/lang/Exception;
    :goto_4
    const-string v11, "DBG_WSS_LF"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 129
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "ncnt":I
    .restart local v1    # "Base64":Ljava/lang/Class;
    .restart local v5    # "i":I
    .restart local v6    # "myroomedData":[B
    .restart local v9    # "ncnt":I
    .restart local v10    # "parameterTypes":[Ljava/lang/Class;
    :catch_1
    move-exception v4

    move v8, v9

    .end local v9    # "ncnt":I
    .restart local v8    # "ncnt":I
    goto :goto_4

    .end local v8    # "ncnt":I
    .restart local v9    # "ncnt":I
    :cond_4
    move v8, v9

    .end local v9    # "ncnt":I
    .restart local v8    # "ncnt":I
    goto :goto_1
.end method

.method public static encode([B)[B
    .locals 8
    .param p0, "binaryData"    # [B

    .prologue
    .line 70
    const/4 v2, 0x0

    .line 74
    .local v2, "buf":[B
    :try_start_0
    const-string v6, "org.apache.commons.codec.binary.Base64"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 75
    .local v1, "Base64":Ljava/lang/Class;
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, [B

    aput-object v7, v5, v6

    .line 76
    .local v5, "parameterTypes":[Ljava/lang/Class;
    const-string v6, "encodeBase64"

    invoke-virtual {v1, v6, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 77
    .local v4, "encodeBase64":Ljava/lang/reflect/Method;
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    invoke-virtual {v4, v1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    move-object v0, v6

    check-cast v0, [B

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v1    # "Base64":Ljava/lang/Class;
    .end local v4    # "encodeBase64":Ljava/lang/reflect/Method;
    .end local v5    # "parameterTypes":[Ljava/lang/Class;
    :goto_0
    return-object v2

    .line 79
    :catch_0
    move-exception v3

    .line 81
    .local v3, "e":Ljava/lang/Exception;
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static xdsIsBase64(B)Z
    .locals 3
    .param p0, "octect"    # B

    .prologue
    const/4 v0, 0x1

    .line 53
    const/16 v1, 0x3d

    if-ne p0, v1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    sget-object v1, Lcom/LocalFota/db/XLFBase64;->base64Alphabet:[B

    aget-byte v1, v1, p0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 59
    const/4 v0, 0x0

    goto :goto_0
.end method
