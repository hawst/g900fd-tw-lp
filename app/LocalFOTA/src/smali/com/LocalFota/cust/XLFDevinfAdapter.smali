.class public Lcom/LocalFota/cust/XLFDevinfAdapter;
.super Ljava/lang/Object;
.source "XLFDevinfAdapter.java"


# direct methods
.method public static xLFBatteryLifeCheck()Z
    .locals 6

    .prologue
    .line 255
    const/4 v2, 0x0

    .line 256
    .local v2, "mMinimumLebel":I
    const/4 v1, 0x0

    .line 257
    .local v1, "battLevel":I
    const/4 v0, 0x0

    .line 259
    .local v0, "bRtnValue":Z
    const-string v3, "/sys/class/power_supply/battery/capacity"

    invoke-static {v3}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetIntFromFile(Ljava/lang/String;)I

    move-result v1

    .line 261
    const/16 v2, 0xf

    .line 263
    const-string v3, "DBG_WSS_LF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "battery level ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], Minimum Level ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    if-lt v1, v2, :cond_0

    .line 266
    const/4 v0, 0x1

    .line 268
    :cond_0
    return v0
.end method

.method public static xLFDevAdpGetDeviceID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    const-string v0, ""

    .line 138
    .local v0, "szDeviceId":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetDevID()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "000000000000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "B0000000"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    :cond_0
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpRetryGetDeviceID()Ljava/lang/String;

    move-result-object v0

    .line 143
    :cond_1
    return-object v0
.end method

.method public static xLFDevAdpGetFullDeviceID()Ljava/lang/String;
    .locals 6

    .prologue
    .line 102
    const-string v1, ""

    .line 103
    .local v1, "szDeviceID":Ljava/lang/String;
    const-string v0, ""

    .line 104
    .local v0, "preId":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetPhoneType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 111
    const-string v0, "IMEI"

    .line 114
    :goto_0
    const-string v2, "%s:%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFDevAdpGetDeviceID()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 116
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szDeviceID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    return-object v1

    .line 107
    :pswitch_0
    const-string v0, "MEID"

    .line 108
    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static xLFDevAdpRetryGetDeviceID()Ljava/lang/String;
    .locals 6

    .prologue
    .line 164
    const-string v2, ""

    .line 166
    .local v2, "szDeviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_1

    .line 170
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_1
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetTargetDevID()Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Default"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "000000000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "B0000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 180
    :cond_0
    const-string v2, "000000000000000"

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 185
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-object v2
.end method

.method private static xLFGetIntFromFile(Ljava/lang/String;)I
    .locals 8
    .param p0, "szPath"    # Ljava/lang/String;

    .prologue
    .line 273
    const/4 v1, 0x0

    .line 274
    .local v1, "in":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 278
    .local v3, "ret":I
    :try_start_0
    const-string v4, ""

    .line 279
    .local v4, "szFileData":Ljava/lang/String;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x1000

    invoke-direct {v2, v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    .end local v1    # "in":Ljava/io/BufferedReader;
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 282
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 298
    if-eqz v2, :cond_0

    .line 300
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 309
    .end local v2    # "in":Ljava/io/BufferedReader;
    .end local v4    # "szFileData":Ljava/lang/String;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return v3

    .line 303
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "szFileData":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 307
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_0

    .line 284
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "szFileData":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_1
    :try_start_3
    const-string v5, "DBG_WSS_LF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t open "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 287
    const/4 v3, 0x0

    .line 298
    if-eqz v1, :cond_1

    .line 300
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 303
    :catch_2
    move-exception v0

    .line 305
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 289
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v5, "DBG_WSS_LF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t open "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 292
    const/4 v3, 0x0

    .line 298
    if-eqz v1, :cond_1

    .line 300
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 303
    :catch_4
    move-exception v0

    .line 305
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 298
    :goto_3
    if-eqz v1, :cond_2

    .line 300
    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 306
    :cond_2
    :goto_4
    throw v5

    .line 303
    :catch_5
    move-exception v0

    .line 305
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 296
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "szFileData":Ljava/lang/String;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_3

    .line 289
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_6
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_2

    .line 284
    .end local v1    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v1    # "in":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public static xLFGetPhoneType()I
    .locals 4

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "nRet":I
    const-string v2, "phone"

    invoke-static {v2}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 124
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 126
    const-string v2, "DBG_WSS_LF"

    const-string v3, "tm == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/4 v0, 0x0

    .line 131
    .end local v0    # "nRet":I
    :goto_0
    return v0

    .line 130
    .restart local v0    # "nRet":I
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 131
    goto :goto_0
.end method

.method public static xLFGetSystemCscFile()Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 208
    const/4 v2, 0x0

    .line 209
    .local v2, "f":Ljava/io/RandomAccessFile;
    new-instance v4, Ljava/io/File;

    const-string v8, "system/CSCVersion.txt"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    .local v4, "file":Ljava/io/File;
    const/4 v0, 0x0

    .line 211
    .local v0, "buffer":[B
    const-string v6, ""

    .line 215
    .local v6, "szRet":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v8, "r"

    invoke-direct {v3, v4, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .local v3, "f":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    cmp-long v8, v8, v10

    if-lez v8, :cond_2

    .line 219
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v8

    long-to-int v8, v8

    add-int/lit8 v8, v8, -0x1

    new-array v0, v8, [B

    .line 220
    const-wide/16 v8, 0x0

    invoke-virtual {v3, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 222
    invoke-virtual {v3, v0}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    .line 223
    .local v5, "len":I
    const/4 v8, -0x1

    if-ne v5, v8, :cond_1

    .line 236
    if-eqz v3, :cond_0

    .line 237
    :try_start_2
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v7, v6

    .line 245
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .end local v5    # "len":I
    .end local v6    # "szRet":Ljava/lang/String;
    .local v7, "szRet":Ljava/lang/String;
    :goto_1
    return-object v7

    .line 239
    .end local v7    # "szRet":Ljava/lang/String;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v5    # "len":I
    .restart local v6    # "szRet":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 225
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_3
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v6    # "szRet":Ljava/lang/String;
    .restart local v7    # "szRet":Ljava/lang/String;
    move-object v6, v7

    .line 236
    .end local v5    # "len":I
    .end local v7    # "szRet":Ljava/lang/String;
    .restart local v6    # "szRet":Ljava/lang/String;
    :cond_2
    if-eqz v3, :cond_3

    .line 237
    :try_start_4
    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    :cond_4
    :goto_2
    move-object v7, v6

    .line 245
    .end local v6    # "szRet":Ljava/lang/String;
    .restart local v7    # "szRet":Ljava/lang/String;
    goto :goto_1

    .line 239
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .end local v7    # "szRet":Ljava/lang/String;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v6    # "szRet":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 241
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 243
    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 228
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 230
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 236
    if-eqz v2, :cond_4

    .line 237
    :try_start_6
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 239
    :catch_3
    move-exception v1

    .line 241
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 234
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 236
    :goto_4
    if-eqz v2, :cond_5

    .line 237
    :try_start_7
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 242
    :cond_5
    :goto_5
    throw v8

    .line 239
    :catch_4
    move-exception v1

    .line 241
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 234
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_4

    .line 228
    .end local v2    # "f":Ljava/io/RandomAccessFile;
    .restart local v3    # "f":Ljava/io/RandomAccessFile;
    :catch_5
    move-exception v1

    move-object v2, v3

    .end local v3    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "f":Ljava/io/RandomAccessFile;
    goto :goto_3
.end method

.method public static xLFGetTargetCscV()Ljava/lang/String;
    .locals 4

    .prologue
    .line 190
    const-string v1, "ril.official_cscver"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "szCsc":Ljava/lang/String;
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read csc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFGetSystemCscFile()Ljava/lang/String;

    move-result-object v0

    .line 195
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read file csc: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    const-string v0, ""

    .line 200
    const-string v1, "DBG_WSS_LF"

    const-string v2, "csc file is Unknown"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_0
    return-object v0
.end method

.method public static xLFGetTargetDevID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 148
    const-string v0, ""

    .line 149
    .local v0, "szDeviceId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 151
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    const-string v2, "phone"

    invoke-static {v2}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "tm":Landroid/telephony/TelephonyManager;
    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 153
    .restart local v1    # "tm":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_1

    .line 155
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 157
    :cond_0
    const-string v0, ""

    .line 159
    :cond_1
    return-object v0
.end method

.method public static xLFGetTargetFwV()Ljava/lang/String;
    .locals 4

    .prologue
    .line 90
    const-string v1, "ro.build.PDA"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "szFwV":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string v0, ""

    .line 96
    :cond_0
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szFwV : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-object v0
.end method

.method public static xLFGetTargetModel()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    const-string v1, "ro.product.model"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "szModel":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const/4 v1, 0x0

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 52
    :cond_0
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "szModel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/LocalFota/core/XLFLib;->LFLibPrivateLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-object v0
.end method

.method public static xLFGetTargetPhoneV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const-string v0, "ril.sw_ver"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
