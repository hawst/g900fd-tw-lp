.class public Lcom/LocalFota/cust/XLFTask;
.super Ljava/lang/Object;
.source "XLFTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static LF_TaskHandler:Landroid/os/Handler;

.field public static XLF_InSync:Z

.field public static XLF_PRIVATE_LOG_ON:Z

.field public static g_IsLFInitialized:Z

.field public static g_TaskInit:Z

.field public static isInitializing:Z

.field private static m_bDrawingPercentageStates:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    sput-boolean v1, Lcom/LocalFota/cust/XLFTask;->g_TaskInit:Z

    .line 29
    new-instance v0, Lcom/LocalFota/cust/XLFTask$1;

    invoke-direct {v0}, Lcom/LocalFota/cust/XLFTask$1;-><init>()V

    sput-object v0, Lcom/LocalFota/cust/XLFTask;->LF_TaskHandler:Landroid/os/Handler;

    .line 46
    sput-boolean v1, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    .line 47
    sput-boolean v1, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    .line 49
    sput-boolean v1, Lcom/LocalFota/cust/XLFTask;->m_bDrawingPercentageStates:Z

    .line 51
    const/4 v0, 0x1

    sput-boolean v0, Lcom/LocalFota/cust/XLFTask;->XLF_PRIVATE_LOG_ON:Z

    .line 53
    sput-boolean v1, Lcom/LocalFota/cust/XLFTask;->XLF_InSync:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    sget-boolean v0, Lcom/LocalFota/cust/XLFTask;->g_TaskInit:Z

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 61
    :cond_0
    return-void
.end method

.method public static DownloadGetDrawingPercentage()Z
    .locals 1

    .prologue
    .line 373
    sget-boolean v0, Lcom/LocalFota/cust/XLFTask;->m_bDrawingPercentageStates:Z

    return v0
.end method

.method public static DownloadSetDrawingPercentage(Z)V
    .locals 0
    .param p0, "bState"    # Z

    .prologue
    .line 368
    sput-boolean p0, Lcom/LocalFota/cust/XLFTask;->m_bDrawingPercentageStates:Z

    .line 369
    return-void
.end method

.method public static GetPrivateLogState()Z
    .locals 1

    .prologue
    .line 393
    sget-boolean v0, Lcom/LocalFota/cust/XLFTask;->XLF_PRIVATE_LOG_ON:Z

    return v0
.end method

.method public static GetSyncMode()Z
    .locals 1

    .prologue
    .line 388
    sget-boolean v0, Lcom/LocalFota/cust/XLFTask;->XLF_InSync:Z

    return v0
.end method

.method public static LFInitAdpEXTInit()Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 337
    const/4 v0, 0x0

    .line 339
    .local v0, "nStatus":I
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFCurrentStatus()I

    move-result v0

    .line 340
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nStatus ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    sparse-switch v0, :sswitch_data_0

    .line 363
    :goto_0
    :sswitch_0
    const/4 v1, 0x1

    return v1

    .line 345
    :sswitch_1
    invoke-static {v5, v4, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 350
    :sswitch_2
    const/4 v1, 0x6

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 354
    :sswitch_3
    invoke-static {v5, v4, v4}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 357
    :sswitch_4
    const/4 v1, 0x7

    invoke-static {v4, v1}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto :goto_0

    .line 342
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xa -> :sswitch_0
        0x14 -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_3
        0x3c -> :sswitch_4
    .end sparse-switch
.end method

.method public static LFTaskHandler(Landroid/os/Message;)Z
    .locals 13
    .param p0, "msg"    # Landroid/os/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const v11, 0x7f04001a

    const/4 v10, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 89
    const/4 v2, 0x0

    .line 91
    .local v2, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    iget-object v7, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v7, :cond_0

    .line 332
    :goto_0
    return v5

    .line 94
    :cond_0
    iget-object v2, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v2    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    check-cast v2, Lcom/LocalFota/core/XLFmsg$LFMsgItem;

    .line 96
    .restart local v2    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    const-string v7, "DBG_WSS_LF"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Task : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v2, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->type:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget v7, v2, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->type:I

    packed-switch v7, :pswitch_data_0

    .line 331
    :cond_1
    :goto_1
    :pswitch_0
    const/4 p0, 0x0

    move v5, v6

    .line 332
    goto :goto_0

    .line 101
    :pswitch_1
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_OS_INITIALIZED"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    sput-boolean v5, Lcom/LocalFota/cust/XLFTask;->g_TaskInit:Z

    .line 103
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 105
    invoke-static {v12, v10, v10}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 110
    :pswitch_2
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_INIT"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v7

    if-nez v7, :cond_2

    .line 114
    sput-boolean v6, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    .line 115
    const-string v5, "DBG_WSS_LF"

    const-string v7, "LocalFota in Not Init!!"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 119
    :cond_2
    invoke-static {}, Lcom/LocalFota/cust/XLFTask;->LFInitAdpEXTInit()Z

    move-result v7

    if-nez v7, :cond_3

    .line 121
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_INIT : Not Initialized"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 125
    :cond_3
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_INIT : Initialized"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sput-boolean v5, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    .line 127
    sput-boolean v6, Lcom/LocalFota/cust/XLFTask;->isInitializing:Z

    goto :goto_1

    .line 134
    :pswitch_3
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_TP_OPEN"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 138
    :pswitch_4
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_SERVER_CONNECT"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-boolean v7, Lcom/LocalFota/cust/XLFTask;->g_IsLFInitialized:Z

    if-eqz v7, :cond_5

    .line 142
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbGetLFCurrentStatus()I

    move-result v3

    .line 143
    .local v3, "nStatus":I
    const-string v7, "DBG_WSS_LF"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "nStatus : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-static {v5}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 147
    sparse-switch v3, :sswitch_data_0

    goto :goto_1

    .line 151
    :sswitch_0
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFDeltaAllClear()V

    .line 152
    const-string v7, "DBG_WSS_LF"

    const-string v8, "Delete Delta File"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-static {v10, v5}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 155
    const/16 v5, 0xf

    invoke-static {v5, v10, v10}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 159
    :sswitch_1
    invoke-static {}, Lcom/LocalFota/cust/XLFDevinfAdapter;->xLFBatteryLifeCheck()Z

    move-result v5

    if-nez v5, :cond_4

    .line 161
    const-string v5, "DBG_WSS_LF"

    const-string v7, "Low Battery"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const-string v5, "Low Battery"

    invoke-static {v5}, Lcom/LocalFota/db/XDB;->xdbSetDownloadStatus(Ljava/lang/String;)V

    .line 164
    const/16 v5, 0x18

    invoke-static {v10, v5}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 168
    :cond_4
    const/4 v5, 0x6

    invoke-static {v5, v10, v10}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 174
    :sswitch_2
    const/16 v5, 0xb

    invoke-static {v10, v5}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    .line 175
    const/16 v5, 0x8

    invoke-static {v5, v10, v10}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 184
    .end local v3    # "nStatus":I
    :cond_5
    const-string v7, "DBG_WSS_LF"

    const-string v8, "g_IsLFInitialized is false."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 190
    :pswitch_5
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_SERVER_CONNECT_FAIL"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 193
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    .line 194
    invoke-static {v10, v12}, Lcom/LocalFota/core/XLFEvent;->LFSetEvent(Ljava/lang/Object;I)V

    goto/16 :goto_1

    .line 198
    :pswitch_6
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_DOWNLOAD_START"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiDownloadProgressInit()V

    .line 201
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 205
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xGetDeltafileFromServer()V

    goto/16 :goto_1

    .line 209
    :cond_6
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 210
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto/16 :goto_1

    .line 216
    :pswitch_7
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_RESUME_DOWNLOAD_START"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 220
    :pswitch_8
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_REPORT_DOWNLOAD_STATE"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 224
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xUpdateResultReport()V

    goto/16 :goto_1

    .line 228
    :cond_7
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 229
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto/16 :goto_1

    .line 235
    :pswitch_9
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_CONNECT_ABORT"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    .line 237
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    goto/16 :goto_1

    .line 241
    :pswitch_a
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_DOWNLOAD_ABORT"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 243
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    .line 245
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFGetUsercancelFlag()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 247
    invoke-static {v6}, Lcom/LocalFota/db/XDB;->xdbSetLFCurrentStatus(I)V

    .line 248
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFDeleteDeltaPkgFile()Z

    move-result v0

    .line 249
    .local v0, "bChcek":Z
    if-eqz v0, :cond_8

    .line 250
    const-string v5, "DBG_WSS_LF"

    const-string v7, "Delete Success.."

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_8
    invoke-static {v6}, Lcom/LocalFota/tp/XLFTpAdapter;->xLFSetUsercancelFlag(Z)V

    goto/16 :goto_1

    .line 258
    .end local v0    # "bChcek":Z
    :pswitch_b
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_FINISH"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 260
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->LFTPClose()V

    goto/16 :goto_1

    .line 264
    :pswitch_c
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_FOTA_REBOOT_COMMAND"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbFileGetDeltaPathFromIndex()Ljava/lang/String;

    move-result-object v4

    .line 266
    .local v4, "szPath":Ljava/lang/String;
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFGetDeltaFileID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 269
    :try_start_0
    invoke-static {v4}, Lcom/LocalFota/XLFApplication;->LFAppReBoot(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 271
    :catch_0
    move-exception v1

    .line 273
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 278
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "szPath":Ljava/lang/String;
    :pswitch_d
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_REBOOT_COMMAND_WIPE_DATA"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :try_start_1
    invoke-static {}, Lcom/LocalFota/XLFApplication;->LFAppReBootWithoutDelta()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 283
    :catch_1
    move-exception v1

    .line 285
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v5, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 290
    .end local v1    # "e":Ljava/lang/Exception;
    :pswitch_e
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_SERVER_RETRY_CONNECT"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-static {v5}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    .line 294
    new-instance v7, Lcom/LocalFota/tp/XNetRetryConnectTimer;

    invoke-direct {v7, v5}, Lcom/LocalFota/tp/XNetRetryConnectTimer;-><init>(Z)V

    goto/16 :goto_1

    .line 298
    :pswitch_f
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_USER_CERTIFICATION_CHECK"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 301
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xCheckUserCertification()V

    goto/16 :goto_1

    .line 305
    :cond_9
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 306
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto/16 :goto_1

    .line 311
    :pswitch_10
    const-string v7, "DBG_WSS_LF"

    const-string v8, "XMSG_LF_DEVICE_VERSION_CHECK"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-static {}, Lcom/LocalFota/XLFApplication;->IsWiFiConnect()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 314
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xCheckDeviceVersion()V

    goto/16 :goto_1

    .line 318
    :cond_a
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    .line 319
    invoke-static {v6}, Lcom/LocalFota/cust/XLFTask;->SetSyncMode(Z)V

    goto/16 :goto_1

    .line 323
    :pswitch_11
    const-string v5, "DBG_WSS_LF"

    const-string v7, "XMSG_LF_END"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-static {}, Lcom/LocalFota/ui/XUiAdminActivity;->xLFAdminActivityFinish()V

    goto/16 :goto_1

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_e
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x14 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public static SetSyncMode(Z)V
    .locals 1
    .param p0, "bState"    # Z

    .prologue
    .line 378
    sput-boolean p0, Lcom/LocalFota/cust/XLFTask;->XLF_InSync:Z

    .line 380
    sget-boolean v0, Lcom/LocalFota/cust/XLFTask;->XLF_InSync:Z

    if-eqz v0, :cond_0

    .line 381
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xWakeLockStart()V

    .line 384
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-static {}, Lcom/LocalFota/tp/XLFTpAdapter;->xWakeLockRelease()V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 65
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 84
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 85
    return-void
.end method
