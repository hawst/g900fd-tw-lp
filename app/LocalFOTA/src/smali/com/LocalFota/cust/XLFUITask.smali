.class public Lcom/LocalFota/cust/XLFUITask;
.super Ljava/lang/Object;
.source "XLFUITask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static UI_TaskHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/LocalFota/cust/XLFUITask$1;

    invoke-direct {v0}, Lcom/LocalFota/cust/XLFUITask$1;-><init>()V

    sput-object v0, Lcom/LocalFota/cust/XLFUITask;->UI_TaskHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 38
    return-void
.end method

.method public static LFUIEvent(Landroid/os/Message;)Z
    .locals 8
    .param p0, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x12

    const/16 v5, 0x11

    const/16 v4, 0xf

    const/4 v2, 0x1

    .line 57
    const/4 v0, 0x0

    .line 59
    .local v0, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    iget-object v3, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v3, :cond_0

    .line 212
    :goto_0
    return v2

    .line 62
    :cond_0
    iget-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    check-cast v0, Lcom/LocalFota/core/XLFmsg$LFMsgItem;

    .line 64
    .restart local v0    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    iget v3, v0, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->type:I

    packed-switch v3, :pswitch_data_0

    .line 211
    :cond_1
    :goto_1
    const/4 p0, 0x0

    .line 212
    const/4 v2, 0x0

    goto :goto_0

    .line 67
    :pswitch_0
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_NONE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 71
    :pswitch_1
    const-string v3, "DBG_WSS_LF"

    const-string v4, "LF_EVENT_UI_SERVER_CONNECT"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 76
    :pswitch_2
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_SERVER_CONNECT_FAIL"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 81
    :pswitch_3
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_DOWNLOAD_PROGRESS"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 86
    :pswitch_4
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_RESUME_DOWNLOAD"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v2, 0x4

    invoke-static {v2, v7, v7}, Lcom/LocalFota/core/XLFmsg;->LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 93
    :pswitch_5
    invoke-static {v2}, Lcom/LocalFota/cust/XLFTask;->DownloadSetDrawingPercentage(Z)V

    .line 95
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiUpdateDownloadInfo()V

    .line 96
    invoke-static {}, Lcom/LocalFota/ui/XUiDownloadProgress;->wssLFUiGetDownloadPercent()I

    move-result v1

    .line 98
    .local v1, "nDownloadPercent":I
    const/16 v2, 0x64

    if-gt v1, v2, :cond_1

    .line 99
    invoke-static {}, Lcom/LocalFota/ui/XUiLFDialogActivity;->wssDmUiUpdateDownloadProgress()V

    goto :goto_1

    .line 104
    .end local v1    # "nDownloadPercent":I
    :pswitch_6
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_DOWNLOAD_COMPLETE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v2, 0x5

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 109
    :pswitch_7
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_INSTALL_CONFRIM"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {v6}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    .line 116
    invoke-static {}, Lcom/LocalFota/db/XDBAdapter;->xLFCheckDeltaCRC()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 117
    invoke-static {v5}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 119
    :cond_2
    invoke-static {v4}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 124
    :pswitch_8
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_MEMORY_FULL"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const/16 v2, 0x8

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto :goto_1

    .line 129
    :pswitch_9
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_NOUPDATE_AVAILABLE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/16 v2, 0x9

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 134
    :pswitch_a
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_MODEL_NOT_REGISTERED"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/16 v2, 0xa

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 139
    :pswitch_b
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_SERVER_CONNECT_FOR_REPORT_DOWNLOAD_STATE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const/16 v2, 0xb

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 144
    :pswitch_c
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_REMOVE_DIALOG_SERVER_CONNECT_FOR_REPORT_DOWNLOAD_STATE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const/16 v2, 0xc

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 149
    :pswitch_d
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_WIFI_CONNECTING"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/16 v2, 0x10

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 153
    :pswitch_e
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_REMOVE_WIFI_CONNECTING"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const/16 v2, 0xd

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 158
    :pswitch_f
    const-string v3, "DBG_WSS_LF"

    const-string v4, "LF_EVENT_UI_WIFI_CONNECTING_FAIL_TOAST"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f04001b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/LocalFota/XLFApplication;->xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 163
    :pswitch_10
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_CRC_FAIL"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-static {v4}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 168
    :pswitch_11
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_PHONE_REBOOT"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {v5}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 173
    :pswitch_12
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_CRC_CHECK_PLEASE_WAIT"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {v6}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 178
    :pswitch_13
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_INVALID_TAG_DATA"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/16 v2, 0x13

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 183
    :pswitch_14
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_LATEST_VERSION"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const/16 v2, 0x14

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 188
    :pswitch_15
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_UPDATE_SUCCESS"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/16 v2, 0x15

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 193
    :pswitch_16
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LF_EVENT_UI_AUTH_ERROR"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/16 v2, 0x16

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 198
    :pswitch_17
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LT_EVENT_UI_DELTA_DOWNLOADING"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const/16 v2, 0x17

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 203
    :pswitch_18
    const-string v2, "DBG_WSS_LF"

    const-string v3, "LT_EVENT_UI_DELTA_DOWNLOADING"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const/16 v2, 0x18

    invoke-static {v2}, Lcom/LocalFota/cust/XLFUITask;->xLFServicesend(I)V

    goto/16 :goto_1

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_d
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method private static xLFServicesend(I)V
    .locals 5
    .param p0, "id"    # I

    .prologue
    .line 217
    const-string v1, "DBG_WSS_LF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    new-instance v0, Landroid/content/Intent;

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/LocalFota/ui/XUiLFDialogActivity;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 219
    .local v0, "DialIntent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 220
    invoke-static {}, Lcom/LocalFota/XLFApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 221
    return-void
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 42
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 52
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 53
    return-void
.end method
