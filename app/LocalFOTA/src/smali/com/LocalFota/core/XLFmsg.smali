.class public Lcom/LocalFota/core/XLFmsg;
.super Ljava/lang/Object;
.source "XLFmsg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/LocalFota/core/XLFmsg$LFMsgItem;,
        Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    }
.end annotation


# static fields
.field private static LFMsgQueue:Lcom/LocalFota/core/XLFLinkedList;

.field private static LFUIMsgQueue:Lcom/LocalFota/core/XLFLinkedList;

.field private static final syncMsgQueueObj:Ljava/lang/Object;

.field private static final syncUIMsgQueueObj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-object v0, Lcom/LocalFota/core/XLFmsg;->LFMsgQueue:Lcom/LocalFota/core/XLFLinkedList;

    .line 14
    sput-object v0, Lcom/LocalFota/core/XLFmsg;->LFUIMsgQueue:Lcom/LocalFota/core/XLFLinkedList;

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/LocalFota/core/XLFmsg;->syncMsgQueueObj:Ljava/lang/Object;

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/LocalFota/core/XLFmsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    .line 9
    return-void
.end method

.method public static LFSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 159
    const/4 v5, 0x0

    .line 160
    .local v5, "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    const/4 v3, 0x0

    .line 162
    .local v3, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    if-eqz p1, :cond_0

    .line 164
    new-instance v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;

    .end local v5    # "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    invoke-direct {v5}, Lcom/LocalFota/core/XLFmsg$LFMsgParam;-><init>()V

    .line 165
    .restart local v5    # "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    iput-object p1, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->param:Ljava/lang/Object;

    .line 167
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->paramFree:Ljava/lang/Object;

    .line 168
    if-eqz p2, :cond_0

    .line 170
    iput-object p2, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->paramFree:Ljava/lang/Object;

    .line 174
    :cond_0
    sget-object v7, Lcom/LocalFota/core/XLFmsg;->syncMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 176
    :try_start_0
    new-instance v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;

    invoke-direct {v4}, Lcom/LocalFota/core/XLFmsg$LFMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 178
    .end local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .local v4, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :try_start_1
    sget-object v6, Lcom/LocalFota/cust/XLFTask;->LF_TaskHandler:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 181
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 202
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 204
    :try_start_2
    iput p0, v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->type:I

    .line 205
    iput-object v5, v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->param:Lcom/LocalFota/core/XLFmsg$LFMsgParam;

    .line 207
    sget-object v6, Lcom/LocalFota/cust/XLFTask;->LF_TaskHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 208
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 209
    sget-object v6, Lcom/LocalFota/cust/XLFTask;->LF_TaskHandler:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 174
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 221
    return-void

    .line 185
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "DBG_WSS_LF"

    const-string v8, "waiting for DM_TaskHandler create"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibDebug(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 193
    :goto_2
    :try_start_5
    sget-object v6, Lcom/LocalFota/cust/XLFTask;->LF_TaskHandler:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 174
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 213
    .end local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "DBG_WSS_LF"

    const-string v8, "Can\'t send message"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibPrintException(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 216
    :catch_1
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "DBG_WSS_LF"

    const-string v8, "Can\'t send message"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibPrintException(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 174
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method

.method public static LFSendMessageUI(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 225
    const/4 v5, 0x0

    .line 226
    .local v5, "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    const/4 v3, 0x0

    .line 228
    .local v3, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    if-eqz p1, :cond_0

    .line 230
    new-instance v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;

    .end local v5    # "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    invoke-direct {v5}, Lcom/LocalFota/core/XLFmsg$LFMsgParam;-><init>()V

    .line 231
    .restart local v5    # "msgParam":Lcom/LocalFota/core/XLFmsg$LFMsgParam;
    iput-object p1, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->param:Ljava/lang/Object;

    .line 233
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->paramFree:Ljava/lang/Object;

    .line 234
    if-eqz p2, :cond_0

    .line 236
    iput-object p2, v5, Lcom/LocalFota/core/XLFmsg$LFMsgParam;->paramFree:Ljava/lang/Object;

    .line 240
    :cond_0
    sget-object v7, Lcom/LocalFota/core/XLFmsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 242
    :try_start_0
    new-instance v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;

    invoke-direct {v4}, Lcom/LocalFota/core/XLFmsg$LFMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244
    .end local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .local v4, "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :try_start_1
    sget-object v6, Lcom/LocalFota/cust/XLFUITask;->UI_TaskHandler:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 247
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 268
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 270
    :try_start_2
    iput p0, v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->type:I

    .line 271
    iput-object v5, v4, Lcom/LocalFota/core/XLFmsg$LFMsgItem;->param:Lcom/LocalFota/core/XLFmsg$LFMsgParam;

    .line 273
    sget-object v6, Lcom/LocalFota/cust/XLFUITask;->UI_TaskHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 274
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 275
    sget-object v6, Lcom/LocalFota/cust/XLFUITask;->UI_TaskHandler:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 240
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 287
    return-void

    .line 251
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "DBG_WSS_LF"

    const-string v8, "waiting for UI_TaskHandler create"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibDebug(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 259
    :goto_2
    :try_start_5
    sget-object v6, Lcom/LocalFota/cust/XLFUITask;->UI_TaskHandler:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 247
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 254
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 240
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 279
    .end local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "DBG_WSS_LF"

    const-string v8, "Can\'t send UI message"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibPrintException(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 282
    :catch_1
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "DBG_WSS_LF"

    const-string v8, "Can\'t send UI message"

    invoke-static {v6, v8}, Lcom/LocalFota/core/XLFLib;->LFLibPrintException(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 240
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    .restart local v3    # "msgItem":Lcom/LocalFota/core/XLFmsg$LFMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method
