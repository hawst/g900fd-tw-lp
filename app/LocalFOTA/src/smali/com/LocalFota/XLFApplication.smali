.class public Lcom/LocalFota/XLFApplication;
.super Landroid/app/Application;
.source "XLFApplication.java"


# static fields
.field private static COMMAND_FILE:Ljava/io/File;

.field private static RECOVERY_DIR:Ljava/io/File;

.field public static Task:Lcom/LocalFota/cust/XLFTask;

.field public static UITask:Lcom/LocalFota/cust/XLFUITask;

.field public static bApplicationStarted:Z

.field private static mContext:Landroid/content/Context;

.field private static m_ToastAlreadyAdded:Landroid/widget/Toast;

.field private static m_szReleaseVer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    sput-object v1, Lcom/LocalFota/XLFApplication;->Task:Lcom/LocalFota/cust/XLFTask;

    .line 56
    sput-object v1, Lcom/LocalFota/XLFApplication;->UITask:Lcom/LocalFota/cust/XLFUITask;

    .line 58
    const/4 v0, 0x0

    sput-boolean v0, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z

    .line 60
    sput-object v1, Lcom/LocalFota/XLFApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    .line 63
    sput-object v1, Lcom/LocalFota/XLFApplication;->RECOVERY_DIR:Ljava/io/File;

    .line 64
    sput-object v1, Lcom/LocalFota/XLFApplication;->COMMAND_FILE:Ljava/io/File;

    .line 66
    const-string v0, ""

    sput-object v0, Lcom/LocalFota/XLFApplication;->m_szReleaseVer:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static IsWiFiConnect()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 200
    const-string v4, "connectivity"

    invoke-static {v4}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 201
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_0

    .line 203
    const-string v3, "DBG_WSS_LF"

    const-string v4, "connectivityManager is null!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :goto_0
    return v2

    .line 207
    :cond_0
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 209
    .local v1, "wifiInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_1

    .line 211
    const-string v3, "DBG_WSS_LF"

    const-string v4, "wifiInfo is null!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 217
    const-string v2, "DBG_WSS_LF"

    const-string v4, "WiFi Connected"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 218
    goto :goto_0

    .line 222
    :cond_2
    const-string v3, "DBG_WSS_LF"

    const-string v4, "WiFi DisConnected"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static LFAppReBoot(Ljava/lang/String;)V
    .locals 5
    .param p0, "szfilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 336
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/lfota/update.zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "dirPath":Ljava/lang/String;
    new-instance v2, Lcom/fota/JNIFOTA;

    invoke-direct {v2}, Lcom/fota/JNIFOTA;-><init>()V

    .line 342
    .local v2, "jnifota":Lcom/fota/JNIFOTA;
    :try_start_0
    const-string v3, "/data/data/com.LocalFota/dprw.tmp"

    invoke-virtual {v2, v0, v3}, Lcom/fota/JNIFOTA;->FOTA_SetPreconfig(Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    :goto_0
    sget-object v3, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/LocalFota/XLFApplication;->LFInstallPackage(Landroid/content/Context;Ljava/io/File;)V

    .line 349
    return-void

    .line 344
    :catch_0
    move-exception v1

    .line 346
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static LFAppReBootWithoutDelta()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 353
    const/4 v0, 0x0

    .line 354
    .local v0, "arg":Ljava/lang/String;
    const-string v1, "DBG_WSS_LF"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const-string v0, "--wipe_data"

    .line 358
    sget-object v1, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/LocalFota/XLFApplication;->LFBootCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method private static LFBootCommand(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "arg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    new-instance v2, Ljava/io/File;

    const-string v3, "/cache/recovery"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/LocalFota/XLFApplication;->RECOVERY_DIR:Ljava/io/File;

    .line 278
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/LocalFota/XLFApplication;->RECOVERY_DIR:Ljava/io/File;

    const-string v4, "command"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v2, Lcom/LocalFota/XLFApplication;->COMMAND_FILE:Ljava/io/File;

    .line 280
    sget-object v2, Lcom/LocalFota/XLFApplication;->RECOVERY_DIR:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 281
    sget-object v2, Lcom/LocalFota/XLFApplication;->COMMAND_FILE:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 283
    new-instance v0, Ljava/io/FileWriter;

    sget-object v2, Lcom/LocalFota/XLFApplication;->COMMAND_FILE:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 285
    .local v0, "command":Ljava/io/FileWriter;
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 286
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    .line 292
    const-string v2, "power"

    invoke-static {v2}, Lcom/LocalFota/XLFApplication;->wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 293
    .local v1, "pm":Landroid/os/PowerManager;
    if-eqz v1, :cond_0

    .line 294
    const-string v2, "recovery"

    invoke-virtual {v1, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 299
    :goto_0
    return-void

    .line 288
    .end local v1    # "pm":Landroid/os/PowerManager;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    throw v2

    .line 296
    .restart local v1    # "pm":Landroid/os/PowerManager;
    :cond_0
    const-string v2, "DBG_WSS_LF"

    const-string v3, "pm is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static LFInstallPackage(Landroid/content/Context;Ljava/io/File;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0xa

    .line 318
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 319
    .local v3, "state_Prefs":Landroid/content/SharedPreferences;
    const-string v4, "WipeDataTag"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 321
    .local v0, "WipeDataTag":Z
    const/4 v1, 0x0

    .line 322
    .local v1, "arg":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    .line 323
    .local v2, "filename":Ljava/lang/String;
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "!!! REBOOTING TO INSTALL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " !!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const-string v4, "DBG_WSS_LF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "!!! WipeDataTag "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " !!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    if-ne v0, v8, :cond_0

    .line 327
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--update_package="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "--carry_out=factory_fota"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "--wipe_data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 331
    :goto_0
    invoke-static {p0, v1}, Lcom/LocalFota/XLFApplication;->LFBootCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 332
    return-void

    .line 329
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--update_package="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "--carry_out=factory_fota"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private createLibrary()V
    .locals 11

    .prologue
    .line 103
    const/4 v3, 0x0

    .line 104
    .local v3, "is":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 106
    .local v5, "os":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v8, "/data/data/com.LocalFota/libdprwl.so"

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 107
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/LocalFota/XLFApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    const-string v9, "libdprwl.so"

    invoke-virtual {v8, v9}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 110
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v7

    .line 111
    .local v7, "size":I
    new-array v0, v7, [B

    .line 112
    .local v0, "buffer":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 113
    .local v4, "len":I
    const/4 v8, -0x1

    if-ne v4, v8, :cond_2

    .line 125
    if-eqz v5, :cond_0

    .line 126
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 127
    :cond_0
    if-eqz v3, :cond_1

    .line 128
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    .end local v0    # "buffer":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "len":I
    .end local v7    # "size":I
    :cond_1
    :goto_0
    return-void

    .line 129
    .restart local v0    # "buffer":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "len":I
    .restart local v7    # "size":I
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 116
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 117
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .local v6, "os":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual {v6, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 118
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->flush()V

    .line 119
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v5, v6

    .line 125
    .end local v0    # "buffer":[B
    .end local v4    # "len":I
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .end local v7    # "size":I
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    :cond_3
    if-eqz v5, :cond_4

    .line 126
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 127
    :cond_4
    if-eqz v3, :cond_1

    .line 128
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 129
    :catch_1
    move-exception v1

    .line 130
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 121
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "file":Ljava/io/File;
    :catch_2
    move-exception v1

    .line 122
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_5
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 125
    if-eqz v5, :cond_5

    .line 126
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 127
    :cond_5
    if-eqz v3, :cond_1

    .line 128
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 129
    :catch_3
    move-exception v1

    .line 130
    const-string v8, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 125
    :goto_2
    if-eqz v5, :cond_6

    .line 126
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 127
    :cond_6
    if-eqz v3, :cond_7

    .line 128
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 131
    :cond_7
    :goto_3
    throw v8

    .line 129
    :catch_4
    move-exception v1

    .line 130
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v9, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 124
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "len":I
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v7    # "size":I
    :catchall_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 121
    .end local v5    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v1

    move-object v5, v6

    .end local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v5    # "os":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static wsGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p0, "serviceName"    # Ljava/lang/String;

    .prologue
    .line 229
    const/4 v2, 0x0

    .line 232
    .local v2, "manager":Ljava/lang/Object;
    :try_start_0
    sget-object v3, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 233
    if-nez v2, :cond_0

    .line 235
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_0

    .line 239
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 245
    :goto_1
    :try_start_2
    const-string v3, "DBG_WSS_LF"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is null, retry..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    sget-object v3, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 247
    if-eqz v2, :cond_1

    .line 256
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :cond_0
    :goto_2
    return-object v2

    .line 241
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 252
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    .end local v2    # "manager":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 235
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v2    # "manager":Ljava/lang/Object;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xLFCallDBHelper()V
    .locals 4

    .prologue
    .line 182
    const/4 v0, 0x0

    .line 185
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "DBG_WSS_LF"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 193
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 194
    invoke-static {v0}, Lcom/LocalFota/db/XDBHelper;->xdbCloseSQLiteDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_1
    throw v2
.end method

.method public static xLFGetLFStatus()I
    .locals 5

    .prologue
    .line 456
    sget-object v2, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 457
    .local v1, "state_Prefs":Landroid/content/SharedPreferences;
    const-string v2, "Status"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 459
    .local v0, "Status":I
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!!! Status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " !!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    return v0
.end method

.method public static xLFGetReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "DBHelper":Lcom/LocalFota/db/XDBHelper;
    const/4 v2, 0x0

    .line 152
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v1, Lcom/LocalFota/db/XDBHelper;

    sget-object v4, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/LocalFota/db/XDBHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    .end local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .local v1, "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :try_start_1
    invoke-virtual {v1}, Lcom/LocalFota/db/XDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    move-object v0, v1

    .line 160
    .end local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .restart local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :goto_0
    return-object v2

    .line 155
    :catch_0
    move-exception v3

    .line 157
    .local v3, "e":Ljava/lang/Exception;
    :goto_1
    const-string v4, "DBG_WSS_LF"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    .end local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :catch_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .restart local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    goto :goto_1
.end method

.method public static xLFGetReleaseVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    sget-object v0, Lcom/LocalFota/XLFApplication;->m_szReleaseVer:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "Not Defined"

    .line 383
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/LocalFota/XLFApplication;->m_szReleaseVer:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xLFGetWipeData()Z
    .locals 5

    .prologue
    .line 446
    sget-object v2, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 447
    .local v1, "state_Prefs":Landroid/content/SharedPreferences;
    const-string v2, "WipeDataTag"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 449
    .local v0, "WipeDataTag":Z
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!!! WipeDataTag "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " !!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    return v0
.end method

.method public static xLFGetWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 6

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "DBHelper":Lcom/LocalFota/db/XDBHelper;
    const/4 v2, 0x0

    .line 169
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v1, Lcom/LocalFota/db/XDBHelper;

    sget-object v4, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/LocalFota/db/XDBHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    .end local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .local v1, "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :try_start_1
    invoke-virtual {v1}, Lcom/LocalFota/db/XDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    move-object v0, v1

    .line 177
    .end local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .restart local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :goto_0
    return-object v2

    .line 172
    :catch_0
    move-exception v3

    .line 174
    .local v3, "e":Ljava/lang/Exception;
    :goto_1
    const-string v4, "DBG_WSS_LF"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    .end local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    :catch_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    .restart local v0    # "DBHelper":Lcom/LocalFota/db/XDBHelper;
    goto :goto_1
.end method

.method public static xLFSetLFStatus(I)V
    .locals 5
    .param p0, "Status"    # I

    .prologue
    .line 466
    sget-object v2, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 467
    .local v1, "state_Prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 468
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "Status"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 469
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 471
    const-string v2, "DBG_WSS_LF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "!!! Status "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " !!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFGetLFStatus()I

    .line 473
    return-void
.end method

.method private static xLFSetReleaseVer(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 365
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 366
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v2, Lcom/LocalFota/XLFApplication;->m_szReleaseVer:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 376
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 372
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static xLFShowToast(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "szText"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 261
    sget-object v0, Lcom/LocalFota/XLFApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 263
    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/LocalFota/XLFApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    .line 265
    :cond_0
    sget-object v0, Lcom/LocalFota/XLFApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 266
    sget-object v0, Lcom/LocalFota/XLFApplication;->m_ToastAlreadyAdded:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 267
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 138
    sput-object p1, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    .line 139
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 75
    :try_start_0
    const-string v1, "DBG_WSS_LF"

    const-string v2, "LocalFOTA Application Start !"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    sput-object p0, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    .line 79
    sget-object v1, Lcom/LocalFota/XLFApplication;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/LocalFota/XLFApplication;->xLFSetReleaseVer(Landroid/content/Context;)V

    .line 81
    invoke-direct {p0}, Lcom/LocalFota/XLFApplication;->createLibrary()V

    .line 83
    invoke-static {}, Lcom/LocalFota/XLFApplication;->xLFCallDBHelper()V

    .line 84
    invoke-static {}, Lcom/LocalFota/db/XDB;->xdbDBInit()V

    .line 86
    sget-object v1, Lcom/LocalFota/XLFApplication;->Task:Lcom/LocalFota/cust/XLFTask;

    if-nez v1, :cond_0

    .line 87
    new-instance v1, Lcom/LocalFota/cust/XLFTask;

    invoke-direct {v1}, Lcom/LocalFota/cust/XLFTask;-><init>()V

    sput-object v1, Lcom/LocalFota/XLFApplication;->Task:Lcom/LocalFota/cust/XLFTask;

    .line 88
    :cond_0
    sget-object v1, Lcom/LocalFota/XLFApplication;->UITask:Lcom/LocalFota/cust/XLFUITask;

    if-nez v1, :cond_1

    .line 89
    new-instance v1, Lcom/LocalFota/cust/XLFUITask;

    invoke-direct {v1}, Lcom/LocalFota/cust/XLFUITask;-><init>()V

    sput-object v1, Lcom/LocalFota/XLFApplication;->UITask:Lcom/LocalFota/cust/XLFUITask;

    .line 94
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/LocalFota/XLFApplication;->bApplicationStarted:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DBG_WSS_LF"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
