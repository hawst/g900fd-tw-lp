.class public Lcom/fota/JNIFOTA;
.super Ljava/lang/Object;
.source "JNIFOTA.java"


# static fields
.field public static final DP_FLAG_DONE:I = 0x3

.field public static final DP_FLAG_ERROR:I = -0x1

.field public static final DP_FLAG_FAIL:I = 0x0

.field public static final DP_FLAG_SKIP:I = 0x1

.field public static final DP_FLAG_TODO:I = 0x2

.field private static loadSuccess:Z


# instance fields
.field public final DP_BLOCK_SIZE:J

.field public final DP_MAX_BLOCK:I

.field public final DP_PAGE_SIZE:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19
    const/4 v2, 0x1

    :try_start_0
    sput-boolean v2, Lcom/fota/JNIFOTA;->loadSuccess:Z

    .line 20
    const-string v2, "/data/data/com.LocalFota/libdprwl.so"

    invoke-static {v2}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 26
    :goto_0
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    .local v0, "e":Ljava/lang/Error;
    sput-boolean v3, Lcom/fota/JNIFOTA;->loadSuccess:Z

    goto :goto_0

    .line 23
    .end local v0    # "e":Ljava/lang/Error;
    :catch_1
    move-exception v1

    .line 24
    .local v1, "e2":Ljava/lang/Exception;
    sput-boolean v3, Lcom/fota/JNIFOTA;->loadSuccess:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const-wide/32 v0, 0x80000

    iput-wide v0, p0, Lcom/fota/JNIFOTA;->DP_BLOCK_SIZE:J

    .line 6
    const-wide/16 v0, 0x1000

    iput-wide v0, p0, Lcom/fota/JNIFOTA;->DP_PAGE_SIZE:J

    .line 7
    const/16 v0, 0x2f

    iput v0, p0, Lcom/fota/JNIFOTA;->DP_MAX_BLOCK:I

    return-void
.end method

.method static native FOTA_EraseDelta()I
.end method

.method static native FOTA_GetAuthName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method static native FOTA_GetAuthSecret(Ljava/lang/String;)Ljava/lang/String;
.end method

.method static native FOTA_Preconfig(Ljava/lang/String;Ljava/lang/String;)J
.end method

.method static native FOTA_ReadFlag()I
.end method

.method static native FOTA_WriteDelta(Ljava/lang/String;)J
.end method

.method static native FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;)J
.end method

.method static native FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method static native FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method static native FOTA_WriteFlag()I
.end method


# virtual methods
.method public FOTA_GetAuthID(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "authKey"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-static {p1}, Lcom/fota/JNIFOTA;->FOTA_GetAuthName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public FOTA_GetAuthPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "authKey"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {p1}, Lcom/fota/JNIFOTA;->FOTA_GetAuthSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public FOTA_GetFlag()I
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/fota/JNIFOTA;->FOTA_ReadFlag()I

    move-result v0

    return v0
.end method

.method public FOTA_SetEraseDelta()I
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/fota/JNIFOTA;->FOTA_EraseDelta()I

    move-result v0

    return v0
.end method

.method public FOTA_SetFlag()I
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/fota/JNIFOTA;->FOTA_WriteFlag()I

    move-result v0

    return v0
.end method

.method public FOTA_SetPreconfig(Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "deltapath"    # Ljava/lang/String;
    .param p2, "splitpath"    # Ljava/lang/String;

    .prologue
    .line 87
    sget-boolean v0, Lcom/fota/JNIFOTA;->loadSuccess:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-static {p1, p2}, Lcom/fota/JNIFOTA;->FOTA_Preconfig(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 90
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0
.end method

.method public FOTA_SetWriteDelta(Ljava/lang/String;)J
    .locals 2
    .param p1, "deltapath"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p1}, Lcom/fota/JNIFOTA;->FOTA_WriteDelta(Ljava/lang/String;)J

    move-result-wide v0

    .line 64
    .local v0, "ret":J
    return-wide v0
.end method

.method public FOTA_SetWriteDelta(Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "deltapath"    # Ljava/lang/String;
    .param p2, "splitpath"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-static {p1, p2}, Lcom/fota/JNIFOTA;->FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 70
    .local v0, "ret":J
    return-wide v0
.end method

.method public FOTA_SetWriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "deltapath"    # Ljava/lang/String;
    .param p2, "splitpath"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p1, p2, p3}, Lcom/fota/JNIFOTA;->FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 76
    .local v0, "ret":J
    return-wide v0
.end method

.method public FOTA_SetWriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p1, "deltapath"    # Ljava/lang/String;
    .param p2, "splitpath"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/lang/String;
    .param p4, "operator"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-static {p1, p2, p3, p4}, Lcom/fota/JNIFOTA;->FOTA_WriteDelta(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 82
    .local v0, "ret":J
    return-wide v0
.end method
