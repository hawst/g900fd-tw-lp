.class public Lcom/android/certinstaller/CertInstaller;
.super Landroid/app/Activity;
.source "CertInstaller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/certinstaller/CertInstaller$InstallOthersAction;,
        Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;,
        Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;,
        Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;,
        Lcom/android/certinstaller/CertInstaller$MyMap;,
        Lcom/android/certinstaller/CertInstaller$InstallCaCertsToKeyChainTask;
    }
.end annotation


# instance fields
.field private mCredentials:Lcom/android/certinstaller/CredentialHelper;

.field private mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

.field private mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

.field private final mKeyStore:Landroid/security/KeyStore;

.field private mState:I

.field private final mView:Lcom/android/certinstaller/ViewHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 89
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mKeyStore:Landroid/security/KeyStore;

    .line 90
    new-instance v0, Lcom/android/certinstaller/ViewHelper;

    invoke-direct {v0}, Lcom/android/certinstaller/ViewHelper;-><init>()V

    iput-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mView:Lcom/android/certinstaller/ViewHelper;

    .line 769
    return-void
.end method

.method static synthetic access$100(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CredentialHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CredentialHelper;)Lcom/android/certinstaller/CredentialHelper;
    .locals 0
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;
    .param p1, "x1"    # Lcom/android/certinstaller/CredentialHelper;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/certinstaller/CertInstaller;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;
    .param p1, "x1"    # Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/certinstaller/CertInstaller;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/certinstaller/CertInstaller;->onValidationDone(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/certinstaller/CertInstaller;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/android/certinstaller/CertInstaller;
    .param p1, "x1"    # [B

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/certinstaller/CertInstaller;->evaluateValidationCertificate([B)V

    return-void
.end method

.method private createCredentialHelper(Landroid/content/Intent;)Lcom/android/certinstaller/CredentialHelper;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 101
    :try_start_0
    new-instance v1, Lcom/android/certinstaller/CredentialHelper;

    invoke-direct {v1, p1}, Lcom/android/certinstaller/CredentialHelper;-><init>(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-object v1

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "CertInstaller"

    const-string v2, "createCredentialHelper"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 104
    const v1, 0x7f060017

    invoke-direct {p0, v1}, Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V

    .line 105
    new-instance v1, Lcom/android/certinstaller/CredentialHelper;

    invoke-direct {v1}, Lcom/android/certinstaller/CredentialHelper;-><init>()V

    goto :goto_0
.end method

.method private createNameCredentialDialog(I)Landroid/app/Dialog;
    .locals 9
    .param p1, "dialogId"    # I

    .prologue
    const/16 v8, 0x8

    .line 504
    const/high16 v6, 0x7f020000

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 506
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f090005

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 507
    .local v1, "credentialInfo":Landroid/widget/TextView;
    const/high16 v6, 0x7f090000

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 508
    .local v0, "credentialError":Landroid/widget/TextView;
    const v6, 0x7f090001

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 509
    .local v3, "nameInput":Landroid/widget/EditText;
    iget-object v6, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v6}, Lcom/android/certinstaller/CredentialHelper;->isInstallAsUidSet()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 510
    const v6, 0x7f090002

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 539
    :goto_0
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->getDefaultName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 540
    invoke-virtual {v3}, Landroid/widget/EditText;->selectAll()V

    .line 541
    iget-object v6, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v6, p0}, Lcom/android/certinstaller/CredentialHelper;->getDescription(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    const/4 v6, 0x2

    if-ne p1, v6, :cond_0

    .line 544
    const v6, 0x7f060011

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 545
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 549
    :cond_0
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f060006

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x104000a

    new-instance v8, Lcom/android/certinstaller/CertInstaller$7;

    invoke-direct {v8, p0, v3, p1}, Lcom/android/certinstaller/CertInstaller$7;-><init>(Lcom/android/certinstaller/CertInstaller;Landroid/widget/EditText;I)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const/high16 v7, 0x1040000

    new-instance v8, Lcom/android/certinstaller/CertInstaller$6;

    invoke-direct {v8, p0}, Lcom/android/certinstaller/CertInstaller$6;-><init>(Lcom/android/certinstaller/CertInstaller;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 581
    .local v2, "d":Landroid/app/Dialog;
    new-instance v6, Lcom/android/certinstaller/CertInstaller$8;

    invoke-direct {v6, p0}, Lcom/android/certinstaller/CertInstaller$8;-><init>(Lcom/android/certinstaller/CertInstaller;)V

    invoke-virtual {v2, v6}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 586
    return-object v2

    .line 512
    .end local v2    # "d":Landroid/app/Dialog;
    :cond_1
    const v6, 0x7f090003

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    .line 517
    .local v4, "usageSpinner":Landroid/widget/Spinner;
    const v6, 0x7f090004

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 519
    new-instance v6, Lcom/android/certinstaller/CertInstaller$5;

    invoke-direct {v6, p0}, Lcom/android/certinstaller/CertInstaller$5;-><init>(Lcom/android/certinstaller/CertInstaller;)V

    invoke-virtual {v4, v6}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method private createPkcs12PasswordDialog(I)Landroid/app/Dialog;
    .locals 11
    .param p1, "dialogId"    # I

    .prologue
    const/4 v10, 0x0

    .line 441
    const v7, 0x7f020001

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 444
    .local v6, "view":Landroid/view/View;
    const v7, 0x7f090006

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 445
    .local v1, "credentialPassword":Landroid/widget/EditText;
    const/high16 v7, 0x7f090000

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 446
    .local v4, "passwordError":Landroid/widget/TextView;
    const-string v7, "input_method"

    invoke-virtual {p0, v7}, Lcom/android/certinstaller/CertInstaller;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 448
    .local v3, "imm":Landroid/view/inputmethod/InputMethodManager;
    new-instance v0, Lcom/android/certinstaller/CertInstaller$1;

    invoke-direct {v0, p0, v3, v1}, Lcom/android/certinstaller/CertInstaller$1;-><init>(Lcom/android/certinstaller/CertInstaller;Landroid/view/inputmethod/InputMethodManager;Landroid/widget/EditText;)V

    .line 457
    .local v0, "IRunnable":Ljava/lang/Runnable;
    const/4 v7, 0x4

    if-ne p1, v7, :cond_0

    .line 458
    const-string v7, ""

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 459
    const v7, 0x7f06000f

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(I)V

    .line 460
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 463
    :cond_0
    if-eqz v3, :cond_1

    .line 464
    const-wide/16 v8, 0x12c

    invoke-virtual {v1, v0, v8, v9}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 469
    :cond_1
    iget-object v7, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v7}, Lcom/android/certinstaller/CredentialHelper;->getName()Ljava/lang/String;

    move-result-object v5

    .line 470
    .local v5, "title":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    const v7, 0x7f060002

    invoke-virtual {p0, v7}, Lcom/android/certinstaller/CertInstaller;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 473
    :goto_0
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x104000a

    new-instance v9, Lcom/android/certinstaller/CertInstaller$3;

    invoke-direct {v9, p0, v1, p1}, Lcom/android/certinstaller/CertInstaller$3;-><init>(Lcom/android/certinstaller/CertInstaller;Landroid/widget/EditText;I)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const/high16 v8, 0x1040000

    new-instance v9, Lcom/android/certinstaller/CertInstaller$2;

    invoke-direct {v9, p0}, Lcom/android/certinstaller/CertInstaller$2;-><init>(Lcom/android/certinstaller/CertInstaller;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 494
    .local v2, "d":Landroid/app/Dialog;
    new-instance v7, Lcom/android/certinstaller/CertInstaller$4;

    invoke-direct {v7, p0}, Lcom/android/certinstaller/CertInstaller$4;-><init>(Lcom/android/certinstaller/CertInstaller;)V

    invoke-virtual {v2, v7}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 499
    return-object v2

    .line 470
    .end local v2    # "d":Landroid/app/Dialog;
    :cond_2
    const v7, 0x7f060005

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/android/certinstaller/CertInstaller;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private evaluateValidationCertificate([B)V
    .locals 4
    .param p1, "cert"    # [B

    .prologue
    .line 758
    invoke-static {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 760
    .local v0, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->isCertificateValidationAtInstallEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 762
    new-instance v1, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;

    invoke-direct {v1, p0, p0, p1}, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;[B)V

    iput-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 763
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 767
    :goto_0
    return-void

    .line 765
    :cond_0
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->installOthers()V

    goto :goto_0
.end method

.method private getDefaultName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 590
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2}, Lcom/android/certinstaller/CredentialHelper;->getName()Ljava/lang/String;

    move-result-object v1

    .line 591
    .local v1, "name":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 592
    const/4 v2, 0x0

    .line 597
    :goto_0
    return-object v2

    .line 595
    :cond_0
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 596
    .local v0, "index":I
    if-lez v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_1
    move-object v2, v1

    .line 597
    goto :goto_0
.end method

.method private getPkeyMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 398
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mKeyStore:Landroid/security/KeyStore;

    const-string v3, "PKEY_MAP"

    invoke-virtual {v2, v3}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 399
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    .line 400
    invoke-static {v0}, Lcom/android/certinstaller/Util;->fromBytes([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 402
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    if-eqz v1, :cond_0

    .line 404
    .end local v1    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/android/certinstaller/CertInstaller$MyMap;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/certinstaller/CertInstaller$MyMap;-><init>(Lcom/android/certinstaller/CertInstaller$1;)V

    goto :goto_0
.end method

.method private nameCredential()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 360
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v1}, Lcom/android/certinstaller/CredentialHelper;->hasAnyForSystemInstall()Z

    move-result v1

    if-nez v1, :cond_1

    .line 362
    invoke-static {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 363
    .local v0, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_0

    .line 364
    const-string v1, "installer_module"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 368
    :cond_0
    const v1, 0x7f060016

    invoke-direct {p0, v1}, Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V

    .line 373
    .end local v0    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    goto :goto_0
.end method

.method private needsKeyStoreAccess()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v0}, Lcom/android/certinstaller/CredentialHelper;->hasKeyPair()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v0}, Lcom/android/certinstaller/CredentialHelper;->hasUserCertificate()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v0}, Landroid/security/KeyStore;->isUnlocked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onValidationDone(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 741
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/certinstaller/CertInstaller;->removeDialog(I)V

    .line 744
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 745
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->installOthers()V

    .line 754
    :goto_0
    return-void

    .line 747
    :cond_0
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 748
    .local v0, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_1

    .line 749
    const-string v1, "installer_module"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 752
    :cond_1
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    goto :goto_0
.end method

.method private saveKeyPair()V
    .locals 6

    .prologue
    .line 376
    iget-object v3, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    const-string v4, "PKEY"

    invoke-virtual {v3, v4}, Lcom/android/certinstaller/CredentialHelper;->getData(Ljava/lang/String;)[B

    move-result-object v2

    .line 377
    .local v2, "privatekey":[B
    iget-object v3, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    const-string v4, "KEY"

    invoke-virtual {v3, v4}, Lcom/android/certinstaller/CredentialHelper;->getData(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/certinstaller/Util;->toMd5([B)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "key":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->getPkeyMap()Ljava/util/Map;

    move-result-object v1

    .line 379
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    invoke-direct {p0, v1}, Lcom/android/certinstaller/CertInstaller;->savePkeyMap(Ljava/util/Map;)V

    .line 381
    const-string v3, "CertInstaller"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "save privatekey: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " --> #keys:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    return-void
.end method

.method private savePkeyMap(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mKeyStore:Landroid/security/KeyStore;

    const-string v2, "PKEY_MAP"

    invoke-virtual {v1, v2}, Landroid/security/KeyStore;->delete(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 387
    const-string v1, "CertInstaller"

    const-string v2, "savePkeyMap(): failed to delete pkey map"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    invoke-static {p1}, Lcom/android/certinstaller/Util;->toBytes(Ljava/lang/Object;)[B

    move-result-object v0

    .line 392
    .local v0, "bytes":[B
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mKeyStore:Landroid/security/KeyStore;

    const-string v2, "PKEY_MAP"

    const/4 v3, -0x1

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/security/KeyStore;->put(Ljava/lang/String;[BII)Z

    move-result v1

    if-nez v1, :cond_0

    .line 393
    const-string v1, "CertInstaller"

    const-string v2, "savePkeyMap(): failed to write pkey map"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private sendUnlockKeyStoreIntent()V
    .locals 1

    .prologue
    .line 356
    invoke-static {}, Landroid/security/Credentials;->getInstance()Landroid/security/Credentials;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/security/Credentials;->unlock(Landroid/content/Context;)V

    .line 357
    return-void
.end method

.method private toastErrorAndFinish(I)V
    .locals 1
    .param p1, "msgId"    # I

    .prologue
    .line 602
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 603
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    .line 604
    return-void
.end method


# virtual methods
.method installOthers()V
    .locals 7

    .prologue
    .line 331
    iget-object v4, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v4}, Lcom/android/certinstaller/CredentialHelper;->hasKeyPair()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->saveKeyPair()V

    .line 333
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    .line 353
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v4, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v4}, Lcom/android/certinstaller/CredentialHelper;->getUserCertificate()Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 336
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    if-eqz v0, :cond_1

    .line 338
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v4

    invoke-interface {v4}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v4

    invoke-static {v4}, Lcom/android/certinstaller/Util;->toMd5([B)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "key":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->getPkeyMap()Ljava/util/Map;

    move-result-object v2

    .line 340
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 341
    .local v3, "privatekey":[B
    if-eqz v3, :cond_2

    .line 342
    const-string v4, "CertInstaller"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found matched key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    invoke-direct {p0, v2}, Lcom/android/certinstaller/CertInstaller;->savePkeyMap(Ljava/util/Map;)V

    .line 346
    iget-object v4, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v4, v3}, Lcom/android/certinstaller/CredentialHelper;->setPrivateKey([B)V

    .line 351
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    .end local v3    # "privatekey":[B
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->nameCredential()V

    goto :goto_0

    .line 348
    .restart local v1    # "key":Ljava/lang/String;
    .restart local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    .restart local v3    # "privatekey":[B
    :cond_2
    const-string v4, "CertInstaller"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "didn\'t find matched private key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 284
    if-ne p1, v3, :cond_2

    .line 285
    if-ne p2, v4, :cond_1

    .line 286
    const-string v0, "CertInstaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "credential is added: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2}, Lcom/android/certinstaller/CredentialHelper;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    const v0, 0x7f060018

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2}, Lcom/android/certinstaller/CredentialHelper;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/android/certinstaller/CertInstaller;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 290
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v0}, Lcom/android/certinstaller/CredentialHelper;->hasCaCerts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    new-instance v0, Lcom/android/certinstaller/CertInstaller$InstallCaCertsToKeyChainTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/certinstaller/CertInstaller$InstallCaCertsToKeyChainTask;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller$1;)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller$InstallCaCertsToKeyChainTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 304
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-virtual {p0, v4}, Lcom/android/certinstaller/CertInstaller;->setResult(I)V

    .line 303
    :goto_1
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    goto :goto_0

    .line 297
    :cond_1
    const-string v0, "CertInstaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "credential not saved, err: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const v0, 0x7f060015

    invoke-direct {p0, v0}, Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V

    goto :goto_1

    .line 301
    :cond_2
    const-string v0, "CertInstaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedStates"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 111
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/certinstaller/CertInstaller;->createCredentialHelper(Landroid/content/Intent;)Lcom/android/certinstaller/CredentialHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    .line 115
    if-nez p1, :cond_1

    move v2, v3

    :goto_0
    iput v2, p0, Lcom/android/certinstaller/CertInstaller;->mState:I

    .line 118
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 121
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "CERT"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 122
    .local v0, "cert":[B
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v1

    .line 124
    .local v1, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0, v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->isCaCertificateTrusted([BZ)Z

    move-result v2

    if-nez v2, :cond_2

    .line 125
    const-string v2, "CertInstaller"

    const-string v4, "canceling cert installation, non trusted cert"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v2, "installer_module"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4, v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 128
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    .line 169
    :cond_0
    :goto_1
    return-void

    .line 115
    .end local v0    # "cert":[B
    .end local v1    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :cond_1
    const/4 v2, 0x2

    goto :goto_0

    .line 133
    .restart local v0    # "cert":[B
    .restart local v1    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :cond_2
    iget v2, p0, Lcom/android/certinstaller/CertInstaller;->mState:I

    if-ne v2, v3, :cond_7

    .line 134
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2}, Lcom/android/certinstaller/CredentialHelper;->containsAnyRawData()Z

    move-result v2

    if-nez v2, :cond_4

    .line 136
    if-eqz v1, :cond_3

    .line 137
    const-string v2, "installer_module"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4, v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 142
    :cond_3
    const v2, 0x7f060016

    invoke-direct {p0, v2}, Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V

    .line 143
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    goto :goto_1

    .line 144
    :cond_4
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2}, Lcom/android/certinstaller/CredentialHelper;->hasPkcs12KeyStore()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 145
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    goto :goto_1

    .line 149
    :cond_5
    new-instance v2, Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    invoke-direct {v2, p0, p0, v0}, Lcom/android/certinstaller/CertInstaller$InstallOthersAction;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;[B)V

    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    .line 150
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->needsKeyStoreAccess()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 151
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->sendUnlockKeyStoreIntent()V

    goto :goto_1

    .line 153
    :cond_6
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    invoke-virtual {v2}, Lcom/android/certinstaller/CertInstaller$InstallOthersAction;->run()V

    .line 154
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    goto :goto_1

    .line 160
    :cond_7
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v2, p1}, Lcom/android/certinstaller/CredentialHelper;->onRestoreStates(Landroid/os/Bundle;)V

    .line 163
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    if-eqz v2, :cond_0

    .line 164
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    invoke-virtual {v2, p0}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->setActivity(Lcom/android/certinstaller/CertInstaller;)V

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1, "dialogId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 248
    packed-switch p1, :pswitch_data_0

    .line 278
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 252
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/certinstaller/CertInstaller;->createPkcs12PasswordDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 258
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/certinstaller/CertInstaller;->createNameCredentialDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 262
    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 263
    .local v0, "dialog":Landroid/app/ProgressDialog;
    const v1, 0x7f060003

    invoke-virtual {p0, v1}, Lcom/android/certinstaller/CertInstaller;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 264
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 265
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 270
    .end local v0    # "dialog":Landroid/app/ProgressDialog;
    :pswitch_3
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 271
    .restart local v0    # "dialog":Landroid/app/ProgressDialog;
    const v1, 0x7f060004

    invoke-virtual {p0, v1}, Lcom/android/certinstaller/CertInstaller;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 272
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 273
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    invoke-virtual {v0, v2}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->setActivity(Lcom/android/certinstaller/CertInstaller;)V

    .line 233
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->cancel(Z)Z

    .line 234
    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 237
    const v0, 0x7f060015

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 240
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 241
    return-void
.end method

.method onExtractionDone(Z)V
    .locals 5
    .param p1, "success"    # Z

    .prologue
    const/4 v4, 0x1

    .line 409
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/android/certinstaller/CertInstaller;->removeDialog(I)V

    .line 411
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    iget-boolean v2, v2, Lcom/android/certinstaller/CredentialHelper;->mFoundNonTrustedCert:Z

    if-eqz v2, :cond_1

    .line 412
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 413
    .local v0, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_0

    .line 414
    const-string v2, "installer_module"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 417
    :cond_0
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    .line 437
    .end local v0    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :goto_0
    return-void

    .line 421
    :cond_1
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    iget v1, v2, Lcom/android/certinstaller/CredentialHelper;->mPkcs12ValidationResultCode:I

    .line 422
    .local v1, "result":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 423
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 424
    .restart local v0    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v0, :cond_2

    .line 425
    const-string v2, "installer_module"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 428
    :cond_2
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller;->finish()V

    goto :goto_0

    .line 432
    .end local v0    # "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    :cond_3
    if-eqz p1, :cond_4

    .line 433
    invoke-direct {p0}, Lcom/android/certinstaller/CertInstaller;->nameCredential()V

    goto :goto_0

    .line 435
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 204
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/certinstaller/CertInstaller;->mState:I

    .line 205
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 175
    iget v0, p0, Lcom/android/certinstaller/CertInstaller;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 176
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/certinstaller/CertInstaller;->mState:I

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    invoke-virtual {v0}, Lcom/android/certinstaller/CertInstaller$InstallOthersAction;->run()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mInstallOthersAction:Lcom/android/certinstaller/CertInstaller$InstallOthersAction;

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 221
    .local v0, "savedTask":Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->setActivity(Lcom/android/certinstaller/CertInstaller;)V

    .line 223
    iput-object v1, p0, Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 225
    :cond_0
    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outStates"    # Landroid/os/Bundle;

    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 211
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;

    invoke-virtual {v0, p1}, Lcom/android/certinstaller/CredentialHelper;->onSaveStates(Landroid/os/Bundle;)V

    .line 212
    return-void
.end method
