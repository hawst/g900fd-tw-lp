.class abstract Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
.super Landroid/os/AsyncTask;
.source "CertInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/certinstaller/CertInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BaseAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field mActivity:Lcom/android/certinstaller/CertInstaller;

.field mCompleted:Z

.field mResult:Z

.field final synthetic this$0:Lcom/android/certinstaller/CertInstaller;


# direct methods
.method constructor <init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;)V
    .locals 0
    .param p2, "activity"    # Lcom/android/certinstaller/CertInstaller;

    .prologue
    .line 625
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->this$0:Lcom/android/certinstaller/CertInstaller;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 626
    iput-object p2, p0, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    .line 627
    return-void
.end method


# virtual methods
.method abstract notifyActivityTaskCompleted()V
.end method

.method setActivity(Lcom/android/certinstaller/CertInstaller;)V
    .locals 1
    .param p1, "activity"    # Lcom/android/certinstaller/CertInstaller;

    .prologue
    .line 630
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    .line 631
    iget-boolean v0, p0, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->mCompleted:Z

    if-eqz v0, :cond_0

    .line 632
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->notifyActivityTaskCompleted()V

    .line 634
    :cond_0
    return-void
.end method
