.class Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;
.super Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
.source "CertInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/certinstaller/CertInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Pkcs12ExtractTask"
.end annotation


# instance fields
.field mCredentialsInternal:Lcom/android/certinstaller/CredentialHelper;

.field mPassword:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/certinstaller/CertInstaller;


# direct methods
.method constructor <init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CredentialHelper;Ljava/lang/String;)V
    .locals 1
    .param p2, "activity"    # Lcom/android/certinstaller/CertInstaller;
    .param p3, "credentials"    # Lcom/android/certinstaller/CredentialHelper;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 643
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->this$0:Lcom/android/certinstaller/CertInstaller;

    .line 644
    invoke-direct {p0, p1, p2}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;)V

    .line 645
    new-instance v0, Lcom/android/certinstaller/CredentialHelper;

    invoke-direct {v0, p3}, Lcom/android/certinstaller/CredentialHelper;-><init>(Lcom/android/certinstaller/CredentialHelper;)V

    iput-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mCredentialsInternal:Lcom/android/certinstaller/CredentialHelper;

    .line 646
    iput-object p4, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mPassword:Ljava/lang/String;

    .line 647
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mCredentialsInternal:Lcom/android/certinstaller/CredentialHelper;

    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CredentialHelper;->extractPkcs12(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 639
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method notifyActivityTaskCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 670
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mCredentialsInternal:Lcom/android/certinstaller/CredentialHelper;

    # setter for: Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;
    invoke-static {v0, v1}, Lcom/android/certinstaller/CertInstaller;->access$102(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CredentialHelper;)Lcom/android/certinstaller/CredentialHelper;

    .line 673
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    iget-boolean v1, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mResult:Z

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller;->onExtractionDone(Z)V

    .line 674
    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    .line 675
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->this$0:Lcom/android/certinstaller/CertInstaller;

    # setter for: Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    invoke-static {v0, v2}, Lcom/android/certinstaller/CertInstaller;->access$402(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 677
    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 664
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mResult:Z

    .line 665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mCompleted:Z

    .line 666
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->notifyActivityTaskCompleted()V

    .line 667
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 639
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    .line 655
    :cond_0
    return-void
.end method
