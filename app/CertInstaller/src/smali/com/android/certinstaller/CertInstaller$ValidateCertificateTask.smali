.class Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;
.super Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
.source "CertInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/certinstaller/CertInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValidateCertificateTask"
.end annotation


# instance fields
.field mCert:[B

.field mCertValidationResultCode:I

.field final synthetic this$0:Lcom/android/certinstaller/CertInstaller;


# direct methods
.method constructor <init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;[B)V
    .locals 1
    .param p2, "activity"    # Lcom/android/certinstaller/CertInstaller;
    .param p3, "cert"    # [B

    .prologue
    .line 684
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->this$0:Lcom/android/certinstaller/CertInstaller;

    .line 685
    invoke-direct {p0, p1, p2}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;)V

    .line 682
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCertValidationResultCode:I

    .line 686
    iput-object p3, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCert:[B

    .line 687
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 14
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const-wide/16 v12, 0x1f4

    .line 698
    iget-object v7, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    invoke-static {v7}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    .line 699
    .local v0, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    const/4 v6, 0x1

    .line 700
    .local v6, "result":Z
    if-eqz v0, :cond_1

    .line 703
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 704
    .local v8, "startTime":J
    iget-object v7, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCert:[B

    invoke-virtual {v0, v7}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->validateCertificateAtInstall([B)I

    move-result v7

    iput v7, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCertValidationResultCode:I

    .line 705
    iget v7, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCertValidationResultCode:I

    const/4 v10, -0x1

    if-eq v7, v10, :cond_0

    .line 706
    const/4 v6, 0x0

    .line 708
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 709
    .local v4, "endTime":J
    sub-long v2, v4, v8

    .line 711
    .local v2, "diffTime":J
    cmp-long v7, v2, v12

    if-gez v7, :cond_1

    .line 713
    sub-long v10, v12, v2

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 720
    .end local v2    # "diffTime":J
    .end local v4    # "endTime":J
    .end local v8    # "startTime":J
    :cond_1
    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    return-object v7

    .line 714
    .restart local v2    # "diffTime":J
    .restart local v4    # "endTime":J
    .restart local v8    # "startTime":J
    :catch_0
    move-exception v1

    .line 715
    .local v1, "ex":Ljava/lang/InterruptedException;
    const-string v7, "CertInstaller"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Sleep exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 680
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method notifyActivityTaskCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 731
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    iget v1, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCertValidationResultCode:I

    # invokes: Lcom/android/certinstaller/CertInstaller;->onValidationDone(I)V
    invoke-static {v0, v1}, Lcom/android/certinstaller/CertInstaller;->access$500(Lcom/android/certinstaller/CertInstaller;I)V

    .line 733
    iput-object v2, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    .line 734
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->this$0:Lcom/android/certinstaller/CertInstaller;

    # setter for: Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    invoke-static {v0, v2}, Lcom/android/certinstaller/CertInstaller;->access$402(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 736
    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 725
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mResult:Z

    .line 726
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mCompleted:Z

    .line 727
    invoke-virtual {p0}, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->notifyActivityTaskCompleted()V

    .line 728
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 680
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/android/certinstaller/CertInstaller$ValidateCertificateTask;->mActivity:Lcom/android/certinstaller/CertInstaller;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    .line 694
    :cond_0
    return-void
.end method
