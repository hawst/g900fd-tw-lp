.class Lcom/android/certinstaller/CertInstaller$3;
.super Ljava/lang/Object;
.source "CertInstaller.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/certinstaller/CertInstaller;->createPkcs12PasswordDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/certinstaller/CertInstaller;

.field final synthetic val$credentialPassword:Landroid/widget/EditText;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/android/certinstaller/CertInstaller;Landroid/widget/EditText;I)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    iput-object p2, p0, Lcom/android/certinstaller/CertInstaller$3;->val$credentialPassword:Landroid/widget/EditText;

    iput p3, p0, Lcom/android/certinstaller/CertInstaller$3;->val$dialogId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 480
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$3;->val$credentialPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "password":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    iget v2, p0, Lcom/android/certinstaller/CertInstaller$3;->val$dialogId:I

    invoke-virtual {v1, v2}, Lcom/android/certinstaller/CertInstaller;->removeDialog(I)V

    .line 482
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    new-instance v2, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;

    iget-object v3, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    iget-object v4, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    iget-object v5, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    # getter for: Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;
    invoke-static {v5}, Lcom/android/certinstaller/CertInstaller;->access$100(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CredentialHelper;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/android/certinstaller/CertInstaller$Pkcs12ExtractTask;-><init>(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CredentialHelper;Ljava/lang/String;)V

    # setter for: Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    invoke-static {v1, v2}, Lcom/android/certinstaller/CertInstaller;->access$402(Lcom/android/certinstaller/CertInstaller;Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    .line 483
    iget-object v1, p0, Lcom/android/certinstaller/CertInstaller$3;->this$0:Lcom/android/certinstaller/CertInstaller;

    # getter for: Lcom/android/certinstaller/CertInstaller;->mCurrentTask:Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;
    invoke-static {v1}, Lcom/android/certinstaller/CertInstaller;->access$400(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/android/certinstaller/CertInstaller$BaseAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 486
    return-void
.end method
