.class Lcom/android/certinstaller/CertInstaller$7;
.super Ljava/lang/Object;
.source "CertInstaller.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/certinstaller/CertInstaller;->createNameCredentialDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/certinstaller/CertInstaller;

.field final synthetic val$dialogId:I

.field final synthetic val$nameInput:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/android/certinstaller/CertInstaller;Landroid/widget/EditText;I)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    iput-object p2, p0, Lcom/android/certinstaller/CertInstaller$7;->val$nameInput:Landroid/widget/EditText;

    iput p3, p0, Lcom/android/certinstaller/CertInstaller$7;->val$dialogId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 555
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->val$nameInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 556
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    iget v3, p0, Lcom/android/certinstaller/CertInstaller$7;->val$dialogId:I

    invoke-virtual {v2, v3}, Lcom/android/certinstaller/CertInstaller;->removeDialog(I)V

    .line 557
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 558
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/android/certinstaller/CertInstaller;->showDialog(I)V

    .line 573
    :goto_0
    return-void

    .line 560
    :cond_0
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    # getter for: Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;
    invoke-static {v2}, Lcom/android/certinstaller/CertInstaller;->access$100(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CredentialHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/certinstaller/CredentialHelper;->setName(Ljava/lang/String;)V

    .line 564
    :try_start_0
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    iget-object v3, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    # getter for: Lcom/android/certinstaller/CertInstaller;->mCredentials:Lcom/android/certinstaller/CredentialHelper;
    invoke-static {v3}, Lcom/android/certinstaller/CertInstaller;->access$100(Lcom/android/certinstaller/CertInstaller;)Lcom/android/certinstaller/CredentialHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/certinstaller/CredentialHelper;->createSystemInstallIntent()Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/certinstaller/CertInstaller;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 567
    :catch_0
    move-exception v0

    .line 568
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "CertInstaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "systemInstall(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v2, p0, Lcom/android/certinstaller/CertInstaller$7;->this$0:Lcom/android/certinstaller/CertInstaller;

    const v3, 0x7f060015

    # invokes: Lcom/android/certinstaller/CertInstaller;->toastErrorAndFinish(I)V
    invoke-static {v2, v3}, Lcom/android/certinstaller/CertInstaller;->access$300(Lcom/android/certinstaller/CertInstaller;I)V

    goto :goto_0
.end method
