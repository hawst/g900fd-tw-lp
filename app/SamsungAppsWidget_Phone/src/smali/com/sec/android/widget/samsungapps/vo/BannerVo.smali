.class public Lcom/sec/android/widget/samsungapps/vo/BannerVo;
.super Ljava/lang/Object;
.source "BannerVo.java"


# instance fields
.field private mAppID:Ljava/lang/String;

.field private mBannerDescription:Ljava/lang/String;

.field private mBannerImgURL:Ljava/lang/String;

.field private mBannerIndex:Ljava/lang/String;

.field private mBannerLinkURL:Ljava/lang/String;

.field private mBannerPos:Ljava/lang/String;

.field private mBannerProductID:Ljava/lang/String;

.field private mBannerTitle:Ljava/lang/String;

.field private mBannerType:Ljava/lang/String;

.field private mNumValue:J

.field private mPromotionTitle:Ljava/lang/String;

.field private mWidgetBGImgURL:Ljava/lang/String;

.field private mWidgetSymbolImgURL:Ljava/lang/String;

.field private mWidgetTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mNumValue:J

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerProductID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerImgURL:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerType:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mPromotionTitle:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerTitle:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerDescription:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerLinkURL:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerPos:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerIndex:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mAppID:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetTitle:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetBGImgURL:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetSymbolImgURL:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getNumValue()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mNumValue:J

    return-wide v0
.end method

.method public getmAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerIndex()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerIndex:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerLinkURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerLinkURL:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerPos()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerPos:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getmBannerType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerType:Ljava/lang/String;

    return-object v0
.end method

.method public getmPromotionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mPromotionTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getmWidgetBGImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetBGImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getmWidgetSymbolImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetSymbolImgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getmWidgetTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setNumValue(J)V
    .locals 1
    .param p1, "_totalNum"    # J

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mNumValue:J

    .line 28
    return-void
.end method

.method public setmAppID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAppID"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mAppID:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setmBannerDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerDescription"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerDescription:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setmBannerImgURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerImgURL"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerImgURL:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setmBannerIndex(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerIndex"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerIndex:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setmBannerLinkURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerLinkURL"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerLinkURL:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setmBannerPos(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerPos"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerPos:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setmBannerProductID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerProductID"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerProductID:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setmBannerTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerTitle"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerTitle:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setmBannerType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mBannerType"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mBannerType:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setmPromotionTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPromotionTitle"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mPromotionTitle:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setmWidgetBGImgURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWidgetBGImgURL"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetBGImgURL:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setmWidgetSymbolImgURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWidgetSymbolImgURL"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetSymbolImgURL:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setmWidgetTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWidgetTitle"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->mWidgetTitle:Ljava/lang/String;

    .line 132
    return-void
.end method
