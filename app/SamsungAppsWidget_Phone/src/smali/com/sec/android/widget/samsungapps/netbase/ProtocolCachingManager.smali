.class public Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;
.super Ljava/lang/Object;
.source "ProtocolCachingManager.java"


# static fields
.field static final CACHING_EFFECTIVE_TIME:J = 0x36ee80L

.field public static PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager; = null

.field static final TAG:Ljava/lang/String; = "ProtocolCachingManager"


# instance fields
.field private mCachingData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/widget/samsungapps/netbase/CachingData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    .line 22
    sput-object p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;-><init>()V

    sput-object v0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    .line 32
    :cond_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->PROTOCOL_CACHING_MGR:Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    return-object v0
.end method


# virtual methods
.method declared-synchronized clearData()V
    .locals 1

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_0
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getData(Ljava/lang/String;)Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .locals 2
    .param p1, "_key"    # Ljava/lang/String;

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 39
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    .line 41
    .local v0, "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    if-eqz v0, :cond_0

    .line 43
    new-instance v1, Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    invoke-direct {v1, v0}, Lcom/sec/android/widget/samsungapps/netbase/CachingData;-><init>(Lcom/sec/android/widget/samsungapps/netbase/CachingData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    .end local v0    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 37
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public isCachingProtocolData(Ljava/lang/String;)Z
    .locals 1
    .param p1, "_data"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v0, ".*contentCategoryProductList.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*getCategoryName.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*featuredProductList2Notc.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*localFeaturedProductList2Notc.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*promotionBannerListEx2Notc.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*localBannerList2Notc.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".*allProductList2Notc.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    :cond_0
    const/4 v0, 0x1

    .line 117
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTimeOutData(Lcom/sec/android/widget/samsungapps/netbase/CachingData;)Z
    .locals 8
    .param p1, "_cachingData"    # Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    .prologue
    const/4 v2, 0x0

    .line 123
    if-nez p1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v2

    .line 128
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mUpdateTime:J

    sub-long v0, v4, v6

    .line 130
    .local v0, "curTime":J
    const-wide/32 v4, 0x36ee80

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 132
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public declared-synchronized putData(Ljava/lang/String;Lcom/sec/android/widget/samsungapps/netbase/CachingData;)V
    .locals 2
    .param p1, "_key"    # Ljava/lang/String;
    .param p2, "_data"    # Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->isCachingProtocolData(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeData(Ljava/lang/String;)V
    .locals 1
    .param p1, "_key"    # Ljava/lang/String;

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    monitor-exit p0

    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeKeyString(Ljava/lang/String;)V
    .locals 6
    .param p1, "_keyString"    # Ljava/lang/String;

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    .line 62
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 63
    .local v2, "keyList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v3, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 67
    .local v1, "key":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v4, v5, :cond_0

    .line 69
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 60
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "keyList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 73
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "keyList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_2

    .line 75
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 77
    .restart local v1    # "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->mCachingData:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 81
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "keyList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    monitor-exit p0

    return-void
.end method
