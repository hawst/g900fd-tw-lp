.class public Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;
.super Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;
.source "AsyncTaskLoad4x2Data.java"


# instance fields
.field private mTempAppsForGalaxyAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempContentGiftAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempFeaturedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLifestyleAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLocalAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[IIZ)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceIds"    # [I
    .param p3, "_widgetSize"    # I
    .param p4, "_useCache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempLocalAppList:Ljava/util/ArrayList;

    .line 33
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 35
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempLifestyleAppList:Ljava/util/ArrayList;

    .line 36
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempAppsForGalaxyAppList:Ljava/util/ArrayList;

    .line 37
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 48
    sget-object v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->TAG:Ljava/lang/String;

    const-string v2, "AsyncTaskLoad4x2Data()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v0

    .line 52
    .local v0, "dataManager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mContext:Landroid/content/Context;

    .line 53
    iput p3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mCurrentWidgetSize:I

    .line 54
    iput-boolean p4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mUseCache:Z

    .line 55
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mInstanceIds:[I

    .line 56
    iget v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mCurrentWidgetSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 57
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 23
    .param p1, "_params"    # [Ljava/lang/String;

    .prologue
    .line 68
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 69
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 70
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempLifestyleAppList:Ljava/util/ArrayList;

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempAppsForGalaxyAppList:Ljava/util/ArrayList;

    .line 72
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 74
    const/4 v14, 0x0

    .line 75
    .local v14, "mTempAppsForGalaxyAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v15, 0x0

    .line 76
    .local v15, "mTempContentGiftAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v17, 0x0

    .line 77
    .local v17, "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v16, 0x0

    .line 84
    .local v16, "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :try_start_0
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;

    move-result-object v11

    .line 85
    .local v11, "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v2}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->doCountrySearchEx(Landroid/content/Context;)V

    .line 87
    const-string v3, ""

    .line 89
    .local v3, "strAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mUseCache:Z

    if-nez v2, :cond_0

    .line 91
    sget-object v3, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 93
    const/4 v2, 0x1

    const-string v5, ""

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v2, v5, :cond_0

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "API2300"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mErrorMessage:Ljava/lang/String;

    .line 99
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 220
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :goto_0
    return-object v2

    .line 104
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_0
    sget-boolean v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    if-eqz v2, :cond_1

    .line 105
    sget-object v3, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingDataHostUrl:Ljava/lang/String;

    .line 109
    :cond_1
    sget-object v2, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mschoi  strAddress  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForCuratedWidgetBannerList()Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "requestBannerXml":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mschoi  mUseCache  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mUseCache:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mUseCache:Z

    if-eqz v2, :cond_2

    .line 119
    new-instance v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 120
    .local v8, "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "curatedWidgetBannerList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 128
    :goto_1
    if-eqz v8, :cond_6

    iget-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_6

    const-string v2, ""

    iget-object v5, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 134
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v20

    .line 135
    .local v20, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v20 .. v20}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v22

    .line 136
    .local v22, "xr":Lorg/xml/sax/XMLReader;
    new-instance v21, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 138
    .local v21, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 139
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    iget-object v6, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 141
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    .line 147
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2328"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 150
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mErrorMessage:Ljava/lang/String;

    .line 151
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 124
    .end local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v20    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v21    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v22    # "xr":Lorg/xml/sax/XMLReader;
    :cond_2
    const/16 v5, 0x2710

    const/16 v6, 0x2710

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mUseProtocolCache:Z

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v8

    .restart local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto :goto_1

    .line 152
    .restart local v20    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v21    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v22    # "xr":Lorg/xml/sax/XMLReader;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 153
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2328"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 154
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mWarningMessage:Ljava/lang/String;

    .line 162
    :cond_4
    new-instance v19, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 163
    .local v19, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v2, 0x3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 166
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 167
    .local v9, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 169
    .end local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v12    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v10

    .line 170
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 175
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_5
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v18, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 182
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v13, "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 185
    .restart local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v9}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmWidgetSymbolImgURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 217
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v4    # "requestBannerXml":Ljava/lang/String;
    .end local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v19    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v20    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v21    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v22    # "xr":Lorg/xml/sax/XMLReader;
    :catch_1
    move-exception v10

    .line 218
    .restart local v10    # "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mErrorMessage:Ljava/lang/String;

    .line 219
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 220
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 157
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v4    # "requestBannerXml":Ljava/lang/String;
    .restart local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .restart local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_6
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mErrorMessage:Ljava/lang/String;

    .line 158
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 191
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v18    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .restart local v19    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v20    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v21    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v22    # "xr":Lorg/xml/sax/XMLReader;
    :cond_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v13, v6}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->downloadImage(Landroid/content/Context;Ljava/util/ArrayList;Z)Z

    move-result v5

    if-ne v2, v5, :cond_8

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "curatedWidgetBannerList2Notc"

    iget-object v6, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 210
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->restorePageList()V

    .line 211
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 213
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->mErrorMessage:Ljava/lang/String;

    .line 214
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
