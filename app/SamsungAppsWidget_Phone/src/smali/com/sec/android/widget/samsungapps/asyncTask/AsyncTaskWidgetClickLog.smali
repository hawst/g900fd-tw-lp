.class public Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;
.super Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;
.source "AsyncTaskWidgetClickLog.java"


# static fields
.field private static final DEFAULT_TIME_OUT:I = 0x3a98

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private imei:Ljava/lang/String;

.field private logList:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mErrorMessage:Ljava/lang/String;

.field private mUseProtocolCache:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1, "imei"    # Ljava/lang/String;
    .param p2, "logList"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mUseProtocolCache:Z

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->imei:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->logList:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mErrorMessage:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mContext:Landroid/content/Context;

    .line 25
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->imei:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->logList:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method private getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;
    .locals 4
    .param p1, "_data"    # Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    .prologue
    .line 61
    const-string v0, ""

    .line 63
    .local v0, "errorMessage":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 64
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0026

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 80
    :goto_0
    return-object v2

    .line 68
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->getErrString()I

    move-result v1

    .line 70
    .local v1, "mesID":I
    iget v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    if-eqz v2, :cond_2

    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_1
    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    move-object v2, v0

    .line 80
    goto :goto_0

    .line 73
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 8
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/16 v3, 0x3a98

    .line 32
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;

    move-result-object v6

    .line 33
    .local v6, "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    const-string v1, ""

    .line 35
    .local v1, "strAddress":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 36
    const-string v0, "http://"

    const-string v4, "https://"

    invoke-virtual {v1, v0, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 38
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->imei:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->logList:Ljava/lang/String;

    invoke-virtual {v6, v0, v4}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForWidgetClickLog(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "widgetClickLogXml":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mUseProtocolCache:Z

    move-object v0, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v7

    .line 44
    .local v7, "netReturn":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    invoke-direct {p0, v7}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mErrorMessage:Ljava/lang/String;

    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 10
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "_result"    # Ljava/lang/Boolean;

    .prologue
    .line 51
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    sget-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->TAG:Ljava/lang/String;

    const-string v1, "WidgetClickLog Task successful"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :goto_0
    return-void

    .line 54
    :cond_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->TAG:Ljava/lang/String;

    const-string v1, "WidgetClickLog Task failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 10
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
