.class Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;
.super Ljava/lang/Object;
.source "WidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/WidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewFactory"
.end annotation


# instance fields
.field private mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

.field private final mContext:Landroid/content/Context;

.field private mCurrentWidgetSize:I

.field private mInstanceId:I

.field mPrefs:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/sec/android/widget/samsungapps/WidgetService;


# direct methods
.method public constructor <init>(Lcom/sec/android/widget/samsungapps/WidgetService;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p2, "_context"    # Landroid/content/Context;
    .param p3, "_intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 68
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->this$0:Lcom/sec/android/widget/samsungapps/WidgetService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    .line 69
    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mInstanceId:I

    .line 73
    const-string v0, "WIDGET_SIZE"

    const/16 v1, 0x10

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    .line 78
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v0, p2}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    .line 81
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 82
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mPrefs:Landroid/content/SharedPreferences;

    .line 86
    :cond_0
    return-void
.end method

.method private getSubtitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 182
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategoryName()Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "selectedCategoryName":Ljava/lang/String;
    const v1, 0x7f0a002b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%s"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;III)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_action"    # Ljava/lang/String;
    .param p4, "_rViews"    # Landroid/widget/RemoteViews;
    .param p5, "_rId"    # I
    .param p6, "_page"    # I
    .param p7, "_index"    # I

    .prologue
    const/4 v3, 0x0

    .line 241
    new-instance v0, Landroid/content/Intent;

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 245
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 247
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 250
    if-ltz p6, :cond_0

    .line 251
    const-string v2, "page"

    invoke-virtual {v0, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 254
    :cond_0
    const/4 v2, -0x1

    if-ne p7, v2, :cond_1

    .line 255
    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 256
    .local v1, "pIntent":Landroid/app/PendingIntent;
    invoke-virtual {p4, p5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 268
    :goto_0
    return-void

    .line 258
    .end local v1    # "pIntent":Landroid/app/PendingIntent;
    :cond_1
    const-string v2, "index"

    invoke-virtual {v0, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 260
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 265
    .restart local v1    # "pIntent":Landroid/app/PendingIntent;
    invoke-virtual {p4, p5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private setOnclickActionInAdapterView(Ljava/lang/String;Landroid/widget/RemoteViews;III)V
    .locals 3
    .param p1, "_action"    # Ljava/lang/String;
    .param p2, "_rViews"    # Landroid/widget/RemoteViews;
    .param p3, "_rId"    # I
    .param p4, "_page"    # I
    .param p5, "_index"    # I

    .prologue
    .line 285
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 286
    .local v1, "fillInIntent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 288
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "page"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    const-string v2, "index"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 291
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 293
    invoke-virtual {p2, p3, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 294
    return-void
.end method

.method private setOneContentPage(Lcom/sec/android/widget/samsungapps/vo/PageVo;Landroid/widget/RemoteViews;I)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .param p2, "_rv"    # Landroid/widget/RemoteViews;
    .param p3, "_page"    # I

    .prologue
    .line 306
    invoke-virtual {p1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getItemCounts()[I

    move-result-object v0

    .line 410
    .local v0, "mItemCounts":[I
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    packed-switch v0, :pswitch_data_0

    .line 113
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 104
    :pswitch_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetService;->m4x2LayoutIds:[I

    array-length v0, v0

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "_position"    # I

    .prologue
    .line 211
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 9
    .param p1, "_position"    # I

    .prologue
    .line 120
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 122
    .local v4, "token":J
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v1

    .line 123
    .local v1, "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iget v7, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    invoke-virtual {v1, v7}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v6

    .line 125
    .local v6, "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    const/4 v2, 0x0

    .line 128
    .local v2, "rv":Landroid/widget/RemoteViews;
    :try_start_0
    iget v7, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    packed-switch v7, :pswitch_data_0

    .line 160
    :goto_0
    invoke-virtual {v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {p0, v7, v2, p1}, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->setOneContentPage(Lcom/sec/android/widget/samsungapps/vo/PageVo;Landroid/widget/RemoteViews;I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 178
    :goto_1
    return-object v2

    .line 130
    :pswitch_0
    :try_start_1
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/widget/samsungapps/WidgetService;->m4x2LayoutIds:[I

    aget v8, v8, p1

    invoke-direct {v3, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v2    # "rv":Landroid/widget/RemoteViews;
    .local v3, "rv":Landroid/widget/RemoteViews;
    move-object v2, v3

    .end local v3    # "rv":Landroid/widget/RemoteViews;
    .restart local v2    # "rv":Landroid/widget/RemoteViews;
    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "caught array index out of bound exception. data might be changed while sleep mode"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 166
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "catch numberformat exception, media object may gone invalid due to system change"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 175
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 169
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_4
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "caught null pointer exception. check source is deleted"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 175
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 172
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 175
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v7

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;->mCurrentWidgetSize:I

    packed-switch v0, :pswitch_data_0

    .line 206
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 197
    :pswitch_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetService;->m4x2LayoutIds:[I

    array-length v0, v0

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onDataSetChanged()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method
