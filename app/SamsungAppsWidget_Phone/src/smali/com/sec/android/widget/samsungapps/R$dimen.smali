.class public final Lcom/sec/android/widget/samsungapps/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final app_height_land:I = 0x7f090009

.field public static final app_left_margin:I = 0x7f090007

.field public static final app_right_margin:I = 0x7f090008

.field public static final app_text_width:I = 0x7f09000a

.field public static final border_btn_banners:I = 0x7f090069

.field public static final border_btn_banners_4x6:I = 0x7f09006a

.field public static final four_cell_height:I = 0x7f090002

.field public static final four_cell_width:I = 0x7f090000

.field public static final icon_size:I = 0x7f090005

.field public static final icon_size_land:I = 0x7f090006

.field public static final icon_size_land_4x2:I = 0x7f090058

.field public static final img_app_icon_size:I = 0x7f09000b

.field public static final img_app_icon_size_4x6:I = 0x7f09000c

.field public static final img_banner_height:I = 0x7f090012

.field public static final img_banner_width:I = 0x7f090011

.field public static final img_category_icon_size:I = 0x7f090014

.field public static final img_next_btn:I = 0x7f09000f

.field public static final img_pregressbar_size:I = 0x7f090013

.field public static final img_prev_btn:I = 0x7f090010

.field public static final img_refresh_btn:I = 0x7f09000d

.field public static final img_setting_btn:I = 0x7f09000e

.field public static final main_widget_unpack_guide_width:I = 0x7f090049

.field public static final margin_btn_apps:I = 0x7f09003d

.field public static final margin_btn_apps_4x2:I = 0x7f09003e

.field public static final margin_btn_apps_4x6:I = 0x7f09003f

.field public static final margin_btn_apps_and_banners:I = 0x7f090042

.field public static final margin_btn_apps_and_banners_4x6:I = 0x7f090043

.field public static final margin_btn_apps_land:I = 0x7f090065

.field public static final margin_btn_descriptions_land_4x2:I = 0x7f090062

.field public static final margin_btn_icon_and_description:I = 0x7f090040

.field public static final margin_btn_icon_and_description_4x6:I = 0x7f090041

.field public static final margin_btn_icon_and_description_land:I = 0x7f090066

.field public static final margin_btn_icon_and_description_land_4x2:I = 0x7f090057

.field public static final margin_btn_statusbar_and_bottom:I = 0x7f090046

.field public static final margin_btn_statusbar_and_bottom_land:I = 0x7f09004d

.field public static final margin_btn_statusbar_and_bottom_land_4x6:I = 0x7f09004e

.field public static final margin_btn_statusbar_and_bottom_port_4x6:I = 0x7f09004f

.field public static final margin_btn_statusbar_and_left:I = 0x7f090045

.field public static final margin_btn_statusbar_and_left_land:I = 0x7f090050

.field public static final margin_btn_statusbar_and_left_land_4x6:I = 0x7f090051

.field public static final margin_btn_statusbar_and_left_port_4x6:I = 0x7f090052

.field public static final margin_btn_statusbar_and_right:I = 0x7f090044

.field public static final margin_btn_statusbar_and_right_land:I = 0x7f090053

.field public static final margin_btn_statusbar_and_right_land_4x6:I = 0x7f090054

.field public static final margin_btn_statusbar_and_right_port_4x6:I = 0x7f090055

.field public static final margin_btn_subtitle_and_apps:I = 0x7f09003b

.field public static final margin_btn_subtitle_and_apps_4x2:I = 0x7f09003c

.field public static final margin_btn_title_and_subtitle:I = 0x7f090039

.field public static final margin_btn_title_and_subtitle_4x2:I = 0x7f09003a

.field public static final margin_btn_title_and_subtitle_land:I = 0x7f090063

.field public static final margin_btn_top_and_search_land:I = 0x7f09004c

.field public static final margin_btn_top_and_title:I = 0x7f090036

.field public static final margin_btn_top_and_title_4x2:I = 0x7f090037

.field public static final margin_btn_top_and_title_4x6:I = 0x7f090038

.field public static final margin_btn_top_and_title_land:I = 0x7f09004a

.field public static final margin_category_bottom:I = 0x7f090048

.field public static final margin_category_side:I = 0x7f090047

.field public static final margin_description_side_4x2:I = 0x7f09001e

.field public static final margin_description_side_4x2_1dp:I = 0x7f09001f

.field public static final margin_description_side_4x6:I = 0x7f090020

.field public static final margin_intro_layout_bottom:I = 0x7f09002a

.field public static final margin_intro_layout_bottom_4x6:I = 0x7f09002b

.field public static final margin_intro_layout_bottom_land:I = 0x7f09002c

.field public static final margin_intro_layout_bottom_land_4x2:I = 0x7f09002d

.field public static final margin_intro_layout_bottom_land_4x6:I = 0x7f09002e

.field public static final margin_intro_layout_left:I = 0x7f090021

.field public static final margin_intro_layout_left_4x6:I = 0x7f090023

.field public static final margin_intro_layout_left_land:I = 0x7f090022

.field public static final margin_intro_layout_right:I = 0x7f090024

.field public static final margin_intro_layout_right_4x6:I = 0x7f090026

.field public static final margin_intro_layout_right_land:I = 0x7f090025

.field public static final margin_intro_layout_top_land:I = 0x7f090027

.field public static final margin_intro_layout_top_land_4x2:I = 0x7f090028

.field public static final margin_intro_layout_top_land_4x6:I = 0x7f090029

.field public static final margin_page_layout_left:I = 0x7f09002f

.field public static final margin_page_layout_left_4x2:I = 0x7f090030

.field public static final margin_page_layout_left_4x6:I = 0x7f090031

.field public static final margin_page_layout_left_land:I = 0x7f09004b

.field public static final margin_page_layout_left_port_4x6:I = 0x7f090032

.field public static final margin_page_layout_right:I = 0x7f090033

.field public static final margin_page_layout_right_4x2:I = 0x7f090034

.field public static final margin_page_layout_right_4x6:I = 0x7f090035

.field public static final margin_page_layout_right_land:I = 0x7f090056

.field public static final one_cell_height:I = 0x7f090004

.field public static final padding_app_icon:I = 0x7f090068

.field public static final padding_app_icon_land:I = 0x7f090067

.field public static final page_banner_item_height:I = 0x7f09001d

.field public static final progressbar_rate_height:I = 0x7f09005a

.field public static final progressbar_rate_height_4x2:I = 0x7f09005d

.field public static final progressbar_rate_height_4x6:I = 0x7f09005c

.field public static final progressbar_rate_height_land:I = 0x7f090060

.field public static final progressbar_rate_height_land_4x2:I = 0x7f090061

.field public static final progressbar_rate_width:I = 0x7f090059

.field public static final progressbar_rate_width_4x6:I = 0x7f09005b

.field public static final progressbar_rate_width_land:I = 0x7f09005e

.field public static final progressbar_rate_width_land_4x2:I = 0x7f09005f

.field public static final two_cell_height:I = 0x7f090003

.field public static final two_cell_width:I = 0x7f090001

.field public static final txt_app_description_size:I = 0x7f090017

.field public static final txt_app_description_size_land:I = 0x7f090064

.field public static final txt_app_description_size_small:I = 0x7f090018

.field public static final txt_category_name_size:I = 0x7f09001b

.field public static final txt_category_name_size_land:I = 0x7f09001c

.field public static final txt_last_refresh_date:I = 0x7f090019

.field public static final txt_last_refresh_date_4x6:I = 0x7f09001a

.field public static final txt_sub_title:I = 0x7f090015

.field public static final txt_sub_title_4x6:I = 0x7f090016


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
