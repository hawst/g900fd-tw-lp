.class public Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# static fields
.field public static final KEY_ANDROID_PLATFORM_KEY_VERSION:Ljava/lang/String; = "version2"

.field public static final KEY_ANDROID_SDK_VERSION:Ljava/lang/String; = "openApiVersion"

.field public static final KEY_MCC:Ljava/lang/String; = "mcc"

.field public static final KEY_MNC:Ljava/lang/String; = "mnc"

.field public static final KEY_MODEL_NAME:Ljava/lang/String; = "deviceModel"

.field public static final KEY_ODC_OPEN_API_VERSION:Ljava/lang/String; = "version"

.field public static final KEY_SALES_CODE:Ljava/lang/String; = "csc"

.field public static final KEY_USE_FILTER:Ljava/lang/String; = "filter"

.field public static final PLATFORM_KEY_NEW:I = 0x3

.field public static final PLATFORM_KEY_OLD:I = 0x1

.field public static final PLATFORM_KEY_UNKNOWN:I = 0x0

.field public static final SAMSUNG_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field private static TAG:Ljava/lang/String; = null

.field public static final WIDGET_RENDERER_USE:Z = true

.field public static final WIDGET_SINGLE_INSTANCE:Z = false

.field public static final WIDGET_SINGLE_INSTANCE_ID:I = 0x1

.field private static final __SHARED_PREFERENCES_KEY_DISCLAIMERSKIP:Ljava/lang/String; = "DisclaimerSkip"

.field private static final __SHARED_PREFERENCES_KEY_SELECTEDMCC:Ljava/lang/String; = "SelectedMcc"

.field private static final __SHARED_PREFERENCES_NAME:Ljava/lang/String; = "SamsungAppsSharedPreferences"

.field public static currencyUnitDivision:Z = false

.field public static currencyUnitPrecedes:Z = false

.field public static gAndroidPlatformKeyVersion:Ljava/lang/String; = null

.field public static gFreeStoreClsf:Z = false

.field public static gImei:Ljava/lang/String; = null

.field private static gInstance:Lcom/sec/android/widget/samsungapps/utils/DeviceInfo; = null

.field public static gIsStaging:Z = false

.field public static gLanguage:Ljava/lang/String; = null

.field public static gMcc:Ljava/lang/String; = null

.field public static gMccMnc:Ljava/lang/String; = null

.field public static gMnc:Ljava/lang/String; = null

.field public static gModel:Ljava/lang/String; = null

.field public static gOdcOpenApiVersion:Ljava/lang/String; = null

.field public static gSalesCode:Ljava/lang/String; = null

.field public static gSamsungAppsVersionName:Ljava/lang/String; = null

.field public static gSdkVersion:Ljava/lang/String; = null

.field public static gSerial:Ljava/lang/String; = null

.field public static gStagingDataHostUrl:Ljava/lang/String; = null

.field public static gStagingImgHostUrl:Ljava/lang/String; = null

.field public static gUseAppFilter:Ljava/lang/String; = null

.field public static gWebWidgetClsf:Z = false

.field public static gWidgetTemplateClsf:Ljava/lang/String; = null

.field public static getcurrencyUnitHasPenny:Z = false

.field private static final mDefaultIMEI:Ljava/lang/String; = "123456789012"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    const-class v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->TAG:Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gInstance:Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    .line 78
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gOdcOpenApiVersion:Ljava/lang/String;

    .line 79
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gAndroidPlatformKeyVersion:Ljava/lang/String;

    .line 81
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSdkVersion:Ljava/lang/String;

    .line 82
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    .line 83
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 84
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 85
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    .line 86
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    .line 87
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gUseAppFilter:Ljava/lang/String;

    .line 88
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gLanguage:Ljava/lang/String;

    .line 89
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSamsungAppsVersionName:Ljava/lang/String;

    .line 90
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gImei:Ljava/lang/String;

    .line 91
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSerial:Ljava/lang/String;

    .line 92
    const-string v0, "03"

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWidgetTemplateClsf:Ljava/lang/String;

    .line 93
    sput-boolean v1, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    .line 94
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingDataHostUrl:Ljava/lang/String;

    .line 95
    const-string v0, ""

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingImgHostUrl:Ljava/lang/String;

    .line 97
    sput-boolean v1, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gFreeStoreClsf:Z

    .line 98
    sput-boolean v1, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWebWidgetClsf:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 12
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->TAG:Ljava/lang/String;

    const-string v7, "setDeviceInfo()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v6, "4.0"

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gOdcOpenApiVersion:Ljava/lang/String;

    .line 128
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gAndroidPlatformKeyVersion:Ljava/lang/String;

    .line 133
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    if-eqz v6, :cond_0

    const-string v6, ""

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v10, :cond_1

    .line 134
    :cond_0
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    .line 138
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x7

    if-le v6, v7, :cond_1

    .line 139
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    invoke-virtual {v6, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v7, "SAMSUNG-"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 140
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    .line 151
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getMccMncFromSamsungApps(Landroid/content/Context;)I

    move-result v3

    .line 153
    .local v3, "mcc":I
    if-eqz v3, :cond_2

    .line 154
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 160
    :cond_2
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 161
    const-string v6, "phone"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 164
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v5, :cond_3

    .line 165
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    .line 167
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    if-eqz v6, :cond_3

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v8, :cond_3

    .line 168
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_11

    .line 169
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 170
    const-string v6, "00"

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 200
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_3
    :goto_0
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    if-eqz v6, :cond_4

    const-string v6, ""

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v10, :cond_5

    .line 201
    :cond_4
    const-string v6, "ro.csc.sales_code"

    const-string v7, ""

    invoke-static {p1, v6, v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getSystemProperties(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    .line 204
    const-string v6, "pmystory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "csc = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_5
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSdkVersion:Ljava/lang/String;

    .line 216
    const-string v6, "1"

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gUseAppFilter:Ljava/lang/String;

    .line 221
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v2, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 222
    .local v2, "locale":Ljava/util/Locale;
    if-eqz v2, :cond_6

    .line 223
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gLanguage:Ljava/lang/String;

    .line 230
    :cond_6
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.samsungapps"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 232
    .local v4, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSamsungAppsVersionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 242
    .end local v4    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gImei:Ljava/lang/String;

    .line 247
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;-><init>()V

    .line 248
    .local v0, "config":Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->isExistSaconfig()Z

    move-result v6

    if-eqz v6, :cond_10

    .line 249
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->isUsingStageURL()Z

    move-result v6

    if-ne v6, v10, :cond_7

    .line 250
    sput-boolean v10, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    .line 251
    :cond_7
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 252
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingImgHostUrl:Ljava/lang/String;

    .line 253
    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getStageDataHostURL()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 254
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getStageDataHostURL()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingDataHostUrl:Ljava/lang/String;

    .line 255
    :cond_9
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getCSC()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 256
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getCSC()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSalesCode:Ljava/lang/String;

    .line 257
    :cond_a
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getIMEI()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 258
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getIMEI()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gImei:Ljava/lang/String;

    .line 259
    :cond_b
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getMCC()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 260
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getMCC()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 261
    :cond_c
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getMNC()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 262
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getMNC()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 263
    :cond_d
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 264
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    .line 265
    :cond_e
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getODCVersion()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_f

    .line 266
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getODCVersion()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gSamsungAppsVersionName:Ljava/lang/String;

    .line 267
    :cond_f
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 268
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gOdcOpenApiVersion:Ljava/lang/String;

    .line 280
    :cond_10
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->loadCountrySearchExCache(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->saveCountrySearchEx(Ljava/lang/String;)V

    .line 281
    return-void

    .line 172
    .end local v0    # "config":Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;
    .end local v2    # "locale":Ljava/util/Locale;
    .restart local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_11
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 173
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    goto/16 :goto_0

    .line 180
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_12
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 181
    const-string v6, "00"

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 183
    const-string v6, "phone"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 186
    .restart local v5    # "tm":Landroid/telephony/TelephonyManager;
    if-eqz v5, :cond_3

    .line 187
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    .line 189
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    if-eqz v6, :cond_3

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v8, :cond_3

    .line 191
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    goto/16 :goto_0

    .line 233
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    .restart local v2    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 235
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public static getDisclaimerSkipFromSamsungApps(Landroid/content/Context;)Z
    .locals 7
    .param p0, "_context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WorldReadableFiles"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 500
    const/4 v0, 0x0

    .line 504
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    const-string v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 507
    const-string v4, "SamsungAppsSharedPreferences"

    const/4 v5, 0x5

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 510
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v4, "1"

    const-string v5, "DisclaimerSkip"

    const-string v6, "0"

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    if-ne v3, v4, :cond_0

    .line 525
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return v3

    .line 514
    :catch_0
    move-exception v1

    .line 515
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 525
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 516
    :catch_1
    move-exception v1

    .line 517
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->TAG:Ljava/lang/String;

    const-string v5, "NullPointerException occurred!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    const v4, 0x7f0a0022

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 519
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 521
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v1

    .line 522
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private getIMEI(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 435
    const-string v1, ""

    .line 437
    .local v1, "imei":Ljava/lang/String;
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 439
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_0

    .line 440
    const-string v3, "123456789012"

    .line 466
    :goto_0
    return-object v3

    .line 444
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 456
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 463
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v1, :cond_3

    const-string v3, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 464
    :cond_2
    const-string v1, "123456789012"

    :cond_3
    move-object v3, v1

    .line 466
    goto :goto_0

    .line 446
    :pswitch_0
    :try_start_1
    sget-object v1, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 447
    goto :goto_1

    .line 449
    :pswitch_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 450
    if-eqz v1, :cond_1

    .line 453
    :pswitch_2
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 454
    goto :goto_1

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 444
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gInstance:Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gInstance:Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    .line 112
    :cond_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gInstance:Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    return-object v0
.end method

.method private getMccMncFromSamsungApps(Landroid/content/Context;)I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WorldReadableFiles"
        }
    .end annotation

    .prologue
    .line 471
    const/4 v1, 0x0

    .line 473
    .local v1, "mcc":I
    const/4 v2, 0x0

    .line 475
    .local v2, "samsungAppsContext":Landroid/content/Context;
    :try_start_0
    const-string v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 477
    const-string v4, "SamsungAppsSharedPreferences"

    const/4 v5, 0x5

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 480
    .local v3, "samsungAppsSharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "SelectedMcc"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 487
    .end local v3    # "samsungAppsSharedPreferences":Landroid/content/SharedPreferences;
    :goto_0
    return v1

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static reloadMccMnc(Landroid/content/Context;)V
    .locals 9
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    .line 287
    const/4 v1, 0x0

    .line 289
    .local v1, "mcc":I
    const/4 v2, 0x0

    .line 291
    .local v2, "samsungAppsContext":Landroid/content/Context;
    :try_start_0
    const-string v5, "com.sec.android.app.samsungapps"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 293
    const-string v5, "SamsungAppsSharedPreferences"

    const/4 v6, 0x5

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 296
    .local v3, "samsungAppsSharedPreferences":Landroid/content/SharedPreferences;
    const-string v5, "SelectedMcc"

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 303
    .end local v3    # "samsungAppsSharedPreferences":Landroid/content/SharedPreferences;
    :goto_0
    if-eqz v1, :cond_0

    .line 304
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 310
    :cond_0
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 311
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 314
    .local v4, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v4, :cond_1

    .line 315
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    .line 317
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v7, :cond_1

    .line 318
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v5, v7, :cond_2

    .line 319
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 320
    const-string v5, "00"

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 345
    .end local v4    # "tm":Landroid/telephony/TelephonyManager;
    :cond_1
    :goto_1
    return-void

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 322
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "tm":Landroid/telephony/TelephonyManager;
    :cond_2
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v5, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 323
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    goto :goto_1

    .line 330
    .end local v4    # "tm":Landroid/telephony/TelephonyManager;
    :cond_3
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 331
    const-string v5, "00"

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 333
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 336
    .restart local v4    # "tm":Landroid/telephony/TelephonyManager;
    if-eqz v4, :cond_1

    .line 337
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    .line 339
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_1

    .line 341
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMccMnc:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    goto :goto_1
.end method

.method public static saveCountrySearchEx(Ljava/lang/String;)V
    .locals 8
    .param p0, "_countrySearchXmlCache"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 355
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 356
    .local v2, "withoutMcc":Ljava/lang/Boolean;
    const-string v5, ""

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v7, v5, :cond_0

    const-string v5, ""

    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v7, v5, :cond_0

    .line 357
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 361
    :cond_0
    :try_start_0
    const-string v5, ""

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 363
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v5

    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    .line 364
    .local v1, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v4

    .line 365
    .local v4, "xr":Lorg/xml/sax/XMLReader;
    new-instance v3, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;

    invoke-direct {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;-><init>()V

    .line 367
    .local v3, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
    invoke-interface {v4, v3}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 368
    new-instance v5, Lorg/xml/sax/InputSource;

    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v4, v5}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 371
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getCountryURL()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 375
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getFreeStoreClsf()Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 377
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gFreeStoreClsf:Z

    .line 383
    :goto_0
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getWidgetTemplateClsf()Ljava/lang/String;

    move-result-object v5

    const-string v6, "01"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getWidgetTemplateClsf()Ljava/lang/String;

    move-result-object v5

    const-string v6, "02"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getWidgetTemplateClsf()Ljava/lang/String;

    move-result-object v5

    const-string v6, "03"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 386
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getWidgetTemplateClsf()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWidgetTemplateClsf:Ljava/lang/String;

    .line 392
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getWebWidgetClsf()Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 394
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWebWidgetClsf:Z

    .line 399
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 400
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getMcc()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    .line 401
    const-string v5, "00"

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    .line 404
    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getCurrencyUnitDivision()Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 406
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->currencyUnitDivision:Z

    .line 411
    :goto_3
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getCurrencyUnitPrecedes()Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 413
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->currencyUnitPrecedes:Z

    .line 418
    :goto_4
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->getCurrencyUnitHasPenny()Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 420
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getcurrencyUnitHasPenny:Z

    .line 430
    .end local v1    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
    .end local v4    # "xr":Lorg/xml/sax/XMLReader;
    :cond_3
    :goto_5
    return-void

    .line 379
    .restart local v1    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
    .restart local v4    # "xr":Lorg/xml/sax/XMLReader;
    :cond_4
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gFreeStoreClsf:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 425
    .end local v1    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
    .end local v4    # "xr":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 427
    sget-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to fetch country url from server"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 388
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
    .restart local v4    # "xr":Lorg/xml/sax/XMLReader;
    :cond_5
    :try_start_1
    const-string v5, "03"

    sput-object v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWidgetTemplateClsf:Ljava/lang/String;

    goto/16 :goto_1

    .line 396
    :cond_6
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gWebWidgetClsf:Z

    goto/16 :goto_2

    .line 408
    :cond_7
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->currencyUnitDivision:Z

    goto :goto_3

    .line 415
    :cond_8
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->currencyUnitPrecedes:Z

    goto :goto_4

    .line 422
    :cond_9
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getcurrencyUnitHasPenny:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method


# virtual methods
.method public loadCountrySearchExCache(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 348
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 351
    .local v0, "mPrefs":Landroid/content/SharedPreferences;
    const-string v1, "countrySearchEx"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
