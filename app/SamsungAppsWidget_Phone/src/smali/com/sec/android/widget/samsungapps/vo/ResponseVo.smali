.class public Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
.super Ljava/lang/Object;
.source "ResponseVo.java"


# instance fields
.field private mDeviceModel:Ljava/lang/String;

.field private mEndNum:Ljava/lang/String;

.field private mErrorCode:Ljava/lang/String;

.field private mErrorString:Ljava/lang/String;

.field private mID:Ljava/lang/String;

.field private mLang:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mNetworkType:Ljava/lang/String;

.field private mReturnCode:Ljava/lang/String;

.field private mStartNum:Ljava/lang/String;

.field private mTotalCount:Ljava/lang/String;

.field private mTransactionId:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mDeviceModel:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mNetworkType:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mLang:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mVersion:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTransactionId:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTotalCount:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mEndNum:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mStartNum:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mReturnCode:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mName:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorCode:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mDeviceModel:Ljava/lang/String;

    return-object v0
.end method

.method public getEndNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mEndNum:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorCode:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mLang:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mNetworkType:Ljava/lang/String;

    return-object v0
.end method

.method public getReturnCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mReturnCode:Ljava/lang/String;

    return-object v0
.end method

.method public getStartNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mStartNum:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTotalCount:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTransactionId:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "_deviceModel"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mDeviceModel:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setEndNum(Ljava/lang/String;)V
    .locals 0
    .param p1, "_endNum"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mEndNum:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setErrorCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "_errorCode"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorCode:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setErrorString(Ljava/lang/String;)V
    .locals 0
    .param p1, "mErrorString"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mErrorString:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "_id"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mID:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setLang(Ljava/lang/String;)V
    .locals 0
    .param p1, "_lang"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mLang:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "_name"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mName:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setNetworkType(Ljava/lang/String;)V
    .locals 0
    .param p1, "_networkType"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mNetworkType:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setReturnCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "_returnCode"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mReturnCode:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setStartNum(Ljava/lang/String;)V
    .locals 0
    .param p1, "_startNum"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mStartNum:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setTotalCount(Ljava/lang/String;)V
    .locals 0
    .param p1, "_totalCount"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTotalCount:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setTransactionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "_transactionId"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mTransactionId:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "_version"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->mVersion:Ljava/lang/String;

    .line 65
    return-void
.end method
