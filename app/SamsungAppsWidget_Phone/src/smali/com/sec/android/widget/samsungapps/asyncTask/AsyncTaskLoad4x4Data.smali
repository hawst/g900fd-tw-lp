.class public Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;
.super Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;
.source "AsyncTaskLoad4x4Data.java"


# instance fields
.field private mTempBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempFeaturedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempTopAllAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[IIZ)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceIds"    # [I
    .param p3, "_widgetSize"    # I
    .param p4, "_useCache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 28
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 39
    sget-object v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->TAG:Ljava/lang/String;

    const-string v2, "AsyncTaskLoad4x4Data()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v0

    .line 43
    .local v0, "dataManager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mContext:Landroid/content/Context;

    .line 44
    iput p3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mCurrentWidgetSize:I

    .line 45
    iput-boolean p4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseCache:Z

    .line 46
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mInstanceIds:[I

    .line 47
    iget v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mCurrentWidgetSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 48
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 26
    .param p1, "_params"    # [Ljava/lang/String;

    .prologue
    .line 55
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 63
    :try_start_0
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;

    move-result-object v17

    .line 64
    .local v17, "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->doCountrySearchEx(Landroid/content/Context;)V

    .line 66
    const-string v3, ""

    .line 68
    .local v3, "strAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseCache:Z

    if-nez v2, :cond_0

    .line 70
    sget-object v3, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 72
    const/4 v2, 0x1

    const-string v5, ""

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v2, v5, :cond_0

    .line 74
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "API2300"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 78
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 329
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v17    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :goto_0
    return-object v2

    .line 84
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v17    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_0
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForBannerList()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "requestBannerXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseCache:Z

    if-eqz v2, :cond_1

    .line 89
    new-instance v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 90
    .local v14, "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "promotionBannerListEx2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 98
    :goto_1
    if-eqz v14, :cond_4

    iget-object v2, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, ""

    iget-object v5, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 104
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v22

    .line 105
    .local v22, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v22 .. v22}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v25

    .line 106
    .local v25, "xr":Lorg/xml/sax/XMLReader;
    new-instance v24, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 108
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 109
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    iget-object v6, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 111
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 117
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2321"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 120
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 121
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 94
    .end local v14    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v25    # "xr":Lorg/xml/sax/XMLReader;
    :cond_1
    const/16 v5, 0x2710

    const/16 v6, 0x2710

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseProtocolCache:Z

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v14

    .restart local v14    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto :goto_1

    .line 122
    .restart local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v25    # "xr":Lorg/xml/sax/XMLReader;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 123
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2321"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 124
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWarningMessage:Ljava/lang/String;

    .line 135
    :cond_3
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForTopAllList()Ljava/lang/String;

    move-result-object v7

    .line 138
    .local v7, "requestTopAllXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseCache:Z

    if-eqz v2, :cond_5

    .line 139
    new-instance v23, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 140
    .local v23, "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "allProductList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 147
    :goto_2
    if-eqz v23, :cond_8

    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_8

    const-string v2, ""

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 151
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v22

    .line 152
    invoke-virtual/range {v22 .. v22}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v25

    .line 153
    new-instance v24, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-direct/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 155
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 156
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 158
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    if-nez v2, :cond_6

    .line 165
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2003"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 167
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 168
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 127
    .end local v7    # "requestTopAllXml":Ljava/lang/String;
    .end local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v23    # "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v25    # "xr":Lorg/xml/sax/XMLReader;
    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 128
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 143
    .restart local v7    # "requestTopAllXml":Ljava/lang/String;
    .restart local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v25    # "xr":Lorg/xml/sax/XMLReader;
    :cond_5
    const/16 v8, 0x2710

    const/16 v9, 0x2710

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseProtocolCache:Z

    move-object/from16 v5, p0

    move-object v6, v3

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v23

    .restart local v23    # "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_2

    .line 170
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_7

    .line 172
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2003"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 173
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWarningMessage:Ljava/lang/String;

    .line 184
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForFeaturedAppList()Ljava/lang/String;

    move-result-object v10

    .line 187
    .local v10, "requestFeaturedAppXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseCache:Z

    if-eqz v2, :cond_9

    .line 188
    new-instance v16, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 189
    .local v16, "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "featuredProductList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 195
    :goto_3
    if-eqz v16, :cond_d

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_d

    const-string v2, ""

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 199
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v22

    .line 200
    invoke-virtual/range {v22 .. v22}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v25

    .line 201
    new-instance v24, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    invoke-direct/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 203
    .restart local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 204
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 207
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    if-nez v2, :cond_a

    .line 214
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2235"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 216
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 217
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 176
    .end local v10    # "requestFeaturedAppXml":Ljava/lang/String;
    .end local v16    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 177
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 191
    .restart local v10    # "requestFeaturedAppXml":Ljava/lang/String;
    .local v24, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    :cond_9
    const/16 v11, 0x2710

    const/16 v12, 0x2710

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mUseProtocolCache:Z

    move-object/from16 v8, p0

    move-object v9, v3

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v16

    .restart local v16    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_3

    .line 219
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_b

    .line 221
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2235"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 222
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWarningMessage:Ljava/lang/String;

    .line 232
    :cond_b
    new-instance v20, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 233
    .local v20, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 235
    new-instance v21, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 236
    .local v21, "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v2, 0x2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 240
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_e

    .line 242
    const/4 v2, 0x4

    move/from16 v0, v18

    if-ge v0, v2, :cond_c

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 240
    :cond_c
    add-int/lit8 v18, v18, 0x1

    goto :goto_4

    .line 225
    .end local v18    # "i":I
    .end local v20    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v21    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 226
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 249
    .restart local v18    # "i":I
    .restart local v20    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v21    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_e
    const/16 v18, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_10

    .line 251
    const/4 v2, 0x4

    move/from16 v0, v18

    if-ge v0, v2, :cond_f

    .line 252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempTopAllAppList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 249
    :cond_f
    add-int/lit8 v18, v18, 0x1

    goto :goto_5

    .line 258
    :cond_10
    const/16 v18, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_12

    .line 260
    rem-int/lit8 v2, v18, 0x6

    const/4 v5, 0x4

    if-ge v2, v5, :cond_11

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 258
    :goto_7
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 268
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mTempBannerList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_7

    .line 318
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v4    # "requestBannerXml":Ljava/lang/String;
    .end local v7    # "requestTopAllXml":Ljava/lang/String;
    .end local v10    # "requestFeaturedAppXml":Ljava/lang/String;
    .end local v14    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v16    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v17    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .end local v18    # "i":I
    .end local v20    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v21    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v23    # "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v25    # "xr":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v15

    .line 319
    .local v15, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 320
    invoke-virtual {v15}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    .line 321
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 276
    .end local v15    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v4    # "requestBannerXml":Ljava/lang/String;
    .restart local v7    # "requestTopAllXml":Ljava/lang/String;
    .restart local v10    # "requestFeaturedAppXml":Ljava/lang/String;
    .restart local v14    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .restart local v16    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .restart local v17    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .restart local v18    # "i":I
    .restart local v20    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v21    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v23    # "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .restart local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .restart local v25    # "xr":Lorg/xml/sax/XMLReader;
    :cond_12
    :try_start_1
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v19, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 286
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->downloadImages(Landroid/content/Context;Ljava/util/ArrayList;Z)Z

    move-result v5

    if-ne v2, v5, :cond_13

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "promotionBannerListEx2Notc"

    iget-object v6, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 297
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "allProductList2Notc"

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "featuredProductList2Notc"

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 301
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 304
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 311
    :cond_13
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 313
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 314
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    goto/16 :goto_0

    .line 322
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v4    # "requestBannerXml":Ljava/lang/String;
    .end local v7    # "requestTopAllXml":Ljava/lang/String;
    .end local v10    # "requestFeaturedAppXml":Ljava/lang/String;
    .end local v14    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v16    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v17    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .end local v18    # "i":I
    .end local v19    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v20    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v21    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v22    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v23    # "topAllApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v24    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v25    # "xr":Lorg/xml/sax/XMLReader;
    :catch_1
    move-exception v15

    .line 323
    .local v15, "e":Lorg/xml/sax/SAXException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 324
    invoke-virtual {v15}, Lorg/xml/sax/SAXException;->printStackTrace()V

    .line 325
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 326
    .end local v15    # "e":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v15

    .line 327
    .local v15, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->mErrorMessage:Ljava/lang/String;

    .line 328
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    .line 329
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x4Data;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
