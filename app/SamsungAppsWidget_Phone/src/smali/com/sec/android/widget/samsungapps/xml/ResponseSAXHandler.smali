.class public Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ResponseSAXHandler.java"


# instance fields
.field private mNode:Ljava/lang/String;

.field private mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

.field private mValue:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 12
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mValue:Ljava/lang/StringBuffer;

    .line 14
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-void
.end method

.method private getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "_attr"    # Lorg/xml/sax/Attributes;
    .param p2, "_attrName"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 113
    const-string v1, ""

    .line 117
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 104
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 83
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 90
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "trimedValue":Ljava/lang/String;
    const-string v1, "errorString"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorString(Ljava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method

.method public getParsedErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-object v0
.end method

.method public getTotalCount()J
    .locals 4

    .prologue
    .line 25
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getTotalCount()Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "strTotalCount":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 30
    .local v2, "totalCount":J
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 37
    :goto_0
    return-wide v2

    .line 32
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    .line 49
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 62
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mNode:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mValue:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 65
    const-string v0, "response"

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "id"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setId(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "name"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setName(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "returnCode"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setReturnCode(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "startNum"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setStartNum(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "endNum"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setEndNum(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "transactionId"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTransactionId(Ljava/lang/String;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const-string v0, "errorString"

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->mResponseVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v1, "errorCode"

    invoke-direct {p0, p4, v1}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto :goto_0
.end method
