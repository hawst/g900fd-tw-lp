.class public Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "CategoryListSAXHandler.java"


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/CategoryVo;",
            ">;"
        }
    .end annotation
.end field

.field private mNode:Ljava/lang/String;

.field private mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

.field private mValue:Ljava/lang/StringBuffer;

.field private mValueType:Ljava/lang/String;

.field private mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValue:Ljava/lang/StringBuffer;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mList:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    .line 24
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-void
.end method

.method private getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "_attr"    # Lorg/xml/sax/Attributes;
    .param p2, "_attrName"    # Ljava/lang/String;

    .prologue
    .line 209
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 212
    const-string v1, ""

    .line 216
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 204
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 162
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "trimedValue":Ljava/lang/String;
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 173
    const-string v1, "contentCategoryID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 175
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->setContentCategoryID(Ljava/lang/String;)V

    .line 188
    :cond_0
    :goto_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    .line 192
    :cond_1
    const-string v1, "list"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_2
    return-void

    .line 177
    :cond_3
    const-string v1, "productCategoryID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 179
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->setProductCategoryID(Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_4
    const-string v1, "productCategoryName"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->setProductCategoryName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getParsedErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParsedList()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 57
    const/4 v4, 0x7

    new-array v3, v4, [Ljava/lang/String;

    .line 59
    .local v3, "mParsedCategories":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    .line 61
    .local v0, "cv":Lcom/sec/android/widget/samsungapps/vo/CategoryVo;
    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getContentCategoryID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "mCategoryId":Ljava/lang/String;
    const-string v4, "0000000331"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 64
    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 65
    :cond_1
    const-string v4, "0000000328"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 66
    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 67
    :cond_2
    const-string v4, "0000000332"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 68
    const/4 v4, 0x2

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 69
    :cond_3
    const-string v4, "0000003640"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 70
    const/4 v4, 0x3

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 71
    :cond_4
    const-string v4, "0000003523"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 72
    const/4 v4, 0x4

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 73
    :cond_5
    const-string v4, "0000001458"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 74
    const/4 v4, 0x5

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto :goto_0

    .line 75
    :cond_6
    const-string v4, "0000001456"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    const/4 v4, 0x6

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->getProductCategoryName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_0

    .line 79
    .end local v0    # "cv":Lcom/sec/android/widget/samsungapps/vo/CategoryVo;
    .end local v2    # "mCategoryId":Ljava/lang/String;
    :cond_7
    return-object v3
.end method

.method public getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-object v0
.end method

.method public getTotalCount()J
    .locals 4

    .prologue
    .line 35
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getTotalCount()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "strTotalCount":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 40
    .local v2, "totalCount":J
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 47
    :goto_0
    return-wide v2

    .line 42
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mList:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    .line 88
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    .line 89
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 101
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    .line 102
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValue:Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 104
    const-string v4, "list"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    new-instance v4, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-direct {v4}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    .line 113
    const-string v4, "numValue"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "strNumValue":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 118
    .local v2, "numValue":J
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 125
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CategoryVo;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->setNumValue(J)V

    .line 156
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_0
    :goto_1
    return-void

    .line 120
    .restart local v1    # "strNumValue":Ljava/lang/String;
    .restart local v2    # "numValue":J
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 128
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_1
    const-string v4, "value"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 132
    const-string v4, "name"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mValueType:Ljava/lang/String;

    goto :goto_1

    .line 135
    :cond_2
    const-string v4, "response"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 137
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "transactionId"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTransactionId(Ljava/lang/String;)V

    .line 138
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "totalCount"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTotalCount(Ljava/lang/String;)V

    .line 139
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "endNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setEndNum(Ljava/lang/String;)V

    .line 140
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "startNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setStartNum(Ljava/lang/String;)V

    .line 141
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "returnCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setReturnCode(Ljava/lang/String;)V

    .line 142
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "name"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setName(Ljava/lang/String;)V

    .line 143
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "id"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setId(Ljava/lang/String;)V

    goto :goto_1

    .line 145
    :cond_3
    const-string v4, "SamsungProtocol"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 147
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "deviceModel"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setDeviceModel(Ljava/lang/String;)V

    .line 148
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "networkType"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setNetworkType(Ljava/lang/String;)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "lang"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setLang(Ljava/lang/String;)V

    .line 150
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "version"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 152
    :cond_4
    const-string v4, "errorString"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 154
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "errorCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CategoryListSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
