.class public Lcom/sec/android/widget/samsungapps/netbase/CachingData;
.super Ljava/lang/Object;
.source "CachingData.java"


# instance fields
.field mData:Ljava/lang/String;

.field mUpdateTime:J


# direct methods
.method constructor <init>(Lcom/sec/android/widget/samsungapps/netbase/CachingData;)V
    .locals 2
    .param p1, "_cachingData"    # Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mData:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mData:Ljava/lang/String;

    .line 17
    iget-wide v0, p1, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mUpdateTime:J

    iput-wide v0, p0, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mUpdateTime:J

    .line 18
    return-void
.end method

.method constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "_data"    # Ljava/lang/String;
    .param p2, "_updateTime"    # J

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mData:Ljava/lang/String;

    .line 11
    iput-wide p2, p0, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mUpdateTime:J

    .line 12
    return-void
.end method
