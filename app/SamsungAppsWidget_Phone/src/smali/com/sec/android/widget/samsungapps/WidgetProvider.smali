.class public Lcom/sec/android/widget/samsungapps/WidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "WidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widget/samsungapps/WidgetProvider$2;
    }
.end annotation


# static fields
.field public static final ACTION_3G_AGREE:Ljava/lang/String;

.field public static final ACTION_ALARM_WIDGET_UPDATE:Ljava/lang/String;

.field public static final ACTION_BANNER_LIST_CHANGED:Ljava/lang/String;

.field public static final ACTION_CLICK_APP:Ljava/lang/String;

.field public static final ACTION_CLICK_BANNER:Ljava/lang/String;

.field public static final ACTION_CLICK_BANNER_TOGGLE:Ljava/lang/String;

.field public static final ACTION_CLICK_CATEGORY:Ljava/lang/String;

.field public static final ACTION_CLICK_CONTENT_GIFTS_APP:Ljava/lang/String;

.field public static final ACTION_CLICK_CONTENT_GIFTS_SEE_ALL:Ljava/lang/String;

.field public static final ACTION_CLICK_LOCAL_APP:Ljava/lang/String;

.field public static final ACTION_CLICK_LOCAL_BANNER:Ljava/lang/String;

.field public static final ACTION_CLICK_LOCAL_CONTENT_SEE_ALL:Ljava/lang/String;

.field public static final ACTION_CLICK_NEXT:Ljava/lang/String;

.field public static final ACTION_CLICK_PREV:Ljava/lang/String;

.field public static final ACTION_CLICK_RELOAD:Ljava/lang/String;

.field public static final ACTION_CLICK_SETTINGS:Ljava/lang/String;

.field public static final ACTION_GOTO_APPS:Ljava/lang/String;

.field public static final ACTION_RELOAD_FINISHED:Ljava/lang/String;

.field public static final ACTION_SEARCH:Ljava/lang/String;

.field public static final ACTION_SETTINGS_FINISHED:Ljava/lang/String;

.field public static final ACTION_TAP_TO_START:Ljava/lang/String;

.field public static final ACTION_TAP_TO_START_01:Ljava/lang/String;

.field public static final ACTION_TAP_TO_START_02:Ljava/lang/String;

.field public static final ACTION_TERMS_AGREE:Ljava/lang/String; = "com.sec.android.app.samsungapps.Disclaimer"

.field public static final PNAME:Ljava/lang/String;

.field public static final SERVICE_ACTION_LAYOUTVIEWS:I = 0x2

.field public static final SERVICE_ACTION_ONSTART:I = 0x0

.field public static final SERVICE_ACTION_RENDERVIEWS:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static final WRONG_ACTION:Ljava/lang/String;


# instance fields
.field private mIsLoadCacheFirst:Z

.field private mIsSecondPage:Z

.field private mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

.field mPrefEditor:Landroid/content/SharedPreferences$Editor;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

.field private mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

.field private mWidgetSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    .line 52
    const-class v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".ALARM_WIDGET_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_ALARM_WIDGET_UPDATE:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".3G_AGREE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_3G_AGREE:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".BANNER_LIST_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_BANNER_LIST_CHANGED:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_RELOAD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_RELOAD:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_BANNER_TOGGLE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER_TOGGLE:Ljava/lang/String;

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".RELOAD_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_RELOAD_FINISHED:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_SETTINGS:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".SETTINGS_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_SETTINGS_FINISHED:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".GOTO_APPS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_GOTO_APPS:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".TAP_TO_START"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".TAP_TO_START_01"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_01:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".TAP_TO_START_02"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_02:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_SEARCH:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_NEXT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_NEXT:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_PREV"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_PREV:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_APP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_APP:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_BANNER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_LOCAL_APP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_APP:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_LOCAL_BANNER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_BANNER:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_LOCAL_CONTENT_SEE_ALL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_CONTENT_SEE_ALL:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_CONTENT_GIFTS_APP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CONTENT_GIFTS_APP:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_CONTENT_GIFTS_SEE_ALL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CONTENT_GIFTS_SEE_ALL:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".CLICK_CATEGORY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CATEGORY:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".WRONG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->WRONG_ACTION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 92
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    .line 97
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    .line 102
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefs:Landroid/content/SharedPreferences;

    .line 103
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    .line 108
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsSecondPage:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widget/samsungapps/WidgetProvider;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/WidgetProvider;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    return v0
.end method

.method private doActionClickApp(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/AppVo;II)V
    .locals 10
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_appVo"    # Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .param p3, "_page"    # I
    .param p4, "_index"    # I

    .prologue
    .line 893
    const/4 v2, 0x0

    .line 895
    .local v2, "isInstalled":Z
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v7

    .line 897
    .local v7, "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PackageInfo;

    .line 898
    .local v6, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 899
    const/4 v2, 0x1

    .line 901
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 902
    .local v5, "manager":Landroid/content/pm/PackageManager;
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 904
    .local v3, "launch":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 906
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 910
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "launch":Landroid/content/Intent;
    .end local v5    # "manager":Landroid/content/pm/PackageManager;
    .end local v6    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :catch_0
    move-exception v0

    .line 911
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 915
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    if-nez v2, :cond_2

    .line 916
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "samsungapps://ProductDetail/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 917
    .local v4, "link":Ljava/lang/String;
    invoke-static {p1, v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 919
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "@B@"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "@ProductDetail/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 922
    .end local v4    # "link":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    .locals 5
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_bannerVo"    # Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .param p3, "_page"    # I
    .param p4, "_index"    # I

    .prologue
    const/4 v4, 0x1

    .line 934
    const-string v0, "\uc9c0\uc6d0\ud558\uc9c0 \uc54a\ub294 \ubc30\ub108 \ud0c0\uc785 \uc785\ub2c8\ub2e4."

    .line 937
    .local v0, "link":Ljava/lang/String;
    const-string v1, ""

    .line 941
    .local v1, "logLink":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v4, v2, :cond_0

    .line 943
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "samsungapps://ProductDetail/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmAppID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 944
    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 946
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@B@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@ProductDetail/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmAppID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 948
    invoke-direct {p0, p1, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 994
    :goto_0
    return-void

    .line 953
    :cond_0
    const-string v2, "1"

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v4, v2, :cond_1

    .line 956
    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 958
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@B@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@ProductSetList/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 960
    sget-object v2, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "banner title : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    invoke-direct {p0, p1, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 966
    :cond_1
    const-string v2, "2"

    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v4, v2, :cond_6

    .line 968
    invoke-virtual {p2}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerLinkURL()Ljava/lang/String;

    move-result-object v0

    .line 971
    const-string v2, "samsungapps://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "SAMSUNGAPPS://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 972
    :cond_2
    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 982
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@B@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "^http[s]?://"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 984
    invoke-direct {p0, p1, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 975
    :cond_3
    const-string v2, "deeplink://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "DEEPLINK://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 976
    :cond_4
    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goDeeplink(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 980
    :cond_5
    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goOutLink(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 991
    :cond_6
    sget-object v2, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private doActionClickCategory(Landroid/content/Context;I)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_id"    # I

    .prologue
    .line 1003
    const-string v1, ""

    .line 1004
    .local v1, "marketLink":Ljava/lang/String;
    const-string v0, ""

    .line 1006
    .local v0, "logLink":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 1062
    :goto_0
    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1064
    invoke-static {p1, v1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1065
    invoke-direct {p0, p1, v0}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 1067
    :cond_0
    return-void

    .line 1012
    :pswitch_0
    const-string v1, "samsungapps://CategoryList/0000000328"

    .line 1013
    const-string v0, "2@L@1@CategoryList/0000000328"

    .line 1014
    goto :goto_0

    .line 1021
    :pswitch_1
    const-string v1, "samsungapps://PaidProductList/"

    .line 1022
    const-string v0, "2@L@2@PaidProductList/"

    .line 1023
    goto :goto_0

    .line 1030
    :pswitch_2
    const-string v1, "samsungapps://FreeProductList/"

    .line 1031
    const-string v0, "2@L@3@FreeProductList/"

    .line 1032
    goto :goto_0

    .line 1039
    :pswitch_3
    const-string v1, "samsungapps://MainCategoryList/"

    .line 1040
    const-string v0, "2@L@4@MainCategoryList"

    .line 1041
    goto :goto_0

    .line 1047
    :pswitch_4
    const-string v1, "samsungapps://CategoryList/0000003553"

    .line 1048
    const-string v0, "2@L@1@CategoryList/0000003553"

    .line 1049
    goto :goto_0

    .line 1055
    :pswitch_5
    const-string v1, "samsungapps://CategoryList/0000000344"

    .line 1056
    const-string v0, "2@L@2@CategoryList/0000000344"

    goto :goto_0

    .line 1006
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private doActionClickContentGiftsSeeAll(Landroid/content/Context;)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 1121
    const-string v0, "samsungapps://CategoryList/0000003953"

    .line 1122
    .local v0, "link":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1124
    const-string v1, "2@L@1@CategoryList/0000003953"

    invoke-direct {p0, p1, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 1125
    return-void
.end method

.method private doActionClickLocalContentSeeAll(Landroid/content/Context;)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 1113
    const-string v0, "samsungapps://CategoryList/0000002474"

    .line 1114
    .local v0, "link":Ljava/lang/String;
    const-string v1, "Apps for Galaxy"

    invoke-static {p1, v0, v1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1116
    const-string v1, "2@L@1@CategoryList/0000002474"

    invoke-direct {p0, p1, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 1117
    return-void
.end method

.method private doActionClickNext(Landroid/content/Context;I)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1077
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1082
    .local v0, "pageRv":Landroid/widget/RemoteViews;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1084
    return-void
.end method

.method private doActionClickPrev(Landroid/content/Context;I)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1094
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1099
    .local v0, "pageRv":Landroid/widget/RemoteViews;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1101
    return-void
.end method

.method private doActionGointoPages(Landroid/content/Context;I)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1253
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionGointoPages(Landroid/content/Context;II)V

    .line 1254
    return-void
.end method

.method private doActionGointoPages(Landroid/content/Context;II)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "index"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1258
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v2, v4}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1260
    sget-object v2, Lcom/sec/android/widget/samsungapps/WidgetProvider$2;->$SwitchMap$com$sec$android$widget$samsungapps$utils$WidgetUtils$NetState:[I

    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1357
    :cond_0
    :goto_0
    return-void

    .line 1266
    :pswitch_0
    const v2, 0x7f0a0025

    const/4 v3, 0x0

    :try_start_0
    invoke-static {p1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1271
    :catch_0
    move-exception v1

    .line 1273
    .local v1, "ex":Landroid/view/InflateException;
    invoke-virtual {v1}, Landroid/view/InflateException;->printStackTrace()V

    goto :goto_0

    .line 1280
    .end local v1    # "ex":Landroid/view/InflateException;
    :pswitch_1
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreference3gPopupDontShow(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1283
    if-nez p3, :cond_1

    .line 1284
    invoke-direct {p0, p1, p2, v5, v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    goto :goto_0

    .line 1285
    :cond_1
    if-ne p3, v4, :cond_0

    .line 1286
    invoke-direct {p0, p1, p2, v4, v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    goto :goto_0

    .line 1292
    :cond_2
    if-nez p3, :cond_4

    .line 1293
    :try_start_1
    const-string v2, "APPSFORGALAXY"

    const-string v3, "GALAXY Essentials"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1301
    :cond_3
    :goto_1
    invoke-direct {p0, p1, p2, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLoadingLayout(Landroid/content/Context;IZ)V

    .line 1302
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1294
    :cond_4
    if-ne p3, v4, :cond_3

    .line 1295
    :try_start_2
    const-string v2, "CONTENTGIFTS"

    const-string v3, "GALAXY Gifts"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1297
    :catch_1
    move-exception v0

    .line 1298
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1311
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreferenceWLANPopupDontShow(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v3, "460"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1314
    if-nez p3, :cond_5

    .line 1315
    invoke-direct {p0, p1, p2, v5, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    goto :goto_0

    .line 1316
    :cond_5
    if-ne p3, v4, :cond_0

    .line 1317
    invoke-direct {p0, p1, p2, v4, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    goto :goto_0

    .line 1323
    :cond_6
    if-nez p3, :cond_8

    .line 1324
    :try_start_3
    const-string v2, "APPSFORGALAXY"

    const-string v3, "GALAXY Essentials"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 1332
    :cond_7
    :goto_2
    invoke-direct {p0, p1, p2, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLoadingLayout(Landroid/content/Context;IZ)V

    .line 1333
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1325
    :cond_8
    if-ne p3, v4, :cond_7

    .line 1326
    :try_start_4
    const-string v2, "CONTENTGIFTS"

    const-string v3, "GALAXY Gifts"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 1328
    :catch_2
    move-exception v0

    .line 1329
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1342
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_3
    if-nez p3, :cond_a

    .line 1343
    :try_start_5
    const-string v2, "APPSFORGALAXY"

    const-string v3, "GALAXY Essentials"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 1351
    :cond_9
    :goto_3
    invoke-direct {p0, p1, p2, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLoadingLayout(Landroid/content/Context;IZ)V

    .line 1352
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1344
    :cond_a
    if-ne p3, v4, :cond_9

    .line 1345
    :try_start_6
    const-string v2, "CONTENTGIFTS"

    const-string v3, "GALAXY Gifts"

    invoke-static {p1, v2, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    .line 1347
    :catch_3
    move-exception v0

    .line 1348
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 1260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private doActionGointoPagesBySADisclaimerAgreement(Landroid/content/Context;)V
    .locals 12
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x1

    .line 1361
    sget-object v8, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v9, "call doActionGointoPagesBySADisclaimerAgreement()"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v8, v11}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1364
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 1366
    .local v2, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v7, Landroid/content/ComponentName;

    iget v8, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v8}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v8

    invoke-direct {v7, p1, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1370
    .local v7, "provider":Landroid/content/ComponentName;
    invoke-virtual {v2, v7}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 1372
    .local v1, "appWidgetIds":[I
    move-object v3, v1

    .local v3, "arr$":[I
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget v0, v3, v5

    .line 1373
    .local v0, "_instanceId":I
    sget-object v8, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doActionGointoPagesBySADisclaimerAgreement - instance Id : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    sget-object v8, Lcom/sec/android/widget/samsungapps/WidgetProvider$2;->$SwitchMap$com$sec$android$widget$samsungapps$utils$WidgetUtils$NetState:[I

    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 1372
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1380
    :pswitch_0
    const v8, 0x7f0a0025

    const/4 v9, 0x0

    :try_start_0
    invoke-static {p1, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1385
    :catch_0
    move-exception v4

    .line 1387
    .local v4, "ex":Landroid/view/InflateException;
    invoke-virtual {v4}, Landroid/view/InflateException;->printStackTrace()V

    goto :goto_1

    .line 1413
    .end local v4    # "ex":Landroid/view/InflateException;
    :pswitch_1
    invoke-direct {p0, p1, v0, v11}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLoadingLayout(Landroid/content/Context;IZ)V

    .line 1414
    invoke-direct {p0, p1, v0, v11}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;IZ)V

    goto :goto_1

    .line 1420
    .end local v0    # "_instanceId":I
    :cond_0
    return-void

    .line 1374
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private doActionReloadPages(Landroid/content/Context;I)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1157
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionReloadPages(Landroid/content/Context;IZI)V

    .line 1158
    return-void
.end method

.method private doActionReloadPages(Landroid/content/Context;IZI)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "isSlientUpdate"    # Z
    .param p4, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 1164
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider$2;->$SwitchMap$com$sec$android$widget$samsungapps$utils$WidgetUtils$NetState:[I

    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1249
    :cond_0
    :goto_0
    return-void

    .line 1168
    :pswitch_0
    if-nez p3, :cond_0

    .line 1171
    const v1, 0x7f0a0025

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/view/InflateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1176
    :catch_0
    move-exception v0

    .line 1178
    .local v0, "ex":Landroid/view/InflateException;
    invoke-virtual {v0}, Landroid/view/InflateException;->printStackTrace()V

    goto :goto_0

    .line 1187
    .end local v0    # "ex":Landroid/view/InflateException;
    :pswitch_1
    if-nez p3, :cond_0

    .line 1191
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreference3gPopupDontShow(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1194
    invoke-direct {p0, p1, p2, p4, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    goto :goto_0

    .line 1202
    :cond_1
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    .line 1203
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1213
    :pswitch_2
    if-nez p3, :cond_3

    .line 1214
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreferenceWLANPopupDontShow(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v2, "460"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1217
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivityForWLAN(Landroid/content/Context;II)V

    goto :goto_0

    .line 1223
    :cond_2
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    .line 1224
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1231
    :cond_3
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    .line 1232
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1243
    :pswitch_3
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    .line 1244
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    goto :goto_0

    .line 1164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private doActionToggleBanner(Landroid/content/Context;I)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1129
    const/4 v2, 0x0

    .line 1130
    .local v2, "scrollPosition":I
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isSecondPage()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1131
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v3, v4}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setIsSecondPage(Z)V

    .line 1132
    iput-boolean v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsSecondPage:Z

    .line 1133
    const/16 v2, 0x3e8

    .line 1140
    :goto_0
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v4

    invoke-direct {v1, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1143
    .local v1, "remoteViews":Landroid/widget/RemoteViews;
    const v3, 0x7f0c000c

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setScrollPosition(II)V

    .line 1145
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 1147
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, p2, v1}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1148
    return-void

    .line 1135
    .end local v0    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v1    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v3, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setIsSecondPage(Z)V

    .line 1136
    iput-boolean v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsSecondPage:Z

    goto :goto_0
.end method

.method private drawIdleBackLayout(Landroid/content/Context;IZ)V
    .locals 8
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_useCacheData"    # Z

    .prologue
    const/high16 v6, 0x10000

    const/4 v7, 0x1

    .line 1940
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1942
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1948
    .local v3, "remoteViews":Landroid/widget/RemoteViews;
    if-ne v7, p3, :cond_1

    .line 1950
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v4, v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->addWidgetStateFalg(I)V

    .line 1961
    :goto_0
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setMainPageContent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V

    .line 1966
    iget-boolean v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    if-nez v4, :cond_0

    .line 1968
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadCacheFirst"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1969
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1972
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setAlarmManagerForUpdate(Landroid/content/Context;)V

    .line 1977
    :cond_0
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 1979
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1983
    .local v2, "provider":Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 1984
    .local v0, "appWidgetIds":[I
    invoke-virtual {v1, v0, v3}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 1988
    const v4, 0x7f0c000c

    invoke-virtual {v1, v0, v4}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 1992
    return-void

    .line 1955
    .end local v0    # "appWidgetIds":[I
    .end local v1    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v2    # "provider":Landroid/content/ComponentName;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v4, v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->deleteWidgetStateFalg(I)V

    goto :goto_0
.end method

.method private drawIdleLayout(Landroid/content/Context;I)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1882
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1884
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1889
    .local v3, "remoteViews":Landroid/widget/RemoteViews;
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setMainPageContent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V

    .line 1894
    iget-boolean v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    if-nez v4, :cond_0

    .line 1896
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadCacheFirst"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1897
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1900
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setAlarmManagerForUpdate(Landroid/content/Context;)V

    .line 1915
    :cond_0
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 1917
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1921
    .local v2, "provider":Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 1923
    .local v0, "appWidgetIds":[I
    invoke-virtual {v1, v0, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 1924
    const v4, 0x7f0c000c

    invoke-virtual {v1, v0, v4}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 1927
    return-void
.end method

.method private drawIdleLoadingLayout(Landroid/content/Context;IZ)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_useCacheData"    # Z

    .prologue
    const/high16 v5, 0x10000

    .line 2009
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 2014
    const/4 v3, 0x1

    if-ne v3, p3, :cond_0

    .line 2016
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v3, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->addWidgetStateFalg(I)V

    .line 2028
    :goto_0
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 2041
    .local v2, "remoteViews":Landroid/widget/RemoteViews;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 2043
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v1, Landroid/content/ComponentName;

    iget v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2047
    .local v1, "provider":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 2052
    return-void

    .line 2021
    .end local v0    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v1    # "provider":Landroid/content/ComponentName;
    .end local v2    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v3, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->deleteWidgetStateFalg(I)V

    goto :goto_0
.end method

.method private drawInitLayout(Landroid/content/Context;I)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1592
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetIntroLayout(I)I

    move-result v1

    invoke-direct {v4, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1628
    .local v4, "introRv":Landroid/widget/RemoteViews;
    sget-object v3, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_01:Ljava/lang/String;

    const v5, 0x7f0c0009

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;I)V

    .line 1631
    sget-object v3, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_02:Ljava/lang/String;

    const v5, 0x7f0c000d

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;I)V

    .line 1650
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v6

    .line 1652
    .local v6, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v6, p2, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1654
    return-void
.end method

.method private drawInitLoadingLayout(Landroid/content/Context;IZ)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_useCacheData"    # Z

    .prologue
    const/high16 v3, 0x10000

    .line 1673
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 1678
    const/4 v1, 0x1

    if-ne v1, p3, :cond_0

    .line 1680
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v1, v3}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->addWidgetStateFalg(I)V

    .line 1692
    :goto_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetIntroLayout(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1701
    .local v0, "rv":Landroid/widget/RemoteViews;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1704
    return-void

    .line 1685
    .end local v0    # "rv":Landroid/widget/RemoteViews;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v1, v3}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->deleteWidgetStateFalg(I)V

    goto :goto_0
.end method

.method private loadWidgetData(Landroid/content/Context;I)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    const/4 v1, 0x0

    .line 2106
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput p2, v0, v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->safeLoadWidgetData(Landroid/content/Context;[IZ)V

    .line 2108
    return-void
.end method

.method private loadWidgetData(Landroid/content/Context;IZ)V
    .locals 5
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "isReceivedAgreementFromSA"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2121
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isHaveSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2124
    if-nez p3, :cond_0

    .line 2127
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getDisclaimerSkipFromSamsungApps(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2131
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.samsungapps"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2136
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2138
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2163
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 2140
    :catch_0
    move-exception v0

    .line 2141
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v3, "NullPointerException occurred!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2142
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2144
    const v2, 0x7f0a0022

    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2145
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V

    goto :goto_0

    .line 2148
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2149
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 2161
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    new-array v2, v4, [I

    aput p2, v2, v3

    invoke-direct {p0, p1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->safeLoadWidgetData(Landroid/content/Context;[IZ)V

    goto :goto_0
.end method

.method private openSettings(Landroid/content/Context;I)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 872
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v2, "startAlertActivity()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 875
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 876
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 877
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 878
    const-string v1, "WidgetSize"

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 879
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 881
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 882
    return-void
.end method

.method private safeLoadWidgetData(Landroid/content/Context;[IZ)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceIds"    # [I
    .param p3, "_loadLocalData"    # Z

    .prologue
    .line 2172
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v2, "safeLoadWidgetData()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2174
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2177
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->cancel(Z)Z

    .line 2180
    :cond_0
    iget v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    packed-switch v1, :pswitch_data_0

    .line 2195
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2205
    :goto_1
    return-void

    .line 2183
    :pswitch_0
    new-instance v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;-><init>(Landroid/content/Context;[IIZ)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    goto :goto_0

    .line 2188
    :pswitch_1
    new-instance v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x2Data;-><init>(Landroid/content/Context;[IIZ)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mLoadWidgetDataTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    goto :goto_0

    .line 2197
    :catch_0
    move-exception v0

    .line 2199
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "safeLoadData\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/concurrent/RejectedExecutionException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2201
    .end local v0    # "e":Ljava/util/concurrent/RejectedExecutionException;
    :catch_1
    move-exception v0

    .line 2203
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2180
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logList"    # Ljava/lang/String;

    .prologue
    .line 2218
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v2, "sendWidgetClickLog()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2223
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->cancel(Z)Z

    .line 2226
    :cond_0
    new-instance v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gImei:Ljava/lang/String;

    invoke-direct {v1, v2, p2, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    .line 2231
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetClickLogTask:Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskWidgetClickLog;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2242
    :goto_0
    return-void

    .line 2233
    :catch_0
    move-exception v0

    .line 2235
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/RejectedExecutionException;->printStackTrace()V

    goto :goto_0

    .line 2237
    .end local v0    # "e":Ljava/util/concurrent/RejectedExecutionException;
    :catch_1
    move-exception v0

    .line 2239
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setMainPageContent(Landroid/content/Context;ILandroid/widget/RemoteViews;)V
    .locals 14
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    .line 1807
    new-instance v8, Landroid/content/Intent;

    const-class v11, Lcom/sec/android/widget/samsungapps/WidgetBannerService;

    invoke-direct {v8, p1, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1808
    .local v8, "intent":Landroid/content/Intent;
    const-string v11, "appWidgetId"

    move/from16 v0, p2

    invoke-virtual {v8, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1809
    const-string v11, "WIDGET_SIZE"

    iget v12, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1810
    const-string v11, "isSecondPage"

    iget-boolean v12, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsSecondPage:Z

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1811
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1812
    const v11, 0x7f0c000c

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v8}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 1822
    new-instance v3, Landroid/content/Intent;

    iget v11, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v11}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v11

    invoke-direct {v3, p1, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1826
    .local v3, "clickIntent":Landroid/content/Intent;
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1827
    const-string v11, "appWidgetId"

    move/from16 v0, p2

    invoke-virtual {v3, v11, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1830
    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p1, v11, v3, v12}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 1836
    .local v9, "pIntent":Landroid/app/PendingIntent;
    const v11, 0x7f0c000c

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v9}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 1839
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    if-eqz v11, :cond_1

    .line 1842
    const/4 v11, 0x2

    :try_start_0
    new-array v4, v11, [I

    fill-array-data v4, :array_0

    .line 1843
    .local v4, "descriptionViewIds":[I
    const/4 v11, 0x2

    new-array v7, v11, [I

    fill-array-data v7, :array_1

    .line 1845
    .local v7, "imageViewIds":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v6, v11, :cond_1

    .line 1846
    aget v12, v4, v6

    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerDescription()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1848
    const/4 v1, 0x0

    .line 1849
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v2, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1851
    .local v2, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmWidgetSymbolImgURL()Ljava/lang/String;

    move-result-object v10

    .line 1852
    .local v10, "strUrl":Ljava/lang/String;
    invoke-virtual {v2, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1854
    if-eqz v1, :cond_0

    .line 1855
    aget v11, v7, v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1845
    .end local v10    # "strUrl":Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1857
    :catch_0
    move-exception v5

    .line 1858
    .local v5, "e":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v5}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1861
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v4    # "descriptionViewIds":[I
    .end local v5    # "e":Ljava/lang/NullPointerException;
    .end local v6    # "i":I
    .end local v7    # "imageViewIds":[I
    :catch_1
    move-exception v5

    .line 1862
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 1865
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_1
    return-void

    .line 1842
    :array_0
    .array-data 4
        0x7f0c000b
        0x7f0c000f
    .end array-data

    .line 1843
    :array_1
    .array-data 4
        0x7f0c000a
        0x7f0c000e
    .end array-data
.end method

.method private setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;I)V
    .locals 8
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_action"    # Ljava/lang/String;
    .param p4, "_rViews"    # Landroid/widget/RemoteViews;
    .param p5, "_rId"    # I

    .prologue
    const/4 v6, -0x1

    .line 2266
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;III)V

    .line 2273
    return-void
.end method

.method private setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;III)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_action"    # Ljava/lang/String;
    .param p4, "_rViews"    # Landroid/widget/RemoteViews;
    .param p5, "_rId"    # I
    .param p6, "_page"    # I
    .param p7, "_index"    # I

    .prologue
    const/high16 v3, 0x8000000

    .line 2294
    new-instance v0, Landroid/content/Intent;

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2297
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2298
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2299
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2301
    if-ltz p6, :cond_0

    .line 2303
    const-string v2, "page"

    invoke-virtual {v0, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2306
    :cond_0
    const/4 v1, 0x0

    .line 2308
    .local v1, "pIntent":Landroid/app/PendingIntent;
    const/4 v2, -0x1

    if-ne p7, v2, :cond_1

    .line 2312
    invoke-static {p1, p2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2330
    :goto_0
    invoke-virtual {p4, p5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 2331
    return-void

    .line 2321
    :cond_1
    const-string v2, "index"

    invoke-virtual {v0, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2323
    invoke-static {p1, p7, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_0
.end method

.method private startAlertActivity(Landroid/content/Context;I)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I

    .prologue
    .line 1477
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    .line 1478
    return-void
.end method

.method private startAlertActivity(Landroid/content/Context;III)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "clickIndex"    # I
    .param p4, "popupType"    # I

    .prologue
    .line 1487
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1488
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1489
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1490
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1491
    const-string v1, "WidgetSize"

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1492
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1493
    const-string v1, "ClickIndex"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1494
    const-string v1, "PopupType"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1496
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1497
    return-void
.end method

.method private startAlertActivityForWLAN(Landroid/content/Context;II)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "index"    # I

    .prologue
    .line 1482
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startAlertActivity(Landroid/content/Context;III)V

    .line 1483
    return-void
.end method

.method private startSamsungApps(Landroid/content/Context;)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 856
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 857
    .local v1, "manager":Landroid/content/pm/PackageManager;
    const-string v2, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 860
    .local v0, "launch":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 862
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 864
    :cond_0
    return-void
.end method

.method private startSearch(Landroid/content/Context;)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 868
    const-string v0, "samsungapps://SearchResult"

    invoke-static {p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    .line 869
    return-void
.end method


# virtual methods
.method public goUrl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "_url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1428
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1429
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 1431
    .local v0, "scheme":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 1469
    :cond_0
    :goto_0
    return-void

    .line 1438
    :cond_1
    const-string v2, "samsungapps"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "musichub"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "videohub"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "readershub"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "learninghub"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "gamehub"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v3, v2, :cond_2

    const-string v2, "market"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v3, v2, :cond_3

    .line 1455
    :cond_2
    invoke-static {p1, p2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 1462
    :cond_3
    const-string v2, "appname"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v3, v2, :cond_0

    .line 1464
    invoke-static {p1, p2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 5
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 1513
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v1, "onEnabled()#####################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1514
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v1, "## Samsung Apps widget"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1517
    :try_start_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Package : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1521
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ver. : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1525
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mcc : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1528
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mnc : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMnc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1531
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Model : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gModel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1536
    :goto_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v1, "onEnabled()#####################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1538
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 1539
    return-void

    .line 1535
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 33
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_intent"    # Landroid/content/Intent;
    .param p3, "_widgetSize"    # I

    .prologue
    .line 117
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v28

    .line 119
    .local v28, "widgetDataManager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    .line 120
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    move/from16 v30, v0

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 125
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;

    .line 130
    const-string v5, ""

    .line 131
    .local v5, "action":Ljava/lang/String;
    const/16 v18, 0x0

    .line 132
    .local v18, "instanceId":I
    const/16 v24, -0x1

    .line 133
    .local v24, "page":I
    const/16 v17, -0x1

    .line 135
    .local v17, "index":I
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 143
    if-eqz v5, :cond_0

    .line 144
    const-string v30, "appWidgetId"

    const/16 v31, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 148
    const-string v30, "page"

    const/16 v31, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    .line 149
    const-string v30, "index"

    const/16 v31, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v30, v0

    if-nez v30, :cond_2

    .line 161
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefs:Landroid/content/SharedPreferences;

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v30, v0

    invoke-interface/range {v30 .. v30}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 170
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v30, v0

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "loadCacheFirst"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x0

    invoke-interface/range {v30 .. v32}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v30

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    .line 171
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreference3gPopupDontShow(Landroid/content/Context;)Z

    move-result v12

    .line 176
    .local v12, "dontShow3GAlert":Z
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v31, "#S##########################################"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "WidgetSize = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "Action = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "State = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetStateString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "PageList Size = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "loadCacheFirst = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "dontshow3galert = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "instanceId = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v31, "#E###########################################"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x10

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v30

    if-eqz v30, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    move/from16 v30, v0

    if-nez v30, :cond_3

    const-string v30, "android.appwidget.action.APPWIDGET_ENABLED"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    const-string v30, "android.appwidget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_01:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_02:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    const-string v30, "com.sec.android.app.samsungapps.Disclaimer"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_ALARM_WIDGET_UPDATE:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    const-string v30, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_GOTO_APPS:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_3

    .line 208
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V

    .line 212
    sget-object v5, Lcom/sec/android/widget/samsungapps/WidgetProvider;->WRONG_ACTION:Ljava/lang/String;

    .line 214
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0a002a

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "("

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0a0029

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ")"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 223
    .local v23, "msg":Ljava/lang/String;
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    .end local v23    # "msg":Ljava/lang/String;
    :cond_3
    const-string v30, "android.appwidget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x10

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 233
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->getDisclaimerSkipFromSamsungApps(Landroid/content/Context;)Z

    move-result v30

    if-eqz v30, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    move/from16 v30, v0

    if-eqz v30, :cond_7

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x4

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 251
    :cond_4
    :goto_1
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_3G_AGREE:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_e

    .line 253
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "ACTION_3G_AGREE index : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 257
    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLoadingLayout(Landroid/content/Context;IZ)V

    .line 259
    if-nez v17, :cond_8

    .line 260
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 292
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->loadWidgetData(Landroid/content/Context;I)V

    .line 753
    :cond_6
    :goto_3
    invoke-super/range {p0 .. p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 150
    .end local v12    # "dontShow3GAlert":Z
    :catch_0
    move-exception v16

    .line 151
    .local v16, "ignored":Ljava/lang/Exception;
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v31, "intent must return int. But, invalid character returned."

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 240
    .end local v16    # "ignored":Ljava/lang/Exception;
    .restart local v12    # "dontShow3GAlert":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    goto :goto_1

    .line 261
    :cond_8
    const/16 v30, 0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-ne v0, v1, :cond_5

    .line 262
    const-string v30, "CONTENTGIFTS"

    const-string v31, "GALAXY Gifts"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_2

    .line 265
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x4

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 267
    if-ltz v17, :cond_a

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 269
    .local v25, "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 270
    .local v10, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V

    .line 272
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_a
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    goto/16 :goto_2

    .line 274
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x10

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 277
    if-ltz v17, :cond_c

    .line 278
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 279
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 280
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 289
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_c
    :goto_4
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLoadingLayout(Landroid/content/Context;IZ)V

    goto/16 :goto_2

    .line 282
    :catch_1
    move-exception v13

    .line 283
    .local v13, "e":Ljava/lang/Exception;
    if-nez v17, :cond_d

    .line 284
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_4

    .line 285
    :cond_d
    const/16 v30, 0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-ne v0, v1, :cond_c

    .line 286
    const-string v30, "CONTENTGIFTS"

    const-string v31, "GALAXY Gifts"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_4

    .line 299
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_e
    const/16 v30, 0x1

    const-string v31, "com.sec.android.app.samsungapps.Disclaimer"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_6

    .line 319
    const/16 v30, 0x1

    const-string v31, "android.appwidget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_6

    const/16 v30, 0x1

    const-string v31, "android.appwidget.action.APPWIDGET_ENABLED"

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_6

    .line 328
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_GOTO_APPS:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_f

    .line 331
    const-string v30, ""

    const-string v31, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 337
    :cond_f
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_APP:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    .line 344
    const/16 v30, -0x1

    move/from16 v0, v24

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 347
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageAppList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 349
    .local v6, "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickApp(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/AppVo;II)V

    goto/16 :goto_3

    .line 356
    .end local v6    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_10
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1a

    .line 358
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Click Banner >>>>>\ninstanceId = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "page = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "index = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetState()I

    move-result v30

    const/16 v31, 0x10

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_14

    .line 364
    if-ltz v17, :cond_11

    const/16 v30, 0x4

    move/from16 v0, v17

    move/from16 v1, v30

    if-lt v0, v1, :cond_12

    .line 365
    :cond_11
    const/16 v17, 0x0

    .line 367
    :cond_12
    const/16 v24, 0x0

    .line 368
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    move/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionReloadPages(Landroid/content/Context;IZI)V

    .line 370
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider$2;->$SwitchMap$com$sec$android$widget$samsungapps$utils$WidgetUtils$NetState:[I

    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->ordinal()I

    move-result v31

    aget v30, v30, v31

    packed-switch v30, :pswitch_data_0

    goto/16 :goto_3

    .line 374
    :pswitch_0
    const/16 v30, 0x1

    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreference3gPopupDontShow(Landroid/content/Context;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_6

    .line 377
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 378
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 379
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_3

    .line 380
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_2
    move-exception v13

    .line 381
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 382
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 391
    .end local v13    # "e":Ljava/lang/Exception;
    :pswitch_1
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreferenceWLANPopupDontShow(Landroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_13

    sget-object v30, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v31, "460"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_6

    .line 394
    :cond_13
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 395
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 396
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_3

    .line 397
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_3
    move-exception v13

    .line 398
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 399
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 409
    .end local v13    # "e":Ljava/lang/Exception;
    :pswitch_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 410
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 411
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_3

    .line 412
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_4
    move-exception v13

    .line 413
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 414
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 420
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_14
    const/16 v30, -0x1

    move/from16 v0, v24

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetState()I

    move-result v30

    const/16 v31, 0x4

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_15

    .line 424
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    move/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionReloadPages(Landroid/content/Context;IZI)V

    .line 427
    :cond_15
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider$2;->$SwitchMap$com$sec$android$widget$samsungapps$utils$WidgetUtils$NetState:[I

    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->ordinal()I

    move-result v31

    aget v30, v30, v31

    packed-switch v30, :pswitch_data_1

    goto/16 :goto_3

    .line 432
    :pswitch_3
    const/16 v30, 0x1

    :try_start_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreference3gPopupDontShow(Landroid/content/Context;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_6

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 435
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 437
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_3

    .line 439
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_5
    move-exception v13

    .line 440
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 441
    const/16 v30, 0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-ne v0, v1, :cond_16

    .line 442
    const-string v30, "CONTENTGIFTS"

    const-string v31, "GALAXY Gifts"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 444
    :cond_16
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 454
    .end local v13    # "e":Ljava/lang/Exception;
    :pswitch_4
    :try_start_6
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getPreferenceWLANPopupDontShow(Landroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_17

    sget-object v30, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v31, "460"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_6

    .line 456
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 457
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 459
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_3

    .line 461
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_6
    move-exception v13

    .line 462
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 463
    const/16 v30, 0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-ne v0, v1, :cond_18

    .line 464
    const-string v30, "CONTENTGIFTS"

    const-string v31, "GALAXY Gifts"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 466
    :cond_18
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 476
    .end local v13    # "e":Ljava/lang/Exception;
    :pswitch_5
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 477
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 479
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_3

    .line 480
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_7
    move-exception v13

    .line 481
    .restart local v13    # "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 482
    const/16 v30, 0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-ne v0, v1, :cond_19

    .line 483
    const-string v30, "CONTENTGIFTS"

    const-string v31, "GALAXY Gifts"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 485
    :cond_19
    const-string v30, "APPSFORGALAXY"

    const-string v31, "GALAXY Essentials"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 495
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_1a
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_APP:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1b

    .line 502
    const/16 v30, -0x1

    move/from16 v0, v24

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 506
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageAppList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 508
    .restart local v6    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickApp(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/AppVo;II)V

    goto/16 :goto_3

    .line 514
    .end local v6    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_1b
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CONTENT_GIFTS_APP:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1c

    .line 521
    const/16 v30, -0x1

    move/from16 v0, v24

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 524
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageContentGiftsAppList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 526
    .restart local v6    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickApp(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/AppVo;II)V

    goto/16 :goto_3

    .line 533
    .end local v6    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_1c
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_BANNER:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1d

    .line 540
    const/16 v30, -0x1

    move/from16 v0, v24

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 543
    .restart local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageLocalBannerList()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 545
    .restart local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v17

    invoke-direct {v0, v1, v10, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickBanner(Landroid/content/Context;Lcom/sec/android/widget/samsungapps/vo/BannerVo;II)V

    goto/16 :goto_3

    .line 552
    .end local v10    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v25    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_1d
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_LOCAL_CONTENT_SEE_ALL:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1e

    .line 559
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickLocalContentSeeAll(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 564
    :cond_1e
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CONTENT_GIFTS_SEE_ALL:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1f

    .line 571
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickContentGiftsSeeAll(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 577
    :cond_1f
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_CATEGORY:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_20

    .line 584
    const/16 v30, -0x1

    move/from16 v0, v17

    move/from16 v1, v30

    if-le v0, v1, :cond_6

    .line 586
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickCategory(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 593
    :cond_20
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_NEXT:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_21

    .line 595
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickNext(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 599
    :cond_21
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_PREV:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_22

    .line 601
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionClickPrev(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 607
    :cond_22
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_RELOAD:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_23

    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_SETTINGS_FINISHED:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_24

    .line 609
    :cond_23
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionReloadPages(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 615
    :cond_24
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER_TOGGLE:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_25

    .line 616
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionToggleBanner(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 622
    :cond_25
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_26

    .line 624
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionGointoPages(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 629
    :cond_26
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_01:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_27

    .line 631
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionGointoPages(Landroid/content/Context;II)V

    goto/16 :goto_3

    .line 636
    :cond_27
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_TAP_TO_START_02:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_28

    .line 638
    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionGointoPages(Landroid/content/Context;II)V

    goto/16 :goto_3

    .line 644
    :cond_28
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_RELOAD_FINISHED:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2f

    .line 646
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 647
    .local v11, "bundle":Landroid/os/Bundle;
    const-string v30, "isReloaded"

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    .line 648
    .local v20, "isWidgetDataLoaded":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetState()I

    move-result v29

    .line 650
    .local v29, "widgetState":I
    const-string v30, "appWidgetIds"

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v19

    .line 653
    .local v19, "instanceIds":[I
    sget-object v30, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Data Reload Success : "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    move-object/from16 v9, v19

    .local v9, "arr$":[I
    array-length v0, v9

    move/from16 v21, v0

    .local v21, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_5
    move/from16 v0, v21

    if-ge v14, v0, :cond_6

    aget v15, v9, v14

    .line 656
    .local v15, "id":I
    const/16 v30, 0x1

    move/from16 v0, v30

    move/from16 v1, v20

    if-ne v0, v1, :cond_2c

    .line 658
    and-int/lit8 v30, v29, 0x8

    if-eqz v30, :cond_2a

    .line 662
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLayout(Landroid/content/Context;I)V

    .line 655
    :cond_29
    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 664
    :cond_2a
    and-int/lit8 v30, v29, 0x2

    if-eqz v30, :cond_2b

    .line 666
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLayout(Landroid/content/Context;I)V

    goto :goto_6

    .line 668
    :cond_2b
    and-int/lit8 v30, v29, 0x10

    if-eqz v30, :cond_29

    .line 670
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLayout(Landroid/content/Context;I)V

    goto :goto_6

    .line 675
    :cond_2c
    and-int/lit8 v30, v29, 0x8

    if-eqz v30, :cond_2d

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v30, v0

    const/16 v31, 0x4

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setWidgetState(I)V

    .line 682
    :cond_2d
    and-int/lit8 v30, v29, 0x2

    if-eqz v30, :cond_2e

    .line 685
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V

    goto :goto_6

    .line 687
    :cond_2e
    and-int/lit8 v30, v29, 0x1

    if-eqz v30, :cond_29

    .line 689
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V

    goto :goto_6

    .line 698
    .end local v9    # "arr$":[I
    .end local v11    # "bundle":Landroid/os/Bundle;
    .end local v14    # "i$":I
    .end local v15    # "id":I
    .end local v19    # "instanceIds":[I
    .end local v20    # "isWidgetDataLoaded":Z
    .end local v21    # "len$":I
    .end local v29    # "widgetState":I
    :cond_2f
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_SEARCH:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_30

    .line 700
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->startSearch(Landroid/content/Context;)V

    .line 701
    const-string v30, "1@S@1@SearchResult"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->sendWidgetClickLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 708
    :cond_30
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_SETTINGS:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_31

    .line 710
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->openSettings(Landroid/content/Context;I)V

    goto/16 :goto_3

    .line 714
    :cond_31
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_ALARM_WIDGET_UPDATE:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_32

    .line 721
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v8

    .line 723
    .local v8, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v26, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v30

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 727
    .local v26, "provider":Landroid/content/ComponentName;
    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v7

    .line 729
    .local v7, "appWidgetIds":[I
    if-eqz v7, :cond_6

    array-length v0, v7

    move/from16 v30, v0

    if-lez v30, :cond_6

    .line 730
    const/16 v30, 0x0

    aget v30, v7, v30

    const/16 v31, 0x1

    const/16 v32, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->doActionReloadPages(Landroid/content/Context;IZI)V

    goto/16 :goto_3

    .line 735
    .end local v7    # "appWidgetIds":[I
    .end local v8    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v26    # "provider":Landroid/content/ComponentName;
    :cond_32
    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_BANNER_LIST_CHANGED:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_6

    .line 736
    new-instance v27, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 739
    .local v27, "remoteViews":Landroid/widget/RemoteViews;
    const v30, 0x7f0c0008

    const/16 v31, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 743
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v22

    .line 745
    .local v22, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v26, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v30

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 749
    .restart local v26    # "provider":Landroid/content/ComponentName;
    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v7

    .line 750
    .restart local v7    # "appWidgetIds":[I
    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v7, v1}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_3

    .line 370
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 427
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "_appWidgetIds"    # [I

    .prologue
    .line 1549
    sget-object v4, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onUpdate() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " widgets"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1553
    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->mIsLoadCacheFirst:Z

    if-ne v4, v5, :cond_2

    .line 1555
    move-object v0, p3

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget v2, v0, v1

    .line 1557
    .local v2, "id":I
    invoke-direct {p0, p1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawIdleLayout(Landroid/content/Context;I)V

    .line 1555
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1561
    .end local v2    # "id":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->setAlarmManagerForUpdate(Landroid/content/Context;)V

    .line 1574
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 1575
    return-void

    .line 1568
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v3    # "len$":I
    :cond_2
    move-object v0, p3

    .restart local v0    # "arr$":[I
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_1
    if-ge v1, v3, :cond_1

    aget v2, v0, v1

    .line 1570
    .restart local v2    # "id":I
    invoke-direct {p0, p1, v2}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->drawInitLayout(Landroid/content/Context;I)V

    .line 1568
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public setAlarmManagerForBannerUpdate(Landroid/content/Context;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 817
    const/4 v1, 0x0

    .line 818
    .local v1, "timer":Ljava/util/Timer;
    const/4 v2, 0x0

    .line 819
    .local v2, "timerTask":Ljava/util/TimerTask;
    move-object v0, p1

    .line 821
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;

    .end local v2    # "timerTask":Ljava/util/TimerTask;
    invoke-direct {v2, p0, v0}, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;-><init>(Lcom/sec/android/widget/samsungapps/WidgetProvider;Landroid/content/Context;)V

    .line 845
    .restart local v2    # "timerTask":Ljava/util/TimerTask;
    new-instance v1, Ljava/util/Timer;

    .end local v1    # "timer":Ljava/util/Timer;
    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 846
    .restart local v1    # "timer":Ljava/util/Timer;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 847
    return-void
.end method

.method public setAlarmManagerForUpdate(Landroid/content/Context;)V
    .locals 20
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 759
    sget-object v3, Lcom/sec/android/widget/samsungapps/WidgetProvider;->TAG:Ljava/lang/String;

    const-string v4, "Set Alarm Manager for SA Widget Update"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    new-instance v9, Landroid/content/Intent;

    sget-object v3, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_ALARM_WIDGET_UPDATE:Ljava/lang/String;

    invoke-direct {v9, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 761
    .local v9, "alarmIntent":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    move-object/from16 v0, p1

    invoke-static {v0, v3, v9, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 763
    .local v8, "pIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 765
    .local v2, "am":Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 767
    .local v16, "networkTS":J
    :try_start_0
    const-string v3, "location"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/location/LocationManager;

    .line 768
    .local v15, "locMan":Landroid/location/LocationManager;
    const-string v3, "network"

    invoke-virtual {v15, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    .line 772
    .end local v15    # "locMan":Landroid/location/LocationManager;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v18, v16, v4

    .line 775
    .local v18, "timeDifference":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 776
    .local v11, "currentCalendar":Ljava/util/Calendar;
    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 779
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 780
    .local v10, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v10, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 782
    const/16 v3, 0xb

    const/16 v4, 0x14

    invoke-virtual {v10, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 784
    const/16 v3, 0xc

    const/4 v4, 0x0

    invoke-virtual {v10, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 785
    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v10, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 786
    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v10, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 789
    const-wide/32 v12, 0xea60

    .line 798
    .local v12, "delaySecond":J
    invoke-virtual {v2, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 799
    const/4 v3, 0x0

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 813
    return-void

    .line 769
    .end local v10    # "calendar":Ljava/util/Calendar;
    .end local v11    # "currentCalendar":Ljava/util/Calendar;
    .end local v12    # "delaySecond":J
    .end local v18    # "timeDifference":J
    :catch_0
    move-exception v14

    .line 770
    .local v14, "e":Ljava/lang/Exception;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    goto :goto_0
.end method
