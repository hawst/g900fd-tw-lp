.class public Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
.super Ljava/lang/Object;
.source "WidgetDataManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

.field private static mWidgetDataArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/widget/samsungapps/data/WidgetData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->TAG:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->instance:Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    .line 15
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mWidgetDataArray:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->TAG:Ljava/lang/String;

    const-string v1, "WidgetDataManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method private addWidgetData(I)V
    .locals 4
    .param p1, "widgetSize"    # I

    .prologue
    .line 61
    sget-object v1, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addWidgetData() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    new-instance v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/widget/samsungapps/data/WidgetData;-><init>(Landroid/content/Context;I)V

    .line 70
    .local v0, "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    sget-object v1, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mWidgetDataArray:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->instance:Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->instance:Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    .line 26
    :cond_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->instance:Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    return-object v0
.end method


# virtual methods
.method public getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;
    .locals 2
    .param p1, "widgetSize"    # I

    .prologue
    .line 45
    sget-object v1, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mWidgetDataArray:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 47
    .local v0, "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    if-nez v0, :cond_0

    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->addWidgetData(I)V

    .line 49
    sget-object v1, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->mWidgetDataArray:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    check-cast v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 51
    .restart local v0    # "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    :cond_0
    return-object v0
.end method
