.class Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;
.super Ljava/lang/Object;
.source "MobileAgreeActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v2, 0x1

    .line 124
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->finish()V

    .line 126
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    # getter for: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$000(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-ne v2, v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    # getter for: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I
    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$100(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 129
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->savePreferenceWLANPopupDontShow(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 139
    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_3G_AGREE:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    # getter for: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mInstanceId:I
    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$200(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 141
    const-string v1, "index"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    # getter for: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mClickIndex:I
    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$300(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    # getter for: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mCurWidgetSize:I
    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$400(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I

    move-result v2

    # invokes: Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getComponentName(I)Landroid/content/ComponentName;
    invoke-static {v1, v2}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->access$500(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;I)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 144
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 145
    return-void

    .line 134
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;->this$0:Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->savePreference3gPopupDontShow(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
