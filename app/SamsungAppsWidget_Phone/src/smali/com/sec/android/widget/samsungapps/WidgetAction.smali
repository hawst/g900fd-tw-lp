.class public Lcom/sec/android/widget/samsungapps/WidgetAction;
.super Ljava/lang/Object;
.source "WidgetAction.java"


# static fields
.field public static final ACTION_DRAW_RENDERER_IDLE:I = 0xff10

.field public static final ACTION_DRAW_RENDERER_IDLE_LOADING:I = 0xff11

.field public static final ACTION_DRAW_RENDERER_INIT:I = 0xff04

.field public static final ACTION_DRAW_RENDERER_INIT_LOADING:I = 0xff08

.field public static final ACTION_MASK_CORE:I = 0xff0000

.field public static final ACTION_MASK_CUSTOM:I = 0xff000

.field public static final ACTION_MASK_UI:I = 0xff00

.field public static final ACTION_REFRESH_BUTTON:I = 0xff02

.field public static final ACTION_START_WEBWIDGET_CLICK_BUTTON:I = 0xff01


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static widgetActionDumpString(I)Ljava/lang/String;
    .locals 1
    .param p0, "_action"    # I

    .prologue
    .line 23
    sparse-switch p0, :sswitch_data_0

    .line 51
    const-string v0, "ACTION_NONE"

    :goto_0
    return-object v0

    .line 27
    :sswitch_0
    const-string v0, "ACTION_START_WEBWIDGET_CLICK_BUTTON"

    goto :goto_0

    .line 31
    :sswitch_1
    const-string v0, "ACTION_REFRESH_BUTTON"

    goto :goto_0

    .line 35
    :sswitch_2
    const-string v0, "ACTION_DRAW_RENDERER_INIT"

    goto :goto_0

    .line 39
    :sswitch_3
    const-string v0, "ACTION_DRAW_RENDERER_INIT_LOADING"

    goto :goto_0

    .line 43
    :sswitch_4
    const-string v0, "ACTION_DRAW_RENDERER_IDLE"

    goto :goto_0

    .line 47
    :sswitch_5
    const-string v0, "ACTION_DRAW_RENDERER_IDLE_LOADING"

    goto :goto_0

    .line 23
    :sswitch_data_0
    .sparse-switch
        0xff01 -> :sswitch_0
        0xff02 -> :sswitch_1
        0xff04 -> :sswitch_2
        0xff08 -> :sswitch_3
        0xff10 -> :sswitch_4
        0xff11 -> :sswitch_5
    .end sparse-switch
.end method
