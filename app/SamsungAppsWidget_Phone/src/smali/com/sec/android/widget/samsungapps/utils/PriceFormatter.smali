.class public Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;
.super Ljava/lang/Object;
.source "PriceFormatter.java"


# instance fields
.field private _CurrencyUnitDivision:Z

.field private _CurrencyUnitHasPenny:Z

.field private _CurrencyUnitPrecedes:Z

.field private _currencyUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZZLjava/lang/String;)V
    .locals 0
    .param p1, "currencyUnitHasPenny"    # Z
    .param p2, "currencyUnitDivision"    # Z
    .param p3, "currencyUnitPrecedes"    # Z
    .param p4, "currencyUnit"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean p1, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitHasPenny:Z

    .line 15
    iput-boolean p2, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitDivision:Z

    .line 16
    iput-boolean p3, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitPrecedes:Z

    .line 17
    iput-object p4, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getFormattedPrice(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "price"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 21
    const-string v2, ""

    .line 22
    .local v2, "result":Ljava/lang/String;
    move-object v3, p1

    .line 23
    .local v3, "strPrice":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 24
    .local v4, "value":D
    iget-boolean v6, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitHasPenny:Z

    if-eqz v6, :cond_0

    .line 26
    :try_start_0
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v6, "0.00"

    invoke-direct {v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 27
    .local v1, "format":Ljava/text/DecimalFormat;
    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 32
    .end local v1    # "format":Ljava/text/DecimalFormat;
    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitDivision:Z

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x3

    if-le v6, v7, :cond_2

    .line 34
    :try_start_1
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v6, "#,###"

    invoke-direct {v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 35
    .restart local v1    # "format":Ljava/text/DecimalFormat;
    iget-boolean v6, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitHasPenny:Z

    if-eqz v6, :cond_1

    .line 36
    new-instance v1, Ljava/text/DecimalFormat;

    .end local v1    # "format":Ljava/text/DecimalFormat;
    const-string v6, "#,###.00"

    invoke-direct {v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 38
    .restart local v1    # "format":Ljava/text/DecimalFormat;
    :cond_1
    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 43
    .end local v1    # "format":Ljava/text/DecimalFormat;
    :cond_2
    :goto_1
    iget-boolean v6, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_CurrencyUnitPrecedes:Z

    if-eqz v6, :cond_3

    .line 44
    const-string v6, "%s%s"

    new-array v7, v8, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 48
    :goto_2
    return-object v2

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, " "

    goto :goto_0

    .line 39
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 40
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    const-string v3, " "

    goto :goto_1

    .line 46
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const-string v6, "%s%s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v3, v7, v9

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/utils/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method
