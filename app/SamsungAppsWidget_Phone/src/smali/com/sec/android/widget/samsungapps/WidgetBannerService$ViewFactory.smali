.class Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;
.super Ljava/lang/Object;
.source "WidgetBannerService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/WidgetBannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewFactory"
.end annotation


# instance fields
.field private mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

.field private final mContext:Landroid/content/Context;

.field private mCurrentWidgetSize:I

.field private mInstanceId:I

.field private mIsSecondPage:Z

.field mPrefs:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/sec/android/widget/samsungapps/WidgetBannerService;


# direct methods
.method public constructor <init>(Lcom/sec/android/widget/samsungapps/WidgetBannerService;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p2, "_context"    # Landroid/content/Context;
    .param p3, "_intent"    # Landroid/content/Intent;

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 121
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->this$0:Lcom/sec/android/widget/samsungapps/WidgetBannerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput v2, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mIsSecondPage:Z

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    .line 122
    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mInstanceId:I

    .line 126
    const-string v0, "WIDGET_SIZE"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    .line 129
    const-string v0, "isSecondPage"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mIsSecondPage:Z

    .line 133
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    .line 134
    new-instance v0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v0, p2}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCache:Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    .line 136
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 137
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mPrefs:Landroid/content/SharedPreferences;

    .line 141
    :cond_0
    return-void
.end method

.method private getSubtitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    .line 326
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategoryName()Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "selectedCategoryName":Ljava/lang/String;
    const v1, 0x7f0a002b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%s"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;III)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceId"    # I
    .param p3, "_action"    # Ljava/lang/String;
    .param p4, "_rViews"    # Landroid/widget/RemoteViews;
    .param p5, "_rId"    # I
    .param p6, "_page"    # I
    .param p7, "_index"    # I

    .prologue
    const/4 v3, 0x0

    .line 389
    new-instance v0, Landroid/content/Intent;

    iget v2, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    invoke-static {v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 393
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 395
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 398
    if-ltz p6, :cond_0

    .line 399
    const-string v2, "page"

    invoke-virtual {v0, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 402
    :cond_0
    const/4 v2, -0x1

    if-ne p7, v2, :cond_1

    .line 403
    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 404
    .local v1, "pIntent":Landroid/app/PendingIntent;
    invoke-virtual {p4, p5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 416
    :goto_0
    return-void

    .line 406
    .end local v1    # "pIntent":Landroid/app/PendingIntent;
    :cond_1
    const-string v2, "index"

    invoke-virtual {v0, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 408
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v3, 0x8000000

    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 413
    .restart local v1    # "pIntent":Landroid/app/PendingIntent;
    invoke-virtual {p4, p5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private setOnclickActionInAdapterView(Ljava/lang/String;Landroid/widget/RemoteViews;III)V
    .locals 3
    .param p1, "_action"    # Ljava/lang/String;
    .param p2, "_rViews"    # Landroid/widget/RemoteViews;
    .param p3, "_rId"    # I
    .param p4, "_page"    # I
    .param p5, "_index"    # I

    .prologue
    .line 433
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 434
    .local v1, "fillInIntent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 436
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "page"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 437
    const-string v2, "index"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 439
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 441
    invoke-virtual {p2, p3, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 442
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 164
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v1

    .line 165
    .local v1, "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    invoke-virtual {v1, v4}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v2

    .line 167
    .local v2, "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    iget v4, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    if-ne v4, v3, :cond_0

    .line 173
    .end local v1    # "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    .end local v2    # "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    :goto_0
    return v3

    .line 170
    .restart local v1    # "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    .restart local v2    # "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 172
    .end local v1    # "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    .end local v2    # "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x2

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "_position"    # I

    .prologue
    .line 358
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "listview.getItemId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 335
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030004

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 19
    .param p1, "_position"    # I

    .prologue
    .line 181
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "listview.getViewAt() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const/4 v9, 0x0

    .line 187
    .local v9, "addedIndex":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v15

    .line 188
    .local v15, "manager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    invoke-virtual {v15, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v18

    .line 190
    .local v18, "widgetData":Lcom/sec/android/widget/samsungapps/data/WidgetData;
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "widgetData.isSecondPage() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isSecondPage()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mIsSecondPage:Z

    if-eqz v1, :cond_0

    .line 192
    const/4 v9, 0x0

    .line 195
    :cond_0
    const/16 v16, 0x0

    .line 198
    .local v16, "rv":Landroid/widget/RemoteViews;
    :try_start_0
    new-instance v5, Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030003

    invoke-direct {v5, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 202
    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .local v5, "rv":Landroid/widget/RemoteViews;
    const v2, 0x7f0c0007

    :try_start_1
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v1

    add-int/lit8 v3, p1, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 203
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "listview.getViewAt()."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".bannerTitle : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v1

    add-int/lit8 v4, p1, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v10, 0x0

    .line 206
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    new-instance v11, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    .local v11, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :try_start_2
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v1

    add-int/lit8 v2, p1, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmWidgetSymbolImgURL()Ljava/lang/String;

    move-result-object v17

    .line 209
    .local v17, "strUrl":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 211
    if-eqz v10, :cond_1

    .line 213
    const v1, 0x7f0c0004

    const/4 v2, 0x4

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 214
    const v1, 0x7f0c0005

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 215
    const v1, 0x7f0c0006

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 217
    const v1, 0x7f0c0005

    invoke-virtual {v5, v1, v10}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 221
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 222
    .local v14, "fillInIntent":Landroid/content/Intent;
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 223
    .local v13, "extras":Landroid/os/Bundle;
    const-string v1, "page"

    const/4 v2, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 224
    const-string v1, "index"

    add-int/lit8 v2, p1, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 225
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER:Ljava/lang/String;

    invoke-virtual {v14, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    invoke-virtual {v14, v13}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 227
    const v1, 0x7f0c0003

    invoke-virtual {v5, v1, v14}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 322
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v13    # "extras":Landroid/os/Bundle;
    .end local v14    # "fillInIntent":Landroid/content/Intent;
    .end local v17    # "strUrl":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 240
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .restart local v17    # "strUrl":Ljava/lang/String;
    :cond_1
    const v1, 0x7f0c0004

    const/4 v2, 0x4

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 241
    const v1, 0x7f0c0005

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 242
    const v1, 0x7f0c0006

    const/4 v2, 0x4

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 243
    if-nez p1, :cond_3

    .line 244
    const v1, 0x7f0c0005

    const v2, 0x7f020015

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 251
    :cond_2
    :goto_1
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 252
    .restart local v14    # "fillInIntent":Landroid/content/Intent;
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 253
    .restart local v13    # "extras":Landroid/os/Bundle;
    const-string v1, "page"

    const/4 v2, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    const-string v1, "index"

    add-int/lit8 v2, p1, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 255
    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER:Ljava/lang/String;

    invoke-virtual {v14, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    invoke-virtual {v14, v13}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 257
    const v1, 0x7f0c0003

    invoke-virtual {v5, v1, v14}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 260
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mInstanceId:I

    sget-object v4, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_CLICK_BANNER:Ljava/lang/String;

    const v6, 0x7f0c0003

    const/4 v7, 0x0

    add-int/lit8 v8, p1, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->setOnclickAction(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;III)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 268
    .end local v13    # "extras":Landroid/os/Bundle;
    .end local v14    # "fillInIntent":Landroid/content/Intent;
    .end local v17    # "strUrl":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 269
    .local v12, "e":Ljava/lang/NullPointerException;
    :try_start_3
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 307
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v12    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v12

    .line 308
    .local v12, "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_2
    :try_start_4
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "caught array index out of bound exception. data might be changed while sleep mode"

    invoke-static {v1, v2, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 318
    .end local v12    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catchall_0
    move-exception v1

    :goto_3
    throw v1

    .line 245
    .restart local v10    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v11    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .restart local v17    # "strUrl":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 246
    const v1, 0x7f0c0005

    const v2, 0x7f020016

    :try_start_5
    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 310
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v17    # "strUrl":Ljava/lang/String;
    :catch_2
    move-exception v12

    .line 311
    .local v12, "e":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_6
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "catch numberformat exception, media object may gone invalid due to system change"

    invoke-static {v1, v2, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 313
    .end local v5    # "rv":Landroid/widget/RemoteViews;
    .end local v12    # "e":Ljava/lang/NumberFormatException;
    .restart local v16    # "rv":Landroid/widget/RemoteViews;
    :catch_3
    move-exception v12

    move-object/from16 v5, v16

    .line 314
    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .restart local v5    # "rv":Landroid/widget/RemoteViews;
    .local v12, "e":Ljava/lang/NullPointerException;
    :goto_5
    # getter for: Lcom/sec/android/widget/samsungapps/WidgetBannerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "caught null pointer exception. check source is deleted"

    invoke-static {v1, v2, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 316
    .end local v5    # "rv":Landroid/widget/RemoteViews;
    .end local v12    # "e":Ljava/lang/NullPointerException;
    .restart local v16    # "rv":Landroid/widget/RemoteViews;
    :catch_4
    move-exception v12

    move-object/from16 v5, v16

    .line 317
    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .restart local v5    # "rv":Landroid/widget/RemoteViews;
    .local v12, "e":Ljava/lang/Exception;
    :goto_6
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 318
    .end local v5    # "rv":Landroid/widget/RemoteViews;
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v16    # "rv":Landroid/widget/RemoteViews;
    :catchall_1
    move-exception v1

    move-object/from16 v5, v16

    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .restart local v5    # "rv":Landroid/widget/RemoteViews;
    goto :goto_3

    .line 316
    :catch_5
    move-exception v12

    goto :goto_6

    .line 313
    :catch_6
    move-exception v12

    goto :goto_5

    .line 310
    .end local v5    # "rv":Landroid/widget/RemoteViews;
    .restart local v16    # "rv":Landroid/widget/RemoteViews;
    :catch_7
    move-exception v12

    move-object/from16 v5, v16

    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .restart local v5    # "rv":Landroid/widget/RemoteViews;
    goto :goto_4

    .line 307
    .end local v5    # "rv":Landroid/widget/RemoteViews;
    .restart local v16    # "rv":Landroid/widget/RemoteViews;
    :catch_8
    move-exception v12

    move-object/from16 v5, v16

    .end local v16    # "rv":Landroid/widget/RemoteViews;
    .restart local v5    # "rv":Landroid/widget/RemoteViews;
    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/sec/android/widget/samsungapps/WidgetBannerService$ViewFactory;->mCurrentWidgetSize:I

    packed-switch v0, :pswitch_data_0

    .line 353
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 342
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 344
    :pswitch_1
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetBannerService;->m4x2LayoutIds:[I

    array-length v0, v0

    goto :goto_0

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public onDataSetChanged()V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method
