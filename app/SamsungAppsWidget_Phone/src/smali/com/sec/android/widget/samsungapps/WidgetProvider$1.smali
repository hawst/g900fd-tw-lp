.class Lcom/sec/android/widget/samsungapps/WidgetProvider$1;
.super Ljava/util/TimerTask;
.source "WidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widget/samsungapps/WidgetProvider;->setAlarmManagerForBannerUpdate(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widget/samsungapps/WidgetProvider;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/widget/samsungapps/WidgetProvider;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->this$0:Lcom/sec/android/widget/samsungapps/WidgetProvider;

    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 825
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->this$0:Lcom/sec/android/widget/samsungapps/WidgetProvider;

    # getter for: Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I
    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->access$000(Lcom/sec/android/widget/samsungapps/WidgetProvider;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetPageLayout(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 828
    .local v3, "remoteViews":Landroid/widget/RemoteViews;
    const v4, 0x7f0c0008

    const/16 v5, 0x8

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 832
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->val$context:Landroid/content/Context;

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 834
    .local v1, "manager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/WidgetProvider$1;->this$0:Lcom/sec/android/widget/samsungapps/WidgetProvider;

    # getter for: Lcom/sec/android/widget/samsungapps/WidgetProvider;->mWidgetSize:I
    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/WidgetProvider;->access$000(Lcom/sec/android/widget/samsungapps/WidgetProvider;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 838
    .local v2, "provider":Landroid/content/ComponentName;
    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 839
    .local v0, "appWidgetIds":[I
    invoke-virtual {v1, v0, v3}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 841
    return-void
.end method
