.class public Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;
.super Ljava/lang/Object;
.source "WidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;
    }
.end annotation


# static fields
.field static INTERVAL:I

.field static MAX_LOG:I

.field static gLogTimeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    .line 33
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->INTERVAL:I

    .line 34
    const/16 v0, 0x64

    sput v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->MAX_LOG:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561
    return-void
.end method

.method public static createBitmap(Landroid/graphics/Picture;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "_picture"    # Landroid/graphics/Picture;

    .prologue
    const/4 v6, 0x0

    .line 363
    if-nez p0, :cond_0

    .line 364
    const/4 v0, 0x0

    .line 377
    :goto_0
    return-object v0

    .line 367
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Picture;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Picture;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 370
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 372
    .local v1, "contentCanvas":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Picture;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0}, Landroid/graphics/Picture;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 375
    .local v2, "dst":Landroid/graphics/Rect;
    invoke-virtual {v1, p0, v2}, Landroid/graphics/Canvas;->drawPicture(Landroid/graphics/Picture;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public static getCurDateString(Landroid/content/Context;Ljava/util/Calendar;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "_calendar"    # Ljava/util/Calendar;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getUpdateTimeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "strTime":Ljava/lang/String;
    return-object v0
.end method

.method private static getDateFormat(Landroid/content/Context;)Ljava/text/SimpleDateFormat;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 72
    .local v0, "format":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[^\\p{Alpha}]*y+[^\\p{Alpha}]*"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 73
    return-object v0
.end method

.method public static getPreference3gPopupDontShow(Landroid/content/Context;)Z
    .locals 3
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 162
    const/4 v0, 0x0

    .line 164
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "com.sec.android.widget.samsungapps"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 167
    const-string v1, "dont_show"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getPreferenceWLANPopupDontShow(Landroid/content/Context;)Z
    .locals 3
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 184
    const/4 v0, 0x0

    .line 186
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "com.sec.android.widget.samsungapps"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 189
    const-string v1, "dont_show_wlan"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    .line 518
    const/4 v0, 0x0

    .line 519
    .local v0, "emailID":Ljava/lang/String;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 520
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 522
    .local v2, "samsungAccnts":[Landroid/accounts/Account;
    array-length v3, v2

    if-lez v3, :cond_0

    .line 523
    const/4 v3, 0x0

    aget-object v3, v2, v3

    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 524
    :cond_0
    return-object v0
.end method

.method private static getShortDateWithoutYears()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 47
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[^\\p{Alpha}]*y+[^\\p{Alpha}]*"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 50
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getShortTime(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 56
    .local v0, "timeformat":Ljava/text/DateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static getSystemProperties(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_key"    # Ljava/lang/String;
    .param p2, "_def"    # Ljava/lang/String;

    .prologue
    .line 476
    move-object v6, p2

    .line 479
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 480
    .local v1, "cl":Ljava/lang/ClassLoader;
    const-string v7, "android.os.SystemProperties"

    invoke-virtual {v1, v7}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 483
    .local v0, "SystemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v7, 0x2

    new-array v4, v7, [Ljava/lang/Class;

    .line 484
    .local v4, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 485
    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 487
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 489
    .local v3, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/Object;

    .line 490
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 491
    const/4 v7, 0x1

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 493
    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 500
    .end local v0    # "SystemProperties":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "cl":Ljava/lang/ClassLoader;
    .end local v3    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 494
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 495
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    move-object v6, p2

    .line 498
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 496
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 497
    .local v2, "e":Ljava/lang/Exception;
    move-object v6, p2

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getUpdateTimeString(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 62
    .local v0, "date":Ljava/util/Date;
    const/4 v1, 0x0

    .line 63
    .local v1, "dateString":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getDateFormat(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    .line 64
    .local v2, "dateformat":Ljava/text/DateFormat;
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    .line 65
    .local v3, "timeformat":Ljava/text/DateFormat;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 67
    return-object v1
.end method

.method public static getWidgetClass(I)Ljava/lang/Class;
    .locals 1
    .param p0, "_widgetSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 529
    packed-switch p0, :pswitch_data_0

    .line 535
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 531
    :pswitch_0
    const-class v0, Lcom/sec/android/widget/samsungapps/WidgetProvider1;

    goto :goto_0

    .line 533
    :pswitch_1
    const-class v0, Lcom/sec/android/widget/samsungapps/WidgetProvider2;

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getWidgetIntroLayout(I)I
    .locals 1
    .param p0, "_widgetSize"    # I

    .prologue
    .line 551
    packed-switch p0, :pswitch_data_0

    .line 557
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 553
    :pswitch_0
    const v0, 0x7f030008

    goto :goto_0

    .line 555
    :pswitch_1
    const v0, 0x7f030009

    goto :goto_0

    .line 551
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getWidgetPageLayout(I)I
    .locals 1
    .param p0, "_widgetSize"    # I

    .prologue
    .line 540
    packed-switch p0, :pswitch_data_0

    .line 546
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 542
    :pswitch_0
    const v0, 0x7f030005

    goto :goto_0

    .line 544
    :pswitch_1
    const v0, 0x7f030006

    goto :goto_0

    .line 540
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static goDeeplink(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 13
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "deeplinkUrl"    # Ljava/lang/String;

    .prologue
    .line 242
    :try_start_0
    const-string v10, "/"

    invoke-virtual {p1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 243
    .local v7, "parsedDeepLink":[Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, v7, v10

    const/4 v11, 0x7

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "action":Ljava/lang/String;
    const-string v10, "goDeepLink"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "action : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 248
    .local v4, "intent":Landroid/content/Intent;
    array-length v10, v7

    const/4 v11, 0x4

    if-ne v10, v11, :cond_0

    const/4 v10, 0x3

    aget-object v10, v7, v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 249
    const/4 v10, 0x3

    aget-object v10, v7, v10

    const-string v11, "&"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 251
    .local v8, "parsedParams":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v1, v3

    .line 252
    .local v6, "param":Ljava/lang/String;
    const-string v10, "="

    invoke-virtual {v6, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 253
    .local v9, "splitedParam":[Ljava/lang/String;
    const/4 v10, 0x0

    aget-object v10, v9, v10

    const/4 v11, 0x1

    aget-object v11, v9, v11

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    const-string v10, "goDeepLink"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "param : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 258
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "param":Ljava/lang/String;
    .end local v8    # "parsedParams":[Ljava/lang/String;
    .end local v9    # "splitedParam":[Ljava/lang/String;
    :cond_0
    const/high16 v10, 0x14000000

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    const/4 v10, 0x1

    .end local v0    # "action":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v7    # "parsedDeepLink":[Ljava/lang/String;
    :goto_1
    return v10

    .line 262
    :catch_0
    move-exception v2

    .line 263
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 264
    const/4 v10, 0x0

    goto :goto_1
.end method

.method public static goExternalApps(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_appUrl"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 348
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 349
    .local v2, "pkgmanager":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 350
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pkgmanager":Landroid/content/pm/PackageManager;
    :goto_0
    return v3

    .line 351
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error with open "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 356
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static goInterimPage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "productID"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 271
    const-string v4, "com.sec.android.app.samsungapps"

    .line 273
    .local v4, "pkgName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".downloadableapps.DownloadableAppsActivity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 275
    .local v3, "interimActivityName":Ljava/lang/String;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v4, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .local v0, "componentName":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 277
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 279
    const v5, 0x14008000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 310
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 312
    const-string v5, "PRODUCTSETID"

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 316
    const-string v5, "TITLE"

    invoke-virtual {v2, v5, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    :cond_1
    :try_start_0
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    const/4 v5, 0x1

    :goto_0
    return v5

    .line 321
    :catch_0
    move-exception v1

    .line 322
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 323
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static goMarket(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_url"    # Ljava/lang/String;

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->goMarket(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static goMarket(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_url"    # Ljava/lang/String;
    .param p2, "_bannerTitle"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 200
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 201
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 203
    if-eqz p2, :cond_0

    .line 204
    const-string v4, "categoryTitle"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    :cond_0
    const-string v4, "amIS2I"

    const-string v5, "Y"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const v4, 0x14000020

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 225
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return v2

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "You\'ve not installed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "://"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v2, v3

    .line 233
    goto :goto_0
.end method

.method public static goOutLink(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_url"    # Ljava/lang/String;

    .prologue
    .line 330
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 332
    const v2, 0x14000020

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 337
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 340
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isConnected(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;
    .locals 8
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    .line 84
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 87
    .local v3, "manager":Landroid/net/ConnectivityManager;
    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v4

    .line 102
    .local v4, "networkInfo":[Landroid/net/NetworkInfo;
    move-object v0, v4

    .local v0, "arr$":[Landroid/net/NetworkInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_8

    aget-object v5, v0, v1

    .line 103
    .local v5, "ni":Landroid/net/NetworkInfo;
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 104
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 105
    :cond_0
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 106
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    .line 145
    .end local v5    # "ni":Landroid/net/NetworkInfo;
    :goto_1
    return-object v6

    .line 110
    .restart local v5    # "ni":Landroid/net/NetworkInfo;
    :cond_1
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    const/4 v7, 0x7

    if-ne v6, v7, :cond_3

    .line 111
    :cond_2
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 112
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    goto :goto_1

    .line 116
    :cond_3
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH_TETHER:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 117
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 118
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH_TETHER:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    goto :goto_1

    .line 122
    :cond_4
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI_P2P:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 123
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 124
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI_P2P:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    goto :goto_1

    .line 129
    :cond_5
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->MOBILE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->CELLULAR:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    if-nez v6, :cond_7

    .line 132
    :cond_6
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 133
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->MOBILE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    goto/16 :goto_1

    .line 102
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 145
    .end local v5    # "ni":Landroid/net/NetworkInfo;
    :cond_8
    sget-object v6, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->NOT_AVAILABLE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    goto/16 :goto_1
.end method

.method public static isHaveSamsungAccount(Landroid/content/Context;)Z
    .locals 3
    .param p0, "_context"    # Landroid/content/Context;

    .prologue
    .line 506
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 507
    .local v0, "manager":Landroid/accounts/AccountManager;
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 510
    .local v1, "samsungAccnts":[Landroid/accounts/Account;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 511
    const/4 v2, 0x1

    .line 513
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isLogDelayPrintLog(Ljava/lang/String;)Z
    .locals 10
    .param p0, "_key"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 451
    :try_start_0
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    sget v5, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->MAX_LOG:I

    if-le v2, v5, :cond_0

    .line 452
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 455
    :cond_0
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 456
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, p0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .line 472
    :goto_0
    return v2

    .line 460
    :cond_1
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 462
    .local v0, "beforeTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    sget v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->INTERVAL:I

    int-to-long v8, v2

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    .line 463
    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->gLogTimeMap:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, p0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 464
    goto :goto_0

    :cond_2
    move v2, v4

    .line 466
    goto :goto_0

    .line 468
    .end local v0    # "beforeTime":J
    :catch_0
    move-exception v2

    move v2, v4

    .line 472
    goto :goto_0
.end method

.method public static savePreference3gPopupDontShow(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 4
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_isDontShow"    # Ljava/lang/Boolean;

    .prologue
    .line 150
    const/4 v1, 0x0

    .line 152
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "com.sec.android.widget.samsungapps"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 154
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 156
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "dont_show"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    return-void
.end method

.method public static savePreferenceWLANPopupDontShow(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 4
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_isDontShow"    # Ljava/lang/Boolean;

    .prologue
    .line 172
    const/4 v1, 0x0

    .line 174
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "com.sec.android.widget.samsungapps"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 176
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "dont_show_wlan"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 181
    return-void
.end method

.method public static separateBitmap(Landroid/graphics/Bitmap;II)[Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "orgBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "_separateWidth"    # I
    .param p2, "_separateHeight"    # I

    .prologue
    .line 382
    const/4 v6, 0x0

    .line 384
    .local v6, "separateBitmaps":[Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 385
    .local v4, "orgWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 389
    .local v3, "orgHeight":I
    const/4 v7, 0x0

    .line 391
    .local v7, "separateHeight":I
    if-le v4, p1, :cond_2

    .line 392
    int-to-float v9, v4

    int-to-float v10, p1

    div-float v5, v9, v10

    .line 393
    .local v5, "ratio":F
    int-to-float v9, p2

    mul-float/2addr v9, v5

    float-to-int v7, v9

    .line 401
    .end local v5    # "ratio":F
    :goto_0
    div-int v1, v3, p2

    .line 403
    .local v1, "count":I
    if-lez v1, :cond_3

    .line 404
    rem-int v9, v3, v7

    if-lez v9, :cond_0

    .line 405
    add-int/lit8 v1, v1, 0x1

    .line 417
    :cond_0
    :goto_1
    const/4 v9, 0x2

    if-le v1, v9, :cond_1

    .line 418
    const/4 v1, 0x2

    .line 422
    :cond_1
    new-array v6, v1, [Landroid/graphics/Bitmap;

    .line 423
    move v8, v3

    .line 426
    .local v8, "totalHeight":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    :try_start_0
    array-length v9, v6

    if-le v9, v2, :cond_5

    .line 427
    if-le v8, v7, :cond_4

    .line 428
    const/4 v9, 0x0

    mul-int v10, v7, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-static {p0, v9, v10, v11, v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    aput-object v9, v6, v2

    .line 438
    sub-int/2addr v8, v7

    .line 426
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 395
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v8    # "totalHeight":I
    :cond_2
    move v7, p2

    goto :goto_0

    .line 408
    .restart local v1    # "count":I
    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    .line 432
    .restart local v2    # "i":I
    .restart local v8    # "totalHeight":I
    :cond_4
    const/4 v9, 0x0

    mul-int v10, v7, v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-static {p0, v9, v10, v11, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    aput-object v9, v6, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    .end local v6    # "separateBitmaps":[Landroid/graphics/Bitmap;
    :cond_5
    :goto_3
    return-object v6

    .line 440
    .restart local v6    # "separateBitmaps":[Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 441
    .local v0, "_ignore":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 442
    const/4 v6, 0x0

    goto :goto_3
.end method
