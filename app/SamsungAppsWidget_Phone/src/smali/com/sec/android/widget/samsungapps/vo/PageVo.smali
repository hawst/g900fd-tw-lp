.class public Lcom/sec/android/widget/samsungapps/vo/PageVo;
.super Ljava/lang/Object;
.source "PageVo.java"


# static fields
.field public static final APPS_FOR_GALAXY_4X1:I = 0x6

.field public static final APPS_FOR_GALAXY_4X2:I = 0x3

.field public static final APPS_FOR_GALAXY_APPS_PER_PAGE:I = 0x5

.field public static final CONTENT_GIFTS_4X2:I = 0x4

.field public static final CONTENT_GIFTS_APPS_PER_PAGE:I = 0x4

.field public static final FEATURED_APPS_PER_PAGE:I = 0x0

.field public static final FEATURED_BANNERS_PER_PAGE:I = 0x1

.field public static final LOCAL_APPS_PER_PAGE:I = 0x2

.field public static final LOCAL_BANNERS_PER_PAGE:I = 0x3

.field public static final LOCAL_CONTENTS_4X6:I = 0x5

.field public static final PREMIUM_CHOICE_4X4:I = 0x1

.field public static final TAG:Ljava/lang/String;

.field public static final TOP_NEW_4X4:I = 0x2


# instance fields
.field private itemCounts:[I

.field protected mAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field protected mBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mContentGiftsAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageAppStartIndex:I

.field private mCurPageBannerImageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageBannerStartIndex:I

.field private mCurPageContentGiftsAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageContentGiftsAppStartIndex:I

.field private mCurPageLocalAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageLocalAppStartIndex:I

.field private mCurPageLocalBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mCurPageLocalBannerStartIndex:I

.field protected mCurrentPage:I

.field private mLocalAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field protected mPrefEditor:Landroid/content/SharedPreferences$Editor;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mType:I

    .line 25
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurrentPage:I

    .line 26
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    .line 27
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    .line 115
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppStartIndex:I

    .line 116
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    .line 164
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerStartIndex:I

    .line 165
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    .line 166
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    .line 278
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 279
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    .line 280
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 500
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppStartIndex:I

    .line 501
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    .line 550
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerStartIndex:I

    .line 551
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    .line 595
    iput v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppStartIndex:I

    .line 596
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 49
    return-void

    .line 39
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method public addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    return-void
.end method

.method public addmContentGiftsAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    return-void
.end method

.method public addmLocalAppList(Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    return-void
.end method

.method public addmLocalBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    .locals 1
    .param p1, "_vo"    # Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 382
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    return-void
.end method

.method public deleteDuplicatedAppForContentGiftsAppList()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 413
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 414
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 415
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getListTitle()Ljava/lang/String;

    move-result-object v4

    .line 416
    .local v4, "localAppListTitle":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getLinkCategoryID()Ljava/lang/String;

    move-result-object v3

    .line 419
    .local v3, "localAppLinkCategoryID":Ljava/lang/String;
    const-string v5, "0000002474"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "Apps for Galaxy"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 420
    :cond_0
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .line 421
    .local v2, "item":Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 422
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getGUID()Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v5

    if-eq v6, v5, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getProductID()Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getProductID()Ljava/lang/String;

    move-result-object v5

    if-ne v6, v5, :cond_3

    .line 424
    :cond_2
    const-string v6, "SamsungAppsMagazineWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removed local app item - "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getProductName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " (id : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v5}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getProductID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") from Content Gifts"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 421
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 434
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "index":I
    .end local v2    # "item":Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;
    .end local v3    # "localAppLinkCategoryID":Ljava/lang/String;
    .end local v4    # "localAppListTitle":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public getCurPageAppList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 119
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 121
    :cond_0
    sget-object v4, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v5, "AppList ERROR Data!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const/4 v4, 0x0

    .line 159
    :goto_0
    return-object v4

    .line 124
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 125
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 128
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    .line 130
    iget v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppStartIndex:I

    .line 131
    .local v3, "start":I
    const/4 v0, 0x0

    .line 133
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v4, v5, :cond_4

    .line 134
    move v1, v3

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 135
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_3

    .line 138
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 140
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 147
    .end local v1    # "i":I
    :cond_4
    const/4 v2, 0x0

    .local v2, "repeatCount":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/2addr v4, v5

    if-gt v2, v4, :cond_7

    .line 148
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 149
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_5

    .line 152
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 154
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 147
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 159
    .end local v1    # "i":I
    .end local v2    # "repeatCount":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageAppList:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public getCurPageBannerImageList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "_context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_1

    .line 172
    :cond_0
    sget-object v7, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v8, "BannerList ERROR Data!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v7, 0x0

    .line 203
    :goto_0
    return-object v7

    .line 175
    :cond_1
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 176
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    goto :goto_0

    .line 179
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    .line 181
    iget v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerStartIndex:I

    .line 182
    .local v5, "start":I
    const/4 v2, 0x0

    .line 185
    .local v2, "count":I
    move v4, v5

    .local v4, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_4

    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v1, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V

    .line 189
    .local v1, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmWidgetSymbolImgURL()Ljava/lang/String;

    move-result-object v6

    .line 190
    .local v6, "strUrl":Ljava/lang/String;
    invoke-virtual {v1, v6}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 195
    .end local v6    # "strUrl":Ljava/lang/String;
    :goto_2
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    add-int/lit8 v7, v7, -0x1

    if-lt v2, v7, :cond_3

    .line 198
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    goto :goto_0

    .line 191
    :catch_0
    move-exception v3

    .line 192
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 200
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 185
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 203
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerImageList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getCurPageBannerList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 211
    :cond_0
    sget-object v3, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v4, "BannerList ERROR Data!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const/4 v3, 0x0

    .line 274
    :goto_0
    return-object v3

    .line 214
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 215
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    goto :goto_0

    .line 218
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    .line 220
    iget v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerStartIndex:I

    .line 221
    .local v2, "start":I
    const/4 v0, 0x0

    .line 224
    .local v0, "count":I
    move v1, v2

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 225
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_3

    .line 228
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    goto :goto_0

    .line 230
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 274
    :cond_4
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageBannerList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getCurPageContentGiftsAppList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    .line 599
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 601
    :cond_0
    sget-object v4, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v5, "ContentGiftsAppList ERROR Data!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    const/4 v4, 0x0

    .line 645
    :goto_0
    return-object v4

    .line 604
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 605
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 613
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    .line 615
    iget v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppStartIndex:I

    .line 616
    .local v3, "start":I
    const/4 v0, 0x0

    .line 618
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v4, v5, :cond_4

    .line 619
    move v1, v3

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 620
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_3

    .line 623
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 625
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 619
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 632
    .end local v1    # "i":I
    :cond_4
    const/4 v2, 0x0

    .local v2, "repeatCount":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/2addr v4, v5

    if-gt v2, v4, :cond_7

    .line 633
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 634
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_5

    .line 637
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 639
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 633
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 632
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 645
    .end local v1    # "i":I
    .end local v2    # "repeatCount":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageContentGiftsAppList:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public getCurPageLocalAppList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 504
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 506
    :cond_0
    sget-object v4, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v5, "LocalAppList ERROR Data!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    const/4 v4, 0x0

    .line 545
    :goto_0
    return-object v4

    .line 509
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 510
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 513
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    .line 515
    iget v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppStartIndex:I

    .line 516
    .local v3, "start":I
    const/4 v0, 0x0

    .line 518
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v4, v5, :cond_4

    .line 519
    move v1, v3

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    .line 520
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 522
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_3

    .line 523
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 525
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 532
    .end local v1    # "i":I
    :cond_4
    const/4 v2, 0x0

    .local v2, "repeatCount":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    div-int/2addr v4, v5

    if-gt v2, v4, :cond_7

    .line 533
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    .line 534
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v4, v4, v6

    add-int/lit8 v4, v4, -0x1

    if-lt v0, v4, :cond_5

    .line 537
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 539
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 533
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 532
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 545
    .end local v1    # "i":I
    .end local v2    # "repeatCount":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalAppList:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public getCurPageLocalBannerList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 554
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 557
    :cond_0
    sget-object v3, Lcom/sec/android/widget/samsungapps/vo/PageVo;->TAG:Ljava/lang/String;

    const-string v4, "LocalBannerList ERROR Data!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    const/4 v3, 0x0

    .line 591
    :goto_0
    return-object v3

    .line 560
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 561
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    goto :goto_0

    .line 564
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    .line 566
    iget v2, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerStartIndex:I

    .line 567
    .local v2, "start":I
    const/4 v0, 0x0

    .line 569
    .local v0, "count":I
    move v1, v2

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 570
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v3, v3, v5

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_3

    .line 573
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    goto :goto_0

    .line 575
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 569
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 581
    :cond_4
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_6

    .line 582
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aget v3, v3, v5

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_5

    .line 585
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    goto :goto_0

    .line 587
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 581
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 591
    :cond_6
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurPageLocalBannerList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getItemCounts()[I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    return-object v0
.end method

.method public getmAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmBannerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmContentGiftsAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmCurrentPage()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurrentPage:I

    return v0
.end method

.method public getmLocalAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmLocalBannerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public loadShuffledSequenceForContentGiftsAppList(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x0

    .line 470
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v12, 0x4

    aget v11, v11, v12

    if-le v10, v11, :cond_3

    .line 471
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getListTitle()Ljava/lang/String;

    move-result-object v1

    .line 473
    .local v1, "contentGiftsAppListTitle":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v10, :cond_0

    .line 474
    const-string v10, "com.sec.android.widget.samsungapps"

    invoke-virtual {p1, v10, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    .line 476
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 479
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-direct {v4, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 480
    .local v4, "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v8, ""

    .line 481
    .local v8, "returnStr":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    const-string v11, "contentCategoryProductList2Notc_SEQUENCE"

    invoke-interface {v10, v11, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 482
    const-string v10, ""

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 483
    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 484
    .local v9, "splitReturnStr":[Ljava/lang/String;
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v6, :cond_1

    aget-object v5, v0, v2

    .line 485
    .local v5, "indexStr":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 488
    .end local v5    # "indexStr":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 489
    .local v7, "mTmpContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 490
    .local v3, "index":I
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 492
    .end local v3    # "index":I
    :cond_2
    iput-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 494
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v10, v1}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->setListTitle(Ljava/lang/String;)V

    .line 497
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "contentGiftsAppListTitle":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "len$":I
    .end local v7    # "mTmpContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v8    # "returnStr":Ljava/lang/String;
    .end local v9    # "splitReturnStr":[Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public loadShuffledSequenceForLocalAppList(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 339
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v13, 0x2

    aget v12, v12, v13

    if-le v11, v12, :cond_4

    .line 340
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getListTitle()Ljava/lang/String;

    move-result-object v7

    .line 341
    .local v7, "localAppListTitle":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getLinkCategoryID()Ljava/lang/String;

    move-result-object v6

    .line 343
    .local v6, "localAppLinkCategoryID":Ljava/lang/String;
    const-string v11, "0000002474"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "Apps for Galaxy"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 344
    :cond_0
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v11, :cond_1

    .line 345
    const-string v11, "com.sec.android.widget.samsungapps"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    .line 347
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 350
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 351
    .local v3, "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v9, ""

    .line 352
    .local v9, "returnStr":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    const-string v12, "localFeaturedProductList2Notc_SEQUENCE"

    invoke-interface {v11, v12, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 353
    const-string v11, ""

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 354
    const-string v11, " "

    invoke-virtual {v9, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 355
    .local v10, "splitReturnStr":[Ljava/lang/String;
    move-object v0, v10

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v4, v0, v1

    .line 356
    .local v4, "indexStr":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v4    # "indexStr":Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-direct {v8, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 360
    .local v8, "mTmpLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 361
    .local v2, "index":I
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 363
    .end local v2    # "index":I
    :cond_3
    iput-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 365
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v11, v7}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setListTitle(Ljava/lang/String;)V

    .line 366
    iget-object v11, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v11, v6}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setLinkCategoryID(Ljava/lang/String;)V

    .line 370
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "len$":I
    .end local v6    # "localAppLinkCategoryID":Ljava/lang/String;
    .end local v7    # "localAppListTitle":Ljava/lang/String;
    .end local v8    # "mTmpLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    .end local v9    # "returnStr":Ljava/lang/String;
    .end local v10    # "splitReturnStr":[Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public saveShuffledSequenceForContentGiftsAppList(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 437
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v9, 0x4

    aget v8, v8, v9

    if-le v7, v8, :cond_4

    .line 438
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getListTitle()Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "contentGiftsAppListTitle":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 442
    .local v4, "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 443
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 445
    :cond_0
    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 446
    const-string v6, ""

    .line 447
    .local v6, "orderStr":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 448
    .local v3, "index":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 449
    goto :goto_1

    .line 451
    .end local v3    # "index":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v7, :cond_2

    .line 452
    const-string v7, "com.sec.android.widget.samsungapps"

    invoke-virtual {p1, v7, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    .line 454
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 456
    :cond_2
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v8, "contentCategoryProductList2Notc_SEQUENCE"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 457
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 459
    new-instance v5, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 460
    .local v5, "mTmpContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 461
    .restart local v3    # "index":I
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 463
    .end local v3    # "index":I
    :cond_3
    iput-object v5, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 465
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v7, v0}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->setListTitle(Ljava/lang/String;)V

    .line 467
    .end local v0    # "contentGiftsAppListTitle":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v5    # "mTmpContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v6    # "orderStr":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public saveShuffledSequenceForLocalAppList(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    .line 301
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v10, 0x2

    aget v9, v9, v10

    if-le v8, v9, :cond_5

    .line 302
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v8}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getListTitle()Ljava/lang/String;

    move-result-object v5

    .line 303
    .local v5, "localAppListTitle":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v8}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getLinkCategoryID()Ljava/lang/String;

    move-result-object v4

    .line 305
    .local v4, "localAppLinkCategoryID":Ljava/lang/String;
    const-string v8, "0000002474"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "Apps for Galaxy"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 307
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {v3, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 308
    .local v3, "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v0, v8, :cond_1

    .line 309
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 311
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 312
    const-string v7, ""

    .line 313
    .local v7, "orderStr":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 314
    .local v2, "index":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 315
    goto :goto_1

    .line 317
    .end local v2    # "index":I
    :cond_2
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v8, :cond_3

    .line 318
    const-string v8, "com.sec.android.widget.samsungapps"

    invoke-virtual {p1, v8, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    .line 320
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 322
    :cond_3
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v9, "localFeaturedProductList2Notc_SEQUENCE"

    invoke-interface {v8, v9, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 323
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    new-instance v6, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 326
    .local v6, "mTmpLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 328
    .restart local v2    # "index":I
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 330
    .end local v2    # "index":I
    :cond_4
    iput-object v6, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 332
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v8, v5}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setListTitle(Ljava/lang/String;)V

    .line 333
    iget-object v8, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v8, v4}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setLinkCategoryID(Ljava/lang/String;)V

    .line 336
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "indexArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v4    # "localAppLinkCategoryID":Ljava/lang/String;
    .end local v5    # "localAppListTitle":Ljava/lang/String;
    .end local v6    # "mTmpLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    .end local v7    # "orderStr":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method public setmAppList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "_appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mAppList:Ljava/util/ArrayList;

    .line 88
    return-void
.end method

.method public setmBannerList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "_bannerVo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mBannerList:Ljava/util/ArrayList;

    .line 112
    return-void
.end method

.method public setmContentGiftsAppList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 400
    .local p1, "_appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mContentGiftsAppList:Ljava/util/ArrayList;

    .line 401
    return-void
.end method

.method public setmCurrentPage(I)V
    .locals 0
    .param p1, "_currentPage"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mCurrentPage:I

    .line 80
    return-void
.end method

.method public setmLocalAppList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 289
    .local p1, "_appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalAppList:Ljava/util/ArrayList;

    .line 290
    return-void
.end method

.method public setmLocalBannerList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p1, "_bannerVo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mLocalBannerList:Ljava/util/ArrayList;

    .line 390
    return-void
.end method

.method public setmType(I)V
    .locals 5
    .param p1, "_type"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x4

    .line 56
    iput p1, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->mType:I

    .line 58
    packed-switch p1, :pswitch_data_0

    .line 72
    :goto_0
    :pswitch_0
    return-void

    .line 60
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 61
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aput v2, v0, v4

    .line 62
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    const/4 v1, 0x6

    aput v1, v0, v3

    goto :goto_0

    .line 65
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aput v3, v0, v3

    goto :goto_0

    .line 68
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aput v2, v0, v2

    .line 69
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/PageVo;->itemCounts:[I

    aput v2, v0, v4

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
