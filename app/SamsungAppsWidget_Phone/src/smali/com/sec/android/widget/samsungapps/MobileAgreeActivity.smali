.class public Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;
.super Landroid/app/Activity;
.source "MobileAgreeActivity.java"


# static fields
.field public static final EXTRA_WIDGET_DONT_SHOW:Ljava/lang/String; = "dont_show"

.field public static final EXTRA_WIDGET_DONT_SHOW_WLAN:Ljava/lang/String; = "dont_show_wlan"


# instance fields
.field backListener:Landroid/content/DialogInterface$OnCancelListener;

.field cancelListener:Landroid/content/DialogInterface$OnClickListener;

.field private m3gDontShowCheckBox:Landroid/widget/CheckBox;

.field private m3gDontShowCheckBoxText:Landroid/widget/TextView;

.field private mClickIndex:I

.field private mCurWidgetSize:I

.field private mInstanceId:I

.field private mPopupType:I

.field okListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mCurWidgetSize:I

    .line 25
    iput v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mInstanceId:I

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mClickIndex:I

    .line 27
    iput v1, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    .line 29
    iput-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    .line 30
    iput-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBoxText:Landroid/widget/TextView;

    .line 113
    new-instance v0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$3;-><init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->cancelListener:Landroid/content/DialogInterface$OnClickListener;

    .line 121
    new-instance v0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$4;-><init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->okListener:Landroid/content/DialogInterface$OnClickListener;

    .line 148
    new-instance v0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$5;-><init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->backListener:Landroid/content/DialogInterface$OnCancelListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mInstanceId:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mClickIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mCurWidgetSize:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;I)Landroid/content/ComponentName;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getComponentName(I)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method private getComponentName(I)Landroid/content/ComponentName;
    .locals 3
    .param p1, "currentWidgetSize"    # I

    .prologue
    .line 156
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 157
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/widget/samsungapps/WidgetProvider1;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    :goto_0
    return-object v0

    .line 159
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 160
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/widget/samsungapps/WidgetProvider2;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 163
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/widget/samsungapps/WidgetProvider2;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private getContent()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 78
    const v2, 0x7f030001

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 80
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0c0001

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    .line 81
    const v2, 0x7f0c0002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBoxText:Landroid/widget/TextView;

    .line 84
    iget v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    if-ne v2, v4, :cond_1

    .line 85
    const/high16 v2, 0x7f0c0000

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    .local v0, "popupText":Landroid/widget/TextView;
    const v2, 0x7f0a002e

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 97
    .end local v0    # "popupText":Landroid/widget/TextView;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$1;-><init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBoxText:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity$2;-><init>(Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-object v1

    .line 91
    :cond_1
    iget v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gMcc:Ljava/lang/String;

    const-string v3, "460"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->m3gDontShowCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    move-result v2

    .line 36
    .local v2, "flags":I
    const/high16 v3, 0x100000

    and-int/2addr v3, v2

    if-eqz v3, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->finish()V

    .line 41
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 45
    .local v1, "extra":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 46
    const-string v3, "WidgetSize"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mCurWidgetSize:I

    .line 47
    const-string v3, "appWidgetId"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mInstanceId:I

    .line 48
    const-string v3, "ClickIndex"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mClickIndex:I

    .line 49
    const-string v3, "PopupType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    .line 52
    :cond_1
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->setContentView(Landroid/view/View;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1040014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "dialogTitleText":Ljava/lang/String;
    iget v3, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->mPopupType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    const-string v3, "Wi-Fi"

    const-string v4, "WLAN"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :cond_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    const/4 v4, 0x5

    invoke-direct {v3, p0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getContent()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->okListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->cancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/MobileAgreeActivity;->backListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 75
    return-void
.end method
