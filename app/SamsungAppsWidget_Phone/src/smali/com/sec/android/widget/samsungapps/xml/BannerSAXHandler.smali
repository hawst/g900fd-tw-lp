.class public Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "BannerSAXHandler.java"


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mNode:Ljava/lang/String;

.field private mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

.field private mValue:Ljava/lang/StringBuffer;

.field private mValueType:Ljava/lang/String;

.field private mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValue:Ljava/lang/StringBuffer;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mList:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 24
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-void
.end method

.method private getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "_attr"    # Lorg/xml/sax/Attributes;
    .param p2, "_attrName"    # Ljava/lang/String;

    .prologue
    .line 227
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 230
    const-string v1, ""

    .line 234
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 222
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 140
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 147
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "trimedValue":Ljava/lang/String;
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    const-string v1, "bannerProductID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 153
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerProductID(Ljava/lang/String;)V

    .line 206
    :cond_0
    :goto_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    .line 210
    :cond_1
    const-string v1, "list"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 212
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_2
    return-void

    .line 155
    :cond_3
    const-string v1, "bannerImgURL"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 157
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerImgURL(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_4
    const-string v1, "bannerType"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 161
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerType(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_5
    const-string v1, "promotionTitle"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 165
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmPromotionTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_6
    const-string v1, "bannerTitle"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 169
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_7
    const-string v1, "bannerPos"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 173
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerPos(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_8
    const-string v1, "bannerLinkURL"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 177
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerLinkURL(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :cond_9
    const-string v1, "appID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 181
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmAppID(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 183
    :cond_a
    const-string v1, "errorCode"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 185
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 187
    :cond_b
    const-string v1, "widgetTitle"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 189
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmWidgetTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    :cond_c
    const-string v1, "widgetBGImgURL"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 193
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmWidgetBGImgURL(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 195
    :cond_d
    const-string v1, "widgetSymbolImgURL"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 197
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmWidgetSymbolImgURL(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 199
    :cond_e
    const-string v1, "bannerDescription"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setmBannerDescription(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getParsedErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParsedList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-object v0
.end method

.method public getTotalCount()J
    .locals 4

    .prologue
    .line 35
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getTotalCount()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "strTotalCount":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 40
    .local v2, "totalCount":J
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 47
    :goto_0
    return-wide v2

    .line 42
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mList:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 66
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    .line 67
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 80
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    .line 81
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValue:Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 83
    const-string v4, "list"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 87
    new-instance v4, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-direct {v4}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 92
    const-string v4, "numValue"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "strNumValue":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 97
    .local v2, "numValue":J
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 104
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->setNumValue(J)V

    .line 134
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_0
    :goto_1
    return-void

    .line 99
    .restart local v1    # "strNumValue":Ljava/lang/String;
    .restart local v2    # "numValue":J
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_1
    const-string v4, "value"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 111
    const-string v4, "name"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mValueType:Ljava/lang/String;

    goto :goto_1

    .line 114
    :cond_2
    const-string v4, "response"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 116
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "id"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setId(Ljava/lang/String;)V

    .line 117
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "name"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setName(Ljava/lang/String;)V

    .line 118
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "returnCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setReturnCode(Ljava/lang/String;)V

    .line 119
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "startNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setStartNum(Ljava/lang/String;)V

    .line 120
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "endNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setEndNum(Ljava/lang/String;)V

    .line 121
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "transactionId"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTransactionId(Ljava/lang/String;)V

    goto :goto_1

    .line 123
    :cond_3
    const-string v4, "SamsungProtocol"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 125
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "deviceModel"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setDeviceModel(Ljava/lang/String;)V

    .line 126
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "networkType"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setNetworkType(Ljava/lang/String;)V

    .line 127
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "lang"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setLang(Ljava/lang/String;)V

    .line 128
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "version"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 130
    :cond_4
    const-string v4, "errorString"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 132
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "errorCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
