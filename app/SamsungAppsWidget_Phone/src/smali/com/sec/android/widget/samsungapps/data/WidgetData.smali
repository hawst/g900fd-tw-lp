.class public Lcom/sec/android/widget/samsungapps/data/WidgetData;
.super Ljava/lang/Object;
.source "WidgetData.java"


# static fields
.field public static final INVALID_WIDGET_SIZE:I = 0x10

.field private static final TAG:Ljava/lang/String;

.field public static final WIDGET_SIZE:Ljava/lang/String; = "WIDGET_SIZE"

.field public static final WIDGET_SIZE_4X1:I = 0x1

.field public static final WIDGET_SIZE_4X2:I = 0x2

.field public static final WIDGET_SIZE_4X4:I = 0x4

.field public static final WIDGET_SIZE_4X6:I = 0x6

.field public static final WIDGET_STATE_FLAG_USECACHE:I = 0x10000

.field public static final WIDGET_STATE_IDLE:I = 0x4

.field public static final WIDGET_STATE_IDLE_LOADING:I = 0x8

.field public static final WIDGET_STATE_INIT:I = 0x1

.field public static final WIDGET_STATE_INIT_LOADING:I = 0x2

.field public static final WIDGET_STATE_TERM:I = 0x10


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsSecondPage:Z

.field private mPageAllAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;"
        }
    .end annotation
.end field

.field private mPageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;"
        }
    .end annotation
.end field

.field private mPageListBackup:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefs:Landroid/content/SharedPreferences;

.field public mStateLock:Ljava/lang/Object;

.field public mTempWebLoadingErrorMsg:Ljava/lang/String;

.field private mWidgetSize:I

.field private mWidgetState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetSize"    # I

    .prologue
    const/16 v0, 0x10

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    .line 57
    iput v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetSize:I

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mStateLock:Ljava/lang/Object;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mTempWebLoadingErrorMsg:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageListBackup:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageAllAppList:Ljava/util/ArrayList;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mIsSecondPage:Z

    .line 71
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mContext:Landroid/content/Context;

    .line 72
    iput p2, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetSize:I

    .line 73
    return-void
.end method

.method private reloadWidgetData1()V
    .locals 13

    .prologue
    .line 153
    sget-object v10, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    const-string v11, "getPageList() reload 4x1"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v4, 0x0

    .line 160
    .local v4, "mTempBannerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    const-string v11, "curatedWidgetBannerList2Notc"

    const-string v12, ""

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "bannerXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v10

    invoke-virtual {v10}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v7

    .line 163
    .local v7, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v7}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v9

    .line 164
    .local v9, "xr":Lorg/xml/sax/XMLReader;
    new-instance v8, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct {v8}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 166
    .local v8, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-interface {v9, v8}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 167
    new-instance v10, Lorg/xml/sax/InputSource;

    new-instance v11, Ljava/io/StringReader;

    invoke-direct {v11, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v10, v11}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v9, v10}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 170
    invoke-virtual {v8}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 176
    .end local v1    # "bannerXmlCache":Ljava/lang/String;
    .end local v7    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v8    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v9    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    new-instance v6, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v6}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 177
    .local v6, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v10, 0x6

    invoke-virtual {v6, v10}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 180
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 181
    .local v0, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v6, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 183
    .end local v0    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 184
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 189
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v5, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-virtual {p0, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 193
    return-void

    .line 172
    .end local v5    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v6    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_1
    move-exception v10

    goto :goto_0
.end method

.method private reloadWidgetData2()V
    .locals 13

    .prologue
    .line 196
    sget-object v10, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    const-string v11, "getPageList() reload 4x2"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    const/4 v4, 0x0

    .line 203
    .local v4, "mTempBannerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    const-string v11, "curatedWidgetBannerList2Notc"

    const-string v12, ""

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "bannerXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v10

    invoke-virtual {v10}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v7

    .line 206
    .local v7, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v7}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v9

    .line 207
    .local v9, "xr":Lorg/xml/sax/XMLReader;
    new-instance v8, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct {v8}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 209
    .local v8, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-interface {v9, v8}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 210
    new-instance v10, Lorg/xml/sax/InputSource;

    new-instance v11, Ljava/io/StringReader;

    invoke-direct {v11, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v10, v11}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v9, v10}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 213
    invoke-virtual {v8}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 219
    .end local v1    # "bannerXmlCache":Ljava/lang/String;
    .end local v7    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v8    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v9    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    new-instance v6, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v6}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 220
    .local v6, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v10, 0x3

    invoke-virtual {v6, v10}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 223
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 224
    .local v0, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v6, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 226
    .end local v0    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 227
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 232
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v5, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    invoke-virtual {p0, v5}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 236
    return-void

    .line 215
    .end local v5    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v6    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_1
    move-exception v10

    goto :goto_0
.end method

.method private reloadWidgetData4()V
    .locals 15

    .prologue
    .line 239
    sget-object v12, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    const-string v13, "getPageList() reload 4x4"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const/4 v4, 0x0

    .line 241
    .local v4, "mTempFeaturedAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v5, 0x0

    .line 242
    .local v5, "mTempTopAllAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v3, 0x0

    .line 247
    .local v3, "mTempBannerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    :try_start_0
    iget-object v12, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    const-string v13, "promotionBannerListEx2Notc"

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "bannerXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v12

    invoke-virtual {v12}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v9

    .line 249
    .local v9, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v9}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v11

    .line 250
    .local v11, "xr":Lorg/xml/sax/XMLReader;
    new-instance v10, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct {v10}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 252
    .local v10, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-interface {v11, v10}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 253
    new-instance v12, Lorg/xml/sax/InputSource;

    new-instance v13, Ljava/io/StringReader;

    invoke-direct {v13, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v12, v13}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v11, v12}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 255
    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 263
    .end local v1    # "bannerXmlCache":Ljava/lang/String;
    .end local v9    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v10    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v11    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    :try_start_1
    iget-object v12, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    const-string v13, "featuredProductList2Notc"

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "appXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v12

    invoke-virtual {v12}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v9

    .line 266
    .restart local v9    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v9}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v11

    .line 267
    .restart local v11    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v10, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct {v10}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 269
    .local v10, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    invoke-interface {v11, v10}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 270
    new-instance v12, Lorg/xml/sax/InputSource;

    new-instance v13, Ljava/io/StringReader;

    invoke-direct {v13, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v12, v13}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v11, v12}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 273
    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 282
    .end local v0    # "appXmlCache":Ljava/lang/String;
    .end local v9    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v10    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v11    # "xr":Lorg/xml/sax/XMLReader;
    :goto_1
    :try_start_2
    iget-object v12, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    const-string v13, "allProductList2Notc"

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .restart local v0    # "appXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v12

    invoke-virtual {v12}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v9

    .line 285
    .restart local v9    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v9}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v11

    .line 286
    .restart local v11    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v10, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct {v10}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 288
    .restart local v10    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    invoke-interface {v11, v10}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 289
    new-instance v12, Lorg/xml/sax/InputSource;

    new-instance v13, Ljava/io/StringReader;

    invoke-direct {v13, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v12, v13}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v11, v12}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 292
    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v5

    .line 300
    .end local v0    # "appXmlCache":Ljava/lang/String;
    .end local v9    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v10    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v11    # "xr":Lorg/xml/sax/XMLReader;
    :goto_2
    new-instance v7, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 301
    .local v7, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v12, 0x1

    invoke-virtual {v7, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 303
    new-instance v8, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v8}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 304
    .local v8, "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v12, 0x2

    invoke-virtual {v8, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 308
    if-eqz v4, :cond_1

    .line 309
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v2, v12, :cond_1

    .line 310
    const/4 v12, 0x4

    if-ge v2, v12, :cond_0

    .line 311
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v7, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 309
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 318
    .end local v2    # "i":I
    :cond_1
    if-eqz v5, :cond_3

    .line 319
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v2, v12, :cond_3

    .line 320
    const/4 v12, 0x4

    if-ge v2, v12, :cond_2

    .line 321
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual {v8, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 319
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 328
    .end local v2    # "i":I
    :cond_3
    if-eqz v3, :cond_5

    .line 329
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v2, v12, :cond_5

    .line 330
    rem-int/lit8 v12, v2, 0x6

    const/4 v13, 0x4

    if-ge v12, v13, :cond_4

    .line 332
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v7, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 329
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 335
    :cond_4
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    invoke-virtual {v8, v12}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    goto :goto_6

    .line 344
    .end local v2    # "i":I
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v6, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    invoke-virtual {p0, v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 350
    return-void

    .line 294
    .end local v6    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v7    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v8    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_0
    move-exception v12

    goto :goto_2

    .line 275
    :catch_1
    move-exception v12

    goto/16 :goto_1

    .line 257
    :catch_2
    move-exception v12

    goto/16 :goto_0
.end method

.method private reloadWidgetData6()V
    .locals 22

    .prologue
    .line 353
    sget-object v19, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    const-string v20, "getPageList() reload 4x6"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    const/4 v10, 0x0

    .line 355
    .local v10, "mTempFeaturedAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v11, 0x0

    .line 356
    .local v11, "mTempFeaturedBannerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    const/4 v12, 0x0

    .line 357
    .local v12, "mTempLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;>;"
    const/4 v13, 0x0

    .line 358
    .local v13, "mTempLocalBannerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/BannerVo;>;"
    const/4 v9, 0x0

    .line 364
    .local v9, "mTempContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "featuredProductList2Notc"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 366
    .local v2, "appXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 367
    .local v16, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 368
    .local v18, "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 370
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 371
    new-instance v19, Lorg/xml/sax/InputSource;

    new-instance v20, Ljava/io/StringReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v19 .. v20}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v18 .. v19}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 374
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v10

    .line 383
    .end local v2    # "appXmlCache":Ljava/lang/String;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "promotionBannerListEx2Notc"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 384
    .local v3, "bannerXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 385
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 386
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 388
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 389
    new-instance v19, Lorg/xml/sax/InputSource;

    new-instance v20, Ljava/io/StringReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v19 .. v20}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v18 .. v19}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 391
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v11

    .line 399
    .end local v3    # "bannerXmlCache":Ljava/lang/String;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "localFeaturedProductList2Notc"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 401
    .local v6, "localAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 402
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 403
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;-><init>()V

    .line 405
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 406
    new-instance v19, Lorg/xml/sax/InputSource;

    new-instance v20, Ljava/io/StringReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v19 .. v20}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v18 .. v19}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 409
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v12

    .line 418
    .end local v6    # "localAppXmlCache":Ljava/lang/String;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "localBannerList2Notc"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 419
    .local v7, "localBannerXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 420
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 421
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 423
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 424
    new-instance v19, Lorg/xml/sax/InputSource;

    new-instance v20, Ljava/io/StringReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v19 .. v20}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v18 .. v19}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 426
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v13

    .line 434
    .end local v7    # "localBannerXmlCache":Ljava/lang/String;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "contentCategoryProductList2Notc"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 436
    .local v4, "contentGiftsAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 437
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 438
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 440
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 441
    new-instance v19, Lorg/xml/sax/InputSource;

    new-instance v20, Ljava/io/StringReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v19 .. v20}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v18 .. v19}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 444
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v9

    .line 452
    .end local v4    # "contentGiftsAppXmlCache":Ljava/lang/String;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :goto_4
    new-instance v15, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v15}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 453
    .local v15, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 454
    invoke-virtual {v15}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getItemCounts()[I

    move-result-object v8

    .line 458
    .local v8, "mItemCounts":[I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_5
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_1

    .line 459
    const/16 v19, 0x0

    aget v19, v8, v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_0

    .line 460
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 458
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 466
    :cond_1
    const/4 v5, 0x0

    :goto_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_3

    .line 467
    const/16 v19, 0x1

    aget v19, v8, v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_2

    .line 468
    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 466
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 473
    :cond_3
    const/4 v5, 0x0

    :goto_7
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_4

    .line 475
    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmLocalAppList(Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;)V

    .line 473
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 481
    :cond_4
    const/4 v5, 0x0

    :goto_8
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_6

    .line 482
    const/16 v19, 0x3

    aget v19, v8, v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_5

    .line 483
    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmLocalBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 481
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 489
    :cond_6
    const/4 v5, 0x0

    :goto_9
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v5, v0, :cond_7

    .line 491
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmContentGiftsAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 489
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 501
    :cond_7
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 503
    .local v14, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 512
    return-void

    .line 446
    .end local v5    # "i":I
    .end local v8    # "mItemCounts":[I
    .end local v14    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v15    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_0
    move-exception v19

    goto/16 :goto_4

    .line 428
    :catch_1
    move-exception v19

    goto/16 :goto_3

    .line 411
    :catch_2
    move-exception v19

    goto/16 :goto_2

    .line 393
    :catch_3
    move-exception v19

    goto/16 :goto_1

    .line 376
    :catch_4
    move-exception v19

    goto/16 :goto_0
.end method


# virtual methods
.method public addWidgetStateFalg(I)V
    .locals 1
    .param p1, "_addFlag"    # I

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    .line 115
    return-void
.end method

.method public deleteWidgetStateFalg(I)V
    .locals 1
    .param p1, "_delFlag"    # I

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 119
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    .line 121
    :cond_0
    return-void
.end method

.method public getPageAllAppList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageAllAppList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPageList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 128
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadCacheFirst"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetSize:I

    packed-switch v0, :pswitch_data_0

    .line 145
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    return-object v0

    .line 137
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->reloadWidgetData2()V

    goto :goto_0

    .line 140
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->reloadWidgetData2()V

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getWidgetSize()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetSize:I

    return v0
.end method

.method public getWidgetState()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    return v0
.end method

.method public getWidgetStateString()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 84
    invoke-virtual {p0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v0

    if-ne v1, v0, :cond_0

    .line 85
    const-string v0, "WIDGET_STATE_INIT"

    .line 95
    :goto_0
    return-object v0

    .line 86
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v0

    if-ne v1, v0, :cond_1

    .line 87
    const-string v0, "WIDGET_STATE_INIT_LOADING"

    goto :goto_0

    .line 88
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v0

    if-ne v1, v0, :cond_2

    .line 89
    const-string v0, "WIDGET_STATE_IDLE"

    goto :goto_0

    .line 90
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v0

    if-ne v1, v0, :cond_3

    .line 91
    const-string v0, "WIDGET_STATE_IDLE_LOADING"

    goto :goto_0

    .line 92
    :cond_3
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->isWidgetState(I)Z

    move-result v0

    if-ne v1, v0, :cond_4

    .line 93
    const-string v0, "WIDGET_STATE_TERM"

    goto :goto_0

    .line 95
    :cond_4
    const-string v0, "Not supported state!!!"

    goto :goto_0
.end method

.method public getWidgetStateUsedFalg(I)Z
    .locals 1
    .param p1, "_addFlag"    # I

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecondPage()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mIsSecondPage:Z

    return v0
.end method

.method public isWidgetState(I)Z
    .locals 1
    .param p1, "_widgetState"    # I

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized restorePageList()V
    .locals 3

    .prologue
    .line 527
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 528
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageListBackup:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 534
    :goto_0
    monitor-exit p0

    return-void

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 527
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setIsSecondPage(Z)V
    .locals 0
    .param p1, "_isSecondPage"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mIsSecondPage:Z

    return-void
.end method

.method public declared-synchronized setPageAllAppList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p1, "_pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageAllAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageAllAppList:Ljava/util/ArrayList;

    .line 539
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageAllAppList:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    monitor-exit p0

    return-void

    .line 537
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPageList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 516
    .local p1, "_pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageListBackup:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 521
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;

    .line 522
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPageList:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 523
    monitor-exit p0

    return-void

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 516
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setWidgetState(I)V
    .locals 2
    .param p1, "_widgetState"    # I

    .prologue
    .line 108
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mStateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    iput p1, p0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mWidgetState:I

    .line 110
    monitor-exit v1

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateInstalledApp()V
    .locals 37

    .prologue
    .line 543
    sget-object v34, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    const-string v35, "removeInstalledApp 4x2"

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v34, v0

    if-nez v34, :cond_0

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    sget-object v35, Lcom/sec/android/widget/samsungapps/WidgetProvider;->PNAME:Ljava/lang/String;

    const/16 v36, 0x0

    invoke-virtual/range {v34 .. v36}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    .line 551
    :cond_0
    const/16 v22, 0x0

    .line 552
    .local v22, "mTempLocalAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v13, 0x0

    .line 553
    .local v13, "mTempContentGiftsAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v19, 0x0

    .line 554
    .local v19, "mTempLifestyleAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v16, 0x0

    .line 556
    .local v16, "mTempFeaturedAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v23, 0x0

    .line 557
    .local v23, "mTempLocalAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/4 v14, 0x0

    .line 558
    .local v14, "mTempContentGiftsAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v20, 0x0

    .line 559
    .local v20, "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    const/16 v17, 0x0

    .line 564
    .local v17, "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v34, v0

    const-string v35, "localFeaturedProductList2Notc"

    const-string v36, ""

    invoke-interface/range {v34 .. v36}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 566
    .local v12, "localAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 567
    .local v31, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 568
    .local v33, "xr":Lorg/xml/sax/XMLReader;
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 570
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 571
    new-instance v34, Lorg/xml/sax/InputSource;

    new-instance v35, Ljava/io/StringReader;

    move-object/from16 v0, v35

    invoke-direct {v0, v12}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v34 .. v35}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v33 .. v34}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 574
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v22

    .line 575
    new-instance v24, Ljava/util/ArrayList;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    .end local v23    # "mTempLocalAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .local v24, "mTempLocalAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    move-object/from16 v23, v24

    .line 583
    .end local v12    # "localAppXmlCache":Ljava/lang/String;
    .end local v24    # "mTempLocalAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    .restart local v23    # "mTempLocalAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v34, v0

    const-string v35, "contentCategoryProductList2Notc"

    const-string v36, ""

    invoke-interface/range {v34 .. v36}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 585
    .local v4, "contentGiftsAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 586
    .restart local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 587
    .restart local v33    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 589
    .restart local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 590
    new-instance v34, Lorg/xml/sax/InputSource;

    new-instance v35, Ljava/io/StringReader;

    move-object/from16 v0, v35

    invoke-direct {v0, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v34 .. v35}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v33 .. v34}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 593
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v13

    .line 594
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .end local v14    # "mTempContentGiftsAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .local v15, "mTempContentGiftsAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    move-object v14, v15

    .line 602
    .end local v4    # "contentGiftsAppXmlCache":Ljava/lang/String;
    .end local v15    # "mTempContentGiftsAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    .restart local v14    # "mTempContentGiftsAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v34, v0

    const-string v35, "contentCategoryProductList"

    const-string v36, ""

    invoke-interface/range {v34 .. v36}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 604
    .local v11, "lifestyleAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 605
    .restart local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 606
    .restart local v33    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 608
    .restart local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 609
    new-instance v34, Lorg/xml/sax/InputSource;

    new-instance v35, Ljava/io/StringReader;

    move-object/from16 v0, v35

    invoke-direct {v0, v11}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v34 .. v35}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v33 .. v34}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 612
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v19

    .line 613
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .end local v20    # "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .local v21, "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    move-object/from16 v20, v21

    .line 621
    .end local v11    # "lifestyleAppXmlCache":Ljava/lang/String;
    .end local v21    # "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    .restart local v20    # "mTempLifestyleAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mPrefs:Landroid/content/SharedPreferences;

    move-object/from16 v34, v0

    const-string v35, "featuredProductList2Notc"

    const-string v36, ""

    invoke-interface/range {v34 .. v36}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 623
    .local v6, "featuredAppXmlCache":Ljava/lang/String;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 624
    .restart local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 625
    .restart local v33    # "xr":Lorg/xml/sax/XMLReader;
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 627
    .restart local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 628
    new-instance v34, Lorg/xml/sax/InputSource;

    new-instance v35, Ljava/io/StringReader;

    move-object/from16 v0, v35

    invoke-direct {v0, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v34 .. v35}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface/range {v33 .. v34}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 631
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v16

    .line 632
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .end local v17    # "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .local v18, "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    move-object/from16 v17, v18

    .line 640
    .end local v6    # "featuredAppXmlCache":Ljava/lang/String;
    .end local v18    # "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    .restart local v17    # "mTempFeaturedAppList2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/AppVo;>;"
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widget/samsungapps/data/WidgetData;->mContext:Landroid/content/Context;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v34

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v26

    .line 642
    .local v26, "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/content/pm/PackageInfo;

    .line 644
    .local v25, "packageInfo":Landroid/content/pm/PackageInfo;
    const/4 v9, -0x1

    .line 645
    .local v9, "index":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_5
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_2

    .line 646
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1a

    .line 647
    move v9, v7

    .line 651
    :cond_2
    if-ltz v9, :cond_3

    .line 652
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 655
    :cond_3
    const/4 v9, -0x1

    .line 656
    const/4 v7, 0x0

    :goto_6
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_4

    .line 657
    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1b

    .line 658
    move v9, v7

    .line 662
    :cond_4
    if-ltz v9, :cond_5

    .line 663
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 666
    :cond_5
    const/4 v9, -0x1

    .line 667
    const/4 v7, 0x0

    :goto_7
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_6

    .line 668
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1c

    .line 669
    move v9, v7

    .line 673
    :cond_6
    if-ltz v9, :cond_7

    .line 674
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 677
    :cond_7
    const/4 v9, -0x1

    .line 678
    const/4 v7, 0x0

    :goto_8
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_8

    .line 679
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1d

    .line 680
    move v9, v7

    .line 684
    :cond_8
    if-ltz v9, :cond_1

    .line 685
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_4

    .line 687
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "index":I
    .end local v25    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v26    # "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :catch_0
    move-exception v5

    .line 688
    .local v5, "e":Ljava/lang/Exception;
    sget-object v34, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_9
    if-eqz v22, :cond_d

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_d

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_d

    .line 693
    sget-object v34, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "mTempLocalAppList.size() : "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, ", mTempLocalAppList2.size() : "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v34

    rsub-int/lit8 v3, v34, 0x3

    .line 696
    .local v3, "cnt":I
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 697
    .local v2, "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    const/4 v10, 0x0

    .line 698
    .local v10, "isDuplicated":Z
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_9
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_b

    .line 699
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_1e

    .line 700
    const/4 v10, 0x1

    .line 704
    :cond_b
    if-nez v10, :cond_c

    .line 705
    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    add-int/lit8 v3, v3, -0x1

    .line 708
    :cond_c
    if-gtz v3, :cond_a

    .line 713
    .end local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v3    # "cnt":I
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "isDuplicated":Z
    :cond_d
    if-eqz v13, :cond_11

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_11

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_11

    .line 714
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v34

    rsub-int/lit8 v3, v34, 0x3

    .line 716
    .restart local v3    # "cnt":I
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 717
    .restart local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    const/4 v10, 0x0

    .line 718
    .restart local v10    # "isDuplicated":Z
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_a
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_f

    .line 719
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v35

    invoke-virtual {v13, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_1f

    .line 720
    const/4 v10, 0x1

    .line 724
    :cond_f
    if-nez v10, :cond_10

    .line 725
    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 726
    add-int/lit8 v3, v3, -0x1

    .line 728
    :cond_10
    if-gtz v3, :cond_e

    .line 733
    .end local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v3    # "cnt":I
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "isDuplicated":Z
    :cond_11
    if-eqz v19, :cond_15

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_15

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_15

    .line 734
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v34

    rsub-int/lit8 v3, v34, 0x3

    .line 736
    .restart local v3    # "cnt":I
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_12
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_15

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 737
    .restart local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    const/4 v10, 0x0

    .line 738
    .restart local v10    # "isDuplicated":Z
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_b
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_13

    .line 739
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_20

    .line 740
    const/4 v10, 0x1

    .line 744
    :cond_13
    if-nez v10, :cond_14

    .line 745
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    add-int/lit8 v3, v3, -0x1

    .line 748
    :cond_14
    if-gtz v3, :cond_12

    .line 753
    .end local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v3    # "cnt":I
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "isDuplicated":Z
    :cond_15
    if-eqz v16, :cond_19

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_19

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v34

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_19

    .line 754
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v34

    rsub-int/lit8 v3, v34, 0x3

    .line 756
    .restart local v3    # "cnt":I
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_16
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v34

    if-eqz v34, :cond_19

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 757
    .restart local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    const/4 v10, 0x0

    .line 758
    .restart local v10    # "isDuplicated":Z
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_c
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v34

    if-ge v7, v0, :cond_17

    .line 759
    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    invoke-virtual/range {v34 .. v34}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getGUID()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_21

    .line 760
    const/4 v10, 0x1

    .line 764
    :cond_17
    if-nez v10, :cond_18

    .line 765
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 766
    add-int/lit8 v3, v3, -0x1

    .line 768
    :cond_18
    if-gtz v3, :cond_16

    .line 774
    .end local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v3    # "cnt":I
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "isDuplicated":Z
    :cond_19
    new-instance v28, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v28 .. v28}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 775
    .local v28, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/16 v34, 0x3

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 777
    new-instance v29, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v29 .. v29}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 778
    .local v29, "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/16 v34, 0x3

    move-object/from16 v0, v29

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 780
    new-instance v30, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 781
    .local v30, "pageVo3":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/16 v34, 0x3

    move-object/from16 v0, v30

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 785
    const/16 v34, 0x0

    :try_start_5
    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 786
    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 787
    const/16 v34, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 788
    const/16 v34, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 790
    const/16 v34, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 791
    const/16 v34, 0x1

    move/from16 v0, v34

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 792
    const/16 v34, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 793
    const/16 v34, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 795
    const/16 v34, 0x2

    move-object/from16 v0, v22

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 796
    const/16 v34, 0x2

    move/from16 v0, v34

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 797
    const/16 v34, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 798
    const/16 v34, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 807
    :goto_d
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 809
    .local v27, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 813
    return-void

    .line 645
    .end local v27    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v28    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v29    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v30    # "pageVo3":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v7    # "i":I
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "index":I
    .restart local v25    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v26    # "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_1a
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_5

    .line 656
    :cond_1b
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_6

    .line 667
    :cond_1c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_7

    .line 678
    :cond_1d
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_8

    .line 698
    .end local v9    # "index":I
    .end local v25    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v26    # "packageInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .restart local v3    # "cnt":I
    .restart local v10    # "isDuplicated":Z
    :cond_1e
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_9

    .line 718
    :cond_1f
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_a

    .line 738
    :cond_20
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_b

    .line 758
    :cond_21
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_c

    .line 799
    .end local v2    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v3    # "cnt":I
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "isDuplicated":Z
    .restart local v28    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v29    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v30    # "pageVo3":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_1
    move-exception v5

    .line 800
    .restart local v5    # "e":Ljava/lang/Exception;
    sget-object v34, Lcom/sec/android/widget/samsungapps/data/WidgetData;->TAG:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 634
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v28    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v29    # "pageVo2":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v30    # "pageVo3":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_2
    move-exception v34

    goto/16 :goto_3

    .line 615
    :catch_3
    move-exception v34

    goto/16 :goto_2

    .line 596
    :catch_4
    move-exception v34

    goto/16 :goto_1

    .line 577
    :catch_5
    move-exception v34

    goto/16 :goto_0
.end method
