.class public final Lcom/sec/android/widget/samsungapps/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final CATEGORY_EDUCATION:I = 0x7f0a0004

.field public static final CATEGORY_FINANCE:I = 0x7f0a0005

.field public static final CATEGORY_GAMES:I = 0x7f0a0003

.field public static final CATEGORY_HEALTH:I = 0x7f0a0002

.field public static final CATEGORY_NAVIGATION:I = 0x7f0a0007

.field public static final CATEGORY_NEWS:I = 0x7f0a0008

.field public static final CATEGORY_WEATHER:I = 0x7f0a0006

.field public static final IDS_ASCAST_BODY_TAP_TO_START_ABB:I = 0x7f0a001b

.field public static final IDS_CHATON_BUTTON_SEE_MORE:I = 0x7f0a000b

.field public static final IDS_PN_POP_THE_DEVICE_WILL_USE_DATA_THROUGH_THE_CURRENTLY_CONNECTED_WLAN_NETWORK_CHN:I = 0x7f0a002e

.field public static final IDS_SAPPS_BODY_CONNECTION_FAILED:I = 0x7f0a0028

.field public static final IDS_SAPPS_BODY_ERROR:I = 0x7f0a002a

.field public static final IDS_SAPPS_BODY_ESSENTIAL_APPS_RECOMMENDED_FOR_YOU:I = 0x7f0a000c

.field public static final IDS_SAPPS_BODY_FEATURED_COLLECTION:I = 0x7f0a0011

.field public static final IDS_SAPPS_BODY_GET_MORE:I = 0x7f0a002c

.field public static final IDS_SAPPS_BODY_INSTALLED_M_NOUN:I = 0x7f0a002d

.field public static final IDS_SAPPS_BODY_LOADING_ING:I = 0x7f0a000f

.field public static final IDS_SAPPS_BODY_NEWLY_UPDATED_IN_PS:I = 0x7f0a002b

.field public static final IDS_SAPPS_BODY_NO_DATA:I = 0x7f0a0029

.field public static final IDS_SAPPS_BODY_PREMIUM_CHOICE:I = 0x7f0a0010

.field public static final IDS_SAPPS_BODY_PS_IS_A_RECOMMENDATION_SERVICE_THAT_YOU_CAN_USE_ON_YOUR_SAMSUNG_SMARTPHONE_AND_TABLET_TO_EASILY_FIND_THE_APPS_YOU_WANT_MSG:I = 0x7f0a001d

.field public static final IDS_SAPPS_BODY_PS_WIDGET:I = 0x7f0a0001

.field public static final IDS_SAPPS_BODY_SEARCH_SAMSUNG_APPS:I = 0x7f0a001e

.field public static final IDS_SAPPS_BODY_TOP_FREE:I = 0x7f0a0013

.field public static final IDS_SAPPS_BODY_TOP_NEW:I = 0x7f0a0012

.field public static final IDS_SAPPS_BODY_TOP_PAID:I = 0x7f0a0014

.field public static final IDS_SAPPS_BODY_UNABLE_TO_FIND_YOUR_LOCATION_CHECK_YOUR_NETWORK_SETTINGS_AND_TRY_AGAIN:I = 0x7f0a0027

.field public static final IDS_SAPPS_BODY_UTILITIES:I = 0x7f0a0017

.field public static final IDS_SAPPS_BUTTON_FREE_M_NO_PAY:I = 0x7f0a0019

.field public static final IDS_SAPPS_BUTTON_GAME:I = 0x7f0a0018

.field public static final IDS_SAPPS_HEADER_CONNECT_TO_WI_FI:I = 0x7f0a002f

.field public static final IDS_SAPPS_HEADER_GALAXY_APPS_HOME:I = 0x7f0a0009

.field public static final IDS_SAPPS_HEADER_GALAXY_ESSENTIALS:I = 0x7f0a0035

.field public static final IDS_SAPPS_HEADER_GALAXY_ESSENTIALS_HOME:I = 0x7f0a0033

.field public static final IDS_SAPPS_HEADER_GALAXY_GIFTS:I = 0x7f0a0036

.field public static final IDS_SAPPS_HEADER_GALAXY_GIFTS_HOME:I = 0x7f0a0034

.field public static final IDS_SAPPS_HEADER_SAMSUNG_APPS:I = 0x7f0a000a

.field public static final IDS_SAPPS_HEADER_SAMSUNG_APPS_WIDGET:I = 0x7f0a0000

.field public static final IDS_SAPPS_HEADER_SELECT_CATEGORY_ABB:I = 0x7f0a0016

.field public static final IDS_SAPPS_OPT2_SETTINGS:I = 0x7f0a0021

.field public static final IDS_SAPPS_POP_CONNECTING_TO_MOBILE_NETWORKS_MAY_INCUR_ADDITIONAL_CHARGES:I = 0x7f0a000d

.field public static final IDS_SAPPS_POP_DOWNLOAD_FAILED_DUE_TO_NETWORK_ERROR:I = 0x7f0a0023

.field public static final IDS_SAPPS_POP_DO_NOT_SHOW_AGAIN:I = 0x7f0a000e

.field public static final IDS_SAPPS_POP_NETWORK_ERROR_OCCURRED_TRY_LATER:I = 0x7f0a0024

.field public static final IDS_SAPPS_POP_NETWORK_UNAVAILABLE:I = 0x7f0a0025

.field public static final IDS_SAPPS_POP_READ_AND_ACCEPT_ALL_TERMS_TO_PROCEED:I = 0x7f0a001a

.field public static final IDS_SAPPS_POP_SERVICE_UNAVAILABLE:I = 0x7f0a0022

.field public static final IDS_SAPPS_POP_UNABLE_TO_COMPLETE_DOWNLOAD_TRY_AGAIN_LATER:I = 0x7f0a0026

.field public static final IDS_SAPPS_SK_NEXT_ABB:I = 0x7f0a0020

.field public static final IDS_SAPPS_SK_UPDATE:I = 0x7f0a001f

.field public static final IDS_SAPPS_TAB_CATEGORY:I = 0x7f0a0015

.field public static final IDS_WGT_MBODY_TAP_TO_GET_STARTED:I = 0x7f0a001c

.field public static final MIDS_SAPPS_BODY_APPS_FOR_NPROFESSIONALS_M_WIDGET_NAME:I = 0x7f0a0031

.field public static final MIDS_SAPPS_BODY_COLOUR_THE_EDGE_OF_YOUR_GALAXY_SCREEN:I = 0x7f0a0038

.field public static final MIDS_SAPPS_BODY_ENJOY_EXCLUSIVE_GIFTS_FOR_YOUR_GALAXY:I = 0x7f0a0037

.field public static final MIDS_SAPPS_BODY_GALAXY_NESSENTIALS_M_WIDGET_NAME:I = 0x7f0a0030

.field public static final MIDS_SAPPS_BODY_GALAXY_NGIFTS_M_WIDGET_NAME:I = 0x7f0a0032


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
