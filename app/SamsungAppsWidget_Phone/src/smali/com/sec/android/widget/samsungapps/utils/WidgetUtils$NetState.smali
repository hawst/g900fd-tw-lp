.class public final enum Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;
.super Ljava/lang/Enum;
.source "WidgetUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum BLUETOOTH:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum BLUETOOTH_TETHER:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum CELLULAR:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum MOBILE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum NOT_AVAILABLE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum WIFI:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

.field public static final enum WIFI_P2P:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 562
    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->MOBILE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "BLUETOOTH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "BLUETOOTH_TETHER"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH_TETHER:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "WIFI_P2P"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI_P2P:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "CELLULAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->CELLULAR:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    new-instance v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    const-string v1, "NOT_AVAILABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->NOT_AVAILABLE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    .line 561
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->MOBILE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->BLUETOOTH_TETHER:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->WIFI_P2P:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->CELLULAR:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->NOT_AVAILABLE:Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->$VALUES:[Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 561
    const-class v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;
    .locals 1

    .prologue
    .line 561
    sget-object v0, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->$VALUES:[Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    invoke-virtual {v0}, [Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/widget/samsungapps/utils/WidgetUtils$NetState;

    return-object v0
.end method
