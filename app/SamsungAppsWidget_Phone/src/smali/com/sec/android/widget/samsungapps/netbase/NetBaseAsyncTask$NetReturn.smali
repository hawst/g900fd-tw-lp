.class public Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
.super Ljava/lang/Object;
.source "NetBaseAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NetReturn"
.end annotation


# instance fields
.field public mErrNo:I

.field public mException:Ljava/lang/Exception;

.field public mReturnStr:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 338
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->this$0:Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 340
    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 341
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    return-void
.end method


# virtual methods
.method public getErrString()I
    .locals 2

    .prologue
    const v0, 0x7f0a0026

    .line 344
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    if-nez v1, :cond_1

    .line 345
    const v0, 0x7f0a002a

    .line 356
    :cond_0
    :goto_0
    return v0

    .line 346
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Ljava/net/SocketTimeoutException;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Ljava/net/UnknownHostException;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Ljava/net/SocketException;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Ljava/net/ConnectException;

    if-eqz v1, :cond_3

    .line 350
    :cond_2
    const v0, 0x7f0a0024

    goto :goto_0

    .line 351
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Lcom/sec/android/widget/samsungapps/utils/UserException;

    if-nez v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    instance-of v1, v1, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    if-eqz v1, :cond_0

    .line 354
    const v0, 0x7f0a0029

    goto :goto_0
.end method
