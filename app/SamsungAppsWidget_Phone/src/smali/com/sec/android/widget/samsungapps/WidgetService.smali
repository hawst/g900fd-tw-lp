.class public Lcom/sec/android/widget/samsungapps/WidgetService;
.super Landroid/widget/RemoteViewsService;
.source "WidgetService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field static final m4x2LayoutIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/widget/samsungapps/WidgetService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetService;->TAG:Ljava/lang/String;

    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f030002

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/widget/samsungapps/WidgetService;->m4x2LayoutIds:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    .line 61
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/widget/samsungapps/WidgetService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 2
    .param p1, "_intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    new-instance v0, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/WidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/widget/samsungapps/WidgetService$ViewFactory;-><init>(Lcom/sec/android/widget/samsungapps/WidgetService;Landroid/content/Context;Landroid/content/Intent;)V

    return-object v0
.end method
