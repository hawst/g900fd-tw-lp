.class public Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "CountrySearchExSAXHandler.java"


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;",
            ">;"
        }
    .end annotation
.end field

.field private mNode:Ljava/lang/String;

.field private mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

.field private mValue:Ljava/lang/StringBuffer;

.field private mValueType:Ljava/lang/String;

.field private mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValue:Ljava/lang/StringBuffer;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mList:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    .line 23
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-void
.end method

.method private getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "_attr"    # Lorg/xml/sax/Attributes;
    .param p2, "_attrName"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 162
    const-string v1, ""

    .line 164
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 157
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 106
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "trimedValue":Ljava/lang/String;
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    const-string v1, "countryURL"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 115
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setCountryURL(Ljava/lang/String;)V

    .line 144
    :cond_0
    :goto_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    .line 148
    :cond_1
    const-string v1, "list"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_2
    return-void

    .line 116
    :cond_3
    const-string v1, "MCC"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setMcc(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_4
    const-string v1, "countryCode"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 119
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setCountryCode(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_5
    const-string v1, "freeStoreClsf"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 121
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setFreeStoreClsf(Ljava/lang/String;)V

    goto :goto_0

    .line 122
    :cond_6
    const-string v1, "description"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 123
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_7
    const-string v1, "currencyUnitPrecedes"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 125
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setCurrencyUnitPrecedes(Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_8
    const-string v1, "currencyUnitHasPenny"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 127
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setCurrencyUnitHasPenny(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_9
    const-string v1, "offset"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 129
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setOffset(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 130
    :cond_a
    const-string v1, "freetabClsf"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 131
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setFreetabClsf(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    :cond_b
    const-string v1, "snsVal"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 133
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setSnsVal(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :cond_c
    const-string v1, "currencyUnitDivision"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 135
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setCurrencyUnitDivision(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 136
    :cond_d
    const-string v1, "webWidgetClsf"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 137
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setWebWidgetClsf(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    :cond_e
    const-string v1, "widgetTemplateClsf"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setWidgetTemplateClsf(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getParsedErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParsedList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-object v0
.end method

.method public getTotalCount()J
    .locals 4

    .prologue
    .line 30
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getTotalCount()Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "strTotalCount":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 34
    .local v2, "totalCount":J
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 39
    :goto_0
    return-wide v2

    .line 35
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mList:Ljava/util/ArrayList;

    .line 53
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    .line 54
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    .line 55
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 60
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    .line 61
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValue:Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 63
    const-string v4, "list"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 66
    new-instance v4, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-direct {v4}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    .line 71
    const-string v4, "numValue"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "strNumValue":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 75
    .local v2, "numValue":J
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 80
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->setNumValue(J)V

    .line 101
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_0
    :goto_1
    return-void

    .line 76
    .restart local v1    # "strNumValue":Ljava/lang/String;
    .restart local v2    # "numValue":J
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 82
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_1
    const-string v4, "value"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 85
    const-string v4, "name"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mValueType:Ljava/lang/String;

    goto :goto_1

    .line 87
    :cond_2
    const-string v4, "response"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 88
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "id"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setId(Ljava/lang/String;)V

    .line 89
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "name"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setName(Ljava/lang/String;)V

    .line 90
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "returnCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setReturnCode(Ljava/lang/String;)V

    .line 91
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "startNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setStartNum(Ljava/lang/String;)V

    .line 92
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "endNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setEndNum(Ljava/lang/String;)V

    .line 93
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "transactionId"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTransactionId(Ljava/lang/String;)V

    goto :goto_1

    .line 94
    :cond_3
    const-string v4, "SamsungProtocol"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 95
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "deviceModel"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setDeviceModel(Ljava/lang/String;)V

    .line 96
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "lang"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setLang(Ljava/lang/String;)V

    .line 97
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "version"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 98
    :cond_4
    const-string v4, "errorString"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "errorCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/CountrySearchExSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
