.class public Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;
.super Lcom/sec/android/widget/samsungapps/vo/AppVo;
.source "LocalAppVo.java"


# instance fields
.field private linkCategoryID:Ljava/lang/String;

.field private listTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/vo/AppVo;-><init>()V

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->listTitle:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->linkCategoryID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getLinkCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->linkCategoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->listTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setLinkCategoryID(Ljava/lang/String;)V
    .locals 0
    .param p1, "linkCategoryID"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->linkCategoryID:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setListTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "listTitle"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->listTitle:Ljava/lang/String;

    .line 20
    return-void
.end method
