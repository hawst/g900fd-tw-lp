.class public Lcom/sec/android/widget/samsungapps/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cropBitmap(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "_bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "ratio"    # F

    .prologue
    .line 77
    if-nez p0, :cond_0

    .line 78
    const/4 p0, 0x0

    .line 102
    .end local p0    # "_bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object p0

    .line 82
    .restart local p0    # "_bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 83
    .local v6, "w":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 86
    .local v3, "h":I
    int-to-float v7, v6

    mul-float/2addr v7, p1

    float-to-int v1, v7

    .line 87
    .local v1, "ex":I
    int-to-float v7, v3

    mul-float/2addr v7, p1

    float-to-int v2, v7

    .line 90
    .local v2, "ey":I
    sub-int v7, v6, v1

    div-int/lit8 v4, v7, 0x2

    .line 91
    .local v4, "sx":I
    sub-int v7, v3, v2

    div-int/lit8 v5, v7, 0x2

    .line 93
    .local v5, "sy":I
    invoke-static {p0, v4, v5, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p0

    goto :goto_0

    .line 94
    .end local v1    # "ex":I
    .end local v2    # "ey":I
    .end local v3    # "h":I
    .end local v4    # "sx":I
    .end local v5    # "sy":I
    .end local v6    # "w":I
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 96
    const/4 p0, 0x0

    .line 100
    goto :goto_0

    .line 97
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 99
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static getResizedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "newWidth"    # I
    .param p2, "newHeight"    # I

    .prologue
    .line 107
    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 109
    .local v0, "resizedBitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static getRoundedCornerBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v0, 0x100

    .line 70
    invoke-static {p1, v0, v0}, Lcom/sec/android/widget/samsungapps/utils/Utils;->getResizedBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    const v1, 0x3f666666    # 0.9f

    invoke-static {v0, v1}, Lcom/sec/android/widget/samsungapps/utils/Utils;->cropBitmap(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {p0, v0, v1}, Lcom/sec/android/widget/samsungapps/utils/Utils;->getRoundedCornerBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static getRoundedCornerBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "_bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "_radius"    # F

    .prologue
    .line 27
    const/4 v4, 0x0

    .line 30
    .local v4, "output":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 33
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 34
    .local v0, "canvas":Landroid/graphics/Canvas;
    const v1, -0xbdbdbe

    .line 35
    .local v1, "color":I
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 36
    .local v5, "paint":Landroid/graphics/Paint;
    new-instance v6, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    invoke-direct {v6, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 39
    .local v6, "rect":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 41
    .local v7, "rectF":Landroid/graphics/RectF;
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 42
    invoke-virtual {v0, v7, p2, p2, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 44
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 45
    const v8, -0xbdbdbe

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 47
    invoke-virtual {v0, p1, v6, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 56
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "color":I
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "rect":Landroid/graphics/Rect;
    .end local v7    # "rectF":Landroid/graphics/RectF;
    :goto_0
    return-object v4

    .line 48
    :catch_0
    move-exception v3

    .line 49
    .local v3, "er":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 50
    move-object v4, p1

    .line 54
    goto :goto_0

    .line 51
    .end local v3    # "er":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v2

    .line 52
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 53
    move-object v4, p1

    goto :goto_0
.end method
