.class public Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
.super Ljava/lang/Object;
.source "ImageFileCache.java"


# static fields
.field private static final MAX_REPEAT_COUNT:I = 0x3

.field private static final REAL_URL:Ljava/lang/String; = "http://img.samsungapps.com"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

.field private mCacheDirName:Ljava/lang/String;

.field private mCachingPeriod:J

.field private mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

.field private mContext:Landroid/content/Context;

.field private mDirectory:Ljava/io/File;

.field private mHash:Ljava/security/MessageDigest;

.field private mImageQuality:I

.field private mNetConnectTimeOut:I

.field private mNetReadTimeOut:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "_context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0x2710

    const/4 v4, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mContext:Landroid/content/Context;

    .line 30
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCacheDirName:Ljava/lang/String;

    .line 31
    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    .line 33
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

    .line 34
    const/16 v1, 0x64

    iput v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mImageQuality:I

    .line 35
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    .line 37
    iput v2, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetConnectTimeOut:I

    .line 38
    iput v2, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetReadTimeOut:I

    .line 39
    const-wide/32 v2, 0x5265c00

    iput-wide v2, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCachingPeriod:J

    .line 41
    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mHash:Ljava/security/MessageDigest;

    .line 56
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mContext:Landroid/content/Context;

    .line 57
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCacheDirName:Ljava/lang/String;

    .line 61
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 62
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    const/16 v2, 0x4000

    new-array v2, v2, [B

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 67
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCacheDirName:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    .line 69
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :cond_0
    :goto_0
    :try_start_1
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mHash:Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 87
    :goto_1
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 85
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1
.end method

.method private deleteAllCachedFile()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->DeleteRecursive(Ljava/io/File;)V

    .line 385
    :goto_0
    return-void

    .line 383
    :cond_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    const-string v1, "Cache Directory is not founded..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method DeleteRecursive(Ljava/io/File;)V
    .locals 12
    .param p1, "_fileOrDirectory"    # Ljava/io/File;

    .prologue
    .line 390
    const/4 v5, 0x1

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-ne v5, v6, :cond_0

    .line 392
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 394
    .local v2, "file":Ljava/io/File;
    invoke-virtual {p0, v2}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->DeleteRecursive(Ljava/io/File;)V

    .line 392
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 398
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 400
    .local v1, "currentTime":Ljava/lang/Long;
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCachingPeriod:J

    sub-long/2addr v8, v10

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 402
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 404
    :cond_1
    return-void
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "_imgUri"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 167
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v3, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mschoi _imgUri  :   "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 170
    if-nez v0, :cond_3

    .line 174
    const/4 v2, 0x0

    .line 177
    .local v2, "repeatCount":I
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->saveToCache(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_1

    .line 184
    sget-object v3, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Saving : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 189
    if-nez v0, :cond_2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 205
    .end local v2    # "repeatCount":I
    :cond_2
    :goto_0
    return-object v0

    .line 196
    :cond_3
    sget-object v3, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Caching : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 200
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getCompressFormat()Landroid/graphics/Bitmap$CompressFormat;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

    return-object v0
.end method

.method public getFileName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "_imgUri"    # Landroid/net/Uri;

    .prologue
    .line 105
    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "_imgUri"    # Ljava/lang/String;

    .prologue
    .line 131
    const-string v3, "ImageFileCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mschoi real _imgUri   :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-boolean v3, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    if-eqz v3, :cond_0

    .line 134
    const-string v3, "http://img.samsungapps.com"

    sget-object v4, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingImgHostUrl:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 136
    :cond_0
    const-string v3, "ImageFileCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mschoi change _imgUri   :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mHash:Ljava/security/MessageDigest;

    monitor-enter v4

    .line 139
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mHash:Ljava/security/MessageDigest;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mHash:Ljava/security/MessageDigest;

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 141
    .local v2, "messageDigest":[B
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    new-instance v1, Ljava/math/BigInteger;

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 144
    .local v1, "md5":Ljava/math/BigInteger;
    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "filename":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    :cond_1
    return-object v0

    .line 141
    .end local v0    # "filename":Ljava/lang/String;
    .end local v1    # "md5":Ljava/math/BigInteger;
    .end local v2    # "messageDigest":[B
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public getFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "_strImgUri"    # Ljava/lang/String;

    .prologue
    .line 217
    const-string v4, ""

    .line 219
    .local v4, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_0

    const-string v6, ""

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 221
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 224
    :cond_0
    const/4 v2, 0x0

    .line 225
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 229
    .local v5, "resultBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .local v3, "fileInputStream":Ljava/io/FileInputStream;
    if-eqz v3, :cond_1

    .line 234
    const/4 v6, 0x0

    :try_start_1
    iget-object v7, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v6, v7}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 256
    :cond_1
    if-eqz v3, :cond_2

    .line 258
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v2, v3

    .line 266
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_3
    :goto_0
    return-object v5

    .line 261
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    .line 265
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 239
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 241
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    sget-object v6, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NotFound : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 242
    const/4 v5, 0x0

    .line 256
    if-eqz v2, :cond_3

    .line 258
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 261
    :catch_2
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 246
    .local v1, "err":Ljava/lang/OutOfMemoryError;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 256
    if-eqz v2, :cond_3

    .line 258
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 261
    :catch_4
    move-exception v0

    .line 263
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 248
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "err":Ljava/lang/OutOfMemoryError;
    :catch_5
    move-exception v0

    .line 250
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 256
    if-eqz v2, :cond_3

    .line 258
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_0

    .line 261
    :catch_6
    move-exception v0

    .line 263
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 254
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 256
    :goto_4
    if-eqz v2, :cond_4

    .line 258
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 264
    :cond_4
    :goto_5
    throw v6

    .line 261
    :catch_7
    move-exception v0

    .line 263
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 254
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 248
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v0

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 244
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v1

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 239
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_a
    move-exception v0

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public getFromCache(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "_strUrl"    # Ljava/lang/String;
    .param p2, "_width"    # I
    .param p3, "_height"    # I

    .prologue
    const/4 v8, 0x1

    .line 273
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 275
    .local v5, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 276
    .local v3, "fileInputStream":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 277
    .local v7, "resultBitmap":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 279
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 280
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    iput v8, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 281
    const/16 v8, 0x4000

    new-array v8, v8, [B

    iput-object v8, v6, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 285
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    iget-object v9, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-direct {v8, v9, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .local v4, "fileInputStream":Ljava/io/FileInputStream;
    if-eqz v4, :cond_0

    .line 289
    const/4 v8, 0x0

    :try_start_1
    invoke-static {v4, v8, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 294
    :cond_0
    const/4 v8, 0x1

    invoke-static {v7, p2, p3, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 316
    if-eqz v4, :cond_1

    .line 318
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 326
    :cond_1
    :goto_0
    if-eqz v7, :cond_8

    .line 328
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    move-object v3, v4

    .line 332
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    return-object v0

    .line 321
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 323
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 299
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v1

    .line 301
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    sget-object v8, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " not found in Cache!"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 302
    const/4 v7, 0x0

    .line 316
    if-eqz v3, :cond_3

    .line 318
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 326
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_3
    if-eqz v7, :cond_2

    .line 328
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 321
    .restart local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 323
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 304
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 306
    .local v2, "err":Ljava/lang/OutOfMemoryError;
    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 316
    if-eqz v3, :cond_4

    .line 318
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 326
    :cond_4
    :goto_5
    if-eqz v7, :cond_2

    .line 328
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 321
    :catch_4
    move-exception v1

    .line 323
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 308
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "err":Ljava/lang/OutOfMemoryError;
    :catch_5
    move-exception v1

    .line 310
    .restart local v1    # "e":Ljava/lang/Exception;
    :goto_6
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 316
    if-eqz v3, :cond_5

    .line 318
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 326
    :cond_5
    :goto_7
    if-eqz v7, :cond_2

    .line 328
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 321
    :catch_6
    move-exception v1

    .line 323
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 314
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    .line 316
    :goto_8
    if-eqz v3, :cond_6

    .line 318
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 326
    :cond_6
    :goto_9
    if-eqz v7, :cond_7

    .line 328
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    :cond_7
    throw v8

    .line 321
    :catch_7
    move-exception v1

    .line 323
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 314
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_8

    .line 308
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v1

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_6

    .line 304
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_9
    move-exception v2

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 299
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_a
    move-exception v1

    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "fileInputStream":Ljava/io/FileInputStream;
    :cond_8
    move-object v3, v4

    .end local v4    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public makeFileOutputStream(Ljava/lang/String;)Ljava/io/FileOutputStream;
    .locals 6
    .param p1, "_strUrl"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 343
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    .local v2, "fileName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mDirectory:Ljava/io/File;

    invoke-direct {v0, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 349
    .local v0, "cacheFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 350
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    const/4 v0, 0x0

    :goto_0
    return-object v3

    .line 352
    :catch_0
    move-exception v1

    .line 354
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v3, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    const-string v5, "FileOutputStream IOException occurred..."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->deleteAllCachedFile()V

    .line 370
    const/4 v0, 0x0

    move-object v3, v4

    goto :goto_0

    .line 363
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 365
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 370
    const/4 v0, 0x0

    move-object v3, v4

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    const/4 v0, 0x0

    throw v3
.end method

.method public saveToCache(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .param p1, "_inputStream"    # Ljava/io/InputStream;
    .param p2, "_strUrl"    # Ljava/lang/String;

    .prologue
    .line 475
    const/4 v3, 0x0

    .line 479
    .local v3, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->makeFileOutputStream(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 480
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 482
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

    iget v5, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mImageQuality:I

    invoke-virtual {v0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 485
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    if-eqz p1, :cond_0

    .line 501
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 504
    :cond_0
    if-eqz v3, :cond_1

    .line 506
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 514
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-void

    .line 509
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 511
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 487
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 489
    .local v2, "err":Ljava/lang/OutOfMemoryError;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 499
    if-eqz p1, :cond_2

    .line 501
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 504
    :cond_2
    if-eqz v3, :cond_1

    .line 506
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 509
    :catch_2
    move-exception v1

    .line 511
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 491
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "err":Ljava/lang/OutOfMemoryError;
    :catch_3
    move-exception v1

    .line 493
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 499
    if-eqz p1, :cond_3

    .line 501
    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 504
    :cond_3
    if-eqz v3, :cond_1

    .line 506
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 509
    :catch_4
    move-exception v1

    .line 511
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 497
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 499
    if-eqz p1, :cond_4

    .line 501
    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 504
    :cond_4
    if-eqz v3, :cond_5

    .line 506
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 512
    :cond_5
    :goto_1
    throw v4

    .line 509
    :catch_5
    move-exception v1

    .line 511
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public saveToCache(Ljava/lang/String;)V
    .locals 8
    .param p1, "_url"    # Ljava/lang/String;

    .prologue
    .line 413
    const/4 v5, 0x0

    .line 414
    .local v5, "inputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 418
    .local v4, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 419
    .local v1, "conn":Ljava/net/URLConnection;
    iget v6, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetConnectTimeOut:I

    invoke-virtual {v1, v6}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 420
    iget v6, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetReadTimeOut:I

    invoke-virtual {v1, v6}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 421
    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/net/URLConnection;->setUseCaches(Z)V

    .line 422
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 423
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 424
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->makeFileOutputStream(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 426
    if-eqz v0, :cond_0

    .line 428
    iget-object v6, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

    iget v7, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mImageQuality:I

    invoke-virtual {v0, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    :cond_0
    if-eqz v5, :cond_1

    .line 452
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 455
    :cond_1
    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 465
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "conn":Ljava/net/URLConnection;
    :cond_2
    :goto_0
    return-void

    .line 460
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "conn":Ljava/net/URLConnection;
    :catch_0
    move-exception v2

    .line 462
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 434
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "conn":Ljava/net/URLConnection;
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 436
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-string v6, "ImageCache saveToCache"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 450
    if-eqz v5, :cond_3

    .line 452
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 455
    :cond_3
    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 460
    :catch_2
    move-exception v2

    .line 462
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 438
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v3

    .line 440
    .local v3, "err":Ljava/lang/OutOfMemoryError;
    :try_start_4
    sget-object v6, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->TAG:Ljava/lang/String;

    const-string v7, "saveToCache OutOfMemoryError occurred"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 450
    if-eqz v5, :cond_4

    .line 452
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 455
    :cond_4
    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 460
    :catch_4
    move-exception v2

    .line 462
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 442
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "err":Ljava/lang/OutOfMemoryError;
    :catch_5
    move-exception v2

    .line 444
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 450
    if-eqz v5, :cond_5

    .line 452
    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 455
    :cond_5
    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_0

    .line 460
    :catch_6
    move-exception v2

    .line 462
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 448
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 450
    if-eqz v5, :cond_6

    .line 452
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 455
    :cond_6
    if-eqz v4, :cond_7

    .line 457
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 463
    :cond_7
    :goto_1
    throw v6

    .line 460
    :catch_7
    move-exception v2

    .line 462
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setBitmapOptions(Landroid/graphics/BitmapFactory$Options;)V
    .locals 0
    .param p1, "_options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    .line 100
    return-void
.end method

.method public setCompressFormat(Landroid/graphics/Bitmap$CompressFormat;)V
    .locals 0
    .param p1, "_format"    # Landroid/graphics/Bitmap$CompressFormat;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mCompressFormat:Landroid/graphics/Bitmap$CompressFormat;

    .line 119
    return-void
.end method

.method public setNetworkTimeOut(II)V
    .locals 0
    .param p1, "_connTimeout"    # I
    .param p2, "_readTimeout"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetConnectTimeOut:I

    .line 93
    iput p2, p0, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->mNetReadTimeOut:I

    .line 94
    return-void
.end method
