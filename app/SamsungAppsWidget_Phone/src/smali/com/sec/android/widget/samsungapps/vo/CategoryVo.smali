.class public Lcom/sec/android/widget/samsungapps/vo/CategoryVo;
.super Ljava/lang/Object;
.source "CategoryVo.java"


# instance fields
.field private mContentCategoryID:Ljava/lang/String;

.field private mNumValue:J

.field private mProductCategoryID:Ljava/lang/String;

.field private mProductCategoryName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mNumValue:J

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mContentCategoryID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryID:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getContentCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mContentCategoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getNumValue()J
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mNumValue:J

    return-wide v0
.end method

.method public getProductCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public setContentCategoryID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mContentCategoryID"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mContentCategoryID:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setNumValue(J)V
    .locals 1
    .param p1, "_totalNum"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mNumValue:J

    .line 17
    return-void
.end method

.method public setProductCategoryID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mProductCategoryIDe"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryID:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setProductCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mProductCategoryName"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CategoryVo;->mProductCategoryName:Ljava/lang/String;

    .line 41
    return-void
.end method
