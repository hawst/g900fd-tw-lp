.class public Lcom/sec/android/widget/samsungapps/vo/AppVo;
.super Ljava/lang/Object;
.source "AppVo.java"


# instance fields
.field private mAverageRating:Ljava/lang/String;

.field private mCategoryID:Ljava/lang/String;

.field private mCategoryID2:Ljava/lang/String;

.field private mCategoryName:Ljava/lang/String;

.field private mCategoryName2:Ljava/lang/String;

.field private mContentType:Ljava/lang/String;

.field private mCurrencyUnit:Ljava/lang/String;

.field private mDate:Ljava/lang/String;

.field private mDiscountFlag:Ljava/lang/String;

.field private mDiscountPrice:Ljava/lang/String;

.field private mGUID:Ljava/lang/String;

.field private mIconBitmap:Landroid/graphics/Bitmap;

.field private mInstallSize:Ljava/lang/String;

.field private mIsIconLoaded:Z

.field private mLinkCategoryID:Ljava/lang/String;

.field private mListTitle:Ljava/lang/String;

.field private mNumValue:J

.field private mPrice:Ljava/lang/String;

.field private mProductID:Ljava/lang/String;

.field private mProductImgUrl:Ljava/lang/String;

.field private mProductName:Ljava/lang/String;

.field private mRealContentSize:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;

.field private mVersionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mNumValue:J

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductID:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductName:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID2:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName2:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductImgUrl:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCurrencyUnit:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mPrice:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountPrice:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountFlag:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mAverageRating:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDate:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mContentType:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mGUID:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersion:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersionCode:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mInstallSize:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mRealContentSize:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mLinkCategoryID:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mListTitle:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIsIconLoaded:Z

    return-void
.end method


# virtual methods
.method public getAverageRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mAverageRating:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryID2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID2:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryName2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName2:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCurrencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountFlag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountFlag:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getIconBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIconBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getInstallSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mInstallSize:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mLinkCategoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mListTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getNumValue()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mNumValue:J

    return-wide v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mPrice:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductName:Ljava/lang/String;

    return-object v0
.end method

.method public getRealContentSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mRealContentSize:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersionCode:Ljava/lang/String;

    return-object v0
.end method

.method public isIconLoaded()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIsIconLoaded:Z

    return v0
.end method

.method public setAverageRating(Ljava/lang/String;)V
    .locals 0
    .param p1, "_averageRating"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mAverageRating:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setCategoryID(Ljava/lang/String;)V
    .locals 0
    .param p1, "_categoryID"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setCategoryID2(Ljava/lang/String;)V
    .locals 0
    .param p1, "_categoryID2"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryID2:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setCategoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "_categoryName"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setCategoryName2(Ljava/lang/String;)V
    .locals 0
    .param p1, "_categoryName2"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCategoryName2:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "_contentType"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mContentType:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public setCurrencyUnit(Ljava/lang/String;)V
    .locals 0
    .param p1, "_currencyUnit"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mCurrencyUnit:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "_date"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDate:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setDiscountFlag(Ljava/lang/String;)V
    .locals 0
    .param p1, "_discountFlag"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountFlag:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public setDiscountPrice(Ljava/lang/String;)V
    .locals 0
    .param p1, "_discountPrice"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mDiscountPrice:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setGUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "_gUID"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mGUID:Ljava/lang/String;

    .line 212
    return-void
.end method

.method public setIconBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "_iconBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 262
    return-void
.end method

.method public setIconLoaded(Z)V
    .locals 0
    .param p1, "_isIconLoaded"    # Z

    .prologue
    .line 271
    iput-boolean p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mIsIconLoaded:Z

    .line 272
    return-void
.end method

.method public setInstallSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "_installSize"    # Ljava/lang/String;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mInstallSize:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public setLinkCategoryID(Ljava/lang/String;)V
    .locals 0
    .param p1, "_linkCategoryID"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mLinkCategoryID:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setListTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "_listTitle"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mListTitle:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setNumValue(J)V
    .locals 1
    .param p1, "_totalNum"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mNumValue:J

    .line 42
    return-void
.end method

.method public setPrice(Ljava/lang/String;)V
    .locals 0
    .param p1, "_price"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mPrice:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setProductID(Ljava/lang/String;)V
    .locals 0
    .param p1, "_productID"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductID:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setProductImgUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "_productImgUrl"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductImgUrl:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setProductName(Ljava/lang/String;)V
    .locals 0
    .param p1, "_productName"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mProductName:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setRealContentSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "_realContentSize"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mRealContentSize:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "_version"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersion:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public setVersionCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "_versionCode"    # Ljava/lang/String;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/AppVo;->mVersionCode:Ljava/lang/String;

    .line 232
    return-void
.end method
