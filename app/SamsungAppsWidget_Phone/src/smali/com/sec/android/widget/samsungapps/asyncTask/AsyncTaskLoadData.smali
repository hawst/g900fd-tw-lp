.class public abstract Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;
.super Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;
.source "AsyncTaskLoadData.java"


# static fields
.field protected static final DEFAULT_TIME_OUT:I = 0x2710

.field protected static final TAG:Ljava/lang/String;

.field public static final WARN_CODE_CATEGORY_NAMES:Ljava/lang/String; = "API2049"

.field public static final WARN_CODE_CATEGORY_TOP_LIST:Ljava/lang/String; = "API2031"

.field public static final WARN_CODE_CONTENT_GIFTS_APP_LIST:Ljava/lang/String; = "API2031"

.field public static final WARN_CODE_COUNTRY_SEARCH_EX:Ljava/lang/String; = "API2300"

.field public static final WARN_CODE_CURATED_WIDGET_BANNER_LIST:Ljava/lang/String; = "API2328"

.field public static final WARN_CODE_FEATURED_APP_LIST:Ljava/lang/String; = "API2235"

.field public static final WARN_CODE_FEATURED_BANNER_LIST:Ljava/lang/String; = "API2321"

.field public static final WARN_CODE_LOCAL_APP_LIST:Ljava/lang/String; = "API2237"

.field public static final WARN_CODE_LOCAL_BANNER_LIST:Ljava/lang/String; = "API2327"

.field public static final WARN_CODE_TOP_ALL_LIST:Ljava/lang/String; = "API2003"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCurrentWidgetSize:I

.field mDownLoadAllBitmapThread:Ljava/lang/Thread;

.field protected mErrorMessage:Ljava/lang/String;

.field protected mInstanceIds:[I

.field protected mPrefEditor:Landroid/content/SharedPreferences$Editor;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mUseCache:Z

.field protected mUseProtocolCache:Z

.field protected mWarningMessage:Ljava/lang/String;

.field protected mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 312
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mUseProtocolCache:Z

    .line 49
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mCurrentWidgetSize:I

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mUseCache:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mWarningMessage:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mErrorMessage:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefs:Landroid/content/SharedPreferences;

    .line 55
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 61
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData$1;

    invoke-direct {v1, p0}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData$1;-><init>(Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mDownLoadAllBitmapThread:Ljava/lang/Thread;

    .line 313
    return-void
.end method


# virtual methods
.method public declared-synchronized downloadImage(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "imgUrl"    # Ljava/lang/String;
    .param p3, "_isPerfect"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 291
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v1, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    .local v1, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :try_start_1
    invoke-virtual {v1, p2}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 296
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-ne v4, p3, :cond_0

    if-nez v0, :cond_0

    .line 308
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    monitor-exit p0

    return v3

    .line 299
    :catch_0
    move-exception v2

    .line 300
    .local v2, "e":Ljava/util/ConcurrentModificationException;
    :try_start_2
    sget-object v4, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v5, "ConcurrentModificationException on downloadImages()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    invoke-virtual {v2}, Ljava/util/ConcurrentModificationException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 291
    .end local v1    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v2    # "e":Ljava/util/ConcurrentModificationException;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 303
    .restart local v1    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    :catch_1
    move-exception v2

    .line 304
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v4, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v5, "Exception on downloadImages()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    move v3, v4

    .line 308
    goto :goto_0
.end method

.method public declared-synchronized downloadImage(Landroid/content/Context;Ljava/util/ArrayList;Z)Z
    .locals 8
    .param p1, "_context"    # Landroid/content/Context;
    .param p3, "_isPerfect"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p2, "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 266
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v1, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V

    .line 268
    .local v1, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 269
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    .local v4, "imgUrl":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v1, v4}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 273
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-ne v6, p3, :cond_0

    if-nez v0, :cond_0

    .line 287
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "imgUrl":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return v5

    .line 276
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "imgUrl":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 277
    .local v2, "e":Ljava/util/ConcurrentModificationException;
    :try_start_2
    sget-object v6, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v7, "ConcurrentModificationException on downloadImages()"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {v2}, Ljava/util/ConcurrentModificationException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 266
    .end local v1    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v2    # "e":Ljava/util/ConcurrentModificationException;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "imgUrl":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 280
    .restart local v1    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "imgUrl":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 281
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v6, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v7, "Exception on downloadImages()"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "imgUrl":Ljava/lang/String;
    :cond_1
    move v5, v6

    .line 287
    goto :goto_0
.end method

.method public declared-synchronized downloadImages(Landroid/content/Context;Ljava/util/ArrayList;Z)Z
    .locals 11
    .param p1, "_context"    # Landroid/content/Context;
    .param p3, "_isPerfect"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/PageVo;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p2, "_pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 203
    monitor-enter p0

    :try_start_0
    new-instance v3, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;

    invoke-direct {v3, p1}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;-><init>(Landroid/content/Context;)V

    .line 205
    .local v3, "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 209
    .local v6, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    :cond_0
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 210
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    .line 212
    .local v7, "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getmAppList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 213
    .local v0, "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getProductImgUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 215
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-ne v9, p3, :cond_1

    if-nez v2, :cond_1

    .line 262
    .end local v0    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :goto_0
    monitor-exit p0

    return v8

    .line 220
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :cond_2
    :try_start_2
    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getmBannerList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 221
    .local v1, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerImgURL()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 223
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-ne v9, p3, :cond_3

    if-nez v2, :cond_3

    goto :goto_0

    .line 228
    .end local v1    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    :cond_4
    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getmLocalAppList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .line 229
    .local v0, "appVo":Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;
    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->getProductImgUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 231
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-ne v9, p3, :cond_5

    if-nez v2, :cond_5

    goto :goto_0

    .line 236
    .end local v0    # "appVo":Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    :cond_6
    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getmLocalBannerList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 237
    .restart local v1    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmBannerImgURL()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 239
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-ne v9, p3, :cond_7

    if-nez v2, :cond_7

    goto :goto_0

    .line 244
    .end local v1    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    :cond_8
    invoke-virtual {v7}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getmContentGiftsAppList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    .line 245
    .local v0, "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/AppVo;->getProductImgUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 247
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-ne v9, p3, :cond_9

    if-nez v2, :cond_9

    goto/16 :goto_0

    .line 253
    .end local v0    # "appVo":Lcom/sec/android/widget/samsungapps/vo/AppVo;
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "pageVo":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    :catch_0
    move-exception v4

    .line 254
    .local v4, "e":Ljava/util/ConcurrentModificationException;
    :try_start_3
    sget-object v9, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v10, "ConcurrentModificationException on downloadImages()"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-virtual {v4}, Ljava/util/ConcurrentModificationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 203
    .end local v3    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .end local v4    # "e":Ljava/util/ConcurrentModificationException;
    .end local v6    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 257
    .restart local v3    # "cache":Lcom/sec/android/widget/samsungapps/cache/ImageFileCache;
    .restart local v6    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    :catch_1
    move-exception v4

    .line 258
    .local v4, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v9, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v10, "Exception on downloadImages()"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .end local v4    # "e":Ljava/lang/Exception;
    :cond_a
    move v8, v9

    .line 262
    goto/16 :goto_0
.end method

.method protected getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;
    .locals 4
    .param p1, "_data"    # Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    .prologue
    .line 158
    const-string v0, ""

    .line 160
    .local v0, "errorMessage":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 161
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    const v3, 0x7f0a002a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 188
    :goto_0
    return-object v2

    .line 166
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->getErrString()I

    move-result v1

    .line 168
    .local v1, "mesID":I
    iget v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    if-eqz v2, :cond_1

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v2, v0

    .line 188
    goto :goto_0

    .line 171
    :cond_1
    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 174
    const-string v2, "jd"

    const-string v3, "There?"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v2, p1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 182
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 184
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected onCancelled(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "_result"    # Ljava/lang/Boolean;

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v1, "onCancelled() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-super {p0, p1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 111
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->onCancelled(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "_result"    # Ljava/lang/Boolean;

    .prologue
    .line 115
    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 117
    sget-object v2, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v3, "Task is Success !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mDownLoadAllBitmapThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_RELOAD_FINISHED:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "isReloaded"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 145
    const-string v2, "result"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    const-string v2, "appWidgetIds"

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mInstanceIds:[I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 149
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v4}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetSize()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/utils/WidgetUtils;->getWidgetClass(I)Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 151
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 155
    return-void

    .line 127
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 133
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v2, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->TAG:Ljava/lang/String;

    const-string v3, "Task is Failed !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 85
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 86
    invoke-virtual {p0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->cancel(Z)Z

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getWidgetState()I

    move-result v0

    .line 91
    .local v0, "widgetState":I
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_3

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mUseProtocolCache:Z

    .line 97
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v1, :cond_2

    .line 98
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.widget.samsungapps"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefs:Landroid/content/SharedPreferences;

    .line 100
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 103
    :cond_2
    invoke-super {p0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->onPreExecute()V

    .line 104
    return-void

    .line 93
    :cond_3
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_1

    .line 94
    iput-boolean v3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;->mUseProtocolCache:Z

    goto :goto_0
.end method
