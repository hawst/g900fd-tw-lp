.class public Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "LocalAppSAXHandler.java"


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mNode:Ljava/lang/String;

.field private mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

.field private mValue:Ljava/lang/StringBuffer;

.field private mValueType:Ljava/lang/String;

.field private mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValue:Ljava/lang/StringBuffer;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mList:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .line 23
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-void
.end method

.method private getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "_attr"    # Lorg/xml/sax/Attributes;
    .param p2, "_attrName"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 261
    const-string v1, ""

    .line 265
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 253
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 139
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValue:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "trimedValue":Ljava/lang/String;
    const-string v1, "value"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    const-string v1, "productID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 152
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setProductID(Ljava/lang/String;)V

    .line 237
    :cond_0
    :goto_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    .line 241
    :cond_1
    const-string v1, "list"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 243
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    :cond_2
    return-void

    .line 154
    :cond_3
    const-string v1, "productName"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 156
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setProductName(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_4
    const-string v1, "categoryID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 160
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setCategoryID(Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :cond_5
    const-string v1, "categoryName"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 164
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setCategoryName(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_6
    const-string v1, "categoryID2"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 168
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setCategoryID2(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_7
    const-string v1, "categoryName2"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 172
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setCategoryName2(Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_8
    const-string v1, "productImgUrl"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 176
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setProductImgUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_9
    const-string v1, "currencyUnit"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 180
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setCurrencyUnit(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :cond_a
    const-string v1, "price"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 184
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setPrice(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 186
    :cond_b
    const-string v1, "discountPrice"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 188
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setDiscountPrice(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    :cond_c
    const-string v1, "discountFlag"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 192
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setDiscountFlag(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 194
    :cond_d
    const-string v1, "averageRating"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 196
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setAverageRating(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 198
    :cond_e
    const-string v1, "date"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 200
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setDate(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 202
    :cond_f
    const-string v1, "contentType"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 204
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setContentType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    :cond_10
    const-string v1, "GUID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 208
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setGUID(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :cond_11
    const-string v1, "version"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 212
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 214
    :cond_12
    const-string v1, "versionCode"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 216
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setVersionCode(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :cond_13
    const-string v1, "installSize"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 220
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setInstallSize(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :cond_14
    const-string v1, "realContentSize"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 224
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setRealContentSize(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 226
    :cond_15
    const-string v1, "listTitle"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 228
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setListTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 230
    :cond_16
    const-string v1, "linkCategoryID"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setLinkCategoryID(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getParsedErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParsedList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    return-object v0
.end method

.method public getTotalCount()J
    .locals 4

    .prologue
    .line 34
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getTotalCount()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "strTotalCount":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 39
    .local v2, "totalCount":J
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 46
    :goto_0
    return-wide v2

    .line 41
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mList:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .line 65
    new-instance v0, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    invoke-direct {v0}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    .line 66
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 78
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    .line 79
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValue:Ljava/lang/StringBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 81
    const-string v4, "list"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 85
    new-instance v4, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-direct {v4}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    .line 90
    const-string v4, "numValue"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "strNumValue":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 95
    .local v2, "numValue":J
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 102
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mVo:Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;->setNumValue(J)V

    .line 133
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_0
    :goto_1
    return-void

    .line 97
    .restart local v1    # "strNumValue":Ljava/lang/String;
    .restart local v2    # "numValue":J
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 105
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "strNumValue":Ljava/lang/String;
    .end local v2    # "numValue":J
    :cond_1
    const-string v4, "value"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 109
    const-string v4, "name"

    invoke-direct {p0, p4, v4}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mValueType:Ljava/lang/String;

    goto :goto_1

    .line 112
    :cond_2
    const-string v4, "response"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 114
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "transactionId"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTransactionId(Ljava/lang/String;)V

    .line 115
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "totalCount"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setTotalCount(Ljava/lang/String;)V

    .line 116
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "endNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setEndNum(Ljava/lang/String;)V

    .line 117
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "startNum"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setStartNum(Ljava/lang/String;)V

    .line 118
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "returnCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setReturnCode(Ljava/lang/String;)V

    .line 119
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "name"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setName(Ljava/lang/String;)V

    .line 120
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "id"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setId(Ljava/lang/String;)V

    goto :goto_1

    .line 122
    :cond_3
    const-string v4, "SamsungProtocol"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 124
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "deviceModel"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setDeviceModel(Ljava/lang/String;)V

    .line 125
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "networkType"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setNetworkType(Ljava/lang/String;)V

    .line 126
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "lang"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setLang(Ljava/lang/String;)V

    .line 127
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "version"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 129
    :cond_4
    const-string v4, "errorString"

    iget-object v5, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mNode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 131
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->mRVo:Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    const-string v5, "errorCode"

    invoke-direct {p0, p4, v5}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getAttrTrimedValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->setErrorCode(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
