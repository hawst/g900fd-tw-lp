.class public Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;
.super Landroid/os/AsyncTask;
.source "NetBaseAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 338
    return-void
.end method

.method private _getHTTPPOSTData2(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .locals 18
    .param p1, "_url"    # Ljava/lang/String;
    .param p2, "_requestXML"    # Ljava/lang/String;
    .param p3, "_connTimeout"    # I
    .param p4, "_sockTimeout"    # I

    .prologue
    .line 194
    const/4 v11, 0x0

    .line 195
    .local v11, "responseCode":I
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    move-result-object v9

    .line 198
    .local v9, "protocolCachingMgr":Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;
    new-instance v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 199
    .local v14, "result":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    const/4 v2, 0x0

    .line 204
    .local v2, "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    const/4 v12, 0x0

    .line 205
    .local v12, "responseEntity":Lorg/apache/http/HttpEntity;
    :try_start_0
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 209
    .local v4, "client":Lorg/apache/http/client/HttpClient;
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v15

    const-string v16, "http.protocol.expect-continue"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    invoke-interface/range {v15 .. v17}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 212
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v15

    const-string v16, "http.connection.timeout"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-interface/range {v15 .. v17}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 215
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v15

    const-string v16, "http.socket.timeout"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-interface/range {v15 .. v17}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 219
    move-object/from16 v8, p1

    .line 220
    .local v8, "postURL":Ljava/lang/String;
    new-instance v7, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v7, v8}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 222
    .local v7, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v15, "User-Agent"

    const-string v16, "SAMSUNG-Android"

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v15, "Content-Type"

    const-string v16, "text/xml"

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v15, "Infinity-Version"

    const-string v16, "4.0"

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    sget-boolean v15, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    if-eqz v15, :cond_0

    .line 226
    const-string v15, "Host"

    const-string v16, "kr-odc.samsungapps.com"

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_0
    new-instance v5, Lorg/apache/http/entity/StringEntity;

    const-string v15, "UTF-8"

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v15}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    .local v5, "content":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v7, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 231
    invoke-interface {v4, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 233
    .local v13, "responsePOST":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    .line 234
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "responseCode : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v12

    .line 238
    invoke-static {v12}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->convertEntityToString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 239
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "RESPONSE : \n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-static {v15}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->getResponseServerError(Ljava/lang/String;)Lcom/sec/android/widget/samsungapps/vo/ResponseVo;

    move-result-object v10

    .line 243
    .local v10, "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    const-string v16, "E================================================="

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    if-eqz v9, :cond_1

    const-string v15, "0"

    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getReturnCode()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 248
    new-instance v3, Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    iget-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v3, v15, v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/CachingData;-><init>(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v2    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .local v3, "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15, v3}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->putData(Ljava/lang/String;Lcom/sec/android/widget/samsungapps/netbase/CachingData;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v3

    .line 298
    .end local v3    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .end local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .end local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v8    # "postURL":Ljava/lang/String;
    .end local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .end local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    .restart local v2    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    :goto_0
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    const-string v16, "unCached Data is returned!!!!\n "

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    return-object v14

    .line 256
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .restart local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v8    # "postURL":Ljava/lang/String;
    .restart local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .restart local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    :cond_1
    const/4 v15, 0x0

    :try_start_2
    iput-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 259
    :try_start_3
    invoke-virtual {v10}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 270
    :goto_1
    const/4 v15, 0x0

    :try_start_4
    iput-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 272
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .end local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .end local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v8    # "postURL":Ljava/lang/String;
    .end local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .end local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v6

    .line 273
    .local v6, "e":Ljava/lang/Exception;
    :goto_2
    iput-object v6, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 274
    const/16 v15, 0x3e7

    iput v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    .line 275
    const/4 v15, 0x0

    iput-object v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 277
    const/4 v15, 0x1

    instance-of v0, v6, Lcom/sec/android/widget/samsungapps/utils/UserException;

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 279
    :try_start_5
    move-object v0, v6

    check-cast v0, Lcom/sec/android/widget/samsungapps/utils/UserException;

    move-object v15, v0

    invoke-virtual {v15}, Lcom/sec/android/widget/samsungapps/utils/UserException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 288
    :goto_3
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "getHTTPGETData Fail : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ")"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 260
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .restart local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v8    # "postURL":Ljava/lang/String;
    .restart local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .restart local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v6

    .line 261
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 262
    const/16 v15, 0x3e7

    iput v15, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    .line 263
    sget-object v15, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "getHTTPGETData Fail : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ")"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_1

    .line 285
    .end local v4    # "client":Lorg/apache/http/client/HttpClient;
    .end local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .end local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v8    # "postURL":Ljava/lang/String;
    .end local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .end local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    :cond_2
    iput v11, v14, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mErrNo:I

    goto :goto_3

    .line 281
    :catch_2
    move-exception v15

    goto :goto_3

    .line 272
    .end local v2    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v3    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .restart local v4    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v5    # "content":Lorg/apache/http/entity/StringEntity;
    .restart local v7    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v8    # "postURL":Ljava/lang/String;
    .restart local v10    # "rVo":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .restart local v13    # "responsePOST":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v6

    move-object v2, v3

    .end local v3    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    .restart local v2    # "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    goto/16 :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->_getHTTPPOSTData2(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v0

    return-object v0
.end method

.method public static convertEntityToString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .locals 8
    .param p0, "_httpEntity"    # Lorg/apache/http/HttpEntity;

    .prologue
    .line 38
    if-nez p0, :cond_0

    .line 39
    const/4 v5, 0x0

    .line 68
    :goto_0
    return-object v5

    .line 42
    :cond_0
    const/4 v5, 0x0

    .line 44
    .local v5, "result":Ljava/lang/String;
    const/4 v2, 0x0

    .line 47
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 48
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 50
    .local v4, "out":Ljava/lang/StringBuffer;
    const/16 v6, 0x2000

    new-array v0, v6, [B

    .line 53
    .local v0, "buffer":[B
    :goto_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "n":I
    const/4 v6, -0x1

    if-eq v3, v6, :cond_1

    .line 54
    new-instance v6, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v6, v0, v7, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 58
    .end local v0    # "buffer":[B
    .end local v3    # "n":I
    .end local v4    # "out":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 63
    :catch_1
    move-exception v1

    .line 64
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 57
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "buffer":[B
    .restart local v3    # "n":I
    .restart local v4    # "out":Ljava/lang/StringBuffer;
    :cond_1
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    .line 62
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 63
    :catch_2
    move-exception v1

    .line 64
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 61
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "n":I
    .end local v4    # "out":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v6

    .line 62
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 65
    :goto_2
    throw v6

    .line 63
    :catch_3
    move-exception v1

    .line 64
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private getCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "_data"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 305
    const/4 v0, 0x0

    .line 307
    .local v0, "key":Ljava/lang/String;
    const-string v1, "featuredProductList2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_2

    .line 308
    const-string v0, "featuredProductList2Notc"

    .line 314
    :cond_0
    :goto_0
    const-string v1, "localFeaturedProductList2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_4

    .line 315
    const-string v0, "localFeaturedProductList2Notc"

    .line 332
    :cond_1
    :goto_1
    sget-object v1, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cacheKey : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-object v0

    .line 309
    :cond_2
    const-string v1, "promotionBannerListEx2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_3

    .line 310
    const-string v0, "promotionBannerListEx2Notc"

    goto :goto_0

    .line 311
    :cond_3
    const-string v1, "curatedWidgetBannerList2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_0

    .line 312
    const-string v0, "curatedWidgetBannerList2Notc"

    goto :goto_0

    .line 316
    :cond_4
    const-string v1, "localBannerList2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_5

    .line 317
    const-string v0, "localBannerList2Notc"

    goto :goto_1

    .line 318
    :cond_5
    const-string v1, "allProductList2Notc"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_6

    .line 319
    const-string v0, "allProductList2Notc"

    goto :goto_1

    .line 320
    :cond_6
    const-string v1, "countrySearchEx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_7

    .line 321
    const-string v0, "countrySearchEx"

    goto :goto_1

    .line 322
    :cond_7
    const-string v1, "widgetClickLog"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_8

    .line 323
    const-string v0, "widgetClickLog"

    goto :goto_1

    .line 324
    :cond_8
    const-string v1, "contentCategoryProductList"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_9

    .line 325
    const-string v0, "contentCategoryProductList"

    goto :goto_1

    .line 326
    :cond_9
    const-string v1, "getCategoryName"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v2, v1, :cond_1

    .line 327
    const-string v0, "getCategoryName"

    goto :goto_1
.end method

.method public static getResponseServerError(Ljava/lang/String;)Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    .locals 8
    .param p0, "_strResponse"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 76
    .local v1, "result":Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v5

    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    .line 78
    .local v2, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v4

    .line 79
    .local v4, "xr":Lorg/xml/sax/XMLReader;
    new-instance v3, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;

    invoke-direct {v3}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;-><init>()V

    .line 81
    .local v3, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;
    invoke-interface {v4, v3}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 82
    new-instance v5, Lorg/xml/sax/InputSource;

    new-instance v6, Ljava/io/StringReader;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v4, v5}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 84
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;->getResponseVo()Lcom/sec/android/widget/samsungapps/vo/ResponseVo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 89
    const/4 v5, 0x1

    const-string v6, "1"

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getReturnCode()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v5, v6, :cond_0

    .line 90
    new-instance v5, Lcom/sec/android/widget/samsungapps/utils/UserException;

    invoke-virtual {v1}, Lcom/sec/android/widget/samsungapps/vo/ResponseVo;->getErrorCode()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/widget/samsungapps/utils/UserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 85
    .end local v2    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;
    .end local v4    # "xr":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v5

    .restart local v2    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v3    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/ResponseSAXHandler;
    .restart local v4    # "xr":Lorg/xml/sax/XMLReader;
    :cond_0
    move-object v5, v1

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .locals 10
    .param p1, "_url"    # Ljava/lang/String;
    .param p2, "_requestXML"    # Ljava/lang/String;
    .param p3, "_connTimeout"    # I
    .param p4, "_sockTimeout"    # I
    .param p5, "_isCaching"    # Z

    .prologue
    const/4 v3, 0x1

    .line 109
    new-instance v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    invoke-direct {v8, p0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 112
    .local v8, "result":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    const-string v1, "S================================================="

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "REQUEST : \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v3, v0, :cond_0

    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v3, v0, :cond_1

    .line 120
    :cond_0
    const/4 v8, 0x0

    .line 181
    .end local v8    # "result":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    :goto_0
    return-object v8

    .line 126
    .restart local v8    # "result":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    :cond_1
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;

    move-result-object v7

    .line 129
    .local v7, "protocolCachingMgr":Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;
    const/4 v6, 0x0

    .line 131
    .local v6, "cachingData":Lcom/sec/android/widget/samsungapps/netbase/CachingData;
    if-eqz v7, :cond_4

    .line 134
    if-nez p5, :cond_3

    .line 136
    :try_start_0
    invoke-direct {p0, p2}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->removeKeyString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_1
    if-eqz v6, :cond_4

    .line 148
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CachingData : \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {v7, v6}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->isTimeOutData(Lcom/sec/android/widget/samsungapps/netbase/CachingData;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 151
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Caching Timeout Reloading Start..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$1;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    .line 170
    :cond_2
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    const-string v1, "Cached Data is returned!!!\n "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    sget-object v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RESPONSE : \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v6, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, v6, Lcom/sec/android/widget/samsungapps/netbase/CachingData;->mData:Ljava/lang/String;

    iput-object v0, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    goto :goto_0

    .line 143
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->getCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/widget/samsungapps/netbase/ProtocolCachingManager;->getData(Ljava/lang/String;)Lcom/sec/android/widget/samsungapps/netbase/CachingData;

    move-result-object v6

    goto :goto_1

    .line 181
    :cond_4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;->_getHTTPPOSTData2(Ljava/lang/String;Ljava/lang/String;II)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v8

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    goto :goto_1
.end method
