.class public Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;
.super Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;
.source "AsyncTaskLoad4x6Data.java"


# instance fields
.field private mTempContentGiftAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempFeaturedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempFeaturedBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLocalAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLocalBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[IIZ)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceIds"    # [I
    .param p3, "_widgetSize"    # I
    .param p4, "_useCache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;-><init>()V

    .line 28
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    .line 31
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    .line 32
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 41
    sget-object v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->TAG:Ljava/lang/String;

    const-string v2, "AsyncTaskLoad4x6Data()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v0

    .line 45
    .local v0, "dataManager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    .line 46
    iput p3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mCurrentWidgetSize:I

    .line 47
    iput-boolean p4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    .line 48
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mInstanceIds:[I

    .line 49
    iget v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mCurrentWidgetSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 50
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 34
    .param p1, "_params"    # [Ljava/lang/String;

    .prologue
    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 57
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    .line 58
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    .line 60
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 66
    :try_start_0
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;

    move-result-object v24

    .line 67
    .local v24, "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->doCountrySearchEx(Landroid/content/Context;)V

    .line 69
    const-string v3, ""

    .line 71
    .local v3, "strAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-nez v2, :cond_0

    .line 72
    sget-object v3, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 74
    const/4 v2, 0x1

    const-string v5, ""

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v2, v5, :cond_0

    .line 75
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "API2300"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 79
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 414
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v24    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :goto_0
    return-object v2

    .line 87
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v24    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_0
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForFeaturedAppList()Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "requestFeaturedAppXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-eqz v2, :cond_1

    .line 91
    new-instance v23, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 92
    .local v23, "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "featuredProductList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 98
    :goto_1
    if-eqz v23, :cond_4

    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string v2, ""

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 102
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 103
    .local v31, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 104
    .local v33, "xr":Lorg/xml/sax/XMLReader;
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 106
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 107
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 110
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 116
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2235"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 118
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 119
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 94
    .end local v23    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    :cond_1
    const/16 v5, 0x2710

    const/16 v6, 0x2710

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseProtocolCache:Z

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v23

    .restart local v23    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto :goto_1

    .line 120
    .restart local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .restart local v33    # "xr":Lorg/xml/sax/XMLReader;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 121
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2235"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 122
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWarningMessage:Ljava/lang/String;

    .line 131
    :cond_3
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForBannerList()Ljava/lang/String;

    move-result-object v7

    .line 134
    .local v7, "requestBannerXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-eqz v2, :cond_5

    .line 135
    new-instance v20, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 136
    .local v20, "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "promotionBannerListEx2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 142
    :goto_2
    if-eqz v20, :cond_8

    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_8

    const-string v2, ""

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 147
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 148
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 149
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 151
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 152
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 154
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    if-nez v2, :cond_6

    .line 160
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2321"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 163
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 164
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 125
    .end local v7    # "requestBannerXml":Ljava/lang/String;
    .end local v20    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 126
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 138
    .restart local v7    # "requestBannerXml":Ljava/lang/String;
    .restart local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .restart local v33    # "xr":Lorg/xml/sax/XMLReader;
    :cond_5
    const/16 v8, 0x2710

    const/16 v9, 0x2710

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseProtocolCache:Z

    move-object/from16 v5, p0

    move-object v6, v3

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v20

    .restart local v20    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_2

    .line 165
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_7

    .line 166
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2321"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 167
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWarningMessage:Ljava/lang/String;

    .line 176
    :cond_7
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForLocalAppList()Ljava/lang/String;

    move-result-object v10

    .line 179
    .local v10, "requestLocalAppXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-eqz v2, :cond_9

    .line 180
    new-instance v26, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 181
    .local v26, "localApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "localFeaturedProductList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 187
    :goto_3
    if-eqz v26, :cond_c

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_c

    const-string v2, ""

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 191
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 192
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 193
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;

    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;-><init>()V

    .line 195
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 196
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v26

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 199
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    if-nez v2, :cond_a

    .line 205
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2237"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 207
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 208
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 170
    .end local v10    # "requestLocalAppXml":Ljava/lang/String;
    .end local v26    # "localApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 171
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 183
    .restart local v10    # "requestLocalAppXml":Ljava/lang/String;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_9
    const/16 v11, 0x2710

    const/16 v12, 0x2710

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseProtocolCache:Z

    move-object/from16 v8, p0

    move-object v9, v3

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v26

    .restart local v26    # "localApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_3

    .line 209
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_b

    .line 210
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2237"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 212
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWarningMessage:Ljava/lang/String;

    .line 221
    :cond_b
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForLocalBannerList()Ljava/lang/String;

    move-result-object v13

    .line 224
    .local v13, "requestLocalBannerXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-eqz v2, :cond_d

    .line 225
    new-instance v27, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 226
    .local v27, "localBanner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "localBannerList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 232
    :goto_4
    if-eqz v27, :cond_10

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_10

    const-string v2, ""

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 237
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 238
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 239
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 241
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 242
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 244
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    .line 249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    if-nez v2, :cond_e

    .line 250
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2327"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 253
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 254
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 215
    .end local v13    # "requestLocalBannerXml":Ljava/lang/String;
    .end local v27    # "localBanner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    :cond_c
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 216
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 228
    .restart local v13    # "requestLocalBannerXml":Ljava/lang/String;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    :cond_d
    const/16 v14, 0x2710

    const/16 v15, 0x2710

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseProtocolCache:Z

    move/from16 v16, v0

    move-object/from16 v11, p0

    move-object v12, v3

    invoke-virtual/range {v11 .. v16}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v27

    .restart local v27    # "localBanner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_4

    .line 255
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_f

    .line 256
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2327"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 257
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWarningMessage:Ljava/lang/String;

    .line 267
    :cond_f
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForContentGiftsAppList()Ljava/lang/String;

    move-result-object v16

    .line 270
    .local v16, "requestContentGiftsAppXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseCache:Z

    if-eqz v2, :cond_11

    .line 271
    new-instance v21, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 272
    .local v21, "contentGiftsApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "contentCategoryProductList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 278
    :goto_5
    if-eqz v21, :cond_15

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_15

    const-string v2, ""

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 282
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v31

    .line 283
    invoke-virtual/range {v31 .. v31}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v33

    .line 284
    new-instance v32, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;

    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    invoke-direct/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;-><init>()V

    .line 286
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 287
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v33

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 290
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    if-nez v2, :cond_12

    .line 296
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2031"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 298
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 299
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 260
    .end local v16    # "requestContentGiftsAppXml":Ljava/lang/String;
    .end local v21    # "contentGiftsApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/LocalAppSAXHandler;
    :cond_10
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 261
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 274
    .restart local v16    # "requestContentGiftsAppXml":Ljava/lang/String;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_11
    const/16 v17, 0x2710

    const/16 v18, 0x2710

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mUseProtocolCache:Z

    move/from16 v19, v0

    move-object/from16 v14, p0

    move-object v15, v3

    invoke-virtual/range {v14 .. v19}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v21

    .restart local v21    # "contentGiftsApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto/16 :goto_5

    .line 300
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_13

    .line 301
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2031"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 302
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWarningMessage:Ljava/lang/String;

    .line 312
    :cond_13
    new-instance v30, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 313
    .local v30, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v2, 0x5

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V

    .line 314
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getItemCounts()[I

    move-result-object v28

    .line 318
    .local v28, "mItemCounts":[I
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_16

    .line 319
    const/4 v2, 0x0

    aget v2, v28, v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_14

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 318
    :cond_14
    add-int/lit8 v25, v25, 0x1

    goto :goto_6

    .line 305
    .end local v25    # "i":I
    .end local v28    # "mItemCounts":[I
    .end local v30    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    :cond_15
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 306
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 326
    .restart local v25    # "i":I
    .restart local v28    # "mItemCounts":[I
    .restart local v30    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .local v32, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    :cond_16
    const/16 v25, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_18

    .line 327
    const/4 v2, 0x1

    aget v2, v28, v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_17

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempFeaturedBannerList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 326
    :cond_17
    add-int/lit8 v25, v25, 0x1

    goto :goto_7

    .line 333
    :cond_18
    const/16 v25, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_19

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalAppList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmLocalAppList(Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;)V

    .line 333
    add-int/lit8 v25, v25, 0x1

    goto :goto_8

    .line 341
    :cond_19
    const/16 v25, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_1b

    .line 342
    const/4 v2, 0x3

    aget v2, v28, v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_1a

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempLocalBannerList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmLocalBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V

    .line 341
    :cond_1a
    add-int/lit8 v25, v25, 0x1

    goto :goto_9

    .line 349
    :cond_1b
    const/16 v25, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v25

    if-ge v0, v2, :cond_1c

    .line 351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widget/samsungapps/vo/AppVo;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmContentGiftsAppList(Lcom/sec/android/widget/samsungapps/vo/AppVo;)V

    .line 349
    add-int/lit8 v25, v25, 0x1

    goto :goto_a

    .line 353
    :cond_1c
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->deleteDuplicatedAppForContentGiftsAppList()V

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->saveShuffledSequenceForLocalAppList(Landroid/content/Context;)V

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->saveShuffledSequenceForContentGiftsAppList(Landroid/content/Context;)V

    .line 360
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 362
    .local v29, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual/range {v29 .. v30}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 369
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v6}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->getPageList()Ljava/util/ArrayList;

    move-result-object v6

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->downloadImages(Landroid/content/Context;Ljava/util/ArrayList;Z)Z

    move-result v5

    if-ne v2, v5, :cond_1d

    .line 377
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "promotionBannerListEx2Notc"

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "featuredProductList2Notc"

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "localBannerList2Notc"

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "localFeaturedProductList2Notc"

    move-object/from16 v0, v26

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "contentCategoryProductList2Notc"

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 387
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 390
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 396
    :cond_1d
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 398
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 399
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    goto/16 :goto_0

    .line 403
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v4    # "requestFeaturedAppXml":Ljava/lang/String;
    .end local v7    # "requestBannerXml":Ljava/lang/String;
    .end local v10    # "requestLocalAppXml":Ljava/lang/String;
    .end local v13    # "requestLocalBannerXml":Ljava/lang/String;
    .end local v16    # "requestContentGiftsAppXml":Ljava/lang/String;
    .end local v20    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v21    # "contentGiftsApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v23    # "featuredApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v24    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .end local v25    # "i":I
    .end local v26    # "localApp":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v27    # "localBanner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v28    # "mItemCounts":[I
    .end local v29    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v30    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v31    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v32    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/AppSAXHandler;
    .end local v33    # "xr":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v22

    .line 404
    .local v22, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 405
    invoke-virtual/range {v22 .. v22}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    .line 406
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 407
    .end local v22    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v22

    .line 408
    .local v22, "e":Lorg/xml/sax/SAXException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 409
    invoke-virtual/range {v22 .. v22}, Lorg/xml/sax/SAXException;->printStackTrace()V

    .line 410
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 411
    .end local v22    # "e":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v22

    .line 412
    .local v22, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->mErrorMessage:Ljava/lang/String;

    .line 413
    invoke-virtual/range {v22 .. v22}, Ljava/io/IOException;->printStackTrace()V

    .line 414
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x6Data;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
