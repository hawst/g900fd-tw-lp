.class public Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;
.super Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;
.source "AsyncTaskLoad4x1Data.java"


# instance fields
.field private mTempAppsForGalaxyAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempBannerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/BannerVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempContentGiftAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempFeaturedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLifestyleAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/AppVo;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLocalAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widget/samsungapps/vo/LocalAppVo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[IIZ)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_instanceIds"    # [I
    .param p3, "_widgetSize"    # I
    .param p4, "_useCache"    # Z

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempLocalAppList:Ljava/util/ArrayList;

    .line 33
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempContentGiftAppList:Ljava/util/ArrayList;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempFeaturedAppList:Ljava/util/ArrayList;

    .line 35
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempLifestyleAppList:Ljava/util/ArrayList;

    .line 36
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempAppsForGalaxyAppList:Ljava/util/ArrayList;

    .line 37
    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 48
    sget-object v1, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->TAG:Ljava/lang/String;

    const-string v2, "AsyncTaskLoad4x1Data()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {p1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;

    move-result-object v0

    .line 52
    .local v0, "dataManager":Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mContext:Landroid/content/Context;

    .line 53
    iput p3, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mCurrentWidgetSize:I

    .line 54
    iput-boolean p4, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mUseCache:Z

    .line 55
    iput-object p2, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mInstanceIds:[I

    .line 56
    iget v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mCurrentWidgetSize:I

    invoke-virtual {v0, v1}, Lcom/sec/android/widget/samsungapps/data/WidgetDataManager;->getWidgetData(I)Lcom/sec/android/widget/samsungapps/data/WidgetData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    .line 57
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 19
    .param p1, "_params"    # [Ljava/lang/String;

    .prologue
    .line 67
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 74
    :try_start_0
    invoke-static {}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getInstance()Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;

    move-result-object v11

    .line 75
    .local v11, "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v2}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->doCountrySearchEx(Landroid/content/Context;)V

    .line 77
    const-string v3, ""

    .line 79
    .local v3, "strAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mUseCache:Z

    if-nez v2, :cond_0

    .line 81
    sget-object v3, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->OPEN_API_URL:Ljava/lang/String;

    .line 83
    const/4 v2, 0x1

    const-string v5, ""

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v2, v5, :cond_0

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "API2300"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mErrorMessage:Ljava/lang/String;

    .line 89
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 207
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :goto_0
    return-object v2

    .line 94
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_0
    sget-boolean v2, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gIsStaging:Z

    if-eqz v2, :cond_1

    .line 95
    sget-object v3, Lcom/sec/android/widget/samsungapps/utils/DeviceInfo;->gStagingDataHostUrl:Ljava/lang/String;

    .line 101
    :cond_1
    invoke-virtual {v11}, Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;->getRequestXMLForCuratedWidgetBannerList()Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "requestBannerXml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mUseCache:Z

    if-eqz v2, :cond_2

    .line 106
    new-instance v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;-><init>(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask;)V

    .line 107
    .local v8, "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mPrefs:Landroid/content/SharedPreferences;

    const-string v5, "curatedWidgetBannerList2Notc"

    const-string v6, ""

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    .line 115
    :goto_1
    if-eqz v8, :cond_6

    iget-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    if-eqz v2, :cond_6

    const-string v2, ""

    iget-object v5, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 121
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v16

    .line 122
    .local v16, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual/range {v16 .. v16}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v18

    .line 123
    .local v18, "xr":Lorg/xml/sax/XMLReader;
    new-instance v17, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;-><init>()V

    .line 125
    .local v17, "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 126
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v5, Ljava/io/StringReader;

    iget-object v6, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 128
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;->getParsedList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    .line 134
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "Null for API2328"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 137
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mErrorMessage:Ljava/lang/String;

    .line 138
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 111
    .end local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :cond_2
    const/16 v5, 0x2710

    const/16 v6, 0x2710

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mUseProtocolCache:Z

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getHTTPPOSTData(Ljava/lang/String;Ljava/lang/String;IIZ)Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;

    move-result-object v8

    .restart local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    goto :goto_1

    .line 139
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 140
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    const-string v5, "API2328"

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 141
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mWarningMessage:Ljava/lang/String;

    .line 149
    :cond_4
    new-instance v15, Lcom/sec/android/widget/samsungapps/vo/PageVo;

    invoke-direct {v15}, Lcom/sec/android/widget/samsungapps/vo/PageVo;-><init>()V

    .line 150
    .local v15, "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    const/4 v2, 0x6

    invoke-virtual {v15, v2}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->setmType(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 153
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mTempBannerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 154
    .local v9, "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v15, v9}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->addmBannerList(Lcom/sec/android/widget/samsungapps/vo/BannerVo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 156
    .end local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v12    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v10

    .line 157
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 162
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_5
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 164
    .local v14, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v2, v14}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->setPageList(Ljava/util/ArrayList;)V

    .line 169
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v13, "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v15}, Lcom/sec/android/widget/samsungapps/vo/PageVo;->getCurPageBannerList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widget/samsungapps/vo/BannerVo;

    .line 172
    .restart local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    invoke-virtual {v9}, Lcom/sec/android/widget/samsungapps/vo/BannerVo;->getmWidgetSymbolImgURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 204
    .end local v3    # "strAddress":Ljava/lang/String;
    .end local v4    # "requestBannerXml":Ljava/lang/String;
    .end local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .end local v9    # "bannerVo":Lcom/sec/android/widget/samsungapps/vo/BannerVo;
    .end local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .end local v15    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .end local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .end local v18    # "xr":Lorg/xml/sax/XMLReader;
    :catch_1
    move-exception v10

    .line 205
    .restart local v10    # "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mErrorMessage:Ljava/lang/String;

    .line 206
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 207
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 144
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v3    # "strAddress":Ljava/lang/String;
    .restart local v4    # "requestBannerXml":Ljava/lang/String;
    .restart local v8    # "banner":Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;
    .restart local v11    # "helper":Lcom/sec/android/widget/samsungapps/netbase/OpenApiHelper;
    :cond_6
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mErrorMessage:Ljava/lang/String;

    .line 145
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 178
    .restart local v12    # "i$":Ljava/util/Iterator;
    .restart local v13    # "imgUrlList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widget/samsungapps/vo/PageVo;>;"
    .restart local v15    # "pageVo1":Lcom/sec/android/widget/samsungapps/vo/PageVo;
    .restart local v16    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v17    # "xmlHandler":Lcom/sec/android/widget/samsungapps/xml/BannerSAXHandler;
    .restart local v18    # "xr":Lorg/xml/sax/XMLReader;
    :cond_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v13, v6}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->downloadImage(Landroid/content/Context;Ljava/util/ArrayList;Z)Z

    move-result v5

    if-ne v2, v5, :cond_8

    .line 186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v5, "curatedWidgetBannerList2Notc"

    iget-object v6, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mReturnStr:Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mPrefEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 191
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 197
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mWidgetData:Lcom/sec/android/widget/samsungapps/data/WidgetData;

    invoke-virtual {v2}, Lcom/sec/android/widget/samsungapps/data/WidgetData;->restorePageList()V

    .line 198
    new-instance v2, Lcom/sec/android/widget/samsungapps/utils/NoDataException;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0026

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/sec/android/widget/samsungapps/utils/NoDataException;-><init>(Ljava/lang/String;)V

    iput-object v2, v8, Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;->mException:Ljava/lang/Exception;

    .line 200
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->getErrorMessage(Lcom/sec/android/widget/samsungapps/netbase/NetBaseAsyncTask$NetReturn;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->mErrorMessage:Ljava/lang/String;

    .line 201
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoad4x1Data;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
