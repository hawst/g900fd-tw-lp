.class public Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;
.super Ljava/lang/Object;
.source "SettingPreferenceManager.java"


# static fields
.field public static final APP_NAME:Ljava/lang/String; = "com.sec.android.widget.samsungapps"

.field public static final STRING_TOKEN:Ljava/lang/String; = "!@"

.field public static final WIDGET_CATEGORY:Ljava/lang/String; = "widget.category"

.field public static final WIDGET_CATEGORY_LIST:Ljava/lang/String; = "widget.category_list"

.field public static volatile instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    .line 26
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    if-nez v0, :cond_1

    .line 31
    const-class v1, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    monitor-enter v1

    .line 32
    :try_start_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    invoke-direct {v0, p0}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    .line 36
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_1
    sget-object v0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->instance:Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getWidgetCategories()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 74
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.widget.samsungapps"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 77
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v4, "widget.category_list"

    const-string v5, ""

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "mergedCategoryList":Ljava/lang/String;
    const/4 v4, 0x1

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v4, v5, :cond_0

    .line 82
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f060000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .local v2, "result":[Ljava/lang/String;
    move-object v3, v2

    .line 88
    .end local v2    # "result":[Ljava/lang/String;
    .local v3, "result":[Ljava/lang/String;
    :goto_0
    return-object v3

    .line 87
    .end local v3    # "result":[Ljava/lang/String;
    :cond_0
    const-string v4, "!@"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "result":[Ljava/lang/String;
    move-object v3, v2

    .line 88
    .end local v2    # "result":[Ljava/lang/String;
    .restart local v3    # "result":[Ljava/lang/String;
    goto :goto_0
.end method

.method public getWidgetCategory()Ljava/lang/String;
    .locals 4

    .prologue
    .line 50
    iget-object v1, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.widget.samsungapps"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 53
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "widget.category"

    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getWidgetCategoryName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 93
    const/4 v3, 0x0

    .line 95
    .local v3, "selectedCategoryName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategories()[Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "categoryNames":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "categoryValues":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 100
    aget-object v4, v1, v2

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategory()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 102
    aget-object v3, v0, v2

    .line 98
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 106
    :cond_1
    return-object v3
.end method

.method public setWidgetCategories([Ljava/lang/String;)V
    .locals 7
    .param p1, "_categoryNames"    # [Ljava/lang/String;

    .prologue
    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v2, "mergedCategoryNames":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_1

    .line 61
    aget-object v4, p1, v1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    add-int/lit8 v4, v1, 0x1

    array-length v5, p1

    if-ge v4, v5, :cond_0

    .line 63
    const-string v4, "!@"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.widget.samsungapps"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 68
    .local v3, "shared_pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 69
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "widget.category_list"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 70
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    return-void
.end method

.method public setWidgetCategory(Ljava/lang/String;)V
    .locals 5
    .param p1, "_categoryName"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v2, p0, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.widget.samsungapps"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 44
    .local v1, "shared_pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 45
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "widget.category"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 46
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 47
    return-void
.end method
