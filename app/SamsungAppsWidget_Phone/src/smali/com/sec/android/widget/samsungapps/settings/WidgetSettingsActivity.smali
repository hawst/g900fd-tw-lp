.class public Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "WidgetSettingsActivity.java"


# static fields
.field private static final FONT_PATH:Ljava/lang/String; = "/system/fonts/"

.field private static final TAG:Ljava/lang/String;

.field private static final TYPEFACE_ROBOTO_REGULAR:Ljava/lang/String; = "Roboto-Regular.ttf"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActionBarTitleTextView(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 6
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v2, 0x0

    .line 53
    .local v2, "font":Landroid/graphics/Typeface;
    :try_start_0
    sget-object v4, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->TAG:Ljava/lang/String;

    const-string v5, "Use system font"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-string v4, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v4}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 60
    :goto_0
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 61
    .local v0, "actionBarTitleTextView":Landroid/widget/TextView;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const v5, 0x10102eb

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 62
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    const/16 v4, 0x13

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 64
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 66
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 67
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 68
    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 69
    return-object v0

    .line 56
    .end local v0    # "actionBarTitleTextView":Landroid/widget/TextView;
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->TAG:Ljava/lang/String;

    const-string v5, "Font not exist"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->getActionBarTitleTextView(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;

    invoke-direct {v2}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 28
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 38
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 44
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 40
    :pswitch_0
    sget-object v0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->TAG:Ljava/lang/String;

    const-string v1, "Setting home button selected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->finish()V

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;->finish()V

    .line 34
    return-void
.end method
