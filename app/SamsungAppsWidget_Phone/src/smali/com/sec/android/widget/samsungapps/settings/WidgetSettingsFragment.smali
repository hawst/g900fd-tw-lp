.class public Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "WidgetSettingsFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final KEY_CATEGORY:Ljava/lang/String; = "widget_category"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCategories:Landroid/preference/ListPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 28
    return-void
.end method

.method private updateSummary()V
    .locals 6

    .prologue
    .line 61
    const-string v4, "WidgetSettings"

    const-string v5, "updateSummary"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    move-result-object v3

    .line 65
    .local v3, "manager":Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;
    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategories()[Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "categoryNames":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "categoryValues":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    invoke-virtual {v4, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 70
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_1

    .line 72
    aget-object v4, v1, v2

    invoke-virtual {v3}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getWidgetCategory()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 74
    iget-object v4, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    aget-object v5, v0, v2

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 70
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 77
    :cond_1
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->addPreferencesFromResource(I)V

    .line 35
    const-string v0, "widget_category"

    invoke-virtual {p0, v0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    .line 36
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 57
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 58
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;

    move-result-object v1

    .local v1, "manager":Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;
    move-object v0, p2

    .line 83
    check-cast v0, Ljava/lang/String;

    .line 85
    .local v0, "categoryName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    sget-object v3, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Set refresh interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {v1, v0}, Lcom/sec/android/widget/samsungapps/settings/SettingPreferenceManager;->setWidgetCategory(Ljava/lang/String;)V

    .line 89
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/sec/android/widget/samsungapps/WidgetProvider2;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v2, "serviceIntent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/widget/samsungapps/WidgetProvider;->ACTION_SETTINGS_FINISHED:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    const/4 v3, 0x1

    .line 94
    .end local v2    # "serviceIntent":Landroid/content/Intent;
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->mCategories:Landroid/preference/ListPreference;

    const-string v1, "0000000328"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050002

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->updateSummary()V

    .line 51
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 52
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widget/samsungapps/settings/WidgetSettingsFragment;->updateSummary()V

    .line 100
    return-void
.end method
