.class public Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;
.super Ljava/lang/Object;
.source "CountrySearchExVo.java"


# instance fields
.field private countryCode:Ljava/lang/String;

.field private countryURL:Ljava/lang/String;

.field private currencyUnitDivision:Ljava/lang/String;

.field private currencyUnitHasPenny:Ljava/lang/String;

.field private currencyUnitPrecedes:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private freeStoreClsf:Ljava/lang/String;

.field private freetabClsf:Ljava/lang/String;

.field private mcc:Ljava/lang/String;

.field private numValue:J

.field private offset:Ljava/lang/String;

.field private snsVal:Ljava/lang/String;

.field private webWidgetClsf:Ljava/lang/String;

.field private widgetTemplateClsf:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->numValue:J

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryURL:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->mcc:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryCode:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freeStoreClsf:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->description:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitPrecedes:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitHasPenny:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->offset:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freetabClsf:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->snsVal:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitDivision:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->webWidgetClsf:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->widgetTemplateClsf:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryURL:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnitDivision()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitDivision:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnitHasPenny()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitHasPenny:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnitPrecedes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitPrecedes:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getFreeStoreClsf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freeStoreClsf:Ljava/lang/String;

    return-object v0
.end method

.method public getFreetabClsf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freetabClsf:Ljava/lang/String;

    return-object v0
.end method

.method public getMcc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->mcc:Ljava/lang/String;

    return-object v0
.end method

.method public getNumValue()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->numValue:J

    return-wide v0
.end method

.method public getOffset()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->offset:Ljava/lang/String;

    return-object v0
.end method

.method public getSnsVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->snsVal:Ljava/lang/String;

    return-object v0
.end method

.method public getWebWidgetClsf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->webWidgetClsf:Ljava/lang/String;

    return-object v0
.end method

.method public getWidgetTemplateClsf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->widgetTemplateClsf:Ljava/lang/String;

    return-object v0
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryCode:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCountryURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryURL"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->countryURL:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setCurrencyUnitDivision(Ljava/lang/String;)V
    .locals 0
    .param p1, "currencyUnitDivision"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitDivision:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setCurrencyUnitHasPenny(Ljava/lang/String;)V
    .locals 0
    .param p1, "currencyUnitHasPenny"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitHasPenny:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setCurrencyUnitPrecedes(Ljava/lang/String;)V
    .locals 0
    .param p1, "currencyUnitPrecedes"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->currencyUnitPrecedes:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->description:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setFreeStoreClsf(Ljava/lang/String;)V
    .locals 0
    .param p1, "freeStoreClsf"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freeStoreClsf:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setFreetabClsf(Ljava/lang/String;)V
    .locals 0
    .param p1, "freetabClsf"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->freetabClsf:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setMcc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mcc"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->mcc:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setNumValue(J)V
    .locals 1
    .param p1, "numValue"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->numValue:J

    .line 27
    return-void
.end method

.method public setOffset(Ljava/lang/String;)V
    .locals 0
    .param p1, "offset"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->offset:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setSnsVal(Ljava/lang/String;)V
    .locals 0
    .param p1, "snsVal"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->snsVal:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setWebWidgetClsf(Ljava/lang/String;)V
    .locals 0
    .param p1, "webWidgetClsf"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->webWidgetClsf:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setWidgetTemplateClsf(Ljava/lang/String;)V
    .locals 0
    .param p1, "widgetTemplateClsf"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/widget/samsungapps/vo/CountrySearchExVo;->widgetTemplateClsf:Ljava/lang/String;

    .line 131
    return-void
.end method
