.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;
.super Ljava/lang/Object;
.source "SaconfigInfoLoader.java"


# static fields
.field static MAX_CONFIG_COUNT:I

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private mAPILevel:Ljava/lang/String;

.field private mCSC:Ljava/lang/String;

.field private mHubUrl:Ljava/lang/String;

.field private mIMEI:Ljava/lang/String;

.field private mIsSaconfig:Z

.field private mIsStaging:Ljava/lang/String;

.field private mMCC:Ljava/lang/String;

.field private mMNC:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mODCVersion:Ljava/lang/String;

.field private mStagingAppHostUrl:Ljava/lang/String;

.field private mStagingDataHostUrl:Ljava/lang/String;

.field private mStagingImgHostUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/widget/samsungapps/asyncTask/AsyncTaskLoadData;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->TAG:Ljava/lang/String;

    .line 35
    const/16 v0, 0x2d

    sput v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->MAX_CONFIG_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 13

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsSaconfig:Z

    .line 22
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    .line 23
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsStaging:Ljava/lang/String;

    .line 24
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    .line 25
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    .line 26
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    .line 27
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    .line 28
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMCC:Ljava/lang/String;

    .line 29
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMNC:Ljava/lang/String;

    .line 30
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mModel:Ljava/lang/String;

    .line 31
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mCSC:Ljava/lang/String;

    .line 32
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    .line 33
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    .line 39
    const-string v0, "/sdcard"

    .line 40
    .local v0, "DEFAULT_ROOT_PATH":Ljava/lang/String;
    const-string v6, "/sdcard"

    .line 41
    .local v6, "path":Ljava/lang/String;
    const-string v10, "78,66,68,74,73,6b,6e,6c,33,6e,73,6e,"

    invoke-direct {p0, v10}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 43
    .local v5, "name":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 49
    :goto_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .local v3, "file":Ljava/io/File;
    sget-object v10, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mschoi file.exists() :  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 52
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsSaconfig:Z

    .line 55
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 56
    .local v1, "bufReader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 57
    .local v4, "mapIndex":I
    const/4 v8, 0x0

    .line 58
    .local v8, "startIndex":I
    const-string v9, ""

    .line 61
    .local v9, "val":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .line 62
    .local v7, "raw":Ljava/lang/String;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-gez v10, :cond_3

    .line 118
    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 129
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "mapIndex":I
    .end local v7    # "raw":Ljava/lang/String;
    .end local v8    # "startIndex":I
    .end local v9    # "val":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void

    .line 44
    .end local v3    # "file":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 45
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "/sdcard"

    .line 46
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 66
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "mapIndex":I
    .restart local v7    # "raw":Ljava/lang/String;
    .restart local v8    # "startIndex":I
    .restart local v9    # "val":Ljava/lang/String;
    :cond_3
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 67
    const-string v10, "="

    invoke-virtual {v7, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v8, v10, 0x1

    .line 68
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v7, v8, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    .end local v9    # "val":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 70
    .restart local v9    # "val":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 71
    packed-switch v4, :pswitch_data_0

    .line 114
    :goto_3
    :pswitch_0
    const-string v9, ""

    .line 115
    add-int/lit8 v4, v4, 0x1

    .line 116
    sget v10, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->MAX_CONFIG_COUNT:I

    if-le v4, v10, :cond_0

    goto :goto_1

    .line 73
    :pswitch_1
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mHubUrl:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_3

    .line 120
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "mapIndex":I
    .end local v7    # "raw":Ljava/lang/String;
    .end local v8    # "startIndex":I
    .end local v9    # "val":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 121
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 77
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "mapIndex":I
    .restart local v7    # "raw":Ljava/lang/String;
    .restart local v8    # "startIndex":I
    .restart local v9    # "val":Ljava/lang/String;
    :pswitch_2
    :try_start_3
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsStaging:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    .line 122
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "mapIndex":I
    .end local v7    # "raw":Ljava/lang/String;
    .end local v8    # "startIndex":I
    .end local v9    # "val":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 123
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 81
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "mapIndex":I
    .restart local v7    # "raw":Ljava/lang/String;
    .restart local v8    # "startIndex":I
    .restart local v9    # "val":Ljava/lang/String;
    :pswitch_3
    :try_start_4
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_3

    .line 124
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "mapIndex":I
    .end local v7    # "raw":Ljava/lang/String;
    .end local v8    # "startIndex":I
    .end local v9    # "val":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 125
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 84
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "mapIndex":I
    .restart local v7    # "raw":Ljava/lang/String;
    .restart local v8    # "startIndex":I
    .restart local v9    # "val":Ljava/lang/String;
    :pswitch_4
    :try_start_5
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    goto :goto_3

    .line 88
    :pswitch_5
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    goto :goto_3

    .line 91
    :pswitch_6
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    goto :goto_3

    .line 94
    :pswitch_7
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMCC:Ljava/lang/String;

    goto :goto_3

    .line 97
    :pswitch_8
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMNC:Ljava/lang/String;

    goto :goto_3

    .line 100
    :pswitch_9
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mModel:Ljava/lang/String;

    goto :goto_3

    .line 103
    :pswitch_a
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mCSC:Ljava/lang/String;

    goto :goto_3

    .line 106
    :pswitch_b
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    goto :goto_3

    .line 109
    :pswitch_c
    iput-object v9, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mAPILevel:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private coverLang(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 132
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 133
    .local v3, "splitedDecryptableString":[Ljava/lang/String;
    array-length v4, v3

    new-array v1, v4, [I

    .line 135
    .local v1, "decryptableIntArray":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    .line 136
    aget-object v4, v3, v2

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    aput v4, v1, v2

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    :cond_0
    const/4 v4, -0x5

    invoke-static {v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->shuffleIntArray([II)[I

    move-result-object v1

    .line 141
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->intArrayToByteArray([I)[B

    move-result-object v0

    .line 143
    .local v0, "decryptableByteArray":[B
    array-length v4, v0

    rem-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_1

    .line 145
    array-length v4, v0

    div-int/lit8 v4, v4, 0x2

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v0, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 149
    array-length v4, v0

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    invoke-static {v0, v6, v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 153
    :cond_1
    array-length v4, v0

    invoke-static {v0, v6, v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->swapCharacterArray([BII)[B

    move-result-object v0

    .line 157
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V

    return-object v4
.end method

.method private static intArrayToByteArray([I)[B
    .locals 3
    .param p0, "inputIntArray"    # [I

    .prologue
    .line 252
    array-length v2, p0

    new-array v1, v2, [B

    .line 253
    .local v1, "outputByteArray":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 254
    aget v2, p0, v0

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_0
    return-object v1
.end method

.method private static shuffleIntArray([II)[I
    .locals 3
    .param p0, "inputIntArray"    # [I
    .param p1, "shuffleNumber"    # I

    .prologue
    .line 261
    array-length v2, p0

    new-array v1, v2, [I

    .line 262
    .local v1, "outputIntArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 263
    aget v2, p0, v0

    add-int/2addr v2, p1

    aput v2, v1, v0

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    :cond_0
    return-object v1
.end method

.method private static swapCharacterArray([BII)[B
    .locals 5
    .param p0, "inputCharArray"    # [B
    .param p1, "swapStartPoint"    # I
    .param p2, "swapEndPoint"    # I

    .prologue
    .line 271
    array-length v1, p0

    .line 273
    .local v1, "stringLength":I
    if-lez p1, :cond_0

    if-ge p2, v1, :cond_0

    if-le p2, p1, :cond_0

    .line 275
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sub-int v3, p2, p1

    if-ge v0, v3, :cond_0

    .line 276
    add-int v3, v0, p1

    aget-byte v2, p0, v3

    .line 277
    .local v2, "temp":B
    add-int v3, v0, p1

    sub-int v4, p2, v0

    aget-byte v4, p0, v4

    aput-byte v4, p0, v3

    .line 279
    sub-int v3, p2, v0

    aput-byte v2, p0, v3

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    .end local v0    # "i":I
    .end local v2    # "temp":B
    :cond_0
    return-object p0
.end method


# virtual methods
.method public getCSC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mCSC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mCSC:Ljava/lang/String;

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHubUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    .line 169
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    .line 203
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMCC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMCC:Ljava/lang/String;

    .line 209
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMNC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMNC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mMNC:Ljava/lang/String;

    .line 215
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mModel:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mModel:Ljava/lang/String;

    .line 221
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getODCVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    .line 233
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOpenAPIVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    .line 240
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStageDataHostURL()Ljava/lang/String;
    .locals 3

    .prologue
    .line 192
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mschoi getStageDataHostURL   :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    .line 196
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStagingAppHostUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    .line 187
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStagingImgHostUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 174
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mschoi getStagingImgHostUrl   :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    .line 179
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExistSaconfig()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsSaconfig:Z

    return v0
.end method

.method public isUsingStageURL()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 244
    const-string v1, "1"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SaconfigInfoLoader;->mIsStaging:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 247
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
