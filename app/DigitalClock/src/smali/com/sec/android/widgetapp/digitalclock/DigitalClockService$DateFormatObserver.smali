.class Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;
.super Landroid/database/ContentObserver;
.source "DigitalClockService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DateFormatObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;->this$0:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    .line 145
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 146
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;->this$0:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "digitalclock.date_format_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->sendBroadcast(Landroid/content/Intent;)V

    .line 156
    return-void
.end method
