.class public Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "DigitalClockWidgetProvider.java"


# static fields
.field private static final ACTION_HOME_RESUME:Ljava/lang/String; = "com.sec.android.intent.action.HOME_RESUME"

.field private static final ACTION_SEC_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field private static final Shared_Pref:Ljava/lang/String; = "AllDigitalClockWidgetIDs"

.field private static final TAG:Ljava/lang/String; = "DigitalClockAppWidget"

.field static fromResize:Z

.field public static mIsSetTheme:Z

.field public static mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

.field private static mbNewClockStyle:Z

.field private static mbWidgetRunning:Z

.field static selectedWidgetID:I

.field private static widgetSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbWidgetRunning:Z

    .line 68
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    .line 72
    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->fromResize:Z

    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    .line 79
    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mIsSetTheme:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static IsHVGA(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1452
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1453
    .local v3, "mMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v2, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1454
    .local v2, "height":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1455
    .local v4, "width":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1456
    .local v1, "densitydpi":I
    const/16 v5, 0x1e0

    if-ne v2, v5, :cond_0

    const/16 v5, 0x140

    if-ne v4, v5, :cond_0

    const/16 v5, 0xa0

    if-ne v1, v5, :cond_0

    const/4 v0, 0x1

    .line 1458
    .local v0, "IsHVGA":Z
    :goto_0
    return v0

    .line 1456
    .end local v0    # "IsHVGA":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static drawAmPm(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "lancode"    # Ljava/lang/String;
    .param p4, "b24HourFormat"    # Z

    .prologue
    const v2, 0x7f0a0008

    const v7, 0x7f0a0009

    const/4 v6, 0x4

    const/4 v5, 0x0

    const v4, 0x7f0a000a

    .line 646
    if-eqz p4, :cond_1

    .line 647
    invoke-virtual {p1, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    invoke-virtual {p1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 652
    const/4 v0, 0x0

    .line 653
    .local v0, "bAm":Z
    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v2

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v3, 0xc

    if-ge v2, v3, :cond_2

    .line 654
    const/4 v0, 0x1

    .line 678
    :cond_2
    const-string v2, "zh"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "es"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ja"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ar"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 681
    :cond_3
    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 682
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 684
    if-eqz v0, :cond_8

    .line 685
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f080000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 695
    :goto_1
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 696
    :cond_5
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 697
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 706
    :cond_6
    :goto_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 707
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {p1, v7, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 708
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 687
    :cond_8
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 689
    :cond_9
    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 690
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 692
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "aaaa"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 693
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 700
    .end local v1    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_a
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_2
.end method

.method private static drawColon(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    const/16 v3, 0x8

    const v1, 0x7f0a0004

    const v2, 0x7f0a0018

    .line 500
    const v0, 0x7f020036

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 502
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 503
    :cond_0
    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 504
    const-string v0, ":"

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 505
    const/4 v0, 0x0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 507
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 508
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 518
    :cond_1
    :goto_0
    return-void

    .line 511
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_0

    .line 514
    :cond_3
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 515
    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 516
    invoke-static {p0, p2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockColonResourceId(Landroid/content/Context;Ljava/util/Date;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private static drawDateText(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V
    .locals 30
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "lancode"    # Ljava/lang/String;

    .prologue
    .line 878
    const/4 v6, 0x0

    .line 879
    .local v6, "bFirstMM":Z
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v23

    .line 880
    .local v23, "orders":[C
    const/4 v4, 0x0

    .line 881
    .local v4, "Date_dateMM":Ljava/lang/String;
    const/4 v5, 0x0

    .line 882
    .local v5, "Date_dateMM_ja":Ljava/lang/String;
    const/16 v25, 0x2f

    .line 883
    .local v25, "slash":C
    const/16 v26, 0x20

    .line 896
    .local v26, "space":C
    const-string v27, "ko"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_0

    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 897
    :cond_0
    const/4 v6, 0x1

    .line 914
    :cond_1
    const-string v27, "MMMM d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    .line 915
    .local v19, "mm12_kor":Ljava/lang/String;
    const-string v27, "dd"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    .line 916
    .local v16, "dd_fHD":Ljava/lang/String;
    const-string v27, "MM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    .line 918
    .local v20, "mm_fHD":Ljava/lang/String;
    const-string v27, "MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    .line 920
    .local v18, "mm12":Ljava/lang/String;
    const-string v27, "d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    .line 922
    .local v15, "dd12":Ljava/lang/String;
    const-string v27, "MMMM dd"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v21

    .line 926
    .local v21, "mmdd12_ja":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f080004

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 927
    .local v17, "digital_date":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f080008

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 929
    .local v22, "month":Ljava/lang/String;
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 930
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 944
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 945
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f080007

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 947
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 953
    :cond_2
    if-eqz v6, :cond_14

    .line 976
    const v27, 0x7f0a0016

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 978
    const v27, 0x7f0a0012

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 979
    const v27, 0x7f0a0013

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 981
    const v27, 0x7f0a0014

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 982
    const v27, 0x7f0a0015

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 984
    const-string v27, "ko"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_3

    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 986
    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    const-string v28, "date_format"

    invoke-static/range {v27 .. v28}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 989
    .local v7, "dateFormatSetting":Ljava/lang/String;
    const-string v27, "dd-MM-yyyy"

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 990
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "d"

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " MMMM"

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 991
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 992
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "d"

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "MM"

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 995
    :cond_4
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v27

    if-nez v27, :cond_5

    .line 996
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ","

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 999
    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 1000
    .local v24, "r":Landroid/content/res/Resources;
    sget-boolean v27, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v27, :cond_12

    .line 1001
    const-string v27, "ko"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 1005
    const v27, 0x7f0a0021

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1006
    const v27, 0x7f0a0021

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1011
    :goto_0
    const-string v27, "MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1012
    .local v12, "dayValueMonth":Ljava/lang/String;
    const-string v27, "yyyy"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1014
    .local v13, "dayValueYear":Ljava/lang/String;
    const v27, 0x7f0a001b

    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1015
    const v27, 0x7f0a001b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1016
    const v27, 0x7f0a0019

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1017
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_d

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_d

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_d

    .line 1018
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1024
    :cond_6
    :goto_1
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v27

    if-nez v27, :cond_7

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v27

    if-eqz v27, :cond_8

    .line 1025
    :cond_7
    const-string v27, "MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1026
    .local v8, "dayValue1":Ljava/lang/String;
    const-string v27, "d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1027
    .local v9, "dayValue2":Ljava/lang/String;
    const-string v27, "EEE"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1029
    .local v14, "day_chn":Ljava/lang/String;
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 1030
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f080007

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1031
    .local v10, "dayValue3":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v27

    if-eqz v27, :cond_f

    .line 1032
    const v27, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v14}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1211
    .end local v7    # "dateFormatSetting":Ljava/lang/String;
    .end local v8    # "dayValue1":Ljava/lang/String;
    .end local v9    # "dayValue2":Ljava/lang/String;
    .end local v10    # "dayValue3":Ljava/lang/String;
    .end local v12    # "dayValueMonth":Ljava/lang/String;
    .end local v13    # "dayValueYear":Ljava/lang/String;
    .end local v14    # "day_chn":Ljava/lang/String;
    .end local v24    # "r":Landroid/content/res/Resources;
    :cond_8
    :goto_2
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v27

    if-nez v27, :cond_9

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v27

    if-nez v27, :cond_a

    :cond_9
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v27

    if-nez v27, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v27

    if-nez v27, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 1212
    :cond_a
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2d

    .line 1213
    const v27, 0x7f0a0019

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f070007

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getColor(I)I

    move-result v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 1220
    :cond_b
    :goto_3
    return-void

    .line 1008
    .restart local v7    # "dateFormatSetting":Ljava/lang/String;
    .restart local v24    # "r":Landroid/content/res/Resources;
    :cond_c
    const v27, 0x7f0a0021

    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 1019
    .restart local v12    # "dayValueMonth":Ljava/lang/String;
    .restart local v13    # "dayValueYear":Ljava/lang/String;
    :cond_d
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_e

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_e

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_e

    .line 1020
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1021
    :cond_e
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_6

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_6

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_6

    .line 1022
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1034
    .restart local v8    # "dayValue1":Ljava/lang/String;
    .restart local v9    # "dayValue2":Ljava/lang/String;
    .restart local v10    # "dayValue3":Ljava/lang/String;
    .restart local v14    # "day_chn":Ljava/lang/String;
    :cond_f
    const-string v27, "dd-MM-yyyy"

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 1035
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1037
    :cond_10
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1041
    .end local v10    # "dayValue3":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f080004

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1042
    .restart local v10    # "dayValue3":Ljava/lang/String;
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1046
    .end local v8    # "dayValue1":Ljava/lang/String;
    .end local v9    # "dayValue2":Ljava/lang/String;
    .end local v10    # "dayValue3":Ljava/lang/String;
    .end local v12    # "dayValueMonth":Ljava/lang/String;
    .end local v13    # "dayValueYear":Ljava/lang/String;
    .end local v14    # "day_chn":Ljava/lang/String;
    :cond_12
    const v27, 0x7f0a000f

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1047
    const v27, 0x7f0a000f

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1051
    .end local v7    # "dateFormatSetting":Ljava/lang/String;
    .end local v24    # "r":Landroid/content/res/Resources;
    :cond_13
    const v27, 0x7f0a0016

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1089
    :cond_14
    sget-boolean v27, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v27, :cond_18

    .line 1101
    const v27, 0x7f0a0021

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1102
    const-string v27, "MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1103
    .restart local v12    # "dayValueMonth":Ljava/lang/String;
    const-string v27, "yyyy"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1108
    .restart local v13    # "dayValueYear":Ljava/lang/String;
    sget v27, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1d

    .line 1109
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1b

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1b

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1b

    .line 1110
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1128
    :cond_15
    :goto_4
    const v27, 0x7f0a001b

    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1129
    const v27, 0x7f0a001b

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1131
    const v27, 0x7f0a0019

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1136
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_20

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_20

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_20

    .line 1137
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1143
    :cond_16
    :goto_5
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v27

    if-nez v27, :cond_17

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v27

    if-eqz v27, :cond_18

    .line 1144
    :cond_17
    const-string v27, "ja"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_22

    .line 1145
    const-string v27, "d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1146
    const-string v27, "M"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1147
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1165
    .end local v12    # "dayValueMonth":Ljava/lang/String;
    .end local v13    # "dayValueYear":Ljava/lang/String;
    :cond_18
    :goto_6
    const v27, 0x7f0a0016

    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1166
    const v27, 0x7f0a0012

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1167
    const v27, 0x7f0a0013

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1169
    const v27, 0x7f0a0014

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1170
    const v27, 0x7f0a0015

    const/16 v28, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1179
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_27

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_27

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_27

    .line 1180
    const-string v27, "MMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1181
    .restart local v9    # "dayValue2":Ljava/lang/String;
    const-string v27, " d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1182
    .local v11, "dayValue4":Ljava/lang/String;
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_19

    const-string v27, "ja"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 1183
    :cond_19
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1185
    :cond_1a
    const v27, 0x7f0a0016

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1111
    .end local v9    # "dayValue2":Ljava/lang/String;
    .end local v11    # "dayValue4":Ljava/lang/String;
    .restart local v12    # "dayValueMonth":Ljava/lang/String;
    .restart local v13    # "dayValueYear":Ljava/lang/String;
    :cond_1b
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1c

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1c

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1c

    .line 1112
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1113
    :cond_1c
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    .line 1114
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1116
    :cond_1d
    sget v27, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    .line 1120
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1e

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1e

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1e

    .line 1121
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1122
    :cond_1e
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1f

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1f

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1f

    .line 1123
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1124
    :cond_1f
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_15

    .line 1125
    const v27, 0x7f0a0021

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1138
    :cond_20
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_21

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_21

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_21

    .line 1139
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1140
    :cond_21
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_16

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_16

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_16

    .line 1141
    const v27, 0x7f0a0019

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1149
    :cond_22
    const/4 v3, 0x0

    .line 1150
    .local v3, "DateMonth":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v27

    sget-object v28, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v27 .. v28}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_23

    .line 1151
    const-string v27, "MMMM d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1161
    :goto_7
    const v27, 0x7f0a0019

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1152
    :cond_23
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v27

    const-string v28, "iw"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_24

    .line 1153
    const-string v27, "d MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1154
    :cond_24
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v27

    const-string v28, "de"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_25

    .line 1155
    const-string v27, "d. MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1156
    :cond_25
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v27

    const-string v28, "fi"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_26

    .line 1157
    const-string v27, "d. MMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1159
    :cond_26
    const-string v27, "d MMMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_7

    .line 1187
    .end local v3    # "DateMonth":Ljava/lang/String;
    .end local v12    # "dayValueMonth":Ljava/lang/String;
    .end local v13    # "dayValueYear":Ljava/lang/String;
    :cond_27
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2a

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2a

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2a

    .line 1188
    const-string v27, " MMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1189
    .restart local v9    # "dayValue2":Ljava/lang/String;
    const-string v27, "d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1190
    .restart local v11    # "dayValue4":Ljava/lang/String;
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_28

    const-string v27, "ja"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_29

    .line 1191
    :cond_28
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1193
    :cond_29
    const v27, 0x7f0a0016

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1195
    .end local v9    # "dayValue2":Ljava/lang/String;
    .end local v11    # "dayValue4":Ljava/lang/String;
    :cond_2a
    const/16 v27, 0x0

    aget-char v27, v23, v27

    const/16 v28, 0x79

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    const/16 v27, 0x1

    aget-char v27, v23, v27

    const/16 v28, 0x4d

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    const/16 v27, 0x2

    aget-char v27, v23, v27

    const/16 v28, 0x64

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    .line 1196
    const-string v27, "MMM"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1197
    .restart local v9    # "dayValue2":Ljava/lang/String;
    const-string v27, " d"

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1198
    .restart local v11    # "dayValue4":Ljava/lang/String;
    const-string v27, "zh"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_2b

    const-string v27, "ja"

    move-object/from16 v0, v27

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2c

    .line 1199
    :cond_2b
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1201
    :cond_2c
    const v27, 0x7f0a0016

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1216
    .end local v9    # "dayValue2":Ljava/lang/String;
    .end local v11    # "dayValue4":Ljava/lang/String;
    :cond_2d
    const v27, 0x7f0a0019

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f070008

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getColor(I)I

    move-result v28

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_3
.end method

.method private static drawDayText(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "lancode"    # Ljava/lang/String;

    .prologue
    .line 714
    const v7, 0x7f0a000d

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 730
    const-string v7, "en"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 731
    const-string v7, "E,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 732
    .local v5, "df":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 733
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 734
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 735
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 737
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_1

    .line 738
    const-string v7, "EEEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 739
    .local v6, "df_fHd":Ljava/lang/CharSequence;
    sget v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 740
    const-string v7, "E"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 747
    :cond_0
    :goto_0
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 748
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 749
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 750
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 866
    .end local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_1
    :goto_1
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 867
    :cond_3
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1a

    .line 868
    const v7, 0x7f0a000f

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 875
    :cond_4
    :goto_2
    return-void

    .line 741
    .restart local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_5
    sget v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_0

    .line 742
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v7

    if-nez v7, :cond_6

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 743
    :cond_6
    const-string v7, "EEE,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    .line 745
    :cond_7
    const-string v7, "EEEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_0

    .line 753
    .end local v5    # "df":Ljava/lang/CharSequence;
    .end local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_8
    const-string v7, "ko"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 754
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 755
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 756
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 758
    const v7, 0x7f080002

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 759
    .local v4, "dayformat":Ljava/lang/String;
    invoke-static {v4, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 760
    .restart local v5    # "df":Ljava/lang/CharSequence;
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_9

    .line 761
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 762
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 764
    :cond_9
    const v7, 0x7f0a0016

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 766
    .end local v4    # "dayformat":Ljava/lang/String;
    .end local v5    # "df":Ljava/lang/CharSequence;
    :cond_a
    const-string v7, "zh"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 767
    const-string v7, "EEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 768
    .restart local v5    # "df":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 769
    const v7, 0x7f0a0010

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 770
    const v7, 0x7f0a000f

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 772
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_b

    .line 773
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 774
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 779
    :goto_3
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 780
    const-string v7, "MMMM"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 781
    .local v1, "dayValue1":Ljava/lang/String;
    const-string v7, "d"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 782
    .local v2, "dayValue2":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080007

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 783
    .local v3, "dayValue3":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "date_format"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 786
    .local v0, "dateFormatSetting":Ljava/lang/String;
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 787
    const v7, 0x7f0a000f

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 776
    .end local v0    # "dateFormatSetting":Ljava/lang/String;
    .end local v1    # "dayValue1":Ljava/lang/String;
    .end local v2    # "dayValue2":Ljava/lang/String;
    .end local v3    # "dayValue3":Ljava/lang/String;
    :cond_b
    const v7, 0x7f0a0010

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_3

    .line 789
    .restart local v0    # "dateFormatSetting":Ljava/lang/String;
    .restart local v1    # "dayValue1":Ljava/lang/String;
    .restart local v2    # "dayValue2":Ljava/lang/String;
    .restart local v3    # "dayValue3":Ljava/lang/String;
    :cond_c
    const v7, 0x7f0a000f

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 792
    .end local v0    # "dateFormatSetting":Ljava/lang/String;
    .end local v1    # "dayValue1":Ljava/lang/String;
    .end local v2    # "dayValue2":Ljava/lang/String;
    .end local v3    # "dayValue3":Ljava/lang/String;
    .end local v5    # "df":Ljava/lang/CharSequence;
    :cond_d
    const-string v7, "fr"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 793
    const-string v7, "EEE."

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 796
    .restart local v5    # "df":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 797
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 798
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 799
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 801
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_1

    .line 802
    const-string v7, "EEEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 803
    .restart local v6    # "df_fHd":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 804
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 805
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 806
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 808
    .end local v5    # "df":Ljava/lang/CharSequence;
    .end local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_e
    const-string v7, "ja"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 809
    const v7, 0x7f080003

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 810
    .restart local v4    # "dayformat":Ljava/lang/String;
    const-string v7, "E,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 812
    .restart local v5    # "df":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 813
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 814
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 815
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 817
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_1

    .line 818
    invoke-static {v4, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 819
    .restart local v6    # "df_fHd":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 820
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 821
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 822
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 825
    .end local v4    # "dayformat":Ljava/lang/String;
    .end local v5    # "df":Ljava/lang/CharSequence;
    .end local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_f
    const-string v7, "E,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 826
    .restart local v5    # "df":Ljava/lang/CharSequence;
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 827
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 828
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 829
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 831
    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v7, :cond_1

    .line 832
    const-string v7, "EEEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 833
    .restart local v6    # "df_fHd":Ljava/lang/CharSequence;
    sget v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_13

    .line 834
    const-string v7, "E"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 851
    :cond_10
    :goto_4
    const-string v7, "as"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "bn"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "gu"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "hi"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "kn"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "ml"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "mr"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "ne"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "or"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "pa"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "si"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "ta"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    const-string v7, "te"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 856
    :cond_11
    const-string v7, "EEEE,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 860
    :cond_12
    :goto_5
    const v7, 0x7f0a000e

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 861
    const v7, 0x7f0a0010

    const/4 v8, 0x4

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 862
    const v7, 0x7f0a000f

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 863
    const v7, 0x7f0a000f

    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 835
    :cond_13
    sget v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_10

    .line 836
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v7

    if-nez v7, :cond_14

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v7

    if-eqz v7, :cond_18

    .line 837
    :cond_14
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "iw"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 838
    const-string v7, "EEE,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_4

    .line 839
    :cond_15
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ar"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_16

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "fa"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_16

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ur"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 842
    :cond_16
    const-string v7, "EEE\u060c"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_4

    .line 844
    :cond_17
    const-string v7, "EEE,"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_4

    .line 847
    :cond_18
    const-string v7, "EEEE"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_4

    .line 857
    :cond_19
    const-string v7, "pl"

    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 858
    const-string v7, "E"

    invoke-static {v7, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_5

    .line 871
    .end local v6    # "df_fHd":Ljava/lang/CharSequence;
    :cond_1a
    const v7, 0x7f0a000f

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07000a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {p1, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_2
.end method

.method private static drawHour(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Z)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "b24HourFormat"    # Z

    .prologue
    .line 331
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    .line 332
    .local v11, "is_359hdpi":Z
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v12

    .line 334
    .local v12, "is_360xhdpi":Z
    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v9

    .line 335
    .local v9, "hour":I
    move v10, v9

    .line 337
    .local v10, "hours":I
    if-nez p1, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    if-eqz p3, :cond_6

    .line 342
    div-int/lit8 v7, v9, 0xa

    .line 343
    .local v7, "hh1":I
    rem-int/lit8 v8, v9, 0xa

    .line 344
    .local v8, "hh2":I
    const v0, 0x7f0a0008

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 384
    :goto_1
    const v0, 0x7f0a0003

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 386
    if-nez p3, :cond_d

    if-nez v7, :cond_d

    .line 387
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 388
    const v0, 0x7f0a0001

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 389
    const v0, 0x7f0a0003

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 395
    :goto_2
    if-eqz v11, :cond_c

    .line 396
    const v0, 0x7f0a0001

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 397
    const/high16 v1, 0x7f0a0000

    const/16 v2, 0x21

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 432
    :cond_2
    :goto_3
    const v0, 0x7f0a0002

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 440
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 441
    const v0, 0x7f0a0002

    invoke-static {p0, p2, v8}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(Landroid/content/Context;Ljava/util/Date;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 454
    :goto_4
    sget-boolean v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    if-eqz v0, :cond_4

    .line 455
    const/4 v6, -0x1

    .line 456
    .local v6, "background":I
    const/4 v0, 0x6

    if-lt v10, v0, :cond_3

    const/16 v0, 0x12

    if-lt v10, v0, :cond_11

    .line 457
    :cond_3
    const v6, 0x7f020001

    .line 472
    :goto_5
    sget v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_12

    .line 473
    const v0, 0x7f0a0020

    invoke-virtual {p1, v0, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 478
    .end local v6    # "background":I
    :cond_4
    :goto_6
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    :cond_5
    const v0, 0x7f0a0003

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 480
    const v0, 0x7f0a0001

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 481
    const v0, 0x7f0a0002

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 482
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_13

    .line 483
    const v0, 0x7f0a0003

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_0

    .line 346
    .end local v7    # "hh1":I
    .end local v8    # "hh2":I
    :cond_6
    if-eqz v9, :cond_7

    const/16 v0, 0xc

    if-ne v9, v0, :cond_9

    .line 347
    :cond_7
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 348
    const/4 v7, 0x0

    .line 349
    .restart local v7    # "hh1":I
    const/4 v8, 0x0

    .restart local v8    # "hh2":I
    goto/16 :goto_1

    .line 351
    .end local v7    # "hh1":I
    .end local v8    # "hh2":I
    :cond_8
    const/4 v7, 0x1

    .line 352
    .restart local v7    # "hh1":I
    const/4 v8, 0x2

    .restart local v8    # "hh2":I
    goto/16 :goto_1

    .line 354
    .end local v7    # "hh1":I
    .end local v8    # "hh2":I
    :cond_9
    if-lez v9, :cond_a

    const/16 v0, 0xc

    if-ge v9, v0, :cond_a

    .line 355
    div-int/lit8 v7, v9, 0xa

    .line 356
    .restart local v7    # "hh1":I
    rem-int/lit8 v8, v9, 0xa

    .restart local v8    # "hh2":I
    goto/16 :goto_1

    .line 358
    .end local v7    # "hh1":I
    .end local v8    # "hh2":I
    :cond_a
    add-int/lit8 v9, v9, -0xc

    .line 359
    div-int/lit8 v7, v9, 0xa

    .line 360
    .restart local v7    # "hh1":I
    rem-int/lit8 v8, v9, 0xa

    .restart local v8    # "hh2":I
    goto/16 :goto_1

    .line 391
    :cond_b
    const v0, 0x7f0a0003

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 392
    const v0, 0x7f0a0001

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 398
    :cond_c
    if-eqz v12, :cond_2

    .line 399
    const v1, 0x7f0a000a

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x22

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_3

    .line 415
    :cond_d
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 416
    const v0, 0x7f0a0003

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 417
    const v0, 0x7f0a0001

    invoke-static {p0, p2, v7}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(Landroid/content/Context;Ljava/util/Date;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 424
    :goto_7
    const v0, 0x7f0a0001

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 425
    if-eqz v11, :cond_f

    .line 426
    const/high16 v1, 0x7f0a0000

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_3

    .line 419
    :cond_e
    const v0, 0x7f0a0003

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 420
    const v0, 0x7f0a0001

    invoke-static {v7}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_7

    .line 427
    :cond_f
    if-eqz v12, :cond_2

    .line 428
    const v1, 0x7f0a000a

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_3

    .line 443
    :cond_10
    const v0, 0x7f0a0002

    invoke-static {v8}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    .line 459
    .restart local v6    # "background":I
    :cond_11
    const/high16 v6, 0x7f020000

    goto/16 :goto_5

    .line 475
    :cond_12
    const v0, 0x7f0a0017

    invoke-virtual {p1, v0, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_6

    .line 486
    .end local v6    # "background":I
    :cond_13
    const v0, 0x7f0a0003

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_0
.end method

.method private static drawMinute(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0a0006

    const v7, 0x7f0a0005

    const/16 v6, 0x8

    const v5, 0x7f0a0007

    .line 521
    invoke-virtual {p2}, Ljava/util/Date;->getMinutes()I

    move-result v0

    .line 526
    .local v0, "minute":I
    div-int/lit8 v1, v0, 0xa

    .line 527
    .local v1, "mm1":I
    rem-int/lit8 v2, v0, 0xa

    .line 533
    .local v2, "mm2":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 534
    invoke-virtual {p1, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 535
    invoke-virtual {p1, v7, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 536
    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 548
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isT()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isK()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 549
    invoke-virtual {p1, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 550
    invoke-static {p0, p2, v1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(Landroid/content/Context;Ljava/util/Date;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 551
    invoke-static {p0, p2, v2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(Landroid/content/Context;Ljava/util/Date;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p1, v8, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 559
    :goto_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 560
    :cond_0
    invoke-virtual {p1, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 561
    invoke-virtual {p1, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 562
    invoke-virtual {p1, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 564
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 565
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 580
    :cond_1
    :goto_1
    return-void

    .line 553
    :cond_2
    invoke-static {v1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(I)I

    move-result v3

    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 554
    invoke-static {v2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockNumberResourceId(I)I

    move-result v3

    invoke-virtual {p1, v8, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 568
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_1
.end method

.method private static drawTime(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "b24HourFormat"    # Z

    .prologue
    .line 584
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 585
    .local v1, "calender":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    .line 588
    .local v7, "now":Ljava/util/Date;
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "HH"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 589
    .local v3, "hour":I
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 591
    .local v5, "min":I
    if-nez p2, :cond_0

    .line 592
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "hh"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v8, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 595
    :cond_0
    const v8, 0x7f0a0003

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 596
    const/4 v4, 0x0

    .line 597
    .local v4, "hourString":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_1

    .line 598
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    rem-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 615
    :goto_0
    const-string v2, ":"

    .line 616
    .local v2, "colon":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v9, v5, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    rem-int/lit8 v9, v5, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 618
    .local v6, "minuteString":Ljava/lang/StringBuilder;
    const v8, 0x7f0a0003

    invoke-virtual {p1, v8, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 619
    const v8, 0x7f0a0004

    invoke-virtual {p1, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 620
    const v8, 0x7f0a0007

    invoke-virtual {p1, v8, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 622
    const/4 v0, -0x1

    .line 623
    .local v0, "background":I
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDay()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 624
    const/high16 v0, 0x7f020000

    .line 625
    const v8, 0x7f0a0003

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 627
    const v8, 0x7f0a0004

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 629
    const v8, 0x7f0a0007

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070002

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 641
    :goto_1
    const v8, 0x7f0a0017

    invoke-virtual {p1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 642
    return-void

    .line 600
    .end local v0    # "background":I
    .end local v2    # "colon":Ljava/lang/String;
    .end local v6    # "minuteString":Ljava/lang/StringBuilder;
    :cond_1
    const/16 v8, 0x9

    if-le v3, v8, :cond_5

    .line 601
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 602
    if-eqz v3, :cond_2

    const/16 v8, 0xc

    if-ne v3, v8, :cond_3

    .line 603
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/16 :goto_0

    .line 605
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    rem-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/16 :goto_0

    .line 608
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    rem-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/16 :goto_0

    .line 612
    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v9, v3, 0xa

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/16 :goto_0

    .line 632
    .restart local v0    # "background":I
    .restart local v2    # "colon":Ljava/lang/String;
    .restart local v6    # "minuteString":Ljava/lang/StringBuilder;
    :cond_6
    const v0, 0x7f020001

    .line 633
    const v8, 0x7f0a0003

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070003

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 635
    const v8, 0x7f0a0004

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070003

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 637
    const v8, 0x7f0a0007

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070003

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1
.end method

.method private static getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "bitmapId"    # Ljava/lang/String;

    .prologue
    .line 1312
    sget-boolean v2, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mIsSetTheme:Z

    if-nez v2, :cond_0

    .line 1313
    const/4 v0, 0x0

    .line 1322
    :goto_0
    return-object v0

    .line 1315
    :cond_0
    const/4 v0, 0x0

    .line 1317
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v2, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v2, p0}, Lcom/samsung/android/theme/SThemeManager;->getItemBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1318
    :catch_0
    move-exception v1

    .line 1319
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v2, "DigitalClockAppWidget"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getClockBitmap( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) failed! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getClockColonResourceId(Landroid/content/Context;Ljava/util/Date;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 316
    invoke-virtual {p1}, Ljava/util/Date;->getHours()I

    move-result v1

    .line 317
    .local v1, "hour":I
    const/4 v0, 0x0

    .line 319
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 320
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001e

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 325
    :goto_0
    return-object v0

    .line 322
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020013

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private static getClockNumber2X1ResourceBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "number"    # I

    .prologue
    .line 1355
    packed-switch p0, :pswitch_data_0

    .line 1377
    const-string v0, "clock_num_small_0"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1358
    :pswitch_0
    const-string v0, "clock_num_small_1"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1360
    :pswitch_1
    const-string v0, "clock_num_small_2"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1362
    :pswitch_2
    const-string v0, "clock_num_small_3"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1364
    :pswitch_3
    const-string v0, "clock_num_small_4"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1366
    :pswitch_4
    const-string v0, "clock_num_small_5"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1368
    :pswitch_5
    const-string v0, "clock_num_small_6"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1370
    :pswitch_6
    const-string v0, "clock_num_small_7"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1372
    :pswitch_7
    const-string v0, "clock_num_small_8"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1374
    :pswitch_8
    const-string v0, "clock_num_small_9"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1355
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static getClockNumberResourceBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "number"    # I

    .prologue
    .line 1327
    packed-switch p0, :pswitch_data_0

    .line 1349
    const-string v0, "clock_num_0"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1330
    :pswitch_0
    const-string v0, "clock_num_1"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1332
    :pswitch_1
    const-string v0, "clock_num_2"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1334
    :pswitch_2
    const-string v0, "clock_num_3"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1336
    :pswitch_3
    const-string v0, "clock_num_4"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1338
    :pswitch_4
    const-string v0, "clock_num_5"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1340
    :pswitch_5
    const-string v0, "clock_num_6"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1342
    :pswitch_6
    const-string v0, "clock_num_7"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1344
    :pswitch_7
    const-string v0, "clock_num_8"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1346
    :pswitch_8
    const-string v0, "clock_num_9"

    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getClockBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1327
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static getClockNumberResourceId(I)I
    .locals 4
    .param p0, "number"    # I

    .prologue
    .line 1249
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1252
    .local v0, "date":Ljava/util/Date;
    const v1, 0x7f020021

    add-int/2addr v1, p0

    return v1
.end method

.method private static getClockNumberResourceId(Landroid/content/Context;Ljava/util/Date;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;
    .param p2, "number"    # I

    .prologue
    .line 302
    invoke-virtual {p1}, Ljava/util/Date;->getHours()I

    move-result v1

    .line 303
    .local v1, "hour":I
    const/4 v0, 0x0

    .line 305
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 306
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020014

    add-int/2addr v3, p2

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 311
    :goto_0
    return-object v0

    .line 308
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020009

    add-int/2addr v3, p2

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static getWidgetRunning()Z
    .locals 1

    .prologue
    .line 177
    sget-boolean v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbWidgetRunning:Z

    return v0
.end method

.method private static isDay()Z
    .locals 4

    .prologue
    .line 1462
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1463
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getHours()I

    move-result v1

    .line 1464
    .local v1, "hours":I
    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 1465
    :cond_0
    const/4 v2, 0x0

    .line 1467
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1275
    const/16 v0, 0x64

    .line 1276
    .local v0, "MAX_NUM":I
    const/4 v1, 0x0

    .line 1278
    .local v1, "found":Z
    const/4 v5, 0x0

    .line 1279
    .local v5, "service_list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    const-string v6, "activity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    .line 1282
    .local v4, "manager":Landroid/app/ActivityManager;
    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v5

    .line 1283
    if-nez v5, :cond_0

    move v2, v1

    .line 1296
    .end local v1    # "found":Z
    .local v2, "found":I
    :goto_0
    return v2

    .line 1286
    .end local v2    # "found":I
    .restart local v1    # "found":Z
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1287
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1288
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v6, v6, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.sec.android.widgetapp.digitalclock.DigitalClockService"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1290
    const/4 v1, 0x1

    :cond_2
    move v2, v1

    .line 1296
    .restart local v2    # "found":I
    goto :goto_0
.end method

.method private static isK()Z
    .locals 2

    .prologue
    .line 1483
    const-string v0, "ro.build.scafe"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "americano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.build.scafe"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "capuccino"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "m2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1486
    :cond_0
    const/4 v0, 0x1

    .line 1488
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLargeFont(Landroid/content/Context;Ljava/util/Date;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 1223
    const/16 v1, 0x20

    .line 1224
    .local v1, "TEXT_SIZE":I
    const/16 v3, 0x62

    .line 1225
    .local v3, "WIDTH_IN_PORTRAIT":I
    const/16 v2, 0x50

    .line 1226
    .local v2, "WIDTH_IN_LAND":I
    const/4 v0, 0x0

    .line 1228
    .local v0, "PORTRAIT":I
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 1230
    .local v4, "paint":Landroid/graphics/Paint;
    const/high16 v8, 0x42000000    # 32.0f

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1232
    const-string v8, "EEEEEEEEEEEEEE"

    invoke-static {v8, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v6

    .line 1234
    .local v6, "widthText":F
    const-string v8, "window"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 1237
    .local v7, "wm":Landroid/view/WindowManager;
    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getOrientation()I

    move-result v8

    if-nez v8, :cond_0

    .line 1238
    const/16 v5, 0x62

    .line 1242
    .local v5, "width":I
    :goto_0
    int-to-float v8, v5

    cmpl-float v8, v6, v8

    if-lez v8, :cond_1

    .line 1243
    const/4 v8, 0x0

    .line 1245
    :goto_1
    return v8

    .line 1240
    .end local v5    # "width":I
    :cond_0
    const/16 v5, 0x50

    .restart local v5    # "width":I
    goto :goto_0

    .line 1245
    :cond_1
    const/4 v8, 0x1

    goto :goto_1
.end method

.method private static isQVGA(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1441
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1442
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1443
    .local v1, "height":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1444
    .local v4, "width":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v0, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1445
    .local v0, "densityDpi":I
    const/16 v5, 0x140

    if-ne v1, v5, :cond_0

    const/16 v5, 0xf0

    if-ne v4, v5, :cond_0

    const/16 v5, 0x78

    if-ne v0, v5, :cond_0

    const/4 v2, 0x1

    .line 1447
    .local v2, "isQVGA":Z
    :goto_0
    return v2

    .line 1445
    .end local v2    # "isQVGA":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isSetTheme(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1301
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "current_sec_theme_package"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1304
    .local v0, "themePackageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1305
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mIsSetTheme:Z

    .line 1308
    :goto_0
    return-void

    .line 1307
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mIsSetTheme:Z

    goto :goto_0
.end method

.method private static isT()Z
    .locals 2

    .prologue
    .line 1472
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1474
    :cond_0
    const/4 v0, 0x1

    .line 1476
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isWVGA(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1430
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1431
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1432
    .local v1, "height":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1433
    .local v4, "width":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v0, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1434
    .local v0, "densityDpi":I
    const/16 v5, 0x320

    if-ne v1, v5, :cond_0

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_0

    const/16 v5, 0xf0

    if-ne v0, v5, :cond_0

    const/4 v2, 0x1

    .line 1436
    .local v2, "isWVGA":Z
    :goto_0
    return v2

    .line 1434
    .end local v2    # "isWVGA":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static setDescription(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;ZLjava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "is24Hour"    # Z
    .param p4, "lancode"    # Ljava/lang/String;

    .prologue
    .line 1403
    const/4 v0, 0x0

    .line 1405
    .local v0, "readDesciption":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1407
    .local v1, "textDate":Ljava/lang/String;
    const-string v2, "ko"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1408
    if-eqz p3, :cond_0

    .line 1409
    new-instance v2, Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MMM dd"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", EEEE, kk:mm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1426
    :goto_0
    const v2, 0x7f0a001a

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 1427
    return-void

    .line 1412
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MMM dd"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", EEEE, aaa KK:mm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1414
    :cond_1
    const-string v2, "it"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1415
    if-eqz p3, :cond_2

    .line 1416
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEEE, MMM dd, kk\'E\' mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1418
    :cond_2
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEEE, MMM dd, KK \'E\' mm aaa"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1420
    :cond_3
    if-eqz p3, :cond_4

    .line 1421
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEEE, MMM dd, kk mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1423
    :cond_4
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEEE, MMM dd, KK mm aaa"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static setThemeTextColor(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 5
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "resourceId"    # I
    .param p2, "colorId"    # Ljava/lang/String;

    .prologue
    .line 1382
    sget-boolean v2, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mIsSetTheme:Z

    if-nez v2, :cond_1

    .line 1398
    :cond_0
    :goto_0
    return-void

    .line 1385
    :cond_1
    if-eqz p0, :cond_0

    .line 1389
    const/4 v0, 0x0

    .line 1391
    .local v0, "color":I
    :try_start_0
    sget-object v2, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v2, p2}, Lcom/samsung/android/theme/SThemeManager;->getItemColor(Ljava/lang/String;)I

    move-result v0

    .line 1392
    if-eqz v0, :cond_0

    .line 1393
    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setTextColor(II)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1395
    :catch_0
    move-exception v1

    .line 1396
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v2, "DigitalClockAppWidget"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setThemeTextColor( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ) failed! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aw"    # Landroid/appwidget/AppWidgetManager;

    .prologue
    .line 191
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v1, "provider":Landroid/content/ComponentName;
    invoke-virtual {p1, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 201
    .local v2, "widgetIds":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 202
    aget v3, v2, v0

    invoke-static {p0, p1, v3}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method private static updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aw"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "widgetIds"    # I

    .prologue
    .line 212
    const-string v8, "ro.csc.country_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    .local v1, "country_code":Ljava/lang/String;
    const/4 v7, 0x0

    .line 215
    .local v7, "views":Landroid/widget/RemoteViews;
    const-string v8, "AllDigitalClockWidgetIDs"

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 216
    .local v6, "pref":Landroid/content/SharedPreferences;
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    .line 217
    sget v8, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    if-nez v8, :cond_0

    .line 218
    const/4 v8, 0x4

    sput v8, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    .line 242
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v2, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 243
    .local v2, "date":Ljava/util/Date;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v5, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 244
    .local v5, "locale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "lancode":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    .line 247
    .local v0, "b24HourFormat":Z
    sget v8, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_6

    .line 248
    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isWVGA(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isQVGA(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->IsHVGA(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 249
    :cond_1
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/high16 v9, 0x7f030000

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 270
    .restart local v7    # "views":Landroid/widget/RemoteViews;
    :goto_0
    invoke-static {p0, v7, v2, v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawHour(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Z)V

    .line 271
    invoke-static {p0, v7, v2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawColon(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;)V

    .line 272
    invoke-static {p0, v7, v2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawMinute(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;)V

    .line 273
    invoke-static {p0, v7, v2, v4, v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawAmPm(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;Z)V

    .line 274
    invoke-static {p0, v7, v2, v4}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawDayText(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V

    .line 275
    invoke-static {p0, v7, v2, v4}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->drawDateText(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V

    .line 277
    invoke-static {p0, v7, v2, v0, v4}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->setDescription(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;ZLjava/lang/String;)V

    .line 279
    const-string v8, "CHINA"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 280
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 281
    .local v3, "i":Landroid/content/Intent;
    const-string v8, "android.settings.DATE_SETTINGS"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    const/high16 v8, 0x7f0a0000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {p0, v9, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 284
    const v8, 0x7f0a0008

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {p0, v9, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 286
    const v8, 0x7f0a000d

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {p0, v9, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 290
    .end local v3    # "i":Landroid/content/Intent;
    :cond_2
    if-eqz p1, :cond_3

    .line 291
    invoke-virtual {p1, p2, v7}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 294
    :cond_3
    return-void

    .line 251
    :cond_4
    if-eqz v0, :cond_5

    .line 252
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030001

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto :goto_0

    .line 254
    :cond_5
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/high16 v9, 0x7f030000

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto :goto_0

    .line 257
    :cond_6
    sget v8, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_8

    .line 258
    if-eqz v0, :cond_7

    .line 259
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030002

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto :goto_0

    .line 262
    :cond_7
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030005

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto/16 :goto_0

    .line 264
    :cond_8
    if-eqz v0, :cond_9

    .line 265
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f030001

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto/16 :goto_0

    .line 267
    :cond_9
    new-instance v7, Landroid/widget/RemoteViews;

    .end local v7    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/high16 v9, 0x7f030000

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .restart local v7    # "views":Landroid/widget/RemoteViews;
    goto/16 :goto_0
.end method

.method private static updateWidget(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    .locals 2
    .param p0, "aw"    # Landroid/appwidget/AppWidgetManager;
    .param p1, "widgetIds"    # [I
    .param p2, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 181
    if-eqz p0, :cond_0

    .line 182
    if-eqz p1, :cond_0

    .line 183
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 184
    aget v1, p1, v0

    invoke-virtual {p0, v1, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 118
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 124
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbWidgetRunning:Z

    .line 127
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 130
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 85
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbWidgetRunning:Z

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->mbNewClockStyle:Z

    .line 89
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 92
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 134
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 140
    .local v0, "awm":Landroid/appwidget/AppWidgetManager;
    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;

    invoke-direct {v6, p1, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v6}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v5

    .line 143
    .local v5, "widgetIDs":[I
    if-eqz v5, :cond_2

    array-length v6, v5

    if-eqz v6, :cond_2

    .line 144
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 146
    new-instance v2, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v2, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .local v2, "intentService":Landroid/content/Intent;
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 152
    .end local v2    # "intentService":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 153
    if-eqz v5, :cond_3

    array-length v6, v5

    if-eqz v6, :cond_3

    .line 154
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 173
    :cond_3
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 156
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 157
    const-string v6, "widgetspanx"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 158
    .local v4, "spanX":I
    sput v4, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    .line 160
    if-eqz v5, :cond_3

    array-length v6, v5

    if-eqz v6, :cond_3

    .line 161
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->fromResize:Z

    .line 162
    const-string v6, "widgetId"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    sput v6, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->selectedWidgetID:I

    .line 164
    const-string v6, "AllDigitalClockWidgetIDs"

    invoke-virtual {p1, v6, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 166
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 167
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    sget v6, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->selectedWidgetID:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->widgetSize:I

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 168
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 169
    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    goto :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 96
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 98
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 99
    .local v0, "awm":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;

    invoke-direct {v3, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 102
    .local v2, "widgetIDs":[I
    if-eqz v2, :cond_0

    array-length v3, v2

    if-eqz v3, :cond_0

    .line 104
    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 106
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 108
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v1, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 113
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
