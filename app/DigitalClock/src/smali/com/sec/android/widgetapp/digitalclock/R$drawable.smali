.class public final Lcom/sec/android/widgetapp/digitalclock/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/digitalclock/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final clock_bg:I = 0x7f020000

.field public static final clock_bg2:I = 0x7f020001

.field public static final clock_divide_line_v:I = 0x7f020002

.field public static final clock_line:I = 0x7f020003

.field public static final clock_preview_4x1_12:I = 0x7f020004

.field public static final clock_preview_4x1_12k:I = 0x7f020005

.field public static final clock_preview_4x1_24:I = 0x7f020006

.field public static final clock_preview_4x1_24k:I = 0x7f020007

.field public static final preview_widget_single_digital_clock:I = 0x7f020008

.field public static final single_clock_day_0:I = 0x7f020009

.field public static final single_clock_day_1:I = 0x7f02000a

.field public static final single_clock_day_2:I = 0x7f02000b

.field public static final single_clock_day_3:I = 0x7f02000c

.field public static final single_clock_day_4:I = 0x7f02000d

.field public static final single_clock_day_5:I = 0x7f02000e

.field public static final single_clock_day_6:I = 0x7f02000f

.field public static final single_clock_day_7:I = 0x7f020010

.field public static final single_clock_day_8:I = 0x7f020011

.field public static final single_clock_day_9:I = 0x7f020012

.field public static final single_clock_day_colon:I = 0x7f020013

.field public static final single_clock_night_0:I = 0x7f020014

.field public static final single_clock_night_1:I = 0x7f020015

.field public static final single_clock_night_2:I = 0x7f020016

.field public static final single_clock_night_3:I = 0x7f020017

.field public static final single_clock_night_4:I = 0x7f020018

.field public static final single_clock_night_5:I = 0x7f020019

.field public static final single_clock_night_6:I = 0x7f02001a

.field public static final single_clock_night_7:I = 0x7f02001b

.field public static final single_clock_night_8:I = 0x7f02001c

.field public static final single_clock_night_9:I = 0x7f02001d

.field public static final single_clock_night_colon:I = 0x7f02001e

.field public static final singleclock_date_divider:I = 0x7f02001f

.field public static final singleclock_horizontal_line:I = 0x7f020020

.field public static final singleclock_time_0:I = 0x7f020021

.field public static final singleclock_time_1:I = 0x7f020022

.field public static final singleclock_time_2:I = 0x7f020023

.field public static final singleclock_time_3:I = 0x7f020024

.field public static final singleclock_time_4:I = 0x7f020025

.field public static final singleclock_time_5:I = 0x7f020026

.field public static final singleclock_time_6:I = 0x7f020027

.field public static final singleclock_time_7:I = 0x7f020028

.field public static final singleclock_time_8:I = 0x7f020029

.field public static final singleclock_time_9:I = 0x7f02002a

.field public static final singleclock_time_d_0:I = 0x7f02002b

.field public static final singleclock_time_d_1:I = 0x7f02002c

.field public static final singleclock_time_d_2:I = 0x7f02002d

.field public static final singleclock_time_d_3:I = 0x7f02002e

.field public static final singleclock_time_d_4:I = 0x7f02002f

.field public static final singleclock_time_d_5:I = 0x7f020030

.field public static final singleclock_time_d_6:I = 0x7f020031

.field public static final singleclock_time_d_7:I = 0x7f020032

.field public static final singleclock_time_d_8:I = 0x7f020033

.field public static final singleclock_time_d_9:I = 0x7f020034

.field public static final singleclock_time_d_dot:I = 0x7f020035

.field public static final singleclock_time_dot:I = 0x7f020036

.field public static final singleclock_vertical_line:I = 0x7f020037

.field public static final widget_bg:I = 0x7f020038

.field public static final widget_shadow:I = 0x7f020039


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
