.class public Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;
.super Landroid/app/Service;
.source "DigitalClockService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;
    }
.end annotation


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "digitalclock.date_format_changed"

.field static final HOME_THEME_CHANGED:Ljava/lang/String; = "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

.field static final START:Ljava/lang/String; = "digitalclock.action.startservice"

.field static final STOP:Ljava/lang/String; = "digitalclock.action.stopservice"

.field private static final TAG:Ljava/lang/String; = "DigitalClockAppWidgetService"


# instance fields
.field private mDateFormatObserver:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    new-instance v0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$1;-><init>(Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 143
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 89
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 161
    iget-object v1, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 169
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->getWidgetRunning()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 170
    const-string v1, "DigitalClockAppWidgetService"

    const-string v2, "onDestroy : The widget is alive. service will be reStarted!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 174
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 177
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 176
    :cond_0
    const-string v1, "DigitalClockAppWidgetService"

    const-string v2, "onDestroy : End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x1

    .line 93
    const-string v3, "DigitalClockAppWidgetService"

    const-string v4, "onStartCommand: register Receiver"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 96
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    const-string v3, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 98
    const-string v3, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 101
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 102
    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    const-string v3, "digitalclock.date_format_changed"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 111
    const-string v3, "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    iget-object v3, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    new-instance v3, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;-><init>(Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;)V

    iput-object v3, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://settings/system/date_format"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->mDateFormatObserver:Lcom/sec/android/widgetapp/digitalclock/DigitalClockService$DateFormatObserver;

    invoke-virtual {v3, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 126
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 128
    const-string v3, "DigitalClockAppWidgetService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartCommand intent = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-nez p1, :cond_0

    .line 132
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/digitalclock/DigitalClockService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 136
    .end local v2    # "i":Landroid/content/Intent;
    :cond_0
    return v6
.end method
