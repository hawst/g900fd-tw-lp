.class Lcom/samsung/android/beaconmanager/network/BleHelper$3;
.super Ljava/lang/Object;
.source "BleHelper.java"

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/network/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$3;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # [B

    .prologue
    .line 316
    if-nez p1, :cond_0

    .line 317
    const-string v0, "BleHelper"

    const-string v1, "LeScanCallback"

    const-string v2, "Ignore  device is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :goto_0
    return-void

    .line 320
    :cond_0
    const-string v0, "BleHelper"

    const-string v1, "LeScanCallback"

    const-string v2, "receive packet"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$3;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # invokes: Lcom/samsung/android/beaconmanager/network/BleHelper;->parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$000(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/BluetoothDevice;I[B)V

    goto :goto_0
.end method
