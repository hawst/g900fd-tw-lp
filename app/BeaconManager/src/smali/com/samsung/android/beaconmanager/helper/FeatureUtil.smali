.class public Lcom/samsung/android/beaconmanager/helper/FeatureUtil;
.super Ljava/lang/Object;
.source "FeatureUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FeatureUtil"

.field private static isBleScanFilterSupported:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isBleAutoEnable()Z
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_BLUETOOTH_CONFIG_LE_AUTO_ENABLE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isBleScanFilterSupported()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 20
    sget v4, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 22
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isOffloadedFilteringSupported()Z

    move-result v1

    .line 23
    .local v1, "isoffloading":Z
    if-eqz v1, :cond_1

    .line 24
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : enabled"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const/4 v4, 0x1

    sput v4, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :cond_0
    :goto_0
    sget v4, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I

    if-ne v4, v2, :cond_2

    .line 38
    :goto_1
    return v2

    .line 27
    :cond_1
    :try_start_1
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : disabled"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const/4 v4, 0x0

    sput v4, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v4, "FeatureUtil"

    const-string v5, "isBleScanFilterSupported"

    const-string v6, "isOffloadedFilteringSupported() : NoSuchMethodError"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    sput v3, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported:I

    goto :goto_0

    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_2
    move v2, v3

    .line 38
    goto :goto_1
.end method
