.class public Lcom/samsung/android/beaconmanager/BeaconManager;
.super Ljava/lang/Object;
.source "BeaconManager.java"


# static fields
.field public static final SEARCH_METHOD_CONTACT:I = 0x4

.field public static final SEARCH_METHOD_RSSI:I = 0x1

.field public static final SEARCH_METHOD_SOUND:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BeaconManager"

.field private static volatile mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;


# instance fields
.field private mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

.field private mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

.field public mBlePacketReceiveListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

.field private mBleProxyCallbackHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mPackageChangedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

.field private mQuickConnectSettingObserver:Landroid/database/ContentObserver;

.field private mRegisterdAppHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<[B",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRegisterdProxyHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<[B",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mScanFinishListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

.field private mScanRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanRunning:Z

    .line 31
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    .line 33
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanFinishListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    .line 43
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdAppHash:Ljava/util/Hashtable;

    .line 44
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    .line 46
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    .line 157
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconManager$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/beaconmanager/BeaconManager$1;-><init>(Lcom/samsung/android/beaconmanager/BeaconManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mQuickConnectSettingObserver:Landroid/database/ContentObserver;

    .line 240
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/BeaconManager$2;-><init>(Lcom/samsung/android/beaconmanager/BeaconManager;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mPackageChangedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .line 248
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconManager$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/BeaconManager$3;-><init>(Lcom/samsung/android/beaconmanager/BeaconManager;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBlePacketReceiveListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    .line 64
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    .line 66
    const-string v0, "BeaconManager"

    const-string v1, "create"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mPackageChangedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;-><init>(Landroid/content/Context;Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    .line 68
    new-instance v0, Lcom/samsung/android/beaconmanager/network/BleHelper;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    .line 70
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->registerConditionChecker()V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->checkBeaconScanMode()Z

    .line 72
    const-string v0, "BeaconManager"

    const-string v1, "register observer"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/beaconmanager/BeaconManager;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    if-eqz p0, :cond_1

    sget-object v0, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    if-nez v0, :cond_1

    .line 50
    const-class v1, Lcom/samsung/android/beaconmanager/BeaconManager;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/BeaconManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    .line 54
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/DLog;->initilize()V

    .line 55
    const-string v0, "BeaconManager"

    const-string v2, "getInstance"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "make new instance "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_1
    const-string v0, "BeaconManager"

    const-string v1, "getInstance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "return existing instance "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private registerConditionChecker()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/beaconmanager/helper/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mQuickConnectSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 80
    return-void
.end method

.method private setScanFilter()V
    .locals 6

    .prologue
    .line 132
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdAppHash:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->clear()V

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v5}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->getInstallPackageList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 135
    .local v2, "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v5, v2, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/android/beaconmanager/helper/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v4

    .line 136
    .local v4, "packet":[B
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdAppHash:Ljava/util/Hashtable;

    invoke-virtual {v5, v4, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 140
    .end local v2    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    .end local v4    # "packet":[B
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v5}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->getAutoStartPackageList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 141
    .restart local v2    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v5, v2, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/android/beaconmanager/helper/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v4

    .line 142
    .restart local v4    # "packet":[B
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdAppHash:Ljava/util/Hashtable;

    invoke-virtual {v5, v4, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 146
    .end local v2    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    .end local v4    # "packet":[B
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    .line 147
    .local v3, "keys":Ljava/util/Enumeration;, "Ljava/util/Enumeration<[B>;"
    :goto_2
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 148
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 149
    .restart local v4    # "packet":[B
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 152
    .end local v4    # "packet":[B
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-virtual {v5, v0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->updateScanFilter(Ljava/util/ArrayList;)V

    .line 154
    return-void
.end method


# virtual methods
.method public addBleProxyCallback(Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "bleProxyCallback"    # Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "packetInfo"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 170
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v3, p2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v7, :cond_1

    .line 171
    const-string v3, "BeaconManager"

    const-string v4, "addBleProxyCallback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update [packageName]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [bleProxyCallback]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v3, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 175
    .local v0, "oldInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v3, p2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    invoke-virtual {v3, v0}, Ljava/util/Hashtable;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    iget-object v4, v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/beaconmanager/helper/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    :cond_0
    invoke-static {p3}, Lcom/samsung/android/beaconmanager/helper/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v1

    .line 182
    .local v1, "packetInfoByte":[B
    new-instance v2, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    invoke-direct {v2, p2, p3, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;)V

    .line 184
    .local v2, "pkInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v3, p2, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    invoke-virtual {v3, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    .end local v0    # "oldInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    .end local v1    # "packetInfoByte":[B
    .end local v2    # "pkInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->checkBeaconScanMode()Z

    .line 191
    return v7

    .line 187
    :cond_1
    const-string v3, "BeaconManager"

    const-string v4, "addBleProxyCallback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add [packageName]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [bleProxyCallback]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkBeaconScanMode()Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 101
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/beaconmanager/helper/Util;->KEY_ALLOW_TO_CONNECT:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/samsung/android/beaconmanager/helper/Util;->getFromDb(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    .line 104
    .local v2, "isQuickconnectRunning":Z
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->size()I

    move-result v1

    .line 105
    .local v1, "callbackCount":I
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v5}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->getAutoStartPackageSize()I

    move-result v5

    add-int/lit8 v3, v5, -0x2

    .line 108
    .local v3, "startCheckCount":I
    if-nez v2, :cond_0

    if-gtz v1, :cond_0

    if-lez v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 111
    .local v0, "allCondition":Z
    :goto_0
    const-string v5, "BeaconManager"

    const-string v6, "checkBeaconScanMode"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "allCondition  is"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-boolean v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanRunning:Z

    if-eq v0, v5, :cond_3

    .line 113
    if-eqz v0, :cond_2

    .line 115
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBlePacketReceiveListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    invoke-virtual {v4, v5}, Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan(Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;)V

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->setScanFilter()V

    .line 123
    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanRunning:Z

    .line 127
    :goto_2
    iget-boolean v4, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanRunning:Z

    return v4

    .end local v0    # "allCondition":Z
    :cond_1
    move v0, v4

    .line 108
    goto :goto_0

    .line 119
    .restart local v0    # "allCondition":Z
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-virtual {v5}, Lcom/samsung/android/beaconmanager/network/BleHelper;->stopBleScan()V

    .line 120
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-virtual {v5}, Lcom/samsung/android/beaconmanager/network/BleHelper;->terminate()V

    .line 121
    iget-object v5, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanFinishListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    invoke-interface {v5, v4}, Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;->onFinished(I)V

    goto :goto_1

    .line 125
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->setScanFilter()V

    goto :goto_2
.end method

.method public clearBleProxyCallbacks()Z
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 217
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->checkBeaconScanMode()Z

    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method public getScanMode()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanRunning:Z

    return v0
.end method

.method public registerAppAutoStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packetFormat"    # Ljava/lang/String;
    .param p3, "intentAction"    # Ljava/lang/String;

    .prologue
    .line 224
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->registerAppAutoStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 227
    .local v0, "changed":Z
    return v0
.end method

.method public removeBleProxyCallback(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    const-string v1, "BeaconManager"

    const-string v2, "removeBleProxyCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not exist [packageName]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 199
    :cond_0
    const-string v1, "BeaconManager"

    const-string v2, "removeBleProxyCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove [packageName]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 202
    .local v0, "oldInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleProxyCallbackHash:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mRegisterdProxyHash:Ljava/util/Hashtable;

    iget-object v2, v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/beaconmanager/helper/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->checkBeaconScanMode()Z

    .line 209
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setFinishListener(Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;)V
    .locals 0
    .param p1, "mScanManagerFinishedListener"    # Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mScanFinishListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    .line 85
    return-void
.end method

.method public terminate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    const-string v0, "BeaconManager"

    const-string v1, "terminate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconManager;->clearBleProxyCallbacks()Z

    .line 91
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mQuickConnectSettingObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 92
    iput-object v3, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mBleHelper:Lcom/samsung/android/beaconmanager/network/BleHelper;

    .line 93
    sput-object v3, Lcom/samsung/android/beaconmanager/BeaconManager;->mInstance:Lcom/samsung/android/beaconmanager/BeaconManager;

    .line 94
    return-void
.end method

.method public unregisterAppAutoStart(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 232
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconManager;->mAppPackageHelper:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v1, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->unregisterAppAutoStart(Ljava/lang/String;)Z

    move-result v0

    .line 234
    .local v0, "changed":Z
    return v0
.end method
