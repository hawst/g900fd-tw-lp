.class public Lcom/samsung/android/beaconmanager/BeaconBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BeaconBroadcastReceiver.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "BeaconBroadcastReceiver"

    sput-object v0, Lcom/samsung/android/beaconmanager/BeaconBroadcastReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "action":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/beaconmanager/BeaconBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v3, "onReceive"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v2, "com.samsung.android.beaconmanager.create"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.android.settings.QUICK_CONNECT_SETTINGS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 20
    :cond_0
    const-string v2, "ENABLED"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 21
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/beaconmanager/BeaconService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 22
    .local v1, "it":Landroid/content/Intent;
    const-string v2, "com.samsung.android.beaconmanager.start"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 24
    sget-object v2, Lcom/samsung/android/beaconmanager/BeaconBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v3, "send"

    const-string v4, "com.samsung.android.beaconmanager.start"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .end local v1    # "it":Landroid/content/Intent;
    :cond_1
    return-void
.end method
