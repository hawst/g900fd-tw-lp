.class Lcom/samsung/android/beaconmanager/BeaconService$2;
.super Lcom/samsung/android/beaconmanager/proxy/IBleProxyService$Stub;
.source "BeaconService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/BeaconService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/BeaconService;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/BeaconService;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/proxy/IBleProxyService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public registerAppAutoStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packetFormat"    # Ljava/lang/String;
    .param p3, "intentAction"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$200(Lcom/samsung/android/beaconmanager/BeaconService;)Lcom/samsung/android/beaconmanager/BeaconManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/BeaconManager;->registerAppAutoStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public registerCallback(Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "callback"    # Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "packetInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$100(Lcom/samsung/android/beaconmanager/BeaconService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[packageName]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " [callback]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$200(Lcom/samsung/android/beaconmanager/BeaconService;)Lcom/samsung/android/beaconmanager/BeaconManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/BeaconManager;->addBleProxyCallback(Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;Ljava/lang/String;Ljava/lang/String;)Z

    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public unregisterAppAutoStart(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$200(Lcom/samsung/android/beaconmanager/BeaconService;)Lcom/samsung/android/beaconmanager/BeaconManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/beaconmanager/BeaconManager;->unregisterAppAutoStart(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public unregisterCallback(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$100(Lcom/samsung/android/beaconmanager/BeaconService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unregisterCallback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[packageName]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService$2;->this$0:Lcom/samsung/android/beaconmanager/BeaconService;

    # getter for: Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;
    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconService;->access$200(Lcom/samsung/android/beaconmanager/BeaconService;)Lcom/samsung/android/beaconmanager/BeaconManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/beaconmanager/BeaconManager;->removeBleProxyCallback(Ljava/lang/String;)Z

    .line 109
    const/4 v0, 0x0

    return v0
.end method
