.class public Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper$AutoStartPackageDatabase;
.super Ljava/lang/Object;
.source "AutoStartPackageDBHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AutoStartPackageDatabase"
.end annotation


# static fields
.field public static final _CREATE:Ljava/lang/String; = "create table ScanMgr_AutoStartInfo_DB(_id integer primary key autoincrement, APPNAME text not null , PKGNAME text not null , VERCODE text not null , PKTINFO text not null , METHOD text not null , INTENT text not null );"

.field public static final _TABLENAME:Ljava/lang/String; = "ScanMgr_AutoStartInfo_DB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
