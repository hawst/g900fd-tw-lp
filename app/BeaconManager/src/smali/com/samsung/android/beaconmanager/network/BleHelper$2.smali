.class Lcom/samsung/android/beaconmanager/network/BleHelper$2;
.super Landroid/os/Handler;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/network/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 266
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 268
    :pswitch_0
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 269
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_0

    .line 270
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    const/16 v2, 0xb

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1, v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$202(Lcom/samsung/android/beaconmanager/network/BleHelper;I)I

    .line 271
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$500(Lcom/samsung/android/beaconmanager/network/BleHelper;)Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z

    goto :goto_0

    .line 275
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$600(Lcom/samsung/android/beaconmanager/network/BleHelper;)I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_3

    .line 276
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # operator++ for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$608(Lcom/samsung/android/beaconmanager/network/BleHelper;)I

    .line 277
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$400(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-nez v1, :cond_1

    .line 278
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1, v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$402(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;

    .line 279
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    iget-object v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$400(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v2

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;
    invoke-static {v1, v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$702(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/le/BluetoothLeScanner;)Landroid/bluetooth/le/BluetoothLeScanner;

    .line 283
    :cond_1
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-nez v1, :cond_2

    .line 284
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$800(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/PowerManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # invokes: Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$900(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    goto :goto_0

    .line 288
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # invokes: Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$900(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    goto :goto_0

    .line 291
    :cond_3
    const-string v1, "BleHelper"

    const-string v2, "MSG_START_LESCAN"

    const-string v3, "mBleStartCnt exceed max count"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 295
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$2;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-virtual {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->stopBleScan()V

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
