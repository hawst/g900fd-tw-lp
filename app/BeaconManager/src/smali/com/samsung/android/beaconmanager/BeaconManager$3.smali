.class Lcom/samsung/android/beaconmanager/BeaconManager$3;
.super Ljava/lang/Object;
.source "BeaconManager.java"

# interfaces
.implements Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/BeaconManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/BeaconManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/BeaconManager;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/BeaconManager$3;->this$0:Lcom/samsung/android/beaconmanager/BeaconManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 4
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "packet"    # [B

    .prologue
    .line 252
    const-string v0, "BeaconManager"

    const-string v1, "mBlePacketReceiveListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    return-void
.end method
