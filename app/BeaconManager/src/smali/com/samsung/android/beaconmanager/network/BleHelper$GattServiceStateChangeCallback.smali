.class Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;
.super Ljava/lang/Object;
.source "BleHelper.java"

# interfaces
.implements Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/network/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GattServiceStateChangeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;


# direct methods
.method private constructor <init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/beaconmanager/network/BleHelper;Lcom/samsung/android/beaconmanager/network/BleHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;
    .param p2, "x1"    # Lcom/samsung/android/beaconmanager/network/BleHelper$1;

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    return-void
.end method


# virtual methods
.method public onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V
    .locals 10
    .param p1, "up"    # Z
    .param p2, "iGatt"    # Landroid/bluetooth/IBluetoothGatt;

    .prologue
    const-wide/16 v8, 0x12c

    const/16 v6, 0xa

    const/4 v5, 0x1

    .line 210
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "up = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", BluetoothGatt is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    if-eqz p1, :cond_2

    .line 212
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 213
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isGattServiceReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 214
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "up = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isGattServiceReady is false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_0
    :goto_0
    return-void

    .line 218
    .restart local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$200(Lcom/samsung/android/beaconmanager/network/BleHelper;)I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    .line 219
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "Radio turned on"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    const/16 v2, 0xc

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1, v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$202(Lcom/samsung/android/beaconmanager/network/BleHelper;I)I

    .line 221
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$300(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 222
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$300(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 225
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$200(Lcom/samsung/android/beaconmanager/network/BleHelper;)I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_3

    .line 226
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "Radio turned off"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1, v6}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$202(Lcom/samsung/android/beaconmanager/network/BleHelper;I)I

    goto :goto_0

    .line 228
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$200(Lcom/samsung/android/beaconmanager/network/BleHelper;)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 229
    const-string v1, "BleHelper"

    const-string v2, "onGattServiceStateChange"

    const-string v3, "recover Radio"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I
    invoke-static {v1, v6}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$202(Lcom/samsung/android/beaconmanager/network/BleHelper;I)I

    .line 231
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$400(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 232
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1, v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$402(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;

    .line 234
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$300(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # getter for: Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$300(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
