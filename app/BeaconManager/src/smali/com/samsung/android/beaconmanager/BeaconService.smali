.class public Lcom/samsung/android/beaconmanager/BeaconService;
.super Landroid/app/Service;
.source "BeaconService.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field mAm:Landroid/app/IActivityManager;

.field private mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

.field mBeaconManagerFinishedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

.field private final mBleProxyService:Lcom/samsung/android/beaconmanager/proxy/IBleProxyService$Stub;

.field private mContext:Landroid/content/Context;

.field final mForegroundToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    const-string v0, "BeaconService"

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

    .line 40
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/BeaconService$1;-><init>(Lcom/samsung/android/beaconmanager/BeaconService;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManagerFinishedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    .line 80
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mForegroundToken:Landroid/os/IBinder;

    .line 81
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mAm:Landroid/app/IActivityManager;

    .line 95
    new-instance v0, Lcom/samsung/android/beaconmanager/BeaconService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/BeaconService$2;-><init>(Lcom/samsung/android/beaconmanager/BeaconService;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBleProxyService:Lcom/samsung/android/beaconmanager/proxy/IBleProxyService$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/beaconmanager/BeaconService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/BeaconService;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/BeaconService;->stopService()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/beaconmanager/BeaconService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/BeaconService;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/beaconmanager/BeaconService;)Lcom/samsung/android/beaconmanager/BeaconManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/BeaconService;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

    return-object v0
.end method

.method private setProcessForeground(Z)V
    .locals 0
    .param p1, "isForeground"    # Z

    .prologue
    .line 93
    return-void
.end method

.method private stopService()V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "Stop Service "

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconService;->stopSelf()V

    .line 50
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "onBind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBleProxyService:Lcom/samsung/android/beaconmanager/proxy/IBleProxyService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/BeaconService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mContext:Landroid/content/Context;

    .line 34
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/beaconmanager/BeaconManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/beaconmanager/BeaconManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

    .line 35
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManagerFinishedListener:Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/beaconmanager/BeaconManager;->setFinishListener(Lcom/samsung/android/beaconmanager/helper/Listener$IFinishListener;)V

    .line 36
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/beaconmanager/BeaconService;->setProcessForeground(Z)V

    .line 37
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 38
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/beaconmanager/BeaconService;->setProcessForeground(Z)V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->mBeaconManager:Lcom/samsung/android/beaconmanager/BeaconManager;

    invoke-virtual {v0}, Lcom/samsung/android/beaconmanager/BeaconManager;->terminate()V

    .line 77
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 78
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "onStartCommand"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/BeaconService;->TAG:Ljava/lang/String;

    const-string v1, "onUnbind"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
