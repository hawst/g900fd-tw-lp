.class public Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
.super Ljava/lang/Object;
.source "AppPackageInfo.java"


# instance fields
.field public mAppName:Ljava/lang/String;

.field public mBlePacketInfo:Ljava/lang/String;

.field public mIntent:Ljava/lang/String;

.field public mPackageName:Ljava/lang/String;

.field public mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

.field public mSearchMethod:I

.field public mVersionCode:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 14
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 16
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 67
    const-string v0, "APPNAME"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 68
    const-string v0, "PKGNAME"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 69
    const-string v0, "VERCODE"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 72
    const-string v0, "PKTINFO"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 73
    const-string v0, "METHOD"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 75
    const-string v0, "INTENT"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 13
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 11
    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput v9, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 14
    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 15
    iput v9, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 16
    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 17
    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 39
    const/16 v7, 0x8

    new-array v2, v7, [I

    .line 40
    .local v2, "index":[I
    const-string v7, "app="

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    aput v7, v2, v9

    .line 41
    const-string v7, "pkg="

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    aput v7, v2, v10

    .line 42
    const-string v7, "ver="

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    aput v7, v2, v11

    .line 43
    const-string v7, "ptn="

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    aput v7, v2, v12

    .line 44
    const/4 v7, 0x4

    const-string v8, "etc="

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    aput v8, v2, v7

    .line 45
    const/4 v7, 0x5

    const-string v8, "itt="

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    aput v8, v2, v7

    .line 46
    const/4 v7, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    aput v8, v2, v7

    .line 48
    aget v7, v2, v9

    add-int/lit8 v7, v7, 0x5

    aget v8, v2, v10

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "appName":Ljava/lang/String;
    aget v7, v2, v10

    add-int/lit8 v7, v7, 0x5

    aget v8, v2, v11

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "pkgName":Ljava/lang/String;
    aget v7, v2, v11

    add-int/lit8 v7, v7, 0x5

    aget v8, v2, v12

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 51
    .local v6, "verName":Ljava/lang/String;
    aget v7, v2, v12

    add-int/lit8 v7, v7, 0x5

    const/4 v8, 0x4

    aget v8, v2, v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "pattern":Ljava/lang/String;
    const/4 v7, 0x4

    aget v7, v2, v7

    add-int/lit8 v7, v7, 0x5

    const/4 v8, 0x5

    aget v8, v2, v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "etc":Ljava/lang/String;
    const/4 v7, 0x5

    aget v7, v2, v7

    add-int/lit8 v7, v7, 0x5

    const/4 v8, 0x6

    aget v8, v2, v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "intent":Ljava/lang/String;
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 56
    iput-object v5, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 57
    const/16 v7, 0xa

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 58
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 60
    invoke-static {v1}, Lcom/samsung/android/beaconmanager/helper/Util;->ConvertAdditinalSearchMethod(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 61
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 62
    iget-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    iput-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 64
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "appname"    # Ljava/lang/String;
    .param p2, "pkgname"    # Ljava/lang/String;
    .param p3, "vercode"    # I
    .param p4, "pktinfo"    # Ljava/lang/String;
    .param p5, "searchMethod"    # I
    .param p6, "intent"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 14
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 16
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 21
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 23
    iput p3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 25
    iput-object p4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 26
    iput p5, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 27
    iput-object p6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "pattern"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 14
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 16
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 79
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgname"    # Ljava/lang/String;
    .param p2, "pktinfo"    # Ljava/lang/String;
    .param p3, "intent"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    .line 14
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 15
    iput v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    .line 16
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mProxyCallback:Lcom/samsung/android/beaconmanager/proxy/IBleProxyCallback;

    .line 31
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public equals(Landroid/database/Cursor;)Z
    .locals 8
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    const/16 v7, 0xa

    const/4 v5, 0x0

    .line 93
    const-string v6, "PKGNAME"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "curPackageName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    if-eq v2, v6, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v5

    .line 96
    :cond_1
    const-string v6, "VERCODE"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    .line 98
    .local v4, "curVersionCode":I
    iget v6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    if-ne v4, v6, :cond_0

    .line 101
    const-string v6, "PKTINFO"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "curBlePacketInfo":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    if-ne v0, v6, :cond_0

    .line 105
    const-string v6, "METHOD"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 107
    .local v3, "curSearchMethod":I
    iget v6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    if-ne v3, v6, :cond_0

    .line 109
    const-string v6, "INTENT"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "curIntent":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    if-ne v1, v6, :cond_0

    .line 112
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public equalsPackageName(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cur"    # Landroid/database/Cursor;

    .prologue
    .line 85
    const-string v1, "PKGNAME"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "curPackageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    if-eq v0, v1, :cond_0

    .line 87
    const/4 v1, 0x0

    .line 89
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
