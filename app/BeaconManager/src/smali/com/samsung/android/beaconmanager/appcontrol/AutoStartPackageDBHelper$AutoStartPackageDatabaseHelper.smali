.class Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper$AutoStartPackageDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "AutoStartPackageDBHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AutoStartPackageDatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 48
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 53
    const-string v0, "create table ScanMgr_AutoStartInfo_DB(_id integer primary key autoincrement, APPNAME text not null , PKGNAME text not null , VERCODE text not null , PKTINFO text not null , METHOD text not null , INTENT text not null );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 59
    const-string v0, "DROP TABLE IF EXISTS ScanMgr_AutoStartInfo_DB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper$AutoStartPackageDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 61
    return-void
.end method
