.class public Lcom/samsung/android/beaconmanager/network/BleHelper;
.super Ljava/lang/Object;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;
    }
.end annotation


# static fields
.field private static final LE_RADIO_STATE_OFF:I = 0xa

.field private static final LE_RADIO_STATE_ON:I = 0xc

.field private static final LE_RADIO_STATE_TURNING_OFF:I = 0xd

.field private static final LE_RADIO_STATE_TURNING_ON:I = 0xb

.field private static final MSG_ENABLE_BLE:I = 0x1

.field private static final MSG_START_LESCAN:I = 0x3

.field private static final MSG_STOP_BLE_SCAN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BleHelper"


# instance fields
.field private bDoScan:Z

.field private mAppFilterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

.field private mBleStartCnt:I

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

.field private mHandler:Landroid/os/Handler;

.field private mLeRadioState:I

.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field private mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

.field private mPacketListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

.field private mPowerManager:Landroid/os/PowerManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mContext:Landroid/content/Context;

    .line 37
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mAppFilterList:Ljava/util/ArrayList;

    .line 39
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPacketListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    .line 41
    iput-boolean v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->bDoScan:Z

    .line 43
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    .line 44
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    .line 48
    iput-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 58
    const/16 v1, 0xa

    iput v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    .line 60
    iput v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I

    .line 263
    new-instance v1, Lcom/samsung/android/beaconmanager/network/BleHelper$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/beaconmanager/network/BleHelper$2;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    .line 313
    new-instance v1, Lcom/samsung/android/beaconmanager/network/BleHelper$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/beaconmanager/network/BleHelper$3;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 338
    new-instance v1, Lcom/samsung/android/beaconmanager/network/BleHelper$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/beaconmanager/network/BleHelper$4;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 63
    const-string v1, "BleHelper"

    const-string v2, "BleHelper"

    const-string v3, "Constructor"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mContext:Landroid/content/Context;

    .line 66
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 67
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 69
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    .line 71
    new-instance v1, Lcom/samsung/android/beaconmanager/network/BleHelper$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/beaconmanager/network/BleHelper$1;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    .line 95
    :goto_0
    new-instance v1, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;-><init>(Lcom/samsung/android/beaconmanager/network/BleHelper;Lcom/samsung/android/beaconmanager/network/BleHelper$1;)V

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    .line 96
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V

    .line 97
    return-void

    .line 87
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 89
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 90
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "x2"    # I
    .param p3, "x3"    # [B

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/network/BleHelper;->parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/beaconmanager/network/BleHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/beaconmanager/network/BleHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/BluetoothAdapter;)Landroid/bluetooth/BluetoothAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/beaconmanager/network/BleHelper;)Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/beaconmanager/network/BleHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I

    return v0
.end method

.method static synthetic access$608(Lcom/samsung/android/beaconmanager/network/BleHelper;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/beaconmanager/network/BleHelper;Landroid/bluetooth/le/BluetoothLeScanner;)Landroid/bluetooth/le/BluetoothLeScanner;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;
    .param p1, "x1"    # Landroid/bluetooth/le/BluetoothLeScanner;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/beaconmanager/network/BleHelper;)Landroid/os/PowerManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/beaconmanager/network/BleHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/beaconmanager/network/BleHelper;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V

    return-void
.end method

.method private getLocalScanFilters()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/le/ScanFilter;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    .line 126
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v6, "scanFilterList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/le/ScanFilter;>;"
    const/16 v3, 0x75

    .line 129
    .local v3, "manufacturerId":I
    new-array v2, v7, [B

    fill-array-data v2, :array_0

    .line 132
    .local v2, "manudata_QC":[B
    new-array v1, v7, [B

    fill-array-data v1, :array_1

    .line 135
    .local v1, "manudata_GEAR":[B
    new-array v0, v7, [B

    fill-array-data v0, :array_2

    .line 139
    .local v0, "manudataMask":[B
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v3, v2, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v4

    .line 141
    .local v4, "sFilter":Landroid/bluetooth/le/ScanFilter;
    new-instance v7, Landroid/bluetooth/le/ScanFilter$Builder;

    invoke-direct {v7}, Landroid/bluetooth/le/ScanFilter$Builder;-><init>()V

    invoke-virtual {v7, v3, v1, v0}, Landroid/bluetooth/le/ScanFilter$Builder;->setManufacturerData(I[B[B)Landroid/bluetooth/le/ScanFilter$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/le/ScanFilter$Builder;->build()Landroid/bluetooth/le/ScanFilter;

    move-result-object v5

    .line 144
    .local v5, "sFilter1":Landroid/bluetooth/le/ScanFilter;
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    return-object v6

    .line 129
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 132
    :array_1
    .array-data 1
        0x1t
        0x0t
        0x2t
    .end array-data

    .line 135
    :array_2
    .array-data 1
        0x0t
        -0x1t
        -0x1t
    .end array-data
.end method

.method private getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 151
    new-instance v1, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v1}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/ScanSettings$Builder;->setCallbackType(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanResultType(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v0

    .line 155
    .local v0, "myScanPreferences":Landroid/bluetooth/le/ScanSettings;
    return-object v0
.end method

.method private parseBlePacket(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 1
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # [B

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPacketListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;->onReceive(Landroid/bluetooth/BluetoothDevice;I[B)V

    .line 311
    return-void
.end method

.method private proxyToAppsCallback(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "rssi"    # I
    .param p4, "scanRecord"    # [B

    .prologue
    .line 329
    return-void
.end method

.method private declared-synchronized startBleScan()V
    .locals 5

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 165
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_1

    .line 166
    invoke-virtual {v0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isGattServiceReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 168
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "isGattServiceReady but not isScreenOn"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :goto_0
    monitor-exit p0

    return-void

    .line 172
    :cond_0
    :try_start_1
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not isGattServiceReady(STATE : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "). Try to BLE On calling addLeRadioReference()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/16 v1, 0xb

    iput v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    .line 175
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 164
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 179
    .restart local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_1
    const/4 v1, 0x0

    :try_start_2
    iput v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleStartCnt:I

    .line 181
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 183
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_4

    .line 184
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 185
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v1, :cond_2

    .line 186
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "call BleScanner.startScan()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->getLocalScanFilters()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->getMyScanPreferences()Landroid/bluetooth/le/ScanSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V

    .line 189
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "call BleScanner.startScan() complete"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->bDoScan:Z

    goto :goto_0

    .line 192
    :cond_2
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "BleScanner is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :cond_3
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "call BluetoothAdapter.startLeScan()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "call BluetoothAdapter.startLeScan() complete"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->bDoScan:Z

    goto/16 :goto_0

    .line 202
    :cond_4
    const-string v1, "BleHelper"

    const-string v2, "startBleScan"

    const-string v3, "BluetoothAdapter is null or LE is not ON state"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized startBleScan(Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPacketListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    .line 160
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopBleScan()V
    .locals 3

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    .line 243
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "call BleScanner.stopScan()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBleScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanFilterCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 247
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "call BleScanner.stopScan() complete"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->bDoScan:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    :goto_0
    monitor-exit p0

    return-void

    .line 250
    :cond_0
    :try_start_1
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "BleScanner is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 253
    :cond_1
    :try_start_2
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "call BluetoothAdapter.stopLeScan()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 255
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "call BluetoothAdapter.stopLeScan() complete"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->bDoScan:Z

    goto :goto_0

    .line 259
    :cond_2
    const-string v0, "BleHelper"

    const-string v1, "stopBleScan"

    const-string v2, "BluetoothAdapter is null or LE is not ON state"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public terminate()V
    .locals 4

    .prologue
    .line 100
    const-string v1, "BleHelper"

    const-string v2, "terminate"

    const-string v3, "Destructor"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/beaconmanager/helper/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/samsung/android/beaconmanager/helper/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 109
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/network/BleHelper;->stopBleScan()V

    .line 114
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mPacketListener:Lcom/samsung/android/beaconmanager/helper/Listener$IPacketReceivedListener;

    .line 116
    iget v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    .line 117
    invoke-static {}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    move-result-object v0

    .line 118
    .local v0, "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    if-eqz v0, :cond_2

    .line 119
    const/16 v1, 0xd

    iput v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mLeRadioState:I

    .line 120
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper;->mGattServiceStateChangeCallback:Lcom/samsung/android/beaconmanager/network/BleHelper$GattServiceStateChangeCallback;

    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->releaseLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z

    .line 123
    .end local v0    # "radioManager":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :cond_2
    return-void
.end method

.method public updateScanFilter(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p1, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    return-void
.end method
