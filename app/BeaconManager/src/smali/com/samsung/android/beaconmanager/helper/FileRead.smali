.class public Lcom/samsung/android/beaconmanager/helper/FileRead;
.super Ljava/lang/Object;
.source "FileRead.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FileRead"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAutoStartInfoFromInfoFiles(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    const-string v0, "AppAutoStartInfo.cfg"

    invoke-static {p0, v0}, Lcom/samsung/android/beaconmanager/helper/FileRead;->readInfoFiles(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getInstallInfoFromInfoFiles(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    const-string v0, "AppInstallInfo.cfg"

    invoke-static {p0, v0}, Lcom/samsung/android/beaconmanager/helper/FileRead;->readInfoFiles(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static readInfoFiles(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 29
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    const/4 v3, 0x0

    .line 31
    .local v3, "is":Ljava/io/InputStream;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v6, "savedPatterns":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;>;"
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 34
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 35
    .local v1, "bufReader":Ljava/io/BufferedReader;
    const-string v4, ","

    .line 36
    .local v4, "line":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 37
    const-string v7, "//"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 38
    new-instance v5, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    invoke-direct {v5, v4}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;-><init>(Ljava/lang/String;)V

    .line 39
    .local v5, "packet":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 42
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "packet":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :catch_0
    move-exception v2

    .line 43
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v7, "FileRead"

    const-string v8, "getStringFromAsset"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    if-eqz v3, :cond_1

    .line 47
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 53
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-object v6

    .line 46
    .restart local v1    # "bufReader":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    :cond_2
    if-eqz v3, :cond_1

    .line 47
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 49
    :catch_1
    move-exception v2

    .line 50
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "FileRead"

    const-string v8, "getStringFromAsset"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 49
    .end local v1    # "bufReader":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .local v2, "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 50
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "FileRead"

    const-string v8, "getStringFromAsset"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 45
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 46
    if-eqz v3, :cond_3

    .line 47
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 51
    :cond_3
    :goto_2
    throw v7

    .line 49
    :catch_3
    move-exception v2

    .line 50
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "FileRead"

    const-string v9, "getStringFromAsset"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
