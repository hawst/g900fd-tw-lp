.class Lcom/samsung/android/beaconmanager/network/BleHelper$4;
.super Landroid/content/BroadcastReceiver;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/network/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/network/BleHelper;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$4;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 341
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 342
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 343
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 344
    .local v1, "user":I
    const-string v2, "BleHelper"

    const-string v3, "mBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCREEN_ON when user = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    if-nez v1, :cond_0

    .line 346
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$4;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    # invokes: Lcom/samsung/android/beaconmanager/network/BleHelper;->startBleScan()V
    invoke-static {v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->access$900(Lcom/samsung/android/beaconmanager/network/BleHelper;)V

    .line 355
    .end local v1    # "user":I
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 350
    .restart local v1    # "user":I
    const-string v2, "BleHelper"

    const-string v3, "mBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCREEN_OFF when user = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    if-nez v1, :cond_0

    .line 352
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/network/BleHelper$4;->this$0:Lcom/samsung/android/beaconmanager/network/BleHelper;

    invoke-virtual {v2}, Lcom/samsung/android/beaconmanager/network/BleHelper;->stopBleScan()V

    goto :goto_0
.end method
