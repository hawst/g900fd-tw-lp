.class public Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;
.super Ljava/lang/Object;
.source "AppPackageHelper.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAutoStartPackageCheckList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

.field private mAutoStartPackageInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

.field private mContext:Landroid/content/Context;

.field private mInstallPackageCheckList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mInstallPacketInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "InstallPackageHelper"

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->TAG:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    .line 28
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .line 29
    iput-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    .line 166
    new-instance v0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;-><init>(Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    .line 32
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    .line 33
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/beaconmanager/helper/FileRead;->getInstallInfoFromInfoFiles(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    .line 34
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/beaconmanager/helper/FileRead;->getAutoStartInfoFromInfoFiles(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    .line 36
    const-string v0, "InstallPackageHelper"

    const-string v1, "intialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mInstallPacketInfos size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v0, "InstallPackageHelper"

    const-string v1, "intialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mAutoStartPackageInfos size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    .line 40
    invoke-direct {p0, p2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->registerInstallObserver(Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;)Z

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updateInstallPackageInfo()Z

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updatePackageInfoOnMarket()Z

    .line 45
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updateAutoStartPackageInfo()Z

    .line 49
    return-void
.end method

.method private registerInstallObserver(Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;)Z
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .prologue
    .line 141
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 142
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 143
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 147
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .line 148
    const/4 v1, 0x1

    .line 150
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateAutoStartPackageInfo()Z
    .locals 14

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 62
    .local v13, "oldSize":I
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 67
    .local v11, "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    iget-object v1, v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    iget v2, v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->open()Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    .line 75
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->queryAll()Landroid/database/Cursor;

    move-result-object v8

    .line 77
    .local v8, "cur":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 79
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v7, "autoStartPackageNoUsed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;>;"
    if-eqz v8, :cond_3

    .line 81
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 82
    new-instance v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    invoke-direct {v11, v8}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;-><init>(Landroid/database/Cursor;)V

    .line 83
    .restart local v11    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    iget-object v1, v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    iget v2, v11, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 87
    :cond_2
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 93
    .end local v11    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 94
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 96
    .local v9, "i":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    iget-object v1, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mAppName:Ljava/lang/String;

    iget-object v2, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    iget v3, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    iget-object v4, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mBlePacketInfo:Ljava/lang/String;

    iget v5, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mSearchMethod:I

    iget-object v6, v9, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mIntent:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->deleteInfo(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_2

    .line 101
    .end local v7    # "autoStartPackageNoUsed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;>;"
    .end local v9    # "i":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->close()V

    .line 102
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 104
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 106
    .local v12, "newSize":I
    const-string v0, "InstallPackageHelper"

    const-string v1, "updateAutoStartPackageInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mAutoStartPackageCheckList size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    if-eq v13, v12, :cond_5

    .line 109
    const/4 v0, 0x1

    .line 111
    :goto_3
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private updateInstallPackageInfo()Z
    .locals 8

    .prologue
    .line 116
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 117
    .local v3, "oldSize":I
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 118
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 119
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    .line 120
    .local v1, "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    iget-object v5, v1, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mPackageName:Ljava/lang/String;

    iget v6, v1, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->mVersionCode:I

    invoke-static {v4, v5, v6}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 121
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 127
    .local v2, "newSize":I
    const-string v4, "InstallPackageHelper"

    const-string v5, "updateInstallPackageInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mInstallPackageCheckList size "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    if-eq v3, v2, :cond_2

    .line 130
    const/4 v4, 0x1

    .line 132
    :goto_1
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAutoStartPackageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAutoStartPackageSize()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 191
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getInstallPackageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getInstallPackageSize()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public registerAppAutoStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bleFormat"    # Ljava/lang/String;
    .param p3, "registeredIntent"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 204
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageUtil;->isAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    new-instance v8, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;

    invoke-direct {v8, p1, p2, p3}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    .local v8, "newInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->queryByPacket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 211
    .local v7, "cur":Landroid/database/Cursor;
    invoke-virtual {v8, v7}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;->equalsPackageName(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const-string v0, "InstallPackageHelper"

    const-string v1, "registerAppAutoStart"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "was registered on DB."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 219
    const/4 v3, 0x1

    .line 223
    .end local v7    # "cur":Landroid/database/Cursor;
    .end local v8    # "newInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :goto_1
    return v3

    .line 214
    .restart local v7    # "cur":Landroid/database/Cursor;
    .restart local v8    # "newInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    const/4 v1, 0x0

    move-object v2, p1

    move-object v4, p2

    move v5, v3

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->insert(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updatePackageInfo()V

    goto :goto_0

    .line 221
    .end local v7    # "cur":Landroid/database/Cursor;
    .end local v8    # "newInfo":Lcom/samsung/android/beaconmanager/appcontrol/AppPackageInfo;
    :cond_1
    const-string v0, "InstallPackageHelper"

    const-string v1, "registerAppAutoStart"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "is not installed."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/beaconmanager/helper/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public terminate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mPackageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 156
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    .line 157
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPacketInfos:Ljava/util/ArrayList;

    .line 158
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mInstallPackageCheckList:Ljava/util/ArrayList;

    .line 159
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageInfos:Ljava/util/ArrayList;

    .line 160
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageCheckList:Ljava/util/ArrayList;

    .line 161
    iput-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mContext:Landroid/content/Context;

    .line 164
    :cond_0
    return-void
.end method

.method public unregisterAppAutoStart(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v2, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->queryByPackage(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 228
    .local v0, "cur":Landroid/database/Cursor;
    const-string v2, "PKGNAME"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 229
    .local v1, "curPackageName":Ljava/lang/String;
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 230
    if-ne v1, p1, :cond_0

    .line 231
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mAutoStartPackageDBHelper:Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;

    invoke-virtual {v2, p1}, Lcom/samsung/android/beaconmanager/appcontrol/AutoStartPackageDBHelper;->deleteInfo(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updatePackageInfo()V

    .line 238
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 234
    :cond_0
    const-string v2, "InstallPackageHelper"

    const-string v3, "unregisterAppAutoStart"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " don\'t fine info :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updatePackageInfo()V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updateInstallPackageInfo()Z

    move-result v0

    .line 53
    .local v0, "installResult":Z
    invoke-direct {p0}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updateAutoStartPackageInfo()Z

    move-result v1

    .line 55
    .local v1, "startResult":Z
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 56
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->mChangeListener:Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;

    invoke-interface {v2}, Lcom/samsung/android/beaconmanager/helper/Listener$IChangeListener;->onChanged()V

    .line 58
    :cond_1
    return-void
.end method

.method public updatePackageInfoOnMarket()Z
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return v0
.end method
