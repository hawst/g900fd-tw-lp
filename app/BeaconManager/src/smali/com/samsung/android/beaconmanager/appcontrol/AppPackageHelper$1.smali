.class Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;
.super Landroid/content/BroadcastReceiver;
.source "AppPackageHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;->this$0:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 169
    const-string v2, "InstallPackageHelper"

    const-string v3, "mPackageReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 172
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;->this$0:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updatePackageInfo()V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "InstallPackageHelper"

    const-string v3, "mPackageReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_PACKAGE_REMOVED "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/beaconmanager/helper/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;->this$0:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v2, v1}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->unregisterAppAutoStart(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper$1;->this$0:Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;

    invoke-virtual {v2}, Lcom/samsung/android/beaconmanager/appcontrol/AppPackageHelper;->updatePackageInfo()V

    goto :goto_0
.end method
