.class public final Lcom/mobeam/barcodeService/c/b;
.super Ljava/lang/Object;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field d:I

.field final e:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->b:I

    iput v1, p0, Lcom/mobeam/barcodeService/c/b;->c:I

    iput v1, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/mobeam/barcodeService/c/b;->e:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(B)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->b:I

    iput v1, p0, Lcom/mobeam/barcodeService/c/b;->c:I

    iput v1, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/mobeam/barcodeService/c/b;->e:Ljava/util/Random;

    const v0, 0x15180

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->c:I

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    const/16 v0, 0xe10

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->a:I

    const v0, 0x127500

    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->b:I

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)I
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    move v2, p1

    :goto_0
    iget v1, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    iget v3, p0, Lcom/mobeam/barcodeService/c/b;->b:I

    if-ge v1, v3, :cond_3

    add-int/lit8 v1, v2, -0x1

    if-gtz v2, :cond_1

    :goto_1
    if-lez v1, :cond_0

    iget v0, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    :cond_0
    iget v1, p0, Lcom/mobeam/barcodeService/c/b;->a:I

    iget-object v2, p0, Lcom/mobeam/barcodeService/c/b;->e:Ljava/util/Random;

    invoke-virtual {v2, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget v2, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    iget v0, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    const v3, 0x55555553

    if-ge v0, v3, :cond_2

    iget v0, p0, Lcom/mobeam/barcodeService/c/b;->b:I

    iget v3, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    iget v4, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    add-int/lit8 v4, v4, 0x1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/mobeam/barcodeService/c/b;->d:I

    move v0, v2

    move v2, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mobeam/barcodeService/c/b;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
