.class final Lcom/mobeam/barcodeService/system/a;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/mobeam/barcodeService/system/AppService;

.field private final synthetic b:Landroid/content/Context;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:I


# direct methods
.method constructor <init>(Lcom/mobeam/barcodeService/system/AppService;Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    iput-object p2, p0, Lcom/mobeam/barcodeService/system/a;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    iput p4, p0, Lcom/mobeam/barcodeService/system/a;->d:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/mobeam/barcodeService/c/a;->a(Landroid/content/Context;)Lcom/mobeam/barcodeService/c/a;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "KEY_RETRY_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    const-string v0, "com.mobeam.barcodeService.UPLOAD_BEAM_RECORD"

    iget-object v3, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    invoke-static {v0}, Lcom/mobeam/barcodeService/system/AppService;->a(Lcom/mobeam/barcodeService/system/AppService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    invoke-static {v0}, Lcom/mobeam/barcodeService/system/AppService;->d(Lcom/mobeam/barcodeService/system/AppService;)V

    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    iget v1, p0, Lcom/mobeam/barcodeService/system/a;->d:I

    invoke-virtual {v0, v1}, Lcom/mobeam/barcodeService/system/AppService;->stopSelfResult(I)Z

    return-void

    :cond_1
    :try_start_1
    const-string v0, "com.mobeam.barcodeService.REGISTER_DEVICE"

    iget-object v3, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    invoke-static {v0}, Lcom/mobeam/barcodeService/system/AppService;->b(Lcom/mobeam/barcodeService/system/AppService;)Lcom/mobeam/mbss/service/DeviceAuth;

    iget-object v0, p0, Lcom/mobeam/barcodeService/system/a;->a:Lcom/mobeam/barcodeService/system/AppService;

    invoke-static {v0}, Lcom/mobeam/barcodeService/system/AppService;->c(Lcom/mobeam/barcodeService/system/AppService;)Lcom/mobeam/barcodeService/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mobeam/barcodeService/a;->b()V

    iget-object v0, v1, Lcom/mobeam/barcodeService/c/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/mobeam/barcodeService/system/AppService;->a()Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const-string v0, "com.mobeam.barcodeService.REGISTER_DEVICE"

    iget-object v3, p0, Lcom/mobeam/barcodeService/system/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, v1, Lcom/mobeam/barcodeService/c/a;->a:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, v1, Lcom/mobeam/barcodeService/c/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v3, v2, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/16 v2, 0x3c

    if-le v0, v2, :cond_2

    invoke-static {}, Lcom/mobeam/barcodeService/system/AppService;->a()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/mobeam/barcodeService/system/AppService;->a()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Schedule failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    :try_start_3
    new-instance v2, Lcom/mobeam/barcodeService/c/b;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/mobeam/barcodeService/c/b;-><init>(B)V

    invoke-virtual {v2, v0}, Lcom/mobeam/barcodeService/c/b;->a(I)I

    move-result v0

    int-to-long v2, v0

    long-to-int v0, v2

    iget-object v4, v1, Lcom/mobeam/barcodeService/c/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "REG_DELAY"

    invoke-virtual {v1}, Lcom/mobeam/barcodeService/c/a;->e()I

    move-result v1

    add-int/2addr v0, v1

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/mobeam/barcodeService/system/a;->b:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/mobeam/barcodeService/GCMIntentService;->a(JLandroid/content/Context;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0
.end method
