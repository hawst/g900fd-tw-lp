.class public Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;
.super Ljava/lang/Object;
.source "ProgramMonitorUtil.java"


# static fields
.field private static final mHiddenPkgString:[Ljava/lang/String;

.field private static final mMustBeShownPkgString:[Ljava/lang/String;

.field static sRunningCount:I

.field static sWarningCpu:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHiddenPkg:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMustBeShownPkg:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPm:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    sput v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->sRunningCount:I

    .line 24
    sput-boolean v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->sWarningCpu:Z

    .line 51
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.samsung.music"

    aput-object v1, v0, v3

    const-string v1, "com.google.android.youtube"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.music"

    aput-object v1, v0, v5

    const-string v1, "com.android.music"

    aput-object v1, v0, v6

    const-string v1, "com.sec.android.app.voicerecorder"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "com.sec.android.app.fm"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.iloen.melon"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.mnet.polgtapp"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.mnet.app"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.mnet.lgtapp"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.neowiz.android.bugs"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.skt.nate.A00000000D"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mMustBeShownPkgString:[Ljava/lang/String;

    .line 62
    const/16 v0, 0x2e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.launcher"

    aput-object v1, v0, v3

    const-string v1, "com.sec.android.app.twlauncher"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.controlpanel"

    aput-object v1, v0, v5

    const-string v1, "com.sec.android.app.dialertop"

    aput-object v1, v0, v6

    const-string v1, "com.sec.android.app.dialer"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "com.android.contacts"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.android.phone"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.providers.telephony"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "android"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "system"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.android.stk"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.settings"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.android.bluetoothtest"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.samsung.sec.android.application.csc"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.sec.android.app.callsetting"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.samsung.crashnotifier"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.sec.android.app.factorytest"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.android.settings.mt"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.samsung.mobileTracker.ui"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.osp.app.signin"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.wipereceiver"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "com.sec.android.app.personalization"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "com.sec.android.Preconfig"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "com.sec.android.app.servicemodeapp"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "com.sec.android.app.wlantest"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "com.sec.android.app.dialertab"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "com.wssyncmldm"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "com.ws.dm"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "com.android.samsungtest.DataCreate"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "com.android.setupwizard"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "com.google.android.googleapps"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "com.android.wallpaper"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "com.android.wallpaper.livepicker"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "com.android.magicsmoke"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "com.sec.android.voip"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "com.android.samsungtest.FactoryTest"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "com.sec.android.widgetapp.dualclock"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "com.spritemobile.backup.samsung"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "com.sec.android.app.samsungapps.una"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "com.palmia.qspider"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "com.android.providers.downloads.ui"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "com.sec.android.widgetapp.buddiesnow"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "com.google.android.gsf.login"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "com.sec.android.widgetapp.activeapplicationwidget"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "com.sec.android.provider.logsprovider"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mHiddenPkgString:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mContext:Landroid/content/Context;

    .line 88
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mPm:Landroid/content/pm/PackageManager;

    .line 89
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mMustBeShownPkg:Ljava/util/Set;

    .line 90
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mHiddenPkg:Ljava/util/Set;

    .line 91
    sget-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mMustBeShownPkgString:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 92
    .local v3, "s":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mMustBeShownPkg:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "s":Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mHiddenPkgString:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 95
    .restart local v3    # "s":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->mHiddenPkg:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 97
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method static getRunningPackageCount()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->sRunningCount:I

    return v0
.end method

.method static setCpuWarning(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 39
    sput-boolean p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->sWarningCpu:Z

    .line 40
    return-void
.end method
