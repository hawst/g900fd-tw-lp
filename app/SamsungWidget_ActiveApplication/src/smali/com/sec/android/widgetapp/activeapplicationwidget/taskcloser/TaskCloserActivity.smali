.class public Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;
.super Ljava/lang/Object;
.source "TaskCloserActivity.java"


# instance fields
.field private final DBG:Z

.field private checkProcessIntent:Landroid/content/Intent;

.field public mEndAllPressed_Flag:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->DBG:Z

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.minimode.taskcloser.CHECK_PROCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->checkProcessIntent:Landroid/content/Intent;

    .line 34
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->mEndAllPressed_Flag:Z

    .line 39
    const-string v0, "TaskCloserActivity"

    const-string v1, "TaskCloserActivity enter()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    const/high16 v9, 0x10000000

    const/4 v8, 0x1

    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 46
    const-string v5, "TaskCloserActivity"

    const-string v6, "TaskCloserActivity onReceive : action is null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const-string v5, "TaskCloserActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TaskCloserActivity onReceive() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const-string v5, "com.sec.android.widgetapp.activeapplicationwidget.taskcloser.CLICK_WIDGET"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    const-string v5, "TaskCloserActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CLICK_WIDGET - Send CHECK_PROCESS broadcast by clicking widget: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->checkProcessIntent:Landroid/content/Intent;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v4, 0x0

    .line 58
    .local v4, "isTaskmanager":Z
    const/4 v3, 0x0

    .line 60
    .local v3, "isJobmanager":Z
    iget-object v5, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->checkProcessIntent:Landroid/content/Intent;

    invoke-virtual {p1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 61
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .local v1, "clickIntent":Landroid/content/Intent;
    const-string v5, "monitor_widget_start"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.taskmanager"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    const/4 v4, 0x1

    .line 72
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.controlpanel"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 73
    const/4 v3, 0x1

    .line 77
    :goto_2
    if-eqz v4, :cond_2

    .line 78
    const-string v5, "com.sec.android.app.taskmanager"

    const-string v6, "com.sec.android.app.taskmanager.activity.TaskManagerActivity"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    invoke-virtual {v1, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 81
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 67
    :catch_0
    move-exception v2

    .line 68
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "TaskCloserActivity"

    const-string v6, "taskmanager is missed"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 82
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    if-eqz v3, :cond_0

    .line 83
    const-string v5, "com.sec.android.app.controlpanel"

    const-string v6, "com.sec.android.app.controlpanel.activity.JobManagerActivity"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    invoke-virtual {v1, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 86
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 74
    :catch_1
    move-exception v5

    goto :goto_2
.end method
