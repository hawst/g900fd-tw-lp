.class public Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "ProgramMonitorProvider.java"


# static fields
.field private static WIDGET_DISABLED:I

.field private static WIDGET_ENABLED:I

.field private static WIDGET_INIT:I

.field static appWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private static isConfiguration:Z

.field private static isWarning:Z

.field private static isWidgetEnabled:I

.field static mCtxt:Landroid/content/Context;

.field private static mImageNotiView:I

.field private static mNeedUpdate:Z

.field private static mNumberView:I

.field private static mPrevOrientation:I

.field private static mProgramMonitorUtil:Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;

.field static mRes:Landroid/content/res/Resources;

.field public static mRunningCount:I

.field private static mSlideTextView:I

.field public static mSpanMapX:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static mSpanMapY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

.field static mViews1x1:Landroid/widget/RemoteViews;


# instance fields
.field private final PROCESS_CPU_WARNING:I

.field private appWidgetIds:[I

.field data:Landroid/os/Bundle;

.field protected mHandler:Landroid/os/Handler;

.field private serviceIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    const v0, 0x7f090002

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    .line 42
    const v0, 0x7f090003

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    .line 44
    const v0, 0x7f090001

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mImageNotiView:I

    .line 64
    sput v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRunningCount:I

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    .line 70
    sput-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWarning:Z

    .line 72
    sput v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_INIT:I

    .line 74
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    .line 76
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_ENABLED:I

    .line 78
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_INIT:I

    sput v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    .line 80
    sput-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isConfiguration:Z

    .line 83
    sput-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNeedUpdate:Z

    .line 97
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    .line 87
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->PROCESS_CPU_WARNING:I

    .line 89
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    .line 195
    new-instance v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider$1;-><init>(Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private forceRestart(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 556
    const-string v2, "ActiveApplication"

    const-string v3, "Restart widget by broadcast intent sent from ControlPanelRemoteService"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_ENABLED:I

    sput v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    .line 559
    const-string v2, "PrefWidgetEnabled"

    invoke-virtual {p1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 560
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 561
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "WidgetEnabled"

    sget v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 562
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 564
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    .line 565
    invoke-direct {p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->startWidgetsUpdating()V

    .line 566
    sput-boolean v4, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isConfiguration:Z

    .line 567
    return-void
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 584
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 585
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 587
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 591
    :goto_0
    return v1

    .line 588
    :catch_0
    move-exception v0

    .line 589
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 104
    const-string v2, "ActiveApplication"

    const-string v3, " START init()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 106
    sput-object p1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    .line 107
    :cond_0
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    if-nez v2, :cond_1

    .line 108
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sput-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    .line 110
    :cond_1
    new-instance v2, Landroid/widget/RemoteViews;

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/high16 v4, 0x7f030000

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    .line 112
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mProgramMonitorUtil:Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;

    if-nez v2, :cond_2

    .line 113
    new-instance v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mProgramMonitorUtil:Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;

    .line 114
    :cond_2
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    if-nez v2, :cond_3

    .line 115
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    sput-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 118
    :cond_3
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    const-class v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .local v0, "clickWidgetIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.widgetapp.activeapplicationwidget.taskcloser.CLICK_WIDGET"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 121
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 122
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 123
    .local v1, "clickWidgetPendingIntent":Landroid/app/PendingIntent;
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 126
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->initSpanMap(Landroid/content/Context;)V

    .line 127
    sget-object v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    .line 128
    return-void
.end method

.method private initSpanMap(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 528
    if-eqz p1, :cond_2

    .line 529
    const-string v11, "ActiveApplicationWidgetSizeX"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 530
    .local v4, "spX":Landroid/content/SharedPreferences;
    const-string v11, "ActiveApplicationWidgetSizeY"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 531
    .local v5, "spY":Landroid/content/SharedPreferences;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 532
    .local v10, "widgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v11, Landroid/content/ComponentName;

    const-class v12, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;

    invoke-direct {v11, p1, v12}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v10, v11}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v9

    .line 533
    .local v9, "widgetIds":[I
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    sput-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    .line 534
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    sput-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    .line 535
    move-object v0, v9

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget v1, v0, v2

    .line 536
    .local v1, "i":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    .line 537
    .local v8, "str":Ljava/lang/String;
    const-string v11, "ActiveApplication"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "widgetIds="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const/4 v11, -0x1

    invoke-interface {v4, v8, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 539
    .local v6, "spanX":I
    const/4 v11, -0x1

    invoke-interface {v5, v8, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 540
    .local v7, "spanY":I
    const/4 v11, -0x1

    if-eq v6, v11, :cond_0

    .line 541
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v8, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    :goto_1
    const/4 v11, -0x1

    if-eq v7, v11, :cond_1

    .line 546
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v8, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 543
    :cond_0
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v8, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 548
    :cond_1
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v8, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 552
    .end local v0    # "arr$":[I
    .end local v1    # "i":I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "spX":Landroid/content/SharedPreferences;
    .end local v5    # "spY":Landroid/content/SharedPreferences;
    .end local v6    # "spanX":I
    .end local v7    # "spanY":I
    .end local v8    # "str":Ljava/lang/String;
    .end local v9    # "widgetIds":[I
    .end local v10    # "widgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_2
    return-void
.end method

.method public static final isTablet()Z
    .locals 2

    .prologue
    .line 579
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 580
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startWidgetsUpdating()V
    .locals 3

    .prologue
    .line 207
    const-string v0, "ActiveApplication"

    const-string v1, "startWidgetsUpdating()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    sget-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    const-string v1, "com.sec.android.app.taskmanager"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    const-string v1, "com.sec.android.app.taskmanager"

    const-string v2, "com.sec.android.app.taskmanager.service.TaskManagerRemoteService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    const-string v1, "startBroadcastResponse"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 215
    sget-object v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 216
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    const-string v1, "com.sec.android.app.controlpanel"

    const-string v2, "com.sec.android.app.controlpanel.service.ControlPanelRemoteService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static final toDigitString(I)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # I

    .prologue
    .line 571
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/Object;

    .line 572
    .local v0, "mArgs":[Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 574
    .local v1, "mBuilder":Ljava/lang/StringBuilder;
    const-string v2, "%d"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method protected static updateDefaultText(Landroid/widget/RemoteViews;I)Landroid/widget/RemoteViews;
    .locals 3
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "spanX"    # I

    .prologue
    .line 173
    const-string v0, "ActiveApplication"

    const-string v1, "START updateDefaultText()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 175
    invoke-static {}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->getRunningPackageCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 176
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 179
    :goto_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 180
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mImageNotiView:I

    const v1, 0x7f020002

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 182
    :cond_0
    const-string v0, "pl"

    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f080006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 192
    :cond_1
    return-object p0

    .line 178
    :cond_2
    sget v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_0
.end method

.method static updateSlideText(Landroid/os/Bundle;)Landroid/widget/RemoteViews;
    .locals 6
    .param p0, "data"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f060004

    const/high16 v4, 0x7f060000

    .line 140
    const-string v1, "ActiveApplication"

    const-string v2, "START updateSlideText()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    if-nez p0, :cond_1

    .line 142
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWarning:Z

    .line 145
    :goto_0
    sget-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWarning:Z

    if-eqz v1, :cond_5

    .line 146
    const-string v1, "warning_level"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "warningLevel":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 148
    const-string v1, "level1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 150
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f060003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 151
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mImageNotiView:I

    const v3, 0x7f020003

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 169
    .end local v0    # "warningLevel":Ljava/lang/String;
    :cond_0
    :goto_1
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    return-object v1

    .line 144
    :cond_1
    const-string v1, "warning"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWarning:Z

    goto :goto_0

    .line 152
    .restart local v0    # "warningLevel":Ljava/lang/String;
    :cond_2
    const-string v1, "level2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 153
    invoke-static {}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isTablet()Z

    move-result v1

    if-nez v1, :cond_3

    .line 154
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 158
    :goto_2
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 159
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mImageNotiView:I

    const v3, 0x7f020004

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    .line 156
    :cond_3
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_2

    .line 161
    :cond_4
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 162
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSlideTextView:I

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f060002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 163
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mImageNotiView:I

    const v3, 0x7f020002

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    .line 167
    .end local v0    # "warningLevel":Ljava/lang/String;
    :cond_5
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateDefaultText(Landroid/widget/RemoteViews;I)Landroid/widget/RemoteViews;

    goto :goto_1
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    const/4 v9, 0x0

    .line 489
    const-string v8, "ActiveApplicationWidgetSizeX"

    invoke-virtual {p1, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 490
    .local v6, "spX":Landroid/content/SharedPreferences;
    const-string v8, "ActiveApplicationWidgetSizeY"

    invoke-virtual {p1, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 491
    .local v7, "spY":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 492
    .local v1, "editorX":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 493
    .local v2, "editorY":Landroid/content/SharedPreferences$Editor;
    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    if-nez v8, :cond_1

    .line 494
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->initSpanMap(Landroid/content/Context;)V

    .line 495
    :cond_1
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget v3, v0, v4

    .line 496
    .local v3, "i":I
    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 497
    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 500
    :cond_2
    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 501
    sget-object v8, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 495
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 505
    .end local v3    # "i":I
    :cond_4
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 506
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 507
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 508
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 512
    sget-object v4, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 513
    sget-object v4, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 514
    :cond_0
    sget-object v4, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 515
    sget-object v4, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 516
    :cond_1
    const-string v4, "ActiveApplicationWidgetSizeX"

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 517
    .local v2, "spX":Landroid/content/SharedPreferences;
    const-string v4, "ActiveApplicationWidgetSizeY"

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 518
    .local v3, "spY":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 519
    .local v0, "editorX":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 520
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 521
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 522
    .local v1, "editorY":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 523
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 524
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 525
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 231
    invoke-super/range {p0 .. p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 232
    const-string v19, "ActiveApplication"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "START onReceive()_intent : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    if-eqz p1, :cond_0

    .line 234
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    if-nez v19, :cond_0

    .line 235
    sput-object p1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    .line 237
    :cond_0
    if-eqz p1, :cond_2

    .line 238
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    if-nez v19, :cond_1

    .line 239
    new-instance v19, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;-><init>(Landroid/content/Context;)V

    sput-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    .line 241
    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v19

    new-instance v20, Landroid/content/ComponentName;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v19 .. v20}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    .line 244
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "action":Ljava/lang/String;
    if-nez v3, :cond_4

    .line 246
    const-string v19, "ActiveApplication"

    const-string v20, "ProgramMonitorProvider onReceive : action is null"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_3
    :goto_0
    return-void

    .line 249
    :cond_4
    if-eqz p1, :cond_3

    .line 251
    const-string v19, "android.appwidget.action.APPWIDGET_ENABLED"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 252
    const-string v19, "ActiveApplication"

    const-string v20, "ACTION_APPWIDGET_ENABLED"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_ENABLED:I

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    .line 255
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    const-string v20, "window"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/WindowManager;

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 256
    .local v7, "display":Landroid/view/Display;
    invoke-virtual {v7}, Landroid/view/Display;->getRotation()I

    move-result v19

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mPrevOrientation:I

    .line 259
    const-string v19, "PrefWidgetEnabled"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 260
    .local v13, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 261
    .local v9, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v19, "WidgetEnabled"

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 262
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 265
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->startWidgetsUpdating()V

    .line 409
    .end local v7    # "display":Landroid/view/Display;
    .end local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v13    # "sp":Landroid/content/SharedPreferences;
    :cond_5
    :goto_1
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    if-eqz v19, :cond_3

    .line 411
    :try_start_0
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v8

    .line 414
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_6
    const-string v19, "android.appwidget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 267
    const-string v19, "ActiveApplication"

    const-string v20, "ACTION_APPWIDGET_UPDATE"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    const-string v20, "window"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/WindowManager;

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 270
    .restart local v7    # "display":Landroid/view/Display;
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mPrevOrientation:I

    invoke-virtual {v7}, Landroid/view/Display;->getRotation()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_7

    .line 271
    const/16 v19, 0x1

    sput-boolean v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNeedUpdate:Z

    .line 272
    invoke-virtual {v7}, Landroid/view/Display;->getRotation()I

    move-result v19

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mPrevOrientation:I

    goto :goto_1

    .line 274
    :cond_7
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    goto :goto_1

    .line 276
    .end local v7    # "display":Landroid/view/Display;
    :cond_8
    const-string v19, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 277
    const-string v19, "ActiveApplication"

    const-string v20, "APPWIDGET_RESIZE"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const-string v19, "ActiveApplicationWidgetSizeX"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 279
    .local v14, "spX":Landroid/content/SharedPreferences;
    const-string v19, "ActiveApplicationWidgetSizeY"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 280
    .local v15, "spY":Landroid/content/SharedPreferences;
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 281
    .local v10, "editorX":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 282
    .local v11, "editorY":Landroid/content/SharedPreferences$Editor;
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-nez v19, :cond_9

    .line 283
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    .line 284
    const/16 v19, 0x1

    sput-boolean v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isConfiguration:Z

    .line 286
    :cond_9
    const-string v19, "widgetspanx"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    .line 287
    .local v16, "spanX":I
    const-string v19, "widgetspany"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 288
    .local v17, "spanY":I
    const-string v19, "widgetId"

    const/16 v20, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 289
    .local v18, "wId":I
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 290
    .local v4, "containX":I
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 291
    .local v5, "containY":I
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    if-eqz v19, :cond_a

    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    if-nez v19, :cond_b

    .line 292
    :cond_a
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->initSpanMap(Landroid/content/Context;)V

    .line 293
    :cond_b
    const-string v19, "ActiveApplication"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "wId="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", spanX="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", spanY="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_c

    const/16 v19, -0x1

    move/from16 v0, v19

    if-le v4, v0, :cond_c

    const/16 v19, -0x1

    move/from16 v0, v19

    if-le v5, v0, :cond_c

    .line 295
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 297
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 298
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 300
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 301
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    goto/16 :goto_1

    .line 302
    :cond_c
    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_d

    const/16 v19, -0x1

    move/from16 v0, v19

    if-le v4, v0, :cond_d

    .line 303
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 305
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 306
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    goto/16 :goto_1

    .line 307
    :cond_d
    const/16 v19, -0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    const/16 v19, -0x1

    move/from16 v0, v19

    if-le v5, v0, :cond_5

    .line 308
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 310
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 311
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    goto/16 :goto_1

    .line 313
    .end local v4    # "containX":I
    .end local v5    # "containY":I
    .end local v10    # "editorX":Landroid/content/SharedPreferences$Editor;
    .end local v11    # "editorY":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "spX":Landroid/content/SharedPreferences;
    .end local v15    # "spY":Landroid/content/SharedPreferences;
    .end local v16    # "spanX":I
    .end local v17    # "spanY":I
    .end local v18    # "wId":I
    :cond_e
    const-string v19, "com.sec.android.intent.action.HOME_RESUME"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_11

    .line 314
    const-string v19, "ActiveApplication"

    const-string v20, "LAUNCHER_RESUME"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-nez v19, :cond_f

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_INIT:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 318
    const-string v19, "PrefWidgetEnabled"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 319
    .restart local v13    # "sp":Landroid/content/SharedPreferences;
    const-string v19, "WidgetEnabled"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v13, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v19

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    .line 320
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_ENABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 321
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    .line 326
    .end local v13    # "sp":Landroid/content/SharedPreferences;
    :cond_f
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 328
    sget-boolean v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNeedUpdate:Z

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 329
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    .line 330
    const/16 v19, 0x0

    sput-boolean v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNeedUpdate:Z

    .line 332
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->startWidgetsUpdating()V

    goto/16 :goto_1

    .line 334
    :cond_11
    const-string v19, "com.sec.android.intent.action.HOME_PAUSE"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 335
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    .line 336
    const-string v19, "ActiveApplication"

    const-string v20, "Widget update running on LAUNCHER_PAUSE"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 339
    :cond_12
    const-string v19, "com.sec.android.app.controlpanel.service.CPU_USAGE_WARN"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_15

    .line 341
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_13

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_INIT:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_14

    .line 342
    :cond_13
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->forceRestart(Landroid/content/Context;)V

    .line 344
    :cond_14
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 345
    const-string v19, "cpu_warning"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 346
    .local v6, "cpuBoolean":Z
    invoke-static {v6}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorUtil;->setCpuWarning(Z)V

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v12

    .line 348
    .local v12, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/os/Bundle;->clear()V

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    move-object/from16 v19, v0

    const-string v20, "warning"

    const-string v21, "cpu_warning"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    move-object/from16 v19, v0

    const-string v20, "warning_level"

    const-string v21, "cpu_warning_level"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    move-object/from16 v19, v0

    const-string v20, "warning_msg"

    const-string v21, "cpu_warning_msg"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->data:Landroid/os/Bundle;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 355
    .end local v6    # "cpuBoolean":Z
    .end local v12    # "msg":Landroid/os/Message;
    :cond_15
    const-string v19, "android.appwidget.action.APPWIDGET_DISABLED"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_16

    .line 356
    const-string v19, "ActiveApplication"

    const-string v20, "ACTION_APPWIDGET_DISABLED"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    .line 360
    const-string v19, "PrefWidgetEnabled"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 361
    .restart local v13    # "sp":Landroid/content/SharedPreferences;
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 362
    .restart local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v19, "WidgetEnabled"

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 363
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    move-object/from16 v19, v0

    const-string v20, "com.sec.android.app.taskmanager"

    const-string v21, "com.sec.android.app.taskmanager.service.TaskManagerRemoteService"

    invoke-virtual/range {v19 .. v21}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 367
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->serviceIntent:Landroid/content/Intent;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_1

    .line 368
    .end local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v13    # "sp":Landroid/content/SharedPreferences;
    :cond_16
    const-string v19, "com.sec.android.app.controlpanel.service.CHECK_PROCESS"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_18

    .line 369
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_17

    sget-boolean v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isConfiguration:Z

    if-eqz v19, :cond_5

    .line 370
    :cond_17
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->forceRestart(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 372
    :cond_18
    const-string v19, "com.sec.android.app.controlpanel.service.RUNNING_COUNT"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1a

    .line 373
    const-string v19, "ActiveApplication"

    const-string v20, "RUNNING_APPS_COUNT"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRunningCount:I

    if-eqz v19, :cond_19

    const-string v19, "runningAppsCount"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    if-nez v19, :cond_19

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v12

    .line 378
    .restart local v12    # "msg":Landroid/os/Message;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 381
    .end local v12    # "msg":Landroid/os/Message;
    :cond_19
    const-string v19, "runningAppsCount"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    sput v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRunningCount:I

    .line 382
    const-string v19, "ActiveApplication"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "runningCount"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget v21, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRunningCount:I

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    .line 385
    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mRunningCount:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateNumberText(I)V

    goto/16 :goto_1

    .line 386
    :cond_1a
    const-string v19, "android.intent.action.LOCALE_CHANGED"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1b

    .line 387
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 388
    const-string v19, "ActiveApplication"

    const-string v20, "CHANGE_LOCALE"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateSlideText(Landroid/os/Bundle;)Landroid/widget/RemoteViews;

    goto/16 :goto_1

    .line 391
    :cond_1b
    const-string v19, "com.sec.android.app.controlpanel.service.TASKMANAGER_STARTED"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1c

    .line 392
    const-string v19, "ActiveApplication"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TASKMANAGER_STARTED_NOTIFICATION mViews = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "isWidgetEnabled = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget v21, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 394
    const-string v19, "ActiveApplication"

    const-string v20, "---startWidgetsUpdating"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->startWidgetsUpdating()V

    goto/16 :goto_1

    .line 399
    :cond_1c
    const-string v19, "com.sec.android.app.controlpanel.service.ENDALL_PROCESS_REFRESH"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1d

    .line 400
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;->mEndAllPressed_Flag:Z

    goto/16 :goto_1

    .line 401
    :cond_1d
    const-string v19, "com.sec.android.widgetapp.activeapplicationwidget.taskcloser.ACTION_ENDALL"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1e

    .line 402
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    goto/16 :goto_1

    .line 403
    :cond_1e
    const-string v19, "com.sec.android.intent.action.LAUNCHER_CHANGED"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 404
    sget-object v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v19, :cond_5

    sget v19, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->isWidgetEnabled:I

    sget v20, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->WIDGET_DISABLED:I

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 405
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->init(Landroid/content/Context;)V

    .line 406
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->startWidgetsUpdating()V

    goto/16 :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 456
    const-string v11, "ActiveApplication"

    const-string v12, "START onUpdate()"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-super/range {p0 .. p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 458
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    if-eqz v11, :cond_0

    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    if-nez v11, :cond_1

    .line 459
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->initSpanMap(Landroid/content/Context;)V

    .line 460
    :cond_1
    const-string v11, "ActiveApplicationWidgetSizeX"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 461
    .local v6, "spX":Landroid/content/SharedPreferences;
    const-string v11, "ActiveApplicationWidgetSizeY"

    const/4 v12, 0x0

    invoke-virtual {p1, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 462
    .local v7, "spY":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 463
    .local v1, "editorX":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 464
    .local v2, "editorY":Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, p3

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_4

    aget v3, v0, v4

    .line 465
    .local v3, "i":I
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    .line 466
    .local v10, "str":Ljava/lang/String;
    const/4 v11, -0x1

    invoke-interface {v6, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 467
    .local v8, "spanX":I
    const/4 v11, -0x1

    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 468
    .local v9, "spanY":I
    const/4 v11, -0x1

    if-ne v8, v11, :cond_2

    .line 469
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    const/4 v11, 0x1

    invoke-interface {v1, v10, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 473
    :cond_2
    const/4 v11, -0x1

    if-ne v9, v11, :cond_3

    .line 474
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    const/4 v11, 0x1

    invoke-interface {v2, v10, v11}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 464
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 477
    :cond_3
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 480
    .end local v3    # "i":I
    .end local v8    # "spanX":I
    .end local v9    # "spanY":I
    .end local v10    # "str":Ljava/lang/String;
    :cond_4
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 481
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 482
    sget-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    if-nez v11, :cond_5

    .line 483
    new-instance v11, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    invoke-direct {v11, p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;-><init>(Landroid/content/Context;)V

    sput-object v11, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mTaskCloser:Lcom/sec/android/widgetapp/activeapplicationwidget/taskcloser/TaskCloserActivity;

    .line 485
    :cond_5
    return-void
.end method

.method protected updateNumberText(I)V
    .locals 4
    .param p1, "runningCount"    # I

    .prologue
    .line 131
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->toDigitString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "count":Ljava/lang/String;
    const-string v1, "ActiveApplication"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Active Applications count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    sget v2, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mNumberView:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 135
    sget-object v1, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mViews1x1:Landroid/widget/RemoteViews;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->updateWidget(Landroid/widget/RemoteViews;)V

    .line 137
    :cond_0
    return-void
.end method

.method protected updateWidget(Landroid/widget/RemoteViews;)V
    .locals 8
    .param p1, "views1x1"    # Landroid/widget/RemoteViews;

    .prologue
    const/4 v7, 0x1

    .line 420
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    new-instance v4, Landroid/content/ComponentName;

    sget-object v5, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    .line 421
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 422
    :cond_0
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mCtxt:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->initSpanMap(Landroid/content/Context;)V

    .line 423
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    array-length v3, v3

    if-lez v3, :cond_6

    .line 425
    :try_start_0
    const-string v3, "ActiveApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateWidget() - appWidgetIds.length:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    array-length v3, v3

    if-ge v2, v3, :cond_5

    .line 427
    iget-object v3, p0, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetIds:[I

    aget v0, v3, v2

    .line 428
    .local v0, "appWidgetId":I
    const-string v3, "ActiveApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "START updateWidget() - appWidgetId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    const-string v3, "ActiveApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSpanMapX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSpanMapY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    .line 431
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3, v0, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 426
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 437
    :cond_3
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gt v3, v7, :cond_2

    .line 438
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapX:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-le v3, v7, :cond_4

    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->mSpanMapY:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v7, :cond_2

    .line 440
    :cond_4
    sget-object v3, Lcom/sec/android/widgetapp/activeapplicationwidget/ProgramMonitorProvider;->appWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3, v0, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 444
    .end local v0    # "appWidgetId":I
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 445
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "ActiveApplication"

    const-string v4, "pass updateWidget() - system server not available"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_5
    :goto_2
    return-void

    .line 450
    :cond_6
    const-string v3, "ActiveApplication"

    const-string v4, "updateWidget() - appWidgetIds.length == 0"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
