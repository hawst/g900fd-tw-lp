.class final Lcom/samsung/android/service/travel/response/TripAdvisorChoice$1;
.super Ljava/lang/Object;
.source "TripAdvisorChoice.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/service/travel/response/TripAdvisorChoice;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 84
    new-instance v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;-><init>()V

    .line 86
    .local v0, "resMessage":Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mRcmId:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mName:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mFeature:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLocationName:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mDistance:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLaunchURL:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLatitude:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLongitude:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mThumbnailImageURL:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mSmallImageURL:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mMediumImageURL:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLargeImageURL:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mOriginalImageURL:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mWebUrl:Ljava/lang/String;

    .line 103
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/TripAdvisorChoice$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/TripAdvisorChoice;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 109
    new-array v0, p1, [Lcom/samsung/android/service/travel/response/TripAdvisorChoice;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/TripAdvisorChoice$1;->newArray(I)[Lcom/samsung/android/service/travel/response/TripAdvisorChoice;

    move-result-object v0

    return-object v0
.end method
