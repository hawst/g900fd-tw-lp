.class public Lcom/samsung/android/service/travel/request/RequestMessage;
.super Ljava/lang/Object;
.source "RequestMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/service/travel/request/RequestMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public cityId:Ljava/lang/String;

.field public fromCurrency:Ljava/lang/String;

.field public latitude:Ljava/lang/String;

.field public longitude:Ljava/lang/String;

.field public mHttpType:I

.field public mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

.field public messageId:I

.field public messanger:Landroid/os/Messenger;

.field public reqId:J

.field public searchStr:Ljava/lang/String;

.field public toCurrency:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public urls:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/samsung/android/service/travel/request/RequestMessage$1;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/request/RequestMessage$1;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/request/RequestMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 83
    iget v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->fromCurrency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->toCurrency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->latitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->longitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->cityId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/android/service/travel/request/RequestMessage;->searchStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    return-void
.end method
