.class public Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
.super Ljava/lang/Object;
.source "WallpaperResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x3606e185b87509bdL


# instance fields
.field public mWallpaperResponse:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/service/travel/response/WallpaperResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage$1;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage$1;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "lat"    # Ljava/lang/String;
    .param p2, "longi"    # Ljava/lang/String;
    .param p3, "cityId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 136
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "https://www.daodao.com/api/platform/1.0/zh_CN/wallpaper_photos?limit=1000&key=31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    aput-object v1, v0, v3

    .line 149
    :goto_0
    return-object v0

    .line 143
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ko_KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    new-array v0, v2, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.tripadvisor.com/api/platform/1.0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/wallpaper_photos?limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0

    .line 149
    :cond_1
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "https://api.tripadvisor.com/api/platform/1.0/wallpaper_photos?limit=1000&key=31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method public merge(Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;)V
    .locals 0
    .param p1, "message"    # Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .prologue
    .line 160
    return-void
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    .locals 12
    .param p1, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 38
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 39
    .local v3, "json":Lorg/json/JSONObject;
    const-string v10, "data"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 40
    .local v4, "jsonArray":Lorg/json/JSONArray;
    new-instance v8, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    invoke-direct {v8}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;-><init>()V

    .line 41
    .local v8, "wallpaperResMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v2, v10, :cond_11

    .line 42
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 43
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    invoke-direct {v7}, Lcom/samsung/android/service/travel/response/WallpaperResponse;-><init>()V

    .line 44
    .local v7, "wallpResponse":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    const-string v10, "location"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 45
    .local v5, "jsonLocationObject":Lorg/json/JSONObject;
    if-eqz v5, :cond_0

    .line 46
    const-string v10, "id"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->id:Ljava/lang/String;

    .line 47
    const-string v10, "name"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->name:Ljava/lang/String;

    .line 48
    const-string v10, "fullname"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->fullName:Ljava/lang/String;

    .line 49
    const-string v10, "shortname"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->shortname:Ljava/lang/String;

    .line 50
    const-string v10, "url"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->launchurl:Ljava/lang/String;

    .line 53
    :cond_0
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "portrait_1080"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 54
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "portrait_1080"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd1080Url:Ljava/lang/String;

    .line 56
    :cond_1
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "portrait_540"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 57
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "portrait_540"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd540Url:Ljava/lang/String;

    .line 59
    :cond_2
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "landscape_1080"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 60
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "landscape_1080"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l1080Url:Ljava/lang/String;

    .line 62
    :cond_3
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "landscape_540"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 63
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "landscape_540"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l540Url:Ljava/lang/String;

    .line 66
    :cond_4
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "square_1024"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 67
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "square_1024"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1024Url:Ljava/lang/String;

    .line 69
    :cond_5
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "square_1280"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 70
    const-string v10, "images"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "square_1280"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1280Url:Ljava/lang/String;

    .line 73
    :cond_6
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd1080Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 74
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd1080Url:Ljava/lang/String;

    .line 77
    :cond_7
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd540Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 78
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd540Url:Ljava/lang/String;

    .line 81
    :cond_8
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l1080Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 82
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l1080Url:Ljava/lang/String;

    .line 85
    :cond_9
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l540Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 86
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->l540Url:Ljava/lang/String;

    .line 89
    :cond_a
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1024Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 90
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1024Url:Ljava/lang/String;

    .line 93
    :cond_b
    const-string v10, "null"

    iget-object v11, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1280Url:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 94
    const/4 v10, 0x0

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1280Url:Ljava/lang/String;

    .line 97
    :cond_c
    sget-object v10, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-virtual {v10}, Lcom/samsung/android/service/travel/TravelService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f070008

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v9, v10

    .line 99
    .local v9, "width":I
    sget-object v10, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-virtual {v10}, Lcom/samsung/android/service/travel/TravelService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f070009

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v1, v10

    .line 103
    .local v1, "height":I
    const/16 v10, 0x438

    if-ne v9, v10, :cond_e

    const/16 v10, 0x780

    if-ne v1, v10, :cond_e

    .line 104
    iget-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd1080Url:Ljava/lang/String;

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;

    .line 124
    :cond_d
    :goto_1
    iget-object v10, v8, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 106
    :cond_e
    const/16 v10, 0x4b0

    if-ne v9, v10, :cond_f

    const/16 v10, 0x2d0

    if-eq v1, v10, :cond_10

    :cond_f
    const/16 v10, 0x500

    if-ne v1, v10, :cond_12

    const/16 v10, 0x2d0

    if-ne v9, v10, :cond_12

    .line 108
    :cond_10
    iget-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1280Url:Ljava/lang/String;

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 127
    .end local v1    # "height":I
    .end local v2    # "i":I
    .end local v3    # "json":Lorg/json/JSONObject;
    .end local v4    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "jsonLocationObject":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v7    # "wallpResponse":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    .end local v8    # "wallpaperResMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    .end local v9    # "width":I
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 130
    const/4 v8, 0x0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_11
    return-object v8

    .line 110
    .restart local v1    # "height":I
    .restart local v2    # "i":I
    .restart local v3    # "json":Lorg/json/JSONObject;
    .restart local v4    # "jsonArray":Lorg/json/JSONArray;
    .restart local v5    # "jsonLocationObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "wallpResponse":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    .restart local v8    # "wallpaperResMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    .restart local v9    # "width":I
    :cond_12
    const/16 v10, 0x21c

    if-ne v9, v10, :cond_13

    const/16 v10, 0x3c0

    if-ne v1, v10, :cond_13

    .line 111
    :try_start_1
    iget-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd540Url:Ljava/lang/String;

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;

    goto :goto_1

    .line 116
    :cond_13
    const/16 v10, 0x1e0

    if-ne v9, v10, :cond_14

    const/16 v10, 0x320

    if-ne v1, v10, :cond_14

    .line 117
    iget-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->hd540Url:Ljava/lang/String;

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;

    goto :goto_1

    .line 119
    :cond_14
    const/16 v10, 0x500

    if-ne v9, v10, :cond_15

    const/16 v10, 0x320

    if-eq v1, v10, :cond_17

    :cond_15
    const/16 v10, 0x320

    if-ne v9, v10, :cond_16

    const/16 v10, 0x500

    if-eq v1, v10, :cond_17

    :cond_16
    const/16 v10, 0x258

    if-ne v9, v10, :cond_d

    const/16 v10, 0x400

    if-ne v1, v10, :cond_d

    .line 122
    :cond_17
    iget-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->sq1280Url:Ljava/lang/String;

    iput-object v10, v7, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WallpaperResponseMessage [mWallpaperResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 170
    return-void
.end method
