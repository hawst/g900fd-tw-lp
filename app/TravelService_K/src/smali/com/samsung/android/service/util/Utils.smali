.class public Lcom/samsung/android/service/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public static IsWifiOnlyModel(Landroid/content/Context;)Z
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 729
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 731
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 732
    .local v1, "mobileNetwork":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    .line 734
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static checkNetwork(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 340
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 342
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 343
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static checkTravelLockscreen(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 703
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "lockscreen_wallpaper_path"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 710
    .local v1, "mWallpaperPath":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "lockscreen_wallpaper"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 712
    .local v0, "isLockScreen":I
    if-eqz v1, :cond_2

    .line 713
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 714
    .local v2, "wallpaperFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 715
    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/images"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    if-ne v0, v3, :cond_1

    .line 725
    .end local v2    # "wallpaperFile":Ljava/io/File;
    :goto_0
    return v3

    .restart local v2    # "wallpaperFile":Ljava/io/File;
    :cond_1
    move v3, v4

    .line 723
    goto :goto_0

    .end local v2    # "wallpaperFile":Ljava/io/File;
    :cond_2
    move v3, v4

    .line 725
    goto :goto_0
.end method

.method public static isDeviceConnectedToNetwork(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 68
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 69
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 348
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static final log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string v0, "TravelService"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    return-void
.end method

.method public static final log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    return-void
.end method

.method public static overlay(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 18
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "image"    # Landroid/graphics/Bitmap;
    .param p2, "locationName"    # Ljava/lang/String;
    .param p3, "locationId"    # Ljava/lang/String;
    .param p4, "extension"    # Ljava/lang/String;

    .prologue
    .line 355
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 356
    .local v5, "res":Landroid/content/res/Resources;
    const v13, 0x7f070008

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v10, v13

    .line 357
    .local v10, "screenWidth":I
    const v13, 0x7f070009

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v9, v13

    .line 360
    .local v9, "screenHeight":I
    const v13, 0x7f020008

    invoke-static {v5, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 362
    .local v4, "logoBitmap":Landroid/graphics/Bitmap;
    const/4 v8, 0x0

    .line 363
    .local v8, "scaledLogoBitmap":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_0

    .line 364
    const v13, 0x7f070001

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    const/high16 v14, 0x7f070000

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    const/4 v15, 0x0

    invoke-static {v4, v13, v14, v15}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 372
    :cond_0
    const/4 v6, 0x0

    .line 374
    .local v6, "rightMarginExtra":I
    const/16 v13, 0x500

    if-ne v10, v13, :cond_1

    const/16 v13, 0x320

    if-eq v9, v13, :cond_5

    :cond_1
    const/16 v13, 0x320

    if-ne v10, v13, :cond_2

    const/16 v13, 0x500

    if-eq v9, v13, :cond_5

    :cond_2
    const/16 v13, 0x258

    if-ne v10, v13, :cond_3

    const/16 v13, 0x400

    if-eq v9, v13, :cond_5

    :cond_3
    const/16 v13, 0x4b0

    if-ne v10, v13, :cond_4

    const/16 v13, 0x2d0

    if-eq v9, v13, :cond_5

    :cond_4
    const/16 v13, 0x500

    if-ne v9, v13, :cond_13

    const/16 v13, 0x2d0

    if-ne v10, v13, :cond_13

    .line 377
    :cond_5
    invoke-static/range {p1 .. p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 385
    .local v7, "scaledImage":Landroid/graphics/Bitmap;
    :goto_0
    const/16 v13, 0x500

    if-ne v10, v13, :cond_6

    const/16 v13, 0x320

    if-eq v9, v13, :cond_a

    :cond_6
    const/16 v13, 0x320

    if-ne v10, v13, :cond_7

    const/16 v13, 0x500

    if-eq v9, v13, :cond_a

    :cond_7
    const/16 v13, 0x258

    if-ne v10, v13, :cond_8

    const/16 v13, 0x400

    if-eq v9, v13, :cond_a

    :cond_8
    const/16 v13, 0x4b0

    if-ne v10, v13, :cond_9

    const/16 v13, 0x2d0

    if-eq v9, v13, :cond_a

    :cond_9
    const/16 v13, 0x500

    if-ne v9, v13, :cond_b

    const/16 v13, 0x2d0

    if-ne v10, v13, :cond_b

    .line 389
    :cond_a
    sub-int v13, v10, v9

    div-int/lit8 v13, v13, 0x2

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 392
    :cond_b
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 395
    .local v1, "bmOverlay":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 397
    .local v2, "canvas":Landroid/graphics/Canvas;
    if-eqz v7, :cond_c

    .line 398
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v2, v7, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 401
    :cond_c
    if-eqz v8, :cond_d

    .line 402
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    const v14, 0x7f070001

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    const v15, 0x7f070002

    invoke-virtual {v5, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v15

    add-float/2addr v14, v15

    int-to-float v15, v6

    add-float/2addr v14, v15

    float-to-int v14, v14

    sub-int/2addr v13, v14

    int-to-float v13, v13

    const v14, 0x7f070003

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    int-to-float v14, v14

    const/4 v15, 0x0

    invoke-virtual {v2, v8, v13, v14, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 418
    :cond_d
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/service/util/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_f

    .line 419
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f020002

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    check-cast v11, Landroid/graphics/drawable/NinePatchDrawable;

    .line 422
    .local v11, "textBackground":Landroid/graphics/drawable/NinePatchDrawable;
    if-eqz v11, :cond_e

    .line 423
    const/4 v13, 0x0

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    const v15, 0x7f070006

    invoke-virtual {v5, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v15

    float-to-int v15, v15

    sub-int/2addr v14, v15

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v11, v13, v14, v15, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 430
    invoke-virtual {v11, v2}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 433
    :cond_e
    new-instance v12, Landroid/text/TextPaint;

    invoke-direct {v12}, Landroid/text/TextPaint;-><init>()V

    .line 434
    .local v12, "textPaint":Landroid/text/TextPaint;
    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Landroid/text/TextPaint;->setColor(I)V

    .line 435
    sget-object v13, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v12, v13}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 436
    const v13, 0x7f070004

    invoke-virtual {v5, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    int-to-float v13, v13

    invoke-virtual {v12, v13}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 438
    int-to-float v13, v10

    const v14, 0x7f070007

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    sub-float/2addr v13, v14

    sget-object v14, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, p2

    invoke-static {v0, v12, v13, v14}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f070005

    invoke-virtual {v5, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    add-int/2addr v14, v6

    int-to-float v14, v14

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    const v16, 0x7f070006

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v16

    const v17, 0x7f070004

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    sub-float v16, v16, v17

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    sub-int v15, v15, v16

    int-to-float v15, v15

    invoke-virtual {v2, v13, v14, v15, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 455
    .end local v11    # "textBackground":Landroid/graphics/drawable/NinePatchDrawable;
    .end local v12    # "textPaint":Landroid/text/TextPaint;
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 456
    if-eqz v4, :cond_10

    .line 457
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 458
    :cond_10
    if-eqz v7, :cond_11

    .line 459
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 461
    :cond_11
    if-eqz v8, :cond_12

    .line 462
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 472
    .end local v1    # "bmOverlay":Landroid/graphics/Bitmap;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "logoBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "res":Landroid/content/res/Resources;
    .end local v6    # "rightMarginExtra":I
    .end local v7    # "scaledImage":Landroid/graphics/Bitmap;
    .end local v8    # "scaledLogoBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "screenHeight":I
    .end local v10    # "screenWidth":I
    :cond_12
    :goto_1
    return-object v1

    .line 379
    .restart local v4    # "logoBitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "res":Landroid/content/res/Resources;
    .restart local v6    # "rightMarginExtra":I
    .restart local v8    # "scaledLogoBitmap":Landroid/graphics/Bitmap;
    .restart local v9    # "screenHeight":I
    .restart local v10    # "screenWidth":I
    :cond_13
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    if-ne v10, v13, :cond_14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    if-ne v9, v13, :cond_14

    .line 380
    invoke-static/range {p1 .. p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    .restart local v7    # "scaledImage":Landroid/graphics/Bitmap;
    goto/16 :goto_0

    .line 382
    .end local v7    # "scaledImage":Landroid/graphics/Bitmap;
    :cond_14
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/samsung/android/service/util/Utils;->resizeBitmap(Landroid/graphics/Bitmap;Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .restart local v7    # "scaledImage":Landroid/graphics/Bitmap;
    goto/16 :goto_0

    .line 469
    .end local v4    # "logoBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "res":Landroid/content/res/Resources;
    .end local v6    # "rightMarginExtra":I
    .end local v7    # "scaledImage":Landroid/graphics/Bitmap;
    .end local v8    # "scaledLogoBitmap":Landroid/graphics/Bitmap;
    .end local v9    # "screenHeight":I
    .end local v10    # "screenWidth":I
    :catch_0
    move-exception v3

    .line 470
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 472
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static resizeBitmap(Landroid/graphics/Bitmap;Landroid/content/res/Resources;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "image"    # Landroid/graphics/Bitmap;
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v11, 0x0

    .line 476
    const v8, 0x7f070008

    invoke-virtual {p1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 477
    .local v7, "width":F
    const v8, 0x7f070009

    invoke-virtual {p1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 478
    .local v4, "height":F
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float v5, v8, v9

    .line 480
    .local v5, "original_ratio":F
    div-float v2, v7, v4

    .line 486
    .local v2, "designer_ratio":F
    cmpl-float v8, v5, v2

    if-lez v8, :cond_0

    .line 487
    move v1, v4

    .line 488
    .local v1, "designer_height":F
    mul-float v3, v1, v5

    .line 490
    .local v3, "designer_width":F
    float-to-int v8, v3

    float-to-int v9, v1

    invoke-static {p0, v8, v9, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 493
    .local v0, "bmp":Landroid/graphics/Bitmap;
    sub-float v8, v3, v7

    div-float/2addr v8, v10

    float-to-int v8, v8

    float-to-int v9, v7

    float-to-int v10, v4

    invoke-static {v0, v8, v11, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 512
    .local v6, "scaledBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v6

    .line 499
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v1    # "designer_height":F
    .end local v3    # "designer_width":F
    .end local v6    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_0
    move v3, v7

    .line 500
    .restart local v3    # "designer_width":F
    div-float v1, v3, v5

    .line 502
    .restart local v1    # "designer_height":F
    float-to-int v8, v3

    float-to-int v9, v1

    invoke-static {p0, v8, v9, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 505
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    sub-float v8, v1, v4

    div-float/2addr v8, v10

    float-to-int v8, v8

    float-to-int v9, v7

    float-to-int v10, v4

    invoke-static {v0, v11, v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v6

    .restart local v6    # "scaledBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public static setLocationName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "locationName"    # Ljava/lang/String;

    .prologue
    .line 281
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_name"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 284
    return-void
.end method

.method public static setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "count"    # I
    .param p3, "cycle_count"    # I

    .prologue
    const-wide/16 v4, 0x3e8

    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, "retValue":Z
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 187
    .local v0, "delayHandler":Landroid/os/Handler;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_path"

    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 190
    if-nez v1, :cond_0

    .line 191
    new-instance v2, Lcom/samsung/android/service/util/Utils$1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/service/util/Utils$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 202
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_path_ripple"

    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 206
    if-nez v1, :cond_1

    .line 207
    new-instance v2, Lcom/samsung/android/service/util/Utils$2;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/service/util/Utils$2;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 220
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_path_2"

    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 223
    if-nez v1, :cond_2

    .line 224
    new-instance v2, Lcom/samsung/android/service/util/Utils$3;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/service/util/Utils$3;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 236
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_path_ripple_2"

    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 239
    if-nez v1, :cond_3

    .line 240
    new-instance v2, Lcom/samsung/android/service/util/Utils$4;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/service/util/Utils$4;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 251
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "last update image"

    invoke-static {v2, v3, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 253
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "metadata_update_cycle"

    invoke-static {v2, v3, p3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/samsung/android/service/util/Utils;->setUpdatedTimeStamp(Landroid/content/Context;J)V

    .line 256
    return-void
.end method

.method public static setUpdatedTimeStamp(Landroid/content/Context;J)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "time"    # J

    .prologue
    .line 276
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "last update time"

    invoke-static {v0, v1, p1, p2}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 278
    return-void
.end method
