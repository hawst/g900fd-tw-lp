.class public Lcom/samsung/android/service/travel/http/HttpClientPoolManager;
.super Ljava/lang/Object;
.source "HttpClientPoolManager.java"


# static fields
.field private static bNotKeepAlive:Z

.field private static final instance:Lcom/samsung/android/service/travel/http/HttpClientPoolManager;


# instance fields
.field private volatile manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->instance:Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->bNotKeepAlive:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getDefaultParams()Lorg/apache/http/params/HttpParams;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 152
    .local v0, "hParam":Lorg/apache/http/params/HttpParams;
    const/16 v1, 0x4e20

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 153
    const v1, 0x1d4c0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 154
    return-object v0
.end method

.method public static getInstance()Lcom/samsung/android/service/travel/http/HttpClientPoolManager;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->instance:Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    return-object v0
.end method

.method private getManager()Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->initialize()V

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    return-object v0
.end method


# virtual methods
.method protected getDefaultHeaderGroup()Lorg/apache/http/message/HeaderGroup;
    .locals 5

    .prologue
    .line 113
    new-instance v0, Lorg/apache/http/message/HeaderGroup;

    invoke-direct {v0}, Lorg/apache/http/message/HeaderGroup;-><init>()V

    .line 114
    .local v0, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    const-string v2, "HTTPCLIENTPOOL_UA"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "ua":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 116
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "User-Agent"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 123
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user agent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 125
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept"

    const-string v4, "*/*"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 126
    sget-boolean v2, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->bNotKeepAlive:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 127
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Connection"

    const-string v4, "close"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 135
    :goto_1
    return-object v0

    .line 119
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "User-Agent"

    const-string v4, "SAMSUNG-Android"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_0

    .line 129
    :cond_1
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Connection"

    const-string v4, "keep-alive"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_1
.end method

.method protected getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getDefaultParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getHttpClient(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method protected getHttpClient(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/client/HttpClient;
    .locals 1
    .param p1, "hParam"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 188
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getHttpClient(Lorg/apache/http/params/HttpParams;I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method protected getHttpClient(Lorg/apache/http/params/HttpParams;I)Lorg/apache/http/client/HttpClient;
    .locals 8
    .param p1, "hParam"    # Lorg/apache/http/params/HttpParams;
    .param p2, "retryCount"    # I

    .prologue
    .line 158
    if-nez p1, :cond_0

    .line 159
    new-instance p1, Lorg/apache/http/params/BasicHttpParams;

    .end local p1    # "hParam":Lorg/apache/http/params/HttpParams;
    invoke-direct {p1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 162
    .restart local p1    # "hParam":Lorg/apache/http/params/HttpParams;
    :cond_0
    const-string v6, "HTTPCLIENTPOOL_PROXY_HOST"

    invoke-static {v6}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "proxyHost":Ljava/lang/String;
    const-string v6, "HTTPCLIENTPOOL_PROXY_PORT"

    invoke-static {v6}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 165
    .local v4, "proxyPort":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 166
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 167
    .local v5, "proxyPort2":I
    new-instance v2, Lorg/apache/http/HttpHost;

    invoke-direct {v2, v3, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 168
    .local v2, "proxy":Lorg/apache/http/HttpHost;
    const-string v6, "http.route.default-proxy"

    invoke-interface {p1, v6, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 169
    const-string v6, "http.default-host"

    invoke-interface {p1, v6, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 171
    if-eqz v4, :cond_1

    .line 172
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getHttpClient - Proxy : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 180
    .end local v2    # "proxy":Lorg/apache/http/HttpHost;
    .end local v5    # "proxyPort2":I
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getManager()Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    move-result-object v1

    .line 182
    .local v1, "m":Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    new-instance v0, Lcom/samsung/android/service/travel/http/AdvancedHttpClient;

    invoke-direct {v0, v1, p1}, Lcom/samsung/android/service/travel/http/AdvancedHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 183
    .local v0, "client":Lcom/samsung/android/service/travel/http/AdvancedHttpClient;
    invoke-virtual {v0, p2}, Lcom/samsung/android/service/travel/http/AdvancedHttpClient;->setRetryCount(I)V

    .line 184
    return-object v0

    .line 174
    .end local v0    # "client":Lcom/samsung/android/service/travel/http/AdvancedHttpClient;
    .end local v1    # "m":Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    .restart local v2    # "proxy":Lorg/apache/http/HttpHost;
    .restart local v5    # "proxyPort2":I
    :cond_1
    const-string v6, "getHttpClient - Proxy port is null "

    invoke-static {v6}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 177
    .end local v2    # "proxy":Lorg/apache/http/HttpHost;
    .end local v5    # "proxyPort2":I
    :cond_2
    const-string v6, "getHttpClient - Proxy is null "

    invoke-static {v6}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized initialize()V
    .locals 8

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    if-nez v4, :cond_5

    .line 76
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 77
    .local v0, "hParam":Lorg/apache/http/params/HttpParams;
    const-string v4, "http.conn-manager.max-total"

    const/4 v5, 0x5

    invoke-interface {v0, v4, v5}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 78
    const-string v4, "http.conn-manager.max-per-route"

    new-instance v5, Lcom/samsung/android/service/travel/http/HttpClientPoolManager$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager$1;-><init>(Lcom/samsung/android/service/travel/http/HttpClientPoolManager;)V

    invoke-interface {v0, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 84
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 85
    .local v3, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    const-string v4, "HTTPCLIENTPOOL_PROXY_PORT"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "ports":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 87
    :cond_0
    const-string v4, "HTTPCLIENTPOOL_PORT"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 89
    :cond_1
    const/16 v1, 0x50

    .line 90
    .local v1, "port":I
    if-eqz v2, :cond_2

    .line 91
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 93
    :cond_2
    if-lez v1, :cond_3

    const v4, 0xffff

    if-le v1, v4, :cond_4

    .line 95
    :cond_3
    const/16 v1, 0x50

    .line 98
    :cond_4
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    invoke-direct {v4, v5, v6, v1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 99
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v6

    const/16 v7, 0x1bb

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 101
    new-instance v4, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v4, v0, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    iput-object v4, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    .end local v0    # "hParam":Lorg/apache/http/params/HttpParams;
    .end local v1    # "port":I
    .end local v2    # "ports":Ljava/lang/String;
    .end local v3    # "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    :cond_5
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-virtual {v0}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;->shutdown()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->manager:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .line 208
    :cond_0
    return-void
.end method
