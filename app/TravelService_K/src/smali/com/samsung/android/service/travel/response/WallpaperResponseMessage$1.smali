.class final Lcom/samsung/android/service/travel/response/WallpaperResponseMessage$1;
.super Ljava/lang/Object;
.source "WallpaperResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    .locals 3
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 176
    new-instance v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;-><init>()V

    .line 177
    .local v0, "resMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    iget-object v1, v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    sget-object v2, Lcom/samsung/android/service/travel/response/WallpaperResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 178
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 183
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage$1;->newArray(I)[Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v0

    return-object v0
.end method
