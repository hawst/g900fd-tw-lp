.class public Lcom/samsung/android/service/travel/http/AdvancedHttpClient;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "AdvancedHttpClient.java"


# instance fields
.field private retryCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .param p1, "conman"    # Lorg/apache/http/conn/ClientConnectionManager;
    .param p2, "params"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected createHttpRequestRetryHandler()Lorg/apache/http/client/HttpRequestRetryHandler;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/android/service/travel/http/AdvancedHttpRequestRetryHandler;

    iget v1, p0, Lcom/samsung/android/service/travel/http/AdvancedHttpClient;->retryCount:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/service/travel/http/AdvancedHttpRequestRetryHandler;-><init>(IZ)V

    return-object v0
.end method

.method public setRetryCount(I)V
    .locals 0
    .param p1, "retryCount"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/android/service/travel/http/AdvancedHttpClient;->retryCount:I

    .line 50
    return-void
.end method
