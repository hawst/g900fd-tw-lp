.class public abstract Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
.super Ljava/lang/Object;
.source "TravelServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TravelServiceResponseCallback"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapResponse(JLandroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "imageKey"    # Ljava/lang/String;

    .prologue
    .line 328
    return-void
.end method

.method public onCancel(J)V
    .locals 0
    .param p1, "reqId"    # J

    .prologue
    .line 339
    return-void
.end method

.method public onCityList(JLcom/samsung/android/service/travel/response/TravelCityListResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "responsecityList"    # Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;

    .prologue
    .line 349
    return-void
.end method

.method public onError(JILjava/lang/Object;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "requestMessageid"    # I
    .param p4, "value"    # Ljava/lang/Object;

    .prologue
    .line 341
    return-void
.end method

.method public onLocationReview(JLcom/samsung/android/service/travel/response/LocationReviewResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "reviewMessage"    # Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;

    .prologue
    .line 347
    return-void
.end method

.method public onSearchResult(JLcom/samsung/android/service/travel/response/SearchResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "searchResponse"    # Lcom/samsung/android/service/travel/response/SearchResponseMessage;

    .prologue
    .line 350
    return-void
.end method

.method public onSelectCity(JLcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "selectCityResponse"    # Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    .prologue
    .line 345
    return-void
.end method

.method public onTripAdvisorChoiceResponse(JLcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "tripAdvisorChoice"    # Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .prologue
    .line 343
    return-void
.end method

.method public onWallpaperResult(JLcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V
    .locals 0
    .param p1, "reqId"    # J
    .param p3, "wallpaperResponse"    # Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .prologue
    .line 331
    return-void
.end method
