.class Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;
.super Ljava/lang/Object;
.source "TravelServiceConnectionManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServiceConnectionImpl"
.end annotation


# instance fields
.field runnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p2, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->runnable:Ljava/lang/Runnable;

    .line 168
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z
    invoke-static {v0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$302(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Z)Z

    .line 177
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$402(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->runnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->runnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 182
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z
    invoke-static {v0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$302(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Z)Z

    .line 187
    return-void
.end method

.method public setRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->runnable:Ljava/lang/Runnable;

    .line 172
    return-void
.end method
