.class public Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;
.super Ljava/lang/Object;
.source "TravelCityListResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0xdd48d36edd136aeL


# instance fields
.field public mAPIName:Ljava/lang/String;

.field public mCities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/TravelCity;",
            ">;"
        }
    .end annotation
.end field

.field public mId:Ljava/lang/String;

.field public mStatus:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage$1;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage$1;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mCities:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "lat"    # Ljava/lang/String;
    .param p2, "longi"    # Ljava/lang/String;
    .param p3, "miles"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 111
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://www.daodao.com/api/platform/1.0/zh_CN/map/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/geos?key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&distance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 117
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.tripadvisor.com/api/platform/1.0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/map/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/geos?key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&distance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method public merge(Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;)V
    .locals 0
    .param p1, "message"    # Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .prologue
    .line 131
    return-void
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    .locals 12
    .param p1, "jsonString"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 70
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 71
    .local v3, "jsonOb":Lorg/json/JSONObject;
    new-instance v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;

    invoke-direct {v5}, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;-><init>()V

    .line 72
    .local v5, "responseMessage":Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;
    const-string v8, "1"

    iput-object v8, v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mStatus:Ljava/lang/String;

    .line 73
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mId:Ljava/lang/String;

    .line 74
    const-string v8, "getTravelCityList"

    iput-object v8, v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mAPIName:Ljava/lang/String;

    .line 75
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mCities:Ljava/util/List;

    .line 76
    const-string v8, "data"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 77
    .local v2, "jsonDataArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v1, v8, :cond_5

    .line 78
    new-instance v6, Lcom/samsung/android/service/travel/response/TravelCity;

    invoke-direct {v6}, Lcom/samsung/android/service/travel/response/TravelCity;-><init>()V

    .line 79
    .local v6, "travelCity":Lcom/samsung/android/service/travel/response/TravelCity;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 80
    .local v4, "jsonObjectDataArry":Lorg/json/JSONObject;
    const-string v8, "location_id"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mId:Ljava/lang/String;

    .line 81
    const-string v8, "name"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mCityName:Ljava/lang/String;

    .line 82
    const-string v8, "address_obj"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "country"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mCountryName:Ljava/lang/String;

    .line 84
    const-string v8, "latitude"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLatitude:Ljava/lang/String;

    .line 85
    const-string v8, "longitude"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLongitude:Ljava/lang/String;

    .line 86
    const-string v8, "null"

    iget-object v9, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLatitude:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 87
    const/4 v8, 0x0

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLatitude:Ljava/lang/String;

    .line 89
    :cond_0
    const-string v8, "null"

    iget-object v9, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLongitude:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 90
    const/4 v8, 0x0

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLongitude:Ljava/lang/String;

    .line 92
    :cond_1
    const-string v8, "null"

    iget-object v9, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 93
    const/4 v8, 0x0

    iput-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mId:Ljava/lang/String;

    .line 95
    :cond_2
    iget-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLatitude:Ljava/lang/String;

    if-eqz v8, :cond_3

    iget-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mLongitude:Ljava/lang/String;

    if-eqz v8, :cond_3

    iget-object v8, v6, Lcom/samsung/android/service/travel/response/TravelCity;->mId:Ljava/lang/String;

    if-nez v8, :cond_4

    .line 77
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    :cond_4
    iget-object v8, v5, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mCities:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 102
    .end local v1    # "i":I
    .end local v2    # "jsonDataArray":Lorg/json/JSONArray;
    .end local v3    # "jsonOb":Lorg/json/JSONObject;
    .end local v4    # "jsonObjectDataArry":Lorg/json/JSONObject;
    .end local v5    # "responseMessage":Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;
    .end local v6    # "travelCity":Lcom/samsung/android/service/travel/response/TravelCity;
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v5, v7

    .line 105
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    return-object v5
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TravelCityListResponseMessage [mStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAPIName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mAPIName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mCities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mAPIName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;->mCities:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 45
    return-void
.end method
