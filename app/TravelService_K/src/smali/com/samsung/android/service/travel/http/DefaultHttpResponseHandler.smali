.class public Lcom/samsung/android/service/travel/http/DefaultHttpResponseHandler;
.super Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;
.source "DefaultHttpResponseHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/http/DefaultHttpResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 10
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    if-nez p1, :cond_1

    .line 35
    const/4 v8, 0x0

    .line 75
    :cond_0
    :goto_0
    return-object v8

    .line 37
    :cond_1
    const/16 v8, 0x100

    new-array v7, v8, [B

    .line 39
    .local v7, "sBuffer":[B
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    .line 41
    .local v6, "responseCode":I
    invoke-virtual {p0, v6}, Lcom/samsung/android/service/travel/http/DefaultHttpResponseHandler;->setResponseCode(I)V

    .line 46
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    .line 47
    .local v5, "rentity":Lorg/apache/http/HttpEntity;
    const/4 v3, 0x0

    .line 48
    .local v3, "inputStream":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 51
    .local v0, "content":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 53
    .local v4, "readBytes":I
    :try_start_0
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 54
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    .end local v0    # "content":Ljava/io/ByteArrayOutputStream;
    .local v1, "content":Ljava/io/ByteArrayOutputStream;
    :goto_1
    :try_start_1
    invoke-virtual {v3, v7}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v8, -0x1

    if-eq v4, v8, :cond_4

    .line 56
    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 59
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 61
    .end local v1    # "content":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "content":Ljava/io/ByteArrayOutputStream;
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_3
    if-eqz v0, :cond_2

    .line 65
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 70
    :cond_2
    :goto_4
    if-eqz v3, :cond_3

    .line 72
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 75
    :cond_3
    :goto_5
    throw v8

    .line 58
    .end local v0    # "content":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "content":Ljava/io/ByteArrayOutputStream;
    :cond_4
    :try_start_5
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v8

    .line 63
    if-eqz v1, :cond_5

    .line 65
    :try_start_6
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 70
    :cond_5
    :goto_6
    if-eqz v3, :cond_0

    .line 72
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_0

    .line 73
    :catch_1
    move-exception v9

    goto :goto_0

    .line 66
    :catch_2
    move-exception v9

    goto :goto_6

    .end local v1    # "content":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "content":Ljava/io/ByteArrayOutputStream;
    :catch_3
    move-exception v9

    goto :goto_4

    .line 73
    :catch_4
    move-exception v9

    goto :goto_5

    .line 63
    .end local v0    # "content":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "content":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v8

    move-object v0, v1

    .end local v1    # "content":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "content":Ljava/io/ByteArrayOutputStream;
    goto :goto_3

    .line 59
    :catch_5
    move-exception v2

    goto :goto_2
.end method
