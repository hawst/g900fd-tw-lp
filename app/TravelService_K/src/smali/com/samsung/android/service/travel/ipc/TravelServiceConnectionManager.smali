.class public Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
.super Ljava/lang/Object;
.source "TravelServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;,
        Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;,
        Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;
    }
.end annotation


# instance fields
.field private _callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

.field private _serviceBound:Z

.field private mContext:Landroid/content/Context;

.field private messenger:Landroid/os/Messenger;

.field messengerIncoming:Landroid/os/Messenger;

.field serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;-><init>(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messengerIncoming:Landroid/os/Messenger;

    .line 190
    new-instance v0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;-><init>(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    .line 136
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Lcom/samsung/android/service/travel/request/RequestMessage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
    .param p1, "x1"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->sendMessageInternal(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z

    return p1
.end method

.method static synthetic access$402(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messenger:Landroid/os/Messenger;

    return-object p1
.end method

.method private bindService(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;->setRunnable(Ljava/lang/Runnable;)V

    .line 197
    iget-object v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.service.travel"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method private sendMessageInternal(Lcom/samsung/android/service/travel/request/RequestMessage;)Z
    .locals 6
    .param p1, "reqMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x0

    .line 218
    const/4 v4, 0x0

    invoke-static {v4, v5, v3, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    .line 219
    .local v2, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messengerIncoming:Landroid/os/Messenger;

    iput-object v4, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    .line 220
    iput v5, v2, Landroid/os/Message;->what:I

    .line 222
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 223
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "request"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 224
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 226
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messenger:Landroid/os/Messenger;

    invoke-virtual {v4, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 227
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public cancelRequest(J)V
    .locals 3
    .param p1, "reqID"    # J

    .prologue
    .line 279
    new-instance v0, Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/request/RequestMessage;-><init>()V

    .line 280
    .local v0, "reqMessage":Lcom/samsung/android/service/travel/request/RequestMessage;
    const/4 v1, 0x4

    iput v1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    .line 281
    iput-wide p1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    .line 282
    invoke-virtual {p0, v0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->sendMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    .line 283
    return-void
.end method

.method public getWallpaper()J
    .locals 4

    .prologue
    .line 255
    new-instance v0, Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/request/RequestMessage;-><init>()V

    .line 256
    .local v0, "reqMessage":Lcom/samsung/android/service/travel/request/RequestMessage;
    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 257
    const/16 v1, 0xa

    iput v1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    .line 258
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    .line 259
    invoke-virtual {p0, v0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->sendMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    .line 260
    iget-wide v2, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    return-wide v2
.end method

.method public getWallpaperImage(Ljava/lang/String;)J
    .locals 4
    .param p1, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v0, Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/request/RequestMessage;-><init>()V

    .line 246
    .local v0, "reqMessage":Lcom/samsung/android/service/travel/request/RequestMessage;
    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 247
    iput-object p1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    .line 248
    const/16 v1, 0xb

    iput v1, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    .line 249
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    .line 250
    invoke-virtual {p0, v0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->sendMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    .line 251
    iget-wide v2, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    return-wide v2
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 203
    iget-boolean v1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z

    if-eqz v1, :cond_0

    .line 205
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->serviceConnImpl:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$ServiceConnectionImpl;

    .line 212
    iput-object v3, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->messengerIncoming:Landroid/os/Messenger;

    .line 213
    iput-object v3, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 214
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z

    .line 215
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public sendMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)Z
    .locals 1
    .param p1, "requestMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_serviceBound:Z

    if-eqz v0, :cond_0

    .line 147
    invoke-direct {p0, p1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->sendMessageInternal(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    .line 159
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 149
    :cond_0
    new-instance v0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$1;-><init>(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;Lcom/samsung/android/service/travel/request/RequestMessage;)V

    invoke-direct {p0, v0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->bindService(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setTravelServiceCallback(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    .line 142
    return-void
.end method
