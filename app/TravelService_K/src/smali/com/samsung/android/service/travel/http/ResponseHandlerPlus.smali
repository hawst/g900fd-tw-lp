.class public abstract Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;
.super Ljava/lang/Object;
.source "ResponseHandlerPlus.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected httpGet:Lorg/apache/http/client/methods/HttpGet;

.field private responseCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    .local p0, "this":Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;, "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->responseCode:I

    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 39
    .local p0, "this":Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;, "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus<TT;>;"
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 41
    :cond_0
    return-void
.end method

.method protected setHttpRequest(Lorg/apache/http/client/methods/HttpGet;)V
    .locals 0
    .param p1, "httpGet"    # Lorg/apache/http/client/methods/HttpGet;

    .prologue
    .line 35
    .local p0, "this":Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;, "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus<TT;>;"
    iput-object p1, p0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 36
    return-void
.end method

.method protected setResponseCode(I)V
    .locals 0
    .param p1, "responseCode"    # I

    .prologue
    .line 31
    .local p0, "this":Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;, "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus<TT;>;"
    iput p1, p0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->responseCode:I

    .line 32
    return-void
.end method
