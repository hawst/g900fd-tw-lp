.class public abstract Lcom/samsung/android/service/travel/http/GetHttpClient;
.super Lcom/samsung/android/service/travel/http/AbstractHttpClient;
.source "GetHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/samsung/android/service/travel/http/AbstractHttpClient",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public static get(Ljava/lang/String;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    const/4 v0, 0x0

    .line 162
    invoke-static {p0, v0, v0, p1}, Lcom/samsung/android/service/travel/http/GetHttpClient;->get(Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lorg/apache/http/params/HttpParams;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lorg/apache/http/params/HttpParams;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Lorg/apache/http/message/HeaderGroup;
    .param p2, "hParam"    # Lorg/apache/http/params/HttpParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lorg/apache/http/message/HeaderGroup;",
            "Lorg/apache/http/params/HttpParams;",
            "Lorg/apache/http/client/ResponseHandler",
            "<+TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    .local p3, "handler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<+TT;>;"
    const/4 v2, 0x0

    .line 116
    .local v2, "client":Lorg/apache/http/client/HttpClient;
    if-nez p2, :cond_1

    .line 117
    invoke-static {}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getInstance()Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    .line 122
    :goto_0
    const/4 v5, 0x0

    .line 125
    .local v5, "request":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v6, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 126
    .end local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    .local v6, "request":Lorg/apache/http/client/methods/HttpGet;
    :try_start_1
    instance-of v7, p3, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    if-eqz v7, :cond_0

    .line 127
    move-object v0, p3

    check-cast v0, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    move-object v7, v0

    invoke-virtual {v7, v6}, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->setHttpRequest(Lorg/apache/http/client/methods/HttpGet;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 137
    :cond_0
    if-eqz p1, :cond_2

    .line 138
    invoke-virtual {p1}, Lorg/apache/http/message/HeaderGroup;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/http/client/methods/HttpGet;->setHeaders([Lorg/apache/http/Header;)V

    .line 142
    :goto_1
    new-instance v1, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v1}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 145
    .local v1, "bContext":Lorg/apache/http/protocol/HttpContext;
    :try_start_2
    invoke-interface {v2, v6, p3, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v7

    return-object v7

    .line 119
    .end local v1    # "bContext":Lorg/apache/http/protocol/HttpContext;
    .end local v6    # "request":Lorg/apache/http/client/methods/HttpGet;
    :cond_1
    invoke-static {}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getInstance()Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getHttpClient(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    goto :goto_0

    .line 129
    .restart local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    :catch_0
    move-exception v4

    .line 130
    .local v4, "e1":Ljava/lang/IllegalArgumentException;
    :goto_2
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 131
    new-instance v7, Ljava/io/IOException;

    const-string v8, "HttpClient IllegalArgumentException.."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 132
    .end local v4    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v4

    .line 134
    .local v4, "e1":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 135
    new-instance v7, Ljava/io/IOException;

    const-string v8, "HttpClient RuntimeException.."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 140
    .end local v4    # "e1":Ljava/lang/Exception;
    .end local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "request":Lorg/apache/http/client/methods/HttpGet;
    :cond_2
    invoke-static {}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getInstance()Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getDefaultHeaderGroup()Lorg/apache/http/message/HeaderGroup;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/http/message/HeaderGroup;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/http/client/methods/HttpGet;->setHeaders([Lorg/apache/http/Header;)V

    goto :goto_1

    .line 146
    .restart local v1    # "bContext":Lorg/apache/http/protocol/HttpContext;
    :catch_2
    move-exception v3

    .line 147
    .local v3, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v3}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 149
    new-instance v7, Ljava/io/IOException;

    const-string v8, "HttpClient RuntimeException.."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 132
    .end local v1    # "bContext":Lorg/apache/http/protocol/HttpContext;
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v4

    move-object v5, v6

    .end local v6    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_3

    .line 129
    .end local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "request":Lorg/apache/http/client/methods/HttpGet;
    :catch_4
    move-exception v4

    move-object v5, v6

    .end local v6    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v5    # "request":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_2
.end method

.method public static getContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Lcom/samsung/android/service/travel/http/DefaultHttpResponseHandler;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/http/DefaultHttpResponseHandler;-><init>()V

    invoke-static {p0, v0}, Lcom/samsung/android/service/travel/http/GetHttpClient;->get(Ljava/lang/String;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
