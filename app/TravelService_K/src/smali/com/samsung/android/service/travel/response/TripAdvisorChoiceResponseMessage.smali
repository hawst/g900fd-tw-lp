.class public Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;
.super Ljava/lang/Object;
.source "TripAdvisorChoiceResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public mAPIName:Ljava/lang/String;

.field public mErrCode:Ljava/lang/String;

.field public mErrContext:Ljava/lang/String;

.field public mEventTypeCode:Ljava/lang/String;

.field public mId:Ljava/lang/String;

.field public mPlaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/TripAdvisorChoice;",
            ">;"
        }
    .end annotation
.end field

.field public mStatus:Ljava/lang/String;

.field public mWidgetType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage$1;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage$1;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "lat"    # Ljava/lang/String;
    .param p2, "longi"    # Ljava/lang/String;
    .param p3, "cityId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 162
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://www.daodao.com/api/platform/1.0/zh_CN/travelers_choice/destinations?key=31a3dfe3-25bc-a28f-3825-47ed3afc6193&random_order=true&seed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    rem-long/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 175
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.tripadvisor.com/api/platform/1.0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/travelers_choice/destinations?key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&random_order=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&seed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    rem-long/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&ignore_locale=true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public merge(Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;)V
    .locals 2
    .param p1, "message"    # Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    check-cast p1, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .end local p1    # "message":Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    iget-object v1, p1, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 199
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 200
    const-string v0, "1"

    iput-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mStatus:Ljava/lang/String;

    .line 201
    :cond_0
    return-void
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    .locals 12
    .param p1, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 95
    :try_start_0
    new-instance v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    invoke-direct {v8}, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;-><init>()V

    .line 96
    .local v8, "message":Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;
    const-string v9, "getTravelWidget"

    iput-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mAPIName:Ljava/lang/String;

    .line 97
    const-string v9, "1"

    iput-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mStatus:Ljava/lang/String;

    .line 98
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mId:Ljava/lang/String;

    .line 99
    const-string v9, "S_TRAVELER"

    iput-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mWidgetType:Ljava/lang/String;

    .line 100
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 101
    .local v7, "mJsonObj":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 102
    .local v3, "jsonArray_data":Lorg/json/JSONArray;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    .line 104
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_4

    .line 105
    new-instance v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;-><init>()V

    .line 106
    .local v0, "data":Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 107
    const-string v9, "name"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mName:Ljava/lang/String;

    .line 108
    const-string v9, "location_id"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mRcmId:Ljava/lang/String;

    .line 109
    const-string v9, "distance"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mDistance:Ljava/lang/String;

    .line 110
    const-string v9, "web_url"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mWebUrl:Ljava/lang/String;

    .line 111
    const-string v9, "intent_url"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLaunchURL:Ljava/lang/String;

    .line 113
    const-string v9, "longitude"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLongitude:Ljava/lang/String;

    .line 114
    const-string v9, "latitude"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLatitude:Ljava/lang/String;

    .line 115
    const-string v9, "address_obj"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    const-string v10, "country"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLocationName:Ljava/lang/String;

    .line 117
    const-string v9, "photo"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_3

    const-string v9, "photo"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "photo"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "photo"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "null"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 120
    new-instance v4, Lorg/json/JSONObject;

    const-string v9, "photo"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 122
    .local v4, "json_image":Lorg/json/JSONObject;
    const-string v9, "images"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    .line 123
    new-instance v5, Lorg/json/JSONObject;

    const-string v9, "images"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 124
    .local v5, "json_image_child":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    const-string v9, "thumbnail"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 126
    .local v6, "json_image_url":Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mThumbnailImageURL:Ljava/lang/String;

    .line 127
    const-string v9, "original"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_0

    .line 128
    new-instance v6, Lorg/json/JSONObject;

    .end local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "original"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 129
    .restart local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mOriginalImageURL:Ljava/lang/String;

    .line 131
    :cond_0
    const-string v9, "small"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_1

    .line 132
    new-instance v6, Lorg/json/JSONObject;

    .end local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "small"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 133
    .restart local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mSmallImageURL:Ljava/lang/String;

    .line 135
    :cond_1
    const-string v9, "large"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    .line 136
    new-instance v6, Lorg/json/JSONObject;

    .end local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "large"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 137
    .restart local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mLargeImageURL:Ljava/lang/String;

    .line 139
    :cond_2
    const-string v9, "medium"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    .line 140
    new-instance v6, Lorg/json/JSONObject;

    .end local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "medium"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 141
    .restart local v6    # "json_image_url":Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoice;->mMediumImageURL:Ljava/lang/String;

    .line 146
    .end local v4    # "json_image":Lorg/json/JSONObject;
    .end local v5    # "json_image_child":Lorg/json/JSONObject;
    .end local v6    # "json_image_url":Lorg/json/JSONObject;
    :cond_3
    iget-object v9, v8, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 149
    .end local v0    # "data":Lcom/samsung/android/service/travel/response/TripAdvisorChoice;
    .end local v2    # "i":I
    .end local v3    # "jsonArray_data":Lorg/json/JSONArray;
    .end local v7    # "mJsonObj":Lorg/json/JSONObject;
    .end local v8    # "message":Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;
    :catch_0
    move-exception v1

    .line 150
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 153
    const/4 v8, 0x0

    .end local v1    # "e":Lorg/json/JSONException;
    :cond_4
    return-object v8
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TripAdvisorChoiceResponseMessage [mStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAPIName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mAPIName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mWidgetType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mWidgetType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEventTypeCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mEventTypeCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mErrCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mErrCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mErrContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mErrContext:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPlaces="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mAPIName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mWidgetType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mEventTypeCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mErrCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mErrContext:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 64
    return-void
.end method
