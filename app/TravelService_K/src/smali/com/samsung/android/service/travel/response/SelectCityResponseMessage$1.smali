.class final Lcom/samsung/android/service/travel/response/SelectCityResponseMessage$1;
.super Ljava/lang/Object;
.source "SelectCityResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
    .locals 3
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 69
    new-instance v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;-><init>()V

    .line 70
    .local v0, "resMessage":Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mStatus:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mId:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mAPIName:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mWidgetType:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mEventTypeCode:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mErrCode:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mErrContext:Ljava/lang/String;

    .line 79
    iget-object v1, v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mPlaces:Ljava/util/List;

    sget-object v2, Lcom/samsung/android/service/travel/response/SelectCity;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 80
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage$1;->newArray(I)[Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    move-result-object v0

    return-object v0
.end method
