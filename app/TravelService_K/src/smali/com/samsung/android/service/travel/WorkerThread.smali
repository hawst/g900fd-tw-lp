.class Lcom/samsung/android/service/travel/WorkerThread;
.super Ljava/lang/Thread;
.source "TravelService.java"


# instance fields
.field private httpRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field private httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mReqId:J

.field private mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

.field private mStop:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/service/travel/request/RequestMessage;J)V
    .locals 2
    .param p1, "requestMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;
    .param p2, "reqId"    # J

    .prologue
    .line 248
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 459
    new-instance v0, Lcom/samsung/android/service/travel/WorkerThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/service/travel/WorkerThread$1;-><init>(Lcom/samsung/android/service/travel/WorkerThread;)V

    iput-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    .line 249
    iput-object p1, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    .line 250
    iput-wide p2, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WorkerThread For :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/service/travel/WorkerThread;->setName(Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method private sendCancel()V
    .locals 6

    .prologue
    .line 429
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 430
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 431
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 432
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "messageId"

    const/4 v4, 0x4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 433
    const-string v3, "response"

    const-string v4, "Request canceled"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v3, "reqId"

    iget-object v4, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-wide v4, v4, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 435
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 436
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 438
    :catch_0
    move-exception v1

    .line 439
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendError(Ljava/lang/String;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 445
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 446
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 447
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 448
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "messageId"

    const/4 v4, -0x2

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 449
    const-string v3, "response"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v3, "reqId"

    iget-object v4, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-wide v4, v4, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 451
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 452
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v1

    .line 455
    .local v1, "e1":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private sendResponseMessage(Landroid/os/Parcelable;)V
    .locals 6
    .param p1, "parseObject"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 411
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 412
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 413
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 414
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "response"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 415
    const-string v3, "messageId"

    iget-object v4, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v4, v4, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 416
    const-string v3, "reqId"

    iget-object v4, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-wide v4, v4, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 417
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 419
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v3, v3, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 420
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 421
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public checkConnection(Lcom/samsung/android/service/travel/request/RequestMessage;)Z
    .locals 5
    .param p1, "requestMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    .line 275
    sget-object v3, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-static {v3}, Lcom/samsung/android/service/util/Utils;->isDeviceConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 277
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 278
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 279
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "messageId"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 280
    const-string v3, "response"

    const-string v4, "No connection to make request"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 283
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v3, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    if-eqz v3, :cond_0

    .line 284
    iget-object v3, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 291
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    return v3

    .line 286
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v1

    .line 287
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 291
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public run()V
    .locals 12

    .prologue
    .line 297
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Getting data for thread : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/samsung/android/service/travel/WorkerThread;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 299
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_1

    .line 300
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    if-nez v8, :cond_13

    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_13

    .line 306
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    const/16 v9, 0xb

    if-ne v8, v9, :cond_a

    .line 309
    :cond_2
    const/4 v1, 0x0

    .line 312
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    if-nez v8, :cond_3

    .line 313
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-static {v8}, Lcom/samsung/android/service/travel/cache/CacheManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/service/travel/cache/CacheManager;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v9, v9, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/service/travel/cache/CacheManager;->getBitmap(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 317
    :cond_3
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_4

    .line 318
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 322
    :cond_4
    if-nez v1, :cond_6

    :try_start_2
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-virtual {p0, v8}, Lcom/samsung/android/service/travel/WorkerThread;->checkConnection(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 323
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    invoke-static {v8, v9}, Lcom/samsung/android/service/travel/http/GetHttpClient;->get(Ljava/lang/String;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 325
    .restart local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "got drawable from network:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 328
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    if-nez v8, :cond_5

    .line 329
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-static {v8}, Lcom/samsung/android/service/travel/cache/CacheManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/service/travel/cache/CacheManager;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v9, v9, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    invoke-virtual {v8, v9, v1}, Lcom/samsung/android/service/travel/cache/CacheManager;->addToCache(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 336
    :cond_5
    :goto_1
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_7

    .line 337
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 333
    :cond_6
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "got drawable from cache:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 395
    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :catch_0
    move-exception v2

    .line 396
    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_14

    .line 397
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 404
    :goto_2
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 341
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_7
    if-nez v1, :cond_8

    .line 342
    :try_start_5
    const-string v8, ""

    invoke-direct {p0, v8}, Lcom/samsung/android/service/travel/WorkerThread;->sendError(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 346
    :cond_8
    :try_start_6
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    if-eqz v8, :cond_9

    .line 347
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 348
    .local v4, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v0, v8}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 349
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "response"

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 350
    const-string v8, "messageId"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    const-string v8, "reqId"

    iget-object v9, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-wide v10, v9, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {v0, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 352
    const-string v8, "imageKey"

    iget-object v9, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v9, v9, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v4, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 354
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->messanger:Landroid/os/Messenger;

    invoke-virtual {v8, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 404
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v4    # "msg":Landroid/os/Message;
    :cond_9
    :goto_3
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 357
    :cond_a
    :try_start_7
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-virtual {p0, v8}, Lcom/samsung/android/service/travel/WorkerThread;->checkConnection(Lcom/samsung/android/service/travel/request/RequestMessage;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 358
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    if-eqz v8, :cond_d

    .line 359
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->url:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/service/travel/http/GetHttpClient;->getContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 360
    .local v5, "response":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    invoke-interface {v8, v5}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    move-result-object v6

    check-cast v6, Landroid/os/Parcelable;

    .line 362
    .local v6, "responseParsedObject":Landroid/os/Parcelable;
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_b

    .line 363
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 366
    :cond_b
    :try_start_8
    invoke-direct {p0, v6}, Lcom/samsung/android/service/travel/WorkerThread;->sendResponseMessage(Landroid/os/Parcelable;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 404
    .end local v5    # "response":Ljava/lang/String;
    .end local v6    # "responseParsedObject":Landroid/os/Parcelable;
    :catchall_0
    move-exception v8

    sget-object v9, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v9, :cond_c

    .line 405
    sget-object v9, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v9, v9, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    throw v8

    .line 367
    :cond_d
    :try_start_9
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    if-eqz v8, :cond_9

    .line 368
    const/4 v6, 0x0

    .line 369
    .local v6, "responseParsedObject":Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_4
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    array-length v8, v8

    if-ge v3, v8, :cond_11

    .line 370
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-static {v8}, Lcom/samsung/android/service/travel/http/GetHttpClient;->getContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 371
    .restart local v5    # "response":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget-object v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    invoke-interface {v8, v5}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    move-result-object v7

    .line 373
    .local v7, "temp":Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    if-nez v6, :cond_f

    if-eqz v7, :cond_f

    .line 374
    move-object v6, v7

    .line 377
    :cond_e
    :goto_5
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_10

    .line 378
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 375
    :cond_f
    if-eqz v7, :cond_e

    .line 376
    :try_start_a
    invoke-interface {v6, v7}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->merge(Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;)V

    goto :goto_5

    .line 369
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 382
    .end local v5    # "response":Ljava/lang/String;
    .end local v7    # "temp":Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    :cond_11
    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    if-eqz v8, :cond_12

    .line 383
    invoke-direct {p0}, Lcom/samsung/android/service/travel/WorkerThread;->sendCancel()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 404
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    if-eqz v8, :cond_0

    .line 405
    sget-object v8, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    iget-object v8, v8, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v10, p0, Lcom/samsung/android/service/travel/WorkerThread;->mReqId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 386
    :cond_12
    :try_start_b
    check-cast v6, Landroid/os/Parcelable;

    .end local v6    # "responseParsedObject":Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    invoke-direct {p0, v6}, Lcom/samsung/android/service/travel/WorkerThread;->sendResponseMessage(Landroid/os/Parcelable;)V

    goto/16 :goto_3

    .line 391
    .end local v3    # "i":I
    :cond_13
    iget-object v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v8, v8, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    iget-boolean v8, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-nez v8, :cond_9

    goto/16 :goto_3

    .line 399
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_14
    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 400
    invoke-virtual {v2}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/samsung/android/service/travel/WorkerThread;->sendError(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_2
.end method

.method setStop()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 255
    iput-boolean v2, p0, Lcom/samsung/android/service/travel/WorkerThread;->mStop:Z

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stopping .. :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v0, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    if-ne v0, v2, :cond_1

    const-string v0, "POST"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    iget v0, v0, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    if-ne v0, v2, :cond_2

    .line 259
    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Aborting post request :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 271
    :cond_0
    :goto_1
    return-void

    .line 256
    :cond_1
    const-string v0, "GET"

    goto :goto_0

    .line 264
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stopping .."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    if-eqz v0, :cond_0

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Aborting get request :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/service/travel/WorkerThread;->mRequestMessage:Lcom/samsung/android/service/travel/request/RequestMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/service/travel/WorkerThread;->httpResponseHandler:Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;

    invoke-virtual {v0}, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;->abort()V

    goto :goto_1
.end method
