.class public Lcom/samsung/android/service/travel/TravelService;
.super Landroid/app/Service;
.source "TravelService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/service/travel/TravelService$IncomingHandler;
    }
.end annotation


# static fields
.field public static _instance:Lcom/samsung/android/service/travel/TravelService;


# instance fields
.field mClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field final mHandler:Landroid/os/Handler;

.field final mMessenger:Landroid/os/Messenger;

.field mProcessingThread:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/android/service/travel/WorkerThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/TravelService;->mClients:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/samsung/android/service/travel/TravelService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/service/travel/TravelService$IncomingHandler;-><init>(Lcom/samsung/android/service/travel/TravelService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/service/travel/TravelService;->mMessenger:Landroid/os/Messenger;

    .line 66
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/TravelService;->mHandler:Landroid/os/Handler;

    .line 68
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    .line 75
    sput-object p0, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    .line 76
    return-void
.end method

.method public static IsChinaModel()Z
    .locals 3

    .prologue
    .line 123
    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CHINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    const/4 v1, 0x1

    .line 128
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized cancelRequest(J)V
    .locals 3
    .param p1, "requestId"    # J

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/service/travel/WorkerThread;

    .line 177
    .local v0, "thread":Lcom/samsung/android/service/travel/WorkerThread;
    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {v0}, Lcom/samsung/android/service/travel/WorkerThread;->setStop()V

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancel request stopping thread :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/service/travel/WorkerThread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0}, Lcom/samsung/android/service/travel/WorkerThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {v0}, Lcom/samsung/android/service/travel/WorkerThread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_0
    monitor-exit p0

    return-void

    .line 176
    .end local v0    # "thread":Lcom/samsung/android/service/travel/WorkerThread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public makeRequestMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)V
    .locals 4
    .param p1, "reqMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 191
    iget v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    :pswitch_0
    return-void

    .line 193
    :pswitch_1
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 194
    new-instance v0, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 195
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    invoke-interface {v0, v2, v2, v2}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 199
    :pswitch_2
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 200
    new-instance v0, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 201
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    iget-object v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->cityId:Ljava/lang/String;

    invoke-interface {v0, v2, v2, v1}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 206
    :pswitch_3
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 207
    new-instance v0, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 208
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    iget-object v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->latitude:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->longitude:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->searchStr:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 213
    :pswitch_4
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 214
    new-instance v0, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 215
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    iget-object v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->cityId:Ljava/lang/String;

    invoke-interface {v0, v2, v2, v1}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 219
    :pswitch_5
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 220
    new-instance v0, Lcom/samsung/android/service/travel/response/SearchResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/SearchResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 221
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    iget-object v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->searchStr:Ljava/lang/String;

    invoke-interface {v0, v2, v2, v1}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 225
    :pswitch_6
    iput v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mHttpType:I

    .line 226
    new-instance v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;-><init>()V

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .line 227
    iget-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->mResponseMessageFactory:Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    iget-object v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->searchStr:Ljava/lang/String;

    invoke-interface {v0, v2, v2, v1}, Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/service/travel/TravelService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 117
    invoke-static {}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->getInstance()Lcom/samsung/android/service/travel/http/HttpClientPoolManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/service/travel/http/HttpClientPoolManager;->initialize()V

    .line 118
    invoke-static {p0}, Lcom/samsung/android/service/travel/cache/CacheManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/service/travel/cache/CacheManager;

    .line 119
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/service/travel/TravelService$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/service/travel/TravelService$1;-><init>(Lcom/samsung/android/service/travel/TravelService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 148
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 149
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 153
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public declared-synchronized putRequest(Lcom/samsung/android/service/travel/request/RequestMessage;)J
    .locals 4
    .param p1, "reqMessage"    # Lcom/samsung/android/service/travel/request/RequestMessage;

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget v1, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->messageId:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 163
    iget-wide v2, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/service/travel/TravelService;->cancelRequest(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    const-wide/16 v2, -0x1

    .line 171
    :goto_0
    monitor-exit p0

    return-wide v2

    .line 167
    :cond_0
    :try_start_1
    sget-object v1, Lcom/samsung/android/service/travel/TravelService;->_instance:Lcom/samsung/android/service/travel/TravelService;

    invoke-virtual {v1, p1}, Lcom/samsung/android/service/travel/TravelService;->makeRequestMessage(Lcom/samsung/android/service/travel/request/RequestMessage;)V

    .line 168
    new-instance v0, Lcom/samsung/android/service/travel/WorkerThread;

    iget-wide v2, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-direct {v0, p1, v2, v3}, Lcom/samsung/android/service/travel/WorkerThread;-><init>(Lcom/samsung/android/service/travel/request/RequestMessage;J)V

    .line 169
    .local v0, "workerThread":Lcom/samsung/android/service/travel/WorkerThread;
    iget-object v1, p0, Lcom/samsung/android/service/travel/TravelService;->mProcessingThread:Ljava/util/Hashtable;

    iget-wide v2, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-virtual {v0}, Lcom/samsung/android/service/travel/WorkerThread;->start()V

    .line 171
    iget-wide v2, p1, Lcom/samsung/android/service/travel/request/RequestMessage;->reqId:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 162
    .end local v0    # "workerThread":Lcom/samsung/android/service/travel/WorkerThread;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
