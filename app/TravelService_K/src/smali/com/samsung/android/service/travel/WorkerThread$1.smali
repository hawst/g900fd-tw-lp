.class Lcom/samsung/android/service/travel/WorkerThread$1;
.super Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;
.source "TravelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/WorkerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/service/travel/http/ResponseHandlerPlus",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/service/travel/WorkerThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/service/travel/WorkerThread;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/samsung/android/service/travel/WorkerThread$1;->this$0:Lcom/samsung/android/service/travel/WorkerThread;

    invoke-direct {p0}, Lcom/samsung/android/service/travel/http/ResponseHandlerPlus;-><init>()V

    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 5
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 462
    const/4 v0, 0x0

    .line 464
    .local v0, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    if-nez p1, :cond_1

    .line 465
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/android/service/travel/WorkerThread$1;->setResponseCode(I)V

    .line 478
    :cond_0
    :goto_0
    return-object v3

    .line 469
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 470
    .local v2, "rentity":Lorg/apache/http/HttpEntity;
    const/4 v1, 0x0

    .line 471
    .local v1, "inputStream":Ljava/io/InputStream;
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 473
    if-eqz v1, :cond_0

    .line 476
    const-string v3, "src"

    invoke-static {v1, v3}, Landroid/graphics/drawable/BitmapDrawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .end local v0    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .restart local v0    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    move-object v3, v0

    .line 478
    goto :goto_0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    invoke-virtual {p0, p1}, Lcom/samsung/android/service/travel/WorkerThread$1;->handleResponse(Lorg/apache/http/HttpResponse;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method
