.class public Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;
.super Ljava/lang/Object;
.source "LocationReviewResponseMessage.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0xeb048308d8379d1L


# instance fields
.field mReviewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/service/travel/response/LocationReview;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage$1;

    invoke-direct {v0}, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage$1;-><init>()V

    sput-object v0, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;->mReviewList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "lat"    # Ljava/lang/String;
    .param p2, "longi"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 61
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://www.daodao.com/api/platform/1.0/zh_CN/location/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/reviews?key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 67
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.tripadvisor.com/api/platform/1.0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/location/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/reviews?key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "31a3dfe3-25bc-a28f-3825-47ed3afc6193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method public merge(Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;)V
    .locals 0
    .param p1, "message"    # Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;

    .prologue
    .line 102
    return-void
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/android/service/travel/resfactory/ResponseMessageFactory;
    .locals 7
    .param p1, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 37
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 38
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "data"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 39
    .local v2, "jsonArray":Lorg/json/JSONArray;
    new-instance v5, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;

    invoke-direct {v5}, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;-><init>()V

    .line 40
    .local v5, "reviewMessage":Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 41
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 42
    new-instance v4, Lcom/samsung/android/service/travel/response/LocationReview;

    invoke-direct {v4}, Lcom/samsung/android/service/travel/response/LocationReview;-><init>()V

    .line 43
    .local v4, "locationReview":Lcom/samsung/android/service/travel/response/LocationReview;
    const-string v6, "lang"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->language:Ljava/lang/String;

    .line 44
    const-string v6, "rating"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->rating:Ljava/lang/String;

    .line 45
    const-string v6, "id"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->reviewId:Ljava/lang/String;

    .line 46
    const-string v6, "url"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->reviewUrl:Ljava/lang/String;

    .line 47
    const-string v6, "text"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->text:Ljava/lang/String;

    .line 48
    const-string v6, "title"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/android/service/travel/response/LocationReview;->title:Ljava/lang/String;

    .line 49
    iget-object v6, v5, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;->mReviewList:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "locationReview":Lcom/samsung/android/service/travel/response/LocationReview;
    .end local v5    # "reviewMessage":Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 55
    const/4 v5, 0x0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v5
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;->mReviewList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 79
    return-void
.end method
