.class Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;
.super Landroid/os/Handler;
.source "TravelServiceConnectionManager.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 53
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$000(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Landroid/content/Context;

    move-result-object v12

    if-nez v12, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$000(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 56
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "messageId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 57
    .local v2, "messageId":I
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 59
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 85
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "response":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 87
    .local v4, "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    const/4 v13, -0x1

    invoke-virtual {v12, v4, v5, v13, v3}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onError(JILjava/lang/Object;)V

    goto :goto_0

    .line 62
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "reqId":J
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 63
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 64
    .restart local v4    # "reqId":J
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "imageKey"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "imageKey":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onBitmapResponse(JLandroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "imageKey":Ljava/lang/String;
    .end local v4    # "reqId":J
    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 70
    .restart local v3    # "response":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 71
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    const/4 v13, -0x1

    invoke-virtual {v12, v4, v5, v13, v3}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onError(JILjava/lang/Object;)V

    goto/16 :goto_0

    .line 78
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "reqId":J
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .line 79
    .local v10, "tripAdvisorChoice":Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 80
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v10}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onTripAdvisorChoiceResponse(JLcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V

    goto/16 :goto_0

    .line 91
    .end local v4    # "reqId":J
    .end local v10    # "tripAdvisorChoice":Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;
    :pswitch_5
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 92
    .restart local v3    # "response":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 93
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onCancel(J)V

    goto/16 :goto_0

    .line 99
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "reqId":J
    :pswitch_6
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;

    .line 100
    .local v6, "responsecityList":Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 101
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v6}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onCityList(JLcom/samsung/android/service/travel/response/TravelCityListResponseMessage;)V

    goto/16 :goto_0

    .line 106
    .end local v4    # "reqId":J
    .end local v6    # "responsecityList":Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;
    :pswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    .line 107
    .local v9, "selectCityResponse":Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 108
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v9}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onSelectCity(JLcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V

    goto/16 :goto_0

    .line 112
    .end local v4    # "reqId":J
    .end local v9    # "selectCityResponse":Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;
    :pswitch_8
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;

    .line 113
    .local v7, "reviewMessage":Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 114
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v7}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onLocationReview(JLcom/samsung/android/service/travel/response/LocationReviewResponseMessage;)V

    goto/16 :goto_0

    .line 117
    .end local v4    # "reqId":J
    .end local v7    # "reviewMessage":Lcom/samsung/android/service/travel/response/LocationReviewResponseMessage;
    :pswitch_9
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/SearchResponseMessage;

    .line 118
    .local v8, "searchResponse":Lcom/samsung/android/service/travel/response/SearchResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 119
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v8}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onSearchResult(JLcom/samsung/android/service/travel/response/SearchResponseMessage;)V

    goto/16 :goto_0

    .line 123
    .end local v4    # "reqId":J
    .end local v8    # "searchResponse":Lcom/samsung/android/service/travel/response/SearchResponseMessage;
    :pswitch_a
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "response"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .line 124
    .local v11, "wallpaperResponse":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "reqId"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 125
    .restart local v4    # "reqId":J
    iget-object v12, p0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$IncomingHandler;->this$0:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    # getter for: Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->_callback:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
    invoke-static {v12}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->access$100(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;

    move-result-object v12

    invoke-virtual {v12, v4, v5, v11}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onWallpaperResult(JLcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    goto/16 :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_2
    .end packed-switch
.end method
