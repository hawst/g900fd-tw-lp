.class Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;
.super Ljava/lang/Thread;
.source "TravelManagerResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/travel/bindService/TravelManagerResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WriteImage"
.end annotation


# instance fields
.field bitmap:Landroid/graphics/Bitmap;

.field reqId:J

.field final synthetic this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;


# direct methods
.method public constructor <init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Landroid/graphics/Bitmap;J)V
    .locals 1
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "reqId"    # J

    .prologue
    .line 270
    iput-object p1, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .line 271
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->bitmap:Landroid/graphics/Bitmap;

    .line 272
    iput-object p2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->bitmap:Landroid/graphics/Bitmap;

    .line 273
    iput-wide p3, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->reqId:J

    .line 274
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 277
    sget-object v8, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v8}, Lcom/samsung/android/travel/bindService/TravelBean;->getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v5

    .line 279
    .local v5, "myTripChoice":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    const/4 v1, 0x0

    .line 280
    .local v1, "imageOutputStream":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 281
    .local v6, "newImageFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 284
    .local v4, "locationName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 286
    .local v3, "imageWithLogo":Landroid/graphics/Bitmap;
    if-eqz v5, :cond_0

    .line 287
    :try_start_0
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->bitmap:Landroid/graphics/Bitmap;

    iget-object v8, v5, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v11, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->shortname:Ljava/lang/String;

    iget-object v8, v5, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v8, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->id:Ljava/lang/String;

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$200(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v10, v11, v8, v12}, Lcom/samsung/android/service/util/Utils;->overlay(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 296
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$200(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    if-eqz v3, :cond_2

    .line 297
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$300()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/images/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v8, v5, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v10}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v8, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->id:Ljava/lang/String;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$200(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    .line 300
    new-instance v7, Ljava/io/File;

    sget-object v8, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 301
    .end local v6    # "newImageFile":Ljava/io/File;
    .local v7, "newImageFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v7}, Ljava/io/File;->createNewFile()Z

    .line 303
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 304
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Ljava/io/File;->setReadable(ZZ)Z

    .line 305
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Ljava/io/File;->setWritable(ZZ)Z

    .line 307
    iget-object v8, v5, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v9}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v4, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->shortname:Ljava/lang/String;

    .line 308
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # operator++ for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$108(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    .line 309
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 311
    .end local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    .local v2, "imageOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$200(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "png"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 312
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-virtual {v3, v8, v9, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 333
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z
    invoke-static {v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$400(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 334
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/travel_wall.jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 335
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v9, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v9}, Lcom/samsung/android/travel/bindService/TravelBean;->getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteImages(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    .line 336
    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    const/4 v9, 0x0

    # setter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z
    invoke-static {v8, v9}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$402(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Z)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_e
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    :cond_1
    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    move-object v1, v2

    .line 358
    .end local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    :cond_2
    if-eqz v1, :cond_3

    .line 360
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 367
    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    .line 369
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 376
    :cond_4
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v9

    .line 377
    :try_start_5
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z
    invoke-static {v8, v10}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz v4, :cond_5

    .line 379
    const-string v8, "Travel Service : Lockscreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cycle  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Image : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/20"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-static {v8, v10, v11, v12}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :cond_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 386
    :goto_3
    return-void

    .line 315
    .end local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :cond_6
    :try_start_6
    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x64

    invoke-virtual {v3, v8, v9, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_e
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    goto/16 :goto_0

    .line 341
    :catch_0
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    move-object v1, v2

    .line 342
    .end local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    :goto_4
    if-eqz v6, :cond_7

    :try_start_7
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 343
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 345
    :cond_7
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 358
    if-eqz v1, :cond_8

    .line 360
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 367
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_8
    :goto_5
    if-eqz v1, :cond_9

    .line 369
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 376
    :cond_9
    :goto_6
    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v9

    .line 377
    :try_start_a
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z
    invoke-static {v8, v10}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    if-eqz v4, :cond_a

    .line 379
    const-string v8, "Travel Service : Lockscreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cycle  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Image : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/20"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-static {v8, v10, v11, v12}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :cond_a
    monitor-exit v9

    goto/16 :goto_3

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    throw v8

    .line 361
    :catch_1
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 363
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 364
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 370
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 371
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 372
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 373
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_2

    .line 384
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    :try_start_b
    monitor-exit v9
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v8

    .line 361
    .local v0, "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 363
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 364
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_5

    .line 370
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 371
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 372
    if-eqz v6, :cond_9

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 373
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_6

    .line 346
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 347
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_7
    if-eqz v6, :cond_b

    :try_start_c
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 348
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 350
    :cond_b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 358
    if-eqz v1, :cond_c

    .line 360
    :try_start_d
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    .line 367
    :cond_c
    :goto_8
    if-eqz v1, :cond_d

    .line 369
    :try_start_e
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    .line 376
    :cond_d
    :goto_9
    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v9

    .line 377
    :try_start_f
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_e

    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z
    invoke-static {v8, v10}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    if-eqz v4, :cond_e

    .line 379
    const-string v8, "Travel Service : Lockscreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cycle  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Image : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/20"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-static {v8, v10, v11, v12}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :cond_e
    monitor-exit v9

    goto/16 :goto_3

    :catchall_2
    move-exception v8

    monitor-exit v9
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v8

    .line 361
    :catch_6
    move-exception v0

    .line 362
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 363
    if-eqz v6, :cond_c

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 364
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_8

    .line 370
    :catch_7
    move-exception v0

    .line 371
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 372
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 373
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_9

    .line 351
    .end local v0    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    :goto_a
    if-eqz v6, :cond_f

    :try_start_10
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 353
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 355
    :cond_f
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 358
    if-eqz v1, :cond_10

    .line 360
    :try_start_11
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    .line 367
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_10
    :goto_b
    if-eqz v1, :cond_11

    .line 369
    :try_start_12
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a

    .line 376
    :cond_11
    :goto_c
    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v9

    .line 377
    :try_start_13
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_12

    iget-object v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z
    invoke-static {v8, v10}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_12

    if-eqz v4, :cond_12

    .line 379
    const-string v8, "Travel Service : Lockscreen"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cycle  : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Image : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/20"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    iget-object v11, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-static {v8, v10, v11, v12}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :cond_12
    monitor-exit v9

    goto/16 :goto_3

    :catchall_3
    move-exception v8

    monitor-exit v9
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    throw v8

    .line 361
    .restart local v0    # "e":Ljava/lang/OutOfMemoryError;
    :catch_9
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 363
    if-eqz v6, :cond_10

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_10

    .line 364
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_b

    .line 370
    .end local v0    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v0

    .line 371
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 372
    if-eqz v6, :cond_11

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_11

    .line 373
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_c

    .line 358
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_4
    move-exception v8

    :goto_d
    if-eqz v1, :cond_13

    .line 360
    :try_start_14
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b

    .line 367
    :cond_13
    :goto_e
    if-eqz v1, :cond_14

    .line 369
    :try_start_15
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_c

    .line 376
    :cond_14
    :goto_f
    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v9

    .line 377
    :try_start_16
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_15

    iget-object v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    sget-object v11, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z
    invoke-static {v10, v11}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_15

    if-eqz v4, :cond_15

    .line 379
    const-string v10, "Travel Service : Lockscreen"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Cycle  : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Image : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/20"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    iget-object v12, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I
    invoke-static {v12}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v12

    iget-object v13, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->this$0:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    invoke-static {v13}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I

    move-result v13

    invoke-static {v10, v11, v12, v13}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 382
    # getter for: Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->access$000()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 384
    :cond_15
    monitor-exit v9
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    throw v8

    .line 361
    :catch_b
    move-exception v0

    .line 362
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 363
    if-eqz v6, :cond_13

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_13

    .line 364
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_e

    .line 370
    .end local v0    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v0

    .line 371
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 372
    if-eqz v6, :cond_14

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_14

    .line 373
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto/16 :goto_f

    .line 384
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_5
    move-exception v8

    :try_start_17
    monitor-exit v9
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_5

    throw v8

    .line 358
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catchall_6
    move-exception v8

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    goto/16 :goto_d

    .end local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catchall_7
    move-exception v8

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    move-object v1, v2

    .end local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_d

    .line 351
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catch_d
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    goto/16 :goto_a

    .end local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catch_e
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    move-object v1, v2

    .end local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_a

    .line 346
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catch_f
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    goto/16 :goto_7

    .end local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catch_10
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    move-object v1, v2

    .end local v2    # "imageOutputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "imageOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_7

    .line 341
    :catch_11
    move-exception v0

    goto/16 :goto_4

    .end local v6    # "newImageFile":Ljava/io/File;
    .restart local v7    # "newImageFile":Ljava/io/File;
    :catch_12
    move-exception v0

    move-object v6, v7

    .end local v7    # "newImageFile":Ljava/io/File;
    .restart local v6    # "newImageFile":Ljava/io/File;
    goto/16 :goto_4
.end method
