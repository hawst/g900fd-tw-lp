.class public Lcom/samsung/android/travel/bindService/TravelServiceConnection;
.super Ljava/lang/Object;
.source "TravelServiceConnection.java"


# static fields
.field private static travelServiceConnection:Lcom/samsung/android/travel/bindService/TravelServiceConnection;


# instance fields
.field connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->travelServiceConnection:Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    invoke-direct {v0}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;-><init>()V

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->travelServiceConnection:Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    .line 35
    :cond_0
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->travelServiceConnection:Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    return-object v0
.end method


# virtual methods
.method public cancelAlarmService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 184
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 185
    .local v0, "alarmService":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.travel.slidingaction"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v1, "broadcastIntent":Landroid/content/Intent;
    invoke-static {p1, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 188
    .local v2, "slideIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 191
    return-void
.end method

.method public getConnectionManager(Landroid/content/Context;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    .line 196
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    invoke-static {p1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->setTravelServiceCallback(Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    return-object v0
.end method

.method public getTravelConnection(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelServiceConnection;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-static {p1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getConnectionManager(Landroid/content/Context;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->getWallpaper()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->setRequestId(J)V

    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method public startAlarmService(Landroid/content/Context;JZ)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "elapsedTime"    # J
    .param p4, "start"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 128
    const/4 v10, 0x0

    .line 130
    .local v10, "time_interval":I
    invoke-static {p1}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 134
    .local v0, "alarmService":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-string v2, "com.samsung.android.travel.slidingaction"

    invoke-direct {v7, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v7, "broadcastIntent":Landroid/content/Intent;
    invoke-static {p1, v4, v7, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 136
    .local v6, "slideIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sliding interval"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 141
    .local v9, "sliding_interval":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "interval":[Ljava/lang/String;
    if-nez v9, :cond_0

    .line 146
    aget-object v9, v8, v4

    .line 152
    :cond_0
    aget-object v2, v8, v4

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    const v10, 0x1499700

    .line 161
    :goto_0
    const-string v2, "TravelServiceConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "slide_intent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    if-eqz p4, :cond_5

    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p2

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 176
    .end local v0    # "alarmService":Landroid/app/AlarmManager;
    .end local v6    # "slideIntent":Landroid/app/PendingIntent;
    .end local v7    # "broadcastIntent":Landroid/content/Intent;
    .end local v8    # "interval":[Ljava/lang/String;
    .end local v9    # "sliding_interval":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 154
    .restart local v0    # "alarmService":Landroid/app/AlarmManager;
    .restart local v6    # "slideIntent":Landroid/app/PendingIntent;
    .restart local v7    # "broadcastIntent":Landroid/content/Intent;
    .restart local v8    # "interval":[Ljava/lang/String;
    .restart local v9    # "sliding_interval":Ljava/lang/String;
    :cond_2
    aget-object v2, v8, v1

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 155
    const v10, 0x2932e00

    goto :goto_0

    .line 156
    :cond_3
    const/4 v2, 0x2

    aget-object v2, v8, v2

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 157
    const v10, 0x5265c00

    goto :goto_0

    .line 159
    :cond_4
    const v10, 0x1499700

    goto :goto_0

    .line 166
    :cond_5
    int-to-long v2, v10

    sub-long/2addr v2, p2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    .line 167
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v10

    sub-long/2addr v4, p2

    add-long/2addr v2, v4

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_1

    .line 170
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public startAlarmService(Landroid/content/Context;Z)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "start"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 81
    const/4 v10, 0x0

    .line 83
    .local v10, "time_interval":I
    invoke-static {p1}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 85
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 87
    .local v0, "alarmService":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-string v2, "com.samsung.android.travel.slidingaction"

    invoke-direct {v7, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    .local v7, "broadcastIntent":Landroid/content/Intent;
    invoke-static {p1, v4, v7, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 90
    .local v6, "slideIntent":Landroid/app/PendingIntent;
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sliding interval"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 95
    .local v9, "sliding_interval":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 99
    .local v8, "interval":[Ljava/lang/String;
    if-nez v9, :cond_0

    .line 100
    aget-object v9, v8, v4

    .line 106
    :cond_0
    aget-object v2, v8, v4

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    const v10, 0x1499700

    .line 116
    :goto_0
    const-string v2, "TravelServiceConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "slide_intent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    if-eqz p2, :cond_5

    .line 118
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 124
    .end local v0    # "alarmService":Landroid/app/AlarmManager;
    .end local v6    # "slideIntent":Landroid/app/PendingIntent;
    .end local v7    # "broadcastIntent":Landroid/content/Intent;
    .end local v8    # "interval":[Ljava/lang/String;
    .end local v9    # "sliding_interval":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 108
    .restart local v0    # "alarmService":Landroid/app/AlarmManager;
    .restart local v6    # "slideIntent":Landroid/app/PendingIntent;
    .restart local v7    # "broadcastIntent":Landroid/content/Intent;
    .restart local v8    # "interval":[Ljava/lang/String;
    .restart local v9    # "sliding_interval":Ljava/lang/String;
    :cond_2
    aget-object v2, v8, v1

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    const v10, 0x2932e00

    goto :goto_0

    .line 110
    :cond_3
    const/4 v2, 0x2

    aget-object v2, v8, v2

    invoke-virtual {v9, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 111
    const v10, 0x5265c00

    goto :goto_0

    .line 113
    :cond_4
    const v10, 0x1499700

    goto :goto_0

    .line 121
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    int-to-long v4, v10

    add-long/2addr v2, v4

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method public unBindService()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    invoke-virtual {v0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->release()V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->connectionManager:Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    .line 206
    :cond_0
    return-void
.end method
