.class Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;
.super Ljava/lang/Thread;
.source "PhotoSliding.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/travel/PhotoSliding;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateDefaultImage"
.end annotation


# instance fields
.field imageFile:Ljava/io/File;

.field inputStream:Ljava/io/InputStream;

.field final synthetic this$0:Lcom/samsung/android/travel/PhotoSliding;


# direct methods
.method public constructor <init>(Lcom/samsung/android/travel/PhotoSliding;Ljava/io/File;Ljava/io/InputStream;)V
    .locals 1
    .param p2, "imageFile"    # Ljava/io/File;
    .param p3, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 394
    iput-object p1, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    .line 395
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    .line 396
    iput-object p2, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    .line 397
    iput-object p3, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    .line 398
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 401
    const/4 v3, 0x0

    .line 403
    .local v3, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 404
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 405
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    .line 406
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setWritable(ZZ)Z

    .line 407
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->imageFile:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    const/16 v5, 0x400

    :try_start_1
    new-array v0, v5, [B

    .line 411
    .local v0, "data":[B
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "n":I
    const/4 v5, -0x1

    if-eq v2, v5, :cond_3

    .line 412
    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 414
    .end local v0    # "data":[B
    .end local v2    # "n":I
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 416
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 421
    if-eqz v3, :cond_0

    .line 423
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 429
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_2
    if-eqz v3, :cond_1

    .line 431
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 437
    :cond_1
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 439
    :try_start_5
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 446
    :cond_2
    :goto_4
    return-void

    .line 421
    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "data":[B
    .restart local v2    # "n":I
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :cond_3
    if-eqz v4, :cond_4

    .line 423
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 429
    :cond_4
    :goto_5
    if-eqz v4, :cond_5

    .line 431
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 437
    :cond_5
    :goto_6
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    if-eqz v5, :cond_b

    .line 439
    :try_start_8
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    move-object v3, v4

    .line 443
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 424
    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 426
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 432
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 434
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 440
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 442
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 443
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 424
    .end local v0    # "data":[B
    .end local v2    # "n":I
    .local v1, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v1

    .line 426
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 432
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 434
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 440
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 442
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 417
    .end local v1    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 419
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_7
    :try_start_9
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 421
    if-eqz v3, :cond_6

    .line 423
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 429
    :cond_6
    :goto_8
    if-eqz v3, :cond_7

    .line 431
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    .line 437
    :cond_7
    :goto_9
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 439
    :try_start_c
    iget-object v5, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_4

    .line 440
    :catch_8
    move-exception v1

    .line 442
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 424
    :catch_9
    move-exception v1

    .line 426
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 432
    :catch_a
    move-exception v1

    .line 434
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 421
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_a
    if-eqz v3, :cond_8

    .line 423
    :try_start_d
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    .line 429
    :cond_8
    :goto_b
    if-eqz v3, :cond_9

    .line 431
    :try_start_e
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    .line 437
    :cond_9
    :goto_c
    iget-object v6, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    if-eqz v6, :cond_a

    .line 439
    :try_start_f
    iget-object v6, p0, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_d

    .line 443
    :cond_a
    :goto_d
    throw v5

    .line 424
    :catch_b
    move-exception v1

    .line 426
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 432
    .end local v1    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v1

    .line 434
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 440
    .end local v1    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v1

    .line 442
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 421
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_a

    .line 417
    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v1

    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_7

    .line 414
    :catch_f
    move-exception v1

    goto/16 :goto_1

    .end local v3    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "data":[B
    .restart local v2    # "n":I
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :cond_b
    move-object v3, v4

    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v3    # "outputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method
