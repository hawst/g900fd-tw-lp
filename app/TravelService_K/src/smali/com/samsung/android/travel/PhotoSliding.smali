.class public Lcom/samsung/android/travel/PhotoSliding;
.super Landroid/app/Activity;
.source "PhotoSliding.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;
    }
.end annotation


# instance fields
.field actionBar:Landroid/app/ActionBar;

.field private builder:Landroid/app/AlertDialog$Builder;

.field private dialog:Landroid/app/AlertDialog;

.field private editor:Landroid/content/SharedPreferences$Editor;

.field private isSelected:Z

.field mBack:Landroid/widget/RelativeLayout;

.field mCheckBox:Landroid/widget/CheckBox;

.field mCheckBoxView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field mInterval:Landroid/widget/LinearLayout;

.field private mRes:Landroid/content/res/Resources;

.field private mSetInterval:Landroid/widget/TextView;

.field private mSetWallpaper:Landroid/widget/TextView;

.field private show_dialog:Z

.field private warningMessage:Landroid/widget/TextView;

.field private warningPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/travel/PhotoSliding;->isSelected:Z

    .line 389
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/travel/PhotoSliding;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/travel/PhotoSliding;->showChoiceDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/travel/PhotoSliding;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/travel/PhotoSliding;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/travel/PhotoSliding;->setTravelChoiceImage()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/travel/PhotoSliding;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/travel/PhotoSliding;->isSelected:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/travel/PhotoSliding;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/travel/PhotoSliding;->isSelected:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/PhotoSliding;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->editor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method private setTravelChoiceImage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 366
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/travel_wall.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v5}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 368
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper_transparent"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 376
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .line 379
    :cond_0
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setSelectCity(Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V

    .line 380
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setTripChoice(Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V

    .line 381
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setmWallpaper(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    .line 382
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->writeDatatoFile(Landroid/content/Context;)V

    .line 385
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/samsung/android/service/util/Utils;->setUpdatedTimeStamp(Landroid/content/Context;J)V

    .line 386
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;Z)V

    .line 387
    return-void
.end method

.method private setUpActionBar()V
    .locals 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->actionBar:Landroid/app/ActionBar;

    .line 340
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->actionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 341
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 342
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->actionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 343
    return-void
.end method

.method private showChoiceDialog()V
    .locals 8

    .prologue
    const/high16 v7, 0x7f090000

    const/4 v6, 0x1

    .line 282
    iget-object v4, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "sliding interval"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 284
    .local v3, "sliding_interval":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "interval":[Ljava/lang/String;
    if-nez v3, :cond_1

    .line 286
    const-string v3, "0"

    .line 306
    :cond_0
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 307
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f08000d

    invoke-virtual {p0, v4}, Lcom/samsung/android/travel/PhotoSliding;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 308
    const v4, 0x7f08000c

    new-instance v5, Lcom/samsung/android/travel/PhotoSliding$10;

    invoke-direct {v5, p0}, Lcom/samsung/android/travel/PhotoSliding$10;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 318
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Lcom/samsung/android/travel/PhotoSliding$11;

    invoke-direct {v5, p0, v2}, Lcom/samsung/android/travel/PhotoSliding$11;-><init>(Lcom/samsung/android/travel/PhotoSliding;[Ljava/lang/String;)V

    invoke-virtual {v1, v7, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 331
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 334
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 336
    return-void

    .line 287
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 288
    const-string v3, "0"

    goto :goto_0

    .line 289
    :cond_2
    aget-object v4, v2, v6

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 290
    const-string v3, "1"

    goto :goto_0

    .line 291
    :cond_3
    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 292
    const-string v3, "2"

    goto :goto_0

    .line 294
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v6, :cond_0

    .line 295
    const-string v4, "6"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 296
    const-string v3, "0"

    goto :goto_0

    .line 297
    :cond_5
    const-string v4, "12"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 298
    const-string v3, "1"

    goto :goto_0

    .line 299
    :cond_6
    const-string v4, "24"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 300
    const-string v3, "2"

    goto :goto_0

    .line 302
    :cond_7
    const-string v3, "0"

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v2, 0x400

    const/16 v3, 0x400

    invoke-virtual {v0, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 91
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/PhotoSliding;->setContentView(I)V

    .line 93
    iput-object p0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    .line 96
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 97
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "isWallpaperChangeAllowed"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 99
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 102
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 103
    const-string v0, "isWallpaperChangeAllowed"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "false"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 278
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mRes:Landroid/content/res/Resources;

    .line 117
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getFilesDir()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 118
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v9

    .line 123
    .local v9, "path":Ljava/lang/String;
    :goto_1
    new-instance v7, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/travel_wall.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    .local v7, "defaultImageFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 125
    new-instance v8, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;

    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mRes:Landroid/content/res/Resources;

    const/high16 v2, 0x7f050000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v8, p0, v7, v0}, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;-><init>(Lcom/samsung/android/travel/PhotoSliding;Ljava/io/File;Ljava/io/InputStream;)V

    .line 127
    .local v8, "defaultImageThread":Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;
    invoke-virtual {v8}, Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;->start()V

    .line 130
    .end local v8    # "defaultImageThread":Lcom/samsung/android/travel/PhotoSliding$UpdateDefaultImage;
    :cond_2
    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/PhotoSliding;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mSetInterval:Landroid/widget/TextView;

    .line 131
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/PhotoSliding;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mSetWallpaper:Landroid/widget/TextView;

    .line 132
    const v0, 0x7f0b0007

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/PhotoSliding;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mBack:Landroid/widget/RelativeLayout;

    .line 133
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mBack:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mBack:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$1;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    :cond_3
    const v0, 0x7f0b0008

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/PhotoSliding;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mInterval:Landroid/widget/LinearLayout;

    .line 149
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mInterval:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 150
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mInterval:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$2;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mSetWallpaper:Landroid/widget/TextView;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$4;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    const v0, 0x7f030005

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBoxView:Landroid/view/View;

    .line 187
    const-string v0, "travelwidget"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/travel/PhotoSliding;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningPrefs:Landroid/content/SharedPreferences;

    .line 188
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->editor:Landroid/content/SharedPreferences$Editor;

    .line 190
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningPrefs:Landroid/content/SharedPreferences;

    const-string v2, "check_warning"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/travel/PhotoSliding;->show_dialog:Z

    .line 191
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBoxView:Landroid/view/View;

    const v2, 0x7f0b0010

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBox:Landroid/widget/CheckBox;

    .line 193
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBoxView:Landroid/view/View;

    const v2, 0x7f0b000e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningMessage:Landroid/widget/TextView;

    .line 195
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBoxView:Landroid/view/View;

    const v2, 0x7f0b000f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    .line 197
    .local v10, "warningLayout":Landroid/widget/LinearLayout;
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->IsWifiOnlyModel(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 198
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 199
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningMessage:Landroid/widget/TextView;

    const v2, 0x7f080012

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 213
    :goto_3
    new-instance v0, Lcom/samsung/android/travel/PhotoSliding$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/travel/PhotoSliding$5;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$6;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    iget-boolean v0, p0, Lcom/samsung/android/travel/PhotoSliding;->show_dialog:Z

    if-nez v0, :cond_4

    .line 237
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    .line 238
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/PhotoSliding;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 240
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/travel/PhotoSliding;->mCheckBoxView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 242
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f08000b

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/PhotoSliding;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/travel/PhotoSliding$7;

    invoke-direct {v3, p0}, Lcom/samsung/android/travel/PhotoSliding$7;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 257
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f08000c

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/PhotoSliding;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/travel/PhotoSliding$8;

    invoke-direct {v3, p0}, Lcom/samsung/android/travel/PhotoSliding$8;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 266
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->dialog:Landroid/app/AlertDialog;

    .line 268
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->dialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$9;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 277
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/travel/PhotoSliding;->setUpActionBar()V

    goto/16 :goto_0

    .line 109
    .end local v7    # "defaultImageFile":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    .end local v10    # "warningLayout":Landroid/widget/LinearLayout;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 120
    :cond_5
    const-string v9, "/data/data/com.samsung.android.service.travel"

    .restart local v9    # "path":Ljava/lang/String;
    goto/16 :goto_1

    .line 159
    .restart local v7    # "defaultImageFile":Ljava/io/File;
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->mSetInterval:Landroid/widget/TextView;

    new-instance v2, Lcom/samsung/android/travel/PhotoSliding$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/PhotoSliding$3;-><init>(Lcom/samsung/android/travel/PhotoSliding;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 201
    .restart local v10    # "warningLayout":Landroid/widget/LinearLayout;
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080011

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v11, v0, v2

    .line 203
    .local v11, "warningString":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 207
    .end local v11    # "warningString":Ljava/lang/String;
    :cond_8
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 208
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningMessage:Landroid/widget/TextView;

    const v2, 0x7f080012

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    .line 210
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding;->warningMessage:Landroid/widget/TextView;

    const v2, 0x7f080011

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 354
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 355
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 361
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 357
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/travel/PhotoSliding;->finish()V

    goto :goto_0

    .line 355
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 348
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 349
    return-void
.end method
