.class Lcom/samsung/android/travel/TravelWallActivity$3;
.super Ljava/lang/Object;
.source "TravelWallActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/travel/TravelWallActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/travel/TravelWallActivity;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/travel/TravelWallActivity;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    iput-object p2, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 139
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    # getter for: Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/travel/TravelWallActivity;->access$100(Lcom/samsung/android/travel/TravelWallActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    # invokes: Lcom/samsung/android/travel/TravelWallActivity;->setTravelChoiceImage()V
    invoke-static {v1}, Lcom/samsung/android/travel/TravelWallActivity;->access$200(Lcom/samsung/android/travel/TravelWallActivity;)V

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    # getter for: Lcom/samsung/android/travel/TravelWallActivity;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/samsung/android/travel/TravelWallActivity;->access$300(Lcom/samsung/android/travel/TravelWallActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "check_warning"

    iget-object v3, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 143
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    # getter for: Lcom/samsung/android/travel/TravelWallActivity;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Lcom/samsung/android/travel/TravelWallActivity;->access$300(Lcom/samsung/android/travel/TravelWallActivity;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 144
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    const-class v2, Lcom/samsung/android/travel/PhotoSliding;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/android/travel/TravelWallActivity;->startActivity(Landroid/content/Intent;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity$3;->this$0:Lcom/samsung/android/travel/TravelWallActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/travel/TravelWallActivity;->setResult(I)V

    .line 150
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 151
    return-void
.end method
