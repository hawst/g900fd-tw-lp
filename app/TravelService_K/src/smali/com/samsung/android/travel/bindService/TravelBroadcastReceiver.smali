.class public Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TravelBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private checkNetwork(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const-string v2, "connectivity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 114
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 115
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkNetworkRequest(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->checkNetwork(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const-string v1, "Network available Retrying ...."

    invoke-static {v1}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 125
    const-wide/16 v2, 0x0

    .line 127
    .local v2, "lastUpdated":J
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "last update time"

    invoke-static {v1, v4}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 133
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 134
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;Z)V

    .line 143
    .end local v2    # "lastUpdated":J
    :cond_0
    :goto_1
    return-void

    .line 129
    .restart local v2    # "lastUpdated":J
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 137
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v6, v7, v5}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;JZ)V

    goto :goto_1
.end method

.method private checkOrCreateDir()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 90
    const-string v1, "/data/data/com.samsung.android.service.travel/files/images"

    .line 91
    .local v1, "filePathImage":Ljava/lang/String;
    const-string v0, "/data/data/com.samsung.android.service.travel/files"

    .line 92
    .local v0, "filePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    .local v3, "imageFile":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v2, "fileRoot":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 109
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 99
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 100
    invoke-virtual {v3, v5, v6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 101
    invoke-virtual {v3, v5, v6}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 102
    invoke-virtual {v3, v5, v5}, Ljava/io/File;->setWritable(ZZ)Z

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 105
    invoke-virtual {v3, v5, v6}, Ljava/io/File;->setReadable(ZZ)Z

    .line 106
    invoke-virtual {v3, v5, v6}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 107
    invoke-virtual {v3, v5, v5}, Ljava/io/File;->setWritable(ZZ)Z

    goto :goto_0
.end method

.method private getBootComplete(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 155
    const-string v0, "travelservice"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "boot_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private setBootComplete(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "flag"    # Z

    .prologue
    .line 146
    const-string v2, "travelservice"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 147
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 148
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "boot_complete"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 149
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 32
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/service/travel/cache/CacheManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/service/travel/cache/CacheManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/service/travel/cache/CacheManager;->storeCacheData(Landroid/content/Context;)V

    .line 37
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    const-string v1, "Intent to TravelDirActivity"

    const-string v4, "inside TravelBroadcastReceiver"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->checkOrCreateDir()V

    .line 43
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 44
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "com.samsung.android.travel.slidingaction"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->updateTravelLockscreenImage(Landroid/content/Context;)V

    .line 87
    :cond_2
    :goto_0
    return-void

    .line 50
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->getBootComplete(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->checkNetworkRequest(Landroid/content/Context;)V

    goto :goto_0

    .line 54
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->setBootComplete(Landroid/content/Context;Z)V

    goto :goto_0

    .line 62
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1, v6}, Lcom/samsung/android/travel/bindService/TravelBroadcastReceiver;->setBootComplete(Landroid/content/Context;Z)V

    .line 64
    const-wide/16 v2, 0x0

    .line 66
    .local v2, "lastUpdated":J
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "last update time"

    invoke-static {v1, v4}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 72
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_6

    .line 73
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4, v6}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;Z)V

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 76
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_6
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v1, v4, v6, v7, v8}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;JZ)V

    goto :goto_0

    .line 83
    .end local v2    # "lastUpdated":J
    :cond_7
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->cancelAlarmService(Landroid/content/Context;)V

    .line 84
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->unBindService()V

    goto :goto_0
.end method
