.class public Lcom/samsung/android/travel/bindService/TravelBean;
.super Ljava/lang/Object;
.source "TravelBean.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mSelectCity:Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

.field private mTripChoice:Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

.field private mWallpaper:Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelBean;->mWallpaper:Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    return-object v0
.end method

.method public setSelectCity(Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V
    .locals 0
    .param p1, "mSelectCity"    # Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/android/travel/bindService/TravelBean;->mSelectCity:Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    .line 60
    return-void
.end method

.method public setTripChoice(Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V
    .locals 0
    .param p1, "tripChoice"    # Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/travel/bindService/TravelBean;->mTripChoice:Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .line 43
    return-void
.end method

.method public setmWallpaper(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V
    .locals 0
    .param p1, "mWallpaper"    # Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/android/travel/bindService/TravelBean;->mWallpaper:Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .line 69
    return-void
.end method
