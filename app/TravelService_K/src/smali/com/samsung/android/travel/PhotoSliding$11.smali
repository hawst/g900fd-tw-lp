.class Lcom/samsung/android/travel/PhotoSliding$11;
.super Ljava/lang/Object;
.source "PhotoSliding.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/travel/PhotoSliding;->showChoiceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/travel/PhotoSliding;

.field final synthetic val$interval:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/travel/PhotoSliding;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/samsung/android/travel/PhotoSliding$11;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    iput-object p2, p0, Lcom/samsung/android/travel/PhotoSliding$11;->val$interval:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 324
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$11;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    invoke-virtual {v0}, Lcom/samsung/android/travel/PhotoSliding;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sliding interval"

    iget-object v2, p0, Lcom/samsung/android/travel/PhotoSliding$11;->val$interval:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 326
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/travel/PhotoSliding$11;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/travel/PhotoSliding;->access$200(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;Z)V

    .line 327
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 328
    return-void
.end method
