.class Lcom/samsung/android/travel/PhotoSliding$7;
.super Ljava/lang/Object;
.source "PhotoSliding.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/travel/PhotoSliding;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/travel/PhotoSliding;


# direct methods
.method constructor <init>(Lcom/samsung/android/travel/PhotoSliding;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$200(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # invokes: Lcom/samsung/android/travel/PhotoSliding;->setTravelChoiceImage()V
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$300(Lcom/samsung/android/travel/PhotoSliding;)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$500(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "check_warning"

    iget-object v2, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    iget-object v2, v2, Lcom/samsung/android/travel/PhotoSliding;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 250
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->editor:Landroid/content/SharedPreferences$Editor;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$500(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 251
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/travel/PhotoSliding;->setResult(I)V

    .line 253
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$7;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    invoke-virtual {v0}, Lcom/samsung/android/travel/PhotoSliding;->finish()V

    .line 254
    return-void
.end method
