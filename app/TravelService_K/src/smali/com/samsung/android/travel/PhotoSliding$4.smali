.class Lcom/samsung/android/travel/PhotoSliding$4;
.super Ljava/lang/Object;
.source "PhotoSliding.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/travel/PhotoSliding;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/travel/PhotoSliding;


# direct methods
.method constructor <init>(Lcom/samsung/android/travel/PhotoSliding;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$100(Lcom/samsung/android/travel/PhotoSliding;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->dialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$100(Lcom/samsung/android/travel/PhotoSliding;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 183
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # getter for: Lcom/samsung/android/travel/PhotoSliding;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$200(Lcom/samsung/android/travel/PhotoSliding;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    # invokes: Lcom/samsung/android/travel/PhotoSliding;->setTravelChoiceImage()V
    invoke-static {v0}, Lcom/samsung/android/travel/PhotoSliding;->access$300(Lcom/samsung/android/travel/PhotoSliding;)V

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/travel/PhotoSliding;->setResult(I)V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/travel/PhotoSliding$4;->this$0:Lcom/samsung/android/travel/PhotoSliding;

    invoke-virtual {v0}, Lcom/samsung/android/travel/PhotoSliding;->finish()V

    goto :goto_0
.end method
