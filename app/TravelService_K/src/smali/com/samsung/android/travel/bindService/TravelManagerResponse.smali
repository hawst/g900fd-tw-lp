.class public Lcom/samsung/android/travel/bindService/TravelManagerResponse;
.super Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;
.source "TravelManagerResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;
    }
.end annotation


# static fields
.field private static _instance:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

.field public static imagePath:Ljava/lang/String;

.field private static imagesDir:Ljava/io/File;

.field private static mContext:Landroid/content/Context;

.field private static mFileDir:Ljava/io/File;

.field public static travelBean:Lcom/samsung/android/travel/bindService/TravelBean;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private bitmapRequestId:J

.field private count:I

.field private cycle_count:I

.field private deleteCache:Z

.field private extension:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private requestId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagePath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;-><init>()V

    .line 49
    const-string v0, "Travel Service : Lockscreen"

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->TAG:Ljava/lang/String;

    .line 55
    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 57
    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    .line 69
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    .line 71
    iput-boolean v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z

    .line 410
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$3;-><init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)V

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    return v0
.end method

.method static synthetic access$108(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/io/File;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    return v0
.end method

.method private checkFile(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 546
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 547
    .local v0, "image":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 548
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 549
    const/4 v2, 0x1

    .line 557
    :cond_0
    :goto_0
    return v2

    .line 550
    :cond_1
    if-eqz p1, :cond_0

    .line 551
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 552
    .local v1, "imageFile":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private checkIfExist(Ljava/lang/String;)Z
    .locals 10
    .param p1, "locationId"    # Ljava/lang/String;

    .prologue
    .line 528
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/images/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 529
    .local v2, "directoryPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 530
    .local v1, "directoryFile":Ljava/io/File;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 532
    .local v3, "fileName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 533
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 534
    .local v4, "files":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 535
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v7, v0, v5

    .line 536
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v7, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 537
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 538
    const/4 v8, 0x1

    .line 542
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "files":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "name":Ljava/lang/String;
    :goto_1
    return v8

    .line 535
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v4    # "files":[Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "name":Ljava/lang/String;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 542
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "files":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "name":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private clearEmptyPlaces(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/WallpaperResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/WallpaperResponse;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 237
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 238
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v1, v3, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;

    .line 239
    .local v1, "lockScreenUrl":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "null"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 242
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 243
    add-int/lit8 v0, v0, -0x1

    .line 244
    add-int/lit8 v2, v2, -0x1

    .line 237
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    .end local v1    # "lockScreenUrl":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private getExtensionOfURL(Ljava/lang/String;)V
    .locals 1
    .param p1, "hqUrl"    # Ljava/lang/String;

    .prologue
    .line 645
    if-eqz p1, :cond_0

    .line 646
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;

    .line 648
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const-class v1, Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    .line 75
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    .line 77
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-direct {v0}, Lcom/samsung/android/travel/bindService/TravelBean;-><init>()V

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    .line 80
    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    const-string v3, "images"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    .line 81
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setReadable(ZZ)Z

    .line 83
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 84
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mFileDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setWritable(ZZ)Z

    .line 85
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 86
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setReadable(ZZ)Z

    .line 87
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 88
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setWritable(ZZ)Z

    .line 91
    :cond_1
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->_instance:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    if-nez v0, :cond_2

    .line 92
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    invoke-direct {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;-><init>()V

    sput-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->_instance:Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .line 93
    :cond_2
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->_instance:Lcom/samsung/android/travel/bindService/TravelManagerResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private readDataFromFile(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 595
    const/4 v3, 0x0

    .line 596
    .local v3, "inputStream":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 597
    .local v0, "bufferedInputStream":Ljava/io/BufferedInputStream;
    const/4 v4, 0x0

    .line 600
    .local v4, "objectInputStream":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    const-string v8, "Response"

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 601
    .local v6, "responseFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 602
    const-string v7, "Response"

    invoke-virtual {p1, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 603
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 604
    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .local v1, "bufferedInputStream":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 605
    .end local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    .local v5, "objectInputStream":Ljava/io/ObjectInputStream;
    :try_start_2
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/travel/bindService/TravelBean;

    sput-object v7, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    .line 606
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 607
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 608
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v4, v5

    .end local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    move-object v0, v1

    .line 621
    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :cond_0
    if-eqz v3, :cond_1

    .line 622
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 624
    :cond_1
    if-eqz v0, :cond_2

    .line 625
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 627
    :cond_2
    if-eqz v4, :cond_3

    .line 628
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 636
    .end local v6    # "responseFile":Ljava/io/File;
    :cond_3
    :goto_0
    return-void

    .line 630
    .restart local v6    # "responseFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 631
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 610
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "responseFile":Ljava/io/File;
    :catch_1
    move-exception v2

    .line 611
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 621
    if-eqz v3, :cond_4

    .line 622
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 624
    :cond_4
    if-eqz v0, :cond_5

    .line 625
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 627
    :cond_5
    if-eqz v4, :cond_3

    .line 628
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 630
    :catch_2
    move-exception v2

    .line 631
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 612
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 613
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 621
    if-eqz v3, :cond_6

    .line 622
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 624
    :cond_6
    if-eqz v0, :cond_7

    .line 625
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 627
    :cond_7
    if-eqz v4, :cond_3

    .line 628
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 630
    :catch_4
    move-exception v2

    .line 631
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 614
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 615
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    :goto_3
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 621
    if-eqz v3, :cond_8

    .line 622
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 624
    :cond_8
    if-eqz v0, :cond_9

    .line 625
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 627
    :cond_9
    if-eqz v4, :cond_3

    .line 628
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 630
    :catch_6
    move-exception v2

    .line 631
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 620
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 621
    :goto_4
    if-eqz v3, :cond_a

    .line 622
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 624
    :cond_a
    if-eqz v0, :cond_b

    .line 625
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 627
    :cond_b
    if-eqz v4, :cond_c

    .line 628
    invoke-virtual {v4}, Ljava/io/ObjectInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 632
    :cond_c
    :goto_5
    throw v7

    .line 630
    :catch_7
    move-exception v2

    .line 631
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 620
    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v6    # "responseFile":Ljava/io/File;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_4

    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_4

    .line 614
    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :catch_8
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_3

    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    :catch_9
    move-exception v2

    move-object v4, v5

    .end local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_3

    .line 612
    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :catch_a
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_2

    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    :catch_b
    move-exception v2

    move-object v4, v5

    .end local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 610
    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :catch_c
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_1

    .end local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    :catch_d
    move-exception v2

    move-object v4, v5

    .end local v5    # "objectInputStream":Ljava/io/ObjectInputStream;
    .restart local v4    # "objectInputStream":Ljava/io/ObjectInputStream;
    move-object v0, v1

    .end local v1    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    goto :goto_1
.end method

.method private removeEmtyPlaces(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/SelectCity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/SelectCity;>;"
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 143
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/service/travel/response/SelectCity;

    iget-object v2, v3, Lcom/samsung/android/service/travel/response/SelectCity;->mOriginalImageURL:Ljava/lang/String;

    .line 144
    .local v2, "originalImageUrl":Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/service/travel/response/SelectCity;

    iget-object v1, v3, Lcom/samsung/android/service/travel/response/SelectCity;->mLargeImageURL:Ljava/lang/String;

    .line 146
    .local v1, "largeImageUrl":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "null"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "null"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 155
    add-int/lit8 v0, v0, -0x1

    .line 142
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    .end local v1    # "largeImageUrl":Ljava/lang/String;
    .end local v2    # "originalImageUrl":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_4

    .line 159
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->cancelAlarmService(Landroid/content/Context;)V

    .line 161
    :cond_4
    return-void
.end method

.method private requestImage(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "placesObject"    # Ljava/lang/Object;

    .prologue
    const/4 v10, 0x0

    .line 453
    move-object v0, p2

    check-cast v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .line 454
    .local v0, "advisorChoice":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    iget-object v9, v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v8, v9, :cond_6

    .line 455
    iget-object v8, v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v3, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->id:Ljava/lang/String;

    .line 456
    .local v3, "locationId":Ljava/lang/String;
    iget-object v8, v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v2, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->lockscreenImageUrl:Ljava/lang/String;

    .line 457
    .local v2, "hqImageUrl":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getExtensionOfURL(Ljava/lang/String;)V

    .line 458
    invoke-direct {p0, v3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkIfExist(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 459
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkNetwork(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 460
    if-eqz v2, :cond_2

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    sget-object v8, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "null"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 463
    iget-wide v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 464
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getConnectionManager(Landroid/content/Context;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    move-result-object v8

    iget-wide v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    invoke-virtual {v8, v10, v11}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->cancelRequest(J)V

    .line 467
    :cond_0
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getConnectionManager(Landroid/content/Context;)Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager;->getWallpaperImage(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    .line 525
    .end local v2    # "hqImageUrl":Ljava/lang/String;
    .end local v3    # "locationId":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 473
    .restart local v2    # "hqImageUrl":Ljava/lang/String;
    .restart local v3    # "locationId":Ljava/lang/String;
    :cond_2
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 474
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 477
    :cond_3
    new-instance v5, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/images"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 479
    .local v5, "originalImageDir":Ljava/io/File;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    array-length v8, v8

    const/4 v9, 0x2

    if-lt v8, v9, :cond_1

    .line 481
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 482
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 486
    .end local v5    # "originalImageDir":Ljava/io/File;
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/images/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->extension:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 488
    .local v1, "cachedImage":Ljava/lang/String;
    monitor-enter p0

    .line 489
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-direct {p0, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->checkFile(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 490
    iget-object v8, v0, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    iget v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    iget-object v4, v8, Lcom/samsung/android/service/travel/response/WallpaperResponse;->shortname:Ljava/lang/String;

    .line 492
    .local v4, "locationName":Ljava/lang/String;
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 493
    const-string v8, "Travel Service : Lockscreen"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cycle  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Image : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/20"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    iget v9, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    invoke-static {p1, v1, v8, v9}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 496
    sget-object v8, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-static {v8, v4}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 498
    .end local v4    # "locationName":Ljava/lang/String;
    :cond_5
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 501
    .end local v1    # "cachedImage":Ljava/lang/String;
    .end local v2    # "hqImageUrl":Ljava/lang/String;
    .end local v3    # "locationId":Ljava/lang/String;
    :cond_6
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    const/4 v9, 0x4

    if-ge v8, v9, :cond_7

    .line 502
    iput v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 503
    iget v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    .line 504
    sget-object v8, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v8}, Lcom/samsung/android/travel/bindService/TravelBean;->getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v7

    .line 505
    .local v7, "wallpaperResponseMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    iget-object v8, v7, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-direct {p0, v8}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->shuffleList(Ljava/util/List;)V

    .line 506
    new-instance v6, Lcom/samsung/android/travel/bindService/TravelManagerResponse$4;

    invoke-direct {v6, p0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$4;-><init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)V

    .line 512
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 513
    invoke-direct {p0, p1, v7}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 515
    .end local v6    # "thread":Ljava/lang/Thread;
    .end local v7    # "wallpaperResponseMessage":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/service/util/Utils;->checkNetwork(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 516
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getTravelConnection(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    goto/16 :goto_0

    .line 519
    :cond_8
    iput v10, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 522
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private shuffleList(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/WallpaperResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "wallpaperList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/WallpaperResponse;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 171
    .local v2, "length":I
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 172
    .local v3, "random":Ljava/util/Random;
    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    .line 173
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 174
    sub-int v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int v0, v1, v4

    .line 175
    .local v0, "change":I
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->swap(Ljava/util/List;II)V

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "change":I
    :cond_0
    return-void
.end method

.method private swap(Ljava/util/List;II)V
    .locals 2
    .param p2, "i"    # I
    .param p3, "change"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/service/travel/response/WallpaperResponse;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "wallpaperList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/WallpaperResponse;>;"
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    .line 181
    .local v0, "helper":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 182
    invoke-interface {p1, p3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 183
    return-void
.end method


# virtual methods
.method public deleteImages(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V
    .locals 13
    .param p1, "wallpaperResponse"    # Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .prologue
    .line 679
    new-instance v1, Ljava/io/File;

    sget-object v10, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    const-string v11, "images"

    invoke-direct {v1, v10, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 681
    .local v1, "directoryFile":Ljava/io/File;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 682
    .local v8, "metadata":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v10, p1, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/service/travel/response/WallpaperResponse;

    .line 683
    .local v9, "obj":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    iget-object v10, v9, Lcom/samsung/android/service/travel/response/WallpaperResponse;->id:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 686
    .end local v9    # "obj":Lcom/samsung/android/service/travel/response/WallpaperResponse;
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 687
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 688
    .local v4, "files":[Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 689
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v3, v0, v6

    .line 690
    .local v3, "filename":Ljava/lang/String;
    const/4 v5, 0x0

    .line 691
    .local v5, "flag":Z
    const/4 v10, 0x0

    const-string v11, "."

    invoke-virtual {v3, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v3, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 692
    .local v2, "file":Ljava/lang/String;
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 693
    if-nez v5, :cond_1

    .line 694
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    move-result v5

    .line 695
    if-eqz v5, :cond_1

    .line 696
    const-string v10, "Travel Service : Lockscreen"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Deleted File Name  : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 701
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "file":Ljava/lang/String;
    .end local v3    # "filename":Ljava/lang/String;
    .end local v5    # "flag":Z
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    const-string v10, "Travel Service : Lockscreen"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Lockscreen Cache Size after deleting images  : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->imagesDir:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v12

    array-length v12, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    .end local v4    # "files":[Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public declared-synchronized onBitmapResponse(JLandroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 5
    .param p1, "reqId"    # J
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "imageKey"    # Ljava/lang/String;

    .prologue
    .line 253
    monitor-enter p0

    if-eqz p3, :cond_0

    :try_start_0
    iget-wide v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    cmp-long v1, v2, p1

    if-nez v1, :cond_0

    .line 254
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    .line 256
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;-><init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;Landroid/graphics/Bitmap;J)V

    .line 258
    .local v0, "writeImageThread":Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;
    invoke-virtual {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    .end local v0    # "writeImageThread":Lcom/samsung/android/travel/bindService/TravelManagerResponse$WriteImage;
    :goto_0
    monitor-exit p0

    return-void

    .line 260
    :cond_0
    :try_start_1
    const-string v1, "Bitmap is null"

    invoke-static {v1}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onCancel(J)V
    .locals 1
    .param p1, "reqId"    # J

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onCancel(J)V

    .line 99
    return-void
.end method

.method public onCityList(JLcom/samsung/android/service/travel/response/TravelCityListResponseMessage;)V
    .locals 1
    .param p1, "reqId"    # J
    .param p3, "responsecityList"    # Lcom/samsung/android/service/travel/response/TravelCityListResponseMessage;

    .prologue
    .line 166
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onCityList(JLcom/samsung/android/service/travel/response/TravelCityListResponseMessage;)V

    .line 167
    return-void
.end method

.method public onError(JILjava/lang/Object;)V
    .locals 5
    .param p1, "reqId"    # J
    .param p3, "requestMessageid"    # I
    .param p4, "value"    # Ljava/lang/Object;

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/service/travel/ipc/TravelServiceConnectionManager$TravelServiceResponseCallback;->onError(JILjava/lang/Object;)V

    .line 104
    const-string v0, "On Error  ...."

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;)V

    .line 105
    iget-wide v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestId:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->bitmapRequestId:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->checkTravelLockscreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->checkNetwork(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 110
    :cond_1
    return-void
.end method

.method public onSelectCity(JLcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V
    .locals 5
    .param p1, "reqId"    # J
    .param p3, "selectCityResponse"    # Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;

    .prologue
    .line 120
    const/4 v1, 0x0

    .line 121
    .local v1, "status":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 122
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mStatus:Ljava/lang/String;

    .line 123
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 126
    :cond_0
    const-string v2, "TravelServiceConnnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 129
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v2, p3}, Lcom/samsung/android/travel/bindService/TravelBean;->setSelectCity(Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V

    .line 130
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->writeDatatoFile(Landroid/content/Context;)V

    .line 131
    iget-object v0, p3, Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;->mPlaces:Ljava/util/List;

    .line 132
    .local v0, "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/SelectCity;>;"
    if-eqz v0, :cond_1

    .line 133
    invoke-direct {p0, v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->removeEmtyPlaces(Ljava/util/List;)V

    .line 134
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2, p3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 137
    .end local v0    # "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/SelectCity;>;"
    :cond_1
    return-void
.end method

.method public declared-synchronized onTripAdvisorChoiceResponse(JLcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V
    .locals 5
    .param p1, "reqId"    # J
    .param p3, "tripAdvisorChoice"    # Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;

    .prologue
    .line 392
    monitor-enter p0

    const/4 v1, 0x0

    .line 393
    .local v1, "status":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 394
    :try_start_0
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mStatus:Ljava/lang/String;

    .line 395
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 398
    :cond_0
    const-string v2, "TravelServiceConnnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    if-eqz v1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 401
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v2, p3}, Lcom/samsung/android/travel/bindService/TravelBean;->setTripChoice(Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V

    .line 402
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->writeDatatoFile(Landroid/content/Context;)V

    .line 403
    iget-object v0, p3, Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;->mPlaces:Ljava/util/List;

    .line 404
    .local v0, "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/TripAdvisorChoice;>;"
    if-eqz v0, :cond_1

    .line 405
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2, p3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    .end local v0    # "placeslist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/service/travel/response/TripAdvisorChoice;>;"
    :cond_1
    monitor-exit p0

    return-void

    .line 392
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public onWallpaperResult(JLcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V
    .locals 7
    .param p1, "reqId"    # J
    .param p3, "wallpaperResponse"    # Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    .prologue
    const/16 v6, 0x14

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestId:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->clearEmptyPlaces(Ljava/util/List;)V

    .line 195
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 196
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->cancelAlarmService(Landroid/content/Context;)V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 198
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->cancelAlarmService(Landroid/content/Context;)V

    .line 199
    iput-boolean v5, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z

    .line 200
    iput v4, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 201
    iput v4, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    .line 202
    sget-object v1, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v1, p3}, Lcom/samsung/android/travel/bindService/TravelBean;->setmWallpaper(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    .line 203
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$1;-><init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)V

    .line 209
    .local v0, "thread":Ljava/lang/Thread;
    const-string v1, "Travel Service : Lockscreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wallpaper Meta data  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 211
    sget-object v1, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 213
    .end local v0    # "thread":Ljava/lang/Thread;
    :cond_3
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->shuffleList(Ljava/util/List;)V

    .line 214
    iget-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v6, :cond_4

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v6}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p3, Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;->mWallpaperResponse:Ljava/util/ArrayList;

    .line 218
    :cond_4
    iput-boolean v5, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->deleteCache:Z

    .line 219
    iput v4, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 220
    iput v4, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I

    .line 221
    sget-object v1, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v1, p3}, Lcom/samsung/android/travel/bindService/TravelBean;->setmWallpaper(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    .line 222
    new-instance v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse$2;-><init>(Lcom/samsung/android/travel/bindService/TravelManagerResponse;)V

    .line 228
    .restart local v0    # "thread":Ljava/lang/Thread;
    const-string v1, "Travel Service : Lockscreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wallpaper Meta data  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/service/util/Utils;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 230
    sget-object v1, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, p3}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public setRequestId(J)V
    .locals 1
    .param p1, "requestId"    # J

    .prologue
    .line 674
    iput-wide p1, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestId:J

    .line 675
    return-void
.end method

.method public updateTravelLockscreenImage(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v2}, Lcom/samsung/android/travel/bindService/TravelBean;->getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v1

    .line 426
    .local v1, "mWallpaperResponse":Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;
    if-eqz v1, :cond_1

    .line 427
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->readDataFromFile(Landroid/content/Context;)V

    .line 431
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "last update image"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->count:I

    .line 433
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "metadata_update_cycle"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->cycle_count:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    :goto_1
    sget-object v2, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v2}, Lcom/samsung/android/travel/bindService/TravelBean;->getmWallpaper()Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;

    move-result-object v1

    .line 441
    if-nez v1, :cond_2

    .line 442
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/service/util/Utils;->checkNetwork(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getTravelConnection(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    goto :goto_0

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 446
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->requestImage(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public writeDatatoFile(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 562
    const/4 v0, 0x0

    .line 563
    .local v0, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    const/4 v3, 0x0

    .line 566
    .local v3, "objectOutputStream":Ljava/io/ObjectOutputStream;
    :try_start_0
    const-string v6, "Response"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    .line 568
    .local v5, "outputSteam":Ljava/io/FileOutputStream;
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 569
    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .local v1, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v4, Ljava/io/ObjectOutputStream;

    invoke-direct {v4, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 570
    .end local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .local v4, "objectOutputStream":Ljava/io/ObjectOutputStream;
    :try_start_2
    sget-object v6, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    if-eqz v6, :cond_0

    .line 571
    sget-object v6, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v4, v6}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 573
    :cond_0
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 582
    if-eqz v4, :cond_1

    .line 583
    :try_start_3
    invoke-virtual {v4}, Ljava/io/ObjectOutputStream;->close()V

    .line 585
    :cond_1
    if-eqz v1, :cond_2

    .line 586
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v3, v4

    .end local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    move-object v0, v1

    .line 592
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v5    # "outputSteam":Ljava/io/FileOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :cond_3
    :goto_0
    return-void

    .line 588
    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v5    # "outputSteam":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 589
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .end local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    move-object v0, v1

    .line 591
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_0

    .line 574
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "outputSteam":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v2

    .line 575
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 582
    if-eqz v3, :cond_4

    .line 583
    :try_start_5
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V

    .line 585
    :cond_4
    if-eqz v0, :cond_3

    .line 586
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 588
    :catch_2
    move-exception v2

    .line 589
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 576
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 577
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 582
    if-eqz v3, :cond_5

    .line 583
    :try_start_7
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V

    .line 585
    :cond_5
    if-eqz v0, :cond_3

    .line 586
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 588
    :catch_4
    move-exception v2

    .line 589
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 581
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 582
    :goto_3
    if-eqz v3, :cond_6

    .line 583
    :try_start_8
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V

    .line 585
    :cond_6
    if-eqz v0, :cond_7

    .line 586
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 590
    :cond_7
    :goto_4
    throw v6

    .line 588
    :catch_5
    move-exception v2

    .line 589
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 581
    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v5    # "outputSteam":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_3

    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_3

    .line 576
    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_2

    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_2

    .line 574
    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catch_8
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1

    .end local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    :catch_9
    move-exception v2

    move-object v3, v4

    .end local v4    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    .restart local v3    # "objectOutputStream":Ljava/io/ObjectOutputStream;
    move-object v0, v1

    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1
.end method
