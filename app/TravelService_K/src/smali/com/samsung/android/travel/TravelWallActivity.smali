.class public Lcom/samsung/android/travel/TravelWallActivity;
.super Landroid/app/Activity;
.source "TravelWallActivity.java"


# instance fields
.field private builder:Landroid/app/AlertDialog$Builder;

.field private dialog:Landroid/app/AlertDialog;

.field private editor:Landroid/content/SharedPreferences$Editor;

.field private isSelected:Z

.field private mContext:Landroid/content/Context;

.field private show_dialog:Z

.field private warningMessage:Landroid/widget/TextView;

.field private warningPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->isSelected:Z

    .line 237
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/travel/TravelWallActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/TravelWallActivity;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->isSelected:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/travel/TravelWallActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/TravelWallActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/android/travel/TravelWallActivity;->isSelected:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/travel/TravelWallActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/TravelWallActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/travel/TravelWallActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/travel/TravelWallActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/travel/TravelWallActivity;->setTravelChoiceImage()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/travel/TravelWallActivity;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/travel/TravelWallActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->editor:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method private setTravelChoiceImage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 214
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/travel_wall.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v5}, Lcom/samsung/android/service/util/Utils;->setTravelLockScreen(Landroid/content/Context;Ljava/lang/String;II)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/samsung/android/service/util/Utils;->setLocationName(Landroid/content/Context;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/travel/TravelWallActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper_transparent"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 222
    invoke-virtual {p0}, Lcom/samsung/android/travel/TravelWallActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "lockscreen_wallpaper"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 224
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    .line 227
    :cond_0
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setSelectCity(Lcom/samsung/android/service/travel/response/SelectCityResponseMessage;)V

    .line 228
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setTripChoice(Lcom/samsung/android/service/travel/response/TripAdvisorChoiceResponseMessage;)V

    .line 229
    sget-object v0, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->travelBean:Lcom/samsung/android/travel/bindService/TravelBean;

    invoke-virtual {v0, v3}, Lcom/samsung/android/travel/bindService/TravelBean;->setmWallpaper(Lcom/samsung/android/service/travel/response/WallpaperResponseMessage;)V

    .line 230
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->getInstance(Landroid/content/Context;)Lcom/samsung/android/travel/bindService/TravelManagerResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/travel/bindService/TravelManagerResponse;->writeDatatoFile(Landroid/content/Context;)V

    .line 233
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/samsung/android/service/util/Utils;->setUpdatedTimeStamp(Landroid/content/Context;J)V

    .line 234
    invoke-static {}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->getInstance()Lcom/samsung/android/travel/bindService/TravelServiceConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/travel/bindService/TravelServiceConnection;->startAlarmService(Landroid/content/Context;Z)V

    .line 235
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    const v13, 0x7f080008

    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    iput-object p0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    .line 61
    const-string v0, "content://com.sec.knox.provider/RestrictionPolicy4"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 62
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "isWallpaperChangeAllowed"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 64
    .local v8, "cr":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 67
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 68
    const-string v0, "isWallpaperChangeAllowed"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "false"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 181
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 78
    :cond_1
    const-string v0, "travelwidget"

    invoke-virtual {p0, v0, v12}, Lcom/samsung/android/travel/TravelWallActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningPrefs:Landroid/content/SharedPreferences;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->editor:Landroid/content/SharedPreferences$Editor;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningPrefs:Landroid/content/SharedPreferences;

    const-string v3, "check_warning"

    invoke-interface {v0, v3, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->show_dialog:Z

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->show_dialog:Z

    .line 86
    const v0, 0x7f030005

    invoke-static {p0, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 87
    .local v7, "checkBoxView":Landroid/view/View;
    const v0, 0x7f0b0010

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 89
    .local v6, "checkBox":Landroid/widget/CheckBox;
    const v0, 0x7f0b000e

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningMessage:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0b000f

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    .line 93
    .local v10, "warningLayout":Landroid/widget/LinearLayout;
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/service/util/Utils;->IsWifiOnlyModel(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/travel/TravelWallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v11, v0, v12

    .line 96
    .local v11, "warningString":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    .end local v11    # "warningString":Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/samsung/android/travel/TravelWallActivity$1;

    invoke-direct {v0, p0, v6}, Lcom/samsung/android/travel/TravelWallActivity$1;-><init>(Lcom/samsung/android/travel/TravelWallActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    new-instance v0, Lcom/samsung/android/travel/TravelWallActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/travel/TravelWallActivity$2;-><init>(Lcom/samsung/android/travel/TravelWallActivity;)V

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-boolean v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->show_dialog:Z

    if-nez v0, :cond_4

    .line 130
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    .line 131
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/TravelWallActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 133
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 135
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f08000b

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/TravelWallActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/travel/TravelWallActivity$3;

    invoke-direct {v3, p0, v6}, Lcom/samsung/android/travel/TravelWallActivity$3;-><init>(Lcom/samsung/android/travel/TravelWallActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 154
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f08000c

    invoke-virtual {p0, v2}, Lcom/samsung/android/travel/TravelWallActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/travel/TravelWallActivity$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/travel/TravelWallActivity$4;-><init>(Lcom/samsung/android/travel/TravelWallActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->dialog:Landroid/app/AlertDialog;

    .line 165
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->dialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/samsung/android/travel/TravelWallActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/travel/TravelWallActivity$5;-><init>(Lcom/samsung/android/travel/TravelWallActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 173
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 74
    .end local v6    # "checkBox":Landroid/widget/CheckBox;
    .end local v7    # "checkBoxView":Landroid/view/View;
    .end local v10    # "warningLayout":Landroid/widget/LinearLayout;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 99
    .restart local v6    # "checkBox":Landroid/widget/CheckBox;
    .restart local v7    # "checkBoxView":Landroid/view/View;
    .restart local v10    # "warningLayout":Landroid/widget/LinearLayout;
    :cond_2
    invoke-static {}, Lcom/samsung/android/service/travel/TravelService;->IsChinaModel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningMessage:Landroid/widget/TextView;

    const v2, 0x7f080009

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/travel/TravelWallActivity;->warningMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 176
    :cond_4
    new-instance v9, Landroid/content/Intent;

    const-class v0, Lcom/samsung/android/travel/PhotoSliding;

    invoke-direct {v9, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v9, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v9}, Lcom/samsung/android/travel/TravelWallActivity;->startActivity(Landroid/content/Intent;)V

    .line 178
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/travel/TravelWallActivity;->setResult(I)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/travel/TravelWallActivity;->finish()V

    goto/16 :goto_0
.end method
