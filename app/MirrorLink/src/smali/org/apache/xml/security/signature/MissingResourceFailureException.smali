.class public Lorg/apache/xml/security/signature/MissingResourceFailureException;
.super Lorg/apache/xml/security/signature/XMLSignatureException;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field uninitializedReference:Lorg/apache/xml/security/signature/Reference;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;Lorg/apache/xml/security/signature/Reference;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    iput-object p3, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/xml/security/signature/Reference;)V
    .locals 1

    invoke-direct {p0, p1}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    iput-object p2, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;Lorg/apache/xml/security/signature/Reference;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    iput-object p4, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;Lorg/apache/xml/security/signature/Reference;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    iput-object p3, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-void
.end method


# virtual methods
.method public getReference()Lorg/apache/xml/security/signature/Reference;
    .locals 1

    iget-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-object v0
.end method

.method public setReference(Lorg/apache/xml/security/signature/Reference;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->uninitializedReference:Lorg/apache/xml/security/signature/Reference;

    return-void
.end method
