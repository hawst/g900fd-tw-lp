.class public Lorg/apache/xml/security/encryption/XMLCipherInput;
.super Ljava/lang/Object;


# static fields
.field static class$org$apache$xml$security$encryption$XMLCipher:Ljava/lang/Class;

.field private static logger:Lorg/apache/commons/logging/Log;


# instance fields
.field private _cipherData:Lorg/apache/xml/security/encryption/CipherData;

.field private _mode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->class$org$apache$xml$security$encryption$XMLCipher:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.encryption.XMLCipher"

    invoke-static {v0}, Lorg/apache/xml/security/encryption/XMLCipherInput;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->class$org$apache$xml$security$encryption$XMLCipher:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->class$org$apache$xml$security$encryption$XMLCipher:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/xml/security/encryption/CipherData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_mode:I

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v1, "CipherData is null"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/xml/security/encryption/EncryptedType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_mode:I

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v1, "CipherData is null"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/xml/security/encryption/EncryptedType;->getCipherData()Lorg/apache/xml/security/encryption/CipherData;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getDecryptBytes()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    invoke-interface {v0}, Lorg/apache/xml/security/encryption/CipherData;->getDataType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    sget-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    const-string v1, "Found a reference type CipherData"

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    invoke-interface {v0}, Lorg/apache/xml/security/encryption/CipherData;->getCipherReference()Lorg/apache/xml/security/encryption/CipherReference;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/xml/security/encryption/CipherReference;->getURIAsAttr()Lorg/w3c/dom/Attr;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v2}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->getInstance(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/utils/resolver/ResourceResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->resolve(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    :try_end_0
    .catch Lorg/apache/xml/security/utils/resolver/ResourceResolverException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v2, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Managed to resolve URI \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {v1}, Lorg/apache/xml/security/encryption/CipherReference;->getURI()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :goto_0
    invoke-interface {v1}, Lorg/apache/xml/security/encryption/CipherReference;->getTransforms()Lorg/apache/xml/security/encryption/Transforms;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    const-string v3, "Have transforms in cipher reference"

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :try_start_1
    invoke-interface {v1}, Lorg/apache/xml/security/encryption/Transforms;->getDSTransforms()Lorg/apache/xml/security/transforms/Transforms;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/xml/security/transforms/Transforms;->performTransforms(Lorg/apache/xml/security/signature/XMLSignatureInput;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    :try_end_1
    .catch Lorg/apache/xml/security/transforms/TransformationException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :cond_0
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;->getBytes()[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :cond_1
    sget-object v2, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Failed to resolve URI \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {v1}, Lorg/apache/xml/security/encryption/CipherReference;->getURI()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :cond_2
    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    invoke-interface {v0}, Lorg/apache/xml/security/encryption/CipherData;->getDataType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_cipherData:Lorg/apache/xml/security/encryption/CipherData;

    invoke-interface {v0}, Lorg/apache/xml/security/encryption/CipherData;->getCipherValue()Lorg/apache/xml/security/encryption/CipherValue;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-interface {v0}, Lorg/apache/xml/security/encryption/CipherValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sget-object v0, Lorg/apache/xml/security/encryption/XMLCipherInput;->logger:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Encrypted octets:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :try_start_3
    invoke-static {v1}, Lorg/apache/xml/security/utils/Base64;->decode(Ljava/lang/String;)[B
    :try_end_3
    .catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v0, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v1, "CipherData.getDataType() returned unexpected value"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_4
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/encryption/XMLEncryptionException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/encryption/XMLEncryptionException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method public getBytes()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation

    iget v0, p0, Lorg/apache/xml/security/encryption/XMLCipherInput;->_mode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/xml/security/encryption/XMLCipherInput;->getDecryptBytes()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
