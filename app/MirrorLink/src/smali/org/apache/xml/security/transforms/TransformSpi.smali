.class public abstract Lorg/apache/xml/security/transforms/TransformSpi;
.super Ljava/lang/Object;


# static fields
.field static class$org$apache$xml$security$transforms$TransformSpi:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# instance fields
.field protected _transformObject:Lorg/apache/xml/security/transforms/Transform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/transforms/TransformSpi;->class$org$apache$xml$security$transforms$TransformSpi:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.transforms.TransformSpi"

    invoke-static {v0}, Lorg/apache/xml/security/transforms/TransformSpi;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/TransformSpi;->class$org$apache$xml$security$transforms$TransformSpi:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/TransformSpi;->log:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/transforms/TransformSpi;->class$org$apache$xml$security$transforms$TransformSpi:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/transforms/TransformSpi;->_transformObject:Lorg/apache/xml/security/transforms/Transform;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected abstract engineGetURI()Ljava/lang/String;
.end method

.method protected abstract enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/xml/security/c14n/CanonicalizationException;,
            Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;,
            Lorg/apache/xml/security/transforms/TransformationException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation
.end method

.method protected enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/xml/security/c14n/CanonicalizationException;,
            Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;,
            Lorg/apache/xml/security/transforms/TransformationException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/xml/security/transforms/TransformSpi;->enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;)Lorg/apache/xml/security/signature/XMLSignatureInput;

    move-result-object v0

    return-object v0
.end method

.method protected setTransform(Lorg/apache/xml/security/transforms/Transform;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/xml/security/transforms/TransformSpi;->_transformObject:Lorg/apache/xml/security/transforms/Transform;

    return-void
.end method
