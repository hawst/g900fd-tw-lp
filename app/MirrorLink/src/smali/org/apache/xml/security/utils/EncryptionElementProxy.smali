.class public abstract Lorg/apache/xml/security/utils/EncryptionElementProxy;
.super Lorg/apache/xml/security/utils/ElementProxy;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/xml/security/utils/ElementProxy;-><init>(Lorg/w3c/dom/Document;)V

    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/exceptions/XMLSecurityException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/ElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getBaseNamespace()Ljava/lang/String;
    .locals 1

    const-string v0, "http://www.w3.org/2001/04/xmlenc#"

    return-object v0
.end method
