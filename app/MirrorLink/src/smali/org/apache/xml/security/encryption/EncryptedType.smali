.class public interface abstract Lorg/apache/xml/security/encryption/EncryptedType;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getCipherData()Lorg/apache/xml/security/encryption/CipherData;
.end method

.method public abstract getEncoding()Ljava/lang/String;
.end method

.method public abstract getEncryptionMethod()Lorg/apache/xml/security/encryption/EncryptionMethod;
.end method

.method public abstract getEncryptionProperties()Lorg/apache/xml/security/encryption/EncryptionProperties;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getKeyInfo()Lorg/apache/xml/security/keys/KeyInfo;
.end method

.method public abstract getMimeType()Ljava/lang/String;
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract setEncoding(Ljava/lang/String;)V
.end method

.method public abstract setEncryptionMethod(Lorg/apache/xml/security/encryption/EncryptionMethod;)V
.end method

.method public abstract setEncryptionProperties(Lorg/apache/xml/security/encryption/EncryptionProperties;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setKeyInfo(Lorg/apache/xml/security/keys/KeyInfo;)V
.end method

.method public abstract setMimeType(Ljava/lang/String;)V
.end method

.method public abstract setType(Ljava/lang/String;)V
.end method
