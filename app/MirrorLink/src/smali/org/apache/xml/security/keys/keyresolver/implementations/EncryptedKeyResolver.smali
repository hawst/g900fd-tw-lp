.class public Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;
.super Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;


# static fields
.field static class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# instance fields
.field _algorithm:Ljava/lang/String;

.field _kek:Ljava/security/Key;

.field _key:Ljava/security/Key;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.keys.keyresolver.implementations.RSAKeyValueResolver"

    invoke-static {v0}, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->log:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;-><init>()V

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_key:Ljava/security/Key;

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_kek:Ljava/security/Key;

    iput-object p1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_algorithm:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/Key;)V
    .locals 1

    invoke-direct {p0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_key:Ljava/security/Key;

    iput-object p1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_algorithm:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_kek:Ljava/security/Key;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public engineCanResolve(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Z
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->log:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "EncryptedKeyResolver - Can I resolve "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    if-nez p1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string v1, "EncryptedKey"

    invoke-static {p1, v1}, Lorg/apache/xml/security/utils/XMLUtils;->elementIsInEncryptionSpace(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, "Passed an Encrypted Key"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :try_start_0
    invoke-static {}, Lorg/apache/xml/security/encryption/XMLCipher;->getInstance()Lorg/apache/xml/security/encryption/XMLCipher;

    move-result-object v1

    const/4 v2, 0x4

    iget-object v3, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_kek:Ljava/security/Key;

    invoke-virtual {v1, v2, v3}, Lorg/apache/xml/security/encryption/XMLCipher;->init(ILjava/security/Key;)V

    invoke-virtual {v1, p1}, Lorg/apache/xml/security/encryption/XMLCipher;->loadEncryptedKey(Lorg/w3c/dom/Element;)Lorg/apache/xml/security/encryption/EncryptedKey;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_algorithm:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/apache/xml/security/encryption/XMLCipher;->decryptKey(Lorg/apache/xml/security/encryption/EncryptedKey;Ljava/lang/String;)Ljava/security/Key;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_key:Ljava/security/Key;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_key:Ljava/security/Key;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public engineResolvePublicKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/PublicKey;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public engineResolveSecretKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljavax/crypto/SecretKey;
    .locals 1

    iget-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/EncryptedKeyResolver;->_key:Ljava/security/Key;

    check-cast v0, Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public engineResolveX509Certificate(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/cert/X509Certificate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
