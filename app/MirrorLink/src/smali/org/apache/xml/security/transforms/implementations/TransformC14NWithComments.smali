.class public Lorg/apache/xml/security/transforms/implementations/TransformC14NWithComments;
.super Lorg/apache/xml/security/transforms/TransformSpi;


# static fields
.field public static final implementedTransformURI:Ljava/lang/String; = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V

    return-void
.end method


# virtual methods
.method protected engineGetURI()Ljava/lang/String;
    .locals 1

    const-string v0, "http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments"

    return-object v0
.end method

.method protected enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/c14n/CanonicalizationException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/xml/security/transforms/implementations/TransformC14NWithComments;->enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;

    move-result-object v0

    return-object v0
.end method

.method protected enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/c14n/CanonicalizationException;
        }
    .end annotation

    new-instance v0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315WithComments;

    invoke-direct {v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315WithComments;-><init>()V

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->setWriter(Ljava/io/OutputStream;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->setNeedsToBeExpanded(Z)V

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->engineCanonicalize(Lorg/apache/xml/security/signature/XMLSignatureInput;)[B

    move-result-object v0

    new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;

    invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V

    if-eqz p2, :cond_1

    invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->setOutputStream(Ljava/io/OutputStream;)V

    :cond_1
    return-object v1
.end method
