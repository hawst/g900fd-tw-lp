.class Lorg/apache/xml/security/encryption/XMLCipher$Factory$TransformsImpl;
.super Lorg/apache/xml/security/transforms/Transforms;

# interfaces
.implements Lorg/apache/xml/security/encryption/Transforms;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/xml/security/encryption/XMLCipher$Factory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TransformsImpl"
.end annotation


# instance fields
.field private final this$1:Lorg/apache/xml/security/encryption/XMLCipher$Factory;


# direct methods
.method public constructor <init>(Lorg/apache/xml/security/encryption/XMLCipher$Factory;)V
    .locals 1

    invoke-static {p1}, Lorg/apache/xml/security/encryption/XMLCipher$Factory;->access$400(Lorg/apache/xml/security/encryption/XMLCipher$Factory;)Lorg/apache/xml/security/encryption/XMLCipher;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/xml/security/encryption/XMLCipher;->access$200(Lorg/apache/xml/security/encryption/XMLCipher;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Document;)V

    iput-object p1, p0, Lorg/apache/xml/security/encryption/XMLCipher$Factory$TransformsImpl;->this$1:Lorg/apache/xml/security/encryption/XMLCipher$Factory;

    return-void
.end method

.method public constructor <init>(Lorg/apache/xml/security/encryption/XMLCipher$Factory;Lorg/w3c/dom/Document;)V
    .locals 0

    invoke-direct {p0, p2}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Document;)V

    iput-object p1, p0, Lorg/apache/xml/security/encryption/XMLCipher$Factory$TransformsImpl;->this$1:Lorg/apache/xml/security/encryption/XMLCipher$Factory;

    return-void
.end method

.method public constructor <init>(Lorg/apache/xml/security/encryption/XMLCipher$Factory;Lorg/w3c/dom/Element;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;,
            Lorg/apache/xml/security/transforms/InvalidTransformException;,
            Lorg/apache/xml/security/exceptions/XMLSecurityException;,
            Lorg/apache/xml/security/transforms/TransformationException;
        }
    .end annotation

    const-string v0, ""

    invoke-direct {p0, p2, v0}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/xml/security/encryption/XMLCipher$Factory$TransformsImpl;->this$1:Lorg/apache/xml/security/encryption/XMLCipher$Factory;

    return-void
.end method


# virtual methods
.method public getBaseNamespace()Ljava/lang/String;
    .locals 1

    const-string v0, "http://www.w3.org/2001/04/xmlenc#"

    return-object v0
.end method

.method public getDSTransforms()Lorg/apache/xml/security/transforms/Transforms;
    .locals 0

    return-object p0
.end method

.method public toElement()Lorg/w3c/dom/Element;
    .locals 1

    iget-object v0, p0, Lorg/apache/xml/security/utils/ElementProxy;->_doc:Lorg/w3c/dom/Document;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/xml/security/encryption/XMLCipher$Factory$TransformsImpl;->this$1:Lorg/apache/xml/security/encryption/XMLCipher$Factory;

    invoke-static {v0}, Lorg/apache/xml/security/encryption/XMLCipher$Factory;->access$400(Lorg/apache/xml/security/encryption/XMLCipher$Factory;)Lorg/apache/xml/security/encryption/XMLCipher;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/xml/security/encryption/XMLCipher;->access$200(Lorg/apache/xml/security/encryption/XMLCipher;)Lorg/w3c/dom/Document;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/xml/security/utils/ElementProxy;->_doc:Lorg/w3c/dom/Document;

    :cond_0
    invoke-virtual {p0}, Lorg/apache/xml/security/utils/ElementProxy;->getElement()Lorg/w3c/dom/Element;

    move-result-object v0

    return-object v0
.end method
