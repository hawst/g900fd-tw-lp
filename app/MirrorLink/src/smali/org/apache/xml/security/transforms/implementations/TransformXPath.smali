.class public Lorg/apache/xml/security/transforms/implementations/TransformXPath;
.super Lorg/apache/xml/security/transforms/TransformSpi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter;
    }
.end annotation


# static fields
.field static class$org$apache$xml$security$transforms$implementations$TransformXPath:Ljava/lang/Class; = null

.field public static final implementedTransformURI:Ljava/lang/String; = "http://www.w3.org/TR/1999/REC-xpath-19991116"

.field static log:Lorg/apache/commons/logging/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->class$org$apache$xml$security$transforms$implementations$TransformXPath:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.transforms.implementations.TransformXPath"

    invoke-static {v0}, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->class$org$apache$xml$security$transforms$implementations$TransformXPath:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->log:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->class$org$apache$xml$security$transforms$implementations$TransformXPath:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private needsCircunvent(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected engineGetURI()Ljava/lang/String;
    .locals 1

    const-string v0, "http://www.w3.org/TR/1999/REC-xpath-19991116"

    return-object v0
.end method

.method protected enginePerformTransform(Lorg/apache/xml/security/signature/XMLSignatureInput;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/transforms/TransformationException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lorg/apache/xml/security/transforms/TransformSpi;->_transformObject:Lorg/apache/xml/security/transforms/Transform;

    invoke-virtual {v0}, Lorg/apache/xml/security/utils/ElementProxy;->getElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/xml/security/utils/CachedXPathAPIHolder;->setDoc(Lorg/w3c/dom/Document;)V

    iget-object v0, p0, Lorg/apache/xml/security/transforms/TransformSpi;->_transformObject:Lorg/apache/xml/security/transforms/Transform;

    invoke-virtual {v0}, Lorg/apache/xml/security/utils/ElementProxy;->getElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    const-string v1, "XPath"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->selectDsNode(Lorg/w3c/dom/Node;Ljava/lang/String;I)Lorg/w3c/dom/Element;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "ds:XPath"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Transform"

    aput-object v2, v0, v1

    new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v2, "xml.WrongContent"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catch Lorg/w3c/dom/DOMException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;->getStrFromNode(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/xml/security/transforms/implementations/TransformXPath;->needsCircunvent(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/xml/security/signature/XMLSignatureInput;->setNeedsToBeExpanded(Z)V

    if-nez v1, :cond_1

    new-instance v0, Lorg/w3c/dom/DOMException;

    const/4 v1, 0x3

    const-string v2, "Text must be in ds:Xpath"

    invoke-direct {v0, v1, v2}, Lorg/w3c/dom/DOMException;-><init>(SLjava/lang/String;)V

    throw v0

    :cond_1
    new-instance v3, Lorg/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter;

    invoke-direct {v3, p0, v0, v1, v2}, Lorg/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter;-><init>(Lorg/apache/xml/security/transforms/implementations/TransformXPath;Lorg/w3c/dom/Element;Lorg/w3c/dom/Node;Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Lorg/apache/xml/security/signature/XMLSignatureInput;->addNodeFilter(Lorg/apache/xml/security/signature/NodeFilter;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;->setNodeSet(Z)V
    :try_end_1
    .catch Lorg/w3c/dom/DOMException; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1
.end method
