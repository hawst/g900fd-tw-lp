.class public Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;
.super Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;


# static fields
.field static class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# instance fields
.field private _rsaKeyElement:Lorg/w3c/dom/Element;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.keys.keyresolver.implementations.RSAKeyValueResolver"

    invoke-static {v0}, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->log:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$RSAKeyValueResolver:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public engineCanResolve(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->log:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Can I resolve "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    if-nez p1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string v2, "KeyValue"

    invoke-static {p1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->elementIsInSignatureSpace(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    move-result v2

    const-string v3, "RSAKeyValue"

    invoke-static {p1, v3}, Lorg/apache/xml/security/utils/XMLUtils;->elementIsInSignatureSpace(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    move-result v3

    if-eqz v2, :cond_3

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    const-string v3, "RSAKeyValue"

    invoke-static {v2, v3, v0}, Lorg/apache/xml/security/utils/XMLUtils;->selectDsNode(Lorg/w3c/dom/Node;Ljava/lang/String;I)Lorg/w3c/dom/Element;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    iget-object v2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_1

    iput-object p1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    move v0, v1

    goto :goto_0
.end method

.method public engineResolvePublicKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/PublicKey;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    if-nez v1, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->engineCanResolve(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v1, Lorg/apache/xml/security/keys/content/keyvalues/RSAKeyValue;

    iget-object v2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->_rsaKeyElement:Lorg/w3c/dom/Element;

    invoke-direct {v1, v2, p2}, Lorg/apache/xml/security/keys/content/keyvalues/RSAKeyValue;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/xml/security/keys/content/keyvalues/RSAKeyValue;->getPublicKey()Ljava/security/PublicKey;
    :try_end_0
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lorg/apache/xml/security/keys/keyresolver/implementations/RSAKeyValueResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v3, "XMLSecurityException"

    invoke-interface {v2, v3, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public engineResolveSecretKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljavax/crypto/SecretKey;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public engineResolveX509Certificate(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/cert/X509Certificate;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
