.class public interface abstract Lorg/apache/xml/security/encryption/CipherReference;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getTransforms()Lorg/apache/xml/security/encryption/Transforms;
.end method

.method public abstract getURI()Ljava/lang/String;
.end method

.method public abstract getURIAsAttr()Lorg/w3c/dom/Attr;
.end method

.method public abstract setTransforms(Lorg/apache/xml/security/encryption/Transforms;)V
.end method
