.class public interface abstract Lorg/apache/xml/security/encryption/EncryptionProperties;
.super Ljava/lang/Object;


# virtual methods
.method public abstract addEncryptionProperty(Lorg/apache/xml/security/encryption/EncryptionProperty;)V
.end method

.method public abstract getEncryptionProperties()Ljava/util/Iterator;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract removeEncryptionProperty(Lorg/apache/xml/security/encryption/EncryptionProperty;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method
