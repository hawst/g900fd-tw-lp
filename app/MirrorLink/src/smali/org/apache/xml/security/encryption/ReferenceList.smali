.class public interface abstract Lorg/apache/xml/security/encryption/ReferenceList;
.super Ljava/lang/Object;


# static fields
.field public static final DATA_REFERENCE:I = 0x1

.field public static final KEY_REFERENCE:I = 0x2


# virtual methods
.method public abstract add(Lorg/apache/xml/security/encryption/Reference;)V
.end method

.method public abstract getReferences()Ljava/util/Iterator;
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract newDataReference(Ljava/lang/String;)Lorg/apache/xml/security/encryption/Reference;
.end method

.method public abstract newKeyReference(Ljava/lang/String;)Lorg/apache/xml/security/encryption/Reference;
.end method

.method public abstract remove(Lorg/apache/xml/security/encryption/Reference;)V
.end method

.method public abstract size()I
.end method
