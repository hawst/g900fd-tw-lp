.class public Lorg/apache/xml/security/algorithms/SignatureAlgorithm;
.super Lorg/apache/xml/security/algorithms/Algorithm;


# static fields
.field static _algorithmHash:Ljava/util/HashMap;

.field static _alreadyInitialized:Z

.field static class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# instance fields
.field protected _signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.algorithms.SignatureAlgorithm"

    invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_alreadyInitialized:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_algorithmHash:Ljava/util/HashMap;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/exceptions/XMLSecurityException;
        }
    .end annotation

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/algorithms/Algorithm;-><init>(Lorg/w3c/dom/Document;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    :try_start_0
    invoke-static {p2}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->getImplementingClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Create URI \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\" class \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    return-void

    :catch_0
    move-exception v0

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_1
    move-exception v0

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_2
    move-exception v0

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/exceptions/XMLSecurityException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;-><init>(Lorg/w3c/dom/Document;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineSetHMACOutputLength(I)V

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    check-cast v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;

    iget-object v1, p0, Lorg/apache/xml/security/utils/ElementProxy;->_constructionElement:Lorg/w3c/dom/Element;

    invoke-virtual {v0, v1}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->engineAddContextToElement(Lorg/w3c/dom/Element;)V

    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/exceptions/XMLSecurityException;
        }
    .end annotation

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/algorithms/Algorithm;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->getURI()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->getImplementingClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Create URI \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" class \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    iget-object v2, p0, Lorg/apache/xml/security/utils/ElementProxy;->_constructionElement:Lorg/w3c/dom/Element;

    invoke-virtual {v0, v2}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineGetContextFromElement(Lorg/w3c/dom/Element;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    return-void

    :catch_0
    move-exception v0

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v1, v3, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v1

    :catch_1
    move-exception v0

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v1, v3, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v0

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v1, v3, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v1
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static getImplementingClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_algorithmHash:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_algorithmHash:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    goto :goto_0
.end method

.method public static providerInit()V
    .locals 2

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    if-nez v0, :cond_2

    const-string v0, "org.apache.xml.security.algorithms.SignatureAlgorithm"

    invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    :cond_0
    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    const-string v1, "Init() called"

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_alreadyInitialized:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_algorithmHash:Ljava/util/HashMap;

    const/4 v0, 0x1

    sput-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_alreadyInitialized:Z

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->class$org$apache$xml$security$algorithms$SignatureAlgorithm:Ljava/lang/Class;

    goto :goto_0
.end method

.method public static register(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;,
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->log:Lorg/apache/commons/logging/Log;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Try to register "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    invoke-static {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->getImplementingClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p0, v1, v3

    aput-object v0, v1, v4

    new-instance v0, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;

    const-string v2, "algorithm.alreadyRegistered"

    invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    :cond_1
    :try_start_0
    sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_algorithmHash:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p0, v1, v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_1
    move-exception v0

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p0, v1, v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;

    const-string v3, "algorithms.NoSuchAlgorithm"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2
.end method


# virtual methods
.method public getBaseLocalName()Ljava/lang/String;
    .locals 1

    const-string v0, "SignatureMethod"

    return-object v0
.end method

.method public getBaseNamespace()Ljava/lang/String;
    .locals 1

    const-string v0, "http://www.w3.org/2000/09/xmldsig#"

    return-object v0
.end method

.method public getJCEAlgorithmString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineGetJCEAlgorithmString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJCEProviderName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineGetJCEProviderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getURI()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/xml/security/utils/ElementProxy;->_constructionElement:Lorg/w3c/dom/Element;

    const/4 v1, 0x0

    const-string v2, "Algorithm"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initSign(Ljava/security/Key;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineInitSign(Ljava/security/Key;)V

    return-void
.end method

.method public initSign(Ljava/security/Key;Ljava/security/SecureRandom;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineInitSign(Ljava/security/Key;Ljava/security/SecureRandom;)V

    return-void
.end method

.method public initSign(Ljava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineInitSign(Ljava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    return-void
.end method

.method public initVerify(Ljava/security/Key;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineInitVerify(Ljava/security/Key;)V

    return-void
.end method

.method public setParameter(Ljava/security/spec/AlgorithmParameterSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineSetParameter(Ljava/security/spec/AlgorithmParameterSpec;)V

    return-void
.end method

.method public sign()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineSign()[B

    move-result-object v0

    return-object v0
.end method

.method public update(B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineUpdate(B)V

    return-void
.end method

.method public update([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineUpdate([B)V

    return-void
.end method

.method public update([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineUpdate([BII)V

    return-void
.end method

.method public verify([B)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/signature/XMLSignatureException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->_signatureAlgorithm:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;

    invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->engineVerify([B)Z

    move-result v0

    return v0
.end method
