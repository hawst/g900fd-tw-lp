.class public interface abstract Lorg/apache/xml/security/encryption/CipherData;
.super Ljava/lang/Object;


# static fields
.field public static final REFERENCE_TYPE:I = 0x2

.field public static final VALUE_TYPE:I = 0x1


# virtual methods
.method public abstract getCipherReference()Lorg/apache/xml/security/encryption/CipherReference;
.end method

.method public abstract getCipherValue()Lorg/apache/xml/security/encryption/CipherValue;
.end method

.method public abstract getDataType()I
.end method

.method public abstract setCipherReference(Lorg/apache/xml/security/encryption/CipherReference;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation
.end method

.method public abstract setCipherValue(Lorg/apache/xml/security/encryption/CipherValue;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/encryption/XMLEncryptionException;
        }
    .end annotation
.end method
