.class public Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;
.super Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;


# static fields
.field static class$org$apache$xml$security$keys$keyresolver$implementations$X509SubjectNameResolver:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# instance fields
.field private _x509childNodes:[Lorg/w3c/dom/Element;

.field private _x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$X509SubjectNameResolver:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.keys.keyresolver.implementations.X509SubjectNameResolver"

    invoke-static {v0}, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$X509SubjectNameResolver:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->class$org$apache$xml$security$keys$keyresolver$implementations$X509SubjectNameResolver:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverSpi;-><init>()V

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    iput-object v0, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public engineCanResolve(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Z
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Can I resolve "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_0
    const-string v1, "X509Data"

    invoke-static {p1, v1}, Lorg/apache/xml/security/utils/XMLUtils;->elementIsInSignatureSpace(Lorg/w3c/dom/Element;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, "I can\'t"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_1
    const-string v1, "X509SubjectName"

    invoke-static {p1, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectDsNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    array-length v1, v1

    if-lez v1, :cond_2

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v1, "Yes Sir, I can"

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, "I can\'t"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public engineResolvePublicKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/PublicKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->engineResolveX509Certificate(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public engineResolveSecretKey(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljavax/crypto/SecretKey;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public engineResolveX509Certificate(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Ljava/security/cert/X509Certificate;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v3, 0x0

    :try_start_0
    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    if-nez v1, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->engineCanResolve(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/keys/storage/StorageResolver;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p3, :cond_2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "X509SubjectName"

    aput-object v2, v0, v1

    new-instance v1, Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;

    const-string v2, "KeyResolver.needStorageResolver"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, ""

    invoke-interface {v0, v2, v1}, Lorg/apache/commons/logging/Log;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    throw v1
    :try_end_0
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, "XMLSecurityException"

    invoke-interface {v1, v2, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;Ljava/lang/Throwable;)V

    new-instance v1, Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;

    const-string v2, "generic.EmptyMessage"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/keys/keyresolver/KeyResolverException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :cond_2
    :try_start_1
    iget-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    array-length v1, v1

    new-array v1, v1, [Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    iput-object v1, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    move v1, v3

    :goto_1
    iget-object v2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    new-instance v4, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    iget-object v5, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childNodes:[Lorg/w3c/dom/Element;

    aget-object v5, v5, v1

    invoke-direct {v4, v5, p2}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    aput-object v4, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p3}, Lorg/apache/xml/security/keys/storage/StorageResolver;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lorg/apache/xml/security/keys/storage/StorageResolver;->next()Ljava/security/cert/X509Certificate;

    move-result-object v1

    new-instance v4, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v2

    invoke-direct {v4, v2, v1}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;-><init>(Lorg/w3c/dom/Document;Ljava/security/cert/X509Certificate;)V

    sget-object v2, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Found Certificate SN: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v4}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->getSubjectName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    move v2, v3

    :goto_2
    iget-object v5, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    array-length v5, v5

    if-ge v2, v5, :cond_3

    sget-object v5, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Found Element SN:     "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->getSubjectName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    iget-object v5, p0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->_x509childObject:[Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v0, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v2, "match !!! "

    invoke-interface {v0, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_4
    sget-object v5, Lorg/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver;->log:Lorg/apache/commons/logging/Log;

    const-string v6, "no match..."

    invoke-interface {v5, v6}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method
