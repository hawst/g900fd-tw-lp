.class public Lorg/apache/xml/security/transforms/implementations/FuncHere;
.super Lorg/apache/xpath/functions/Function;


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/xpath/functions/Function;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/xpath/XPathContext;)Lorg/apache/xpath/objects/XObject;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Lorg/apache/xpath/XPathContext;->getOwnerObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Node;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lorg/apache/xpath/XPathContext;->getDTMHandleFromNode(Lorg/w3c/dom/Node;)I

    move-result v2

    invoke-virtual {p1}, Lorg/apache/xpath/XPathContext;->getCurrentNode()I

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/xpath/XPathContext;->getDTM(I)Lorg/apache/xml/dtm/DTM;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/xml/dtm/DTM;->getDocument()I

    move-result v5

    const/4 v6, -0x1

    if-ne v6, v5, :cond_1

    const-string v5, "ER_CONTEXT_HAS_NO_OWNERDOC"

    invoke-virtual {p0, p1, v5, v1}, Lorg/apache/xpath/Expression;->error(Lorg/apache/xpath/XPathContext;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-interface {v4, v3}, Lorg/apache/xml/dtm/DTM;->getNode(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/xml/security/utils/XMLUtils;->getOwnerDocument(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Document;

    move-result-object v1

    invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->getOwnerDocument(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Document;

    move-result-object v0

    if-eq v1, v0, :cond_2

    new-instance v0, Ljavax/xml/transform/TransformerException;

    const-string v1, "xpath.funcHere.documentsDiffer"

    invoke-static {v1}, Lorg/apache/xml/security/utils/I18n;->translate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/xml/transform/TransformerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lorg/apache/xpath/objects/XNodeSet;

    invoke-virtual {p1}, Lorg/apache/xpath/XPathContext;->getDTMManager()Lorg/apache/xml/dtm/DTMManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/xpath/objects/XNodeSet;-><init>(Lorg/apache/xml/dtm/DTMManager;)V

    invoke-virtual {v0}, Lorg/apache/xpath/objects/XNodeSet;->mutableNodeset()Lorg/apache/xpath/NodeSetDTM;

    move-result-object v1

    invoke-interface {v4, v2}, Lorg/apache/xml/dtm/DTM;->getNodeType(I)S

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {v1}, Lorg/apache/xpath/NodeSetDTM;->detach()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1, v2}, Lorg/apache/xpath/NodeSetDTM;->addNode(I)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {v1, v2}, Lorg/apache/xpath/NodeSetDTM;->addNode(I)V

    goto :goto_1

    :pswitch_3
    invoke-interface {v4, v2}, Lorg/apache/xml/dtm/DTM;->getParent(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/xpath/NodeSetDTM;->addNode(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public fixupVariables(Ljava/util/Vector;I)V
    .locals 0

    return-void
.end method
