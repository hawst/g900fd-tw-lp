.class public Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/xml/sax/ErrorHandler;


# static fields
.field static class$org$apache$xml$security$utils$IgnoreAllErrorHandler:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;

.field static final throwExceptions:Z

.field static final warnOnExceptions:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->class$org$apache$xml$security$utils$IgnoreAllErrorHandler:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.utils.IgnoreAllErrorHandler"

    invoke-static {v0}, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->class$org$apache$xml$security$utils$IgnoreAllErrorHandler:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->log:Lorg/apache/commons/logging/Log;

    const-string v0, "org.apache.xml.security.test.warn.on.exceptions"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->warnOnExceptions:Z

    const-string v0, "org.apache.xml.security.test.throw.exceptions"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->throwExceptions:Z

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->class$org$apache$xml$security$utils$IgnoreAllErrorHandler:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public error(Lorg/xml/sax/SAXParseException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->warnOnExceptions:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->log:Lorg/apache/commons/logging/Log;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/logging/Log;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_0
    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->throwExceptions:Z

    if-eqz v0, :cond_1

    throw p1

    :cond_1
    return-void
.end method

.method public fatalError(Lorg/xml/sax/SAXParseException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->warnOnExceptions:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->log:Lorg/apache/commons/logging/Log;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_0
    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->throwExceptions:Z

    if-eqz v0, :cond_1

    throw p1

    :cond_1
    return-void
.end method

.method public warning(Lorg/xml/sax/SAXParseException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->warnOnExceptions:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->log:Lorg/apache/commons/logging/Log;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_0
    sget-boolean v0, Lorg/apache/xml/security/utils/IgnoreAllErrorHandler;->throwExceptions:Z

    if-eqz v0, :cond_1

    throw p1

    :cond_1
    return-void
.end method
