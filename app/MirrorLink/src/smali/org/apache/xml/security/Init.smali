.class public Lorg/apache/xml/security/Init;
.super Ljava/lang/Object;


# static fields
.field public static final CONF_NS:Ljava/lang/String; = "http://www.xmlsecurity.org/NS/#configuration"

.field private static _alreadyInitialized:Z

.field static class$org$apache$xml$security$Init:Ljava/lang/Class;

.field static log:Lorg/apache/commons/logging/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/xml/security/Init;->class$org$apache$xml$security$Init:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.xml.security.Init"

    invoke-static {v0}, Lorg/apache/xml/security/Init;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/Init;->class$org$apache$xml$security$Init:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/xml/security/Init;->_alreadyInitialized:Z

    return-void

    :cond_0
    sget-object v0, Lorg/apache/xml/security/Init;->class$org$apache$xml$security$Init:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static declared-synchronized init()V
    .locals 57

    const-class v37, Lorg/apache/xml/security/Init;

    monitor-enter v37

    :try_start_0
    sget-boolean v3, Lorg/apache/xml/security/Init;->_alreadyInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit v37

    return-void

    :cond_1
    const-wide/16 v35, 0x0

    const-wide/16 v33, 0x0

    const-wide/16 v31, 0x0

    const-wide/16 v29, 0x0

    const-wide/16 v27, 0x0

    const-wide/16 v25, 0x0

    const-wide/16 v23, 0x0

    const-wide/16 v21, 0x0

    const-wide/16 v19, 0x0

    const-wide/16 v17, 0x0

    const/4 v3, 0x1

    :try_start_1
    sput-boolean v3, Lorg/apache/xml/security/Init;->_alreadyInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v42

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v44

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->setValidating(Z)V

    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v4

    const-string v3, "org.apache.xml.security.resource.config"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "org.apache.xml.security.Init"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    if-eqz v3, :cond_4

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v46

    const-wide/16 v15, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v48

    :try_start_3
    invoke-static {}, Lorg/apache/xml/security/keys/KeyInfo;->init()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v50

    const-wide/16 v13, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v5, 0x0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_2

    const-string v4, "Configuration"

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_2
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v4

    :goto_3
    if-eqz v4, :cond_1e

    instance-of v3, v4, Lorg/w3c/dom/Element;

    if-nez v3, :cond_6

    :cond_3
    :goto_4
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v4

    goto :goto_3

    :cond_4
    const-string v3, "resource/config.xml"

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v3

    :try_start_5
    sget-object v4, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v5, "Bad: "

    invoke-interface {v4, v5, v3}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V

    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v37

    throw v3

    :cond_5
    :try_start_6
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v3

    goto :goto_2

    :cond_6
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v52

    const-string v3, "ResourceBundles"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    move-object v0, v4

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v3, v0

    const-string v35, "defaultLanguageCode"

    move-object/from16 v0, v35

    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->getAttributeNode(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v35

    const-string v36, "defaultCountryCode"

    move-object/from16 v0, v36

    invoke-interface {v3, v0}, Lorg/w3c/dom/Element;->getAttributeNode(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v36

    if-nez v35, :cond_9

    const/4 v3, 0x0

    move-object/from16 v35, v3

    :goto_5
    if-nez v36, :cond_a

    const/4 v3, 0x0

    :goto_6
    move-object/from16 v0, v35

    invoke-static {v0, v3}, Lorg/apache/xml/security/utils/I18n;->init(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v35

    :cond_7
    const-string v3, "CanonicalizationMethods"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v33

    invoke-static {}, Lorg/apache/xml/security/c14n/Canonicalizer;->init()V

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v31, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v32, "CanonicalizationMethod"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-static {v3, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v31

    const/4 v3, 0x0

    :goto_7
    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v3, v0, :cond_b

    aget-object v32, v31, v3

    const/16 v53, 0x0

    const-string v54, "URI"

    move-object/from16 v0, v32

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    aget-object v53, v31, v3

    const/16 v54, 0x0

    const-string v55, "JAVACLASS"

    invoke-interface/range {v53 .. v55}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v53

    :try_start_7
    invoke-static/range {v53 .. v53}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v54

    if-eqz v54, :cond_8

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Canonicalizer.register("

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ", "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ")"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-interface/range {v54 .. v55}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_8
    move-object/from16 v0, v32

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lorg/apache/xml/security/c14n/Canonicalizer;->register(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_9
    :try_start_8
    invoke-interface/range {v35 .. v35}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v35, v3

    goto/16 :goto_5

    :cond_a
    invoke-interface/range {v36 .. v36}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :catch_2
    move-exception v54

    const/16 v54, 0x2

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v32, v54, v55

    const/16 v32, 0x1

    aput-object v53, v54, v32

    sget-object v32, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v53, "algorithm.classDoesNotExist"

    invoke-static/range {v53 .. v54}, Lorg/apache/xml/security/utils/I18n;->translate(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v32

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V

    goto :goto_8

    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v31

    :cond_c
    const-string v3, "TransformAlgorithms"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-static {}, Lorg/apache/xml/security/transforms/Transform;->init()V

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v19, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v20, "TransformAlgorithm"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v3, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v19

    const/4 v3, 0x0

    :goto_9
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v3, v0, :cond_e

    aget-object v20, v19, v3

    const/16 v53, 0x0

    const-string v54, "URI"

    move-object/from16 v0, v20

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    aget-object v53, v19, v3

    const/16 v54, 0x0

    const-string v55, "JAVACLASS"

    invoke-interface/range {v53 .. v55}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v53

    :try_start_9
    invoke-static/range {v53 .. v53}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v54

    if-eqz v54, :cond_d

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Transform.register("

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ", "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ")"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-interface/range {v54 .. v55}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_d
    move-object/from16 v0, v20

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lorg/apache/xml/security/transforms/Transform;->register(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :goto_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :catch_3
    move-exception v54

    const/16 v54, 0x2

    :try_start_a
    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v20, v54, v55

    const/16 v20, 0x1

    aput-object v53, v54, v20

    sget-object v20, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v53, "algorithm.classDoesNotExist"

    invoke-static/range {v53 .. v54}, Lorg/apache/xml/security/utils/I18n;->translate(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v20

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V

    goto :goto_a

    :catch_4
    move-exception v20

    sget-object v20, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v53, "Not able to found dependecies for algorithm, I\'m keep working."

    move-object/from16 v0, v20

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    goto :goto_a

    :cond_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    :cond_f
    const-string v3, "JCEAlgorithmMappings"

    move-object/from16 v0, v52

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    move-object v0, v4

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v3, v0

    invoke-static {v3}, Lorg/apache/xml/security/algorithms/JCEMapper;->init(Lorg/w3c/dom/Element;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v29

    :cond_10
    const-string v3, "SignatureAlgorithms"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->providerInit()V

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v21, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v22, "SignatureAlgorithm"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v3, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v21

    const/4 v3, 0x0

    :goto_b
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v3, v0, :cond_12

    aget-object v22, v21, v3

    const/16 v53, 0x0

    const-string v54, "URI"

    move-object/from16 v0, v22

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    aget-object v53, v21, v3

    const/16 v54, 0x0

    const-string v55, "JAVACLASS"

    invoke-interface/range {v53 .. v55}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v53

    :try_start_b
    invoke-static/range {v53 .. v53}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v54

    if-eqz v54, :cond_11

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "SignatureAlgorithm.register("

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ", "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ")"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v55

    invoke-interface/range {v54 .. v55}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_11
    move-object/from16 v0, v22

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->register(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :goto_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :catch_5
    move-exception v54

    const/16 v54, 0x2

    :try_start_c
    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v22, v54, v55

    const/16 v22, 0x1

    aput-object v53, v54, v22

    sget-object v22, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v53, "algorithm.classDoesNotExist"

    invoke-static/range {v53 .. v54}, Lorg/apache/xml/security/utils/I18n;->translate(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v22

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V

    goto :goto_c

    :cond_12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    :cond_13
    const-string v3, "ResourceResolvers"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    invoke-static {}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->init()V

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v53, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v54, "Resolver"

    move-object/from16 v0, v53

    move-object/from16 v1, v54

    invoke-static {v3, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v53

    const/4 v3, 0x0

    :goto_d
    move-object/from16 v0, v53

    array-length v0, v0

    move/from16 v54, v0

    move/from16 v0, v54

    if-ge v3, v0, :cond_16

    aget-object v7, v53, v3

    const/4 v8, 0x0

    const-string v54, "JAVACLASS"

    move-object/from16 v0, v54

    invoke-interface {v7, v8, v0}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aget-object v8, v53, v3

    const/16 v54, 0x0

    const-string v55, "DESCRIPTION"

    move-object/from16 v0, v54

    move-object/from16 v1, v55

    invoke-interface {v8, v0, v1}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_15

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v54

    if-lez v54, :cond_15

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v54

    if-eqz v54, :cond_14

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Register Resolver: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ": "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v54

    invoke-interface {v0, v8}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_14
    :goto_e
    :try_start_d
    invoke-static {v7}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->register(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :goto_f
    :try_start_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_15
    sget-object v8, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v8}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_14

    sget-object v8, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Register Resolver: "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    const-string v55, ": For unknown purposes"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-interface {v8, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    goto :goto_e

    :catch_6
    move-exception v8

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Cannot register:"

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v55, " perhaps some needed jars are not installed"

    move-object/from16 v0, v55

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v54

    invoke-interface {v0, v7, v8}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_f

    :cond_16
    const-string v3, "KeyResolver"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-static {}, Lorg/apache/xml/security/keys/keyresolver/KeyResolver;->init()V

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v27, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v28, "Resolver"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v3, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v27

    const/4 v3, 0x0

    :goto_10
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v3, v0, :cond_19

    aget-object v28, v27, v3

    const/16 v53, 0x0

    const-string v54, "JAVACLASS"

    move-object/from16 v0, v28

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aget-object v53, v27, v3

    const/16 v54, 0x0

    const-string v55, "DESCRIPTION"

    invoke-interface/range {v53 .. v55}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    if-eqz v53, :cond_18

    invoke-virtual/range {v53 .. v53}, Ljava/lang/String;->length()I

    move-result v54

    if-lez v54, :cond_18

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v54

    if-eqz v54, :cond_17

    sget-object v54, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v55, Ljava/lang/StringBuffer;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V

    const-string v56, "Register Resolver: "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    const-string v56, ": "

    invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v53

    move-object/from16 v0, v54

    move-object/from16 v1, v53

    invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_17
    :goto_11
    invoke-static/range {v28 .. v28}, Lorg/apache/xml/security/keys/keyresolver/KeyResolver;->register(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_18
    sget-object v53, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v53 .. v53}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v53

    if-eqz v53, :cond_17

    sget-object v53, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Register Resolver: "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    const-string v55, ": For unknown purposes"

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-interface/range {v53 .. v54}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    goto :goto_11

    :cond_19
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    :cond_1a
    const-string v3, "PrefixMappings"

    move-object/from16 v0, v52

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v25

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface {v3}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_1b

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    const-string v5, "Now I try to bind prefixes:"

    invoke-interface {v3, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_1b
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v3

    const-string v5, "http://www.xmlsecurity.org/NS/#configuration"

    const-string v6, "PrefixMapping"

    invoke-static {v3, v5, v6}, Lorg/apache/xml/security/utils/XMLUtils;->selectNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v5

    const/4 v3, 0x0

    :goto_12
    array-length v6, v5

    if-ge v3, v6, :cond_1d

    aget-object v6, v5, v3

    const/16 v52, 0x0

    const-string v53, "namespace"

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-interface {v6, v0, v1}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aget-object v52, v5, v3

    const/16 v53, 0x0

    const-string v54, "prefix"

    invoke-interface/range {v52 .. v54}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    sget-object v53, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v53 .. v53}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v53

    if-eqz v53, :cond_1c

    sget-object v53, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v54, Ljava/lang/StringBuffer;

    invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V

    const-string v55, "Now I try to bind "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    const-string v55, " to "

    invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    move-object/from16 v0, v54

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v54

    invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v54

    invoke-interface/range {v53 .. v54}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_1c
    move-object/from16 v0, v52

    invoke-static {v6, v0}, Lorg/apache/xml/security/utils/ElementProxy;->setDefaultPrefix(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_1d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    goto/16 :goto_4

    :cond_1e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-object v52, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    invoke-interface/range {v52 .. v52}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v52

    if-eqz v52, :cond_0

    sget-object v52, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v53, Ljava/lang/StringBuffer;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuffer;-><init>()V

    const-string v54, "XX_init                             "

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v53

    sub-long v3, v3, v38

    long-to-int v3, v3

    move-object/from16 v0, v53

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v52

    invoke-interface {v0, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v38, "  XX_prng                           "

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v38, v42, v40

    move-wide/from16 v0, v38

    long-to-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v38, " ms"

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v38, "  XX_parsing                        "

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v38, v46, v44

    move-wide/from16 v0, v38

    long-to-int v0, v0

    move/from16 v38, v0

    move/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v38, " ms"

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v38, "  XX_configure_i18n                 "

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v15, v35, v15

    long-to-int v15, v15

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v15, " ms"

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "  XX_configure_reg_c14n             "

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v15, v31, v33

    long-to-int v15, v15

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v15, " ms"

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "  XX_configure_reg_jcemapper        "

    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v11, v29, v11

    long-to-int v11, v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v11, " ms"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "  XX_configure_reg_keyInfo          "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v11, v50, v48

    long-to-int v11, v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v11, " ms"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "  XX_configure_reg_keyResolver      "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v11, v27, v17

    long-to-int v11, v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v11, " ms"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "  XX_configure_reg_prefixes         "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v5, v5, v25

    long-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "  XX_configure_reg_resourceresolver "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v5, v7, v23

    long-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "  XX_configure_reg_sigalgos         "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v5, v21, v9

    long-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v3, Lorg/apache/xml/security/Init;->log:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "  XX_configure_reg_transforms       "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sub-long v5, v19, v13

    long-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0
.end method

.method public static final isInitialized()Z
    .locals 1

    sget-boolean v0, Lorg/apache/xml/security/Init;->_alreadyInitialized:Z

    return v0
.end method
