.class public interface abstract Lorg/apache/xml/security/encryption/EncryptedKey;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/apache/xml/security/encryption/EncryptedType;


# virtual methods
.method public abstract getCarriedName()Ljava/lang/String;
.end method

.method public abstract getRecipient()Ljava/lang/String;
.end method

.method public abstract getReferenceList()Lorg/apache/xml/security/encryption/ReferenceList;
.end method

.method public abstract setCarriedName(Ljava/lang/String;)V
.end method

.method public abstract setRecipient(Ljava/lang/String;)V
.end method

.method public abstract setReferenceList(Lorg/apache/xml/security/encryption/ReferenceList;)V
.end method
