.class public interface abstract Lorg/apache/xml/security/encryption/AgreementMethod;
.super Ljava/lang/Object;


# virtual methods
.method public abstract addAgreementMethodInformation(Lorg/w3c/dom/Element;)V
.end method

.method public abstract getAgreementMethodInformation()Ljava/util/Iterator;
.end method

.method public abstract getAlgorithm()Ljava/lang/String;
.end method

.method public abstract getKANonce()[B
.end method

.method public abstract getOriginatorKeyInfo()Lorg/apache/xml/security/keys/KeyInfo;
.end method

.method public abstract getRecipientKeyInfo()Lorg/apache/xml/security/keys/KeyInfo;
.end method

.method public abstract revoveAgreementMethodInformation(Lorg/w3c/dom/Element;)V
.end method

.method public abstract setKANonce([B)V
.end method

.method public abstract setOriginatorKeyInfo(Lorg/apache/xml/security/keys/KeyInfo;)V
.end method

.method public abstract setRecipientKeyInfo(Lorg/apache/xml/security/keys/KeyInfo;)V
.end method
