.class public Lcom/samsung/pmr/PersonalMessageControlPoint;
.super Ljava/lang/Object;
.source "PersonalMessageControlPoint.java"

# interfaces
.implements Lcom/samsung/upnp/IControlPoint;


# static fields
.field private static final DEFAULT_EXPIRED_DEVICE_MONITORING_INTERVAL:I = 0x3c

.field private static final DEFAULT_SSDP_PORT:I = 0x1f48


# instance fields
.field private activityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private devNodeList:Lcom/samsung/xml/NodeList;

.field deviceChangeListenerList:Lcom/samsung/util/ListenerList;

.field private deviceDisposer:Lcom/samsung/upnp/device/Disposer;

.field private deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

.field private deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

.field private expiredDeviceMonitoringInterval:J

.field private mutex:Lcom/samsung/util/Mutex;

.field private nmprMode:Z

.field private searchMx:I

.field private ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

.field private ssdpPort:I

.field private ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 85
    invoke-static {}, Lcom/samsung/upnp/UPnP;->initialize()V

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    const/16 v0, 0x1f48

    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;-><init>(I)V

    .line 108
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "ssdpPort"    # I

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Lcom/samsung/util/Mutex;

    invoke-direct {v0}, Lcom/samsung/util/Mutex;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->mutex:Lcom/samsung/util/Mutex;

    .line 130
    iput v2, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpPort:I

    .line 160
    new-instance v0, Lcom/samsung/xml/NodeList;

    invoke-direct {v0}, Lcom/samsung/xml/NodeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->activityList:Ljava/util/ArrayList;

    .line 390
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    .line 415
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    .line 443
    new-instance v0, Lcom/samsung/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    .line 515
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->searchMx:I

    .line 94
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    invoke-direct {v0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    .line 95
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    invoke-direct {v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;-><init>()V

    iput-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    .line 97
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setSSDPPort(I)V

    .line 99
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 100
    const-wide/16 v0, 0x3c

    invoke-virtual {p0, v0, v1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setExpiredDeviceMonitoringInterval(J)V

    .line 102
    invoke-virtual {p0, v2}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setNMPRMode(Z)V

    .line 103
    return-void
.end method

.method private addActivityDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method private declared-synchronized addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 14
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v11

    .line 183
    .local v11, "usn":Ljava/lang/String;
    invoke-static {v11}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 185
    .local v8, "udn":Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->addActivityDevice(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 188
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Device;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :goto_0
    monitor-exit p0

    return-void

    .line 193
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 194
    .local v2, "location":Ljava/lang/String;
    const/4 v10, 0x0

    .line 196
    .local v10, "urlIn":Ljava/io/InputStream;
    :try_start_2
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 197
    .local v3, "locationUrl":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v9

    check-cast v9, Ljava/net/HttpURLConnection;

    .line 198
    .local v9, "urlCon":Ljava/net/HttpURLConnection;
    const/16 v12, 0xbb8

    invoke-virtual {v9, v12}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 199
    const-string v12, "GET"

    invoke-virtual {v9, v12}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 200
    const-string v12, "User-Agent"

    const-string v13, "SEC_HHP_01099990000/1.0"

    invoke-virtual {v9, v12, v13}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    .line 203
    invoke-static {}, Lcom/samsung/upnp/UPnP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v4

    .line 205
    .local v4, "parser":Lcom/samsung/xml/Parser;
    invoke-virtual {v4, v10}, Lcom/samsung/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/xml/Node;

    move-result-object v7

    .line 206
    .local v7, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v6

    .line 207
    .local v6, "rootDev":Lcom/samsung/upnp/Device;
    invoke-virtual {v9}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 209
    if-nez v6, :cond_2

    .line 210
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 231
    if-eqz v10, :cond_1

    .line 233
    :try_start_3
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 237
    :cond_1
    :goto_1
    :try_start_4
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 182
    .end local v0    # "dev":Lcom/samsung/upnp/Device;
    .end local v2    # "location":Ljava/lang/String;
    .end local v3    # "locationUrl":Ljava/net/URL;
    .end local v4    # "parser":Lcom/samsung/xml/Parser;
    .end local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .end local v7    # "rootNode":Lcom/samsung/xml/Node;
    .end local v8    # "udn":Ljava/lang/String;
    .end local v9    # "urlCon":Ljava/net/HttpURLConnection;
    .end local v10    # "urlIn":Ljava/io/InputStream;
    .end local v11    # "usn":Ljava/lang/String;
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 234
    .restart local v0    # "dev":Lcom/samsung/upnp/Device;
    .restart local v2    # "location":Ljava/lang/String;
    .restart local v3    # "locationUrl":Ljava/net/URL;
    .restart local v4    # "parser":Lcom/samsung/xml/Parser;
    .restart local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .restart local v7    # "rootNode":Lcom/samsung/xml/Node;
    .restart local v8    # "udn":Ljava/lang/String;
    .restart local v9    # "urlCon":Ljava/net/HttpURLConnection;
    .restart local v10    # "urlIn":Ljava/io/InputStream;
    .restart local v11    # "usn":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/io/IOException;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 213
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_6
    invoke-virtual {v6, p1}, Lcom/samsung/upnp/Device;->setSSDPPacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 214
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    .line 215
    invoke-direct {p0, v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;->addDevice(Lcom/samsung/xml/Node;)V

    .line 221
    invoke-virtual {p0, v6}, Lcom/samsung/pmr/PersonalMessageControlPoint;->performAddDeviceListener(Lcom/samsung/upnp/Device;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 231
    if-eqz v10, :cond_3

    .line 233
    :try_start_7
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 237
    :cond_3
    :goto_2
    :try_start_8
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 223
    .end local v3    # "locationUrl":Ljava/net/URL;
    .end local v4    # "parser":Lcom/samsung/xml/Parser;
    .end local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .end local v7    # "rootNode":Lcom/samsung/xml/Node;
    .end local v9    # "urlCon":Ljava/net/HttpURLConnection;
    :catch_1
    move-exception v1

    .line 224
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 231
    if-eqz v10, :cond_4

    .line 233
    :try_start_a
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 237
    :cond_4
    :goto_3
    :try_start_b
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 234
    :catch_2
    move-exception v1

    .line 235
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_3

    .line 226
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 227
    .local v5, "pe":Lcom/samsung/xml/ParserException;
    :try_start_c
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/String;)V

    .line 228
    invoke-static {v5}, Lcom/samsung/api/Debugs;->warning(Ljava/lang/Exception;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 231
    if-eqz v10, :cond_5

    .line 233
    :try_start_d
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 237
    :cond_5
    :goto_4
    :try_start_e
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :catch_4
    move-exception v1

    .line 235
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_4

    .line 230
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "pe":Lcom/samsung/xml/ParserException;
    :catchall_1
    move-exception v12

    .line 231
    if-eqz v10, :cond_6

    .line 233
    :try_start_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 237
    :cond_6
    :goto_5
    :try_start_10
    invoke-direct {p0, v8}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    .line 238
    throw v12

    .line 234
    :catch_5
    move-exception v1

    .line 235
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 234
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v3    # "locationUrl":Ljava/net/URL;
    .restart local v4    # "parser":Lcom/samsung/xml/Parser;
    .restart local v6    # "rootDev":Lcom/samsung/upnp/Device;
    .restart local v7    # "rootNode":Lcom/samsung/xml/Node;
    .restart local v9    # "urlCon":Ljava/net/HttpURLConnection;
    :catch_6
    move-exception v1

    .line 235
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_2
.end method

.method private addDevice(Lcom/samsung/xml/Node;)V
    .locals 1
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/xml/NodeList;->add(Ljava/lang/Object;)Z

    .line 165
    return-void
.end method

.method private getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;
    .locals 3
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    const/4 v1, 0x0

    .line 243
    if-nez p1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-object v1

    .line 245
    :cond_1
    const-string v2, "device"

    invoke-virtual {p1, v2}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 246
    .local v0, "devNode":Lcom/samsung/xml/Node;
    if-eqz v0, :cond_0

    .line 248
    new-instance v1, Lcom/samsung/upnp/Device;

    invoke-direct {v1, p1, v0}, Lcom/samsung/upnp/Device;-><init>(Lcom/samsung/xml/Node;Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method private getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpNotifySocketList:Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    return-object v0
.end method

.method private getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpSearchResponseSocketList:Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    return-object v0
.end method

.method private isActivityDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 13
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 646
    invoke-static {}, Lcom/samsung/api/Debugs;->off()V

    .line 647
    new-instance v7, Lcom/samsung/pmr/PersonalMessageControlPoint;

    invoke-direct {v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;-><init>()V

    .line 648
    .local v7, "pmcp":Lcom/samsung/pmr/PersonalMessageControlPoint;
    invoke-virtual {v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;->start()Z

    .line 650
    new-instance v10, Lcom/samsung/pmr/PersonalMessageControlPoint$1;

    invoke-direct {v10}, Lcom/samsung/pmr/PersonalMessageControlPoint$1;-><init>()V

    invoke-virtual {v7, v10}, Lcom/samsung/pmr/PersonalMessageControlPoint;->addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)V

    .line 663
    new-instance v5, Ljava/io/InputStreamReader;

    sget-object v10, Ljava/lang/System;->in:Ljava/io/InputStream;

    invoke-direct {v5, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 664
    .local v5, "isr":Ljava/io/InputStreamReader;
    const/16 v10, 0x100

    new-array v1, v10, [C

    .line 666
    .local v1, "buf":[C
    :cond_0
    :goto_0
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 668
    :try_start_0
    invoke-virtual {v5, v1}, Ljava/io/InputStreamReader;->read([C)I

    move-result v8

    .line 669
    .local v8, "read":I
    const-string v10, "PersonalMessageControlPoint"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "read : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/api/Debugs;->debug(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v1}, Ljava/lang/String;-><init>([C)V

    .line 672
    .local v9, "temp":Ljava/lang/String;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v10, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 673
    const-string v10, "q"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 674
    invoke-virtual {v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;->stop()Z

    .line 675
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/System;->exit(I)V

    .line 677
    :cond_1
    const-string v10, "search"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 678
    const-string v10, "urn:samsung.com:device:PersonalMessageReceiver:1"

    invoke-virtual {v7, v10}, Lcom/samsung/pmr/PersonalMessageControlPoint;->search(Ljava/lang/String;)V

    .line 680
    :cond_2
    const-string v10, "send"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 681
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "hello world"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 682
    invoke-virtual {v7}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v3

    .line 683
    .local v3, "dlist":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/upnp/Device;

    .line 684
    .local v2, "dev":Lcom/samsung/upnp/Device;
    const-string v6, "<Message><Category>SMS</Category><DisplayType>Minimum</DisplayType><ReceiveTime><Date>2008-01-30</Date><Time>23:10:00</Time></ReceiveTime><Receiver><Name>hhh</Name><Number>1111</Number></Receiver><Sender><Name>aaaa</Name><Number>2222</Number></Sender></Message>"

    .line 690
    .local v6, "msg":Ljava/lang/String;
    const-string v11, "AddMessage"

    invoke-virtual {v2, v11}, Lcom/samsung/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/upnp/Action;

    move-result-object v0

    .line 691
    .local v0, "action":Lcom/samsung/upnp/Action;
    if-eqz v0, :cond_3

    .line 692
    const-string v11, "MessageID"

    const-string v12, "1111"

    invoke-virtual {v0, v11, v12}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v11, "MessageType"

    const-string v12, "text/xml; charset=&quot;utf-8&quot;"

    invoke-virtual {v0, v11, v12}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v11, "Message"

    invoke-virtual {v0, v11, v6}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    invoke-virtual {v0}, Lcom/samsung/upnp/Action;->postControlAction()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 701
    .end local v0    # "action":Lcom/samsung/upnp/Action;
    .end local v2    # "dev":Lcom/samsung/upnp/Device;
    .end local v3    # "dlist":Lcom/samsung/upnp/DeviceList;
    .end local v6    # "msg":Ljava/lang/String;
    .end local v8    # "read":I
    .end local v9    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 703
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private removeActivityDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->activityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 178
    return-void
.end method

.method private removeDevice(Lcom/samsung/upnp/Device;)V
    .locals 1
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 316
    if-nez p1, :cond_0

    .line 319
    :goto_0
    return-void

    .line 318
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/Device;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDevice(Lcom/samsung/xml/Node;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 4
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 334
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v2

    if-nez v2, :cond_0

    .line 342
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "udn":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "remove udn: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 341
    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDeviceByUdn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/xml/Node;)V
    .locals 2
    .param p1, "rootNode"    # Lcom/samsung/xml/Node;

    .prologue
    .line 306
    invoke-direct {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 307
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 308
    invoke-virtual {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->performRemoveDeviceListener(Lcom/samsung/upnp/Device;)V

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v1, p1}, Lcom/samsung/xml/NodeList;->remove(Ljava/lang/Object;)Z

    .line 312
    return-void
.end method

.method private removeDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 324
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDevice(Lcom/samsung/upnp/Device;)V

    .line 325
    return-void
.end method

.method private removeDeviceByUdn(Ljava/lang/String;)V
    .locals 1
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 329
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDeviceByUdn(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 330
    .local v0, "dev":Lcom/samsung/upnp/Device;
    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDevice(Lcom/samsung/upnp/Device;)V

    .line 331
    return-void
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 448
    return-void
.end method

.method public addNotifyListener(Lcom/samsung/upnp/device/NotifyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/NotifyListener;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 395
    return-void
.end method

.method public addSearchResponseListener(Lcom/samsung/upnp/device/SearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/SearchResponseListener;

    .prologue
    .line 419
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 420
    return-void
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 267
    iget-object v5, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/xml/NodeList;->size()I

    move-result v3

    .line 268
    .local v3, "nRoots":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 279
    const/4 v1, 0x0

    :cond_0
    :goto_1
    return-object v1

    .line 269
    :cond_1
    iget-object v5, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5, v2}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 270
    .local v4, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v4}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v1

    .line 271
    .local v1, "dev":Lcom/samsung/upnp/Device;
    if-nez v1, :cond_3

    .line 268
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 273
    :cond_3
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->isDevice(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 275
    invoke-virtual {v1, p1}, Lcom/samsung/upnp/Device;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 276
    .local v0, "cdev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 277
    goto :goto_1
.end method

.method public getDeviceByUdn(Ljava/lang/String;)Lcom/samsung/upnp/Device;
    .locals 4
    .param p1, "udn"    # Ljava/lang/String;

    .prologue
    .line 284
    iget-object v2, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v2}, Lcom/samsung/xml/NodeList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 292
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 284
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/xml/Node;

    .line 285
    .local v1, "node":Lcom/samsung/xml/Node;
    invoke-direct {p0, v1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 286
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method

.method public getDeviceDisposer()Lcom/samsung/upnp/device/Disposer;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceDisposer:Lcom/samsung/upnp/device/Disposer;

    return-object v0
.end method

.method public getDeviceList()Lcom/samsung/upnp/DeviceList;
    .locals 6

    .prologue
    .line 253
    new-instance v1, Lcom/samsung/upnp/DeviceList;

    invoke-direct {v1}, Lcom/samsung/upnp/DeviceList;-><init>()V

    .line 254
    .local v1, "devList":Lcom/samsung/upnp/DeviceList;
    iget-object v5, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/xml/NodeList;->size()I

    move-result v3

    .line 255
    .local v3, "nRoots":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 262
    return-object v1

    .line 256
    :cond_0
    iget-object v5, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->devNodeList:Lcom/samsung/xml/NodeList;

    invoke-virtual {v5, v2}, Lcom/samsung/xml/NodeList;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 257
    .local v4, "rootNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v4}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Lcom/samsung/xml/Node;)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 258
    .local v0, "dev":Lcom/samsung/upnp/Device;
    if-nez v0, :cond_1

    .line 255
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    :cond_1
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getExpiredDeviceMonitoringInterval()J
    .locals 2

    .prologue
    .line 373
    iget-wide v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->expiredDeviceMonitoringInterval:J

    return-wide v0
.end method

.method public getSSDPPort()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpPort:I

    return v0
.end method

.method public getSearchMx()I
    .locals 1

    .prologue
    .line 518
    iget v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->searchMx:I

    return v0
.end method

.method public hasDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 297
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNMPRMode()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->nmprMode:Z

    return v0
.end method

.method public isValidAlivePacket(Lcom/samsung/upnp/ssdp/SSDPPacket;)Z
    .locals 1
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 478
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isPMRDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    const/4 v0, 0x0

    .line 480
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public lock()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->mutex:Lcom/samsung/util/Mutex;

    invoke-virtual {v0}, Lcom/samsung/util/Mutex;->lock()V

    .line 119
    return-void
.end method

.method public notifyReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 485
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 486
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "udn":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isPMRDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 489
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 490
    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->isActivityDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 491
    invoke-virtual {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v2

    if-nez v2, :cond_0

    .line 492
    invoke-direct {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 493
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 494
    invoke-direct {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 496
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->performNotifyListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 497
    return-void
.end method

.method public performAddDeviceListener(Lcom/samsung/upnp/Device;)V
    .locals 4
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 457
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 458
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 462
    return-void

    .line 459
    :cond_0
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/DeviceChangeListener;

    .line 460
    .local v0, "listener":Lcom/samsung/api/DeviceChangeListener;
    new-instance v3, Lcom/samsung/api/DeviceItem;

    invoke-direct {v3, p1}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    invoke-interface {v0, v3}, Lcom/samsung/api/DeviceChangeListener;->deviceAdded(Lcom/samsung/api/DeviceItem;)V

    .line 458
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public performNotifyListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 4
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 404
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 405
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 409
    return-void

    .line 406
    :cond_0
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/device/NotifyListener;

    .line 407
    .local v0, "listener":Lcom/samsung/upnp/device/NotifyListener;
    invoke-interface {v0, p1}, Lcom/samsung/upnp/device/NotifyListener;->deviceNotifyReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 405
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public performRemoveDeviceListener(Lcom/samsung/upnp/Device;)V
    .locals 4
    .param p1, "dev"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 466
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 467
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 471
    return-void

    .line 468
    :cond_0
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/api/DeviceChangeListener;

    .line 469
    .local v0, "listener":Lcom/samsung/api/DeviceChangeListener;
    new-instance v3, Lcom/samsung/api/DeviceItem;

    invoke-direct {v3, p1}, Lcom/samsung/api/DeviceItem;-><init>(Lcom/samsung/upnp/Device;)V

    invoke-interface {v0, v3}, Lcom/samsung/api/DeviceChangeListener;->deviceRemoved(Lcom/samsung/api/DeviceItem;)V

    .line 467
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public performSearchResponseListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 4
    .param p1, "ssdpPacket"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 429
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/util/ListenerList;->size()I

    move-result v1

    .line 430
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 434
    return-void

    .line 431
    :cond_0
    iget-object v3, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/upnp/device/SearchResponseListener;

    .line 432
    .local v0, "listener":Lcom/samsung/upnp/device/SearchResponseListener;
    invoke-interface {v0, p1}, Lcom/samsung/upnp/device/SearchResponseListener;->deviceSearchResponseReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 430
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public print()V
    .locals 7

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 630
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 631
    .local v1, "devCnt":I
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Device Num = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 632
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 636
    return-void

    .line 633
    :cond_0
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v0

    .line 634
    .local v0, "dev":Lcom/samsung/upnp/Device;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getLeaseTime()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/upnp/Device;->getElapsedTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 632
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public removeDeviceChangeListener(Lcom/samsung/api/DeviceChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/api/DeviceChangeListener;

    .prologue
    .line 452
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceChangeListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 453
    return-void
.end method

.method public removeExpiredDevices()V
    .locals 6

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDeviceList()Lcom/samsung/upnp/DeviceList;

    move-result-object v2

    .line 354
    .local v2, "devList":Lcom/samsung/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/upnp/DeviceList;->size()I

    move-result v1

    .line 355
    .local v1, "devCnt":I
    new-array v0, v1, [Lcom/samsung/upnp/Device;

    .line 356
    .local v0, "dev":[Lcom/samsung/upnp/Device;
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 358
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_1

    .line 364
    return-void

    .line 357
    :cond_0
    invoke-virtual {v2, v3}, Lcom/samsung/upnp/DeviceList;->getDevice(I)Lcom/samsung/upnp/Device;

    move-result-object v4

    aput-object v4, v0, v3

    .line 356
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 359
    :cond_1
    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/samsung/upnp/Device;->isExpired()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expired device = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v0, v3

    invoke-virtual {v5}, Lcom/samsung/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/api/Debugs;->info(Ljava/lang/String;)V

    .line 361
    aget-object v4, v0, v3

    invoke-direct {p0, v4}, Lcom/samsung/pmr/PersonalMessageControlPoint;->removeDevice(Lcom/samsung/upnp/Device;)V

    .line 358
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public removeNotifyListener(Lcom/samsung/upnp/device/NotifyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/NotifyListener;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceNotifyListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 400
    return-void
.end method

.method public removeSearchResponseListener(Lcom/samsung/upnp/device/SearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/upnp/device/SearchResponseListener;

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceSearchResponseListenerList:Lcom/samsung/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 425
    return-void
.end method

.method public renewSubscriberService()V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method public search()V
    .locals 2

    .prologue
    .line 536
    const-string v0, "urn:samsung.com:device:PersonalMessageReceiver:1"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->search(Ljava/lang/String;I)V

    .line 537
    return-void
.end method

.method public search(Ljava/lang/String;)V
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 532
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->search(Ljava/lang/String;I)V

    .line 533
    return-void
.end method

.method public search(Ljava/lang/String;I)V
    .locals 2
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "mx"    # I

    .prologue
    .line 526
    new-instance v0, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;

    invoke-direct {v0, p1, p2}, Lcom/samsung/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 527
    .local v0, "msReq":Lcom/samsung/upnp/ssdp/SSDPSearchRequest;
    invoke-direct {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v1

    .line 528
    .local v1, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    invoke-virtual {v1, v0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/upnp/ssdp/SSDPSearchRequest;)Z

    .line 529
    return-void
.end method

.method public searchResponseReceived(Lcom/samsung/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/upnp/ssdp/SSDPPacket;

    .prologue
    .line 501
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/upnp/device/USN;->getUDN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "udn":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/upnp/ssdp/SSDPPacket;->isPMRDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 505
    invoke-direct {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->isActivityDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 506
    invoke-virtual {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/upnp/Device;

    move-result-object v2

    if-nez v2, :cond_0

    .line 507
    invoke-direct {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->addDevice(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 508
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->performSearchResponseListener(Lcom/samsung/upnp/ssdp/SSDPPacket;)V

    .line 509
    return-void
.end method

.method public setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V
    .locals 0
    .param p1, "disposer"    # Lcom/samsung/upnp/device/Disposer;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->deviceDisposer:Lcom/samsung/upnp/device/Disposer;

    .line 379
    return-void
.end method

.method public setExpiredDeviceMonitoringInterval(J)V
    .locals 0
    .param p1, "interval"    # J

    .prologue
    .line 368
    iput-wide p1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->expiredDeviceMonitoringInterval:J

    .line 369
    return-void
.end method

.method public setNMPRMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->nmprMode:Z

    .line 149
    return-void
.end method

.method public setSSDPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->ssdpPort:I

    .line 138
    return-void
.end method

.method public setSearchMx(I)V
    .locals 0
    .param p1, "mx"    # I

    .prologue
    .line 522
    iput p1, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->searchMx:I

    .line 523
    return-void
.end method

.method public start()Z
    .locals 2

    .prologue
    .line 597
    const-string v0, "urn:samsung.com:device:PersonalMessageReceiver:1"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/pmr/PersonalMessageControlPoint;->start(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public start(Ljava/lang/String;)Z
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 593
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->start(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public start(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "mx"    # I

    .prologue
    const/4 v5, 0x0

    .line 545
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->stop()Z

    .line 551
    invoke-direct {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    move-result-object v2

    .line 552
    .local v2, "ssdpNotifySocketList":Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->open()Z

    move-result v6

    if-nez v6, :cond_1

    .line 589
    :cond_0
    :goto_0
    return v5

    .line 554
    :cond_1
    invoke-virtual {v2, p0}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    .line 555
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->start()V

    .line 561
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPPort()I

    move-result v3

    .line 562
    .local v3, "ssdpPort":I
    const/4 v1, 0x0

    .line 563
    .local v1, "retryCnt":I
    invoke-direct {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v4

    .line 564
    .local v4, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    :goto_1
    invoke-virtual {v4, v3}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->open(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 571
    invoke-virtual {v4, p0}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->setPersonalMessageCP(Lcom/samsung/pmr/PersonalMessageControlPoint;)V

    .line 572
    invoke-virtual {v4}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->start()V

    .line 578
    invoke-virtual {p0, p1, p2}, Lcom/samsung/pmr/PersonalMessageControlPoint;->search(Ljava/lang/String;I)V

    .line 584
    new-instance v0, Lcom/samsung/upnp/device/Disposer;

    invoke-direct {v0, p0}, Lcom/samsung/upnp/device/Disposer;-><init>(Lcom/samsung/upnp/IControlPoint;)V

    .line 585
    .local v0, "disposer":Lcom/samsung/upnp/device/Disposer;
    const-string v5, "Device Disposer"

    invoke-virtual {v0, v5}, Lcom/samsung/upnp/device/Disposer;->setName(Ljava/lang/String;)V

    .line 586
    invoke-virtual {p0, v0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 587
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Disposer;->start()V

    .line 589
    const/4 v5, 0x1

    goto :goto_0

    .line 565
    .end local v0    # "disposer":Lcom/samsung/upnp/device/Disposer;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 566
    const/16 v6, 0x64

    if-lt v6, v1, :cond_0

    .line 568
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p0, v6}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setSSDPPort(I)V

    .line 569
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPPort()I

    move-result v3

    goto :goto_1
.end method

.method public stop()Z
    .locals 4

    .prologue
    .line 601
    invoke-direct {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPNotifySocketList()Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;

    move-result-object v1

    .line 602
    .local v1, "ssdpNotifySocketList":Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;
    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->stop()V

    .line 603
    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->close()V

    .line 604
    invoke-virtual {v1}, Lcom/samsung/upnp/ssdp/SSDPNotifySocketList;->clear()V

    .line 606
    invoke-direct {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getSSDPSearchResponseSocketList()Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v2

    .line 607
    .local v2, "ssdpSearchResponseSocketList":Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->stop()V

    .line 608
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->close()V

    .line 609
    invoke-virtual {v2}, Lcom/samsung/upnp/ssdp/SSDPSearchResponseSocketList;->clear()V

    .line 615
    invoke-virtual {p0}, Lcom/samsung/pmr/PersonalMessageControlPoint;->getDeviceDisposer()Lcom/samsung/upnp/device/Disposer;

    move-result-object v0

    .line 616
    .local v0, "disposer":Lcom/samsung/upnp/device/Disposer;
    if-eqz v0, :cond_0

    .line 617
    invoke-virtual {v0}, Lcom/samsung/upnp/device/Disposer;->stop()V

    .line 618
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/samsung/pmr/PersonalMessageControlPoint;->setDeviceDisposer(Lcom/samsung/upnp/device/Disposer;)V

    .line 621
    :cond_0
    const/4 v3, 0x1

    return v3
.end method

.method public unlock()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/pmr/PersonalMessageControlPoint;->mutex:Lcom/samsung/util/Mutex;

    invoke-virtual {v0}, Lcom/samsung/util/Mutex;->unlock()V

    .line 124
    return-void
.end method
