.class Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;
.super Ljava/lang/Object;
.source "TMNetworkReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UsbCheck"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 155
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() UsbCheck Thread Enter"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    new-instance v11, Ljava/io/File;

    const-string v12, "/sys/class/android_usb/android0/terminal_version"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v11}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$3(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Ljava/io/File;)V

    .line 157
    :goto_0
    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z
    invoke-static {}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$4()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 260
    :cond_0
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() UsbCheck Thread Exit"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return-void

    .line 159
    :cond_1
    new-instance v6, Ljava/lang/StringBuffer;

    const-string v10, ""

    invoke-direct {v6, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 160
    .local v6, "strContent":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 162
    .local v2, "fin":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mUsbfile:Ljava/io/File;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$5(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Ljava/io/File;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 164
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .local v3, "fin":Ljava/io/FileInputStream;
    :goto_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->read()I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    move-result v0

    .local v0, "ch":I
    const/4 v10, -0x1

    if-ne v0, v10, :cond_5

    move-object v2, v3

    .line 173
    .end local v0    # "ch":I
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    :goto_2
    new-instance v8, Ljava/util/StringTokenizer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, " "

    invoke-direct {v8, v10, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .local v8, "tokenizer":Ljava/util/StringTokenizer;
    const/4 v6, 0x0

    .line 175
    :cond_2
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v10

    if-nez v10, :cond_6

    .line 242
    :cond_3
    :goto_3
    const/4 v8, 0x0

    .line 244
    if-eqz v2, :cond_4

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    const/4 v2, 0x0

    .line 250
    :cond_4
    :goto_4
    :try_start_3
    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z
    invoke-static {}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$4()Z

    move-result v10

    if-nez v10, :cond_0

    .line 253
    const-wide/16 v10, 0x1f4

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 255
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() interrupted while sleeping "

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 165
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "tokenizer":Ljava/util/StringTokenizer;
    .restart local v0    # "ch":I
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :cond_5
    int-to-char v10, v0

    :try_start_4
    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_1

    .line 167
    .end local v0    # "ch":I
    :catch_1
    move-exception v1

    move-object v2, v3

    .line 168
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    :goto_5
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() FileNotFoundException for not opening usb file"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 169
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 170
    .local v1, "e":Ljava/io/IOException;
    :goto_6
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() IOException for not opening usb file"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 176
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v8    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_6
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 177
    .local v9, "val":Ljava/lang/String;
    const-string v10, "major"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 178
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "majorVal":Ljava/lang/String;
    const-string v10, "0"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 181
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 182
    const-string v10, "minor"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 183
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "minorVal":Ljava/lang/String;
    const-string v10, "0"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 187
    const/4 v10, 0x1

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    .line 188
    const-string v10, "TMNetworkReceiver"

    const-string v11, "mIsFileUpdated set true"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$0(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/content/Context;

    move-result-object v10

    const-string v11, "phone"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 190
    .local v7, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_7

    .line 191
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_9

    .line 192
    :cond_7
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$7(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 193
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$8(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 195
    :cond_8
    const-string v10, "TMNetworkReceiver"

    const-string v11, "Phone rining or offhook, Not launching MirrorLink"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    goto/16 :goto_3

    .line 197
    :cond_9
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # invokes: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->isDeviceLocked()Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$9(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 198
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$10(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-nez v10, :cond_a

    .line 199
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$8(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 201
    :cond_a
    const-string v10, "TMNetworkReceiver"

    const-string v11, "Device is locked, Not launching MirrorLink"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    goto/16 :goto_3

    .line 204
    :cond_b
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TmsService startService : major 0 minor 1"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    iget-object v11, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;
    invoke-static {v11}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$0(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/content/Context;

    move-result-object v11

    # invokes: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->startServices(Landroid/content/Context;)V
    invoke-static {v10, v11}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$11(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Landroid/content/Context;)V

    goto/16 :goto_3

    .line 212
    .end local v5    # "minorVal":Ljava/lang/String;
    .end local v7    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_c
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 213
    const-string v10, "minor"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 214
    const/4 v10, 0x1

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    .line 215
    const-string v10, "TMNetworkReceiver"

    const-string v11, "mIsFileUpdated set true"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$0(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/content/Context;

    move-result-object v10

    const-string v11, "phone"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 217
    .restart local v7    # "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_d

    .line 218
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_f

    .line 219
    :cond_d
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$7(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 220
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$8(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 222
    :cond_e
    const-string v10, "TMNetworkReceiver"

    const-string v11, "Phone rining or offhook, Not launching MirrorLink"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    goto/16 :goto_3

    .line 224
    :cond_f
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # invokes: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->isDeviceLocked()Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$9(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 225
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$10(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z

    move-result v10

    if-nez v10, :cond_10

    .line 226
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$8(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 228
    :cond_10
    const-string v10, "TMNetworkReceiver"

    const-string v11, "Device is locked, Not launching MirrorLink"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$6(Z)V

    goto/16 :goto_3

    .line 231
    :cond_11
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TmsService startService : major 1 minor"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v10, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    iget-object v11, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;->this$0:Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;

    # getter for: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;
    invoke-static {v11}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$0(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/content/Context;

    move-result-object v11

    # invokes: Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->startServices(Landroid/content/Context;)V
    invoke-static {v10, v11}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->access$11(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Landroid/content/Context;)V

    goto/16 :goto_3

    .line 238
    .end local v4    # "majorVal":Ljava/lang/String;
    .end local v7    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_12
    const-string v10, "TMNetworkReceiver"

    const-string v11, "TMNetworkReceiver.onReceive() Major Version is not present"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 245
    .end local v9    # "val":Ljava/lang/String;
    :catch_3
    move-exception v1

    .line 246
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 169
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "tokenizer":Ljava/util/StringTokenizer;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 167
    :catch_5
    move-exception v1

    goto/16 :goto_5
.end method
