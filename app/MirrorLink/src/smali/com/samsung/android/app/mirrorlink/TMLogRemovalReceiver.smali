.class public Lcom/samsung/android/app/mirrorlink/TMLogRemovalReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TMLogRemovalReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSLogRemovalReceiver"

.field private static isCalled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/mirrorlink/TMLogRemovalReceiver;->isCalled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    const-string v1, "TMSLogRemovalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMLogRemovalReceiver.onReceive() Enter isCalled ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v3, Lcom/samsung/android/app/mirrorlink/TMLogRemovalReceiver;->isCalled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    sget-boolean v1, Lcom/samsung/android/app/mirrorlink/TMLogRemovalReceiver;->isCalled:Z

    if-nez v1, :cond_0

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "actionString":Ljava/lang/String;
    const-string v1, "TMSLogRemovalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMLogRemovalReceiver.onReceive() action "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/app/mirrorlink/TMLogRemovalReceiver;->isCalled:Z

    .line 47
    .end local v0    # "actionString":Ljava/lang/String;
    :cond_0
    const-string v1, "TMSLogRemovalReceiver"

    const-string v2, "TMLogRemovalReceiver.onReceive() Exit"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method
