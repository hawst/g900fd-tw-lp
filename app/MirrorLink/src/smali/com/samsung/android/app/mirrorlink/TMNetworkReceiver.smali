.class public Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TMNetworkReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;
    }
.end annotation


# static fields
.field private static final ACTION_USB_STATE:Ljava/lang/String; = "android.hardware.usb.action.USB_STATE"

.field private static final TAG:Ljava/lang/String; = "TMNetworkReceiver"

.field private static final USB_CONFIGURED:Ljava/lang/String; = "configured"

.field private static final USB_CONNECTED:Ljava/lang/String; = "connected"

.field private static lock:Ljava/lang/Object;

.field private static mIsFileUpdated:Z

.field private static mThread:Ljava/lang/Thread;


# instance fields
.field private mCntxt:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsCallToastShown:Z

.field private mIsLockedToastShown:Z

.field private mUsbfile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->lock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mUsbfile:Ljava/io/File;

    .line 56
    iput-boolean v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z

    .line 57
    iput-boolean v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z

    .line 59
    new-instance v0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$1;-><init>(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;

    .line 73
    const-string v0, "TMNetworkReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMNetworkReceiver.TMNetworkReceiver() Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Z)V
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->startServices(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Z)V
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mUsbfile:Ljava/io/File;

    return-void
.end method

.method static synthetic access$4()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Ljava/io/File;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mUsbfile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$6(Z)V
    .locals 0

    .prologue
    .line 49
    sput-boolean p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)Z
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->isDeviceLocked()Z

    move-result v0

    return v0
.end method

.method private isDeviceLocked()Z
    .locals 3

    .prologue
    .line 142
    iget-object v1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 143
    .local v0, "keyGMngr":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    return v1
.end method

.method private startServices(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 147
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 148
    .local v0, "startIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 149
    const-string v1, "TMNetworkReceiver"

    const-string v2, "TmsService startService :TMDisplayService4"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 82
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive() Enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    .line 85
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "actionString":Ljava/lang/String;
    const-string v5, "TMNetworkReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TMNetworkReceiver.onReceive() Action "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v5, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "connected":Z
    const/4 v1, 0x0

    .line 90
    .local v1, "configured":Z
    const/4 v4, 0x0

    .line 91
    .local v4, "isMtp":Z
    iput-boolean v8, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsCallToastShown:Z

    .line 92
    iput-boolean v8, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsLockedToastShown:Z

    .line 94
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 95
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive(): intent.getExtras() is not null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "connected"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 97
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "configured"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 98
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "mtp"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 103
    :goto_0
    const-string v5, "TMNetworkReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TMNetworkReceiver.onReceive() on USB - connected:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 104
    const-string v7, ", configured :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mtp = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mIsFileUpdated= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 103
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    sget-object v6, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 107
    :try_start_0
    sget-object v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    if-eqz v5, :cond_0

    .line 108
    sget-object v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    const-string v5, "TMNetworkReceiver"

    const-string v7, "TMNetworkReceiver.onReceive() mThread.interrupt()"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    sget-object v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 114
    :cond_0
    if-eqz v2, :cond_3

    .line 115
    sget-boolean v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    if-eqz v5, :cond_1

    if-eqz v4, :cond_1

    .line 116
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    .line 118
    :cond_1
    const-string v5, "TMNetworkReceiver"

    const-string v7, "TMNetworkReceiver.onReceive() USB CONNECTED"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v5, Ljava/lang/Thread;

    new-instance v7, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver$UsbCheck;-><init>(Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;)V

    const-string v8, "USB_Check"

    invoke-direct {v5, v7, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    sput-object v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    .line 120
    sget-object v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 106
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    .end local v1    # "configured":Z
    .end local v2    # "connected":Z
    .end local v4    # "isMtp":Z
    :goto_2
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive() Exit "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void

    .line 100
    .restart local v1    # "configured":Z
    .restart local v2    # "connected":Z
    .restart local v4    # "isMtp":Z
    :cond_2
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive(): intent.getExtras() is null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :cond_3
    :try_start_1
    const-string v5, "TMNetworkReceiver"

    const-string v7, "TMNetworkReceiver.onReceive() USB NOT CONNECTED"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    sget-boolean v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    if-eqz v5, :cond_4

    .line 124
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mIsFileUpdated:Z

    .line 125
    :cond_4
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v3, "dismissScreen":Landroid/content/Intent;
    const-string v5, "IS_DISCONNECT"

    const/4 v7, 0x1

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 127
    iget-object v5, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 106
    .end local v3    # "dismissScreen":Landroid/content/Intent;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 130
    .end local v1    # "configured":Z
    .end local v2    # "connected":Z
    .end local v4    # "isMtp":Z
    :cond_5
    const-string v5, "samsung.intent.action.ML_UNDOCK_CARMODE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 131
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive() samsung.intent.action.ML_UNDOCK_CARMODE "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v5, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    iget-object v5, p0, Lcom/samsung/android/app/mirrorlink/TMNetworkReceiver;->mCntxt:Landroid/content/Context;

    invoke-virtual {v5, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2

    .line 135
    :cond_6
    const-string v5, "TMNetworkReceiver"

    const-string v6, "TMNetworkReceiver.onReceive() UnHandled "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
