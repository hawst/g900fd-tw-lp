.class public Lcom/samsung/android/mirrorlink/acms/api/AcmsService;
.super Landroid/app/Service;
.source "AcmsService.java"


# static fields
.field private static final ACMS_DBVIEWER_SERVICE:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsDbviewer"

.field private static final ACMS_SERVICE_INTENT_FILTER:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

.field private static final TAG:Ljava/lang/String; = "AcmsService"


# instance fields
.field private mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

.field private mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

.field private mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

.field private mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

.field private ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    .line 49
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    .line 40
    return-void
.end method

.method private getAcmsCore()Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    if-nez v0, :cond_0

    .line 85
    const-string v0, "AcmsService"

    const-string v1, "creating AcmsCore"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->getAcmsCore(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 87
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->init()V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    return-object v0
.end method

.method private handleGracePeriodExpired(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V
    .locals 2
    .param p1, "ocspResponseEntryInterface"    # Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    .param p2, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p3, "currentState"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .prologue
    .line 169
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne p3, v0, :cond_0

    .line 170
    invoke-virtual {p2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->notifyUser(Ljava/lang/String;I)V

    .line 172
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v0

    .line 174
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 173
    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 179
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne p3, v0, :cond_1

    .line 182
    invoke-virtual {p2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->notifyUser(Ljava/lang/String;I)V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v0

    .line 187
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 186
    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 191
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne p3, v0, :cond_2

    .line 192
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v0

    .line 194
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 193
    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 196
    :cond_2
    return-void
.end method

.method private handleQueryPeriodExpired(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    .locals 2
    .param p1, "ocspResponseEntryInterface"    # Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    .param p2, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    .line 154
    invoke-virtual {p2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v0, v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 163
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleStartIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    .line 97
    .local v1, "appEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v7

    .line 96
    iput-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    .line 98
    const-string v7, "AcmsService"

    const-string v8, "Inside handle Intent"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 100
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    .line 101
    .local v5, "pkgName":Ljava/lang/String;
    const-string v7, "AcmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "App added - package name: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    const/4 v8, 0x2

    invoke-virtual {v7, v5, v8}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    .line 147
    .end local v5    # "pkgName":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v7, "AcmsService"

    const-string v8, "Decremented Counter Value : handleStartIntent"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 149
    :goto_1
    return-void

    .line 103
    :cond_1
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    .line 104
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 103
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 104
    if-eqz v7, :cond_2

    .line 105
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    .line 106
    .restart local v5    # "pkgName":Ljava/lang/String;
    const-string v7, "AcmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "App removed - package name: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    const/4 v8, 0x5

    invoke-virtual {v7, v5, v8}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    goto :goto_0

    .line 108
    .end local v5    # "pkgName":Ljava/lang/String;
    :cond_2
    const-string v7, "com.samsung.android.mirrorlink.acms.ALARM_TIMEOUT_EVENT"

    .line 109
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 108
    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 109
    if-eqz v7, :cond_5

    .line 110
    const-string v7, "AcmsService"

    const-string v8, "ACMS TIME OUT Occured"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v7, "android.mirrorlink.acms.extra.APPID"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "appId":Ljava/lang/String;
    const-string v7, "android.mirrorlink.acms.extra.SCHEDULE_REASON"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "reason":Ljava/lang/String;
    const-string v7, "AcmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Alarm time out for appid "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " for reason "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 115
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 114
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v0

    .line 117
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v0, :cond_3

    .line 118
    const-string v7, "AcmsService"

    const-string v8, "appEntryInterface is null: Hence ignore"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 122
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v4

    .line 124
    .local v4, "currentState":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    const-string v7, "grace period"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 125
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    invoke-direct {p0, v7, v0, v4}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->handleGracePeriodExpired(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 128
    :cond_4
    const-string v7, "query period"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 129
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    invoke-direct {p0, v7, v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->handleQueryPeriodExpired(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_0

    .line 134
    .end local v0    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v2    # "appId":Ljava/lang/String;
    .end local v4    # "currentState":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .end local v6    # "reason":Ljava/lang/String;
    :cond_5
    const-string v7, "com.samsung.android.mirrorlink.ML_STATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 135
    const-string v7, "mlstatus"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v3

    .line 137
    .local v3, "connected":Z
    if-eqz v3, :cond_0

    .line 138
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    const-string v8, ""

    const/16 v9, 0x8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 142
    .end local v3    # "connected":Z
    :cond_6
    const-string v7, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 144
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getAcmsCore()Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->checkapps()V

    goto/16 :goto_0
.end method

.method private notifyUser(Ljava/lang/String;I)V
    .locals 12
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "reason"    # I

    .prologue
    const/16 v11, 0x4d2

    const/4 v10, 0x0

    .line 199
    const-string v7, "AcmsService"

    const-string v8, "Entered NotifyUser()"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 203
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 204
    .local v6, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 206
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v6, p1, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 214
    if-eqz v1, :cond_1

    .line 215
    invoke-virtual {v6, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 214
    :goto_0
    check-cast v0, Ljava/lang/String;

    .line 217
    .local v0, "appName":Ljava/lang/String;
    const-string v7, "AcmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "App name for pkg name "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 220
    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 219
    check-cast v5, Landroid/app/NotificationManager;

    .line 221
    .local v5, "notificationManager":Landroid/app/NotificationManager;
    new-instance v2, Landroid/app/Notification$Builder;

    .line 222
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 221
    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 223
    .local v2, "builder":Landroid/app/Notification$Builder;
    const/4 v7, -0x2

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 224
    const-string v7, "DriveLink Notification"

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 225
    const/4 v7, 0x2

    if-ne p2, v7, :cond_2

    .line 226
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 227
    const-string v8, " Application is no more a certified application"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 226
    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 235
    :cond_0
    :goto_1
    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 236
    .local v4, "n":Landroid/app/Notification;
    const/16 v7, 0x10

    iput v7, v4, Landroid/app/Notification;->flags:I

    .line 237
    iput v10, v4, Landroid/app/Notification;->defaults:I

    .line 239
    invoke-virtual {v5, v11, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 240
    invoke-virtual {p0, v11, v4}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->startForeground(ILandroid/app/Notification;)V

    .line 242
    .end local v0    # "appName":Ljava/lang/String;
    .end local v2    # "builder":Landroid/app/Notification$Builder;
    .end local v4    # "n":Landroid/app/Notification;
    .end local v5    # "notificationManager":Landroid/app/NotificationManager;
    :goto_2
    return-void

    .line 207
    :catch_0
    move-exception v3

    .line 209
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "AcmsService"

    const-string v8, "Package Name not present "

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 215
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const-string v0, "???"

    goto :goto_0

    .line 230
    .restart local v0    # "appName":Ljava/lang/String;
    .restart local v2    # "builder":Landroid/app/Notification$Builder;
    .restart local v5    # "notificationManager":Landroid/app/NotificationManager;
    :cond_2
    const/4 v7, 0x1

    if-ne p2, v7, :cond_0

    .line 231
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 232
    const-string v8, " Application is no more drive certified application."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 233
    const-string v8, " Can be used in park mode"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 231
    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_1
.end method


# virtual methods
.method public cleanUp()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    const-string v0, "AcmsService"

    const-string v1, "cleanUp(): inside service clean up"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->cleanUp()V

    .line 307
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->cleanUp()V

    .line 311
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->deinit()V

    .line 315
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 317
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    if-eqz v0, :cond_3

    .line 318
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->cleanUp()V

    .line 319
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->ocspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    .line 321
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    if-eqz v0, :cond_4

    .line 322
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getSvcCounter()I

    .line 323
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->cleanUp()V

    .line 324
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    .line 327
    :cond_4
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 248
    if-eqz p1, :cond_1

    .line 249
    const-string v0, "AcmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onBind(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const-string v0, "AcmsService"

    const-string v1, "onBind(): intent is matching"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->getAcmsManager(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 253
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0, p0, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter(Landroid/app/Service;I)V

    .line 254
    const-string v0, "AcmsService"

    const-string v1, "Incremented Counter Value : onBind"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 267
    :goto_0
    return-object v0

    .line 258
    :cond_0
    const-string v0, "com.samsung.android.mirrorlink.acms.api.IAcmsDbviewer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    const-string v0, "AcmsService"

    const-string v1, "onBind(): intent is matching"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->getAcmsDbViewerMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    .line 261
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0, p0, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter(Landroid/app/Service;I)V

    .line 262
    const-string v0, "AcmsService"

    const-string v1, "Incremented Counter Value : onBind"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    goto :goto_0

    .line 267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 56
    const-string v0, "AcmsService"

    const-string v1, "Enter Oncreate"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    .line 58
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->setContext(Landroid/content/Context;)V

    .line 59
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getAcmsCore()Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 60
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getSvcCounter()I

    move-result v0

    if-gtz v0, :cond_0

    .line 294
    const-string v0, "AcmsService"

    const-string v1, "Enter onDestroy"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->cleanUp()V

    .line 296
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 297
    const-string v0, "AcmsService"

    const-string v1, "killing acms process"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 300
    :cond_0
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 273
    const-string v0, "AcmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRebind(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    .line 275
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 276
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 65
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 67
    :cond_0
    const-string v0, "AcmsService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-eqz p1, :cond_1

    .line 69
    const-string v0, "AcmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->getAcmsCore()Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0, p0, p3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter(Landroid/app/Service;I)V

    .line 75
    const-string v0, "AcmsService"

    const-string v1, "Incremented Counter Value : onStartCommand"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->handleStartIntent(Landroid/content/Intent;)V

    .line 80
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 282
    if-eqz p1, :cond_1

    const-string v0, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    const-string v0, "com.samsung.android.mirrorlink.acms.api.IAcmsDbviewer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    const-string v0, "AcmsService"

    const-string v1, "Decremented Counter Value : onUnbind"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;->mAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 287
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
