.class public Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;
.super Lcom/mirrorlink/android/service/IMirrorlinkManager$Stub;
.source "MirrorLinkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
    }
.end annotation


# static fields
.field private static final ACMS_SERVICE_INTENT:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

.field private static final TAG:Ljava/lang/String; = "MirrorLinkManager"

.field private static final TERMINAL_VERSION_FILE_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/terminal_version"


# instance fields
.field private mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

.field private mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

.field private final mClientListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/service/IMirrorlinkListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/mirrorlink/android/service/IMirrorlinkManager$Stub;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    .line 83
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    .line 85
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    return-object v0
.end method


# virtual methods
.method public clearSelectedAppList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 261
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter clearAllSelected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->clearAllSelected(Ljava/util/List;)Z

    move-result v0

    .line 263
    .local v0, "res":Z
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit clearAllSelected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    return v0
.end method

.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    const-string v0, "MirrorLinkManager"

    const-string v1, "Enter deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 139
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 141
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->clearMirrorlinkMgr()V

    .line 144
    :cond_1
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    .line 145
    return-void
.end method

.method public getAllAppList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 216
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter getAllAppList "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getAllApps()Ljava/util/List;

    move-result-object v0

    .line 218
    .local v0, "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 219
    :cond_0
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit getAllAppList. No apps selected"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    if-nez v0, :cond_1

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .restart local v0    # "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v1, "MirrorLinkManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exit getAllAppList. Sending  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " apps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-object v0
.end method

.method public getApplicationElements(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 352
    const-string v0, "MirrorLinkManager"

    const-string v1, "Enter getApplicationList "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getAppListBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getCertificationInformation(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 277
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter getCertificationInformation "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getCertificationInfo(Ljava/lang/String;)I

    move-result v0

    .line 280
    .local v0, "certInfo":I
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit getCertificationInformation "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return v0
.end method

.method public getCertifyingEntities(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 308
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter getCertifyingEntity "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getCertifyingEntities(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "certEntities":Ljava/lang/String;
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit getCertifyingEntity "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-object v0
.end method

.method public getPolicy()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 297
    const-string v2, "MirrorLinkManager"

    const-string v3, "Enter getPolicy "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    const-string v3, "PreferencePolicy"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 301
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "policy"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 302
    .local v0, "policy":I
    const-string v2, "MirrorLinkManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exit getPolicy : current policy : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return v0
.end method

.method public getSelectedAppList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 235
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter getSelectedAppList "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getSelectedApps()Ljava/util/List;

    move-result-object v0

    .line 237
    .local v0, "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 238
    :cond_0
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit getSelectedAppList. No apps selected"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    if-nez v0, :cond_1

    .line 240
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .restart local v0    # "selectedApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v1, "MirrorLinkManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exit getSelectedAppList. Sending  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " apps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    return-object v0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 89
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter Init()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    if-nez v1, :cond_0

    .line 92
    const-string v1, "MirrorLinkManager"

    const-string v2, "mServiceConnection is null. So creating"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v1, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    .line 96
    new-instance v1, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    .line 97
    new-instance v1, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 126
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 130
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 132
    return-void
.end method

.method public isApplicationSelected(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 269
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter isApplicationSelected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getApplicationShowValue(Ljava/lang/String;)Z

    move-result v0

    .line 271
    .local v0, "showValue":Z
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit isApplicationSelected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    return v0
.end method

.method public notifyResult(Ljava/lang/String;I)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "result"    # I

    .prologue
    .line 172
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 174
    .local v2, "length":I
    const-string v3, "MirrorLinkManager"

    .line 175
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MirrorLinkManager.notifyRevocationCheckResult():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 176
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 175
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 174
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 185
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 187
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 180
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/service/IMirrorlinkListener;

    invoke-interface {v3, p1, p2}, Lcom/mirrorlink/android/service/IMirrorlinkListener;->notifyRevocationCheckResult(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public performRevocation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 335
    const-string v0, "MirrorLinkManager"

    const-string v1, "Enter performRevocation "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    invoke-interface {v0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->performRevocation()V

    .line 347
    :goto_0
    const-string v0, "MirrorLinkManager"

    const-string v1, "Exit performRevocation "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    return-void

    .line 340
    :cond_0
    const-string v0, "MirrorLinkManager"

    const-string v1, "Acms Service is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public performRevocationCheck(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 318
    const-string v0, "MirrorLinkManager"

    const-string v1, "Enter performRevocationCheck "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    invoke-interface {v0, p1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->performRevocationCheck(Ljava/lang/String;)V

    .line 330
    :goto_0
    const-string v0, "MirrorLinkManager"

    const-string v1, "Exit performRevocationCheck "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    return-void

    .line 323
    :cond_0
    const-string v0, "MirrorLinkManager"

    const-string v1, "Acms Service is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readTerminalVersion()Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 406
    const-string v11, "MirrorLinkManager"

    const-string v12, "readTerminalVersion - Enter"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const/4 v7, 0x0

    .line 408
    .local v7, "ready":Z
    const/4 v1, 0x0

    .line 409
    .local v1, "fin":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 411
    .local v5, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const-string v11, "/sys/class/android_usb/android0/terminal_version"

    invoke-direct {v2, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    .line 412
    .end local v1    # "fin":Ljava/io/FileInputStream;
    .local v2, "fin":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    const-string v12, "UTF-8"

    invoke-direct {v11, v2, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v6, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    .line 413
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .local v6, "reader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 414
    .local v3, "line":Ljava/lang/String;
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion usb file content is  "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 415
    if-nez v3, :cond_4

    .line 417
    if-eqz v2, :cond_0

    .line 418
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    .line 424
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 425
    :try_start_4
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_1
    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    move-object v1, v2

    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    move v8, v7

    .line 475
    .end local v3    # "line":Ljava/lang/String;
    .end local v7    # "ready":Z
    .local v8, "ready":I
    :goto_2
    return v8

    .line 420
    .end local v1    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .end local v8    # "ready":I
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "ready":Z
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion file close exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 446
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "line":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    move-object v1, v2

    .line 447
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .local v0, "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    :goto_3
    const-string v11, "MirrorLinkManager"

    .line 448
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion FileNotFoundException for not opening usb file"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 449
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 448
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 447
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 461
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :goto_4
    if-eqz v1, :cond_2

    .line 462
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 468
    :cond_2
    :goto_5
    if-eqz v5, :cond_3

    .line 469
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 474
    :cond_3
    :goto_6
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion Exit ,bNcmReady-  "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v7

    .line 475
    .restart local v8    # "ready":I
    goto :goto_2

    .line 427
    .end local v1    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .end local v8    # "ready":I
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v0

    .line 428
    .local v0, "e":Ljava/io/IOException;
    :try_start_8
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion file close exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_1

    .line 451
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "line":Ljava/lang/String;
    :catch_3
    move-exception v0

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    move-object v1, v2

    .line 452
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    :goto_7
    const-string v11, "MirrorLinkManager"

    .line 453
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion IOException for not opening usb file"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 453
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 452
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 432
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :cond_4
    :try_start_9
    new-instance v9, Ljava/util/StringTokenizer;

    const-string v11, " "

    invoke-direct {v9, v3, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .local v9, "tokenizer":Ljava/util/StringTokenizer;
    :cond_5
    :goto_8
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v11

    if-nez v11, :cond_6

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    move-object v1, v2

    .line 446
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    goto :goto_4

    .line 435
    .end local v1    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :cond_6
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 436
    .local v10, "val":Ljava/lang/String;
    const-string v11, "major"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 437
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 438
    .local v4, "majorVal":Ljava/lang/String;
    const-string v11, "1"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 439
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    .line 440
    const-string v11, "minor"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    move-result v11

    if-eqz v11, :cond_5

    .line 441
    const/4 v7, 0x1

    goto :goto_8

    .line 463
    .end local v2    # "fin":Ljava/io/FileInputStream;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "majorVal":Ljava/lang/String;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .end local v9    # "tokenizer":Ljava/util/StringTokenizer;
    .end local v10    # "val":Ljava/lang/String;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    .line 464
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion file close exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 471
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 472
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v11, "MirrorLinkManager"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "readTerminalVersion file close exception: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 451
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    goto :goto_7

    .end local v1    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    goto :goto_7

    .line 446
    :catch_8
    move-exception v0

    goto/16 :goto_3

    .end local v1    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "fin":Ljava/io/FileInputStream;
    :catch_9
    move-exception v0

    move-object v1, v2

    .end local v2    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public registerListener(Lcom/mirrorlink/android/service/IMirrorlinkListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/mirrorlink/android/service/IMirrorlinkListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v0, "MirrorLinkManager"

    const-string v1, "MirrorLinkManager.registerListener()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    invoke-interface {v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->registerListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 156
    const-string v0, "MirrorLinkManager"

    const-string v1, "MirrorLinkManager.resgisterListener() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public setDevMode(Z)V
    .locals 2
    .param p1, "mode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 359
    const-string v0, "MirrorLinkManager"

    const-string v1, "Enter setDevMode "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    invoke-interface {v0, p1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->setDevMode(Z)V

    .line 367
    :goto_0
    return-void

    .line 364
    :cond_0
    const-string v0, "MirrorLinkManager"

    const-string v1, "Acms Service is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setPolicy(I)V
    .locals 4
    .param p1, "policy"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 287
    const-string v1, "MirrorLinkManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter setPolicy :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mContext:Landroid/content/Context;

    const-string v2, "PreferencePolicy"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 289
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "policy"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 291
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit setPolicy "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method public setSelectedAppList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "MirrorLinkManager"

    const-string v2, "Enter setSelectedAppList "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mMlUtil:Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->setSelectedApps(Ljava/util/List;)Z

    move-result v0

    .line 255
    .local v0, "res":Z
    const-string v1, "MirrorLinkManager"

    const-string v2, "Exit setSelectedAppList "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return v0
.end method

.method public unRegisterListener(Lcom/mirrorlink/android/service/IMirrorlinkListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/mirrorlink/android/service/IMirrorlinkListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    const-string v0, "MirrorLinkManager"

    const-string v1, "MirrorLinkManager.unRegisterListener()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    invoke-interface {v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->unRegisterListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 168
    const-string v0, "MirrorLinkManager"

    const-string v1, "MirrorLinkManager.unRegisterListener() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public writeTerminalVersion(Z)V
    .locals 7
    .param p1, "enable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 371
    const-string v4, "MirrorLinkManager"

    const-string v5, "writeTerminalVersion - Enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const/4 v2, 0x0

    .line 374
    .local v2, "outStream":Ljava/io/PrintWriter;
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v4, "/sys/class/android_usb/android0/terminal_version"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 376
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    const-string v4, "/sys/class/android_usb/android0/terminal_version"

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 377
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 378
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 379
    const-string v4, "MirrorLinkManager"

    const-string v5, "New file created"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :cond_0
    :goto_0
    new-instance v3, Ljava/io/PrintWriter;

    const-string v4, "/sys/class/android_usb/android0/terminal_version"

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    .end local v2    # "outStream":Ljava/io/PrintWriter;
    .local v3, "outStream":Ljava/io/PrintWriter;
    if-eqz p1, :cond_3

    .line 388
    :try_start_1
    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 392
    :goto_1
    const-string v4, "MirrorLinkManager"

    const-string v5, "Updated successfully"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 397
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "outStream":Ljava/io/PrintWriter;
    .restart local v2    # "outStream":Ljava/io/PrintWriter;
    :goto_2
    if-eqz v2, :cond_1

    .line 398
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 400
    :cond_1
    const-string v4, "MirrorLinkManager"

    const-string v5, "writeTerminalVersion - exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    return-void

    .line 381
    .restart local v1    # "file":Ljava/io/File;
    :cond_2
    :try_start_2
    const-string v4, "MirrorLinkManager"

    const-string v5, "File already exist. Hence, not created"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 393
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 395
    const-string v4, "MirrorLinkManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not update file content: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 390
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "outStream":Ljava/io/PrintWriter;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "outStream":Ljava/io/PrintWriter;
    :cond_3
    :try_start_3
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 393
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "outStream":Ljava/io/PrintWriter;
    .restart local v2    # "outStream":Ljava/io/PrintWriter;
    goto :goto_3
.end method
