.class public Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
.super Ljava/lang/Object;
.source "AcmsHttpCertificateHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;,
        Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;
    }
.end annotation


# static fields
.field private static final ACMS_APP_CERT_HOST:Ljava/lang/String; = "acms.carconnectivity.org"

.field private static final ACMS_DEV_CERT_HOST:Ljava/lang/String; = "acms.carconnectivity.org"

.field private static final TAG:Ljava/lang/String; = "AcmsHttpCertificateHandler"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    .line 66
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->httpgetAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V

    return-void
.end method

.method private getByteArrayInputStream(Ljava/lang/String;)Ljava/io/ByteArrayInputStream;
    .locals 2
    .param p1, "certString"    # Ljava/lang/String;

    .prologue
    .line 324
    const-string v0, "AcmsHttpCertificateHandler"

    const-string v1, "getByteArrayInputStream(): Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .line 326
    const-string v1, "US-ASCII"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    .line 325
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method private getHttpResponse(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;
    .param p3, "url"    # Ljava/net/URI;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 224
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 226
    .local v1, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 228
    .local v2, "httpclient":Lorg/apache/http/client/HttpClient;
    const-string v6, "User-Agent"

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-interface {v2, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 232
    .local v4, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 234
    .local v3, "respStatusCode":I
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object p4

    .line 236
    const/16 v6, 0x1f4

    if-ne v3, v6, :cond_2

    .line 237
    const-string v6, "800"

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 238
    const-string v6, "AcmsHttpCertificateHandler"

    const-string v7, "httpgetAppCert() ERROR_NO_CERT_AVAILABLE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v6, 0x3

    .line 239
    invoke-interface {p2, p1, v6}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    .line 270
    .end local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v3    # "respStatusCode":I
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    :goto_0
    return-object v5

    .line 241
    .restart local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "respStatusCode":I
    .restart local v4    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    const-string v6, "801"

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 242
    const-string v6, "AcmsHttpCertificateHandler"

    const-string v7, "httpgetAppCert() ERROR_DATABASE_OFFLINE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v6, 0x4

    invoke-interface {p2, p1, v6}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 256
    .end local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v3    # "respStatusCode":I
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-interface {p2, p1, v9}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    .line 258
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 245
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    .restart local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "respStatusCode":I
    .restart local v4    # "response":Lorg/apache/http/HttpResponse;
    :cond_1
    :try_start_1
    const-string v6, "AcmsHttpCertificateHandler"

    const-string v7, "httpgetAppCert() 500 No Value"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v6, 0x2

    invoke-interface {p2, p1, v6}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 260
    .end local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v3    # "respStatusCode":I
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    :catch_1
    move-exception v0

    .line 261
    .local v0, "e":Ljava/io/IOException;
    invoke-interface {p2, p1, v9}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    .line 262
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 249
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v2    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "respStatusCode":I
    .restart local v4    # "response":Lorg/apache/http/HttpResponse;
    :cond_2
    const/16 v6, 0xc8

    if-ne v3, v6, :cond_3

    .line 250
    :try_start_2
    const-string v6, "AcmsHttpCertificateHandler"

    const-string v7, "httpgetAppCert() Response is 200 OK"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 266
    if-nez p4, :cond_4

    .line 267
    invoke-interface {p2, p1, v8}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    goto :goto_0

    .line 252
    :cond_3
    :try_start_3
    const-string v6, "AcmsHttpCertificateHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "httpgetAppCert() Error : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 253
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 252
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :cond_4
    move-object v5, p4

    .line 270
    goto :goto_0
.end method

.method private httpgetAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V
    .locals 25
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    const/4 v9, 0x0

    .line 127
    .local v9, "directory":Ljava/lang/String;
    new-instance v21, Ljava/util/LinkedList;

    invoke-direct/range {v21 .. v21}, Ljava/util/LinkedList;-><init>()V

    .line 128
    .local v21, "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    const/4 v7, 0x0

    .line 131
    .local v7, "host":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 132
    const-string v7, "acms.carconnectivity.org"

    .line 133
    const-string v9, "/obtainDeveloperCertificate.html"

    .line 135
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    .line 134
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v5

    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 137
    .local v18, "devId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    .line 138
    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    .line 137
    check-cast v20, Landroid/telephony/TelephonyManager;

    .line 139
    .local v20, "mngr":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v23

    .line 140
    .local v23, "serverId":Ljava/lang/String;
    if-eqz v23, :cond_0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x5

    if-ge v5, v6, :cond_1

    .line 141
    :cond_0
    const-string v5, "AcmsHttpCertificateHandler"

    const-string v6, "serverId is null in httpgetAppCert"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    .end local v18    # "devId":Ljava/lang/String;
    .end local v20    # "mngr":Landroid/telephony/TelephonyManager;
    .end local v23    # "serverId":Ljava/lang/String;
    :goto_0
    return-void

    .line 144
    .restart local v18    # "devId":Ljava/lang/String;
    .restart local v20    # "mngr":Landroid/telephony/TelephonyManager;
    .restart local v23    # "serverId":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 145
    const-string v5, "AcmsHttpCertificateHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Server ID is: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "Dev ID is: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "certificateVersion"

    const-string v8, "1.0"

    invoke-direct {v5, v6, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "developerID"

    move-object/from16 v0, v18

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "serverID"

    move-object/from16 v0, v23

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    .end local v18    # "devId":Ljava/lang/String;
    .end local v20    # "mngr":Landroid/telephony/TelephonyManager;
    .end local v23    # "serverId":Ljava/lang/String;
    :goto_1
    const-string v5, "utf-8"

    move-object/from16 v0, v21

    invoke-static {v0, v5}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 161
    .local v10, "query":Ljava/lang/String;
    const-string v5, "AcmsHttpCertificateHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "httpgetCert() host =  "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/16 v24, 0x0

    .line 164
    .local v24, "url":Ljava/net/URI;
    const/16 v22, 0x0

    .line 167
    .local v22, "responseBody":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/net/URI;

    const-string v5, "http"

    const/4 v6, 0x0

    const/16 v8, 0x50

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    .end local v24    # "url":Ljava/net/URI;
    .local v4, "url":Ljava/net/URI;
    :try_start_1
    const-string v5, "AcmsHttpCertificateHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "URL: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_2

    .line 175
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->getHttpResponse(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;Ljava/net/URI;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 176
    if-nez v22, :cond_3

    .line 177
    const-string v5, "AcmsHttpCertificateHandler"

    const-string v6, "Http Response is not succesfull"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 151
    .end local v4    # "url":Ljava/net/URI;
    .end local v10    # "query":Ljava/lang/String;
    .end local v22    # "responseBody":Ljava/lang/String;
    :cond_2
    const-string v7, "acms.carconnectivity.org"

    .line 152
    const-string v9, "/obtainCertificate.html"

    .line 154
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "certificateVersion"

    const-string v8, "1.0"

    invoke-direct {v5, v6, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "platformID"

    const-string v8, "Android"

    invoke-direct {v5, v6, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "runtimeID"

    const-string v8, "Native"

    invoke-direct {v5, v6, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "appID"

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 169
    .restart local v10    # "query":Ljava/lang/String;
    .restart local v22    # "responseBody":Ljava/lang/String;
    .restart local v24    # "url":Ljava/net/URI;
    :catch_0
    move-exception v19

    move-object/from16 v4, v24

    .line 170
    .end local v24    # "url":Ljava/net/URI;
    .restart local v4    # "url":Ljava/net/URI;
    .local v19, "e":Ljava/net/URISyntaxException;
    :goto_2
    const/4 v5, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    .line 171
    invoke-virtual/range {v19 .. v19}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto/16 :goto_0

    .line 181
    .end local v19    # "e":Ljava/net/URISyntaxException;
    :cond_3
    const-string v5, "\n\n\n"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 182
    .local v15, "certsplit":[Ljava/lang/String;
    array-length v5, v15

    if-nez v5, :cond_4

    .line 183
    const/4 v5, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    goto/16 :goto_0

    .line 187
    :cond_4
    array-length v5, v15

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 188
    const-string v5, "\n\n"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 189
    array-length v5, v15

    if-nez v5, :cond_5

    .line 190
    const/4 v5, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    goto/16 :goto_0

    .line 195
    :cond_5
    array-length v5, v15

    new-array v14, v5, [Ljava/security/cert/X509Certificate;

    .line 196
    .local v14, "certs":[Ljava/security/cert/X509Certificate;
    const/16 v16, 0x0

    .line 197
    .local v16, "count":I
    array-length v6, v15

    const/4 v5, 0x0

    move/from16 v17, v16

    .end local v16    # "count":I
    .local v17, "count":I
    :goto_3
    if-lt v5, v6, :cond_6

    .line 211
    if-eqz v14, :cond_7

    .line 212
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v14}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->updateDbEntry(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;[Ljava/security/cert/X509Certificate;)V

    .line 213
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v14}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onSuccess(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;[Ljava/security/cert/X509Certificate;)V

    goto/16 :goto_0

    .line 197
    :cond_6
    aget-object v13, v15, v5

    .line 198
    .local v13, "certString":Ljava/lang/String;
    const/4 v12, 0x0

    .line 200
    .local v12, "cert":Ljava/security/cert/X509Certificate;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v8

    .line 202
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->getByteArrayInputStream(Ljava/lang/String;)Ljava/io/ByteArrayInputStream;

    move-result-object v11

    .line 201
    invoke-virtual {v8, v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getCertificateFromInputStream(Ljava/io/InputStream;)Ljava/security/cert/X509Certificate;
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v12

    .line 207
    :goto_4
    if-eqz v12, :cond_8

    .line 208
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "count":I
    .restart local v16    # "count":I
    aput-object v12, v14, v17

    .line 197
    :goto_5
    add-int/lit8 v5, v5, 0x1

    move/from16 v17, v16

    .end local v16    # "count":I
    .restart local v17    # "count":I
    goto :goto_3

    .line 203
    :catch_1
    move-exception v19

    .line 204
    .local v19, "e":Ljava/security/cert/CertificateException;
    const/4 v8, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    .line 205
    invoke-virtual/range {v19 .. v19}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_4

    .line 215
    .end local v12    # "cert":Ljava/security/cert/X509Certificate;
    .end local v13    # "certString":Ljava/lang/String;
    .end local v19    # "e":Ljava/security/cert/CertificateException;
    :cond_7
    const-string v5, "AcmsHttpCertificateHandler"

    const-string v6, "certs is  null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const/4 v5, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;->onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V

    goto/16 :goto_0

    .line 169
    .end local v14    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v15    # "certsplit":[Ljava/lang/String;
    .end local v17    # "count":I
    :catch_2
    move-exception v19

    goto/16 :goto_2

    .restart local v12    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v13    # "certString":Ljava/lang/String;
    .restart local v14    # "certs":[Ljava/security/cert/X509Certificate;
    .restart local v15    # "certsplit":[Ljava/lang/String;
    .restart local v17    # "count":I
    :cond_8
    move/from16 v16, v17

    .end local v17    # "count":I
    .restart local v16    # "count":I
    goto :goto_5
.end method

.method private updateDbEntry(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;[Ljava/security/cert/X509Certificate;)V
    .locals 10
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;
    .param p3, "certs"    # [Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    if-nez p3, :cond_0

    .line 321
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 282
    array-length v7, p3

    add-int/lit8 v7, v7, -0x1

    aget-object v7, p3, v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getClientIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v3

    .line 283
    .local v3, "clientId":Ljava/lang/String;
    const-string v7, "EMPTY"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 284
    const/4 v3, 0x0

    .line 287
    :cond_1
    array-length v7, p3

    add-int/lit8 v7, v7, -0x1

    aget-object v7, p3, v7

    .line 286
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v4

    .line 289
    .local v4, "devId":Ljava/lang/String;
    const-string v7, "AcmsHttpCertificateHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DevId: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ClientId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v5, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    invoke-direct {v5}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;-><init>()V

    .line 292
    .local v5, "devIdCertEntry":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;
    invoke-virtual {v5, v3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->setClientIds(Ljava/lang/String;)V

    .line 293
    invoke-virtual {v5, v4}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->setDevId(Ljava/lang/String;)V

    .line 296
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v6

    .line 297
    .local v6, "devIdCertEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    invoke-virtual {v6, v5}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->updateCertificateForDevId(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z

    goto :goto_0

    .line 300
    .end local v3    # "clientId":Ljava/lang/String;
    .end local v4    # "devId":Ljava/lang/String;
    .end local v5    # "devIdCertEntry":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;
    .end local v6    # "devIdCertEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    :cond_2
    const/4 v1, 0x0

    .line 301
    .local v1, "appinfoxml":Ljava/lang/String;
    const/4 v0, 0x0

    .line 302
    .local v0, "appinfocert":Ljava/security/cert/X509Certificate;
    array-length v8, p3

    const/4 v7, 0x0

    :goto_1
    if-lt v7, v8, :cond_3

    .line 309
    if-nez v1, :cond_5

    .line 310
    const-string v7, "AcmsHttpCertificateHandler"

    .line 311
    const-string v8, "httpgetAppCert() Certificate does not contain the app info"

    .line 310
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_3
    aget-object v2, p3, v7

    .line 303
    .local v2, "cert":Ljava/security/cert/X509Certificate;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getAppInfoFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v1

    .line 304
    if-eqz v1, :cond_4

    .line 305
    move-object v0, v2

    .line 302
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 314
    .end local v2    # "cert":Ljava/security/cert/X509Certificate;
    :cond_5
    const-string v7, "AcmsHttpCertificateHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "httpgetAppCert() app info is: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    invoke-static {v0, p1}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->updateGetCertificateData(Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 317
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 319
    const-string v7, "AcmsHttpCertificateHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "handleAppAdded() ID = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public fetchAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)Z
    .locals 3
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;

    .prologue
    .line 80
    const-string v0, "AcmsHttpCertificateHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchAppCert() Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;-><init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;)V

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->start(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V

    .line 82
    const-string v0, "AcmsHttpCertificateHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchAppCert() Exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x1

    return v0
.end method
