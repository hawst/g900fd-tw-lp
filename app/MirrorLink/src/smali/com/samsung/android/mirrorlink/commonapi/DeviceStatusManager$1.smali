.class Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;
.super Landroid/os/RemoteCallbackList;
.source "DeviceStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 23
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;->onCallbackDied(Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;Ljava/lang/Object;)V

    return-void
.end method

.method public onCallbackDied(Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;Ljava/lang/Object;)V
    .locals 3
    .param p1, "callback"    # Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;
    .param p2, "packName"    # Ljava/lang/Object;

    .prologue
    .line 27
    const-string v1, "TMSDeviceStatusManager"

    const-string v2, "DeviceStatusManager.onCallbackDied()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-super {p0, p1}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;)V

    .line 29
    const-string v1, "TMSDeviceStatusManager"

    .line 30
    const-string v2, "DeviceStatusManager.onCallbackDied() : Calling clearPackageFromMap()"

    .line 29
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    move-object v1, p2

    .line 33
    check-cast v1, Ljava/lang/String;

    .line 32
    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->clearPackageFromMap(Ljava/lang/String;)V

    .line 39
    :cond_0
    const-string v1, "TMSDeviceStatusManager"

    .line 40
    const-string v2, "DeviceStatusManager.onCallbackDied() : Calling CommonApiService().applicationStopping() method"

    .line 38
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    .line 44
    .local v0, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-eqz v0, :cond_1

    .line 46
    check-cast p2, Ljava/lang/String;

    .end local p2    # "packName":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onApplicationStopping(Ljava/lang/String;)V

    .line 48
    :cond_1
    return-void
.end method
