.class public final enum Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;
.super Ljava/lang/Enum;
.source "TMApplicationServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "eAPPACTION_RESULT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum LAUNCH_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum LAUNCH_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum STATUS_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum UNAUTH_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

.field public static final enum UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;


# instance fields
.field errorDesc:Ljava/lang/String;

.field launchResult:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 81
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "BAD_APPID"

    const/16 v2, 0x32a

    const-string v3, "Bad AppId"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 82
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "UNAUTH_APPID"

    const/16 v2, 0x32b

    const-string v3, "Unauthorized AppId"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->UNAUTH_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 83
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "STATUS_ERROR"

    const/16 v2, 0x32b

    const-string v3, "Cannot determine status"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->STATUS_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 84
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "LAUNCH_FAILED"

    const/16 v2, 0x32d

    const-string v3, "Launch Failed"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 85
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "RESOURCE_BUSY"

    const/16 v2, 0x32e

    const-string v3, "Resource Busy"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 86
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "DEVICE_LOCKED"

    const/4 v2, 0x5

    const/16 v3, 0x32f

    const-string v4, "Device Locked"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 87
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "INAVLID_ARGUMENT"

    const/4 v2, 0x6

    const/16 v3, 0x334

    const-string v4, "Inavlid Argument"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 88
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "INVALID_PROFILEID"

    const/4 v2, 0x7

    const/16 v3, 0x33e

    const-string v4, "Invalid Profile ID"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 89
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "UNKOWN_ERROR"

    const/16 v2, 0x8

    const/16 v3, 0x383

    const-string v4, "Unknown error"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 90
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    const-string v1, "LAUNCH_SUCCESS"

    const/16 v2, 0x9

    const/16 v3, 0x384

    const-string v4, "Launch success"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    .line 79
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->UNAUTH_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->STATUS_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # I
    .param p4, "errorDesc"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 97
    iput p3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->launchResult:I

    .line 98
    iput-object p4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->errorDesc:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->launchResult:I

    return v0
.end method

.method public getErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->errorDesc:Ljava/lang/String;

    return-object v0
.end method
