.class public Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
.super Ljava/lang/Object;
.source "TMSAppManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;,
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;
    }
.end annotation


# static fields
.field private static final APPSTATUS_BACKGRND:I = 0x15

.field private static final APPSTATUS_FOREGRND:I = 0x14

.field private static final APPSTATUS_NOTRUNNING:I = 0x16

.field private static final APP_ID:Ljava/lang/String; = "AppID"

.field private static final BLUETOOTH_A2DP:Ljava/lang/String; = "Bluetooth A2DP"

.field private static final BLUETOOTH_HFP:Ljava/lang/String; = "Bluetooth HFP"

.field private static final CDB_APP:Ljava/lang/String; = "Common Data Bus"

.field private static final DAP_APP:Ljava/lang/String; = "Device Attestation"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSAppMngr"

.field private static final ORIENTATION_BOTH:I = 0x2

.field private static final ORIENTATION_LANDSCAPE:I = 0x0

.field private static final ORIENTATION_PORTRAIT:I = 0x1

.field private static final RTP_ASERVER:Ljava/lang/String; = "RTP AServer"

.field private static final RTP_CLIENT:Ljava/lang/String; = "RTP Client"

.field private static final RTP_SERVER:Ljava/lang/String; = "RTP Server"

.field private static final SEND_APPLISTSTATUS:I = 0x33

.field private static final SEND_APPSTATUS:I = 0x32

.field public static final STATUS_CB_EVENT_APPLAUNCHED:I = 0x64

.field private static final TW_LAUNCHER:Ljava/lang/String; = "com.sec.android.app.twlauncher"

.field private static mActivtyMngr:Landroid/app/ActivityManager;

.field public static mCurrentLaunchingApp:I

.field private static mUseBlock:Z


# instance fields
.field private cntxt:Landroid/content/Context;

.field handler:Landroid/os/Handler;

.field private isFirstVncApp:Z

.field private isInited:Z

.field private mAppHolders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

.field private mNotificationSupportedApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

.field private mPreviousAllowedApp:Ljava/lang/String;

.field private mPreviousTopAppId:I

.field private mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

.field private mStatusCallack:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

.field private mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

.field private usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 130
    sput v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mCurrentLaunchingApp:I

    .line 139
    sput-boolean v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    .line 117
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    .line 119
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    .line 120
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    .line 121
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    .line 123
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    .line 125
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    .line 127
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 128
    iput v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousTopAppId:I

    .line 131
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    .line 132
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mStatusCallack:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    .line 135
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isFirstVncApp:Z

    .line 137
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z

    .line 1133
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    .line 152
    const-string v1, "TMSAppMngr"

    const-string v2, "TMSAppmanager Constructor enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    .line 160
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerContentObserver()V

    .line 164
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    .line 167
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    .line 172
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    sput-object v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 174
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->regForAppStatusEvents()V

    .line 176
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    .line 183
    const-string v1, "com.android.phone"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 184
    .local v0, "phoneApp":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v0, :cond_0

    .line 186
    iget v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->addToNotificationSupportedApps(I)V

    .line 193
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isFirstVncApp:Z

    .line 194
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$6(Z)V
    .locals 0

    .prologue
    .line 139
    sput-boolean p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    return-void
.end method

.method private addPkgToList(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 2
    .param p1, "newAppInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 888
    const-string v0, "TMSAppMngr"

    const-string v1, "addPkgToList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 893
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "Added new package "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    const-string v0, "TMSAppMngr"

    const-string v1, "addPkgToList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    return-void
.end method

.method private changeAppStatus(Ljava/lang/String;IZ)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "bNotify"    # Z

    .prologue
    .line 933
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 935
    .local v1, "appinfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v1, :cond_1

    .line 937
    const-string v3, "TMSAppMngr"

    const-string v4, "changeAppStatus appinfo is null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    iget v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 942
    .local v0, "appid":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 945
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    const/16 v4, 0x16

    if-ne v3, v4, :cond_2

    const/16 v3, 0x15

    if-eq p2, v3, :cond_4

    .line 946
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput p2, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 953
    :cond_3
    :goto_1
    if-eqz p3, :cond_0

    .line 957
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    if-nez v3, :cond_5

    .line 959
    const-string v3, "TMSAppMngr"

    const-string v4, "changeAppStatus new list creating for eventing"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    .line 961
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 962
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    const/16 v4, 0x32

    const-wide/16 v5, 0x3e8

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 949
    :cond_4
    const-string v3, "TMSAppMngr"

    const-string v4, "changeAppStatus not changing app status because app is in not running state and new state received is backgorund"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 966
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 967
    .local v2, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/Integer;>;"
    :cond_6
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_7

    .line 975
    const-string v3, "TMSAppMngr"

    const-string v4, "changeAppStatus adding for eventing , "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 969
    :cond_7
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v0, :cond_6

    .line 971
    const-string v3, "TMSAppMngr"

    const-string v4, "changeAppStatus exit.package name already added for eventing"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private checkIfAppIsAdvrtsdForClient(I)Z
    .locals 7
    .param p1, "appId"    # I

    .prologue
    .line 1103
    const/4 v2, 0x0

    .line 1104
    .local v2, "ret":Z
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v0

    .line 1106
    .local v0, "client":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v0, :cond_0

    move v3, v2

    .line 1129
    .end local v2    # "ret":Z
    .local v3, "ret":I
    :goto_0
    return v3

    .line 1110
    .end local v3    # "ret":I
    .restart local v2    # "ret":Z
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v1

    .line 1112
    .local v1, "clientApps":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_2

    .line 1114
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1116
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "checkIfAppIsAdvrtsdForClient "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is adverstised for client 0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    const/4 v2, 0x1

    .line 1128
    :goto_1
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "checkIfAppIsAdvrtsdForClient exit ret =  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 1129
    .restart local v3    # "ret":I
    goto :goto_0

    .line 1121
    .end local v3    # "ret":I
    :cond_1
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "checkIfAppIsAdvrtsdForClient "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is NOT adverstised for client 0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1126
    :cond_2
    const-string v4, "TMSAppMngr"

    const-string v5, "checkIfAppIsAdvrtsdForClient (null == clientApps) "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getAppCategory(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)I
    .locals 2
    .param p1, "holderIntfc"    # Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .prologue
    .line 412
    const-string v0, "TMSAppMngr"

    const-string v1, "Enter getAppCategory "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    instance-of v0, p1, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    if-eqz v0, :cond_0

    .line 414
    const/4 v0, 0x0

    .line 426
    :goto_0
    return v0

    .line 415
    :cond_0
    instance-of v0, p1, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    if-eqz v0, :cond_1

    .line 416
    const/4 v0, 0x3

    goto :goto_0

    .line 417
    :cond_1
    instance-of v0, p1, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    if-eqz v0, :cond_2

    .line 418
    const/4 v0, 0x2

    goto :goto_0

    .line 419
    :cond_2
    instance-of v0, p1, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    if-eqz v0, :cond_3

    .line 420
    const/4 v0, 0x4

    goto :goto_0

    .line 421
    :cond_3
    instance-of v0, p1, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    if-eqz v0, :cond_4

    .line 422
    const/4 v0, 0x1

    goto :goto_0

    .line 424
    :cond_4
    const-string v0, "TMSAppMngr"

    const-string v1, "Exit getAppCategory. No matching category found. hence returning null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getHolderAppList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V
    .locals 4
    .param p1, "holderIntfc"    # Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .prologue
    .line 354
    const-string v1, "TMSAppMngr"

    const-string v2, "getHolderAppList enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    if-nez p1, :cond_0

    .line 357
    const-string v1, "TMSAppMngr"

    .line 358
    const-string v2, "getHolderAppList exit.Invalid input params"

    .line 357
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :goto_0
    return-void

    .line 363
    :cond_0
    invoke-interface {p1}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->getAppList()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 370
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getHolderAppList exit.Added "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 371
    invoke-interface {p1}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->getAppList()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " apps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 370
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 365
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iput-object p1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .line 367
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private getTopPackage()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1598
    const-string v2, "TMSAppMngr"

    const-string v3, "Enter getTopPackage"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 1600
    .local v1, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v0, 0x0

    .line 1601
    .local v0, "pkgName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_0

    .line 1603
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 1605
    :cond_0
    return-object v0
.end method

.method private regForAppStatusEvents()V
    .locals 6

    .prologue
    .line 736
    const-string v4, "TMSAppMngr"

    const-string v5, "regForAppStatusEvents Enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    .line 738
    .local v3, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 740
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 746
    const-string v4, "TMSAppMngr"

    const-string v5, "regForAppStatusEvents exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    return-void

    .line 742
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 743
    .local v0, "holder":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .line 744
    .local v1, "holderInt":Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;
    invoke-interface {v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z

    goto :goto_0
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    .line 1559
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    if-eqz v0, :cond_0

    .line 1560
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1562
    :cond_0
    return-void
.end method

.method private removeHolderAppList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V
    .locals 5
    .param p1, "holderIntfc"    # Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .prologue
    .line 380
    const-string v2, "TMSAppMngr"

    const-string v3, "removeHolderAppList enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    if-nez p1, :cond_0

    .line 383
    const-string v2, "TMSAppMngr"

    .line 384
    const-string v3, "removeholderAppList holderIntfc==null"

    .line 383
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 389
    const-string v2, "TMSAppMngr"

    .line 390
    const-string v3, "removeholderAppList mAppInfoList is empty"

    .line 389
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 393
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppCategory(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)I

    move-result v0

    .line 394
    .local v0, "category":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 405
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->deleteCategory(I)Z

    .line 407
    const-string v2, "TMSAppMngr"

    const-string v3, "removeHolderAppList exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 394
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 396
    .local v1, "crntAppInfo":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    if-ne v2, p1, :cond_2

    .line 399
    const-string v2, "TMSAppMngr"

    .line 400
    const-string v4, "removeHolderAppList removing app"

    .line 399
    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private removePkgFromList(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 903
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removePkgFormList enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 905
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->delete(Ljava/lang/String;)Z

    .line 909
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "removePkgFormList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 1565
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    if-eqz v0, :cond_0

    .line 1566
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1568
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mObserver:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;

    .line 1569
    return-void
.end method

.method private updatePkgFromList(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 2
    .param p1, "newAppInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 917
    const-string v0, "TMSAppMngr"

    const-string v1, "updatePkgFromList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 922
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "updatePkgFromList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    return-void
.end method


# virtual methods
.method public addToNotificationSupportedApps(I)V
    .locals 3
    .param p1, "appID"    # I

    .prologue
    .line 1544
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addToNotificationSupportedApps() enter appID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1545
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1550
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "addToNotificationSupportedApps() exit  "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    return-void
.end method

.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1171
    const-string v0, "TMSAppMngr"

    const-string v1, "deinit enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z

    .line 1173
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1174
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    .line 1175
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1176
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    .line 1177
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->unregisterContentObserver()V

    .line 1178
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->deinit()Z

    .line 1179
    monitor-enter p0

    .line 1180
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->delete()V

    .line 1179
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1183
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->deinit()V

    .line 1185
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 1186
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 1189
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "deinit exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    return-void

    .line 1179
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public deregisterAppHolder(Ljava/lang/String;)V
    .locals 3
    .param p1, "holderName"    # Ljava/lang/String;

    .prologue
    .line 269
    if-nez p1, :cond_0

    .line 271
    const-string v0, "TMSAppMngr"

    .line 272
    const-string v1, "deregisterAppHolder exit.Inavlid params"

    .line 271
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-void

    .line 275
    :cond_0
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deregisterAppHolder .holderName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 276
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 275
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->removeHolderAppList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    const-string v0, "TMSAppMngr"

    const-string v1, "deregisterAppHolder exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 6
    .param p1, "appId"    # I

    .prologue
    const/4 v1, 0x0

    .line 613
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 615
    const-string v3, "TMSAppMngr"

    const-string v4, "getAppInfoFromAppId: No Apps found. returning null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    :cond_0
    :goto_0
    return-object v1

    .line 619
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getPackageNameFromAppId(I)Ljava/lang/String;

    move-result-object v2

    .line 620
    .local v2, "packageName":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 621
    const-string v3, "TMSAppMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getAppInfoFromAppId: No Apps found with appID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " retruning null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 624
    :cond_2
    const/4 v1, 0x0

    .line 626
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    monitor-enter p0

    .line 627
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    if-eqz v3, :cond_3

    .line 628
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object v1, v0

    .line 626
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    if-nez v1, :cond_0

    .line 633
    const-string v3, "TMSAppMngr"

    const-string v4, "getAppInfoFromAppId Exit return null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 626
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 2
    .param p1, "packName"    # Ljava/lang/String;

    .prologue
    .line 685
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 687
    :cond_0
    const-string v0, "TMSAppMngr"

    .line 688
    const-string v1, "getAppInfoFromAppName exit.Inavlid input params "

    .line 687
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const/4 v0, 0x0

    .line 692
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    goto :goto_0
.end method

.method public getAppInfoRunningTop()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 650
    sget-object v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 651
    .local v2, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v1, 0x0

    .line 652
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v3, :cond_0

    .line 654
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 656
    :cond_0
    if-nez v1, :cond_2

    .line 657
    const-string v3, "TMSAppMngr"

    const-string v5, " There is no topActivity"

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 673
    :cond_1
    :goto_0
    return-object v0

    .line 661
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 662
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_3

    .line 663
    const-string v3, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " mAppInfoMap does not have "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 664
    goto :goto_0

    .line 667
    :cond_3
    iget v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    const/16 v5, 0x14

    if-eq v3, v5, :cond_1

    .line 668
    const-string v3, "TMSAppMngr"

    const-string v5, " mAppStatus is not foreground"

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 669
    goto :goto_0
.end method

.method public getAppStatus(I)I
    .locals 4
    .param p1, "appId"    # I

    .prologue
    .line 705
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAppStatus exit.appId= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 707
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_0

    .line 709
    const-string v1, "TMSAppMngr"

    const-string v2, "getAppStatus AppInfo not found"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const/4 v1, -0x1

    .line 714
    :goto_0
    return v1

    .line 712
    :cond_0
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAppStatus exit .Status is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 713
    iget v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 712
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    goto :goto_0
.end method

.method public getApplicationMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    const-string v0, "TMSAppMngr"

    const-string v1, "getApplicationList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 438
    const-string v0, "TMSAppMngr"

    .line 439
    const-string v1, "getApplicationList mAppInfoList size is 0"

    .line 438
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    const-string v0, "TMSAppMngr"

    .line 442
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getApplicationList exit.Return list of size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 442
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 441
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getCertifiedAppList()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 448
    const-string v5, "TMSAppMngr"

    const-string v6, "getCertifiedAppList enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 451
    const-string v5, "TMSAppMngr"

    .line 452
    const-string v6, "getCertifiedAppList mAppInfoList size is 0"

    .line 451
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 455
    .local v2, "certifiedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v5, "TMSAppMngr"

    .line 456
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getCertifiedAppList mAppInfoList size is "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 455
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getCertifiedAppsFromCategory(I)Ljava/util/List;

    move-result-object v1

    .line 458
    .local v1, "certifiedAppPackageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 459
    const-string v5, "TMSAppMngr"

    .line 460
    const-string v6, "getCertifiedAppList certifiedAppPackageNames is null"

    .line 459
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 479
    .end local v2    # "certifiedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :goto_0
    return-object v2

    .line 463
    .restart local v2    # "certifiedList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 472
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_4

    .line 473
    const-string v5, "TMSAppMngr"

    .line 474
    const-string v6, "getCertifiedAppList certifiedList size is 0"

    .line 473
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 475
    goto :goto_0

    .line 463
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 464
    .local v3, "packge":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 465
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 466
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Adding certified app "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 477
    .end local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v3    # "packge":Ljava/lang/String;
    :cond_4
    const-string v4, "TMSAppMngr"

    .line 478
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getCertifiedAppList certified apps size is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 477
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getNegotiatedServerRtpPayloads(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1465
    .local p1, "clientPyldList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v5, "TMSAppMngr"

    const-string v6, "getNegotiatedServerRtpPayloads enter "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1467
    .local v4, "supportedPylds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_4

    .line 1469
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    if-eqz v5, :cond_3

    .line 1471
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    invoke-interface {v5}, Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;->getServerSupportedRtpPayloads()Ljava/util/ArrayList;

    move-result-object v3

    .line 1472
    .local v3, "serverPyldList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1473
    .local v1, "clinetPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1496
    .end local v1    # "clinetPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v3    # "serverPyldList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_0
    const-string v5, "TMSAppMngr"

    const-string v6, "getNegotiatedServerRtpPayloads exit "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    return-object v4

    .line 1475
    .restart local v1    # "clinetPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v3    # "serverPyldList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1476
    .local v2, "pyld":I
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1477
    .local v0, "ServerPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1479
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v2, :cond_2

    .line 1481
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1488
    .end local v0    # "ServerPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v1    # "clinetPyldListItrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v2    # "pyld":I
    .end local v3    # "serverPyldList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_3
    const-string v5, "TMSAppMngr"

    const-string v6, "getNegotiatedServerRtpPayloads registerIRtpParamsHolder is null "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1493
    :cond_4
    const-string v5, "TMSAppMngr"

    const-string v6, "getNegotiatedServerRtpPayloads rtpPayloads size is 0"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getNotificationSupportedApps()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1522
    const-string v1, "TMSAppMngr"

    const-string v2, "getNotificationSupportedApps() enter "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1525
    const/4 v0, 0x0

    .line 1530
    :goto_0
    return-object v0

    .line 1528
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mNotificationSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1529
    .local v0, "apps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "TMSAppMngr"

    const-string v2, "getNotificationSupportedApps() exit "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getServerSupportedRtpPayloads()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1506
    const-string v0, "TMSAppMngr"

    const-string v1, "getServerSupportedRtpPayloads() enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    if-eqz v0, :cond_0

    .line 1509
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    invoke-interface {v0}, Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;->getServerSupportedRtpPayloads()Ljava/util/ArrayList;

    move-result-object v0

    .line 1517
    :goto_0
    return-object v0

    .line 1513
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "getServerSupportedRtpPayloads() registerIRtpParamsHolder is null exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    const-string v0, "TMSAppMngr"

    const-string v1, "getServerSupportedRtpPayloads() exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 1

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    if-nez v0, :cond_0

    .line 198
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z

    .line 202
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    .line 203
    return-void
.end method

.method public isRemotingInfoRtpServer(II)Z
    .locals 5
    .param p1, "appId"    # I
    .param p2, "profileId"    # I

    .prologue
    const/4 v1, 0x0

    .line 539
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isRemotingInfoRtpServer "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 543
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_1

    .line 544
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isRemotingInfoRtpServer : no app with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    :cond_0
    :goto_0
    return v1

    .line 548
    :cond_1
    const-string v2, "RTP"

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 549
    const-string v1, "TMSAppMngr"

    const-string v2, "isRemotingInfoRtpServer : It is RTP"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onAppStatusChange(Ljava/lang/String;I)V
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    const/16 v3, 0x14

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 757
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAppStatusChange enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v7

    .line 760
    .local v7, "eventedAppinfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 762
    invoke-direct {p0, p1, p2, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->changeAppStatus(Ljava/lang/String;IZ)V

    .line 763
    iget v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->checkIfAppIsAdvrtsdForClient(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "VNC"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 765
    if-ne v3, p2, :cond_0

    .line 767
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    .line 768
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mPreviousAllowedApp "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V

    .line 783
    :cond_0
    :goto_0
    sget v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mCurrentLaunchingApp:I

    iget v1, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    if-ne v0, v1, :cond_4

    .line 785
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAppStatusChange launched apps status recvd, status is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    sput v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mCurrentLaunchingApp:I

    .line 819
    :cond_1
    :goto_1
    return-void

    .line 772
    :cond_2
    const-string v0, "VNC"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    if-ne v3, p2, :cond_0

    .line 776
    iget v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_3

    .line 778
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    .line 780
    :cond_3
    invoke-virtual {p0, v7, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V

    goto :goto_0

    .line 790
    :cond_4
    const-string v0, "TMSAppMngr"

    const-string v1, "onAppStatusChange mCurrentLaunchingApp = which doesnot match "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 795
    :cond_5
    if-ne v3, p2, :cond_1

    .line 797
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 798
    const-string v1, "content://com.samsung.mirrorlink.acms.pkgnames/pkgname"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "pkgname"

    aput-object v3, v2, v8

    .line 799
    const-string v3, "pkgname=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    const/4 v5, 0x0

    .line 797
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 801
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_6

    .line 802
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "query returns null, request framebuffer block! << "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    invoke-virtual {p0, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncFrameBufferBlocked(I)V

    goto :goto_1

    .line 806
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 807
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " is in the white list, framebuffer is allowed ."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 814
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 811
    :cond_7
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "request framebuffer block! << "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    invoke-virtual {p0, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncFrameBufferBlocked(I)V

    goto :goto_2
.end method

.method public onApplicationStarted(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1573
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter onApplicationStarted << "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1574
    if-nez p1, :cond_1

    .line 1575
    const-string v1, "TMSAppMngr"

    const-string v2, "onApplicationStarted : Package name is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    :cond_0
    :goto_0
    return-void

    .line 1578
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTopPackage()Ljava/lang/String;

    move-result-object v0

    .line 1579
    .local v0, "pkgName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1580
    const/16 v1, 0x14

    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onAppStatusChange(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onApplicationStopping(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1585
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter onApplicationStopping << "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    if-nez p1, :cond_1

    .line 1587
    const-string v1, "TMSAppMngr"

    const-string v2, "onApplicationStarted : Package name is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    :cond_0
    :goto_0
    return-void

    .line 1590
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTopPackage()Ljava/lang/String;

    move-result-object v0

    .line 1591
    .local v0, "pkgName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1592
    const/16 v1, 0x15

    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onAppStatusChange(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onPackageStatusChange(Ljava/lang/String;Ljava/util/Map;)V
    .locals 11
    .param p1, "state"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "newAppInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const/4 v10, 0x0

    .line 829
    const-string v7, "TMSAppMngr"

    const-string v8, "onPackageStatusChange enter"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    const/16 v8, 0x33

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 831
    .local v4, "msg":Landroid/os/Message;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 833
    .local v1, "appidbuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 835
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTopPackage()Ljava/lang/String;

    move-result-object v6

    .line 837
    .local v6, "topPackage":Ljava/lang/String;
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 838
    const-string v7, "TMSAppMngr"

    const-string v8, "onPackageStatusChange ACTION_PACKAGE_CHANGED"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    .line 870
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 871
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v1, v10, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 872
    .local v2, "appids":Ljava/lang/String;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 873
    .local v5, "msgbundle":Landroid/os/Bundle;
    const-string v7, "AppID"

    invoke-virtual {v5, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    invoke-virtual {v4, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 875
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    invoke-virtual {v7, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 877
    .end local v2    # "appids":Ljava/lang/String;
    .end local v5    # "msgbundle":Landroid/os/Bundle;
    :cond_2
    const-string v7, "TMSAppMngr"

    const-string v8, "onPackageStatusChange exit"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    return-void

    .line 839
    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 840
    .local v3, "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 841
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->updatePkgFromList(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    .line 842
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 843
    const-string v7, "TMSAppMngr"

    const-string v9, "Updated app is in foreground. Context information needs to be changed"

    invoke-static {v7, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual {p0, v7, v10}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V

    .line 846
    :cond_4
    if-eqz v0, :cond_0

    iget v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    if-eqz v7, :cond_0

    .line 847
    new-instance v7, Ljava/lang/StringBuilder;

    iget v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 850
    .end local v3    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_5
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 851
    const-string v7, "TMSAppMngr"

    const-string v8, "onPackageStatusChange ACTION_PACKAGE_REMOVED"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 853
    .restart local v3    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 854
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->removePkgFromList(Ljava/lang/String;)V

    .line 855
    if-eqz v0, :cond_6

    iget v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    if-eqz v7, :cond_6

    .line 856
    new-instance v7, Ljava/lang/StringBuilder;

    iget v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 859
    .end local v3    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_7
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 860
    const-string v7, "TMSAppMngr"

    const-string v8, "onPackageStatusChange ACTION_PACKAGE_ADDED"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 862
    .restart local v3    # "info":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->addPkgToList(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    .line 863
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 864
    if-eqz v0, :cond_8

    iget v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    if-eqz v7, :cond_8

    .line 865
    new-instance v7, Ljava/lang/StringBuilder;

    iget v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public prepareTempFileFromAssets(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 13
    .param p1, "assetName"    # Ljava/lang/String;
    .param p2, "tempAssetName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z

    .prologue
    .line 1337
    new-instance v6, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "/data/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/files/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1338
    .local v6, "f":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1339
    const-string v10, "TMSAppMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "TMServerDevice.prepareTempFileFromAssets appinfo"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " file already exist "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    if-nez p3, :cond_0

    .line 1342
    const-string v10, "TMSAppMngr"

    const-string v11, "TMServerDevice.prepareTempFileFromAssets exit.File exist and overwrite is false "

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1409
    :goto_0
    return-object v10

    .line 1347
    :cond_0
    const-string v10, "TMSAppMngr"

    const-string v11, "TMServerDevice.prepareTempFileFromAssets deleting file since it exists and overwrite is true "

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1349
    const-string v10, "TMSAppMngr"

    const-string v11, "file deleted successfully"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    :cond_1
    const/4 v2, 0x0

    .line 1356
    .local v2, "deviceIcon":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 1359
    .local v9, "out":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 1360
    .local v0, "assetMngr":Landroid/content/res/AssetManager;
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Common/"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 1361
    const/16 v10, 0x400

    new-array v1, v10, [B

    .line 1362
    .local v1, "b":[B
    move-object v7, p2

    .line 1363
    .local v7, "filename":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    const/4 v11, 0x0

    invoke-virtual {v10, v7, v11}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v9

    .line 1365
    :goto_1
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v8

    .local v8, "len":I
    if-gtz v8, :cond_2

    .line 1369
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1370
    const/4 v9, 0x0

    .line 1371
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1409
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    .line 1367
    :cond_2
    const/4 v10, 0x0

    :try_start_1
    invoke-virtual {v9, v1, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1373
    .end local v0    # "assetMngr":Landroid/content/res/AssetManager;
    .end local v1    # "b":[B
    .end local v7    # "filename":Ljava/lang/String;
    .end local v8    # "len":I
    :catch_0
    move-exception v3

    .line 1375
    .local v3, "e":Ljava/io/IOException;
    if-eqz v9, :cond_3

    .line 1379
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1389
    :cond_3
    :goto_2
    if-eqz v2, :cond_4

    .line 1393
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1404
    :cond_4
    :goto_3
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 1405
    const-string v10, "TMSAppMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "prepareTempFileFromAssets exception hpnd "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 1381
    :catch_1
    move-exception v5

    .line 1384
    .local v5, "eout":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 1385
    const-string v10, "TMSAppMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "prepareTempFileFromAssets out.close() exception hpnd "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1395
    .end local v5    # "eout":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 1398
    .local v4, "edev":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 1399
    const-string v10, "TMSAppMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "prepareTempFileFromAssets deviceIcon.close() exception hpnd "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;)V
    .locals 2
    .param p1, "appStatusEventsListener"    # Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    .prologue
    .line 725
    const-string v0, "TMSAppMngr"

    const-string v1, "RegAppStatusUpdateListener Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    .line 727
    const-string v0, "TMSAppMngr"

    const-string v1, "RegAppStatusUpdateListener Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    return-void
.end method

.method public registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V
    .locals 3
    .param p1, "holderName"    # Ljava/lang/String;
    .param p2, "holderIntfc"    # Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .prologue
    .line 230
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 232
    :cond_0
    const-string v0, "TMSAppMngr"

    .line 233
    const-string v1, "registerAppHolder exit.Inavlid params"

    .line 232
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 236
    :cond_1
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerAppHolder .holderName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    invoke-interface {p2, p0}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z

    .line 241
    invoke-direct {p0, p2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getHolderAppList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 242
    const-string v0, "TMSAppMngr"

    const-string v1, "registerAppHolder exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerCallback(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;)V
    .locals 2
    .param p1, "cb"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

    .prologue
    .line 210
    const-string v0, "TMSAppMngr"

    const-string v1, "registerCallback enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mStatusCallack:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

    .line 212
    const-string v0, "TMSAppMngr"

    const-string v1, "registerCallback exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method public registerIRtpParamsHolder(Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;)V
    .locals 2
    .param p1, "holderIntfc"    # Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    .prologue
    .line 255
    const-string v0, "TMSAppMngr"

    const-string v1, "registerIRtpParamsHolder enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    .line 257
    const-string v0, "TMSAppMngr"

    const-string v1, "registerIRtpParamsHolder exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public removeUnsupportedVncApp(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1535
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client is not supporting app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1537
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "VNC"

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1538
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1539
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->removeUnsupportedVncApp(Ljava/lang/String;)V

    .line 1541
    :cond_0
    return-void
.end method

.method public sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V
    .locals 8
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .param p2, "sendfalseAppID"    # Z

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 995
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendAppValuesToVNC topApp enter sendfalseAppID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    if-nez p1, :cond_1

    .line 998
    const-string v2, "TMSAppMngr"

    const-string v3, "sendAppValuesToVNC appInfo is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    :cond_0
    :goto_0
    return-void

    .line 1001
    :cond_1
    iget v0, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1023
    .local v0, "appId":I
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendAppValuesToVNC proceede to sent params to VNC mPreviousTopAppId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousTopAppId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendAppValuesToVNC evented app id is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iget v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousTopAppId:I

    if-ne v0, v2, :cond_2

    if-ne v0, v6, :cond_b

    .line 1030
    :cond_2
    const-string v2, "TMSAppMngr"

    const-string v3, "sendAppValuesToVNC evented appid/topapp is not same as previous top app id"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    new-instance v1, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 1032
    .local v1, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    if-eqz p2, :cond_3

    .line 1033
    const-string v2, "appId"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1036
    :goto_1
    iget v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    if-eq v6, v2, :cond_4

    .line 1038
    const-string v2, "AppCatgryTrstLvl"

    iget v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1044
    :goto_2
    iget v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    if-eq v6, v2, :cond_5

    .line 1046
    const-string v2, "CntntCatgryTrstLvl"

    iget v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1052
    :goto_3
    iget v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    if-eq v6, v2, :cond_6

    .line 1054
    const-string v2, "AppCategory"

    iget v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1060
    :goto_4
    iget v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    if-eq v6, v2, :cond_7

    .line 1062
    const-string v2, "CntCategory"

    iget v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1068
    :goto_5
    const-string v2, "rulesInfo"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1069
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1071
    const-string v2, "landscape"

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1073
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 1089
    :goto_6
    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousTopAppId:I

    .line 1090
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v2, :cond_0

    .line 1091
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->sendAppCntxtInfo(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1035
    :cond_3
    const-string v2, "appId"

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_1

    .line 1042
    :cond_4
    const-string v2, "AppCatgryTrstLvl"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_2

    .line 1050
    :cond_5
    const-string v2, "CntntCatgryTrstLvl"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_3

    .line 1058
    :cond_6
    const-string v2, "AppCategory"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_4

    .line 1066
    :cond_7
    const-string v2, "CntCategory"

    invoke-virtual {v1, v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_5

    .line 1075
    :cond_8
    const-string v2, "portrait"

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1077
    const-string v2, "Orientation"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_6

    .line 1081
    :cond_9
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v7}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_6

    .line 1086
    :cond_a
    const-string v2, "Orientation"

    invoke-virtual {v1, v2, v7}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    goto :goto_6

    .line 1095
    .end local v1    # "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    :cond_b
    const-string v2, "TMSAppMngr"

    const-string v3, "sendAppValuesToVNC evented appid/topapp is same as previous top app id!!!!!!!.Eventing might have been already done"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setAudioIplAndMpl(II)V
    .locals 9
    .param p1, "audioIpl"    # I
    .param p2, "audioMpl"    # I

    .prologue
    .line 1420
    const-string v6, "TMSAppMngr"

    const-string v7, "setAudioIplandMpl enter "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    if-eqz v6, :cond_2

    .line 1423
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    invoke-interface {v6, p1, p2}, Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;->setIplAndMpl(II)Ljava/util/List;

    move-result-object v5

    .line 1424
    .local v5, "updatedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mRtpParamsHolder:Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;

    check-cast v6, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    invoke-virtual {p0, v6}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->updateHolderApplicationList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 1425
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1426
    .local v1, "appidbuilder":Ljava/lang/StringBuilder;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1429
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 1430
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    const/16 v7, 0x33

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 1431
    .local v3, "msg":Landroid/os/Message;
    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1432
    .local v2, "appids":Ljava/lang/String;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1433
    .local v4, "msgbundle":Landroid/os/Bundle;
    const-string v6, "AppID"

    invoke-virtual {v4, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1435
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1443
    .end local v1    # "appidbuilder":Ljava/lang/StringBuilder;
    .end local v2    # "appids":Ljava/lang/String;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "msgbundle":Landroid/os/Bundle;
    .end local v5    # "updatedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    :goto_1
    const-string v6, "TMSAppMngr"

    const-string v7, "setAudioIplandMpl exit "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    return-void

    .line 1426
    .restart local v1    # "appidbuilder":Ljava/lang/StringBuilder;
    .restart local v5    # "updatedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1427
    .local v0, "appId":Ljava/lang/Integer;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1440
    .end local v0    # "appId":Ljava/lang/Integer;
    .end local v1    # "appidbuilder":Ljava/lang/StringBuilder;
    .end local v5    # "updatedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_2
    const-string v6, "TMSAppMngr"

    const-string v7, "setAudioIplandMpl registerIRtpParamsHolder is null exit "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setClientSupportedPayloads(Ljava/util/ArrayList;I)V
    .locals 2
    .param p2, "profileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1453
    .local p1, "rtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v0, "TMSAppMngr"

    const-string v1, "setClientSupportedPayloads enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    const-string v0, "TMSAppMngr"

    const-string v1, "setClientSupportedPayloads exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    return-void
.end method

.method public setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 2
    .param p1, "engine"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 344
    const-string v0, "TMSAppMngr"

    const-string v1, "setNativeInterface"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 346
    return-void
.end method

.method public setVNCenable(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 983
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVVNCenable enter flag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setVNCenable(Z)V

    .line 986
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "setVVNCenable exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    return-void
.end method

.method public startApp(II)Z
    .locals 5
    .param p1, "appId"    # I
    .param p2, "profileId"    # I

    .prologue
    const/4 v1, 0x0

    .line 492
    const-string v2, "TMSAppMngr"

    const-string v3, "startApp enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 495
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_0

    .line 497
    const-string v2, "TMSAppMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startApp : no app with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :goto_0
    return v1

    .line 500
    :cond_0
    sput p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mCurrentLaunchingApp:I

    .line 501
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->startApp(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 503
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 504
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    const-string v3, "VNC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isFirstVncApp:Z

    if-eqz v2, :cond_1

    .line 505
    const-string v2, "TMSAppMngr"

    const-string v3, "Started first vnc app and mirroring started on screen"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isFirstVncApp:Z

    .line 510
    :cond_1
    const-string v1, "TMSAppMngr"

    const-string v2, "startApp exit.App,  ,started succesfully"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mStatusCallack:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

    if-eqz v1, :cond_2

    .line 515
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mStatusCallack:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;

    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;->cb(IILjava/lang/Object;)V

    .line 518
    :cond_2
    const/16 v1, 0x14

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 520
    const/4 v1, 0x1

    goto :goto_0

    .line 524
    :cond_3
    const-string v2, "TMSAppMngr"

    const-string v3, "startApp exit.App,  ,staring failed"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopApp(I)Z
    .locals 6
    .param p1, "appId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 563
    const-string v4, "TMSAppMngr"

    const-string v5, "stopApp enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 565
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 567
    .local v1, "sendFakeNotify":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 569
    const-string v3, "TMSAppMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "stopApp : no app id "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    :goto_0
    return v2

    .line 581
    :cond_0
    iget v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    const/16 v4, 0x15

    if-ne v2, v4, :cond_1

    .line 583
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 590
    :cond_1
    const-string v2, "TMSAppMngr"

    const-string v4, "stopApp Call forceStopPackage"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->stopApp(Ljava/lang/String;)Z

    .line 593
    const/16 v2, 0x16

    iput v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 595
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 597
    const-string v2, "TMSAppMngr"

    const-string v4, "stopApp sending fake notify"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {p0, v2, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onAppStatusChange(Ljava/lang/String;I)V

    .line 601
    :cond_2
    const-string v2, "TMSAppMngr"

    const-string v4, "stopApp Exit "

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 602
    goto :goto_0
.end method

.method public updateApplicationList()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    const-string v5, "TMSAppMngr"

    const-string v6, "updateApplicationList enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppHolders:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 292
    .local v4, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 294
    .local v3, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 315
    const-string v5, "TMSAppMngr"

    const-string v6, "updateApplicationList exit"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    return-object v5

    .line 296
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 297
    .local v1, "holder":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .line 299
    .local v2, "holderInt":Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;
    invoke-interface {v2}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->getAppList()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 300
    .local v0, "crntAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v5

    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppId(Ljava/lang/String;)I

    move-result v5

    iput v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 302
    iget v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    const/4 v7, -0x1

    if-ne v5, v7, :cond_2

    .line 303
    const-string v5, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "AppId not found in db for package + "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!!! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_2
    iput-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .line 308
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    const-string v7, "TMSAppMngr"

    .line 310
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "updateApplicationList added new app id="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 311
    iget v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " from holder "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 312
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 310
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 309
    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateHolderApplicationList(Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V
    .locals 5
    .param p1, "holderInt"    # Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .prologue
    .line 325
    const-string v2, "TMSAppMngr"

    const-string v3, "updateHolderApplicationList enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const/4 v1, 0x0

    .line 328
    .local v1, "holderApps":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "holderApps":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {p1}, Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;->getAppList()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 330
    .restart local v1    # "holderApps":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 334
    const-string v2, "TMSAppMngr"

    const-string v3, "updateHolderApplicationList exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    return-void

    .line 330
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 332
    .local v0, "crntHolderAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppInfoMap:Ljava/util/Map;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public vncFrameBufferBlocked(I)V
    .locals 12
    .param p1, "appId"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x1

    .line 1199
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "vncFrameBufferBlocked appId = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    sget-boolean v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    if-eqz v6, :cond_0

    .line 1202
    const-string v6, "TMSAppMngr"

    const-string v7, "too early! ignore!"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    sput-boolean v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    .line 1283
    :goto_0
    return-void

    .line 1216
    :cond_0
    const/4 v1, 0x0

    .line 1218
    .local v1, "clientId":Ljava/lang/Object;
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const-string v7, "get"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 1219
    .local v4, "listener":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "net.mirrorlink.clientid"

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 1229
    .end local v1    # "clientId":Ljava/lang/Object;
    .end local v4    # "listener":Ljava/lang/reflect/Method;
    :goto_1
    const-string v6, "VW-Mibstd2"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "VW MirrorLink"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1230
    const-string v6, "MLC01"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "VWAG_VOLKSWAGEN"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1231
    const-string v6, "VWAG_SKODA"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1233
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 1234
    .local v5, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    if-ne v6, v11, :cond_3

    .line 1235
    :cond_2
    const-string v6, "TMSAppMngr"

    const-string v7, "do not proceed to vnc block during call!"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1220
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    .restart local v1    # "clientId":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 1221
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SystemProperties.get() "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1222
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .line 1223
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SystemProperties.get() "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1224
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v2

    .line 1225
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SystemProperties.get() "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1226
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v2

    .line 1227
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-string v6, "TMSAppMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SystemProperties.get() "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1239
    .end local v1    # "clientId":Ljava/lang/Object;
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_3
    const-string v6, "TMSAppMngr"

    const-string v7, "VW skip to set client native UI msg"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 1241
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mPreviousAllowedApp:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1242
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_4

    .line 1243
    const/high16 v6, 0x10020000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1245
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->cntxt:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1246
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$2;

    invoke-direct {v7, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$2;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    .line 1251
    const-wide/16 v8, 0x3e8

    .line 1246
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    .line 1275
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_4
    :goto_2
    sput-boolean v11, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mUseBlock:Z

    .line 1276
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$3;

    invoke-direct {v7, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$3;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    .line 1281
    const-wide/16 v8, 0x1f4

    .line 1276
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1282
    const-string v6, "TMSAppMngr"

    const-string v7, "vncFrameBufferBlocked exit"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1252
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :catch_4
    move-exception v2

    .line 1253
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "TMSAppMngr"

    const-string v7, "Exception caught: can not Find the activity"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 1259
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v6, "TMSAppMngr"

    const-string v7, "terminateVNCSession"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1261
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v6, :cond_4

    .line 1262
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->terminateVNCSession()V

    goto :goto_2

    .line 1267
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_6
    const-string v6, "TMSAppMngr"

    const-string v7, "send to HU, switch native UI event!"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    .line 1270
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    const/4 v6, -0x1

    iput v6, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1271
    const v6, -0xfff0001

    iput v6, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 1272
    invoke-virtual {p0, v0, v11}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V

    goto :goto_2
.end method

.method public vncTerminateTopApp()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1307
    const-string v4, "TMSAppMngr"

    const-string v5, "vncTerminateTopApp enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    sget-object v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    invoke-virtual {v4, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 1310
    .local v2, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v2, :cond_0

    .line 1311
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 1312
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1314
    .local v3, "topPckgName":Ljava/lang/String;
    :try_start_0
    sget-object v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 1315
    const-string v5, "forceStopPackage"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    .line 1314
    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1316
    .local v1, "listener":Ljava/lang/reflect/Method;
    sget-object v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1326
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    .end local v3    # "topPckgName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1317
    .restart local v3    # "topPckgName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1318
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not connect to channel."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1319
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 1320
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not connect to channel."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1321
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 1322
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "TMSAppMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not connect to channel."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
