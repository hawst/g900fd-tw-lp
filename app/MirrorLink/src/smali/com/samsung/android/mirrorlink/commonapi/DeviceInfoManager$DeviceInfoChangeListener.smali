.class Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;
.super Ljava/lang/Object;
.source "DeviceInfoManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceInfoChangeListener"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode()[I
    .locals 3

    .prologue
    .line 309
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->values()[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_AUDIO_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CLIENT_PIXEL_FORMAT_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DISPLAY_CONFIG_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DRIVEMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_FB_BLOCKING:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MIC_OPEN:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_NIGHTMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_REMOTE_DISPLAY_CONNECTION:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;)V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;-><init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;)V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;Ljava/lang/Object;)V
    .locals 4
    .param p1, "event"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, -0x1

    .line 316
    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 361
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p2

    .line 318
    check-cast v0, Landroid/os/Bundle;

    .line 319
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 320
    const-string v1, "CLIENT_MODEL_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 321
    const-string v1, "CLIENT_MODEL_NAME"

    .line 322
    const-string v2, "Not Available"

    .line 321
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_1
    const-string v1, "CLIENT_MODEL_NUMBER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 325
    const-string v1, "CLIENT_MODEL_NUMBER"

    .line 326
    const-string v2, "Not Available"

    .line 325
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_2
    const-string v1, "CLIENT_FRIENDLY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 329
    const-string v1, "CLIENT_FRIENDLY_NAME"

    .line 330
    const-string v2, "Not Available"

    .line 329
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_3
    const-string v1, "CLIENT_IDENTIFIER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 333
    const-string v1, "CLIENT_IDENTIFIER"

    .line 334
    const-string v2, "Not Available"

    .line 333
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_4
    const-string v1, "CLIENT_MANUFACTURER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 337
    const-string v1, "CLIENT_MANUFACTURER"

    .line 338
    const-string v2, "Not Available"

    .line 337
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_5
    const-string v1, "VERSION_MAJOR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 341
    const-string v1, "VERSION_MAJOR"

    .line 342
    const/4 v2, 0x1

    .line 341
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 344
    :cond_6
    const-string v1, "VERSION_MINOR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_7

    .line 345
    const-string v1, "VERSION_MINOR"

    .line 346
    const/4 v2, 0x0

    .line 345
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 348
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    # invokes: Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->isClientInfoChanged(Landroid/os/Bundle;)Z
    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->access$1(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Landroid/os/Bundle;)V

    .line 350
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->notifyDeviceInfoChanged(Landroid/os/Bundle;)V

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
