.class public Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;
.super Ljava/lang/Object;
.source "AcmsCore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsCore"

.field private static sAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;


# instance fields
.field private looper:Landroid/os/Looper;

.field private mApplicationContext:Landroid/content/Context;

.field private mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

.field private mLooperHandler:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "applicationContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    .line 67
    const-string v1, "AcmsCore"

    const-string v2, "AcmsCore"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;

    .line 70
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AcmsHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 71
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->startThread(Landroid/os/HandlerThread;)V

    .line 72
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    .line 73
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;-><init>(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Landroid/os/Looper;Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    .line 76
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    return-object v0
.end method

.method public static getAcmsCore(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;
    .locals 2
    .param p0, "appCxt"    # Landroid/content/Context;

    .prologue
    .line 83
    const-string v0, "AcmsCore"

    const-string v1, "AcmsCore.getAcmsCore() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-class v1, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    monitor-enter v1

    .line 85
    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->sAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->sAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->sAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private startThread(Landroid/os/HandlerThread;)V
    .locals 0
    .param p1, "handlerThread"    # Landroid/os/HandlerThread;

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    .line 80
    return-void
.end method


# virtual methods
.method public checkapps()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 338
    const-string v0, ""

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    .line 340
    :cond_0
    return-void
.end method

.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    const-string v0, "AcmsCore"

    const-string v1, "AcmsCore: inside DeInit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 106
    const-string v0, "AcmsCore"

    const-string v1, "AcmsCore: mLooperHandler is not null and making it null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->looper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 110
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->cleanUp()V

    .line 115
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->cleanUp()V

    .line 119
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->getMlConnectTimeEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->cleanUp()V

    .line 120
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;

    .line 121
    sput-object v2, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->sAcmsCore:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 122
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 93
    const-string v0, "AcmsCore"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 95
    const-string v0, "AcmsCore"

    const-string v1, "mLooperHandler is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 97
    :cond_0
    const-string v0, "AcmsCore"

    const-string v1, "Looper handler is not null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, ""

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public postToAcmsCoreHandler(Ljava/lang/String;I)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "what"    # I

    .prologue
    .line 322
    const-string v2, "AcmsCore"

    const-string v3, "Post to AcmsCoreHandler"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 324
    .local v1, "msg":Landroid/os/Message;
    iput p2, v1, Landroid/os/Message;->what:I

    .line 325
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 326
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 328
    .local v0, "isSuccess":Z
    if-eqz v0, :cond_0

    .line 330
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v2

    .line 331
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 332
    const-string v2, "AcmsCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incremented Counter Value: postToAcmsCoreHandler =>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    return-void
.end method
