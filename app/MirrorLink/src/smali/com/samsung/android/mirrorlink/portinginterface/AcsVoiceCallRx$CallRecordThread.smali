.class Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;
.super Ljava/lang/Thread;
.source "AcsVoiceCallRx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallRecordThread"
.end annotation


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mIsRecordThreadActive:Z

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->startThread()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V
    .locals 0

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->stopThread()V

    return-void
.end method

.method private getAudioRecordInstance(IIIII)Landroid/media/AudioRecord;
    .locals 6
    .param p1, "voiceDownlink"    # I
    .param p2, "samplingRate"    # I
    .param p3, "channelInMono"    # I
    .param p4, "encodingPcm16bit"    # I
    .param p5, "bufferSize"    # I

    .prologue
    .line 229
    new-instance v0, Landroid/media/AudioRecord;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    return-object v0
.end method

.method private startThread()V
    .locals 3

    .prologue
    .line 238
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startThread Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->start()V

    .line 244
    :cond_0
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startThread Exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method private stopThread()V
    .locals 4

    .prologue
    .line 251
    const-string v1, "TMSVoiceCallRx"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopThread Enter "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    if-nez v1, :cond_0

    .line 254
    const-string v1, "TMSVoiceCallRx"

    const-string v2, "stop: Already stopped"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :goto_0
    return-void

    .line 258
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    .line 261
    :try_start_0
    const-string v1, "TMSVoiceCallRx"

    const-string v2, "Call join "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TMSVoiceCallRx"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopThread join FAILED "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 113
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    .line 118
    const v0, 0xac44

    .line 119
    const/16 v1, 0x10

    .line 120
    const/4 v2, 0x2

    .line 117
    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v5

    .line 122
    .local v5, "bufferSize":I
    const/4 v9, 0x0

    .line 124
    .local v9, "bytesRead":I
    mul-int/lit8 v0, v5, 0x4

    new-array v8, v0, [B

    .line 126
    .local v8, "buffer":[B
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run:sizeInBytes "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " channel "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->access$0(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mAudioManager:Landroid/media/AudioManager;

    .line 128
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v7

    .line 130
    .local v7, "btStatePrev":Z
    const/4 v1, 0x3

    .line 131
    const v2, 0xac44

    .line 132
    const/16 v3, 0x10

    .line 133
    const/4 v4, 0x2

    move-object v0, p0

    .line 130
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->getAudioRecordInstance(IIIII)Landroid/media/AudioRecord;

    move-result-object v10

    .line 136
    .local v10, "recordInstance":Landroid/media/AudioRecord;
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run::state is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v10}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 139
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run: tate is NOT INITIALISED "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    .line 141
    const/4 v10, 0x0

    .line 223
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-virtual {v10}, Landroid/media/AudioRecord;->startRecording()V

    .line 147
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mIsRecordThreadActive:Z

    if-nez v0, :cond_2

    .line 218
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    .line 219
    const/4 v10, 0x0

    .line 222
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_2
    const/4 v0, 0x0

    const/16 v1, 0x800

    invoke-virtual {v10, v8, v0, v1}, Landroid/media/AudioRecord;->read([BII)I

    move-result v9

    .line 151
    if-lez v9, :cond_7

    .line 153
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run::bytesRead is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v6

    .line 155
    .local v6, "btState":Z
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v11

    .line 157
    .local v11, "speakerState":Z
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v12

    .line 158
    .local v12, "wiredState":Z
    if-nez v6, :cond_3

    if-nez v11, :cond_3

    if-eqz v12, :cond_4

    .line 160
    :cond_3
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run:: btState is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", speakerState is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wiredState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    if-eq v7, v6, :cond_1

    .line 163
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run::bt state is changed "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    move v7, v6

    .line 166
    goto :goto_1

    .line 169
    :cond_4
    if-eq v7, v6, :cond_6

    .line 171
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run::bt state is changed "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run::creating new record instance "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    .line 175
    const/4 v1, 0x3

    .line 176
    const v2, 0xac44

    .line 177
    const/16 v3, 0x10

    .line 178
    const/4 v4, 0x2

    move-object v0, p0

    .line 175
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->getAudioRecordInstance(IIIII)Landroid/media/AudioRecord;

    move-result-object v10

    .line 180
    invoke-virtual {v10}, Landroid/media/AudioRecord;->startRecording()V

    .line 182
    move v7, v6

    .line 184
    invoke-virtual {v10}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    .line 185
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run: tate is NOT INITIALISED "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v10}, Landroid/media/AudioRecord;->release()V

    .line 187
    const/4 v10, 0x0

    .line 188
    goto/16 :goto_0

    .line 191
    :cond_5
    const/4 v0, 0x0

    const/16 v1, 0x800

    invoke-virtual {v10, v8, v0, v1}, Landroid/media/AudioRecord;->read([BII)I

    move-result v9

    .line 193
    :cond_6
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "CallRecordThread run::routing through RTP "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    move-result-object v0

    const/16 v1, 0x3ec

    invoke-interface {v0, v1, v9, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;->onEvent(IILjava/lang/Object;)V

    goto/16 :goto_1

    .line 200
    .end local v6    # "btState":Z
    .end local v11    # "speakerState":Z
    .end local v12    # "wiredState":Z
    :cond_7
    const-string v0, "TMSVoiceCallRx"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallRecordThread run::bytesRead is Wrong "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
