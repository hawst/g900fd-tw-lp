.class Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;
.super Landroid/os/Handler;
.source "BtAppholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    .line 420
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 423
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 481
    const-string v6, "TMSBtAppholder"

    const-string v7, "handleMessage : default"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 426
    :pswitch_0
    const-string v6, "TMSBtAppholder"

    const-string v7, "handleMessage : TM_EVENT_TMSERVER_MAC_SET"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    :pswitch_1
    const-string v6, "TMSBtAppholder"

    const-string v7, "handleMessage : TM_EVENT_STATE_CHANGE_DELAYED"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget v7, p1, Landroid/os/Message;->arg1:I

    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v6, v7, v8}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    goto :goto_0

    .line 433
    :pswitch_2
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    if-nez v6, :cond_1

    .line 434
    const-string v6, "TMSBtAppholder"

    const-string v7, "Bluetooth adapter is null. Cannot make the device discoverable"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 438
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v3

    .line 439
    .local v3, "scanMode":I
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "handleMessage : TM_EVENT_ENABLE_DISCOVERABLE "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    const/4 v4, -0x1

    invoke-static {v6, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)V

    .line 442
    .local v4, "timeout":I
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "getDiscoverableTimeout"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 443
    .local v1, "listener":Ljava/lang/reflect/Method;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 444
    .end local v4    # "timeout":I
    .local v5, "timeout":I
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->checkMakeDiscoverable(II)Z
    invoke-static {v6, v3, v5}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;II)Z

    move-result v2

    .line 446
    .local v2, "makeDiscoverable":Z
    if-eqz v2, :cond_0

    .line 455
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v7, v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)V

    .line 456
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, " TM_EVENT_ENABLE_DISCOVERABLE : old timeout "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 457
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurDiscoverablePeriod:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " new timeout : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x78

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 456
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "setDiscoverableTimeout"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    sget-object v10, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 459
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/16 v9, 0x78

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v7

    .line 462
    const/16 v8, 0x17

    .line 463
    const/16 v9, 0x78

    .line 461
    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->callBluetoothAdapterSetScanMode(Landroid/bluetooth/BluetoothAdapter;II)V
    invoke-static {v6, v7, v8, v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Landroid/bluetooth/BluetoothAdapter;II)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    .line 465
    .end local v2    # "makeDiscoverable":Z
    :catch_0
    move-exception v0

    move v4, v5

    .line 466
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    .end local v5    # "timeout":I
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    .restart local v4    # "timeout":I
    :goto_1
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TM_EVENT_ENABLE_DISCOVERABLE error."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 467
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    :goto_2
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TM_EVENT_ENABLE_DISCOVERABLE error."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 469
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :goto_3
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TM_EVENT_ENABLE_DISCOVERABLE error."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 475
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v3    # "scanMode":I
    .end local v4    # "timeout":I
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 477
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$7(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    goto/16 :goto_0

    .line 469
    .restart local v1    # "listener":Ljava/lang/reflect/Method;
    .restart local v3    # "scanMode":I
    .restart local v5    # "timeout":I
    :catch_3
    move-exception v0

    move v4, v5

    .end local v5    # "timeout":I
    .restart local v4    # "timeout":I
    goto :goto_3

    .line 467
    .end local v4    # "timeout":I
    .restart local v5    # "timeout":I
    :catch_4
    move-exception v0

    move v4, v5

    .end local v5    # "timeout":I
    .restart local v4    # "timeout":I
    goto :goto_2

    .line 465
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :catch_5
    move-exception v0

    goto :goto_1

    .line 423
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
