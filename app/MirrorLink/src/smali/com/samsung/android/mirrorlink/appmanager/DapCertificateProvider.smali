.class public Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;
.super Ljava/lang/Object;
.source "DapCertificateProvider.java"


# static fields
.field public static final GET_CA_CERT_URL:Ljava/lang/String; = "https://eu-mlca.samsungosp.com/v2/ca/scep?operation=GetCACert"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSDapCertificateProvider"

.field public static final POST_DEV_CERT_URL:Ljava/lang/String; = "https://eu-mlca.samsungosp.com/v2/ca/devicecert"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const-string v0, "TMSDapCertificateProvider"

    const-string v1, "DapCertificateProvider:Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;->mContext:Landroid/content/Context;

    .line 81
    return-void
.end method


# virtual methods
.method public getCaCertificate()V
    .locals 2

    .prologue
    .line 84
    const-string v0, "TMSDapCertificateProvider"

    const-string v1, "getCaCertificate:Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->start()V

    .line 86
    return-void
.end method

.method public getDeviceCertificate(Ljava/lang/String;)V
    .locals 3
    .param p1, "querySting"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v1, "TMSDapCertificateProvider"

    const-string v2, "getDeviceCertificate:Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;-><init>(Landroid/content/Context;)V

    .line 91
    .local v0, "devCertThreadObj":Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->setQueryString(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->start()V

    .line 93
    return-void
.end method
