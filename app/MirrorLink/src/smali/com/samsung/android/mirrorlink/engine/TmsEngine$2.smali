.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$2;
.super Landroid/content/BroadcastReceiver;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine;->registerBuildInfoReqReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$2;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 1379
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1383
    const-string v1, "TMSEngine"

    const-string v2, "TmsEngine.onReceive enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1385
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.android.tmserver.service.BUILD_INFO_REQ"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1387
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$2;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->sendBuildVersion()V
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$22(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 1389
    :cond_0
    const-string v1, "TMSEngine"

    const-string v2, "TmsEngine.onReceive exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    return-void
.end method
