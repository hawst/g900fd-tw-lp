.class public Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
.super Ljava/lang/Object;
.source "OcspResponseEntry.java"


# static fields
.field private static final DEFAULT_BASE_GRACE_PERIOD:J = 0x870L

.field private static final DEFAULT_DRIVE_GRACE_PERIOD:J = 0x2d0L

.field private static final DEFAULT_QUERY_PERIOD:J = 0xa8L


# instance fields
.field private baseGracePeriod:J

.field private driveGracePeriod:J

.field private queryPeriod:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, 0xa8

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->queryPeriod:J

    .line 34
    const-wide/16 v0, 0x2d0

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->driveGracePeriod:J

    .line 35
    const-wide/16 v0, 0x870

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->baseGracePeriod:J

    .line 36
    return-void
.end method


# virtual methods
.method public getBaseGraceTimeperiod()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->baseGracePeriod:J

    return-wide v0
.end method

.method public getDriveGraceTimeperiod()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->driveGracePeriod:J

    return-wide v0
.end method

.method public getQueryTimeperiod()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->queryPeriod:J

    return-wide v0
.end method

.method public setBaseGraceTimeperiod(J)V
    .locals 0
    .param p1, "basegraceTimeperiod"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->baseGracePeriod:J

    .line 60
    return-void
.end method

.method public setDriveGraceTimeperiod(J)V
    .locals 0
    .param p1, "drivegraceTimeperiod"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->driveGracePeriod:J

    .line 52
    return-void
.end method

.method public setQueryTimeperiod(J)V
    .locals 0
    .param p1, "queryTimeperiod"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->queryPeriod:J

    .line 44
    return-void
.end method
