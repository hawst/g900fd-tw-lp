.class public Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;
.super Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;
.source "AcmsManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsManager"

.field private static sAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

.field private static sIsCreateAcmsManager:Z


# instance fields
.field private final mClientListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sIsCreateAcmsManager:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 34
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    .line 35
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sIsCreateAcmsManager:Z

    .line 36
    return-void
.end method

.method public static getAcmsManager(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    sget-boolean v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sIsCreateAcmsManager:Z

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 42
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    return-object v0
.end method

.method private handleAllDevApps(Z)V
    .locals 6
    .param p1, "mode"    # Z

    .prologue
    .line 197
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v3

    .line 198
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getAllDevApps()Ljava/util/List;

    move-result-object v2

    .line 200
    .local v2, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_1

    .line 201
    const-string v3, "AcmsManager"

    const-string v4, "No dev apps found in DB"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    return-void

    .line 205
    :cond_1
    if-eqz p1, :cond_3

    .line 206
    const-string v3, "AcmsManager"

    const-string v4, "DevMode is on: Hence including all valid devApps present in DB "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 209
    .local v1, "appId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 208
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v4

    .line 209
    invoke-virtual {v4, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v0

    .line 210
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v0, :cond_2

    .line 211
    const-string v4, "AcmsManager"

    .line 212
    const-string v5, "AcmsCore.handleAllDevApps appEntry null for given appId"

    .line 211
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v4

    .line 216
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    .end local v0    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v1    # "appId":Ljava/lang/String;
    :cond_3
    const-string v3, "AcmsManager"

    const-string v4, "DevMode is off: Hence moving all devApps present in DB to non-certified state "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 222
    .restart local v1    # "appId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 221
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v4

    .line 222
    invoke-virtual {v4, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v0

    .line 223
    .restart local v0    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v0, :cond_4

    .line 224
    const-string v4, "AcmsManager"

    .line 225
    const-string v5, "AcmsCore.handleDevIdCert appEntry null for given appId"

    .line 224
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 229
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v4

    .line 231
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 230
    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 239
    const-string v0, "AcmsManager"

    const-string v1, "AcmsManager: cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->sAcmsManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    .line 241
    return-void
.end method

.method public notifyRevocationCheckResult(Ljava/lang/String;I)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "result"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 173
    monitor-enter p0

    .line 175
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 176
    .local v2, "length":I
    const-string v3, "AcmsManager"

    .line 177
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AcmsManager.notifyResultRevocationCheck():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 177
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 176
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 188
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 173
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 182
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;

    .line 183
    invoke-interface {v3, p1, p2}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;->notifyRevocationCheckResult(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 180
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 173
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i":I
    .end local v2    # "length":I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public performRevocation()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    const-string v3, "AcmsManager"

    const-string v4, "Entered PerformRevocation Check"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 93
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    .line 94
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAllUncheckedApps()Ljava/util/List;

    move-result-object v2

    .line 95
    .local v2, "uncheckedAppEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-eqz v2, :cond_0

    .line 96
    const-string v3, "AcmsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Update Certificate: No of Apps: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 109
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 108
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    .line 109
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAllCertifiedApps()Ljava/util/List;

    move-result-object v0

    .line 110
    .local v0, "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-eqz v0, :cond_1

    .line 111
    const-string v3, "AcmsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "performRevocation(): No of Apps: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    .line 114
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 119
    :cond_1
    return-void

    .line 97
    .end local v0    # "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 98
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v4, "AcmsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Updating Certificate for: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v4

    .line 101
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v4, v5, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performRevocationCheck(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 48
    const-string v2, "AcmsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Entered PerformRevocation Check for package name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v2

    .line 52
    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryFromPackage(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v1

    .line 54
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v1, :cond_0

    .line 55
    const-string v2, "AcmsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PerformRevocation Check(): AppEntry is null for package name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 55
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 61
    const-string v2, "AcmsManager"

    .line 62
    const-string v3, "performRevocationCheck(pkgname): data Connection not available "

    .line 61
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ERROR_NO_DATA_CONNECTIVITY:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    .line 64
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v3

    .line 63
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->notifyRevocationCheckResult(Ljava/lang/String;I)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isCertified()Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const-string v2, "AcmsManager"

    .line 69
    const-string v3, "performRevocationCheck(pkgname): - Updating Certificate "

    .line 68
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v2

    .line 72
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 71
    invoke-virtual {v2, v3, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->NO_VALIDAPPS_TO_PERFORM_REVOCATION:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    .line 74
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v3

    .line 73
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->notifyRevocationCheckResult(Ljava/lang/String;I)V

    goto :goto_0

    .line 77
    :cond_2
    const-string v2, "AcmsManager"

    .line 78
    const-string v3, "performRevocationCheck(pkgname): - Revocation Check"

    .line 77
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v0, "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v2

    .line 83
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 82
    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public registerListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    const-string v1, "AcmsManager"

    const-string v2, "AcmsManager.resgisterListener()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 150
    const-string v1, "AcmsManager"

    const-string v2, "mClientListener is not null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    move-result v0

    .line 152
    .local v0, "reg":Z
    const-string v1, "AcmsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unreg Value is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .end local v0    # "reg":Z
    :cond_0
    const-string v1, "AcmsManager"

    const-string v2, "AcmsManager.resgisterListener() exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public setDevMode(Z)V
    .locals 6
    .param p1, "mode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    const-string v3, "AcmsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Entered setDevMode(): mode is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mContext:Landroid/content/Context;

    .line 125
    const-string v4, "DevModePref"

    .line 126
    const/4 v5, 0x0

    .line 125
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 129
    .local v2, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "DevMode"

    .line 130
    const/4 v4, 0x1

    .line 128
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 132
    .local v1, "presentDevMode":Z
    if-ne v1, p1, :cond_0

    .line 133
    const-string v3, "AcmsManager"

    .line 134
    const-string v4, "Present devMode is same as entered devMode. Hence no change"

    .line 133
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 137
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "DevMode"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 138
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 140
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->handleAllDevApps(Z)V

    goto :goto_0
.end method

.method public unRegisterListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 160
    const-string v1, "AcmsManager"

    const-string v2, "AcmsManager.unRegisterListener()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    if-eqz v1, :cond_0

    .line 162
    const-string v1, "AcmsManager"

    const-string v2, "mClientListener is not null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->mClientListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    move-result v0

    .line 164
    .local v0, "unreg":Z
    const-string v1, "AcmsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unreg Value is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .end local v0    # "unreg":Z
    :cond_0
    const-string v1, "AcmsManager"

    const-string v2, "AcmsManager.unRegisterListener() exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    return-void
.end method
