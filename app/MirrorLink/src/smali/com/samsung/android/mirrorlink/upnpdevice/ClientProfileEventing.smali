.class final Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;
.super Ljava/lang/Object;
.source "TMServerDevice.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/upnpdevice/IClientProfileEventsListener;


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mDevice:Lcom/samsung/upnp/Device;


# direct methods
.method constructor <init>(Lcom/samsung/upnp/Device;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 940
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 932
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->mDevice:Lcom/samsung/upnp/Device;

    .line 933
    const-string v0, "TMSUPnP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    .line 942
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice. ClientProfileEventing enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->mDevice:Lcom/samsung/upnp/Device;

    .line 944
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice. ClientProfileEventing exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    return-void
.end method


# virtual methods
.method public OnUnUsedProfileEvent(Ljava/lang/String;)V
    .locals 4
    .param p1, "unUsedProfiles"    # Ljava/lang/String;

    .prologue
    .line 953
    if-nez p1, :cond_0

    .line 955
    const-string p1, ""

    .line 958
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 959
    .local v0, "stateVarValue":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 960
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMServerDevice. OnUnUsedPrfoileEvent stateVarValue =  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->mDevice:Lcom/samsung/upnp/Device;

    .line 964
    const-string v2, "UnusedProfileIDs"

    .line 965
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 963
    invoke-static {v1, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 966
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    .line 967
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMServerDevice.OnUnUsedPrfoileEvent set state varibale UnusedProfileIDs with value "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 968
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 967
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 966
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;->LOG_TAG:Ljava/lang/String;

    const-string v2, "TMServerDevice.OnUnUsedPrfoileEvent exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    return-void
.end method
