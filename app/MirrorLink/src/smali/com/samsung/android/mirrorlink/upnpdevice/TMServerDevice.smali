.class public Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
.super Lcom/samsung/upnp/Device;
.source "TMServerDevice.java"


# static fields
.field private static final ACMS_INTENT_ACTION:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

.field private static final DESCRIPTION_XML:Ljava/lang/String; = "description.xml"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSUPnP"


# instance fields
.field private isFirstConnection:Z

.field private mAppStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;

.field private mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

.field private mClientProfileEventing:Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

.field private mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

.field private mContext:Landroid/content/Context;

.field private mDevice:Lcom/samsung/upnp/Device;

.field private mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 108
    invoke-direct {p0}, Lcom/samsung/upnp/Device;-><init>()V

    .line 83
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    .line 84
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 85
    iput-object p0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDevice:Lcom/samsung/upnp/Device;

    .line 87
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    .line 89
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    .line 90
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileEventing:Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

    .line 91
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    .line 92
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mAppStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;

    .line 93
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

    .line 95
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->isFirstConnection:Z

    .line 110
    const-string v6, "TMSUPnP"

    const-string v7, "TMServerDevice.TMServerDevice enter "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v4, 0x0

    .line 112
    .local v4, "tmDevice_desc":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 113
    .local v2, "tmApplicationServer_desc":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 114
    .local v3, "tmClientProfile_desc":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 115
    .local v5, "tmnotification_desc":Ljava/io/InputStream;
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    .line 116
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDevice:Lcom/samsung/upnp/Device;

    invoke-direct {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;-><init>(Lcom/samsung/upnp/Device;)V

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mAppStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;

    .line 117
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDevice:Lcom/samsung/upnp/Device;

    invoke-direct {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;-><init>(Lcom/samsung/upnp/Device;)V

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileEventing:Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

    .line 118
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDevice:Lcom/samsung/upnp/Device;

    invoke-direct {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;-><init>(Lcom/samsung/upnp/Device;)V

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

    .line 119
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 121
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 123
    const-string v6, "TMSUPnP"

    const-string v7, "TMServerDevice.TMServerDevice loading normal desc "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const-string v7, "description.xml"

    const-string v8, "description.xml"

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->prepareTempFileFromAssets(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 126
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const-string v7, "tmapplicationserver.xml"

    const-string v8, "tmapplicationserver.xml"

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->prepareTempFileFromAssets(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 127
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const-string v7, "tmclientprofile.xml"

    const-string v8, "tmclientprofile.xml"

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->prepareTempFileFromAssets(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 128
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const-string v7, "tmnotificationseversvc.xml"

    const-string v8, "tmnotificationseversvc.xml"

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->prepareTempFileFromAssets(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 131
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    .line 132
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    .line 133
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;-><init>(Landroid/content/Context;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    .line 132
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    .line 134
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 135
    new-instance v6, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7, v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    .line 138
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "screen_mode_key"

    const-string v8, "1"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "screen_mode":Ljava/lang/String;
    const-string v6, "TMSUPnP"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TMServerDevice.TMServerDevice screen mode is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    const-string v7, "description.xml"

    invoke-virtual {v6, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v4

    .line 144
    invoke-virtual {p0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->loadDescription(Ljava/io/InputStream;)Z

    .line 145
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;

    .line 146
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 147
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->enabledNotiInDescXml()V

    .line 149
    :cond_1
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/upnp/device/InvalidDescriptionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 163
    :goto_0
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    const-string v7, "tmnotificationseversvc.xml"

    invoke-virtual {v6, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    .line 164
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    const-string v7, "tmapplicationserver.xml"

    invoke-virtual {v6, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 165
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    const-string v7, "tmclientprofile.xml"

    invoke-virtual {v6, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 172
    :goto_1
    if-eqz v2, :cond_6

    if-eqz v3, :cond_6

    if-eqz v5, :cond_6

    .line 174
    invoke-virtual {p0, v2, v3, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->initializeDevice(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 182
    :goto_2
    if-eqz v2, :cond_2

    .line 183
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 185
    :cond_2
    if-eqz v3, :cond_3

    .line 186
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 188
    :cond_3
    if-eqz v5, :cond_4

    .line 189
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 197
    :cond_4
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mAppStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->regAppStatusEventListener(Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;)V

    .line 198
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileEventing:Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->regForClientProfileEvents(Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;)V

    .line 199
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    if-eqz v6, :cond_5

    .line 201
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->regForUpnpNotiEvents(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;)V

    .line 203
    :cond_5
    const-string v6, "TMSUPnP"

    const-string v7, "TMServerDevice.TMServerDevice exit "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 158
    .local v0, "e":Lcom/samsung/upnp/device/InvalidDescriptionException;
    invoke-virtual {v0}, Lcom/samsung/upnp/device/InvalidDescriptionException;->printStackTrace()V

    goto :goto_0

    .line 167
    .end local v0    # "e":Lcom/samsung/upnp/device/InvalidDescriptionException;
    :catch_2
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 178
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    const-string v6, "TMSUPnP"

    const-string v7, "TMServerDevice.TMServerDevice tmApplicationServer_desc or  tmClientProfile_desc is null"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 191
    :catch_3
    move-exception v0

    .line 192
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method private enabledNotiInDescXml()V
    .locals 9

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v7

    const-string v8, "serviceList"

    invoke-virtual {v7, v8}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 209
    .local v6, "servicelistNode":Lcom/samsung/xml/Node;
    new-instance v4, Lcom/samsung/xml/Node;

    const-string v7, "service"

    invoke-direct {v4, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 211
    .local v4, "serviceNode":Lcom/samsung/xml/Node;
    new-instance v5, Lcom/samsung/xml/Node;

    const-string v7, "serviceType"

    invoke-direct {v5, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 212
    .local v5, "serviceTypeNode":Lcom/samsung/xml/Node;
    const-string v7, "urn:schemas-upnp-org:service:TmNotificationServer:1"

    invoke-virtual {v5, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 213
    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 215
    new-instance v3, Lcom/samsung/xml/Node;

    const-string v7, "serviceId"

    invoke-direct {v3, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 216
    .local v3, "serviceId":Lcom/samsung/xml/Node;
    const-string v7, "urn:upnp-org:serviceId:TmNotificationServer1"

    invoke-virtual {v3, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v4, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 219
    new-instance v0, Lcom/samsung/xml/Node;

    const-string v7, "SCPDURL"

    invoke-direct {v0, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 220
    .local v0, "SCPDURL":Lcom/samsung/xml/Node;
    const-string v7, "/tmnotificationseversvc.xml"

    invoke-virtual {v0, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 221
    invoke-virtual {v4, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 223
    new-instance v1, Lcom/samsung/xml/Node;

    const-string v7, "controlURL"

    invoke-direct {v1, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 224
    .local v1, "controlURL":Lcom/samsung/xml/Node;
    const-string v7, "/TmNotificationServer1-control"

    invoke-virtual {v1, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v4, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 227
    new-instance v2, Lcom/samsung/xml/Node;

    const-string v7, "eventSubURL"

    invoke-direct {v2, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 228
    .local v2, "eventSubURL":Lcom/samsung/xml/Node;
    const-string v7, "/TmNotificationServer1-eventing"

    invoke-virtual {v2, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v4, v2}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 231
    if-eqz v6, :cond_0

    .line 232
    invoke-virtual {v6, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_0
    const-string v7, "TMSUPnP"

    const-string v8, "servicelistNode is null in function TMServerDevice.enabledNotiInDescXml"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 596
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 597
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "com.mirrorlink.android.app.LAUNCH"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 598
    invoke-virtual {v3, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 599
    const/4 v0, 0x0

    .line 600
    .local v0, "drawble":Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 602
    .local v4, "pkgMngr":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    move-object v5, v0

    .line 612
    :goto_1
    return-object v5

    .line 603
    :catch_0
    move-exception v1

    .line 604
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "TMSUPnP"

    const-string v6, "Icon not found in Launch activity. Getting default icon"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :try_start_1
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 607
    :catch_1
    move-exception v2

    .line 608
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "TMSUPnP"

    const-string v6, "No icon found for the application"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getUUIDFromMac(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "macAddr"    # Ljava/lang/String;

    .prologue
    .line 623
    const-string v2, "TMSUPnP"

    const-string v3, "TMServerDevice. getUUIDFromMac enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    const/4 v1, 0x0

    .line 626
    .local v1, "result":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "uuid:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 627
    const-string v3, "UTF-8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 626
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 632
    :goto_0
    const-string v2, "TMSUPnP"

    const-string v3, "TMServerDevice. getUUIDFromMac exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    return-object v1

    .line 628
    :catch_0
    move-exception v0

    .line 629
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v2, "TMSUPnP"

    const-string v3, "TMServerDevice. getUUIDFromMac UnsupportedEncodingException"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private httpTMGetRequestReceived(Lcom/samsung/http/HTTPRequest;)Z
    .locals 30
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 447
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived enter "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v24

    .line 449
    .local v24, "uri":Ljava/lang/String;
    const/4 v9, 0x0

    .line 450
    .local v9, "drwbl":Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    .line 455
    .local v12, "file":Ljava/io/File;
    const-string v26, "TMSUPnP"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "TMServerDevice. httpTMGetRequestReceived uri===>>>>>>>> "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v26, "description.xml"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 458
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setSignedDescription()Ljava/lang/String;

    move-result-object v23

    .line 460
    .local v23, "signedDesc":Ljava/lang/String;
    new-instance v16, Lcom/samsung/http/HTTPResponse;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 461
    .local v16, "httpRes":Lcom/samsung/http/HTTPResponse;
    const/16 v26, 0xc8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 462
    const-string v26, "text/xml"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 464
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 465
    const/4 v11, 0x0

    .line 467
    .local v11, "exception":Z
    :try_start_0
    const-string v26, "UTF-8"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v26

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setContent([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :goto_0
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 476
    const-string v26, "TMSUPnP"

    const-string v27, "sent signed description.xml"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->isFirstConnection:Z

    move/from16 v26, v0

    if-eqz v26, :cond_0

    if-nez v11, :cond_0

    .line 479
    const-string v26, "TMSUPnP"

    const-string v27, "First connection. Starting acms"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 481
    .local v25, "values":Landroid/content/ContentValues;
    const-string v26, "mlConnectedtime"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 483
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    sget-object v27, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_MLTIMEENTRY:Landroid/net/Uri;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 485
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->isFirstConnection:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 489
    :goto_1
    new-instance v18, Landroid/content/Intent;

    const-string v26, "com.samsung.android.mirrorlink.acms.api.IAcmsManager"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    .local v18, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 492
    .end local v18    # "intent":Landroid/content/Intent;
    .end local v25    # "values":Landroid/content/ContentValues;
    :cond_0
    const/16 v26, 0x1

    .line 592
    .end local v11    # "exception":Z
    .end local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v23    # "signedDesc":Ljava/lang/String;
    :goto_2
    return v26

    .line 468
    .restart local v11    # "exception":Z
    .restart local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .restart local v23    # "signedDesc":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 469
    .local v10, "e":Ljava/io/UnsupportedEncodingException;
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived UnsupportedEncodingException"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    invoke-virtual {v10}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 471
    const/4 v11, 0x1

    .line 472
    const/16 v26, 0x1f4

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    goto/16 :goto_0

    .line 486
    .end local v10    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v25    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v17

    .line 487
    .local v17, "ie":Ljava/lang/IllegalArgumentException;
    const-string v26, "TMSUPnP"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Cannot insert MLConnected time: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v28

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 495
    .end local v11    # "exception":Z
    .end local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v17    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v23    # "signedDesc":Ljava/lang/String;
    .end local v25    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string v26, ".png"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_2

    const-string v26, ".XPM"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 497
    :cond_2
    new-instance v16, Lcom/samsung/http/HTTPResponse;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 498
    .restart local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    const/16 v26, 0xc8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 499
    const-string v26, ".png"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 501
    const-string v26, "image/png"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 508
    :cond_3
    :goto_3
    const-string v26, "TMSUPnP"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "TMServerDevice. httpTMGetRequestReceived uri.substring(1, uri.length() - 3) == "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v28, 0x1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x4

    move-object/from16 v0, v24

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const/16 v26, 0x1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x4

    move-object/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 511
    if-nez v9, :cond_5

    .line 512
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived drwbl is null"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 503
    :cond_4
    const-string v26, ".xpm"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 505
    const-string v26, "image/xpm"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    goto :goto_3

    .line 516
    :cond_5
    :try_start_2
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived getting app icon for app"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    const-string v27, "temp_icon.png"

    .line 518
    const/16 v28, 0x0

    .line 517
    invoke-virtual/range {v26 .. v28}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v21

    .line 519
    .local v21, "out":Ljava/io/FileOutputStream;
    if-eqz v9, :cond_6

    .line 520
    move-object v0, v9

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 523
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    sget-object v26, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    .line 524
    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v26

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    move/from16 v27, v0

    .line 525
    sget-object v26, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    .line 526
    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v26

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    move/from16 v26, v0

    const/16 v28, 0x1

    .line 522
    move/from16 v0, v27

    move/from16 v1, v26

    move/from16 v2, v28

    invoke-static {v7, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 527
    .local v20, "newBitmap":Landroid/graphics/Bitmap;
    sget-object v26, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v27, 0x64

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    move/from16 v2, v27

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 529
    .end local v7    # "bitmap":Landroid/graphics/Bitmap;
    .end local v20    # "newBitmap":Landroid/graphics/Bitmap;
    :cond_6
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V

    .line 530
    new-instance v13, Ljava/io/File;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/temp_icon.png"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 531
    .end local v12    # "file":Ljava/io/File;
    .local v13, "file":Ljava/io/File;
    :try_start_3
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/temp_icon.png"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 532
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived getting drawable failed "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b

    move-object v12, v13

    .line 538
    .end local v13    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "file":Ljava/io/File;
    :goto_4
    if-nez v12, :cond_7

    .line 539
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived file is null"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 533
    :catch_2
    move-exception v10

    .line 534
    .local v10, "e":Ljava/io/IOException;
    :goto_5
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived failed................ "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 542
    .end local v10    # "e":Ljava/io/IOException;
    :cond_7
    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v26

    move-object/from16 v0, v16

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 543
    const/4 v14, 0x0

    .line 545
    .local v14, "fs":Ljava/io/FileInputStream;
    :try_start_4
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, v24

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 546
    .end local v14    # "fs":Ljava/io/FileInputStream;
    .local v15, "fs":Ljava/io/FileInputStream;
    :try_start_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 547
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 548
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9

    move-object v14, v15

    .line 556
    .end local v15    # "fs":Ljava/io/FileInputStream;
    .restart local v14    # "fs":Ljava/io/FileInputStream;
    :goto_6
    const-string v26, "TMSUPnP"

    .line 557
    const-string v27, "TMServerDevice. httpTMGetRequestReceived exit. sent icon"

    .line 556
    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const/16 v26, 0x1

    goto/16 :goto_2

    .line 549
    :catch_3
    move-exception v10

    .line 550
    .local v10, "e":Ljava/io/FileNotFoundException;
    :goto_7
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived failed................2 "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_6

    .line 552
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v10

    .line 553
    .local v10, "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 559
    .end local v10    # "e":Ljava/io/IOException;
    .end local v14    # "fs":Ljava/io/FileInputStream;
    .end local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    :cond_8
    const-string v26, ".cert"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_9

    const-string v26, ".crt"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 560
    :cond_9
    const-string v26, "TMSUPnP"

    const-string v27, "http request for Applicaiton certificate is received"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const/16 v26, 0x1

    const-string v27, "."

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 562
    .local v22, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getApplicationIdentifierFromAcms(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 563
    .local v4, "appIdentifier":Ljava/lang/String;
    if-nez v4, :cond_a

    .line 564
    const-string v26, "TMSUPnP"

    const-string v27, "AppIdentifier not present in acms"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 567
    :cond_a
    new-instance v19, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;-><init>(Landroid/content/Context;)V

    .line 568
    .local v19, "keystoreHelper":Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;->getKeystoreContent(Ljava/lang/String;)[B

    move-result-object v8

    .line 569
    .local v8, "certData":[B
    if-nez v8, :cond_b

    .line 570
    const-string v26, "TMSUPnP"

    const-string v27, "Certificate cannot be fetched from keystore"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 573
    :cond_b
    new-instance v16, Lcom/samsung/http/HTTPResponse;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/http/HTTPResponse;-><init>()V

    .line 574
    .restart local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    const/16 v26, 0xc8

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setStatusCode(I)V

    .line 575
    array-length v0, v8

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/http/HTTPResponse;->setContentLength(J)V

    .line 576
    const-string v26, "application/octet-stream"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 577
    const/4 v5, 0x0

    .line 579
    .local v5, "bis":Ljava/io/ByteArrayInputStream;
    :try_start_6
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 580
    .end local v5    # "bis":Ljava/io/ByteArrayInputStream;
    .local v6, "bis":Ljava/io/ByteArrayInputStream;
    :try_start_7
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/samsung/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 581
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/http/HTTPRequest;->post(Lcom/samsung/http/HTTPResponse;)Z

    .line 582
    invoke-virtual {v6}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    move-object v5, v6

    .line 589
    .end local v6    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "bis":Ljava/io/ByteArrayInputStream;
    :goto_9
    const/16 v26, 0x1

    goto/16 :goto_2

    .line 583
    :catch_5
    move-exception v10

    .line 584
    .local v10, "e":Ljava/io/FileNotFoundException;
    :goto_a
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived failed................2 "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_9

    .line 586
    .end local v10    # "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v10

    .line 587
    .local v10, "e":Ljava/io/IOException;
    :goto_b
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 591
    .end local v4    # "appIdentifier":Ljava/lang/String;
    .end local v5    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v8    # "certData":[B
    .end local v10    # "e":Ljava/io/IOException;
    .end local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .end local v19    # "keystoreHelper":Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;
    .end local v22    # "packageName":Ljava/lang/String;
    :cond_c
    const-string v26, "TMSUPnP"

    const-string v27, "TMServerDevice. httpTMGetRequestReceived exit.its not a geticon request or icon type not matching "

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 586
    .restart local v4    # "appIdentifier":Ljava/lang/String;
    .restart local v6    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v8    # "certData":[B
    .restart local v16    # "httpRes":Lcom/samsung/http/HTTPResponse;
    .restart local v19    # "keystoreHelper":Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;
    .restart local v22    # "packageName":Ljava/lang/String;
    :catch_7
    move-exception v10

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "bis":Ljava/io/ByteArrayInputStream;
    goto :goto_b

    .line 583
    .end local v5    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "bis":Ljava/io/ByteArrayInputStream;
    :catch_8
    move-exception v10

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "bis":Ljava/io/ByteArrayInputStream;
    goto :goto_a

    .line 552
    .end local v4    # "appIdentifier":Ljava/lang/String;
    .end local v5    # "bis":Ljava/io/ByteArrayInputStream;
    .end local v8    # "certData":[B
    .end local v19    # "keystoreHelper":Lcom/samsung/android/mirrorlink/upnpdevice/KeystoreHelper;
    .end local v22    # "packageName":Ljava/lang/String;
    .restart local v15    # "fs":Ljava/io/FileInputStream;
    :catch_9
    move-exception v10

    move-object v14, v15

    .end local v15    # "fs":Ljava/io/FileInputStream;
    .restart local v14    # "fs":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .line 549
    .end local v14    # "fs":Ljava/io/FileInputStream;
    .restart local v15    # "fs":Ljava/io/FileInputStream;
    :catch_a
    move-exception v10

    move-object v14, v15

    .end local v15    # "fs":Ljava/io/FileInputStream;
    .restart local v14    # "fs":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .line 533
    .end local v12    # "file":Ljava/io/File;
    .end local v14    # "fs":Ljava/io/FileInputStream;
    .restart local v13    # "file":Ljava/io/File;
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v10

    move-object v12, v13

    .end local v13    # "file":Ljava/io/File;
    .restart local v12    # "file":Ljava/io/File;
    goto/16 :goto_5
.end method

.method private setSignedDescription()Ljava/lang/String;
    .locals 6

    .prologue
    .line 296
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. setSignedDescription enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v0, ""

    .line 298
    .local v0, "desc":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getRootNode()Lcom/samsung/xml/Node;

    move-result-object v1

    .line 299
    .local v1, "rootNode":Lcom/samsung/xml/Node;
    if-eqz v1, :cond_0

    .line 300
    const-string v0, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    .line 301
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 302
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;

    move-result-object v3

    .line 305
    .local v3, "xmlSigning":Lcom/samsung/android/mirrorlink/util/XmlSigning;
    const-string v4, "X_Signature"

    invoke-virtual {v3, v0, v4}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->prepareSignedXml(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 306
    .local v2, "signedDesc":Ljava/lang/String;
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. setSignedDescription exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    return-object v2
.end method

.method public static setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "device"    # Lcom/samsung/upnp/Device;
    .param p1, "stateVar"    # Ljava/lang/String;
    .param p2, "eventVal"    # Ljava/lang/String;

    .prologue
    .line 648
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMServerDevice. setStateVariable enter.stateVar = ,"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " eventVal= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    const-string v4, "AppStatusUpdate"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 651
    const-string v4, "TMSUPnP"

    .line 652
    const-string v5, "TMServerDevice. setStateVariable AppStatus Update event received"

    .line 651
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v4, "AppStatusUpdate"

    invoke-virtual {p0, v4}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v2

    .line 654
    .local v2, "appStatusUpdate":Lcom/samsung/upnp/StateVariable;
    if-nez v2, :cond_1

    .line 656
    const-string v4, "TMSUPnP"

    .line 657
    const-string v5, "TMServerDevice. setStateVariable AppStatus getStateVariable failed"

    .line 656
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    .end local v2    # "appStatusUpdate":Lcom/samsung/upnp/StateVariable;
    :cond_0
    :goto_0
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. setStateVariable exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const/4 v4, 0x1

    return v4

    .line 661
    .restart local v2    # "appStatusUpdate":Lcom/samsung/upnp/StateVariable;
    :cond_1
    const-string v4, "TMSUPnP"

    .line 662
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMServerDevice. setStateVariable Found state variable "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 663
    invoke-virtual {v2}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 662
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 661
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-virtual {v2, p2}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 668
    .end local v2    # "appStatusUpdate":Lcom/samsung/upnp/StateVariable;
    :cond_2
    const-string v4, "AppListUpdate"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 670
    const-string v4, "TMSUPnP"

    const-string v5, "Got State variable AppListUpdate"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    const-string v4, "AppListUpdate"

    invoke-virtual {p0, v4}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v1

    .line 673
    .local v1, "appListUpdate":Lcom/samsung/upnp/StateVariable;
    if-eqz v1, :cond_0

    .line 675
    invoke-virtual {v1, p2}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 678
    .end local v1    # "appListUpdate":Lcom/samsung/upnp/StateVariable;
    :cond_3
    const-string v4, "UnusedProfileIDs"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 680
    const-string v4, "UnusedProfileIDs"

    invoke-virtual {p0, v4}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v3

    .line 682
    .local v3, "unUsedProfileIDVar":Lcom/samsung/upnp/StateVariable;
    if-eqz v3, :cond_0

    .line 684
    invoke-virtual {v3, p2}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 687
    .end local v3    # "unUsedProfileIDVar":Lcom/samsung/upnp/StateVariable;
    :cond_4
    const-string v4, "ActiveNotiEvent"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 689
    const-string v4, "ActiveNotiEvent"

    invoke-virtual {p0, v4}, Lcom/samsung/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/upnp/StateVariable;

    move-result-object v0

    .line 690
    .local v0, "activeNotiEvent":Lcom/samsung/upnp/StateVariable;
    if-nez v0, :cond_5

    .line 692
    const-string v4, "TMSUPnP"

    .line 693
    const-string v5, "TMServerDevice. setStateVariable AppStatus getStateVariable failed"

    .line 692
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 697
    :cond_5
    const-string v4, "TMSUPnP"

    .line 698
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMServerDevice. setStateVariable Found state variable "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 699
    invoke-virtual {v0}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sendEvents "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/upnp/StateVariable;->isSendEvents()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 698
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 697
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual {v0, p2}, Lcom/samsung/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public getAppServerSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    return-object v0
.end method

.method public getClientProfileSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    return-object v0
.end method

.method public httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V
    .locals 2
    .param p1, "httpReq"    # Lcom/samsung/http/HTTPRequest;

    .prologue
    .line 422
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. httpRequestRecieved enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    if-nez p1, :cond_0

    .line 425
    const-string v0, "TMSUPnP"

    .line 426
    const-string v1, "TMServerDevice.httpRequestRecieved exit.httpReq = null "

    .line 425
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->httpTMGetRequestReceived(Lcom/samsung/http/HTTPRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    const-string v0, "TMSUPnP"

    .line 432
    const-string v1, "TMServerDevice. httpRequestRecieved finished HTTP get request "

    .line 431
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 435
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/upnp/Device;->httpRequestRecieved(Lcom/samsung/http/HTTPRequest;)V

    .line 436
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. httpRequestRecieved exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initStateVars()V
    .locals 6

    .prologue
    .line 818
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. initStateVars enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    const/4 v1, 0x0

    .line 820
    .local v1, "applist":Ljava/lang/String;
    new-instance v2, Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getApplicationMap()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 821
    .local v2, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->removeUnsupportedAppByClient(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    .line 822
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 824
    .local v3, "itrtr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 838
    const-string v4, "AppStatusUpdate"

    invoke-static {p0, v4, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 839
    const-string v4, "AppListUpdate"

    invoke-static {p0, v4, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 840
    const-string v4, "UnusedProfileIDs"

    const-string v5, "0"

    invoke-static {p0, v4, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 841
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. initStateVars exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    return-void

    .line 826
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 829
    .local v0, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v1, :cond_1

    .line 830
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0x"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 831
    goto :goto_0

    .line 833
    :cond_1
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 834
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0x"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public initializeDevice(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 6
    .param p1, "TMApplicationServer_desc"    # Ljava/io/InputStream;
    .param p2, "TMClientProfile_desc"    # Ljava/io/InputStream;
    .param p3, "tmnotification_desc"    # Ljava/io/InputStream;

    .prologue
    .line 255
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice.initializeDevice enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setDeviceParameters()V

    .line 259
    const/4 v3, 0x0

    .line 260
    .local v3, "ntfService":Lcom/samsung/upnp/Service;
    const-string v4, "urn:schemas-upnp-org:service:TmApplicationServer:1"

    invoke-virtual {p0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    .line 261
    .local v0, "appService":Lcom/samsung/upnp/Service;
    const-string v4, "urn:schemas-upnp-org:service:TmClientProfile:1"

    invoke-virtual {p0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v1

    .line 262
    .local v1, "cpService":Lcom/samsung/upnp/Service;
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 264
    const-string v4, "urn:schemas-upnp-org:service:TmNotificationServer:1"

    invoke-virtual {p0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v3

    .line 266
    :cond_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 293
    :cond_1
    :goto_0
    return-void

    .line 271
    :cond_2
    const/16 v4, 0x9

    invoke-static {v4}, Lcom/samsung/upnp/UPnP;->setEnable(I)V

    .line 274
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/samsung/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z

    .line 275
    invoke-virtual {v1, p2}, Lcom/samsung/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z

    .line 276
    if-eqz v3, :cond_3

    .line 277
    invoke-virtual {v3, p3}, Lcom/samsung/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mClientProfileSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    invoke-virtual {v1, v4}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 287
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v0, v4}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 288
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v3, :cond_4

    .line 289
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    invoke-virtual {v3, v4}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 292
    :cond_4
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice.initializeDevice exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 280
    :catch_0
    move-exception v2

    .line 282
    .local v2, "e":Lcom/samsung/xml/ParserException;
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice.initializeDevice loadSCPD failed"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-virtual {v2}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    goto :goto_1
.end method

.method public setBtParams(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "startConnection"    # Z

    .prologue
    .line 739
    const-string v2, "TMSUPnP"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMServerDevice. setBtParams enter ipAddr = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 740
    const-string v4, "port ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 739
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v2

    const-string v3, "X_connectivity"

    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    .line 743
    .local v1, "deviceXConNode":Lcom/samsung/xml/Node;
    if-nez v1, :cond_0

    .line 745
    const-string v2, "TMSUPnP"

    const-string v3, " TMServerDevice. setBtParams X_connectivity not found"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->setBtParams(Ljava/lang/String;Z)V

    .line 767
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getUUIDFromMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setUDN(Ljava/lang/String;)V

    .line 768
    const-string v2, "TMSUPnP"

    const-string v3, "TMServerDevice. setBtParams exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    return-void

    .line 749
    :cond_0
    const-string v2, "bluetooth"

    invoke-virtual {v1, v2}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 750
    .local v0, "bluetoothNode":Lcom/samsung/xml/Node;
    if-nez v0, :cond_1

    .line 752
    const-string v2, "TMSUPnP"

    const-string v3, " TMServerDevice. setBtParams bluetooth not found"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 756
    :cond_1
    const-string v2, "bdAddr"

    invoke-virtual {v0, v2, p1}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    if-eqz p2, :cond_2

    .line 758
    const-string v2, "startConnection"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 760
    :cond_2
    const-string v2, "startConnection"

    const-string v3, "false"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCdbParams(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 784
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice. setCdbParams enter ipAddr = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 785
    const-string v2, "port ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 784
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->setCdbParams(Ljava/lang/String;I)V

    .line 787
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. setCdbParams exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    return-void
.end method

.method public setDapParams(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 777
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice. setDapParams enter ipAddr = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 778
    const-string v2, " port ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 777
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->setDapParams(Ljava/lang/String;I)V

    .line 780
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. setDapParams exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    return-void
.end method

.method public setDeviceParameters()V
    .locals 2

    .prologue
    .line 316
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. setDeviceParameters enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v0, "SAMSUNG"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setFriendlyName(Ljava/lang/String;Z)Z

    .line 318
    const/16 v0, 0xfa4

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setHTTPPort(I)V

    .line 386
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice.setDeviceParameters exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    return-void
.end method

.method public setModelName()V
    .locals 9

    .prologue
    .line 791
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. setModelName enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const-string v3, ""

    .line 795
    .local v3, "modelName":Ljava/lang/String;
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 796
    .local v2, "listener":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "scmm.mirrorlink.ctsmode"

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 797
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. Now CTS mode exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    .end local v2    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 800
    .restart local v2    # "listener":Ljava/lang/reflect/Method;
    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "ro.product.model"

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 813
    .end local v2    # "listener":Ljava/lang/reflect/Method;
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getDeviceNode()Lcom/samsung/xml/Node;

    move-result-object v4

    const-string v5, "modelName"

    invoke-virtual {v4, v5, v3}, Lcom/samsung/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    const-string v4, "TMSUPnP"

    const-string v5, "TMServerDevice. setModelName exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 802
    :catch_0
    move-exception v1

    .line 803
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMNetworkReceiver.onReceive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 804
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 805
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMNetworkReceiver.onReceive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 806
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v1

    .line 807
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMNetworkReceiver.onReceive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 808
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 809
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "TMSUPnP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMNetworkReceiver.onReceive "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public setRtpParams(Ljava/lang/String;II)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "serverPort"    # I
    .param p3, "clientPort"    # I

    .prologue
    .line 727
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice. setRtpParams enter ipAddr = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 728
    const-string v2, "server port ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " client port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 727
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->setRtpParams(Ljava/lang/String;II)V

    .line 730
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. setRtpParams exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    return-void
.end method

.method public setVncParams(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 714
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice. setVncParams enter ipAddr = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 715
    const-string v2, "port ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 714
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mApplicationServerSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->setVncParams(Ljava/lang/String;I)V

    .line 717
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. setVncParams exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    return-void
.end method

.method public stop()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 391
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. stop enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v0, "urn:schemas-upnp-org:service:TmClientProfile:1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 393
    const-string v0, "urn:schemas-upnp-org:service:TmApplicationServer:1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 395
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 396
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 397
    sput-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mTMNotiSrvc:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->cleanUp()V

    .line 402
    :cond_1
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 403
    const-string v0, "urn:schemas-upnp-org:service:TmNotificationServer:1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getService(Ljava/lang/String;)Lcom/samsung/upnp/Service;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/upnp/Service;->setActionListener(Lcom/samsung/upnp/control/ActionListener;)V

    .line 405
    :cond_2
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 406
    sput-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    .line 407
    invoke-super {p0}, Lcom/samsung/upnp/Device;->stop()Z

    .line 408
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->deinit()V

    .line 409
    const-string v0, "TMSUPnP"

    const-string v1, "TMServerDevice. stop exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v0, 0x1

    return v0
.end method
