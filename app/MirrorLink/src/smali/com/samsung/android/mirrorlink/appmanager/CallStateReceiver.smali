.class Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneAppEventsListner.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field public mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;)V
    .locals 2
    .param p1, "upnpCallAppNotifier"    # Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    .prologue
    .line 181
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 182
    const-string v0, "TMSAppMngr"

    const-string v1, "CallStateReceiver.CallStateReceiver enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;->mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    .line 185
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 188
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "CallStateReceiver.onReceive intent.getAction() = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 188
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v5, "state"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "extraState":Ljava/lang/String;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "CallStateReceiver,onReceive intent.getStringExtra() = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 191
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v5, "incoming_number"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "callid":Ljava/lang/String;
    const-string v5, "RINGING"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 196
    sget-object v5, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 197
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sput-object v5, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    .line 200
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;->mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 201
    .local v3, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v0, "bndl":Landroid/os/Bundle;
    const-string v5, "callid"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 204
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;->mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    invoke-virtual {v5, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->sendMessage(Landroid/os/Message;)Z

    .line 217
    .end local v0    # "bndl":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    const-string v5, "IDLE"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "OFFHOOK"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 206
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;->mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotifications:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 207
    .local v4, "notiId":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 208
    const-string v5, "TMSAppMngr"

    .line 209
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UpnpCallAppNotifier.UpnpCallAppNotifier clear notification "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 209
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 208
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNotification(Ljava/lang/String;)Z

    .line 213
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;->mPhoneAppEventsListner:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotifications:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
