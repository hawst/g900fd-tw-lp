.class public Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
.super Ljava/lang/Object;
.source "TMSAppInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppCertEntityInfo"
.end annotation


# instance fields
.field public mEntityName:Ljava/lang/String;

.field public mNonrestricted:Ljava/lang/String;

.field public mRestricted:Ljava/lang/String;

.field public mServiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mTargetList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    if-ne p0, p1, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 162
    :cond_1
    if-nez p1, :cond_2

    .line 163
    const-string v1, "TMSAppInfo"

    const-string v3, "obj is null in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 164
    goto :goto_0

    .line 166
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    .line 167
    const-string v1, "TMSAppInfo"

    const-string v3, "getClass not same in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 168
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 170
    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 171
    .local v0, "other":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 172
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 173
    const-string v1, "TMSAppInfo"

    const-string v3, "entity name null in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 174
    goto :goto_0

    .line 176
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 177
    const-string v1, "TMSAppInfo"

    const-string v3, "entity name not same in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 178
    goto :goto_0

    .line 180
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 181
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 182
    const-string v1, "TMSAppInfo"

    const-string v3, "mNonrestricted null in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 183
    goto :goto_0

    .line 185
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 186
    const-string v1, "TMSAppInfo"

    const-string v3, "mNonrestricted not same in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 187
    goto :goto_0

    .line 189
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 190
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 191
    const-string v1, "TMSAppInfo"

    const-string v3, "mRestricted null in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 192
    goto :goto_0

    .line 194
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 195
    const-string v1, "TMSAppInfo"

    const-string v3, "mRestricted not same in entity info"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 196
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 203
    const/16 v0, 0x1f

    .line 204
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 206
    .local v1, "result":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    .line 205
    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 207
    mul-int/lit8 v4, v1, 0x1f

    .line 209
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    .line 207
    :goto_1
    add-int v1, v4, v2

    .line 210
    mul-int/lit8 v4, v1, 0x1f

    .line 211
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    .line 210
    :goto_2
    add-int v1, v4, v2

    .line 212
    mul-int/lit8 v4, v1, 0x1f

    .line 213
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    if-nez v2, :cond_3

    move v2, v3

    .line 212
    :goto_3
    add-int v1, v4, v2

    .line 214
    mul-int/lit8 v2, v1, 0x1f

    .line 215
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    if-nez v4, :cond_4

    .line 214
    :goto_4
    add-int v1, v2, v3

    .line 216
    return v1

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 209
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 211
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 213
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_3

    .line 215
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_4
.end method
