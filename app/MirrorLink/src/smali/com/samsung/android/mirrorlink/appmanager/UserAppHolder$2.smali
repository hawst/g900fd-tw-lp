.class Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;
.super Ljava/lang/Object;
.source "UserAppHolder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRemoved(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 160
    const-string v1, "TMSUserAppHolder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " removed from ACMS"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$11(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getUpnpCommonApiUtil(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    .line 162
    .local v0, "upnpCommonApiUtil":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->acmsAppStateChanged(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public onUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 167
    const-string v0, "TMSUserAppHolder"

    const-string v1, "Some Acms app state is changed. Sending info to CommonApi"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$11(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getUpnpCommonApiUtil(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->acmsAppStateChanged(Ljava/lang/String;)V

    .line 170
    return-void
.end method
