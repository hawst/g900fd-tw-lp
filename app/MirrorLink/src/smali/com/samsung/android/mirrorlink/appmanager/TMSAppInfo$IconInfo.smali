.class public Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
.super Ljava/lang/Object;
.source "TMSAppInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IconInfo"
.end annotation


# static fields
.field private static final DEFAULT_DEPTH:I = 0x20

.field private static final DEFAULT_HEIGHT:I = 0x28

.field private static final DEFAULT_WIDTH:I = 0x28


# instance fields
.field public mDepth:I

.field public mHeight:I

.field public mMimetype:Ljava/lang/String;

.field public mUrl:Ljava/lang/String;

.field public mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0x28

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    .line 99
    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mHeight:I

    .line 100
    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mWidth:I

    .line 101
    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mDepth:I

    .line 102
    return-void
.end method
