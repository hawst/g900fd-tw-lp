.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsPlatConstants;
.super Ljava/lang/Object;
.source "AcsPlatConstants.java"


# static fields
.field public static final ACS_CONTENT_RULES_EVT:I = 0x40

.field public static final ACS_DEVICE_LOCK_EVT:I = 0x1

.field public static final ACS_DEVICE_LOCK_STATUS_LOCKED:I = 0x1

.field public static final ACS_DEVICE_LOCK_STATUS_UNKNOWN:I = -0x1

.field public static final ACS_DEVICE_LOCK_STATUS_UNLOCKED:I = 0x0

.field public static final ACS_FB_ORIENTATION_EVT:I = 0x100

.field public static final ACS_FB_ORIENTATION_LANDSCAPE:I = 0x0

.field public static final ACS_FB_ORIENTATION_PORTRAIT:I = 0x1

.field public static final ACS_FB_ORIENTATION_UNKOWN:I = -0x1

.field public static final ACS_FB_ROTATION_0:I = 0x0

.field public static final ACS_FB_ROTATION_180:I = 0x2

.field public static final ACS_FB_ROTATION_270:I = 0x3

.field public static final ACS_FB_ROTATION_90:I = 0x1

.field public static final ACS_FB_ROTATION_EVT:I = 0x80

.field public static final ACS_KEY_ACTION_DOWN:I = 0x0

.field public static final ACS_KEY_ACTION_INVALID:I = -0x1

.field public static final ACS_KEY_ACTION_UP:I = 0x1

.field public static final ACS_KEY_GUARD_EVT:I = 0x4

.field public static final ACS_MIC_INPUT_EVT:I = 0x20

.field public static final ACS_NIGHT_MODE_EVT:I = 0x8

.field public static final ACS_NIGHT_MODE_STATUS_OFF:I = 0x0

.field public static final ACS_NIGHT_MODE_STATUS_ON:I = 0x1

.field public static final ACS_NIGHT_MODE_STATUS_UNKOWN:I = -0x1

.field public static final ACS_PRESENTATION_MODE_EVT:I = 0x200

.field public static final ACS_SCREEN_SAVER_EVT:I = 0x2

.field public static final ACS_SCREEN_SAVER_STATUS_OFF:I = 0x0

.field public static final ACS_SCREEN_SAVER_STATUS_ON:I = 0x1

.field public static final ACS_SCREEN_SAVER_STATUS_UNKNOWN:I = -0x1

.field public static final ACS_TOUCH_PRESSURE_FULL_VAL:I = 0xff

.field public static final ACS_TOUCH_PRESSURE_INVALID:I = -0x1

.field public static final ACS_TOUCH_PRESSURE_NONE_VAL:I = 0x0

.field public static final ACS_VNC_CONTENT_RULES_DRIVING:I = 0x1

.field public static final ACS_VNC_CONTENT_RULES_IGNORE:I = -0x1

.field public static final ACS_VNC_CONTENT_RULES_PARKING:I = 0x0

.field public static final ACS_VNC_DEVICE_STATUS_DISABLED:I = 0x2

.field public static final ACS_VNC_DEVICE_STATUS_ENABLED:I = 0x3

.field public static final ACS_VNC_DEVICE_STATUS_NOTUSED:I = 0x1

.field public static final ACS_VNC_DEVICE_STATUS_UNKNOWN:I = 0x0

.field public static final ACS_VOICE_INPUT_EVT:I = 0x10

.field public static final ENABLE_DUAL_DISPLAY:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
