.class public Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;
.super Ljava/lang/Object;
.source "AppIdGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;
    }
.end annotation


# static fields
.field private static final BASE64_LOOKUP:[I

.field public static final END_FILE:Ljava/lang/String; = "endFile"

.field public static final END_MANIFEST_MAIN:Ljava/lang/String; = "endManifestMain"

.field private static final ENTRY_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/util/jar/JarEntry;",
            ">;"
        }
    .end annotation
.end field

.field public static final HASH_ALGORITHM:Ljava/lang/String; = "SHA-256"

.field public static final META_INF_PATH:Ljava/lang/String; = "META-INF/"

.field public static final ML_CERT_PATH:Ljava/lang/String; = "assets/self-signed.ccc.crt"

.field public static final START_FILE:Ljava/lang/String; = "startFile"

.field public static final START_MANIFEST_MAIN:Ljava/lang/String; = "startManifestMain"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->BASE64_LOOKUP:[I

    .line 128
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$1;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$1;-><init>()V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->ENTRY_COMPARATOR:Ljava/util/Comparator;

    .line 133
    return-void

    .line 52
    nop

    :array_0
    .array-data 4
        0x41
        0x42
        0x43
        0x44
        0x45
        0x46
        0x47
        0x48
        0x49
        0x4a
        0x4b
        0x4c
        0x4d
        0x4e
        0x4f
        0x50
        0x51
        0x52
        0x53
        0x54
        0x55
        0x56
        0x57
        0x58
        0x59
        0x5a
        0x61
        0x62
        0x63
        0x64
        0x65
        0x66
        0x67
        0x68
        0x69
        0x6a
        0x6b
        0x6c
        0x6d
        0x6e
        0x6f
        0x70
        0x71
        0x72
        0x73
        0x74
        0x75
        0x76
        0x77
        0x78
        0x79
        0x7a
        0x30
        0x31
        0x32
        0x33
        0x34
        0x35
        0x36
        0x37
        0x38
        0x39
        0x2d
        0x5f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addAttribsToDigest(Ljava/util/jar/Attributes;Ljava/security/MessageDigest;)Z
    .locals 7
    .param p0, "attribs"    # Ljava/util/jar/Attributes;
    .param p1, "md"    # Ljava/security/MessageDigest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 155
    const/4 v3, 0x0

    .line 156
    .local v3, "ret":Z
    if-nez p0, :cond_0

    .line 157
    const/4 v5, 0x0

    .line 180
    :goto_0
    return v5

    .line 160
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v1, "names":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/util/jar/Attributes;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 164
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 165
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    move v5, v3

    .line 180
    goto :goto_0

    .line 161
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 162
    .local v2, "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 165
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 166
    .local v0, "attrib":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 167
    .local v4, "value":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 172
    const-string v4, ""

    .line 174
    :cond_3
    const-string v6, "-Digest"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 175
    const/4 v3, 0x1

    .line 177
    :cond_4
    invoke-static {v0, p1}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 178
    invoke-static {v4, p1}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    goto :goto_2
.end method

.method private static addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V
    .locals 3
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "md"    # Ljava/security/MessageDigest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 143
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 144
    .local v0, "valueBytes":[B
    array-length v1, v0

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->longToBytes(J)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 145
    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 146
    return-void
.end method

.method private static base64Encode([B)Ljava/lang/String;
    .locals 8
    .param p0, "hashOutput"    # [B

    .prologue
    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .local v4, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, p0

    if-lt v0, v6, :cond_0

    .line 112
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 84
    :cond_0
    aget-byte v6, p0, v0

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v5, v6, 0x10

    .line 85
    .local v5, "value":I
    const/4 v3, 0x2

    .line 86
    .local v3, "outputChars":I
    add-int/lit8 v6, v0, 0x1

    array-length v7, p0

    if-ge v6, v7, :cond_1

    .line 87
    add-int/lit8 v6, v0, 0x1

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    .line 88
    add-int/lit8 v3, v3, 0x1

    .line 90
    :cond_1
    add-int/lit8 v6, v0, 0x2

    array-length v7, p0

    if-ge v6, v7, :cond_2

    .line 91
    add-int/lit8 v6, v0, 0x2

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    .line 92
    add-int/lit8 v3, v3, 0x1

    .line 94
    :cond_2
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    const/4 v6, 0x4

    if-lt v2, v6, :cond_3

    .line 79
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 95
    :cond_3
    if-lez v3, :cond_4

    .line 101
    rsub-int/lit8 v6, v2, 0x3

    mul-int/lit8 v6, v6, 0x6

    shr-int v6, v5, v6

    and-int/lit8 v1, v6, 0x3f

    .line 102
    .local v1, "index":I
    sget-object v6, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->BASE64_LOOKUP:[I

    aget v6, v6, v1

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    .line 109
    .end local v1    # "index":I
    :cond_4
    add-int/lit8 v3, v3, -0x1

    .line 94
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static calculateAppId(Ljava/lang/String;JLjava/io/File;)Ljava/lang/String;
    .locals 9
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "versionCode"    # J
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;,
            Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;
        }
    .end annotation

    .prologue
    .line 187
    const-string v6, "SHA-256"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 189
    .local v5, "md":Ljava/security/MessageDigest;
    invoke-static {p0, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 191
    invoke-static {p1, p2}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->longToBytes(J)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 193
    new-instance v4, Ljava/util/jar/JarFile;

    invoke-direct {v4, p3}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 196
    .local v4, "jar":Ljava/util/jar/JarFile;
    const-string v6, "startManifestMain"

    invoke-static {v6, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 197
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object v6

    if-nez v6, :cond_0

    .line 198
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->close()V

    .line 199
    const/4 v6, 0x0

    .line 237
    :goto_0
    return-object v6

    .line 202
    :cond_0
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addAttribsToDigest(Ljava/util/jar/Attributes;Ljava/security/MessageDigest;)Z

    .line 203
    const-string v6, "endManifestMain"

    invoke-static {v6, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/jar/JarEntry;>;"
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/util/jar/JarEntry;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_3

    .line 219
    sget-object v6, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->ENTRY_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 220
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_4

    .line 233
    if-eqz v4, :cond_2

    .line 234
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->close()V

    .line 236
    :cond_2
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    .line 237
    .local v3, "hashOutput":[B
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->base64Encode([B)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 217
    .end local v3    # "hashOutput":[B
    :cond_3
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/jar/JarEntry;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 220
    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/jar/JarEntry;

    .line 221
    .local v2, "entry":Ljava/util/jar/JarEntry;
    invoke-virtual {v2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "assets/self-signed.ccc.crt"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 222
    invoke-virtual {v2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "META-INF/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 225
    const-string v7, "startFile"

    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 226
    invoke-virtual {v2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    .line 227
    invoke-virtual {v2}, Ljava/util/jar/JarEntry;->getAttributes()Ljava/util/jar/Attributes;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addAttribsToDigest(Ljava/util/jar/Attributes;Ljava/security/MessageDigest;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 228
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->close()V

    .line 229
    new-instance v6, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;

    invoke-virtual {v2}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 231
    :cond_5
    const-string v7, "endFile"

    invoke-static {v7, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->addStringToDigest(Ljava/lang/String;Ljava/security/MessageDigest;)V

    goto :goto_2
.end method

.method private static longToBytes(J)[B
    .locals 6
    .param p0, "v"    # J

    .prologue
    .line 117
    const/16 v2, 0x8

    new-array v1, v2, [B

    .line 118
    .local v1, "ret":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 121
    return-object v1

    .line 119
    :cond_0
    rsub-int/lit8 v2, v0, 0x7

    mul-int/lit8 v2, v2, 0x8

    shr-long v2, p0, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
