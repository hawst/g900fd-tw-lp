.class Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;
.super Landroid/os/Handler;
.source "UserAppHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    .line 674
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 680
    const-string v13, "TMSUserAppHolder"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "handleMessage enter "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v15, v0, Landroid/os/Message;->what:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$0()Landroid/app/ActivityManager;

    move-result-object v13

    if-nez v13, :cond_0

    .line 684
    const-string v13, "TMSUserAppHolder"

    .line 685
    const-string v14, "handleMessage mActivtyMngr is null"

    .line 684
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :goto_0
    return-void

    .line 688
    :cond_0
    move-object/from16 v0, p1

    iget v13, v0, Landroid/os/Message;->what:I

    packed-switch v13, :pswitch_data_0

    .line 812
    :cond_1
    :goto_1
    const-string v13, "TMSUserAppHolder"

    const-string v14, "handleMessage exit "

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 692
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "PackageNames"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 693
    .local v7, "packName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    .line 694
    const-string v14, "newappStatus"

    .line 693
    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 695
    .local v4, "newAppStatus":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    .line 696
    const-string v14, "oldappStatus"

    .line 695
    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 697
    .local v6, "oldAppStatus":Ljava/lang/String;
    const-string v13, "TMSUserAppHolder"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "handleMessage Status  has changed. oldAppStatus = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 699
    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " newAppStatus = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 697
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v13}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 702
    const/4 v12, 0x0

    .line 703
    .local v12, "status":I
    const-string v13, "STOPPING"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 704
    const-string v13, "PAUSED"

    invoke-virtual {v13, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 710
    const/16 v12, 0x15

    .line 733
    :cond_2
    :goto_2
    if-eqz v12, :cond_1

    .line 735
    const-string v13, "TMSUserAppHolder"

    .line 736
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "handleMessage Status of    is set to>>>>  "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 738
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 736
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 735
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v13}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v13

    invoke-interface {v13, v7, v12}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 712
    :cond_3
    const-string v13, "DESTROYED"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 714
    const/16 v12, 0x16

    .line 715
    goto :goto_2

    .line 716
    :cond_4
    const-string v13, "RESUMED"

    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 721
    const/16 v12, 0x14

    .line 722
    const-string v13, "TMSUserAppHolder"

    .line 723
    const-string v14, "getRunningApps  is the foreground app"

    .line 722
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 748
    .end local v4    # "newAppStatus":Ljava/lang/String;
    .end local v6    # "oldAppStatus":Ljava/lang/String;
    .end local v7    # "packName":Ljava/lang/String;
    .end local v12    # "status":I
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "PackageNames"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 749
    .local v8, "packNames":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "State"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 750
    .local v11, "state":Ljava/lang/String;
    const/4 v5, 0x0

    .line 751
    .local v5, "newPkg":Landroid/content/pm/ApplicationInfo;
    const/4 v2, 0x0

    .line 752
    .local v2, "newAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 753
    .local v3, "newAppInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v13, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 754
    const-string v13, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 755
    :cond_5
    const-string v13, "TMSUserAppHolder"

    const-string v14, "handleMessage trying to add new package "

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    const-string v13, ","

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 758
    .local v9, "packages":[Ljava/lang/String;
    array-length v14, v9

    const/4 v13, 0x0

    :goto_3
    if-lt v13, v14, :cond_7

    .line 803
    .end local v9    # "packages":[Ljava/lang/String;
    :cond_6
    const-string v13, "TMSUserAppHolder"

    const-string v14, "Calling on PackageStatus change"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v13

    if-lez v13, :cond_1

    .line 805
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    invoke-static {v13}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    move-result-object v13

    invoke-interface {v13, v11, v3}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onPackageStatusChange(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 758
    .restart local v9    # "packages":[Ljava/lang/String;
    :cond_7
    aget-object v10, v9, v13

    .line 760
    .local v10, "pkg":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$2(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 761
    const/16 v16, 0x80

    .line 760
    move/from16 v0, v16

    invoke-virtual {v15, v10, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 762
    if-nez v5, :cond_9

    .line 758
    :cond_8
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 766
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getNewTMSPkg(Landroid/content/pm/ApplicationInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-static {v15, v5}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$3(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Landroid/content/pm/ApplicationInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v2

    .line 767
    const-string v15, "TMSUserAppHolder"

    const-string v16, "handleMessage added new package"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    :goto_5
    if-eqz v2, :cond_8

    .line 779
    invoke-interface {v3, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->regsiterAppForPkgStatus(Ljava/lang/String;)V
    invoke-static {v15, v10}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$4(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V

    goto :goto_4

    .line 769
    :catch_0
    move-exception v1

    .line 770
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v15, "TMSUserAppHolder"

    .line 771
    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "handleMessage failed to add new package  "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 772
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 771
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 770
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_5

    .line 783
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v9    # "packages":[Ljava/lang/String;
    .end local v10    # "pkg":Ljava/lang/String;
    :cond_a
    const-string v13, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 784
    const-string v13, "TMSUserAppHolder"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Package removed event obtained "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    const-string v13, ","

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 786
    .restart local v9    # "packages":[Ljava/lang/String;
    array-length v14, v9

    const/4 v13, 0x0

    :goto_6
    if-ge v13, v14, :cond_6

    aget-object v10, v9, v13

    .line 787
    .restart local v10    # "pkg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$5(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/util/Map;

    move-result-object v15

    invoke-interface {v15, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 788
    const-string v15, "TMSUserAppHolder"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Package removed event obtained "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$5(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/util/Map;

    move-result-object v15

    invoke-interface {v15, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "newAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 790
    .restart local v2    # "newAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$5(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/util/Map;

    move-result-object v15

    invoke-interface {v15, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->access$6(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v15

    invoke-virtual {v15, v10}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->delete(Ljava/lang/String;)Z

    .line 797
    invoke-interface {v3, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    :cond_b
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 688
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
