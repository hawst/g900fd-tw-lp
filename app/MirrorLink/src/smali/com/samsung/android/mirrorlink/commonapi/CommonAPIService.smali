.class public Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
.super Landroid/app/Service;
.source "CommonAPIService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;,
        Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;
    }
.end annotation


# static fields
.field public static final ACTION_BIND:Ljava/lang/String; = "com.mirrorlink.android.service.BIND"

.field private static final EVENT_ML_VERSION:I = 0x11

.field private static final LOG_TAG:Ljava/lang/String; = "TMSCommonAPIService"

.field public static final NOT_AVAILABLE:Ljava/lang/String; = "Not Available"

.field private static final REMOTE_DISPLAY_CONN:I = 0xe

.field private static final VNC_AUDIO_BLOCKING:I = 0xa

.field private static final VNC_CLIENT_DISPLAY_CONFIG_RECV:I = 0x1

.field private static final VNC_CLIENT_EVENT_CONFIG:I = 0x5

.field private static final VNC_CLIENT_PIXEL_FORMAT:I = 0x2

.field private static final VNC_DISP_SURFACE:I = 0x10

.field private static final VNC_DRIVE_MODE:I = 0xb

.field private static final VNC_EVENT_MAPPING:I = 0x6

.field private static final VNC_FB_BLOCKING:I = 0x8

.field private static final VNC_FB_OREINTATION_BLOCKING:I = 0x9

.field private static final VNC_FB_ORIENATION:I = 0x4

.field private static final VNC_NIGHT_MODE:I = 0xc

.field private static final VNC_OPEN_MIC:I = 0xd

.field private static final VNC_SERVER_SCALING_CONFIG:I = 0x3

.field private static final VNC_VIRTUAL_KEYBOARD_TEXT_ENTRY:I = 0x7

.field private static sCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;


# instance fields
.field private isInitialised:Z

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

.field private mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

.field private mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

.field private mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

.field private mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

.field private mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

.field private mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

.field private mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

.field private mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

.field private mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

.field private mNativeContext:I

.field private mNativeEvtHandler:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

.field private mNotificationManager:Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;

.field private mNumBounded:I

.field mServiceContext:Landroid/content/Context;

.field private mTmClientProfileService:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

.field private mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

.field private mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

.field private mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 67
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    .line 69
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 71
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .line 73
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 77
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .line 79
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    .line 81
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNotificationManager:Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    .line 101
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 102
    iput v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    .line 103
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mServiceContext:Landroid/content/Context;

    .line 104
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->isInitialised:Z

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Ljava/util/List;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNativeEvtHandler:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ContextManager;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    return-object v0
.end method

.method private static audioBlockingEventFromNative(Ljava/lang/Object;Ljava/lang/String;)I
    .locals 6
    .param p0, "obj_ref"    # Ljava/lang/Object;
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 381
    const-string v3, "TMSCommonAPIService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "audioBlockingEventFromNative - Enter :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    check-cast p0, Ljava/lang/ref/WeakReference;

    .line 384
    .end local p0    # "obj_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 385
    .local v0, "c":Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    if-nez v0, :cond_1

    .line 386
    const-string v3, "TMSCommonAPIService"

    const-string v4, "audioBlockingEventFromNative- obj_ref is NULL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    :goto_0
    return v2

    .line 389
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .line 390
    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setNativeAudioBlockInfo(Ljava/lang/String;)Z

    move-result v1

    .line 391
    .local v1, "ret":Z
    const-string v3, "TMSCommonAPIService"

    const-string v4, "audioBlockingEventFromNative - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static dataServiceEventFromNative(Ljava/lang/Object;ILjava/lang/String;)I
    .locals 6
    .param p0, "obj_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "param"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 397
    const-string v3, "TMSCommonAPIService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dataServiceEventFromNative - Enter :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    check-cast p0, Ljava/lang/ref/WeakReference;

    .line 400
    .end local p0    # "obj_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 401
    .local v0, "c":Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    if-nez v0, :cond_1

    .line 402
    const-string v3, "TMSCommonAPIService"

    const-string v4, "dataServiceEventFromNative- obj_ref is NULL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :cond_0
    :goto_0
    return v2

    .line 405
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 406
    invoke-virtual {v3, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->setNativeEventHandler(ILjava/lang/String;)Z

    move-result v1

    .line 407
    .local v1, "ret":Z
    const-string v3, "TMSCommonAPIService"

    const-string v4, "dataServiceEventFromNative - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static fbBlockingEventFromNative(Ljava/lang/Object;Ljava/lang/String;)I
    .locals 6
    .param p0, "obj_ref"    # Ljava/lang/Object;
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 366
    const-string v3, "TMSCommonAPIService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fbBlockingEventFromNative - Enter :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    check-cast p0, Ljava/lang/ref/WeakReference;

    .line 369
    .end local p0    # "obj_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 370
    .local v0, "c":Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    if-nez v0, :cond_1

    .line 371
    const-string v3, "TMSCommonAPIService"

    const-string v4, "fbBlockingEventFromNative- obj_ref is NULL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :cond_0
    :goto_0
    return v2

    .line 374
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    .line 375
    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeFbBlockInfo(Ljava/lang/String;)Z

    move-result v1

    .line 376
    .local v1, "ret":Z
    const-string v3, "TMSCommonAPIService"

    const-string v4, "fbBlockingEventFromNative - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->sCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    return-object v0
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 5
    .param p0, "obj_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 345
    const-string v2, "TMSCommonAPIService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "postEventFromNative - Enter - what:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    check-cast p0, Ljava/lang/ref/WeakReference;

    .line 349
    .end local p0    # "obj_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 350
    .local v0, "c":Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    if-nez v0, :cond_0

    .line 351
    const-string v2, "TMSCommonAPIService"

    const-string v3, "postEventFromNative- obj_ref is NULL"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNativeEvtHandler:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

    if-eqz v2, :cond_1

    .line 356
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNativeEvtHandler:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

    .line 357
    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 358
    .local v1, "m":Landroid/os/Message;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNativeEvtHandler:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->sendMessage(Landroid/os/Message;)Z

    .line 362
    .end local v1    # "m":Landroid/os/Message;
    :goto_1
    const-string v2, "TMSCommonAPIService"

    const-string v3, "postEventFromNative - Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :cond_1
    const-string v2, "TMSCommonAPIService"

    const-string v3, "postEventFromNative - mNativeHandler is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public convertEnabledToBoolean(I)Z
    .locals 1
    .param p1, "enabled"    # I

    .prologue
    const/4 v0, 0x1

    .line 333
    if-ne p1, v0, :cond_0

    .line 336
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method deInitTMSObjs()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    const-string v0, "TMSCommonAPIService"

    const-string v1, "deInitTMSObjs"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmClientProfileService:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    .line 183
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 184
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setMlSessionStatus(Z)V

    .line 185
    invoke-static {}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->deinit()V

    .line 186
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    .line 187
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 188
    return-void
.end method

.method deinitCommonApiObjs()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    const-string v0, "TMSCommonAPIService"

    const-string v1, "deinitCommonApiObjs"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->isInitialised:Z

    if-nez v0, :cond_0

    .line 226
    const-string v0, "TMSCommonAPIService"

    const-string v1, "deinitCommonApiObjs Already deinitialised return"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :goto_0
    return-void

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->isInitialised:Z

    .line 231
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setAudioConnManager(Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;)V

    .line 233
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->killListener()V

    .line 238
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->killListener()V

    .line 239
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->killListener()V

    .line 240
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->killListener()V

    .line 241
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->killListener()V

    .line 242
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->killListener()V

    .line 243
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->killListener()V

    .line 245
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->killListener()V

    .line 249
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    .line 250
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 251
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 252
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 253
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 254
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .line 255
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    .line 256
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .line 257
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    .line 258
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .line 259
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 260
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method public getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 2

    .prologue
    .line 313
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Enter : getTmsEngine"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    return-object v0
.end method

.method initCommonAPIObjs()V
    .locals 2

    .prologue
    .line 193
    const-string v0, "TMSCommonAPIService"

    const-string v1, "initCommonAPIObjs"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    .line 195
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .line 196
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 198
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 199
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    .line 200
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 201
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 202
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 203
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .line 204
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    .line 206
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNotificationManager:Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;

    .line 208
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .line 210
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setAudioConnManager(Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setContextManager(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->isInitialised:Z

    .line 219
    return-void
.end method

.method initTMSObjs()V
    .locals 6

    .prologue
    .line 154
    const-string v4, "TMSCommonAPIService"

    const-string v5, "initTMSObjs"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v4

    .line 156
    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getClientProfileSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v4

    .line 155
    iput-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmClientProfileService:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    .line 157
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyMirrorLinkSessionChanged(Z)V

    .line 159
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v0

    .line 163
    .local v0, "mClientprofileNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-eqz v0, :cond_0

    .line 164
    const-string v4, "TMSCommonAPIService"

    const-string v5, "initTMSObjs : To notifyDeviceInfoChanged "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget v1, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    .line 166
    .local v1, "majorVersion":I
    iget v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    .line 167
    .local v2, "minorVersion":I
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 168
    .local v3, "version":Landroid/os/Bundle;
    const-string v4, "VERSION_MAJOR"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    const-string v4, "VERSION_MINOR"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 170
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-virtual {v4, v3}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->notifyDeviceInfoChanged(Landroid/os/Bundle;)V

    .line 173
    .end local v1    # "majorVersion":I
    .end local v2    # "minorVersion":I
    .end local v3    # "version":Landroid/os/Bundle;
    :cond_0
    const-string v4, "TMSCommonAPIService"

    const-string v5, "initTMSObjs : To notifyCertStatusForAllPackage "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->notifyCertStatusForAllPackage()V

    .line 175
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getTMSAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 177
    return-void
.end method

.method public final native native_ca_enableMicOnClient(II)I
.end method

.method public final native native_ca_getClientDisplayInfo()Ljava/lang/String;
.end method

.method public final native native_ca_getClientPixelFormat()Ljava/lang/String;
.end method

.method public final native native_ca_getDriveMode()I
.end method

.method public final native native_ca_getEventConfig()Ljava/lang/String;
.end method

.method public final native native_ca_getEventMapping()Ljava/lang/String;
.end method

.method public final native native_ca_getKeyEventListSupport()I
.end method

.method public final native native_ca_getMicStatus()I
.end method

.method public final native native_ca_getNightMode()I
.end method

.method public final native native_ca_getOrientationSwitchSupport()I
.end method

.method public final native native_ca_getRemoteDisplayConnection()I
.end method

.method public final native native_ca_getServerScalingConfig()Ljava/lang/String;
.end method

.method public final native native_ca_getServerVKBSupport()Ljava/lang/String;
.end method

.method public final native native_ca_reSetAudioCtxtInfo(I)I
.end method

.method public final native native_ca_release()I
.end method

.method public final native native_ca_resetCtxtInfo(III)I
.end method

.method public final native native_ca_setAudioCtxtInfo(I[III)I
.end method

.method public final native native_ca_setEventMapping(Ljava/lang/Object;)I
.end method

.method public final native native_ca_setFbCtxtInfo(Ljava/lang/String;)I
.end method

.method public final native native_ca_setFbOrientationSupported(I)I
.end method

.method public final native native_ca_setKeyEventList([J)I
.end method

.method public final native native_ca_setup(Ljava/lang/Object;)I
.end method

.method public final native native_ca_switchFbOrientation(I)I
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 141
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Enter onBind "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    if-eqz p1, :cond_0

    const-string v0, "com.mirrorlink.android.service.BIND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    :cond_0
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Bind intent action is not matching"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/4 v0, 0x0

    .line 148
    :goto_0
    return-object v0

    .line 147
    :cond_1
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    .line 148
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 109
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->incrementCounter()V

    .line 110
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Enter on Create "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Incremented monitor value "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    sput-object p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->sCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    .line 114
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mServiceContext:Landroid/content/Context;

    .line 116
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Creating common api svc manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 118
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->initCommonAPIObjs()V

    .line 119
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 296
    const-string v0, "TMSCommonAPIService"

    const-string v1, "onDestroy CommonAPI service killed"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->deinitCommonApiObjs()V

    .line 302
    sput-object v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->sCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    .line 303
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 305
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->decrementCounter()V

    .line 306
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Decremented monitor value "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 309
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->killTMServiceProcess()V

    .line 310
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 134
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Enter onRebind "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    .line 136
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 137
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 125
    const-string v0, "TMSCommonAPIService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    if-eqz p1, :cond_0

    .line 127
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 320
    const-string v0, "TMSCommonAPIService"

    const-string v1, "onUnbind Called"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    if-eqz p1, :cond_0

    const-string v0, "com.mirrorlink.android.service.BIND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    .line 324
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-nez v0, :cond_0

    .line 325
    const-string v0, "TMSCommonAPIService"

    const-string v1, "stopSelf CommonAPI Service TMS Service not started"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->stopSelf()V

    .line 329
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 2
    .param p1, "tmsengine"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 269
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Setting tmsengine for common api"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 272
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-nez v0, :cond_1

    .line 273
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Deinit common api"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_release()I

    .line 281
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->deInitTMSObjs()V

    .line 292
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v0, "TMSCommonAPIService"

    const-string v1, "Setting tmsengine in commonapi"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    if-nez v0, :cond_2

    .line 285
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 286
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Landroid/content/Context;)V

    .line 285
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 289
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->initTMSObjs()V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCommonApiSvcManager:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    goto :goto_0
.end method

.method public stopCommonService()V
    .locals 4

    .prologue
    .line 660
    const-string v1, "TMSCommonAPIService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopCommonService  Status="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mNumBounded:I

    if-lez v1, :cond_0

    .line 668
    :goto_0
    return-void

    .line 664
    :cond_0
    const-string v1, "TMSCommonAPIService"

    const-string v2, "stopCommonService Stop called"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 666
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mServiceContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 667
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mServiceContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method
