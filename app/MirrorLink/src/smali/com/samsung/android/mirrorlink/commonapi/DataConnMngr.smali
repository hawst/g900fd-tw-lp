.class public Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
.super Ljava/lang/Object;
.source "DataConnMngr.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# static fields
.field private static final EVENT_UPDATE_DATASERVICE_LIST:I = 0xf

.field private static final EVENT_UPDATE_GPS_NMEA_LIST:I = 0x10

.field private static final LOG_TAG:Ljava/lang/String; = "TMSDataConnMngr"


# instance fields
.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

.field private mGetObjList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mGpsServiceID:I

.field private mLocationServiceID:I

.field private mRegisteredSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSetObjList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSubscribeObjSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    const/4 v0, -0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    .line 37
    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    .line 40
    const-string v0, "TMSDataConnMngr"

    const-string v1, "Enter DataConnMngr"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSetObjList:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGetObjList:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    .line 49
    const-string v0, "TMSDataConnMngr"

    const-string v1, "Exit DataConnMngr"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method


# virtual methods
.method public cleanUp()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 510
    const-string v0, "TMSDataConnMngr"

    const-string v1, "DataConn Mgr Cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 513
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 515
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSetObjList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 516
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGetObjList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 517
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 518
    iput v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    .line 519
    iput v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    .line 520
    return-void
.end method

.method public getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    if-nez v0, :cond_0

    .line 275
    const/4 v0, 0x0

    .line 277
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    goto :goto_0
.end method

.method public getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 254
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    if-nez v1, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-object v0

    .line 256
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 259
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    goto :goto_0
.end method

.method public getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    if-nez v1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-object v0

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    goto :goto_0
.end method

.method public getClientServiceList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getClientServiceList size ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    return-object v0
.end method

.method public getClientServiceNameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    return-object v0
.end method

.method public getGpsServiceID()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    return v0
.end method

.method public getLocationServiceID()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    return v0
.end method

.method public getObject(II)V
    .locals 7
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I

    .prologue
    .line 411
    const-string v4, "TMSDataConnMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DataConnMngr.getObject() -- Enter with serviceId = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " objectId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 413
    .local v3, "servId":Ljava/lang/Integer;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 415
    .local v2, "objId":Ljava/lang/Integer;
    const v4, 0xaac4540

    if-eq p2, v4, :cond_0

    .line 416
    const v4, -0x62f74e63

    if-ne p2, v4, :cond_3

    .line 417
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v5, "com.mirrorlink.gps"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 418
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    .line 420
    .local v0, "getObjAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    .line 421
    const-string v4, "TMSDataConnMngr"

    const-string v5, "get for gps"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGetObjList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    invoke-virtual {v0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getObject(I)V

    .line 447
    .end local v0    # "getObjAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :goto_0
    return-void

    .line 425
    .restart local v0    # "getObjAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_1
    const-string v4, "TMSDataConnMngr"

    const-string v5, "serviceId is not registered"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    .end local v0    # "getObjAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_2
    const-string v4, "TMSDataConnMngr"

    const-string v5, "Can\'t get now"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 430
    :cond_3
    const v4, 0x572a6461

    if-ne p2, v4, :cond_6

    .line 431
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v5, "com.mirrorlink.location"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 432
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v1

    .line 434
    .local v1, "getObjAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    .line 435
    const-string v4, "TMSDataConnMngr"

    const-string v5, "get for location"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGetObjList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    invoke-virtual {v1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->getObject(I)V

    goto :goto_0

    .line 439
    :cond_4
    const-string v4, "TMSDataConnMngr"

    const-string v5, "serviceId is not registered"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 442
    .end local v1    # "getObjAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    :cond_5
    const-string v4, "TMSDataConnMngr"

    const-string v5, "Can\'t get now"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 445
    :cond_6
    const-string v4, "TMSDataConnMngr"

    const-string v5, "inavlid Object id"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 2
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    .line 161
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    .line 163
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    .line 165
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_0

    .line 169
    const-string v0, "TMSDataConnMngr"

    const-string v1, "DataConnMngr.handleEvent() engine is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V
    .locals 4
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z
    .param p4, "object"    # Landroid/os/Bundle;

    .prologue
    .line 451
    const-string v1, "TMSDataConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notifyGetDataObjectResponse -- Enter with serviceId = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " objectId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 454
    .local v0, "serviObj":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    const-string v1, "TMSDataConnMngr"

    const-string v2, "mRegisteredSet has serviceID"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGetObjList:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 458
    :cond_0
    const-string v1, "TMSDataConnMngr"

    const-string v2, "mGetObjList has objectId or mSubscribeObjSet"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    if-eqz v1, :cond_1

    .line 460
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 468
    :cond_1
    :goto_0
    return-void

    .line 464
    :cond_2
    const-string v1, "TMSDataConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " not requested for get"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public notifyRegisterForService(IZ)V
    .locals 1
    .param p1, "servId"    # I
    .param p2, "result"    # Z

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->notifyRegisterForService(IZ)V

    .line 218
    :cond_0
    return-void
.end method

.method public notifySetDataObjectResponse(IIZ)V
    .locals 5
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z

    .prologue
    .line 473
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 475
    .local v1, "serviId":Ljava/lang/Integer;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 477
    .local v0, "objId":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 478
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSetObjList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 479
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    if-eqz v2, :cond_0

    .line 480
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->notifySetDataObjectResponse(IIZ)V

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " not requested for set"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public notifySubscribeResponse(IIZBI)V
    .locals 7
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z
    .param p4, "subscriptionType"    # B
    .param p5, "interval"    # I

    .prologue
    .line 491
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataConn Mgr : notifySubscribeResponse with service Id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Object Id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 494
    .local v6, "serviObj":Ljava/lang/Integer;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    const-string v0, "TMSDataConnMngr"

    const-string v1, "DataConn Mgr : notifySubscribeResponsemRegisteredSet.contains(serviObj)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 498
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    if-eqz v0, :cond_0

    .line 499
    const-string v0, "TMSDataConnMngr"

    const-string v1, "DataConn Mgr : notifySubscribeResponse to data service mngr"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->notifySubscribeResponse(IIZBI)V

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " not requested for subscribe"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public register(III)V
    .locals 5
    .param p1, "serviceId"    # I
    .param p2, "majorVer"    # I
    .param p3, "minorVer"    # I

    .prologue
    .line 176
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DataConnMngr.register() -- Enter "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 180
    .local v1, "serviObj":Ljava/lang/Integer;
    const/4 v2, -0x1

    if-eq p1, v2, :cond_3

    .line 181
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    .line 183
    .local v0, "regAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v3, "com.mirrorlink.gps"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 184
    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    if-ne v2, p1, :cond_1

    .line 185
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "registered with GPS"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 186
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    if-eqz v0, :cond_0

    .line 189
    const-string v2, "TMSDataConnMngr"

    .line 190
    const-string v3, "GPS_DataConnMngr.register() --getAcsDeviceMngr != null "

    .line 189
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 193
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startGpsService()V

    .line 212
    .end local v0    # "regAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :cond_0
    :goto_0
    return-void

    .line 195
    .restart local v0    # "regAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    .line 196
    const-string v3, "com.mirrorlink.location"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    if-ne v2, p1, :cond_2

    .line 198
    const-string v2, "TMSDataConnMngr"

    const-string v3, "registered with Location"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    if-eqz v0, :cond_0

    .line 200
    const-string v2, "TMSDataConnMngr"

    .line 201
    const-string v3, "LOCATION_DataConnMngr.register() --getAcsDeviceMngr != null "

    .line 200
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 204
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startLocationService()V

    goto :goto_0

    .line 207
    :cond_2
    const-string v2, "TMSDataConnMngr"

    const-string v3, "Invalid Service Id"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    .end local v0    # "regAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :cond_3
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "serviceId is invalid : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDataServicesManager(Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;)V
    .locals 0
    .param p1, "dataServicesManager"    # Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .line 55
    :cond_0
    return-void
.end method

.method public setNativeEventHandler(ILjava/lang/String;)Z
    .locals 3
    .param p1, "what"    # I
    .param p2, "param"    # Ljava/lang/String;

    .prologue
    .line 58
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter setNativeEventHandler - Event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 59
    const-string v2, " param: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    packed-switch p1, :pswitch_data_0

    .line 75
    :goto_0
    const-string v0, "TMSDataConnMngr"

    const-string v1, "Exit setNativeEventHandler"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    return v0

    .line 63
    :pswitch_0
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EVENT_UPDATE_DATASERVICE_LIST "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->updateClientServiceList(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_1
    const-string v0, "TMSDataConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EVENT_UPDATE_GPS_NMEA_LIST "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setObject(IILandroid/os/Bundle;)V
    .locals 6
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "object"    # Landroid/os/Bundle;

    .prologue
    .line 364
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 365
    .local v1, "servId":Ljava/lang/Integer;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 368
    .local v0, "objId":Ljava/lang/Integer;
    const v4, 0xaac4540

    if-eq p2, v4, :cond_0

    .line 369
    const v4, -0x62f74e63

    if-ne p2, v4, :cond_3

    .line 370
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v5, "com.mirrorlink.gps"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v2

    .line 373
    .local v2, "setAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 374
    const-string v4, "TMSDataConnMngr"

    const-string v5, "set for gps"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSetObjList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    invoke-virtual {v2, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->setObject(ILandroid/os/Bundle;)V

    .line 400
    .end local v2    # "setAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :goto_0
    return-void

    .line 378
    .restart local v2    # "setAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_1
    const-string v4, "TMSDataConnMngr"

    const-string v5, "serviceId is not registered"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 381
    .end local v2    # "setAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_2
    const-string v4, "TMSDataConnMngr"

    const-string v5, "Can\'t set now"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :cond_3
    const v4, 0x572a6461

    if-ne p2, v4, :cond_6

    .line 384
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v5, "com.mirrorlink.location"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 385
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v3

    .line 387
    .local v3, "setAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v3, :cond_4

    .line 388
    const-string v4, "TMSDataConnMngr"

    const-string v5, "set for location"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSetObjList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    invoke-virtual {v3, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->setObject(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 392
    :cond_4
    const-string v4, "TMSDataConnMngr"

    const-string v5, "serviceId is not registered"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 395
    .end local v3    # "setAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    :cond_5
    const-string v4, "TMSDataConnMngr"

    const-string v5, "Can\'t set now"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 398
    :cond_6
    const-string v4, "TMSDataConnMngr"

    const-string v5, "inavlid service id"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public subscribeObject(II)V
    .locals 6
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I

    .prologue
    .line 282
    const-string v3, "TMSDataConnMngr"

    .line 283
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataConnMngr.subscribeObject() -- Enter with serviceId ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " objectId ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 283
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 282
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 289
    .local v0, "serviObj":Ljava/lang/Integer;
    const v3, 0xaac4540

    if-eq p2, v3, :cond_0

    .line 290
    const v3, -0x62f74e63

    if-ne p2, v3, :cond_4

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v1

    .line 292
    .local v1, "subAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v4, "com.mirrorlink.gps"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 293
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 294
    if-eqz v1, :cond_1

    .line 295
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-virtual {v1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->subScribeRequest(I)V

    .line 322
    .end local v1    # "subAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_1
    :goto_0
    return-void

    .line 299
    .restart local v1    # "subAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_2
    const-string v3, "TMSDataConnMngr"

    const-string v4, "serviceId is not registered"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_3
    const-string v3, "TMSDataConnMngr"

    const-string v4, "Can\'t subscribe now"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    .end local v1    # "subAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_4
    const v3, 0x572a6461

    if-ne p2, v3, :cond_7

    .line 305
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v4, "com.mirrorlink.location"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 306
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v2

    .line 307
    .local v2, "subAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 308
    const-string v3, "TMSDataConnMngr"

    const-string v4, "subscribe for location"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    if-eqz v2, :cond_1

    .line 310
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual {v2, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->subScribeRequest(I)V

    goto :goto_0

    .line 314
    :cond_5
    const-string v3, "TMSDataConnMngr"

    const-string v4, "serviceId is not registered"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    .end local v2    # "subAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    :cond_6
    const-string v3, "TMSDataConnMngr"

    const-string v4, "Can\'t subscribe now"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    :cond_7
    const-string v3, "TMSDataConnMngr"

    const-string v4, "inavlid service id"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unRegister(I)V
    .locals 7
    .param p1, "serviceId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 222
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DataConnMngr.Unregister() -- Enter "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mGpsServiceID ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mLocationServiceID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 225
    .local v0, "serviObj":Ljava/lang/Integer;
    if-eq p1, v5, :cond_5

    .line 226
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    .line 228
    .local v1, "unregAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 229
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mRegisteredSet:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 230
    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    if-ne v2, p1, :cond_1

    .line 231
    if-eqz v1, :cond_0

    .line 232
    invoke-virtual {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 233
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopGpsService()V

    .line 235
    :cond_0
    iput v5, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    .line 251
    .end local v1    # "unregAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :goto_0
    return-void

    .line 236
    .restart local v1    # "unregAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :cond_1
    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    if-ne v2, p1, :cond_3

    .line 237
    if-eqz v1, :cond_2

    .line 238
    invoke-virtual {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V

    .line 239
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopLocationService()V

    .line 241
    :cond_2
    iput v5, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    goto :goto_0

    .line 243
    :cond_3
    const-string v2, "TMSDataConnMngr"

    const-string v3, "**********CRITICAL ERROR service id************ "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_4
    const-string v2, "TMSDataConnMngr"

    const-string v3, "Unregistering Error"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 249
    .end local v1    # "unregAcsDeviceMngr":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    :cond_5
    const-string v2, "TMSDataConnMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "serviceId is invalid : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unSubscribeObject(II)V
    .locals 5
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I

    .prologue
    .line 326
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 328
    .local v0, "serviObj":Ljava/lang/Integer;
    const v3, 0xaac4540

    if-eq p2, v3, :cond_0

    .line 329
    const v3, -0x62f74e63

    if-ne p2, v3, :cond_3

    .line 330
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v4, "com.mirrorlink.gps"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 331
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v1

    .line 333
    .local v1, "unsubAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    .line 334
    const-string v3, "TMSDataConnMngr"

    const-string v4, "unsubscribe for gps"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-virtual {v1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->unsubScribeRequest(I)V

    .line 336
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 360
    .end local v1    # "unsubAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :goto_0
    return-void

    .line 338
    .restart local v1    # "unsubAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_1
    const-string v3, "TMSDataConnMngr"

    const-string v4, "gps serviceId is not registered"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 341
    .end local v1    # "unsubAcsGpsService":Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    :cond_2
    const-string v3, "TMSDataConnMngr"

    const-string v4, "Can\'t unsubscribe now"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_3
    const v3, 0x572a6461

    if-ne p2, v3, :cond_6

    .line 344
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    const-string v4, "com.mirrorlink.location"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 345
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v2

    .line 347
    .local v2, "unsubAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    .line 348
    const-string v3, "TMSDataConnMngr"

    const-string v4, "unsubscribe for location"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-virtual {v2, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->unsubScribeRequest(I)V

    .line 350
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mSubscribeObjSet:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 352
    :cond_4
    const-string v3, "TMSDataConnMngr"

    const-string v4, "loc serviceId is not registered"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 355
    .end local v2    # "unsubAcsLocationSink":Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    :cond_5
    const-string v3, "TMSDataConnMngr"

    const-string v4, "Can\'t unsubscribe now"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_6
    const-string v3, "TMSDataConnMngr"

    const-string v4, "inavlid service id"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateClientServiceList(Ljava/lang/String;)V
    .locals 21
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 90
    const-string v16, "TMSDataConnMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "updateClientServiceList: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 94
    const-string v16, "&"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 98
    .local v15, "serviceInfo":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v17, v0

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_1

    .line 153
    const-string v16, "TMSDataConnMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "mServiceList size =: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    move-object/from16 v16, v0

    if-eqz v16, :cond_0

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mDataServicesManager:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->notifyAvailableServicesChanged(Ljava/util/List;)V

    .line 157
    :cond_0
    return-void

    .line 98
    :cond_1
    aget-object v5, v15, v16

    .line 100
    .local v5, "iter":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 101
    .local v2, "bundle":Landroid/os/Bundle;
    const/4 v3, 0x0

    .line 102
    .local v3, "isGps":Z
    const/4 v4, 0x0

    .line 104
    .local v4, "isLoc":Z
    new-instance v13, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v13}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 105
    .local v13, "servInfo":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v13, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 107
    const-string v18, "SERVICE_NAME"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 109
    .local v10, "name":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 110
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    .line 111
    .local v11, "name1":Ljava/lang/String;
    const-string v18, "gps"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    move-object/from16 v18, v0

    const-string v19, "com.mirrorlink.gps"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    const-string v18, "SERVICE_NAME"

    .line 114
    const-string v19, "com.mirrorlink.gps"

    .line 113
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const/4 v3, 0x1

    .line 131
    .end local v11    # "name1":Ljava/lang/String;
    :cond_2
    :goto_1
    const-string v18, "VERSION_MAJOR"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 132
    .local v7, "majorVersion":Ljava/lang/String;
    const-string v18, "VERSION_MINOR"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 133
    .local v9, "minorVersion":Ljava/lang/String;
    const-string v18, "SERVICE_ID"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 135
    .local v14, "serviceID":Ljava/lang/String;
    const-string v18, "TMSDataConnMngr"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, " Service Name ="

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "MAJOR = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "MINOR "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " SERVICEID "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 138
    .local v6, "majorVer":I
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 139
    .local v8, "minorVer":I
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 140
    .local v12, "servId":I
    if-eqz v3, :cond_6

    .line 141
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mGpsServiceID:I

    .line 146
    :cond_3
    :goto_2
    const-string v18, "VERSION_MAJOR"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 147
    const-string v18, "VERSION_MINOR"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v18, "SERVICE_ID"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceList:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 117
    .end local v6    # "majorVer":I
    .end local v7    # "majorVersion":Ljava/lang/String;
    .end local v8    # "minorVer":I
    .end local v9    # "minorVersion":Ljava/lang/String;
    .end local v12    # "servId":I
    .end local v14    # "serviceID":Ljava/lang/String;
    .restart local v11    # "name1":Ljava/lang/String;
    :cond_4
    const-string v18, "location"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    move-object/from16 v18, v0

    const-string v19, "com.mirrorlink.location"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v18, "SERVICE_NAME"

    .line 120
    const-string v19, "com.mirrorlink.location"

    .line 119
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v4, 0x1

    .line 122
    goto/16 :goto_1

    .line 123
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mServiceNameList:Ljava/util/List;

    move-object/from16 v18, v0

    const-string v19, "com.mirrorlink.gps"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v18, "SERVICE_NAME"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v18, "TMSDataConnMngr"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Ignored name: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 142
    .end local v11    # "name1":Ljava/lang/String;
    .restart local v6    # "majorVer":I
    .restart local v7    # "majorVersion":Ljava/lang/String;
    .restart local v8    # "minorVer":I
    .restart local v9    # "minorVersion":Ljava/lang/String;
    .restart local v12    # "servId":I
    .restart local v14    # "serviceID":Ljava/lang/String;
    :cond_6
    if-eqz v4, :cond_3

    .line 143
    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->mLocationServiceID:I

    goto :goto_2
.end method
