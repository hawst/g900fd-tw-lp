.class public Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;
.super Ljava/lang/Object;
.source "CdbAppholder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "FRAMEWORKS/CdbAppholder"

.field private static final TM_AUDIO_CDB_HOLDERNAME:Ljava/lang/String; = "CdbAppholder"

.field private static final TM_PROTOCOL_ID:Ljava/lang/String; = "CDB"

.field private static final TM_RESOURCE_STATUS_FREE:Ljava/lang/String; = "free"

.field private static final TM_TRUST_LEVEL:I = 0x80


# instance fields
.field private APP_DESC_CDB:Ljava/lang/String;

.field private APP_DISPLAY_NAME_CDB:Ljava/lang/String;

.field private APP_NAME_CDB:Ljava/lang/String;

.field private cntxt:Landroid/content/Context;

.field private mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppIsRunningCdbApp:Z

.field private mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

.field private mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

.field private mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const-string v0, "Common Data Bus"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_NAME_CDB:Ljava/lang/String;

    .line 60
    const-string v0, "CDB Server Endpoint"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_DESC_CDB:Ljava/lang/String;

    .line 62
    const-string v0, "Common Data Bus"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_DISPLAY_NAME_CDB:Ljava/lang/String;

    .line 88
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "Enter : CdbAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->cntxt:Landroid/content/Context;

    .line 92
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->cntxt:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 93
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "ERROR !!! No Context"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .line 98
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "Exit : CdbAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private fillAppStatus()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method private launchCdbApp()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppIsRunningCdbApp:Z

    .line 145
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->cdbStart()I

    .line 147
    :cond_0
    return-void
.end method

.method private setConfig()V
    .locals 9

    .prologue
    .line 108
    const-string v6, "FRAMEWORKS/CdbAppholder"

    const-string v7, " setConfig "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 114
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "cdb_self_port_key"

    const-string v7, "9980"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "selfPort":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 118
    .local v2, "iSelfPort":I
    const-string v6, "FRAMEWORKS/CdbAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "selfPort "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "iSelfPort "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v6, "bt_ConnectionInitateEnable"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 122
    .local v0, "ConnectionPref":Z
    const-string v6, "FRAMEWORKS/CdbAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bt_ConnectionInitateEnable "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "iBtStartConnectionPref":I
    if-eqz v0, :cond_0

    .line 127
    const/4 v1, 0x1

    .line 129
    :cond_0
    const-string v6, "FRAMEWORKS/CdbAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "iBtStartConnectionPref "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v3, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v3}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 133
    .local v3, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v6, "CdbSelfPort"

    invoke-virtual {v3, v6, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 134
    const-string v6, "iBtStartConnectionPref"

    invoke-virtual {v3, v6, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 136
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v6, :cond_1

    .line 137
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setCdbParams(Ljava/lang/String;)I

    .line 139
    :cond_1
    const-string v6, "FRAMEWORKS/CdbAppholder"

    const-string v7, " setConfig "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method private stopCdbApp()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppIsRunningCdbApp:Z

    .line 151
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->cdbStop()I

    .line 153
    :cond_0
    return-void
.end method


# virtual methods
.method public deinit()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 173
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppIsRunningCdbApp:Z

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->cdbStop()I

    .line 178
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 179
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppIsRunningCdbApp:Z

    .line 180
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 181
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 182
    return v2
.end method

.method public destroy()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return v0
.end method

.method public getAllAppList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppList()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "CdbAppholder.getAppList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->fillAppStatus()V

    .line 283
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "CdbAppholder.getAppList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 257
    if-nez p1, :cond_0

    .line 258
    const-string v1, "FRAMEWORKS/CdbAppholder"

    const-string v2, "getAppStatus(packageName == null)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :goto_0
    return-object v0

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->fillAppStatus()V

    .line 269
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    const-string v1, "FRAMEWORKS/CdbAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "getAppStatus : packageName found. Hence returning status : "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 274
    :cond_1
    const-string v1, "FRAMEWORKS/CdbAppholder"

    const-string v2, "getAppStatus : packageName not found. Hence returning null "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAppStatusCdbApp()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public getNotRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public informAppStatusChange(Ljava/lang/String;I)V
    .locals 2
    .param p1, "app"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    if-eqz v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->fillAppStatus()V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 170
    :cond_0
    return-void
.end method

.method public init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
    .locals 8
    .param p1, "appmngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "cntxt"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, -0x1

    .line 202
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "CdbAppholder.init enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 204
    :cond_0
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "mDbInfo is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :goto_0
    return v4

    .line 208
    :cond_1
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 210
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_DISPLAY_NAME_CDB:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 211
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_NAME_CDB:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 212
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_DESC_CDB:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 214
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/high16 v1, -0x10000000

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 215
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v1, 0x80

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 216
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 217
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 218
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 219
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 222
    const-string v1, "CDB"

    .line 223
    const-string v2, "1.1"

    const/4 v3, 0x0

    move v5, v4

    .line 221
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 226
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->fillAppStatus()V

    .line 228
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 229
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 233
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v0

    long-to-int v6, v0

    .line 234
    .local v6, "rowNum":I
    if-eq v6, v7, :cond_4

    .line 235
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v6, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 236
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :cond_3
    :goto_1
    const-string v0, "CdbAppholder"

    invoke-virtual {p1, v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 247
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, "CdbAppholder.init exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 238
    :cond_4
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v6

    .line 239
    if-eqz v6, :cond_3

    .line 240
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v6, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 241
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppInfoCdbApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
    .locals 1
    .param p1, "appStatusListener"    # Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .line 253
    const/4 v0, 0x1

    return v0
.end method

.method public setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 2
    .param p1, "nativeIface"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 103
    const-string v0, "FRAMEWORKS/CdbAppholder"

    const-string v1, " setnativeinterface "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 105
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->setConfig()V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_NAME_CDB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->launchCdbApp()V

    .line 292
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->APP_NAME_CDB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->stopCdbApp()V

    .line 299
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
