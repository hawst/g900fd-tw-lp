.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.super Ljava/lang/Object;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHolder;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;
    }
.end annotation


# static fields
.field private static final ACS_CALL_IDLE_EVT:I = 0x4

.field private static final ACS_CALL_OFF_HOOK_EVT:I = 0x2

.field private static final ACS_CALL_RINGING_EVT:I = 0x3

.field public static final ACS_FAILURE:I = -0x1

.field public static final ACS_SUCCESS:I = 0x0

.field public static final ACTION_TM_ALIVEREQ_CTS:Ljava/lang/String; = "CTS_TESTS_ALIVE_REQ_HU_GPS"

.field public static final ACTION_TM_CANCEL_CTS:Ljava/lang/String; = "CTS_TESTS_CANCEL_HU_GPS"

.field public static final ACTION_TM_CANCEL_LOCATION:Ljava/lang/String; = "CTS_TESTS_CANCEL_SUBSCRIBE_LOCATION"

.field public static final ACTION_TM_GEO_LOCATION:Ljava/lang/String; = "CTS_TESTS_GET_GEO_LOCATION"

.field public static final ACTION_TM_GET_NMEA_CTS:Ljava/lang/String; = "CTS_TESTS_GET_NMEA_GPS"

.field public static final ACTION_TM_GPS_START:Ljava/lang/String; = "START_HU_GPS"

.field public static final ACTION_TM_GPS_STOP:Ljava/lang/String; = "STOP_HU_GPS"

.field public static final ACTION_TM_GPS_SUBSCRIBE:Ljava/lang/String; = "SUBSRCRIBE_HU_GPS"

.field public static final ACTION_TM_LOCATION_START:Ljava/lang/String; = "START_HU_LOCATION"

.field public static final ACTION_TM_LOCATION_STOP:Ljava/lang/String; = "STOP_HU_LOCATION"

.field public static final ACTION_TM_SUBSCRIBE_CTS:Ljava/lang/String; = "CTS_TESTS_SUBSCRIBE_HU_GPS"

.field public static final ACTION_TM_SUBSCRIBE_LOCATION:Ljava/lang/String; = "CTS_TESTS_SUBSCRIBE_HU_LOCATION"

.field public static final CDB_START_SERVICE:I = 0xb103

.field public static final CDB_STOP_SERVICE:I = 0xb104

.field private static final EVENT_DEINIT_GPS_SERVICE:I = 0xd

.field private static final EVENT_DEINIT_LOCATION_SINK:I = 0x17

.field private static final EVENT_DEINIT_PHONE_LISTNER:I = 0x2

.field private static final EVENT_INIT_GPS_SERVICE:I = 0xa

.field private static final EVENT_INIT_LOCATION_SINK:I = 0x14

.field private static final EVENT_INIT_PHONE_LISTNER:I = 0x1

.field private static final EVENT_START_GPS_SERVICE:I = 0xb

.field private static final EVENT_START_LOCATION_SINK:I = 0x15

.field private static final EVENT_STOP_GPS_SERVICE:I = 0xc

.field private static final EVENT_STOP_LOCATION_SINK:I = 0x16

.field private static final EVENT_SUBSCRPTION_INTERVAL_GPS_SERVICE:I = 0xe

.field private static final EVENT_SUBSCRPTION_INTERVAL_LOCATION_SINK:I = 0x18

.field private static final EVENT_TM_REGISTER_GPS_STATE_CHANGE:I = 0xf

.field private static final EVENT_TM_UNREGISTER_GPS_STATE_CHANGE:I = 0x10

.field public static final EXTRA_INTERVAL_ACTION_TM_GPS_SUBSCRIBE:Ljava/lang/String; = "SUBSRCRIBE_INTERVAL_HU_GPS"

.field public static final EXTRA_TYPE_ACTION_TM_GPS_SUBSCRIBE:Ljava/lang/String; = "SUBSRCRIBE_TYPE_HU_GPS"

.field public static final SBP_CANCEL:I = 0xb4

.field public static final SBP_GET:I = 0xb1

.field public static final SBP_SET:I = 0xb2

.field public static final SBP_SUBSCRIBE:I = 0xb3

.field private static final TAG:Ljava/lang/String; = "TMSDevMngr"


# instance fields
.field private ACS_VNC_DISABLE_ORIENTATION:Z

.field private final INJECT_METAKEY_EVENT_KITKAT:Z

.field private isLandscapeSet:Z

.field private isPortraitSet:Z

.field private mActivtyMngr:Landroid/app/ActivityManager;

.field private mAutoRotation:Z

.field private mCxt:Landroid/content/Context;

.field private mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

.field private mDisplay:Landroid/view/Display;

.field private mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

.field private mEvtHandler:Landroid/os/Handler;

.field private mEvts:I

.field private mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

.field private mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

.field private mHomeKeyDownTime:J

.field private mInputManager:Ljava/lang/Object;

.field private mIsDeviceLock:Z

.field private mIsHomeKeyDown:Z

.field private mKeyModifierList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:Landroid/location/LocationManager;

.field private mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

.field private mLooper:Landroid/os/Looper;

.field private mMainHandler:Landroid/os/Handler;

.field private mNativeContext:I

.field private mNightMode:Z

.field private mNightModeValue:I

.field private mNonce:Ljava/lang/String;

.field private mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

.field private mPkgMngr:Landroid/content/pm/PackageManager;

.field private mPowerMngr:Landroid/os/PowerManager;

.field private mPrevMotionEventAction:I

.field private mPrevMotionEventX:I

.field private mPrevMotionEventY:I

.field private mPrevOrientation:I

.field private mPrevTouchEvent:I

.field private mPrevTouchEventState:I

.field private mSurface:Landroid/view/Surface;

.field private mTime:J

.field private mUIModeMngr:Landroid/app/UiModeManager;

.field private mVirtualDisplay:Ljava/lang/Object;

.field private mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

.field private mVoiceRxCallBack:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;

.field private mWindowManager:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-boolean v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->ACS_VNC_DISABLE_ORIENTATION:Z

    .line 113
    iput-boolean v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mIsDeviceLock:Z

    .line 114
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    .line 115
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mInputManager:Ljava/lang/Object;

    .line 116
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    .line 126
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    .line 129
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    .line 130
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mSurface:Landroid/view/Surface;

    .line 136
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightMode:Z

    .line 142
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    .line 145
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    .line 176
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightModeValue:I

    .line 178
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mTime:J

    .line 2026
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    .line 2028
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEvent:I

    .line 3569
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    .line 301
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.AcsDeviceMngr() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventAction:I

    .line 304
    iput v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventX:I

    .line 305
    iput v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventY:I

    .line 306
    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    .line 307
    iput-boolean v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mIsHomeKeyDown:Z

    .line 308
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->INJECT_METAKEY_EVENT_KITKAT:Z

    .line 309
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.AcsDeviceMngr() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 3142
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->deInitGpsService()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 3450
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->deInitLocationService()V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2921
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->onCallStateChanged(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 3100
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initGpsService()V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 3407
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initLocationService()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mIsDeviceLock:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mIsDeviceLock:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I
    .locals 1

    .prologue
    .line 708
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getOrientation()I

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;I)V
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    return-void
.end method

.method private checkRotation(II)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    .locals 19
    .param p1, "inVncX"    # I
    .param p2, "inVncY"    # I

    .prologue
    .line 1618
    const-string v16, "TMSDevMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, " checkRotation Enter x: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " y: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    new-instance v5, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v5, v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;-><init>(II)V

    .line 1623
    .local v5, "coorOrig":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    const/4 v11, 0x0

    .line 1624
    .local v11, "isDevOreintationPortrait":Z
    const/4 v10, 0x0

    .line 1629
    .local v10, "isAppOreintationPortrait":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mActivtyMngr:Landroid/app/ActivityManager;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v13

    .line 1631
    .local v13, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v13, :cond_0

    .line 1632
    const-string v16, "TMSDevMngr"

    const-string v17, "Running tasks is null"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747
    .end local v5    # "coorOrig":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    :goto_0
    return-object v5

    .line 1635
    .restart local v5    # "coorOrig":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    :cond_0
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/ActivityManager$RunningTaskInfo;

    move-object/from16 v0, v16

    iget-object v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 1636
    .local v3, "activityCom":Landroid/content/ComponentName;
    const-string v16, "TMSDevMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "checkRotation : class "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 1637
    const-string v18, " package "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1636
    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1642
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPkgMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v16, v0

    .line 1643
    const/16 v17, 0x80

    .line 1642
    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1650
    .local v2, "actInfo":Landroid/content/pm/ActivityInfo;
    const-string v16, "TMSDevMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, " checkRotation Enter orientation "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1651
    iget v0, v2, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1650
    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    iget v0, v2, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    move/from16 v16, v0

    packed-switch v16, :pswitch_data_0

    .line 1675
    const-string v16, "TMSDevMngr"

    const-string v17, " This code will never come here "

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 1684
    .local v15, "sharedPref":Landroid/content/SharedPreferences;
    const-string v16, "screen_mode_key"

    const-string v17, "1"

    invoke-interface/range {v15 .. v17}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1685
    .local v14, "screen_mode":Ljava/lang/String;
    const-string v16, "1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 1687
    const/4 v11, 0x0

    .line 1688
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation - Device is LANDSCAPE"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1696
    :goto_2
    const-string v16, "TMSDevMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "checkRotation - Adjust isDevOreintationPortrait"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1697
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " isAppOreintationPortrait "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 1698
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1696
    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1703
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    .line 1705
    .local v6, "d":Landroid/view/Display;
    new-instance v12, Landroid/util/DisplayMetrics;

    invoke-direct {v12}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1706
    .local v12, "m":Landroid/util/DisplayMetrics;
    invoke-virtual {v6, v12}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1707
    iget v8, v12, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1708
    .local v8, "displayWidth":I
    iget v7, v12, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1709
    .local v7, "displayHeight":I
    const-string v16, "TMSDevMngr"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "checkRotation - "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1716
    if-nez v11, :cond_2

    .line 1717
    if-eqz v10, :cond_2

    .line 1719
    new-instance v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;

    iget v0, v5, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mY:I

    move/from16 v16, v0

    sub-int v16, v8, v16

    iget v0, v5, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mX:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v4, v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;-><init>(II)V

    .local v4, "coorNew":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    move-object v5, v4

    .line 1720
    goto/16 :goto_0

    .line 1645
    .end local v2    # "actInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "coorNew":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    .end local v6    # "d":Landroid/view/Display;
    .end local v7    # "displayHeight":I
    .end local v8    # "displayWidth":I
    .end local v12    # "m":Landroid/util/DisplayMetrics;
    .end local v14    # "screen_mode":Ljava/lang/String;
    .end local v15    # "sharedPref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v9

    .line 1647
    .local v9, "e":Ljava/lang/Exception;
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation : getActivityInfo failed"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1656
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v2    # "actInfo":Landroid/content/pm/ActivityInfo;
    :pswitch_0
    const/4 v10, 0x1

    .line 1657
    goto/16 :goto_1

    .line 1659
    :pswitch_1
    const/4 v10, 0x0

    .line 1660
    goto/16 :goto_1

    .line 1671
    :pswitch_2
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation : Sending what VNC provided"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1692
    .restart local v14    # "screen_mode":Ljava/lang/String;
    .restart local v15    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_1
    const/4 v11, 0x1

    .line 1693
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation - Device is POTRAIT"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1726
    .restart local v6    # "d":Landroid/view/Display;
    .restart local v7    # "displayHeight":I
    .restart local v8    # "displayWidth":I
    .restart local v12    # "m":Landroid/util/DisplayMetrics;
    :cond_2
    if-nez v11, :cond_3

    .line 1727
    if-eqz v10, :cond_3

    .line 1729
    new-instance v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;

    iget v0, v5, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mY:I

    move/from16 v16, v0

    iget v0, v5, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mX:I

    move/from16 v17, v0

    sub-int v17, v7, v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v4, v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;-><init>(II)V

    .restart local v4    # "coorNew":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    move-object v5, v4

    .line 1730
    goto/16 :goto_0

    .line 1737
    .end local v4    # "coorNew":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    :cond_3
    if-ne v11, v10, :cond_4

    .line 1739
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation - Dont Rotate at all"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    :goto_3
    const-string v16, "TMSDevMngr"

    const-string v17, " checkRotation Exit"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1743
    :cond_4
    const-string v16, "TMSDevMngr"

    const-string v17, "checkRotation - unhandled"

    invoke-static/range {v16 .. v17}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1653
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private deInitCallAudio()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2889
    const-string v0, "TMSDevMngr"

    const-string v1, "deInitCallAudio: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->deInitPhoneStateListner()V

    .line 2893
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    if-eqz v0, :cond_0

    .line 2895
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->stop()Z

    .line 2896
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    .line 2898
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceRxCallBack:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;

    .line 2900
    const-string v0, "TMSDevMngr"

    const-string v1, "deInitCallAudio: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2901
    return-void
.end method

.method private deInitGpsService()V
    .locals 3

    .prologue
    .line 3144
    const-string v1, "TMSDevMngr"

    const-string v2, "deInitGpsService: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3146
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3147
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xd

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3148
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3150
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->unRegisterGpsListener()V

    .line 3152
    const-string v1, "TMSDevMngr"

    const-string v2, "deInitGpsService: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3153
    return-void
.end method

.method private deInitLocationService()V
    .locals 3

    .prologue
    .line 3452
    const-string v1, "TMSDevMngr"

    const-string v2, "deInitLocationService: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3454
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3455
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x17

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3456
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3458
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->unRegisterGpsListener()V

    .line 3460
    const-string v1, "TMSDevMngr"

    const-string v2, "deInitLocationService: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3461
    return-void
.end method

.method private deInitPhoneStateListner()V
    .locals 2

    .prologue
    .line 2634
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2635
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2636
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2638
    return-void
.end method

.method private fakeKeyEvent(Landroid/view/KeyEvent;I)V
    .locals 3
    .param p1, "fakeEvent"    # Landroid/view/KeyEvent;
    .param p2, "modifer"    # I

    .prologue
    .line 1586
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1588
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fakeKeyEvent inject fake event for modifier"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1589
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1588
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1590
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1593
    :cond_0
    return-void
.end method

.method private getAcsCertificates()Lcom/samsung/android/mirrorlink/portinginterface/AcsCertificates;
    .locals 1

    .prologue
    .line 2828
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsCertificates;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsCertificates;-><init>()V

    return-object v0
.end method

.method public static getAcsDeviceMngr()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 186
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getAcsDeviceMngr() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHolder;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    return-object v0
.end method

.method private getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 2825
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public static getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    .locals 2

    .prologue
    .line 191
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getAcsDeviceMngr() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHolder;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    return-object v0
.end method

.method private getKeyEventObj(III)Landroid/view/KeyEvent;
    .locals 10
    .param p1, "androidKeyAction"    # I
    .param p2, "keycode"    # I
    .param p3, "meta"    # I

    .prologue
    .line 1546
    const/4 v3, 0x0

    .line 1547
    .local v3, "keyEvent":Landroid/view/KeyEvent;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1551
    .local v4, "now":J
    :try_start_0
    const-string v6, "android.view.KeyEvent"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const/16 v7, 0xb

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget-object v9, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x3

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x4

    .line 1552
    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x5

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x6

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x7

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/16 v8, 0x8

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/16 v8, 0x9

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/16 v8, 0xa

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    .line 1551
    invoke-virtual {v6, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 1554
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/16 v6, 0xb

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0xa

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    invoke-virtual {v8}, Landroid/view/Display;->getDisplayId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Landroid/view/KeyEvent;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    .line 1574
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-object v3

    .line 1555
    :catch_0
    move-exception v2

    .line 1557
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 1558
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .line 1560
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1561
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_2
    move-exception v2

    .line 1563
    .local v2, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 1564
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_3
    move-exception v2

    .line 1566
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 1567
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v2

    .line 1569
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1570
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_5
    move-exception v2

    .line 1572
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private getNightMode()I
    .locals 5

    .prologue
    .line 626
    const-string v2, "TMSDevMngr"

    const-string v3, "getNightMode "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightMode:Z

    if-eqz v2, :cond_1

    .line 629
    iget v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightModeValue:I

    .line 647
    :cond_0
    :goto_0
    return v1

    .line 634
    :cond_1
    const/4 v1, -0x1

    .line 635
    .local v1, "ret":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    if-nez v2, :cond_2

    .line 636
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initUIModeManagerService()V

    .line 637
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    if-eqz v2, :cond_0

    .line 639
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 640
    .local v0, "config":Landroid/content/res/Configuration;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AcsDeviceMngr.getNightMode() - config.uiMode = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 641
    iget v4, v0, Landroid/content/res/Configuration;->uiMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 640
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    iget v2, v0, Landroid/content/res/Configuration;->uiMode:I

    const/16 v3, 0x23

    if-ne v2, v3, :cond_3

    .line 643
    const/4 v1, 0x1

    goto :goto_0

    .line 645
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getOrientation()I
    .locals 5

    .prologue
    .line 709
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr::getOrientation "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const/4 v1, -0x1

    .line 713
    .local v1, "ret":I
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getRotation()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 719
    :goto_0
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " AcsDeviceMngr.getOrientation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 725
    :cond_0
    const/4 v1, 0x1

    .line 729
    :cond_1
    :goto_1
    return v1

    .line 715
    :catch_0
    move-exception v0

    .line 717
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " AcsDeviceMngr.getOrientation Exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 726
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 727
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getPixelFormatString(I)Ljava/lang/String;
    .locals 1
    .param p1, "p"    # I

    .prologue
    .line 1004
    packed-switch p1, :pswitch_data_0

    .line 1014
    const-string v0, "None"

    :goto_0
    return-object v0

    .line 1008
    :pswitch_0
    const-string v0, "RGB_565"

    goto :goto_0

    .line 1011
    :pswitch_1
    const-string v0, "RGB_888"

    goto :goto_0

    .line 1004
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getRotation()I
    .locals 6

    .prologue
    .line 737
    const-string v3, "TMSDevMngr"

    const-string v4, "AcsDeviceMngr::getRotation "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    const/4 v2, -0x1

    .line 741
    .local v2, "ret":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getRotation"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 752
    :goto_0
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " AcsDeviceMngr.getRotation "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    return v2

    .line 742
    :catch_0
    move-exception v0

    .line 743
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " AcsDeviceMngr.getRotation NoSuchMethodException"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 744
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 745
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " AcsDeviceMngr.getRotation InvocationTargetException"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 746
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v1

    .line 747
    .local v1, "e1":Ljava/lang/IllegalAccessException;
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " AcsDeviceMngr.getRotation IllegalAccessException"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 748
    .end local v1    # "e1":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 749
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " AcsDeviceMngr.getRotation IllegalArgumentException"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getScreenSaver()I
    .locals 4

    .prologue
    .line 684
    const-string v2, "TMSDevMngr"

    const-string v3, "getScreenSaver "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    const/4 v1, -0x1

    .line 687
    .local v1, "ret":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    if-nez v2, :cond_0

    .line 688
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initPowerManagerService()V

    .line 690
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    if-eqz v2, :cond_1

    .line 692
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 693
    .local v0, "isScreenOn":Z
    if-eqz v0, :cond_2

    .line 694
    const/4 v1, 0x0

    .line 701
    .end local v0    # "isScreenOn":Z
    :cond_1
    :goto_0
    return v1

    .line 697
    .restart local v0    # "isScreenOn":Z
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private handleFakeKeyEvent(IILandroid/view/KeyEvent;)V
    .locals 5
    .param p1, "androidKeyAction"    # I
    .param p2, "fakeModifier"    # I
    .param p3, "androidEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 1271
    const-string v2, "TMSDevMngr"

    const-string v3, "Enter : handleFakeKeyEvent "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "androidKeyAction"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fakeModifier "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1273
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " androidEvent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1272
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 1278
    packed-switch p1, :pswitch_data_0

    .line 1307
    :cond_0
    :goto_0
    return-void

    .line 1283
    :pswitch_0
    const/4 v2, 0x0

    .line 1282
    :try_start_0
    invoke-direct {p0, p1, p2, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v1

    .line 1284
    .local v1, "fakeKeyEventObj":Landroid/view/KeyEvent;
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1285
    const/4 v2, 0x0

    invoke-direct {p0, p3, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1302
    .end local v1    # "fakeKeyEventObj":Landroid/view/KeyEvent;
    :catch_0
    move-exception v0

    .line 1304
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "TMSDevMngr"

    const-string v3, " handleFakeKeyEvent Exception"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1292
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    const/4 v2, 0x0

    .line 1291
    :try_start_1
    invoke-direct {p0, p1, p2, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v1

    .line 1293
    .restart local v1    # "fakeKeyEventObj":Landroid/view/KeyEvent;
    const/4 v2, 0x0

    invoke-direct {p0, p3, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1294
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1278
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleJambitLineEnd(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;)V
    .locals 6
    .param p1, "andKey"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;

    .prologue
    .line 1153
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 1155
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x5

    if-lt v2, v3, :cond_1

    .line 1182
    .end local v2    # "i":I
    :cond_0
    return-void

    .line 1163
    .restart local v2    # "i":I
    :cond_1
    const/4 v3, 0x0

    .line 1164
    :try_start_0
    iget v4, p1, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v5, 0x0

    .line 1163
    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v0

    .line 1165
    .local v0, "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1171
    const/4 v3, 0x1

    .line 1172
    iget v4, p1, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v5, 0x0

    .line 1171
    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v0

    .line 1173
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1155
    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1176
    :catch_0
    move-exception v1

    .line 1178
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "TMSDevMngr"

    const-string v4, " handleJambitLineEnd Exception"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private handleKnobEvent(II)V
    .locals 13
    .param p1, "action"    # I
    .param p2, "eventCode"    # I

    .prologue
    .line 1190
    const/4 v1, 0x0

    .line 1192
    .local v1, "andkeyCode":I
    const/4 v3, 0x0

    .line 1193
    .local v3, "androidKeyAction":I
    if-nez p1, :cond_0

    .line 1195
    const/4 v3, 0x0

    .line 1206
    :goto_0
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getKnobBaseValue(I)I

    move-result v5

    .line 1207
    .local v5, "knobBase":I
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getKnobMvalue(I)I

    move-result v6

    .line 1208
    .local v6, "knobM":I
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getKnobNvalue(I)I

    move-result v7

    .line 1210
    .local v7, "knobN":I
    const-string v10, "TMSDevMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "handleKnobEvent base["

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] M[ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1211
    const-string v12, "] N["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1210
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;-><init>()V

    .line 1218
    .local v0, "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    invoke-static {v0, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getAndroidKeyEvent(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V

    .line 1219
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    .line 1221
    const-string v10, "TMSDevMngr"

    .line 1222
    const-string v11, "handleKnobEvent (andKey == AcsTmKeyConverter.AND_KEY_CODE_INVALID) "

    .line 1221
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1261
    :goto_1
    return-void

    .line 1199
    .end local v0    # "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .end local v5    # "knobBase":I
    .end local v6    # "knobM":I
    .end local v7    # "knobN":I
    :cond_0
    const/4 v3, 0x1

    goto :goto_0

    .line 1226
    .restart local v0    # "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .restart local v5    # "knobBase":I
    .restart local v6    # "knobM":I
    .restart local v7    # "knobN":I
    :cond_1
    iget v1, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 1228
    const-string v10, "TMSDevMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "handleKnobEvent mAndKeyCode = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v10, :cond_2

    .line 1242
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1244
    .local v8, "now":J
    const-string v10, "TMSDevMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "handleKnobEvent injecting now= "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    invoke-direct {p0, v3, v1, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v2

    .line 1250
    .local v2, "androidEvent":Landroid/view/KeyEvent;
    const/4 v10, 0x0

    invoke-direct {p0, v2, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1260
    .end local v2    # "androidEvent":Landroid/view/KeyEvent;
    .end local v8    # "now":J
    :cond_2
    :goto_2
    const-string v10, "TMSDevMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "handleKnobEvent mAndKeyCode = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1253
    :catch_0
    move-exception v4

    .line 1255
    .local v4, "e":Ljava/lang/Exception;
    const-string v10, "TMSDevMngr"

    const-string v11, " handleKnobEvent Exception"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private initCallAudio()V
    .locals 2

    .prologue
    .line 2850
    const-string v0, "TMSDevMngr"

    const-string v1, "initCallAudio: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2852
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    .line 2853
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceRxCallBack:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;

    .line 2855
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initPhoneStateListner()V

    .line 2857
    const-string v0, "TMSDevMngr"

    const-string v1, "initCallAudio: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2858
    return-void
.end method

.method private initGpsService()V
    .locals 3

    .prologue
    .line 3102
    const-string v1, "TMSDevMngr"

    const-string v2, "initGpsService: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3103
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->registerGpsListener()V

    .line 3104
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3105
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3106
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3108
    const-string v1, "TMSDevMngr"

    const-string v2, "initGpsService: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3109
    return-void
.end method

.method private initLocationService()V
    .locals 3

    .prologue
    .line 3409
    const-string v1, "TMSDevMngr"

    const-string v2, "initLocationService: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3411
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->registerGpsListener()V

    .line 3412
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3413
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x14

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3414
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3416
    const-string v1, "TMSDevMngr"

    const-string v2, "initLocationService: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3417
    return-void
.end method

.method private initPhoneStateListner()V
    .locals 2

    .prologue
    .line 2623
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2624
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2625
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2627
    return-void
.end method

.method private initPowerManagerService()V
    .locals 2

    .prologue
    .line 973
    const-string v0, "TMSDevMngr"

    const-string v1, "initPowerManagerService enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    if-nez v0, :cond_0

    .line 976
    const-string v0, "TMSDevMngr"

    const-string v1, "initPowerManagerService make power manager service"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    .line 978
    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 977
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    .line 981
    :cond_0
    return-void
.end method

.method private initUIModeManagerService()V
    .locals 2

    .prologue
    .line 987
    const-string v0, "TMSDevMngr"

    const-string v1, "initUIModeManagerService enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    if-nez v0, :cond_0

    .line 990
    const-string v0, "TMSDevMngr"

    .line 991
    const-string v1, "initUIModeManagerService maker uimode Manager service"

    .line 990
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    .line 993
    const-string v1, "uimode"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 992
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    .line 995
    :cond_0
    return-void
.end method

.method private injectEvent(Landroid/view/InputEvent;Z)V
    .locals 10
    .param p1, "event"    # Landroid/view/InputEvent;
    .param p2, "sync"    # Z

    .prologue
    .line 410
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mInputManager:Ljava/lang/Object;

    if-eqz v5, :cond_0

    .line 412
    const/4 v2, 0x0

    .line 414
    .local v2, "fieldMode":Ljava/lang/reflect/Field;
    if-nez p2, :cond_1

    .line 415
    const-class v5, Landroid/hardware/input/InputManager;

    const-string v6, "INJECT_INPUT_EVENT_MODE_ASYNC"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 422
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 424
    .local v4, "mode":I
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mInputManager:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "injectInputEvent"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/view/InputEvent;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 425
    .local v3, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 426
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mInputManager:Ljava/lang/Object;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    .end local v2    # "fieldMode":Ljava/lang/reflect/Field;
    .end local v3    # "m":Ljava/lang/reflect/Method;
    .end local v4    # "mode":I
    :cond_0
    :goto_1
    return-void

    .line 418
    .restart local v2    # "fieldMode":Ljava/lang/reflect/Field;
    :cond_1
    const-class v5, Landroid/hardware/input/InputManager;

    const-string v6, "INJECT_INPUT_EVENT_MODE_WAIT_FOR_RESULT"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    goto :goto_0

    .line 428
    .end local v2    # "fieldMode":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v5, "TMSDevMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " AcsDeviceMngr.injectEvent NoSuchMethodException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 430
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 431
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v5, "TMSDevMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " AcsDeviceMngr.injectEvent InvocationTargetException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 432
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 433
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v5, "TMSDevMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " AcsDeviceMngr.injectEvent NoSuchFieldException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 434
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v1

    .line 435
    .local v1, "e1":Ljava/lang/IllegalAccessException;
    const-string v5, "TMSDevMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " AcsDeviceMngr.injectEvent IllegalAccessException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 436
    .end local v1    # "e1":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v1

    .line 437
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v5, "TMSDevMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " AcsDeviceMngr.injectEvent IllegalArgumentException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private injectMotionEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "mv"    # Landroid/view/MotionEvent;

    .prologue
    .line 1934
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 1936
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1942
    :cond_0
    :goto_0
    return-void

    .line 1937
    :catch_0
    move-exception v0

    .line 1938
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "injectMotionEvent : Exception Occured "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V
    .locals 12
    .param p1, "nCount"    # I
    .param p2, "arrInput"    # [Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;
    .param p3, "action"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1965
    new-array v6, p1, [Landroid/view/MotionEvent$PointerProperties;

    .line 1966
    .local v6, "pointerProperties":[Landroid/view/MotionEvent$PointerProperties;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 1970
    new-array v5, p1, [Landroid/view/MotionEvent$PointerCoords;

    .line 1971
    .local v5, "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v1, 0x0

    :goto_1
    if-lt v1, p1, :cond_1

    .line 1989
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 1992
    .local v3, "now":J
    :try_start_0
    const-string v7, "android.view.MotionEvent"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const-string v8, "obtain"

    const/16 v9, 0xf

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    .line 1993
    sget-object v11, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    sget-object v11, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x3

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x4

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x5

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x6

    .line 1994
    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/4 v10, 0x7

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0x8

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0x9

    sget-object v11, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0xa

    sget-object v11, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0xb

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0xc

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0xd

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v10, 0xe

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    .line 1992
    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 1994
    const/4 v8, 0x0

    const/16 v9, 0xf

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 1995
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    .line 1996
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    .line 1997
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x4

    .line 1998
    aput-object v6, v9, v10

    const/4 v10, 0x5

    .line 1999
    aput-object v5, v9, v10

    const/4 v10, 0x6

    .line 2000
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x7

    .line 2001
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x8

    .line 2002
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    invoke-virtual {v11}, Landroid/view/Display;->getDisplayId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0x9

    .line 2003
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xa

    .line 2004
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xb

    .line 2005
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xc

    .line 2006
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xd

    .line 2007
    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/16 v10, 0xe

    .line 2008
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    .line 1994
    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1992
    check-cast v2, Landroid/view/MotionEvent;

    .line 2009
    .local v2, "mv":Landroid/view/MotionEvent;
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMotionEvent(Landroid/view/MotionEvent;)V

    .line 2011
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4

    .line 2024
    .end local v2    # "mv":Landroid/view/MotionEvent;
    :goto_2
    return-void

    .line 1967
    .end local v3    # "now":J
    .end local v5    # "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    :cond_0
    new-instance v7, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v7, v6, v1

    .line 1968
    aget-object v7, v6, v1

    iput v1, v7, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 1966
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1972
    .restart local v5    # "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    :cond_1
    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v5, v1

    .line 1973
    aget-object v7, v5, v1

    aget-object v8, p2, v1

    iget v8, v8, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->x:I

    int-to-float v8, v8

    iput v8, v7, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 1974
    aget-object v7, v5, v1

    aget-object v8, p2, v1

    iget v8, v8, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->y:I

    int-to-float v8, v8

    iput v8, v7, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 1976
    aget-object v7, p2, v1

    iget v7, v7, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->pressure:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_2

    aget-object v7, p2, v1

    iget v7, v7, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->pressure:I

    if-le v7, v9, :cond_3

    .line 1978
    :cond_2
    if-ne p3, v9, :cond_4

    .line 1980
    aget-object v7, v5, v1

    iput v10, v7, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 1971
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 1984
    :cond_4
    aget-object v7, v5, v1

    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, v7, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    goto :goto_3

    .line 2013
    .restart local v3    # "now":J
    :catch_0
    move-exception v0

    .line 2014
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v7, "TMSDevMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "injectMultiPointerEvent : Exception Occured "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2015
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2016
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v7, "TMSDevMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "injectMultiPointerEvent : Exception Occured "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2017
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2018
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v7, "TMSDevMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "injectMultiPointerEvent : Exception Occured "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2019
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_3
    move-exception v0

    .line 2020
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "TMSDevMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "injectMultiPointerEvent : Exception Occured "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2021
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 2022
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "TMSDevMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "injectMultiPointerEvent : Exception Occured "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private injectSinglePointerEvent(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V
    .locals 16
    .param p1, "arrInput"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;
    .param p2, "action"    # I

    .prologue
    .line 1945
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 1949
    .local v1, "now":J
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->x:I

    int-to-float v6, v3

    .line 1950
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->y:I

    int-to-float v7, v3

    .line 1951
    move-object/from16 v0, p1

    iget v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->pressure:I

    int-to-float v8, v3

    .line 1952
    const/4 v9, 0x0

    .line 1953
    const/4 v10, 0x0

    .line 1954
    const/4 v11, 0x0

    .line 1955
    const/4 v12, 0x0

    .line 1956
    const/4 v13, 0x0

    .line 1957
    const/4 v14, 0x0

    move-wide v3, v1

    move/from16 v5, p2

    .line 1946
    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v15

    .line 1958
    .local v15, "mv":Landroid/view/MotionEvent;
    const/4 v3, 0x2

    invoke-virtual {v15, v3}, Landroid/view/MotionEvent;->setSource(I)V

    .line 1959
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMotionEvent(Landroid/view/MotionEvent;)V

    .line 1960
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "injectSinglePointerEvent "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1961
    invoke-virtual {v15}, Landroid/view/MotionEvent;->recycle()V

    .line 1962
    return-void
.end method

.method private iskeyModifierPressed(I)Z
    .locals 4
    .param p1, "modifier"    # I

    .prologue
    .line 1109
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "iskeyModifierPressed : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1110
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1109
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1122
    const-string v1, "TMSDevMngr"

    const-string v2, "iskeyModifierPressed : false"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 1116
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 1118
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "iskeyModifierPressed : true"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    const/4 v1, 0x1

    goto :goto_1

    .line 1114
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private keyModifierPressed(I)V
    .locals 3
    .param p1, "modifier"    # I

    .prologue
    .line 1096
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "keyModifierPressed : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1101
    return-void
.end method

.method private keyModifierReleased(I)V
    .locals 4
    .param p1, "modifier"    # I

    .prologue
    .line 1134
    const-string v1, "TMSDevMngr"

    const-string v2, "Enter :keyModifierReleased "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1145
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "keyModifierReleased : modifier"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    :goto_1
    return-void

    .line 1138
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 1140
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "keyModifierReleased : true"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mKeyModifierList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 1136
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private final native native_AcsDeviceMngr_onEvent(IILjava/lang/String;)V
.end method

.method private final native native_AcsDvcMngr_aliveRequestGps()V
.end method

.method private final native native_AcsDvcMngr_cancelSubscribeGps()V
.end method

.method private final native native_AcsDvcMngr_cancelSubscribeLocation()V
.end method

.method private final native native_AcsDvcMngr_getCacertResp([BI[BI[BI)V
.end method

.method private final native native_AcsDvcMngr_getDevcertResp([BI)V
.end method

.method private final native native_AcsDvcMngr_getGeoLocation()V
.end method

.method private final native native_AcsDvcMngr_getNmeaDesc()V
.end method

.method private final native native_AcsDvcMngr_getNmeaGps()V
.end method

.method private final native native_AcsDvcMngr_sendFetchCertNegativeResp()V
.end method

.method private final native native_AcsDvcMngr_sendGpsNmeaParam(Ljava/lang/String;)V
.end method

.method private final native native_AcsDvcMngr_sendGpsNmeaTime(J)V
.end method

.method private final native native_AcsDvcMngr_sendUpnpComponentPublicKey([BI)V
.end method

.method private final native native_AcsDvcMngr_startClientGpsSource()V
.end method

.method private final native native_AcsDvcMngr_startClientLocationService()V
.end method

.method private final native native_AcsDvcMngr_stopClientGpsSource()V
.end method

.method private final native native_AcsDvcMngr_stopClientLocationService()V
.end method

.method private final native native_AcsDvcMngr_subscribeGps(II)V
.end method

.method private final native native_AcsDvcMngr_subscribeLocation(II)V
.end method

.method private final native native_call_audio_evt(I)I
.end method

.method private final native native_call_audio_write([BI)I
.end method

.method private onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 2923
    packed-switch p1, :pswitch_data_0

    .line 2944
    const-string v0, "TMSDevMngr"

    const-string v1, "onCallStateChanged:Unhandled"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2946
    :goto_0
    return-void

    .line 2927
    :pswitch_0
    const-string v0, "TMSDevMngr"

    const-string v1, "onCallStateChanged:IDLE"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2929
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_call_audio_evt(I)I

    goto :goto_0

    .line 2933
    :pswitch_1
    const-string v0, "TMSDevMngr"

    const-string v1, "onCallStateChanged:CALL_STATE_OFFHOOK"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2935
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_call_audio_evt(I)I

    goto :goto_0

    .line 2939
    :pswitch_2
    const-string v0, "TMSDevMngr"

    const-string v1, "onCallStateChanged:CALL_STATE_RINGING"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2940
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_call_audio_evt(I)I

    goto :goto_0

    .line 2923
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private registerGpsListener()V
    .locals 3

    .prologue
    .line 3073
    const-string v1, "TMSDevMngr"

    const-string v2, "registerGpsListener: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3075
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3076
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xf

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3077
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3079
    const-string v1, "TMSDevMngr"

    const-string v2, "registerGpsListener: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3080
    return-void
.end method

.method private resetLanscapeOrientation()V
    .locals 3

    .prologue
    .line 875
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.resetLanscapeOrientation - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    if-eqz v0, :cond_0

    .line 884
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    .line 886
    const-string v0, "TMSDevMngr"

    .line 887
    const-string v1, "AcsDeviceMngr.setLandscapeOrientation - Sending Intent EXTRA_DOCK_STATE_UNDOCKED"

    .line 885
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.resetLanscapeOrientation - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    return-void

    .line 891
    :cond_0
    const-string v0, "TMSDevMngr"

    .line 892
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.resetLanscapeOrientation - isLandscapeSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 893
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 892
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 891
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private resetPortriatOrientation()V
    .locals 3

    .prologue
    .line 941
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setPortraitOrientation - Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 942
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mAutoRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 941
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    if-eqz v0, :cond_0

    .line 946
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    .line 957
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setPortraitOrientation - Setting"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setPortraitOrientation - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    return-void

    .line 961
    :cond_0
    const-string v0, "TMSDevMngr"

    .line 962
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setPortraitOrientation - isPortraitSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 963
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 962
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 961
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setGpsLocationData(Ljava/lang/Object;)V
    .locals 2
    .param p1, "locationData"    # Ljava/lang/Object;

    .prologue
    .line 3520
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsLocationData: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3521
    if-nez p1, :cond_0

    .line 3523
    const-string v0, "TMSDevMngr"

    const-string v1, "locationData is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3526
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    if-eqz v0, :cond_1

    .line 3528
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "locationData":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->setLocation(Ljava/lang/String;)V

    .line 3530
    :cond_1
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsLocationData: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3531
    return-void
.end method

.method private setGpsNMEA(Ljava/lang/Object;J)V
    .locals 2
    .param p1, "nmea"    # Ljava/lang/Object;
    .param p2, "time"    # J

    .prologue
    .line 3173
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsNMEA: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3174
    if-nez p1, :cond_0

    .line 3176
    const-string v0, "TMSDevMngr"

    const-string v1, "nmea is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3186
    .end local p1    # "nmea":Ljava/lang/Object;
    :goto_0
    return-void

    .line 3180
    .restart local p1    # "nmea":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    if-eqz v0, :cond_1

    .line 3182
    const-string v0, "TMSDevMngr"

    const-string v1, "nmea and GpsService is not null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3183
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "nmea":Ljava/lang/Object;
    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->updateNmea(Ljava/lang/String;J)V

    .line 3185
    :cond_1
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsNMEA: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setGpsNMEADesc(I)V
    .locals 2
    .param p1, "nmeaDesc"    # I

    .prologue
    .line 3193
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsNMEADesc: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3195
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    if-eqz v0, :cond_0

    .line 3197
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->updateNmeaDesc(I)V

    .line 3199
    :cond_0
    const-string v0, "TMSDevMngr"

    const-string v1, "setGpsNMEADesc: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3200
    return-void
.end method

.method private setGpsSubscripInterval(I)V
    .locals 4
    .param p1, "time"    # I

    .prologue
    .line 3160
    const-string v1, "TMSDevMngr"

    const-string v2, "setGpsSubscripInterval: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3162
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 3163
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3165
    const-string v1, "TMSDevMngr"

    const-string v2, "setGpsSubscripInterval: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3166
    return-void
.end method

.method private setLandscapeOrientation(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 837
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setLandscapeOrientation - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    if-eqz v0, :cond_0

    .line 842
    const-string v0, "TMSDevMngr"

    .line 843
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setLandscapeOrientation - isLandscapeSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 844
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 843
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 842
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setLandscapeOrientation - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    return-void

    .line 849
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    if-eqz v0, :cond_1

    .line 851
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->resetPortriatOrientation()V

    .line 858
    :cond_1
    const-string v0, "TMSDevMngr"

    .line 859
    const-string v1, "AcsDeviceMngr.setLandscapeOrientation - Sending Intent EXTRA_DOCK_STATE_CAR"

    .line 858
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    goto :goto_0
.end method

.method private setLocationSinkSubscripInterval(I)V
    .locals 4
    .param p1, "time"    # I

    .prologue
    .line 3468
    const-string v1, "TMSDevMngr"

    const-string v2, "setGpsSubscripInterval: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3470
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    const/16 v2, 0x18

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 3471
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3473
    const-string v1, "TMSDevMngr"

    const-string v2, "setGpsSubscripInterval: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3474
    return-void
.end method

.method private setNightMode(I)V
    .locals 3
    .param p1, "night_mode_value"    # I

    .prologue
    const/4 v2, 0x1

    .line 588
    const-string v0, "TMSDevMngr"

    const-string v1, "setNightMode "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightMode:Z

    if-eqz v0, :cond_1

    .line 590
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNightModeValue:I

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 595
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    if-nez v0, :cond_2

    .line 596
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initUIModeManagerService()V

    .line 598
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    if-eqz v0, :cond_0

    .line 600
    if-ne p1, v2, :cond_3

    .line 602
    const-string v0, "TMSDevMngr"

    const-string v1, "UiModeManager now set night mode... "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    .line 605
    invoke-virtual {v0, v2}, Landroid/app/UiModeManager;->enableCarMode(I)V

    .line 606
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    .line 609
    :cond_3
    if-nez p1, :cond_0

    .line 611
    const-string v0, "TMSDevMngr"

    const-string v1, "UiModeManager now re-set night mode... "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mUIModeMngr:Landroid/app/UiModeManager;

    invoke-virtual {v0, v2}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0
.end method

.method private setPortraitOrientation(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 903
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setPortraitOrientation - Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 904
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mAutoRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 903
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    if-eqz v0, :cond_0

    .line 908
    const-string v0, "TMSDevMngr"

    .line 909
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setPortraitOrientation - isPortraitSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 910
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 909
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 908
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setLandscapeOrientation - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    return-void

    .line 914
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    .line 916
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    if-eqz v0, :cond_1

    .line 918
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->resetLanscapeOrientation()V

    .line 925
    :cond_1
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.setPortraitOrientation - Setting"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 926
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mAutoRotation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 925
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setScreenSaver(I)V
    .locals 3
    .param p1, "screen_saver_value"    # I

    .prologue
    .line 657
    const-string v0, "TMSDevMngr"

    const-string v1, "setScreenSaver "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    if-nez v0, :cond_0

    .line 660
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initPowerManagerService()V

    .line 662
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    if-eqz v0, :cond_1

    .line 664
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 666
    const-string v0, "TMSDevMngr"

    const-string v1, "PowerManager now  gotosleep... "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPowerMngr:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 677
    :cond_1
    :goto_0
    return-void

    .line 669
    :cond_2
    if-nez p1, :cond_1

    .line 671
    const-string v0, "TMSDevMngr"

    const-string v1, "PowerManager now wake up from sleep... "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    goto :goto_0
.end method

.method private startCallAudio()V
    .locals 2

    .prologue
    .line 2865
    const-string v0, "TMSDevMngr"

    const-string v1, "startCallAudio: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2867
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceRxCallBack:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->start(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;)Z

    .line 2869
    const-string v0, "TMSDevMngr"

    const-string v1, "startCallAudio: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2870
    return-void
.end method

.method private startClientLocationService()V
    .locals 2

    .prologue
    .line 3478
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.startClientLocationService() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3479
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_startClientLocationService()V

    .line 3480
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.startClientLocationService() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3482
    return-void
.end method

.method private stopCallAudio()V
    .locals 2

    .prologue
    .line 2877
    const-string v0, "TMSDevMngr"

    const-string v1, "stopCallAudio: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2879
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVoiceCallRx:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->stop()Z

    .line 2881
    const-string v0, "TMSDevMngr"

    const-string v1, "stopCallAudio: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    return-void
.end method

.method private unRegisterGpsListener()V
    .locals 3

    .prologue
    .line 3083
    const-string v1, "TMSDevMngr"

    const-string v2, "unRegisterGpsListener: Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3085
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3086
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x10

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3087
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3089
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    if-eqz v1, :cond_0

    .line 3090
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3091
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    .line 3094
    :cond_0
    const-string v1, "TMSDevMngr"

    const-string v2, "unRegisterGpsListener: Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3095
    return-void
.end method


# virtual methods
.method public aliveRequestGps()V
    .locals 2

    .prologue
    .line 3248
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.aliveRequestGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3249
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_aliveRequestGps()V

    .line 3250
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.aliveRequestGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3251
    return-void
.end method

.method public callNativeOnEvent(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2179
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.callNativeOnEvent() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2181
    iget v0, p1, Landroid/os/Message;->what:I

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDeviceMngr_onEvent(IILjava/lang/String;)V

    .line 2183
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.callNativeOnEvent() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2184
    return-void
.end method

.method public cancelSubcribeLocation()V
    .locals 2

    .prologue
    .line 3510
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getGeoLocation() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3511
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_cancelSubscribeLocation()V

    .line 3512
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getGeoLocation() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3513
    return-void
.end method

.method public cancelSubscribeGps()V
    .locals 2

    .prologue
    .line 3239
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.cancelSubscribeGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3240
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_cancelSubscribeGps()V

    .line 3241
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.cancelSubscribeGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3242
    return-void
.end method

.method public createVirtualDisplay(IILandroid/view/Surface;)V
    .locals 14
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "surface"    # Landroid/view/Surface;

    .prologue
    .line 196
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.createVirtualDisplay() - Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    const-string v3, "display"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    .line 201
    .local v1, "dm":Landroid/hardware/display/DisplayManager;
    const/4 v10, 0x0

    .line 203
    .local v10, "flagPublic":Ljava/lang/reflect/Field;
    const/4 v9, 0x0

    .line 205
    .local v9, "flagPresentation":Ljava/lang/reflect/Field;
    const/4 v11, 0x0

    .line 208
    .local v11, "flagSecure":Ljava/lang/reflect/Field;
    :try_start_0
    const-class v2, Landroid/hardware/display/DisplayManager;

    const-string v3, "VIRTUAL_DISPLAY_FLAG_PUBLIC"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    .line 209
    const-class v2, Landroid/hardware/display/DisplayManager;

    const-string v3, "VIRTUAL_DISPLAY_FLAG_PRESENTATION"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    .line 210
    const-class v2, Landroid/hardware/display/DisplayManager;

    const-string v3, "VIRTUAL_DISPLAY_FLAG_SECURE"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v11

    .line 212
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_8

    if-eqz v2, :cond_0

    .line 221
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 222
    .local v13, "release":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v13, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 232
    .end local v13    # "release":Ljava/lang/reflect/Method;
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    .line 233
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    .line 236
    :cond_0
    const-string v2, "MirrorLinkDisplay"

    .line 237
    const/16 v5, 0xa0

    .line 238
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 239
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    .line 238
    or-int/2addr v3, v4

    .line 239
    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    .line 238
    or-int v7, v3, v4

    move v3, p1

    move/from16 v4, p2

    move-object/from16 v6, p3

    .line 236
    invoke-virtual/range {v1 .. v7}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    .line 242
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    if-eqz v2, :cond_1

    .line 243
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.createVirtualDisplay() - success"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mSurface:Landroid/view/Surface;

    .line 245
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getDisplay"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    .line 246
    .local v12, "m":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v12, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/Display;

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_8

    .line 262
    .end local v12    # "m":Ljava/lang/reflect/Method;
    :goto_1
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.createVirtualDisplay() - Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    return-void

    .line 223
    :catch_0
    move-exception v8

    .line 224
    .local v8, "e":Ljava/lang/NoSuchMethodException;
    :try_start_3
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : NoSuchMethodException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_8

    goto :goto_0

    .line 250
    .end local v8    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v8

    .line 251
    .local v8, "e":Ljava/lang/NoSuchFieldException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVirtualDisplay : NoSuchFieldException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 225
    .end local v8    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v8

    .line 226
    .local v8, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_4
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : InvocationTargetException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_8

    goto/16 :goto_0

    .line 252
    .end local v8    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v8

    .line 253
    .local v8, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVirtualDisplay : NoSuchMethodException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 227
    .end local v8    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v8

    .line 228
    .local v8, "e":Ljava/lang/IllegalAccessException;
    :try_start_5
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : IllegalAccessException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_8

    goto/16 :goto_0

    .line 254
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v8

    .line 255
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVirtualDisplay : IllegalArgumentException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 229
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v8

    .line 230
    .restart local v8    # "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : IllegalArgumentException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_6} :catch_8

    goto/16 :goto_0

    .line 256
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :catch_7
    move-exception v8

    .line 257
    .local v8, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVirtualDisplay : IllegalAccessException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 248
    .end local v8    # "e":Ljava/lang/IllegalAccessException;
    :cond_1
    :try_start_7
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.createVirtualDisplay() - failed"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/NoSuchFieldException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_7 .. :try_end_7} :catch_8

    goto/16 :goto_1

    .line 258
    :catch_8
    move-exception v8

    .line 259
    .local v8, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVirtualDisplay : InvocationTargetException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public getAcsGpsService()Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    .locals 1

    .prologue
    .line 2611
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    return-object v0
.end method

.method public getAcsLocationSink()Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    .locals 1

    .prologue
    .line 2615
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    return-object v0
.end method

.method public getCACert()V
    .locals 3

    .prologue
    .line 3330
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.getCACert() "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;-><init>(Landroid/content/Context;)V

    .line 3332
    .local v0, "certProvider":Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;->getCaCertificate()V

    .line 3333
    return-void
.end method

.method public getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .locals 1

    .prologue
    .line 3377
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    return-object v0
.end method

.method public getDeviceCertficate([B)V
    .locals 5
    .param p1, "buf"    # [B

    .prologue
    .line 3336
    const-string v3, "TMSDevMngr"

    const-string v4, "AcsDeviceMngr.getDeviceCertficate()"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3337
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;-><init>(Landroid/content/Context;)V

    .line 3339
    .local v0, "certProvider":Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;
    :try_start_0
    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 3340
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/DapCertificateProvider;->getDeviceCertificate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3344
    .end local v2    # "value":Ljava/lang/String;
    :goto_0
    return-void

    .line 3341
    :catch_0
    move-exception v1

    .line 3342
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDeviceStatus(I)I
    .locals 3
    .param p1, "feature"    # I

    .prologue
    .line 538
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.getDeviceStatus - ENTER"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const/4 v0, -0x1

    .line 540
    .local v0, "feature_status":I
    sparse-switch p1, :sswitch_data_0

    .line 575
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.getDeviceStatus - Unknown feature"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :goto_0
    return v0

    .line 544
    :sswitch_0
    const-string v1, "TMSDevMngr"

    .line 545
    const-string v2, "AcsDeviceMngr.getDeviceStatus - call getScreenSaver"

    .line 544
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getScreenSaver()I

    move-result v0

    .line 548
    goto :goto_0

    .line 552
    :sswitch_1
    const-string v1, "TMSDevMngr"

    .line 553
    const-string v2, "AcsDeviceMngr.getDeviceStatus - call getNightMode"

    .line 552
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getNightMode()I

    move-result v0

    .line 556
    goto :goto_0

    .line 560
    :sswitch_2
    const-string v1, "TMSDevMngr"

    .line 561
    const-string v2, "AcsDeviceMngr.getDeviceStatus - call getOrientation"

    .line 560
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getOrientation()I

    move-result v0

    .line 564
    goto :goto_0

    .line 568
    :sswitch_3
    const-string v1, "TMSDevMngr"

    .line 569
    const-string v2, "AcsDeviceMngr.getDeviceStatus - call getRotation"

    .line 568
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getRotation()I

    move-result v0

    .line 572
    goto :goto_0

    .line 540
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x80 -> :sswitch_3
        0x100 -> :sswitch_2
    .end sparse-switch
.end method

.method public getDisplayInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 448
    const-string v3, "TMSDevMngr"

    const-string v4, "AcsDeviceMngr.getDisplayInfo() - Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 452
    const-string v3, "TMSDevMngr"

    const-string v4, "AcsDeviceMngr.getDisplayInfo() - mCxt is NULL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const/4 v3, 0x0

    .line 470
    :goto_0
    return-object v3

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    .line 461
    .local v0, "d":Landroid/view/Display;
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 462
    .local v2, "m":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 464
    new-instance v1, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 465
    .local v1, "dispInfo":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v3, "Width"

    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 466
    const-string v3, "Height"

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 467
    const-string v3, "PixelFormat"

    invoke-virtual {v0}, Landroid/view/Display;->getPixelFormat()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getPixelFormatString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v3, "TMSDevMngr"

    const-string v4, "AcsDeviceMngr.getDisplayInfo() - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getGeoLocation()V
    .locals 2

    .prologue
    .line 3501
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getGeoLocation() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3502
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_getGeoLocation()V

    .line 3503
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getGeoLocation() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3504
    return-void
.end method

.method public getNmeaDescription()V
    .locals 2

    .prologue
    .line 3266
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getNmeaGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3267
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_getNmeaDesc()V

    .line 3268
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getNmeaGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3269
    return-void
.end method

.method public getNmeaGps()V
    .locals 2

    .prologue
    .line 3257
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getNmeaGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3258
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_getNmeaGps()V

    .line 3259
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.getNmeaGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3260
    return-void
.end method

.method public getNonce()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3313
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.getNonce() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNonce:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3314
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNonce:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3315
    const-string v0, ""

    .line 3317
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNonce:Ljava/lang/String;

    goto :goto_0
.end method

.method public injectKeyEvent(II)V
    .locals 11
    .param p1, "action"    # I
    .param p2, "eventCode"    # I

    .prologue
    .line 1318
    :try_start_0
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "injectKeyEvent tmcode "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " action "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1319
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1318
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    const/high16 v8, 0x30000000

    if-gt v8, p2, :cond_1

    .line 1325
    const v8, 0x300000ff

    if-lt v8, p2, :cond_1

    .line 1327
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->handleKnobEvent(II)V

    .line 1537
    :cond_0
    :goto_0
    return-void

    .line 1331
    :cond_1
    const/4 v2, 0x0

    .line 1332
    .local v2, "androidKeyAction":I
    if-nez p1, :cond_2

    .line 1334
    const/4 v2, 0x0

    .line 1344
    :goto_1
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;-><init>()V

    .line 1370
    .local v0, "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    invoke-static {v0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getAndroidKeyEvent(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V

    .line 1371
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_3

    .line 1373
    const-string v8, "TMSDevMngr"

    .line 1374
    const-string v9, "injectKeyEvent (andKey == AcsTmKeyConverter.AND_KEY_CODE_INVALID) "

    .line 1373
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1532
    .end local v0    # "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .end local v2    # "androidKeyAction":I
    :catch_0
    move-exception v4

    .line 1534
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1535
    const-string v8, "TMSDevMngr"

    const-string v9, " injectKeyEvent Exception"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1338
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "androidKeyAction":I
    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    .line 1378
    .restart local v0    # "andKey":Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    :cond_3
    :try_start_1
    iget-boolean v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mCapsOn:Z

    .line 1380
    .local v3, "capsEnable":Z
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "injectKeyEvent andCode "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1381
    const-string v10, " capsEnable "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1380
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    iget-boolean v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->INJECT_METAKEY_EVENT_KITKAT:Z

    if-eqz v8, :cond_5

    if-nez v3, :cond_4

    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    const/16 v9, 0x3b

    if-ne v8, v9, :cond_5

    .line 1388
    :cond_4
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "injectKeyEvent capsEnable true"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v9, 0x1

    invoke-direct {p0, v2, v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v1

    .line 1403
    .local v1, "androidEvent":Landroid/view/KeyEvent;
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    if-eqz v8, :cond_0

    .line 1410
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 1411
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v8, "CTSTool"

    .line 1412
    const/4 v9, 0x0

    .line 1411
    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 1413
    .local v7, "test_Jambit":Z
    if-eqz v7, :cond_6

    iget-boolean v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mLineEndKeyPressed:Z

    if-eqz v8, :cond_6

    .line 1415
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Line End JAMBIT IS ENALED "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1416
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1415
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->handleJambitLineEnd(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;)V

    goto/16 :goto_0

    .line 1399
    .end local v1    # "androidEvent":Landroid/view/KeyEvent;
    .end local v6    # "sharedPreferences":Landroid/content/SharedPreferences;
    .end local v7    # "test_Jambit":Z
    :cond_5
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "injectKeyEvent capsEnable false "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    const/4 v9, 0x0

    invoke-direct {p0, v2, v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v1

    .restart local v1    # "androidEvent":Landroid/view/KeyEvent;
    goto :goto_2

    .line 1420
    .restart local v6    # "sharedPreferences":Landroid/content/SharedPreferences;
    .restart local v7    # "test_Jambit":Z
    :cond_6
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " mfakeModifierCode "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1421
    const-string v10, "keycode"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "androidKeyAction"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1422
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1420
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " capsEnable "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "keycode"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1424
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "androidKeyAction"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1425
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1423
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    iget-boolean v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->INJECT_METAKEY_EVENT_KITKAT:Z

    if-nez v8, :cond_7

    if-eqz v3, :cond_7

    .line 1431
    if-nez v2, :cond_7

    .line 1439
    const/16 v8, 0x3b

    const/4 v9, 0x0

    .line 1438
    invoke-direct {p0, v2, v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v5

    .line 1440
    .local v5, "fakeCaps":Landroid/view/KeyEvent;
    const/4 v8, 0x0

    invoke-direct {p0, v5, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1441
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    goto/16 :goto_0

    .line 1447
    .end local v5    # "fakeCaps":Landroid/view/KeyEvent;
    :cond_7
    iget-boolean v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->INJECT_METAKEY_EVENT_KITKAT:Z

    if-nez v8, :cond_8

    if-eqz v3, :cond_8

    .line 1448
    const/4 v8, 0x1

    if-ne v2, v8, :cond_8

    .line 1456
    const/16 v8, 0x3b

    const/4 v9, 0x0

    .line 1455
    invoke-direct {p0, v2, v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getKeyEventObj(III)Landroid/view/KeyEvent;

    move-result-object v5

    .line 1457
    .restart local v5    # "fakeCaps":Landroid/view/KeyEvent;
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 1458
    const/4 v8, 0x0

    invoke-direct {p0, v5, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    goto/16 :goto_0

    .line 1464
    .end local v5    # "fakeCaps":Landroid/view/KeyEvent;
    :cond_8
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_a

    .line 1467
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    invoke-direct {p0, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->iskeyModifierPressed(I)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1469
    const-string v8, "TMSDevMngr"

    .line 1470
    const-string v9, " injectKeyEvent ;modifier key is present in list"

    .line 1469
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1471
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    goto/16 :goto_0

    .line 1475
    :cond_9
    const-string v8, "TMSDevMngr"

    .line 1476
    const-string v9, "injectKeyEvent:modifier key is not present in list"

    .line 1475
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 1477
    invoke-direct {p0, v2, v8, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->handleFakeKeyEvent(IILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 1484
    :cond_a
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    if-eqz v8, :cond_d

    .line 1486
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " injectKeyEvent mMetaState "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1487
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1486
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    if-nez v2, :cond_c

    .line 1493
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-direct {p0, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->iskeyModifierPressed(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1495
    const-string v8, "TMSDevMngr"

    .line 1496
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " injectKeyEvent meta key is pressed and already present in the list "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1497
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1496
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1495
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    const-string v8, "TMSDevMngr"

    .line 1499
    const-string v9, " injectKeyEvent iskeyModifierPressed is true"

    .line 1498
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1504
    :cond_b
    const-string v8, "TMSDevMngr"

    .line 1505
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " injectKeyEvent meta key is pressed and not present in the list "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1506
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1505
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1504
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-direct {p0, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->keyModifierPressed(I)V

    .line 1508
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    goto/16 :goto_0

    .line 1515
    :cond_c
    const-string v8, "TMSDevMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " injectKeyEvent meta key is released "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1516
    iget v10, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1515
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517
    iget v8, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-direct {p0, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->keyModifierReleased(I)V

    .line 1518
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V

    goto/16 :goto_0

    .line 1527
    :cond_d
    const/4 v8, 0x0

    invoke-direct {p0, v1, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectEvent(Landroid/view/InputEvent;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public injectPointerEvent(IIII)V
    .locals 13
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "action"    # I
    .param p4, "pressure"    # I

    .prologue
    .line 1761
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    .line 1762
    .local v7, "now":J
    const/4 v1, 0x0

    .line 1763
    .local v1, "androidAction":I
    const/4 v2, 0x0

    .line 1768
    .local v2, "androidPressure":I
    const/4 v10, -0x1

    move/from16 v0, p3

    if-ne v0, v10, :cond_0

    .line 1770
    if-nez p4, :cond_3

    .line 1772
    const/16 p3, 0x1

    .line 1783
    :cond_0
    :goto_0
    const/4 v10, -0x1

    move/from16 v0, p4

    if-ne v0, v10, :cond_1

    .line 1785
    const/4 v10, 0x1

    move/from16 v0, p3

    if-ne v0, v10, :cond_4

    .line 1787
    const/16 p4, 0x0

    .line 1798
    :cond_1
    :goto_1
    const/4 v10, 0x1

    move/from16 v0, p3

    if-ne v0, v10, :cond_5

    .line 1800
    const/4 v1, 0x1

    .line 1807
    :goto_2
    if-nez p4, :cond_6

    .line 1809
    const/4 v2, 0x0

    .line 1820
    :goto_3
    if-nez v1, :cond_7

    .line 1821
    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventAction:I

    if-nez v10, :cond_7

    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventX:I

    if-ne v10, p1, :cond_2

    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventY:I

    if-eq v10, p2, :cond_7

    .line 1834
    :cond_2
    const-string v10, "TMSDevMngr"

    const-string v11, "injectPointerEvent This is Move only"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1835
    const/4 v1, 0x2

    .line 1856
    :goto_4
    iget-boolean v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->ACS_VNC_DISABLE_ORIENTATION:Z

    if-eqz v10, :cond_9

    .line 1858
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->checkRotation(II)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;

    move-result-object v9

    .line 1860
    .local v9, "rotatedCoordinates":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    const-string v10, "TMSDevMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "injectPointerEvent x "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v9, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mX:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1861
    const-string v12, " y "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v9, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mY:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1862
    const-string v12, " now "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " action "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " pressure "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1863
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1860
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1869
    iget v10, v9, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mX:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1870
    .local v5, "int_x":Ljava/lang/Integer;
    iget v10, v9, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mY:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1906
    .end local v9    # "rotatedCoordinates":Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
    .local v6, "int_y":Ljava/lang/Integer;
    :goto_5
    const/4 v10, 0x1

    new-array v3, v10, [Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;

    .line 1907
    .local v3, "arrInput":[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;
    const/4 v10, 0x0

    new-instance v11, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;

    invoke-direct {v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;-><init>()V

    aput-object v11, v3, v10

    .line 1908
    const/4 v10, 0x0

    aget-object v10, v3, v10

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iput v11, v10, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->x:I

    .line 1909
    const/4 v10, 0x0

    aget-object v10, v3, v10

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v11

    iput v11, v10, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->y:I

    .line 1910
    const/4 v10, 0x0

    aget-object v10, v3, v10

    iput v2, v10, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->pressure:I

    .line 1911
    const/4 v10, 0x0

    aget-object v10, v3, v10

    const/4 v11, 0x0

    iput v11, v10, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->eventId:I

    .line 1912
    const/4 v10, 0x1

    invoke-direct {p0, v10, v3, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 1921
    .end local v1    # "androidAction":I
    .end local v2    # "androidPressure":I
    .end local v3    # "arrInput":[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;
    .end local v5    # "int_x":Ljava/lang/Integer;
    .end local v6    # "int_y":Ljava/lang/Integer;
    .end local v7    # "now":J
    :goto_6
    return-void

    .line 1776
    .restart local v1    # "androidAction":I
    .restart local v2    # "androidPressure":I
    .restart local v7    # "now":J
    :cond_3
    const/16 p3, 0x0

    goto/16 :goto_0

    .line 1791
    :cond_4
    const/16 p4, 0xff

    goto/16 :goto_1

    .line 1804
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1814
    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_3

    .line 1838
    :cond_7
    if-nez v1, :cond_8

    .line 1839
    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventAction:I

    if-nez v10, :cond_8

    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventX:I

    if-ne v10, p1, :cond_8

    iget v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventY:I

    if-ne v10, p2, :cond_8

    .line 1841
    const-string v10, "TMSDevMngr"

    const-string v11, "injectPointerEvent This down  already send  refrain  from sending again samit"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 1917
    .end local v1    # "androidAction":I
    .end local v2    # "androidPressure":I
    .end local v7    # "now":J
    :catch_0
    move-exception v4

    .line 1919
    .local v4, "e":Ljava/lang/Exception;
    const-string v10, "TMSDevMngr"

    const-string v11, " injectPointerEvent Exception"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1846
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "androidAction":I
    .restart local v2    # "androidPressure":I
    .restart local v7    # "now":J
    :cond_8
    :try_start_1
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventAction:I

    .line 1847
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventX:I

    .line 1848
    iput p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevMotionEventY:I

    goto/16 :goto_4

    .line 1874
    :cond_9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1875
    .restart local v5    # "int_x":Ljava/lang/Integer;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    .restart local v6    # "int_y":Ljava/lang/Integer;
    goto :goto_5
.end method

.method public injectTouchEvent(I[Ljava/lang/Object;)V
    .locals 10
    .param p1, "nCount"    # I
    .param p2, "in"    # [Ljava/lang/Object;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    .line 2032
    const-string v4, "TMSDevMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injectTouchEvent : Enter Count"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 2035
    :cond_0
    const-string v4, "TMSDevMngr"

    const-string v5, "injectTouchEvent : Wrong Input"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    :goto_0
    return-void

    :cond_1
    move-object v1, p2

    .line 2039
    check-cast v1, [Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;

    .line 2042
    .local v1, "arrInput":[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;
    const/4 v0, 0x0

    .line 2043
    .local v0, "action":I
    const/4 v3, 0x0

    .line 2044
    .local v3, "upCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, p1, :cond_2

    .line 2054
    if-ne v0, v8, :cond_7

    .line 2056
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    if-ne v4, v7, :cond_6

    .line 2059
    const/4 v0, 0x2

    .line 2060
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2062
    if-lt p1, v7, :cond_5

    .line 2064
    const/4 v0, 0x6

    .line 2065
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2066
    if-ge v3, p1, :cond_4

    .line 2069
    const/4 v0, 0x2

    .line 2127
    :goto_2
    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    .line 2128
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEvent:I

    goto :goto_0

    .line 2045
    :cond_2
    aget-object v4, v1, v2

    iget v4, v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;->pressure:I

    if-nez v4, :cond_3

    .line 2046
    const/4 v0, 0x1

    .line 2047
    add-int/lit8 v3, v3, 0x1

    .line 2044
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2073
    :cond_4
    const/4 v0, 0x1

    .line 2074
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2075
    const/4 v0, 0x3

    .line 2077
    goto :goto_2

    .line 2081
    :cond_5
    const/4 v0, 0x1

    .line 2082
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2083
    const/4 v0, 0x3

    .line 2085
    goto :goto_2

    .line 2087
    :cond_6
    const-string v4, "TMSDevMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injectTouchEvent : Not Moving and Up Prev "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2093
    :cond_7
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEvent:I

    if-eq p1, v4, :cond_8

    .line 2095
    iput v9, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    .line 2098
    :cond_8
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    if-ne v4, v7, :cond_9

    .line 2099
    const/4 v0, 0x2

    .line 2100
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    goto :goto_2

    .line 2103
    :cond_9
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    if-ne v4, v9, :cond_b

    .line 2105
    if-lt p1, v7, :cond_a

    .line 2107
    const/4 v0, 0x0

    .line 2109
    invoke-direct {p0, v8, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2110
    const/16 v0, 0x105

    .line 2111
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2112
    const/4 v0, 0x2

    .line 2113
    goto :goto_2

    .line 2116
    :cond_a
    const/4 v0, 0x0

    .line 2118
    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->injectMultiPointerEvent(I[Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$TouchInputEvent;I)V

    .line 2119
    const/4 v0, 0x2

    .line 2122
    goto :goto_2

    .line 2123
    :cond_b
    const-string v4, "TMSDevMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "injectTouchEvent : Not Moving and Down Prev "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevTouchEventState:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public notifyContentRules(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 802
    return-void
.end method

.method public registerEvents(I)V
    .locals 4
    .param p1, "evt"    # I

    .prologue
    .line 1023
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.registerEvents() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    if-nez v1, :cond_0

    .line 1027
    const-string v1, "TMSDevMngr"

    .line 1028
    const-string v2, "AcsDeviceMngr.registerEvents() - mDvcEvtReceiver is null"

    .line 1027
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    new-instance v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvtHandler:Landroid/os/Handler;

    .line 1031
    new-instance v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvtHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    .line 1032
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1034
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "TMSDevMngr"

    .line 1035
    const-string v2, "AcsDeviceMngr.registerEvents() - ACS_SCREEN_SAVER_EVT"

    .line 1034
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1038
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1040
    const-string v1, "TMSDevMngr"

    .line 1041
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AcsDeviceMngr.registerEvents() - configuration event"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1040
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getOrientation()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I

    .line 1044
    const-string v1, "TMSDevMngr"

    .line 1045
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AcsDeviceMngr.registerEvents() - configuration event"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1044
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1051
    const-string v1, "TMSDevMngr"

    .line 1052
    const-string v2, "AcsDeviceMngr.registerEvents() - ACS_DEVICE_LOCK_EVT"

    .line 1051
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1056
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1059
    const-string v1, "com.samsung.intent.action.SEC_PRESENTATION_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1060
    const-string v1, "com.samsung.intent.action.SEC_PRESENTATION_STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1062
    const-string v1, "TMSDevMngr"

    .line 1063
    const-string v2, "AcsDeviceMngr.registerEvents() - registering Receiver"

    .line 1062
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1066
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    or-int/2addr v1, p1

    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    .line 1068
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.registerEvents() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    return-void
.end method

.method public releaseVirtualDisplay()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 267
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.releaseVirtualDisplay() - Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 279
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "release"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 280
    .local v1, "release":Ljava/lang/reflect/Method;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    .line 290
    .end local v1    # "release":Ljava/lang/reflect/Method;
    :goto_0
    iput-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mVirtualDisplay:Ljava/lang/Object;

    .line 291
    iput-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;

    .line 294
    :cond_0
    const-string v2, "TMSDevMngr"

    const-string v3, "AcsDeviceMngr.releaseVirtualDisplay() - Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : NoSuchMethodException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : InvocationTargetException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 285
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : IllegalAccessException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "releaseVirtualDisplay : IllegalArgumentException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetOrientation()V
    .locals 3

    .prologue
    .line 761
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.resetOrientation isLandscapeSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 762
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isPortraitSet "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 761
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isLandscapeSet:Z

    if-eqz v0, :cond_0

    .line 765
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->resetLanscapeOrientation()V

    .line 775
    :goto_0
    return-void

    .line 767
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->isPortraitSet:Z

    if-eqz v0, :cond_1

    .line 769
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->resetPortriatOrientation()V

    goto :goto_0

    .line 773
    :cond_1
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.resetOrientation"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public responseFromClient(III)V
    .locals 3
    .param p1, "objectId"    # I
    .param p2, "type"    # I
    .param p3, "result"    # I

    .prologue
    .line 3283
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsDeviceMngr.responseFromClient() - Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3285
    sparse-switch p1, :sswitch_data_0

    .line 3300
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "response not handled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3304
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.responseFromClient() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3305
    return-void

    .line 3287
    :sswitch_0
    const-string v0, "TMSDevMngr"

    const-string v1, "response for NMEA_OBJECT_UID"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3288
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->responseForGpsNmeaData(II)V

    goto :goto_0

    .line 3291
    :sswitch_1
    const-string v0, "TMSDevMngr"

    const-string v1, "response for NMEADESCRIPTION_OBJECT_UID"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3292
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->responseForGpsNmeaDesc(II)V

    goto :goto_0

    .line 3295
    :sswitch_2
    const-string v0, "TMSDevMngr"

    const-string v1, "response for LOCATION_OBJECT_UID"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3296
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->responseForLocation(II)V

    goto :goto_0

    .line 3285
    :sswitch_data_0
    .sparse-switch
        -0x62f74e63 -> :sswitch_1
        0xaac4540 -> :sswitch_0
        0x572a6461 -> :sswitch_2
    .end sparse-switch
.end method

.method public sendCallAudioData([BI)V
    .locals 3
    .param p1, "audioData"    # [B
    .param p2, "sizeInBytes"    # I

    .prologue
    .line 2909
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendCallAudioData: Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2911
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_call_audio_write([BI)I

    .line 2913
    const-string v0, "TMSDevMngr"

    const-string v1, "sendCallAudioData: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2914
    return-void
.end method

.method public sendFetchCertNegativeResp()V
    .locals 2

    .prologue
    .line 3363
    const-string v0, "TMSDevMngr"

    const-string v1, "sendNegativeFetchCertResponse: Enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3365
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_sendFetchCertNegativeResp()V

    .line 3367
    const-string v0, "TMSDevMngr"

    const-string v1, "sendNegativeFetchCertResponse: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3368
    return-void
.end method

.method public sendGetCACertResp([BI[BI[BI)V
    .locals 3
    .param p1, "caRootCert"    # [B
    .param p2, "caRootCertLen"    # I
    .param p3, "caCert"    # [B
    .param p4, "caCertLen"    # I
    .param p5, "nonce"    # [B
    .param p6, "nonceLen"    # I

    .prologue
    .line 3347
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendGetCACertResp: Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3349
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_getCacertResp([BI[BI[BI)V

    .line 3351
    const-string v0, "TMSDevMngr"

    const-string v1, "sendGetCACertResp: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3352
    return-void
.end method

.method public sendGetDevCertResponse([BI)V
    .locals 3
    .param p1, "Cert"    # [B
    .param p2, "CertLen"    # I

    .prologue
    .line 3355
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendDevCertificate: Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3357
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_getDevcertResp([BI)V

    .line 3359
    const-string v0, "TMSDevMngr"

    const-string v1, "sendDevCertificate: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3360
    return-void
.end method

.method public sendGpsData(Ljava/lang/String;J)V
    .locals 2
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 3206
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setGpsData() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3207
    iput-wide p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mTime:J

    .line 3208
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_sendGpsNmeaParam(Ljava/lang/String;)V

    .line 3209
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setGpsData() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3210
    return-void
.end method

.method public sendNonceData(Ljava/lang/Object;)V
    .locals 3
    .param p1, "buf"    # Ljava/lang/Object;

    .prologue
    .line 3308
    const-string v1, "TMSDevMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "AcsDeviceMngr.sendNonceData()"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3309
    check-cast p1, Ljava/lang/String;

    .end local p1    # "buf":Ljava/lang/Object;
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mNonce:Ljava/lang/String;

    .line 3310
    return-void
.end method

.method public sendUpnpComponentPublicKey([BI)V
    .locals 3
    .param p1, "pubKey"    # [B
    .param p2, "pubKeyLen"    # I

    .prologue
    .line 3322
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendUpnpComponentPublicKey: Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3324
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_sendUpnpComponentPublicKey([BI)V

    .line 3326
    const-string v0, "TMSDevMngr"

    const-string v1, "sendUpnpComponentPublicKey: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3327
    return-void
.end method

.method public setContext(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 17
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "l"    # Landroid/os/Looper;

    .prologue
    .line 319
    const-string v12, "TMSDevMngr"

    const-string v13, "AcsDeviceMngr.setContext() - Enter"

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    .line 322
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLooper:Landroid/os/Looper;

    .line 334
    :try_start_0
    const-string v12, "android.os.ServiceManager"

    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    .line 335
    .local v9, "serviceManagerClass":Ljava/lang/Class;
    const-string v12, "getService"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Class;

    const/4 v14, 0x0

    const-class v15, Ljava/lang/String;

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 337
    .local v5, "getService":Ljava/lang/reflect/Method;
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "window"

    aput-object v14, v12, v13

    invoke-virtual {v5, v9, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/IBinder;

    .line 338
    .local v11, "wmbinder":Landroid/os/IBinder;
    invoke-interface {v11}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    .line 339
    .local v10, "wmClass":Ljava/lang/Class;
    invoke-virtual {v10}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v12, v12, v13

    const-string v13, "asInterface"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Class;

    const/4 v15, 0x0

    const-class v16, Landroid/os/IBinder;

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mWindowManager:Ljava/lang/Object;

    .line 341
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "input"

    aput-object v14, v12, v13

    invoke-virtual {v5, v9, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/IBinder;

    .line 342
    .local v7, "imbinder":Landroid/os/IBinder;
    invoke-interface {v7}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 343
    .local v6, "imClass":Ljava/lang/Class;
    invoke-virtual {v6}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v12, v12, v13

    const-string v13, "asInterface"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Class;

    const/4 v15, 0x0

    const-class v16, Landroid/os/IBinder;

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-virtual {v12, v13, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mInputManager:Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5

    .line 359
    .end local v5    # "getService":Ljava/lang/reflect/Method;
    .end local v6    # "imClass":Ljava/lang/Class;
    .end local v7    # "imbinder":Landroid/os/IBinder;
    .end local v9    # "serviceManagerClass":Ljava/lang/Class;
    .end local v10    # "wmClass":Ljava/lang/Class;
    .end local v11    # "wmbinder":Landroid/os/IBinder;
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    .line 360
    const-string v13, "activity"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager;

    .line 359
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 361
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPkgMngr:Landroid/content/pm/PackageManager;

    .line 363
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    .line 364
    .local v8, "looper":Landroid/os/Looper;
    new-instance v12, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v12, v0, v8, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Looper;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    .line 366
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    const-string v13, "location"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;

    .line 378
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 379
    .local v4, "filter":Landroid/content/IntentFilter;
    const-string v12, "START_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 380
    const-string v12, "SUBSRCRIBE_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    const-string v12, "STOP_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 382
    const-string v12, "START_HU_LOCATION"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 383
    const-string v12, "STOP_HU_LOCATION"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 385
    const-string v12, "CTS_TESTS_SUBSCRIBE_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 386
    const-string v12, "CTS_TESTS_CANCEL_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 387
    const-string v12, "CTS_TESTS_ALIVE_REQ_HU_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 388
    const-string v12, "CTS_TESTS_GET_NMEA_GPS"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 389
    const-string v12, "CTS_TESTS_GET_GEO_LOCATION"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 390
    const-string v12, "CTS_TESTS_CANCEL_SUBSCRIBE_LOCATION"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 391
    const-string v12, "CTS_TESTS_SUBSCRIBE_HU_LOCATION"

    invoke-virtual {v4, v12}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    if-nez v12, :cond_0

    .line 394
    new-instance v12, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    .line 395
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsEventreceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;

    invoke-virtual {v12, v13, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 400
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "mock_location"

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 401
    new-instance v12, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;

    invoke-direct {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;-><init>(Landroid/location/LocationManager;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    .line 403
    new-instance v12, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;

    invoke-direct {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;-><init>(Landroid/location/LocationManager;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    .line 405
    const-string v12, "TMSDevMngr"

    const-string v13, "AcsDeviceMngr.setContext() - Exit"

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    return-void

    .line 346
    .end local v4    # "filter":Landroid/content/IntentFilter;
    .end local v8    # "looper":Landroid/os/Looper;
    :catch_0
    move-exception v2

    .line 347
    .local v2, "e":Landroid/os/RemoteException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : RemoteException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 348
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 349
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : NoSuchMethodException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 350
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v2

    .line 351
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : InvocationTargetException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 352
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v2

    .line 353
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : ClassNotFoundException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 354
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_4
    move-exception v3

    .line 355
    .local v3, "e1":Ljava/lang/IllegalAccessException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : IllegalAccessException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 356
    .end local v3    # "e1":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v3

    .line 357
    .local v3, "e1":Ljava/lang/IllegalArgumentException;
    const-string v12, "TMSDevMngr"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "setContext : IllegalArgumentException "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V
    .locals 0
    .param p1, "dataConnMngr"    # Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .prologue
    .line 3373
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 3374
    return-void
.end method

.method public setDeviceStatus(II)V
    .locals 2
    .param p1, "feature"    # I
    .param p2, "value"    # I

    .prologue
    .line 480
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setDeviceStatus - ENTER"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    sparse-switch p1, :sswitch_data_0

    .line 525
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setDeviceStatus - Unknown feature"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    :goto_0
    return-void

    .line 485
    :sswitch_0
    const-string v0, "TMSDevMngr"

    .line 486
    const-string v1, "AcsDeviceMngr.setDeviceStatus - call setScreenSaver"

    .line 485
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-direct {p0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setScreenSaver(I)V

    goto :goto_0

    .line 492
    :sswitch_1
    const-string v0, "TMSDevMngr"

    .line 493
    const-string v1, "AcsDeviceMngr.setDeviceStatus - call setNightMode"

    .line 492
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    invoke-direct {p0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setNightMode(I)V

    goto :goto_0

    .line 499
    :sswitch_2
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.setDeviceStatus - call rotation"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :sswitch_3
    const-string v0, "TMSDevMngr"

    .line 506
    const-string v1, "AcsDeviceMngr.setDeviceStatus - call orientation"

    .line 505
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    if-nez p2, :cond_0

    .line 509
    invoke-direct {p0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setLandscapeOrientation(I)V

    goto :goto_0

    .line 511
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 513
    invoke-direct {p0, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->setPortraitOrientation(I)V

    goto :goto_0

    .line 518
    :cond_1
    const-string v0, "TMSDevMngr"

    .line 519
    const-string v1, "AcsDeviceMngr.setDeviceStatus - Unknown orientation"

    .line 518
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 481
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x80 -> :sswitch_2
        0x100 -> :sswitch_3
    .end sparse-switch
.end method

.method public startClientGpsSource()V
    .locals 2

    .prologue
    .line 3216
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.startClientGpsSource() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3217
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_startClientGpsSource()V

    .line 3218
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.startClientGpsSource() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3219
    return-void
.end method

.method public startGpsService()V
    .locals 3

    .prologue
    .line 3115
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.startGpsService() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3117
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3118
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3119
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3121
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startClientGpsSource()V

    .line 3123
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.startGpsService() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3124
    return-void
.end method

.method public startLocationService()V
    .locals 3

    .prologue
    .line 3423
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.startLocationService() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3425
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3426
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x15

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3427
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3429
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startClientLocationService()V

    .line 3431
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.startLocationService() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3432
    return-void
.end method

.method public stopClientGpsSource()V
    .locals 2

    .prologue
    .line 3277
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.stopClientGpsSource() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3278
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_stopClientGpsSource()V

    .line 3279
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.stopClientGpsSource() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3280
    return-void
.end method

.method public stopClientLocationService()V
    .locals 2

    .prologue
    .line 3538
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.stopClientGpsSource() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3539
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_stopClientLocationService()V

    .line 3540
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.stopClientGpsSource() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3541
    return-void
.end method

.method public stopGpsService()V
    .locals 3

    .prologue
    .line 3130
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.stopGpsService() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3132
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3133
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0xc

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3134
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3136
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.stopGpsService() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3137
    return-void
.end method

.method public stopLocationService()V
    .locals 3

    .prologue
    .line 3438
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.stopLocationService() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3440
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 3441
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x16

    iput v1, v0, Landroid/os/Message;->what:I

    .line 3442
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 3444
    const-string v1, "TMSDevMngr"

    const-string v2, "AcsDeviceMngr.stopLocationService() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3445
    return-void
.end method

.method public subscribeGps(II)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "interval"    # I

    .prologue
    .line 3225
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.subscribeGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3230
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_subscribeGps(II)V

    .line 3231
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.subscribeGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3232
    return-void
.end method

.method public subscribeLocation(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "interval"    # I

    .prologue
    .line 3488
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.subscribeLocation() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3489
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    if-eqz v0, :cond_0

    .line 3491
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->setMinTime(J)V

    .line 3493
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->native_AcsDvcMngr_subscribeLocation(II)V

    .line 3494
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.subscribeLocation() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3495
    return-void
.end method

.method public unregisterEvents(I)V
    .locals 2
    .param p1, "evt"    # I

    .prologue
    .line 1076
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.unregisterEvents() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    .line 1079
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    if-eqz v0, :cond_0

    .line 1081
    const-string v0, "TMSDevMngr"

    .line 1082
    const-string v1, "AcsDeviceMngr.registerEvents() - unregistering Receiver"

    .line 1081
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1084
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDvcEvtReceiver:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;

    .line 1088
    :cond_0
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngr.unregisterEvents() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    return-void
.end method
