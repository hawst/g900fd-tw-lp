.class public Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;
.super Ljava/lang/Object;
.source "AppInfoParser.java"


# static fields
.field private static final CHARSET_UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final LOG_TAG:Ljava/lang/String; = "AcmsAppInfoParser"

.field private static final XML_TAG_APP_ID:Ljava/lang/String; = "appIdentifier"

.field private static final XML_TAG_BLACKLISTED_PLATFORM:Ljava/lang/String; = "blacklistedPlatformVersions"

.field private static final XML_TAG_ENTITY:Ljava/lang/String; = "entity"

.field private static final XML_TAG_NAME:Ljava/lang/String; = "name"

.field private static final XML_TAG_PLATFORM_ID:Ljava/lang/String; = "platformid"

.field private static final XML_TAG_RESTRICTED:Ljava/lang/String; = "restricted"

.field private static final XML_TAG_RUNTIME_ID:Ljava/lang/String; = "runtimeid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppInfoFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 6
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "getAppInfoFromCert() Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v2, 0x0

    .line 224
    .local v2, "xmlAppInfo":Ljava/lang/String;
    const-string v3, "1.3.6.1.4.1.41577.2.1"

    invoke-virtual {p0, v3}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v0

    .line 226
    .local v0, "appInfo":[B
    if-eqz v0, :cond_0

    .line 227
    new-instance v2, Ljava/lang/String;

    .end local v2    # "xmlAppInfo":Ljava/lang/String;
    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 228
    .restart local v2    # "xmlAppInfo":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 235
    .end local v1    # "i":I
    :cond_0
    :goto_1
    if-eqz v2, :cond_3

    .line 236
    const-string v3, "AcmsAppInfoParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getAppInfoFromCert() AppInfo is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :goto_2
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "getAppInfoFromCert() Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-object v2

    .line 229
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3c

    if-ne v3, v4, :cond_2

    .line 230
    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 231
    goto :goto_1

    .line 228
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 238
    .end local v1    # "i":I
    :cond_3
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "getAppInfoFromCert() AppInfo is: null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static getCertificateData(Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .locals 7
    .param p0, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide v5, 0x7fffffffffffffffL

    const/4 v4, 0x0

    .line 50
    new-instance p1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .end local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-direct {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    .line 52
    .restart local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getAppInfoFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "appInfoXmlString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 55
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "appInfo is not present in certificate"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/4 p1, 0x0

    .line 89
    .end local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :goto_0
    return-object p1

    .line 59
    .restart local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_0
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDataFromAppInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/utils/AppData;

    move-result-object v0

    .line 61
    .local v0, "appData":Lcom/samsung/android/mirrorlink/acms/utils/AppData;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getAppCertEntities()Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "entities":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1, v5, v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 66
    invoke-virtual {p1, v5, v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 67
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 68
    invoke-virtual {p1, v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 69
    invoke-virtual {p1, v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 70
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 71
    invoke-virtual {p1, v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 73
    if-nez v2, :cond_1

    .line 74
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "entities is null. Hence return"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getRestricted()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 77
    const-string v3, "DEVELOPER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 78
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_0

    .line 80
    :cond_2
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_0

    .line 83
    :cond_3
    const-string v3, "DEVELOPER"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 84
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_0

    .line 86
    :cond_4
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_0
.end method

.method public static getClientIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 7
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    const-string v4, "AcmsAppInfoParser"

    const-string v5, "getClientIdFromCert() Enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const/4 v1, 0x0

    .line 308
    .local v1, "clientId":Ljava/lang/String;
    const-string v4, "1.3.6.1.4.1.41577.3.3"

    invoke-virtual {p0, v4}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v0

    .line 309
    .local v0, "clId":[B
    if-eqz v0, :cond_0

    const/4 v4, 0x0

    aget-byte v4, v0, v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 310
    array-length v4, v0

    add-int/lit8 v4, v4, -0x2

    new-array v3, v4, [B

    .line 311
    .local v3, "id":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v3

    if-lt v2, v4, :cond_1

    .line 314
    new-instance v1, Ljava/lang/String;

    .end local v1    # "clientId":Ljava/lang/String;
    const-string v4, "UTF-8"

    invoke-direct {v1, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 317
    .end local v2    # "i":I
    .end local v3    # "id":[B
    .restart local v1    # "clientId":Ljava/lang/String;
    :cond_0
    const-string v4, "AcmsAppInfoParser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getClientIdFromCert() ClientId is: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v4, "AcmsAppInfoParser"

    const-string v5, "getClientIdFromCert() Exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-object v1

    .line 312
    .restart local v2    # "i":I
    .restart local v3    # "id":[B
    :cond_1
    add-int/lit8 v4, v2, 0x2

    aget-byte v4, v0, v4

    aput-byte v4, v3, v2

    .line 311
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getDataFromAppInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/utils/AppData;
    .locals 13
    .param p0, "appInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 129
    if-nez p0, :cond_1

    .line 130
    const-string v9, "AcmsAppInfoParser"

    const-string v10, "Input appInfoxml is null. So return with null"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v0, 0x0

    .line 192
    :cond_0
    :goto_0
    return-object v0

    .line 133
    :cond_1
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 134
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4, v12}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 135
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v8

    .line 136
    .local v8, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v9, Ljava/io/StringReader;

    invoke-direct {v9, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 138
    const/4 v2, 0x0

    .line 139
    .local v2, "entity":Z
    const-string v7, ""

    .line 140
    .local v7, "text":Ljava/lang/String;
    const-string v1, ""

    .line 141
    .local v1, "entities":Ljava/lang/String;
    const-string v5, ""

    .line 142
    .local v5, "restricted":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;-><init>()V

    .line 143
    .local v0, "appData":Lcom/samsung/android/mirrorlink/acms/utils/AppData;
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 144
    .local v3, "eventType":I
    :goto_1
    if-ne v3, v12, :cond_3

    .line 183
    const-string v9, ""

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 185
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    .line 184
    invoke-virtual {v5, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setRestricted(Ljava/lang/String;)V

    .line 187
    :cond_2
    const-string v9, ""

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 189
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    .line 188
    invoke-virtual {v1, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setAppCertEntities(Ljava/lang/String;)V

    goto :goto_0

    .line 146
    :cond_3
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 147
    .local v6, "tagname":Ljava/lang/String;
    packed-switch v3, :pswitch_data_0

    .line 181
    :cond_4
    :goto_2
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_1

    .line 149
    :pswitch_0
    const-string v9, "entity"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 150
    const/4 v2, 0x1

    .line 152
    goto :goto_2

    .line 155
    :pswitch_1
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    .line 156
    goto :goto_2

    .line 159
    :pswitch_2
    const-string v9, "appIdentifier"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 160
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setAppId(Ljava/lang/String;)V

    goto :goto_2

    .line 161
    :cond_5
    const-string v9, "platformid"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 162
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setPlatformId(Ljava/lang/String;)V

    goto :goto_2

    .line 163
    :cond_6
    const-string v9, "runtimeid"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 164
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setRunTimeId(Ljava/lang/String;)V

    goto :goto_2

    .line 166
    :cond_7
    const-string v9, "blacklistedPlatformVersions"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 167
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->setBlackListedPlatform(Ljava/lang/String;)V

    goto :goto_2

    .line 168
    :cond_8
    const-string v9, "restricted"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 169
    if-eqz v7, :cond_9

    const-string v9, ""

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 170
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 171
    goto :goto_2

    :cond_9
    const-string v9, "name"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    if-eqz v2, :cond_a

    .line 172
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 173
    goto/16 :goto_2

    :cond_a
    const-string v9, "entity"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 174
    const/4 v2, 0x0

    .line 176
    goto/16 :goto_2

    .line 147
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 6
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "getDevIdFromCert() Enter "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/4 v0, 0x0

    .line 287
    .local v0, "devId":Ljava/lang/String;
    const-string v3, "1.3.6.1.4.1.41577.3.1"

    invoke-virtual {p0, v3}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v1

    .line 288
    .local v1, "developerId":[B
    if-eqz v1, :cond_0

    .line 289
    new-instance v0, Ljava/lang/String;

    .end local v0    # "devId":Ljava/lang/String;
    const-string v3, "UTF-8"

    invoke-direct {v0, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 290
    .restart local v0    # "devId":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 297
    :goto_1
    const-string v3, "AcmsAppInfoParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Inside AppInfoParser Dev Id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    .end local v2    # "i":I
    :cond_0
    const-string v3, "AcmsAppInfoParser"

    const-string v4, "getDevIdFromCert() Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    return-object v0

    .line 291
    .restart local v2    # "i":I
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2f

    if-le v3, v4, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3a

    if-lt v3, v4, :cond_3

    .line 292
    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x60

    if-le v3, v4, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x7b

    if-ge v3, v4, :cond_4

    .line 293
    :cond_3
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 294
    goto :goto_1

    .line 290
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getServerIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 7
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    const-string v4, "AcmsAppInfoParser"

    const-string v5, "getServerIdFromCert() Enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const/4 v3, 0x0

    .line 327
    .local v3, "serverId":Ljava/lang/String;
    const-string v4, "1.3.6.1.4.1.41577.3.2"

    invoke-virtual {p0, v4}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v2

    .line 328
    .local v2, "sId":[B
    if-eqz v2, :cond_0

    const/4 v4, 0x0

    aget-byte v4, v2, v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 329
    array-length v4, v2

    add-int/lit8 v4, v4, -0x2

    new-array v1, v4, [B

    .line 330
    .local v1, "id":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-lt v0, v4, :cond_1

    .line 333
    new-instance v3, Ljava/lang/String;

    .end local v3    # "serverId":Ljava/lang/String;
    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 335
    .end local v0    # "i":I
    .end local v1    # "id":[B
    .restart local v3    # "serverId":Ljava/lang/String;
    :cond_0
    const-string v4, "AcmsAppInfoParser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getServerIdFromCert Server Id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v4, "AcmsAppInfoParser"

    const-string v5, "getServerIdFromCert() Exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    return-object v3

    .line 331
    .restart local v0    # "i":I
    .restart local v1    # "id":[B
    :cond_1
    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v2, v4

    aput-byte v4, v1, v0

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static updateGetCertificateData(Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .locals 6
    .param p0, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    const-string v3, "AcmsAppInfoParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updateGetCertificateData appEntry:pkgName : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 95
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getAppInfoFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "appInfoXmlString":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 99
    const-string v3, "AcmsAppInfoParser"

    .line 100
    const-string v4, "AppInfoXml from Certificate is null: Hence Ignore"

    .line 99
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 p1, 0x0

    .line 123
    .end local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_0
    :goto_0
    return-object p1

    .line 103
    .restart local p1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_1
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDataFromAppInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/utils/AppData;

    move-result-object v0

    .line 105
    .local v0, "appData":Lcom/samsung/android/mirrorlink/acms/utils/AppData;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p1, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getAppCertEntities()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "entities":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getRestricted()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 110
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 115
    :goto_1
    if-eqz v2, :cond_0

    .line 116
    const-string v3, "CCC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 117
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    goto :goto_0

    .line 112
    :cond_2
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    goto :goto_1

    .line 119
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    goto :goto_0
.end method
