.class public final enum Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;
.super Ljava/lang/Enum;
.source "ITMServerListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

.field public static final enum SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

.field public static final enum SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    const-string v1, "SERVICE_STARTED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .line 6
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    const-string v1, "SERVICE_STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .line 4
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
