.class Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;
.super Landroid/os/Handler;
.source "CommonAPIService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NativeEvtHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)V
    .locals 2

    .prologue
    .line 497
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 498
    const-string v0, "TMSCommonAPIService"

    const-string v1, "NativeEvtHandler.NativeEvtHandler - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    const-string v0, "TMSCommonAPIService"

    const-string v1, "NativeEvtHandler.NativeEvtHandler - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 504
    const-string v8, "TMSCommonAPIService"

    .line 505
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NativeEvtHandler.handleMessage - Enter - msg.what:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 506
    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 505
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 504
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    .line 651
    :pswitch_0
    const-string v8, "TMSCommonAPIService"

    .line 652
    const-string v9, "NativeEvtHandler.handleMessage - Unknown message type"

    .line 651
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    :cond_0
    :goto_0
    const-string v8, "TMSCommonAPIService"

    const-string v9, "NativeEvtHandler.handleMessage - Exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    return-void

    .line 509
    :pswitch_1
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_1

    .line 510
    const-string v8, "TMSCommonAPIService"

    .line 511
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_CLIENT_DISPLAY_CONFIG_RECV"

    .line 510
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getDisplayConfig()Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v1

    .line 524
    .local v1, "dispConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 525
    if-eqz v1, :cond_3

    .line 526
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getDisplaySurface()Landroid/view/Surface;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 527
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v8

    .line 528
    iget v9, v1, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sHoriRes:I

    iget v10, v1, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sVertRes:I

    .line 529
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getDisplaySurface()Landroid/view/Surface;

    move-result-object v11

    .line 527
    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->createVirtualDisplay(IILandroid/view/Surface;)V

    goto :goto_0

    .line 513
    .end local v1    # "dispConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 514
    .local v0, "d":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 515
    const-string v8, "TMSCommonAPIService"

    .line 516
    const-string v9, "NativeEvtHandler.handleMessage Error data"

    .line 515
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_2
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_CLIENT_DISPLAY_CONFIG_RECV:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeClientDisplayConfig(Ljava/lang/String;)V

    goto :goto_1

    .line 533
    .end local v0    # "d":Ljava/lang/String;
    .restart local v1    # "dispConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v8

    .line 534
    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->releaseVirtualDisplay()V

    goto/16 :goto_0

    .line 541
    .end local v1    # "dispConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    :pswitch_2
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_4

    .line 542
    const-string v8, "TMSCommonAPIService"

    .line 543
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_CLIENT_EVENT_CONFIG"

    .line 542
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 545
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 546
    .restart local v0    # "d":Ljava/lang/String;
    if-nez v0, :cond_5

    .line 547
    const-string v8, "TMSCommonAPIService"

    .line 548
    const-string v9, "NativeEvtHandler.handleMessage Error data"

    .line 547
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_5
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_CLIENT_EVENT_CONFIG:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativetEventConfig(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 556
    .end local v0    # "d":Ljava/lang/String;
    :pswitch_3
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_6

    .line 557
    const-string v8, "TMSCommonAPIService"

    .line 558
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_SERVER_SCALING_CONFIG"

    .line 557
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 560
    :cond_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 561
    .restart local v0    # "d":Ljava/lang/String;
    if-nez v0, :cond_7

    .line 562
    const-string v8, "TMSCommonAPIService"

    .line 563
    const-string v9, "NativeEvtHandler.handleMessage Error data"

    .line 562
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_7
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_SERVER_SCALING_CONFIG:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeServerScalingConfig(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 571
    .end local v0    # "d":Ljava/lang/String;
    :pswitch_4
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_8

    .line 572
    const-string v8, "TMSCommonAPIService"

    .line 573
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_CLIENT_PIXEL_FORMAT"

    .line 572
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 575
    :cond_8
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 576
    .local v4, "format":Ljava/lang/String;
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_CLIENT_PIXEL_FORMAT:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeClientPixelFormat(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 582
    .end local v4    # "format":Ljava/lang/String;
    :pswitch_5
    iget v4, p1, Landroid/os/Message;->arg1:I

    .line 583
    .local v4, "format":I
    const-string v8, "TMSCommonAPIService"

    .line 584
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NativeEvtHandler.handleMessage REMOTE_DISPLAY_CONN: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 585
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 584
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 583
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeRemoteDisplayConnection(I)V

    goto/16 :goto_0

    .line 590
    .end local v4    # "format":I
    :pswitch_6
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 591
    .local v2, "enabled":I
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v8, v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->convertEnabledToBoolean(I)Z

    move-result v6

    .line 592
    .local v6, "ret":Z
    const-string v8, "TMSCommonAPIService"

    .line 593
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NativeEvtHandler.handleMessage VNC_DRIVE_MODE: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 592
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeDriveMode(Z)V

    goto/16 :goto_0

    .line 598
    .end local v2    # "enabled":I
    .end local v6    # "ret":Z
    :pswitch_7
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 599
    .restart local v2    # "enabled":I
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v8, v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->convertEnabledToBoolean(I)Z

    move-result v6

    .line 600
    .restart local v6    # "ret":Z
    const-string v8, "TMSCommonAPIService"

    .line 601
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NativeEvtHandler.handleMessage VNC_NIGHT_MODE: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 600
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeNightMode(Z)V

    goto/16 :goto_0

    .line 606
    .end local v2    # "enabled":I
    .end local v6    # "ret":Z
    :pswitch_8
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_9

    .line 607
    const-string v8, "TMSCommonAPIService"

    .line 608
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_OPEN_MIC"

    .line 607
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 610
    :cond_9
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 611
    .local v4, "format":Ljava/lang/String;
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NativeEvtHandler.VNC_OPEN_MIC value:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 612
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 611
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeMicOpen(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 619
    .end local v4    # "format":Ljava/lang/String;
    :pswitch_9
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_a

    .line 620
    const-string v8, "TMSCommonAPIService"

    .line 621
    const-string v9, "NativeEvtHandler.handleMessage Error data in VNC_EVENT_MAPPING"

    .line 620
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 623
    :cond_a
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 624
    .local v3, "eventmap":Ljava/lang/String;
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_EVENT_MAPPING:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setNativeEventMapping(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 632
    .end local v3    # "eventmap":Ljava/lang/String;
    :pswitch_a
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Landroid/view/Surface;

    .line 633
    .local v7, "surface":Landroid/view/Surface;
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VNC_DISP_SURFACE:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setDisplaySurface(Landroid/view/Surface;)V

    goto/16 :goto_0

    .line 639
    .end local v7    # "surface":Landroid/view/Surface;
    :pswitch_b
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v8, :cond_b

    .line 640
    const-string v8, "TMSCommonAPIService"

    .line 641
    const-string v9, "NativeEvtHandler.handleMessage Error data in EVENT_ML_VERSION"

    .line 640
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 643
    :cond_b
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 644
    .local v5, "mlVersion":Ljava/lang/String;
    const-string v8, "TMSCommonAPIService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "EVENT_ML_VERSION:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->cbMLVersion(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_5
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
