.class public Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
.super Ljava/lang/Object;
.source "AcmsCertificateMngr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
    }
.end annotation


# static fields
.field private static final DAY_IN_HOURS:J = 0x18L

.field private static final MAX_RETRY_HOUR:I = 0xfc0

.field private static RETRY_COUNT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AcmsCertificateMngr"

.field private static sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;


# instance fields
.field private mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

.field private mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

.field private mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

.field private mContext:Landroid/content/Context;

.field private mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

.field private mMlAwareHandler:Landroid/os/Handler;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;-><init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mMlAwareHandler:Landroid/os/Handler;

    .line 89
    const-string v0, "AcmsCertificateMngr"

    const-string v1, "AcmsCertificateMngr"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 92
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    invoke-direct {v0, p1}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    .line 93
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;-><init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    .line 94
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 95
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 98
    invoke-static {p1}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getKeyStoreHelper(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getKeyStoreForApplication()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "AcmsCertificateMngr"

    const-string v1, "getKeyStoreForApplication success "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "AcmsCertificateMngr"

    const-string v1, "getKeyStoreForApplication fail"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    return-object v0
.end method

.method static synthetic access$5()I
    .locals 1

    .prologue
    .line 75
    sget v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I

    return v0
.end method

.method static synthetic access$6(I)V
    .locals 0

    .prologue
    .line 75
    sput p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I

    return-void
.end method

.method public static declared-synchronized getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    const-class v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    .line 287
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addToKeyStore(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "certChain"    # [Ljava/security/cert/X509Certificate;

    .prologue
    .line 575
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    monitor-enter v2

    .line 576
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 577
    .local v0, "certs":[Ljava/security/cert/X509Certificate;
    if-eqz v0, :cond_0

    .line 578
    const-string v1, "AcmsCertificateMngr"

    .line 579
    const-string v3, "Already entry for the same AppId hence deleting"

    .line 578
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->deleteFromKeyStore(Ljava/lang/String;)Z

    .line 582
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->storeCertChain(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z

    move-result v1

    monitor-exit v2

    return v1

    .line 575
    .end local v0    # "certs":[Ljava/security/cert/X509Certificate;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public cleanUp()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 697
    const-string v0, "AcmsCertificateMngr"

    const-string v1, "AcmsCertificateMngr: inside cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->cleanUp()V

    .line 700
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    if-eqz v0, :cond_1

    .line 703
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->cleanUp()V

    .line 704
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    if-eqz v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->cleanup()V

    .line 708
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 710
    :cond_2
    const-class v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    monitor-enter v1

    .line 711
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->sAcmsCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    .line 710
    monitor-exit v1

    .line 713
    return-void

    .line 710
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deleteFromKeyStore(Ljava/lang/String;)Z
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 595
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    monitor-enter v1

    .line 596
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->deleteCertChain(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 595
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCertificateFromInputStream(Ljava/io/InputStream;)Ljava/security/cert/X509Certificate;
    .locals 6
    .param p1, "ipStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 603
    const-string v3, "AcmsCertificateMngr"

    .line 604
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getCertificateFromInputStream() Enter "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 603
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    new-instance v2, Lorg/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v2, p1}, Lorg/bouncycastle/asn1/ASN1InputStream;-><init>(Ljava/io/InputStream;)V

    .line 607
    .local v2, "in":Lorg/bouncycastle/asn1/ASN1InputStream;
    const/4 v1, 0x0

    .line 608
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    const/4 v0, 0x0

    .line 610
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    const-string v3, "X.509"

    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 611
    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 612
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    const-string v3, "AcmsCertificateMngr"

    const-string v4, "getCertificateFromInputStream() Exit "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    return-object v0
.end method

.method public getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 588
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    monitor-enter v1

    .line 589
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getCertChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 588
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleAppAdded(Ljava/lang/String;)V
    .locals 24
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 396
    const-string v21, "AcmsCertificateMngr"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "handleAppAdded() Enter "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const/4 v14, 0x0

    .line 399
    .local v14, "file":Ljava/lang/String;
    const/4 v6, 0x0

    .line 400
    .local v6, "appID":Ljava/lang/String;
    const/4 v15, 0x0

    .line 401
    .local v15, "in":Ljava/io/InputStream;
    if-nez p1, :cond_1

    .line 402
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "pkgName is NULL"

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    :try_start_0
    const-string v21, "AcmsCertificateMngr"

    .line 410
    const-string v22, "Calculating app id from pkg name, file name and version name"

    .line 409
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v20, v0

    .line 413
    .local v20, "version":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    .line 414
    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .line 413
    :cond_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_3

    .line 422
    :goto_1
    if-nez v14, :cond_5

    .line 423
    const-string v21, "AcmsCertificateMngr"

    .line 424
    const-string v22, "file is null: Can not calculate app Id, hence returning"

    .line 423
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_10
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_1
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 564
    :catch_0
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 414
    .end local v12    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    .line 415
    .local v4, "app":Landroid/content/pm/ApplicationInfo;
    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 416
    iget-object v14, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 417
    const-string v21, "AcmsCertificateMngr"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Package name "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 418
    const-string v23, ", file name "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " VersionCode "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 417
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_10
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 552
    .end local v4    # "app":Landroid/content/pm/ApplicationInfo;
    .end local v20    # "version":I
    :catch_1
    move-exception v12

    .line 553
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    invoke-virtual {v12}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 561
    if-eqz v15, :cond_4

    .line 563
    :try_start_4
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b

    .line 570
    .end local v12    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    :goto_2
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "handleAppAdded() Exit "

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 427
    .restart local v20    # "version":I
    :cond_5
    :try_start_5
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_10
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 430
    .local v13, "f":Ljava/io/File;
    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v21, v0

    :try_start_6
    move-object/from16 v0, p1

    move-wide/from16 v1, v21

    invoke-static {v0, v1, v2, v13}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator;->calculateAppId(Ljava/lang/String;JLjava/io/File;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_10
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v6

    .line 441
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v17

    .line 442
    .local v17, "r":Landroid/content/res/Resources;
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    .line 443
    .local v3, "am":Landroid/content/res/AssetManager;
    sget-object v21, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->SELF_SIGNED_CERT_FILENAME:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v15

    .line 446
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getCertificateFromInputStream(Ljava/io/InputStream;)Ljava/security/cert/X509Certificate;

    move-result-object v18

    .line 447
    .local v18, "selfSignCert":Ljava/security/cert/X509Certificate;
    if-eqz v15, :cond_6

    .line 448
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V

    .line 452
    :cond_6
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 453
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->finishPreloading()V

    .line 455
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->printCert(Ljava/security/cert/X509Certificate;)V

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-object/from16 v21, v0

    .line 464
    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryFromPackage(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v5

    .line 466
    .local v5, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v5, :cond_7

    .line 467
    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getCertificateData(Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v5

    .line 470
    :cond_7
    if-nez v5, :cond_8

    .line 471
    const-string v21, "AcmsCertificateMngr"

    .line 472
    const-string v22, "handleAppAdded(): null appEntry returned form getCertificateData"

    .line 471
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_7 .. :try_end_7} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_10
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_8
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    .line 564
    :catch_2
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 431
    .end local v3    # "am":Landroid/content/res/AssetManager;
    .end local v5    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v12    # "e":Ljava/io/IOException;
    .end local v17    # "r":Landroid/content/res/Resources;
    .end local v18    # "selfSignCert":Ljava/security/cert/X509Certificate;
    :catch_3
    move-exception v12

    .line 432
    .local v12, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_9
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "Exception caught: can not calculate app id"

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-virtual {v12}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_9
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_9 .. :try_end_9} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_9} :catch_10
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_a
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto/16 :goto_0

    .line 564
    :catch_4
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 435
    .end local v12    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v12

    .line 436
    .local v12, "e":Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;
    :try_start_b
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "Exception caught: can not calculate app id"

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-virtual {v12}, Lcom/samsung/android/mirrorlink/acms/utils/AppIdGenerator$NotSignedException;->printStackTrace()V
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_b .. :try_end_b} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b .. :try_end_b} :catch_10
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_c
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    goto/16 :goto_0

    .line 564
    :catch_6
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 475
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v3    # "am":Landroid/content/res/AssetManager;
    .restart local v5    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .restart local v17    # "r":Landroid/content/res/Resources;
    .restart local v18    # "selfSignCert":Ljava/security/cert/X509Certificate;
    :cond_8
    :try_start_d
    sget-object v21, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 476
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 477
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 479
    const-string v21, "AcmsCertificateMngr"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Is Revoked is: "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v21, "AcmsCertificateMngr"

    .line 483
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "App Id from Self-Signed Cert: "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 484
    const-string v23, " Calculated App Id: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 483
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 482
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 488
    const-string v21, "AcmsCertificateMngr"

    .line 489
    const-string v22, "app id from self-signed cert with calculated app id are not same hence returning"

    .line 487
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const-string v22, "com.android.phone"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 491
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "Skip AppID check for com.android.phone"

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_9
    const-string v21, "AcmsCertificateMngr"

    .line 498
    const-string v22, "app id from self-signed cert with calculated app id are same hence proceeding"

    .line 497
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 504
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v8, v0, [Ljava/security/cert/X509Certificate;

    const/16 v21, 0x0

    aput-object v18, v8, v21

    .line 505
    .local v8, "certs":[Ljava/security/cert/X509Certificate;
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->addToKeyStore(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z

    .line 508
    .end local v8    # "certs":[Ljava/security/cert/X509Certificate;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->insertCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 510
    const-string v21, "AcmsCertificateMngr"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "handleAppAdded() ID = "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    .line 516
    const-string v22, "phone"

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    .line 515
    check-cast v16, Landroid/telephony/TelephonyManager;

    .line 517
    .local v16, "mngr":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v19

    .line 518
    .local v19, "serverId":Ljava/lang/String;
    if-eqz v19, :cond_b

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_d

    .line 519
    :cond_b
    const-string v21, "AcmsCertificateMngr"

    .line 520
    const-string v22, "ServerId is null for this device. Hence not proceeding"

    .line 519
    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_d .. :try_end_d} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_d .. :try_end_d} :catch_10
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_e
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_0

    .line 564
    :catch_7
    move-exception v12

    .line 565
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 561
    .end local v12    # "e":Ljava/io/IOException;
    .end local v16    # "mngr":Landroid/telephony/TelephonyManager;
    .end local v19    # "serverId":Ljava/lang/String;
    :cond_c
    if-eqz v15, :cond_0

    .line 563
    :try_start_f
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    goto/16 :goto_0

    .line 564
    :catch_8
    move-exception v12

    .line 565
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 523
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v16    # "mngr":Landroid/telephony/TelephonyManager;
    .restart local v19    # "serverId":Ljava/lang/String;
    :cond_d
    :try_start_10
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x4

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    .line 525
    const/4 v9, 0x0

    .line 526
    .local v9, "devId":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v9

    .line 527
    const-string v21, "AcmsCertificateMngr"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Dev Id is: "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v11

    .line 531
    .local v11, "devIdCertEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    new-instance v10, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    invoke-direct {v10}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;-><init>()V

    .line 532
    .local v10, "devIdCertEntry":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->setAppId(Ljava/lang/String;)V

    .line 533
    invoke-virtual {v10, v9}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->setDevId(Ljava/lang/String;)V

    .line 536
    invoke-virtual {v11, v10}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->insertCertificate(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z

    .line 541
    .end local v9    # "devId":Ljava/lang/String;
    .end local v10    # "devIdCertEntry":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;
    .end local v11    # "devIdCertEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    .end local v16    # "mngr":Landroid/telephony/TelephonyManager;
    .end local v19    # "serverId":Ljava/lang/String;
    :cond_e
    new-instance v7, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    .line 541
    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;-><init>(Landroid/content/Context;)V

    .line 543
    .local v7, "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->validateCert(Ljava/security/cert/X509Certificate;)Z

    move-result v21

    if-nez v21, :cond_f

    .line 544
    const-string v21, "AcmsCertificateMngr"

    const-string v22, "handleAppAdded() certificate validation failed"

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 546
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    :try_end_10
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_10 .. :try_end_10} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_10} :catch_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 561
    if-eqz v15, :cond_0

    .line 563
    :try_start_11
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9

    goto/16 :goto_0

    .line 564
    :catch_9
    move-exception v12

    .line 565
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 550
    .end local v12    # "e":Ljava/io/IOException;
    :cond_f
    :try_start_12
    sget-object v21, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_12 .. :try_end_12} :catch_e
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_12 .. :try_end_12} :catch_10
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 561
    if-eqz v15, :cond_4

    .line 563
    :try_start_13
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_a

    goto/16 :goto_2

    .line 564
    :catch_a
    move-exception v12

    .line 565
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 564
    .end local v3    # "am":Landroid/content/res/AssetManager;
    .end local v5    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v7    # "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    .end local v13    # "f":Ljava/io/File;
    .end local v17    # "r":Landroid/content/res/Resources;
    .end local v18    # "selfSignCert":Ljava/security/cert/X509Certificate;
    .end local v20    # "version":I
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_b
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 554
    .end local v12    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v12

    .line 555
    .restart local v12    # "e":Ljava/io/IOException;
    :try_start_14
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 561
    if-eqz v15, :cond_4

    .line 563
    :try_start_15
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_d

    goto/16 :goto_2

    .line 564
    :catch_d
    move-exception v12

    .line 565
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 556
    .end local v12    # "e":Ljava/io/IOException;
    :catch_e
    move-exception v12

    .line 557
    .local v12, "e":Ljava/security/cert/CertificateException;
    :try_start_16
    invoke-virtual {v12}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 561
    if-eqz v15, :cond_4

    .line 563
    :try_start_17
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_f

    goto/16 :goto_2

    .line 564
    :catch_f
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 558
    .end local v12    # "e":Ljava/io/IOException;
    :catch_10
    move-exception v12

    .line 559
    .local v12, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_18
    invoke-virtual {v12}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 561
    if-eqz v15, :cond_4

    .line 563
    :try_start_19
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_11

    goto/16 :goto_2

    .line 564
    :catch_11
    move-exception v12

    .line 565
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 560
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v21

    .line 561
    if-eqz v15, :cond_10

    .line 563
    :try_start_1a
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_12

    .line 568
    :cond_10
    :goto_3
    throw v21

    .line 564
    :catch_12
    move-exception v12

    .line 565
    .restart local v12    # "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public handleAppRemoved(Ljava/lang/String;)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 293
    const-string v0, "AcmsCertificateMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleAppRemoved() Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    if-nez v0, :cond_0

    .line 295
    const-string v0, "AcmsCertificateMngr"

    const-string v1, "AppEntryInterface is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v0

    .line 296
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 300
    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 299
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->deleteCertificate(Ljava/lang/String;)Z

    move-result v0

    .line 300
    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 303
    :cond_1
    return-void
.end method

.method public handleExpiredApps()V
    .locals 9

    .prologue
    .line 326
    const-string v5, "AcmsCertificateMngr"

    const-string v6, "handleExpiredApps() Enter "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 328
    .local v3, "getNow":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 329
    .local v1, "currentTime":J
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v5, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getExpiredApps(J)Ljava/util/List;

    move-result-object v4

    .line 330
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-nez v4, :cond_1

    .line 331
    const-string v5, "AcmsCertificateMngr"

    const-string v6, "handleExpiredApps: Expired App list is Null "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_0
    return-void

    .line 334
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 335
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v6, "AcmsCertificateMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "App Pkg Name "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 337
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 339
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v6, v7, :cond_3

    .line 340
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 342
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v6, v7, :cond_4

    .line 343
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 345
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v1, v6

    if-lez v6, :cond_8

    .line 346
    const-string v6, "AcmsCertificateMngr"

    const-string v7, "Grace period has expired"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-eq v6, v7, :cond_5

    .line 348
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v6, v7, :cond_6

    .line 349
    :cond_5
    const-string v6, "AcmsCertificateMngr"

    .line 350
    const-string v7, "Grace period has expired: State is IN Base Grace or IN Drive Grace"

    .line 349
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 352
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    .line 351
    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 353
    :cond_6
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v6, v7, :cond_2

    .line 354
    const-string v6, "AcmsCertificateMngr"

    .line 355
    const-string v7, "Grace period has expired: State is IN Query Period"

    .line 354
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-ne v6, v7, :cond_7

    .line 357
    const-string v6, "AcmsCertificateMngr"

    .line 358
    const-string v7, "Grace period has expired: State is IN Query Period, Type Base Certified"

    .line 357
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 360
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 359
    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 362
    :cond_7
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-ne v6, v7, :cond_2

    .line 363
    const-string v6, "AcmsCertificateMngr"

    .line 364
    const-string v7, "Grace period has expired: State is IN Query Period, Type Drive Certified"

    .line 363
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 366
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 365
    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 370
    :cond_8
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v1, v6

    if-lez v6, :cond_9

    .line 371
    const-string v6, "AcmsCertificateMngr"

    const-string v7, "Query period has expired"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 373
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 372
    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 375
    :cond_9
    const-string v6, "AcmsCertificateMngr"

    const-string v7, "Resuming from current state"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public handlePendingJobs()V
    .locals 5

    .prologue
    .line 306
    const-string v2, "AcmsCertificateMngr"

    const-string v3, "handlePendingJobs() Enter "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    if-nez v2, :cond_1

    .line 308
    const-string v2, "AcmsCertificateMngr"

    const-string v3, "mAppEntryInterface is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getPendingJobs()Ljava/util/List;

    move-result-object v1

    .line 312
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-nez v1, :cond_2

    .line 313
    const-string v2, "AcmsCertificateMngr"

    const-string v3, "handlePendingJobs: Pending job list is Null "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 317
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v3

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 318
    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 319
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    .locals 5
    .param p1, "st"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 383
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mMlAwareHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 384
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 385
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 386
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mMlAwareHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 387
    .local v0, "isSuccess":Z
    if-eqz v0, :cond_0

    .line 389
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v2

    .line 390
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 391
    const-string v2, "AcmsCertificateMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incremented Counter Value : postToMlAwareHandler=>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    return-void
.end method

.method public replaceEntityInAppCertInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 618
    const-string v5, "AcmsCertificateMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "replaceEntityInAppCertInfo: from:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 619
    const-string v7, "for appId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 618
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    .line 623
    .local v1, "appEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getCertificateInfoForAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 625
    .local v3, "orgnlCertInfo":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 626
    const-string v5, "AcmsCertificateMngr"

    .line 627
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "no certificate info is present in DB for this appId"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 628
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 627
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 626
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    :goto_0
    return v4

    .line 632
    :cond_0
    invoke-virtual {v3, p2, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 633
    .local v2, "certInfo":Ljava/lang/String;
    const-string v5, "AcmsCertificateMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "CertInfo: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v0

    .line 636
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v0, :cond_1

    .line 637
    const-string v5, "AcmsCertificateMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "no entry present in DB for this appId"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 641
    :cond_1
    invoke-virtual {v0, p3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 642
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 644
    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    move-result v4

    goto :goto_0
.end method
