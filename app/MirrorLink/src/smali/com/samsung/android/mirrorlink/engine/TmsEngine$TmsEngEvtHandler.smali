.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;
.super Landroid/os/Handler;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TmsEngEvtHandler"
.end annotation


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Landroid/os/Looper;)V
    .locals 2
    .param p1, "tmsEng"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 740
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 741
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngEvtHandler.TmsEngEvtHandler() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngEvtHandler.TmsEngEvtHandler() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 751
    const-string v0, "TMSEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TmsEngEvtHandler.handleMessage() - Enter - msg.what:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 752
    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 751
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 763
    const-string v0, "TMSEngine"

    .line 764
    const-string v1, "TmsEngEvtHandler.handleMessage() - Unknown message type"

    .line 763
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :goto_0
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngEvtHandler.handleMessage() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    return-void

    .line 759
    :pswitch_0
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngEvtHandler.handleMessage() - Do nothing"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 754
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
