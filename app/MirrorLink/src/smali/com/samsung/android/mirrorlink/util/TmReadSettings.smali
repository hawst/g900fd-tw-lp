.class public Lcom/samsung/android/mirrorlink/util/TmReadSettings;
.super Ljava/lang/Object;
.source "TmReadSettings.java"


# static fields
.field private static final RESTRICTED_APPS_CTS:Ljava/lang/String; = "com.android.music;"

.field private static final RESTRICTED_APPS_NO_CTS:Ljava/lang/String; = ""

.field private static mAppsInRestrictedMode:Ljava/lang/String; = null

.field public static final mEnableDap:Z = true

.field private static final mEnableNotiService:Z

.field private static mRestrictedApp:Z

.field private static mSharedPreferences:Landroid/content/SharedPreferences;

.field private static mTestingWithCts:Z


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mRestrictedApp:Z

    .line 14
    sput-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mTestingWithCts:Z

    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mAppsInRestrictedMode:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mContext:Landroid/content/Context;

    .line 24
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 23
    sput-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 25
    sget-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "app_restricted_mode"

    .line 26
    const/4 v2, 0x1

    .line 25
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mRestrictedApp:Z

    .line 27
    sget-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "CTSTool"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mTestingWithCts:Z

    .line 39
    return-void
.end method

.method public static getBuildVersion()I
    .locals 1

    .prologue
    .line 65
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public static isAppInRestrictedModeSet()Ljava/lang/String;
    .locals 3

    .prologue
    .line 42
    sget-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mRestrictedApp:Z

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    .line 45
    :cond_0
    sget-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mTestingWithCts:Z

    if-eqz v0, :cond_1

    .line 46
    sget-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 47
    const-string v1, "apps_in_restricted_mode"

    .line 48
    const-string v2, ""

    .line 46
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mAppsInRestrictedMode:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.android.music;"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mAppsInRestrictedMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 52
    const-string v1, "apps_in_restricted_mode"

    .line 53
    const-string v2, ""

    .line 51
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mAppsInRestrictedMode:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mAppsInRestrictedMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isNotificationSupported()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public static isTestingWithCtsTool()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->mTestingWithCts:Z

    return v0
.end method
