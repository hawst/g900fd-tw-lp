.class Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$CertificateTags;
.super Ljava/lang/Object;
.source "AcmsAppsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CertificateTags"
.end annotation


# static fields
.field private static final APP_CATEGORY_TAG:Ljava/lang/String; = "appCategory"

.field private static final APP_IDENTIFIER_TAG:Ljava/lang/String; = "appIdentifier"

.field private static final APP_UUID_TAG:Ljava/lang/String; = "appUUID"

.field private static final AUDIO_INFO_TAG:Ljava/lang/String; = "audioInfo"

.field private static final AUDIO_TYPE_TAG:Ljava/lang/String; = "audioType"

.field private static final CONTENT_CATEGORY_TAG:Ljava/lang/String; = "contentCategory"

.field private static final DESCRIPTION_TAG:Ljava/lang/String; = "description"

.field private static final DISPLAY_INFO_TAG:Ljava/lang/String; = "displayInfo"

.field private static final ENTITY_TAG:Ljava/lang/String; = "entity"

.field private static final ICON_DEPTH_TAG:Ljava/lang/String; = "depth"

.field private static final ICON_HEIGHT_TAG:Ljava/lang/String; = "height"

.field private static final ICON_LIST_TAG:Ljava/lang/String; = "iconList"

.field private static final ICON_MIMETYPE_TAG:Ljava/lang/String; = "mimetype"

.field private static final ICON_TAG:Ljava/lang/String; = "icon"

.field private static final ICON_URL_TAG:Ljava/lang/String; = "url"

.field private static final ICON_WIDTH_TAG:Ljava/lang/String; = "width"

.field private static final NAME_TAG:Ljava/lang/String; = "name"

.field private static final NON_RESTRICTED_TAG:Ljava/lang/String; = "nonRestricted"

.field private static final ORIENTATION_TAG:Ljava/lang/String; = "orientation"

.field private static final PROPERTIES_TAG:Ljava/lang/String; = "properties"

.field private static final PROVIDER_NAME_TAG:Ljava/lang/String; = "providerName"

.field private static final PROVIDER_URL_TAG:Ljava/lang/String; = "providerURL"

.field private static final RESTRICTED_TAG:Ljava/lang/String; = "restricted"

.field private static final SERVICE_LIST_TAG:Ljava/lang/String; = "serviceList"

.field private static final SERVICE_TAG:Ljava/lang/String; = "service"

.field private static final SIGNATURE_TAG:Ljava/lang/String; = "Signature"

.field private static final TARGET_LIST_TAG:Ljava/lang/String; = "targetList"

.field private static final TARGET_TAG:Ljava/lang/String; = "target"

.field private static final VARIANT_TAG:Ljava/lang/String; = "variant"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
