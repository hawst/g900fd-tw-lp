.class Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;
.super Landroid/os/RemoteCallbackList;
.source "ContextManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/commonapi/ContextManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Lcom/mirrorlink/android/commonapi/IContextListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 25
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->onCallbackDied(Lcom/mirrorlink/android/commonapi/IContextListener;Ljava/lang/Object;)V

    return-void
.end method

.method public onCallbackDied(Lcom/mirrorlink/android/commonapi/IContextListener;Ljava/lang/Object;)V
    .locals 5
    .param p1, "callback"    # Lcom/mirrorlink/android/commonapi/IContextListener;
    .param p2, "packName"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 29
    const-string v2, "TMSContextManager"

    const-string v3, "ContextManager.onCallbackDied()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;)V

    .line 32
    const-string v2, "TMSContextManager"

    .line 33
    const-string v3, "ContextManager.onCallbackDied() : Calling clearPackageFromMap()"

    .line 32
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v3

    move-object v2, p2

    .line 36
    check-cast v2, Ljava/lang/String;

    .line 35
    invoke-virtual {v3, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->clearPackageFromMap(Ljava/lang/String;)V

    .line 49
    const-string v2, "TMSContextManager"

    const-string v3, "ContextManager.onCallbackDied() : Calling unset AudioContext method"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 51
    .local v0, "audioCategories":[I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v3

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v2, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setAudioCtxtInfo(Z[ILjava/lang/String;Z)Z

    .line 54
    .end local v0    # "audioCategories":[I
    :cond_0
    const-string v2, "TMSContextManager"

    .line 55
    const-string v3, "ContextManager.onCallbackDied() : Calling CommonApiService().applicationStopping() method"

    .line 53
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->access$0(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-result-object v2

    .line 57
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v1

    .line 59
    .local v1, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-eqz v1, :cond_1

    .line 61
    check-cast p2, Ljava/lang/String;

    .end local p2    # "packName":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onApplicationStopping(Ljava/lang/String;)V

    .line 63
    :cond_1
    return-void
.end method
