.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;
.super Ljava/lang/Object;
.source "AcsSingleTimer.java"


# static fields
.field private static final AlarmTimerCancel:I = 0x2

.field private static final AlarmTimerSet:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TMSSingleTimer"

.field private static sMe:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;


# instance fields
.field private alarmSet:Z

.field private mCxt:Landroid/content/Context;

.field private mNativeContext:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.AcsSingleTimer() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    sput-object p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->sMe:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    .line 40
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.AcsSingleTimer() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static getAcsSingleTimer()Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;
    .locals 2

    .prologue
    .line 27
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.getAcsSingleTimer() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->sMe:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    if-nez v0, :cond_0

    .line 29
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.getAcsSingleTimer() - null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->sMe:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    return-object v0
.end method

.method private handleAlarmEvents(III)V
    .locals 8
    .param p1, "func_type"    # I
    .param p2, "duration"    # I
    .param p3, "timerId"    # I

    .prologue
    const/4 v7, 0x0

    .line 93
    const-string v3, "TMSSingleTimer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "handleAlarmEvents: Func: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " duration::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 94
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timerId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v3, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->sMe:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    if-nez v3, :cond_0

    .line 97
    const-string v3, "TMSSingleTimer"

    const-string v4, "AcsSingleTimer.getAcsSingleTimer() - null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 142
    const-string v3, "TMSSingleTimer"

    const-string v4, "handleAlarmEvents: default case "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 103
    :pswitch_0
    const-string v3, "TMSSingleTimer"

    const-string v4, " AlarmTimerSet case "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "TMSSingleTimer"

    const-string v4, " Alarm intent created "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v3, "com.samsung.acs.framework.tid"

    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 112
    .local v0, "am":Landroid/app/AlarmManager;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    invoke-static {v3, p3, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 114
    .local v2, "pi":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    int-to-long v5, p2

    add-long/2addr v3, v5

    invoke-virtual {v0, v7, v3, v4, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 116
    const-string v3, "TMSSingleTimer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ALARM SET at :::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for duration"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->alarmSet:Z

    goto :goto_0

    .line 123
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pi":Landroid/app/PendingIntent;
    :pswitch_1
    const-string v3, "TMSSingleTimer"

    const-string v4, " AlarmTimerCancel case "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->alarmSet:Z

    if-eqz v3, :cond_1

    .line 125
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v3, "TMSSingleTimer"

    const-string v4, " Alarm intent created "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 130
    .restart local v0    # "am":Landroid/app/AlarmManager;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    invoke-static {v3, p3, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 132
    .restart local v2    # "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 133
    const-string v3, "TMSSingleTimer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ALARM CANCELLED at :::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iput-boolean v7, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->alarmSet:Z

    goto/16 :goto_0

    .line 137
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pi":Landroid/app/PendingIntent;
    :cond_1
    const-string v3, "TMSSingleTimer"

    const-string v4, " NO ALARM RUNNING "

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private nativeTimerEvent(III)V
    .locals 3
    .param p1, "func_type"    # I
    .param p2, "duration"    # I
    .param p3, "timerId"    # I

    .prologue
    .line 55
    const-string v0, "TMSSingleTimer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "nativeTimerEvent: func_type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->handleAlarmEvents(III)V

    .line 57
    return-void
.end method

.method private native native_acs_timer_expiry_cb(I)V
.end method


# virtual methods
.method public alarmExpiryCallback(I)V
    .locals 3
    .param p1, "timerId"    # I

    .prologue
    .line 147
    const-string v0, "TMSSingleTimer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "alarmExpiryCallback: Id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->native_acs_timer_expiry_cb(I)V

    .line 149
    return-void
.end method

.method public setContext(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "l"    # Landroid/os/Looper;

    .prologue
    .line 44
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.setContext() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->mCxt:Landroid/content/Context;

    .line 50
    const-string v0, "TMSSingleTimer"

    const-string v1, "AcsSingleTimer.setContext() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method
