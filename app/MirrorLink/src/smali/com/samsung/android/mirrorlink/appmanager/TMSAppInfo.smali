.class public Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
.super Ljava/lang/Object;
.source "TMSAppInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;,
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;,
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;,
        Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$Type;
    }
.end annotation


# static fields
.field private static final APPSTATUS_NOTRUNNING:I = 0x16

.field private static final TAG:Ljava/lang/String; = "TMSAppInfo"


# instance fields
.field public flagCertified:Z

.field public mAppCategory:I

.field public mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

.field public mAppCertifcateUrl:Ljava/lang/String;

.field public mAppDisplayName:Ljava/lang/String;

.field public mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

.field public mAppId:I

.field public mAppStatus:I

.field public mAudioInfoContentCategory:I

.field public mAudioInfoContentRules:I

.field public mAudioInfoTustLevel:I

.field public mAudioType:Ljava/lang/String;

.field public mDescription:Ljava/lang/String;

.field public mDisplayInfoContentCategory:I

.field public mDisplayInfoContentRules:I

.field public mDisplayInfoTustLevel:I

.field public mExtraEleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;",
            ">;"
        }
    .end annotation
.end field

.field public mIconList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mOrientation:Ljava/lang/String;

.field public mPackageName:Ljava/lang/String;

.field public mProviderName:Ljava/lang/String;

.field public mProviderURL:Ljava/lang/String;

.field private mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

.field public mResourceStatus:Ljava/lang/String;

.field public mTrustLevel:I

.field public mVariant:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    .line 226
    const/16 v0, 0x16

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 227
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 228
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 229
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 230
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 231
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 232
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 233
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentRules:I

    .line 234
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 237
    const-string v0, "free"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 238
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 316
    if-ne p0, p1, :cond_1

    .line 348
    :cond_0
    :goto_0
    return v1

    .line 318
    :cond_1
    if-nez p1, :cond_2

    .line 319
    const-string v1, "TMSAppInfo"

    const-string v3, "obj is null"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 320
    goto :goto_0

    .line 322
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    .line 323
    const-string v1, "TMSAppInfo"

    const-string v3, "class is not same "

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 324
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 326
    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 327
    .local v0, "other":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    iget-boolean v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eq v3, v4, :cond_4

    .line 328
    const-string v1, "TMSAppInfo"

    const-string v3, "flagCertified is chanfed"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 329
    goto :goto_0

    .line 331
    :cond_4
    iget v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    iget v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    if-eq v3, v4, :cond_5

    .line 332
    const-string v1, "TMSAppInfo"

    const-string v3, "mAppCategory is chanfed"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 333
    goto :goto_0

    .line 335
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-nez v3, :cond_6

    .line 336
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v3, :cond_7

    .line 337
    const-string v1, "TMSAppInfo"

    const-string v3, "mAppCertInfo is null"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 338
    goto :goto_0

    .line 340
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    invoke-virtual {v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 341
    const-string v1, "TMSAppInfo"

    const-string v3, "mAppCertInfo is changed"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 342
    goto :goto_0

    .line 344
    :cond_7
    iget v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    iget v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    if-eq v3, v4, :cond_0

    .line 345
    const-string v1, "TMSAppInfo"

    const-string v3, "mDisplayInfoContentCategory is changed"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 346
    goto :goto_0
.end method

.method public getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 304
    const/16 v0, 0x1f

    .line 305
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 306
    .local v1, "result":I
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 307
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    add-int v1, v2, v3

    .line 308
    mul-int/lit8 v3, v1, 0x1f

    .line 309
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 308
    :goto_1
    add-int v1, v3, v2

    .line 310
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    add-int v1, v2, v3

    .line 311
    return v1

    .line 306
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0

    .line 309
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "protocolId"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "direction"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    .line 272
    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    .line 283
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p1, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p2, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 278
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p3, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 280
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    goto :goto_0
.end method

.method public setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p1, "protocolId"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "direction"    # Ljava/lang/String;
    .param p4, "audioIpl"    # I
    .param p5, "audioMpl"    # I

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 253
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 249
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    .line 264
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p1, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 258
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p2, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 259
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput-object p3, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 260
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput p4, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 261
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mRemotingInfo:Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    iput p5, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    goto :goto_0
.end method
