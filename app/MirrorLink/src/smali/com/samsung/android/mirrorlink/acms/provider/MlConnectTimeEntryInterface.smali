.class public Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;
.super Ljava/lang/Object;
.source "MlConnectTimeEntryInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface$MlConnectTimeData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsMlConnectTimeEntryInterface"

.field private static sMlConnectTimeEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public static getMlConnectTimeEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const-class v1, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    monitor-enter v1

    .line 41
    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->sMlConnectTimeEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->sMlConnectTimeEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    .line 40
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->sMlConnectTimeEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 78
    const-class v1, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    monitor-enter v1

    .line 79
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->sMlConnectTimeEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    .line 78
    monitor-exit v1

    .line 81
    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getRecentMLConnectionTime()J
    .locals 9

    .prologue
    const-wide/16 v7, 0x0

    const/4 v3, 0x0

    .line 51
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 52
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_MLTIME:Landroid/net/Uri;

    .line 53
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "mlConnectedtime"

    aput-object v5, v2, v4

    .line 54
    const-string v5, "mlConnectedtime DESC LIMIT 1 "

    move-object v4, v3

    .line 51
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 55
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 56
    const-string v0, "AcmsMlConnectTimeEntryInterface"

    const-string v1, "Cursor is null: Hence Returning"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :goto_0
    return-wide v7

    .line 58
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 59
    const-string v0, "AcmsMlConnectTimeEntryInterface"

    const-string v1, "No data in Ml connect table"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 63
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 65
    const-string v0, "mlConnectedtime"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 64
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 66
    .local v7, "recentMLTime":J
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
