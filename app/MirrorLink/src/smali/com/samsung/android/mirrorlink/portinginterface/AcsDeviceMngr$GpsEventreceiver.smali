.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;
.super Landroid/content/BroadcastReceiver;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GpsEventreceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0

    .prologue
    .line 3571
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 3573
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3574
    .local v0, "action":Ljava/lang/String;
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mGpsEventreceiver "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3575
    if-nez v0, :cond_1

    .line 3623
    :cond_0
    :goto_0
    return-void

    .line 3578
    :cond_1
    const-string v3, "START_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3579
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMSERVER_START_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3580
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initGpsService()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$17(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    goto :goto_0

    .line 3581
    :cond_2
    const-string v3, "STOP_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3582
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMSERVER_STOP_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3583
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopGpsService()V

    goto :goto_0

    .line 3584
    :cond_3
    const-string v3, "START_HU_LOCATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3585
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMSERVER_START_LOCATION "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3586
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->initLocationService()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$18(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    goto :goto_0

    .line 3587
    :cond_4
    const-string v3, "STOP_HU_LOCATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 3588
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMSERVER_STOP_LOCATION "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3589
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopLocationService()V

    goto/16 :goto_0

    .line 3590
    :cond_5
    const-string v3, "SUBSRCRIBE_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3591
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMSERVER_SUBSCRIBE_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3592
    const-string v3, "SUBSRCRIBE_TYPE_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 3593
    .local v1, "type":I
    const-string v3, "SUBSRCRIBE_INTERVAL_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3594
    .local v2, "val":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->subscribeGps(II)V

    goto/16 :goto_0

    .line 3596
    .end local v1    # "type":I
    .end local v2    # "val":I
    :cond_6
    const-string v3, "CTS_TESTS_SUBSCRIBE_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3597
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_SUBSCRIBE_HU_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3598
    const-string v3, "SUBSRCRIBE_TYPE_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 3599
    .restart local v1    # "type":I
    const-string v3, "SUBSRCRIBE_INTERVAL_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3600
    .restart local v2    # "val":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->subscribeGps(II)V

    goto/16 :goto_0

    .line 3601
    .end local v1    # "type":I
    .end local v2    # "val":I
    :cond_7
    const-string v3, "CTS_TESTS_CANCEL_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 3602
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_CANCEL_HU_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3603
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->cancelSubscribeGps()V

    goto/16 :goto_0

    .line 3604
    :cond_8
    const-string v3, "CTS_TESTS_ALIVE_REQ_HU_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3605
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_ALIVE_REQ_HU_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3606
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->aliveRequestGps()V

    goto/16 :goto_0

    .line 3607
    :cond_9
    const-string v3, "CTS_TESTS_GET_NMEA_GPS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 3608
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_GET_NMEA_GPS "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3609
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getNmeaGps()V

    goto/16 :goto_0

    .line 3610
    :cond_a
    const-string v3, "CTS_TESTS_GET_GEO_LOCATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 3611
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_GET_GEO_LOCATION "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3612
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getGeoLocation()V

    goto/16 :goto_0

    .line 3613
    :cond_b
    const-string v3, "CTS_TESTS_CANCEL_SUBSCRIBE_LOCATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 3614
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_CANCEL_SUBSCRIBE_LOCATION "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3615
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->cancelSubcribeLocation()V

    goto/16 :goto_0

    .line 3616
    :cond_c
    const-string v3, "CTS_TESTS_SUBSCRIBE_HU_LOCATION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3617
    const-string v3, "TMSDevMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CTS_TESTS_SUBSCRIBE_LOCATION "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3618
    const-string v3, "SUBSRCRIBE_TYPE_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 3619
    .restart local v1    # "type":I
    const-string v3, "SUBSRCRIBE_INTERVAL_HU_GPS"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 3620
    .restart local v2    # "val":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$GpsEventreceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->subscribeLocation(II)V

    goto/16 :goto_0
.end method
