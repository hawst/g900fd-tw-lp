.class Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;
.super Landroid/database/ContentObserver;
.source "TMSAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Observer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1611
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 1612
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1613
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1617
    const-string v0, "TMSAppMngr"

    const-string v1, "Observer.onChange : State is inited."

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;->onChange(ZLandroid/net/Uri;)V

    .line 1622
    :goto_0
    return-void

    .line 1620
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "Observer.onChange .In process of deinit. Ignore the callback"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1626
    const-string v0, "TMSAppMngr"

    const-string v1, "On change in Acms Db"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->isInited:Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1628
    const-string v0, "TMSAppMngr"

    const-string v1, "Observer.onChange uri : State is inited."

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1630
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$Observer;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->usrAppHldr:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$4(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->updateCertifiedApps()V

    .line 1635
    :goto_0
    return-void

    .line 1632
    :cond_0
    const-string v0, "TMSAppMngr"

    const-string v1, "Observer.onChange uri.In process of deinit. Ignore the callback"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
