.class public Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;
.super Ljava/lang/Object;
.source "DapAppholder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSDapAppholder"

.field private static final TM_AUDIO_DAP_HOLDERNAME:Ljava/lang/String; = "DapAppholder"

.field private static final TM_PROTOCOL_ID:Ljava/lang/String; = "DAP"

.field private static final TM_RESOURCE_STATUS_FREE:Ljava/lang/String; = "free"


# instance fields
.field private APP_NAME_DAP:Ljava/lang/String;

.field private cntxt:Landroid/content/Context;

.field private mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppIsRunningDapApp:Z

.field private mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

.field private mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

.field private mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-string v0, "Device Attestation"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->APP_NAME_DAP:Ljava/lang/String;

    .line 114
    const-string v0, "TMSDapAppholder"

    const-string v1, "Enter : DapAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 119
    const-string v0, "TMSDapAppholder"

    const-string v1, "ERROR !!! No Context"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .line 123
    const-string v0, "TMSDapAppholder"

    const-string v1, "Exit : DapAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private fillAppStatus()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method private launchDapApp()V
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppIsRunningDapApp:Z

    .line 222
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->dapStart()I

    .line 224
    :cond_0
    return-void
.end method

.method private setConfig()V
    .locals 8

    .prologue
    .line 184
    const-string v6, "TMSDapAppholder"

    const-string v7, " setConfig "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 191
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v6, "dap_self_port_key"

    const-string v7, "9920"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "selfPort":Ljava/lang/String;
    const-string v6, "dap_passw_key"

    const-string v7, "9FY)f5/8Q<04Yc"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 195
    .local v2, "manfPassKey":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 202
    .local v1, "iSelfPort":I
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/data/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/files/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "assetFileName":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v3}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 206
    .local v3, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v6, "DapSelfPort"

    invoke-virtual {v3, v6, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 207
    const-string v6, "DapManfPassKey"

    invoke-virtual {v3, v6, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v6, "DapKeyStorePath"

    invoke-virtual {v3, v6, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v6, :cond_0

    .line 211
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setDapParams(Ljava/lang/String;)I

    .line 213
    :cond_0
    const-string v6, "TMSDapAppholder"

    const-string v7, " setConfig "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method private stopDapApp()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppIsRunningDapApp:Z

    .line 231
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->dapStop()I

    .line 233
    :cond_0
    return-void
.end method


# virtual methods
.method public deinit()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 447
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppIsRunningDapApp:Z

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->dapStop()I

    .line 451
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 452
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppIsRunningDapApp:Z

    .line 453
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 454
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 455
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->cntxt:Landroid/content/Context;

    .line 456
    return v2
.end method

.method public destroy()Z
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    return v0
.end method

.method public getAllAppList()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppList()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619
    const-string v0, "TMSDapAppholder"

    const-string v1, "DapAppholder.getAppList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->fillAppStatus()V

    .line 621
    const-string v0, "TMSDapAppholder"

    const-string v1, "DapAppholder.getAppList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 584
    if-nez p1, :cond_0

    .line 585
    const-string v0, "TMSDapAppholder"

    const-string v1, "getAppStatus(packageName == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :goto_0
    return-object v3

    .line 589
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->fillAppStatus()V

    .line 596
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_3

    .line 597
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 598
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    const-string v0, "TMSDapAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAppStatus mAppInfoDapApp.mAppStatus "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 602
    :cond_1
    const-string v0, "TMSDapAppholder"

    const-string v1, "getAppStatus(mAppInfoDapApp.mPackageName.equals(packageName))FAILED"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 605
    :cond_2
    const-string v0, "TMSDapAppholder"

    const-string v1, "getAppStatus(mAppInfoDapApp.mPackageName == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :cond_3
    const-string v0, "TMSDapAppholder"

    const-string v1, "getAppStatus(mAppInfoDapApp == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAppStatusDapApp()V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public getNotRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 488
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public informAppStatusChange(Ljava/lang/String;I)V
    .locals 2
    .param p1, "app"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    if-eqz v0, :cond_0

    .line 435
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->fillAppStatus()V

    .line 436
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 440
    :cond_0
    return-void
.end method

.method public init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
    .locals 5
    .param p1, "appmngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "cntxt"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 498
    const-string v1, "TMSDapAppholder"

    const-string v2, "DapAppholder.init enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    if-nez v1, :cond_0

    .line 500
    const-string v1, "TMSDapAppholder"

    const-string v2, "mDbInfo is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :goto_0
    return v4

    .line 527
    :cond_0
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 529
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->APP_NAME_DAP:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 530
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->APP_NAME_DAP:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 531
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v2, -0xfffffff

    iput v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 533
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v2, "free"

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 535
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 536
    const-string v2, "DAP"

    .line 535
    invoke-virtual {v1, v2, v3, v3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->fillAppStatus()V

    .line 541
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    if-nez v1, :cond_1

    .line 542
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 545
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 546
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 547
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v1

    long-to-int v0, v1

    .line 548
    .local v0, "rowNum":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 549
    const-string v1, "TMSDapAppholder"

    const-string v2, " Inserted dapApp into db"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 551
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    :cond_2
    :goto_1
    const-string v1, "DapAppholder"

    invoke-virtual {p1, v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 564
    const-string v1, "TMSDapAppholder"

    const-string v2, "DapAppholder.init exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 553
    :cond_3
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    .line 554
    if-eqz v0, :cond_2

    .line 555
    const-string v1, "TMSDapAppholder"

    const-string v2, "Found appId from db for dap app"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 557
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppInfoDapApp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
    .locals 1
    .param p1, "appStatusListener"    # Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .prologue
    .line 574
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .line 575
    const/4 v0, 0x1

    return v0
.end method

.method public setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 2
    .param p1, "nativeIface"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 177
    const-string v0, "TMSDapAppholder"

    const-string v1, " setnativeinterface "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 179
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 631
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->setConfig()V

    .line 632
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->APP_NAME_DAP:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->launchDapApp()V

    .line 635
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 644
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->APP_NAME_DAP:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->stopDapApp()V

    .line 647
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
