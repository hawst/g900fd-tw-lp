.class public Lcom/samsung/android/mirrorlink/commonapi/EventConfig;
.super Ljava/lang/Object;
.source "EventConfig.java"


# instance fields
.field deviceKeySupport:I

.field funcKeySupport:I

.field ituKeySupport:Z

.field keyboardCountry:Ljava/lang/String;

.field keyboardLanguage:Ljava/lang/String;

.field knobSupport:I

.field mmKeySupport:I

.field pressureMask:I

.field touchSupport:I

.field uiCountry:Ljava/lang/String;

.field uiLanguage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    return-void
.end method


# virtual methods
.method public getDeviceKeySupport()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->deviceKeySupport:I

    return v0
.end method

.method public getFuncKeySupport()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->funcKeySupport:I

    return v0
.end method

.method public getItuKeySupport()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->ituKeySupport:Z

    return v0
.end method

.method public getKeyboardCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->keyboardCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyboardLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->keyboardLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getKnobSupport()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->knobSupport:I

    return v0
.end method

.method public getMmKeySupport()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->mmKeySupport:I

    return v0
.end method

.method public getPressureMask()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->pressureMask:I

    return v0
.end method

.method public getTouchSupport()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->touchSupport:I

    return v0
.end method

.method public getUICountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->uiCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getUILanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->uiLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceKeySupport(I)V
    .locals 0
    .param p1, "deviceKeySupport"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->deviceKeySupport:I

    .line 124
    return-void
.end method

.method public setFuncKeySupport(I)V
    .locals 0
    .param p1, "funcKeySupport"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->funcKeySupport:I

    .line 152
    return-void
.end method

.method public setItuKeySupport(Z)V
    .locals 0
    .param p1, "ituKeySupport"    # Z

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->ituKeySupport:Z

    .line 166
    return-void
.end method

.method public setKeyboardCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyboardCountry"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->keyboardCountry:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setKeyboardLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyboardLanguage"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->keyboardLanguage:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setKnobSupport(I)V
    .locals 0
    .param p1, "knobSupport"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->knobSupport:I

    .line 110
    return-void
.end method

.method public setMmKeySupport(I)V
    .locals 0
    .param p1, "mmKeySupport"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->mmKeySupport:I

    .line 138
    return-void
.end method

.method public setPressureMask(I)V
    .locals 0
    .param p1, "pressureMask"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->pressureMask:I

    .line 58
    return-void
.end method

.method public setTouchSupport(I)V
    .locals 0
    .param p1, "touchSupport"    # I

    .prologue
    .line 179
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->touchSupport:I

    .line 180
    return-void
.end method

.method public setUICountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "uiCountry"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->uiCountry:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setUILanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "uiLanguage"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->uiLanguage:Ljava/lang/String;

    .line 86
    return-void
.end method
