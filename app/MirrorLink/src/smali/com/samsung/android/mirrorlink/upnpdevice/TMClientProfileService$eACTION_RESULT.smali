.class public final enum Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;
.super Ljava/lang/Enum;
.source "TMClientProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "eACTION_RESULT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field public static final enum INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field public static final enum INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field public static final enum OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field public static final enum RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

.field public static final enum UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;


# instance fields
.field actionResult:I

.field errorDesc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 131
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "OPERATION_REJECTED"

    const/16 v2, 0x2c1

    const-string v3, "Operation Rejected"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 132
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "RESOURCE_BUSY"

    const/16 v2, 0x32e

    const-string v3, "Resource busy"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 133
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "DEVICE_LOCKED"

    const/16 v2, 0x32f

    const-string v3, "Device Locked"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 134
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "INVALID_PROFILEID"

    const/16 v2, 0x33e

    const-string v3, "Invalid Profile ID"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 135
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "INVALID_PROFILE"

    const/16 v2, 0x33f

    const-string v3, "Invalid Profile"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 136
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    const-string v1, "UNKOWN_ERROR"

    const/4 v2, 0x5

    const/16 v3, 0x383

    const-string v4, "Unknown error"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    .line 129
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v1, v0, v8

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->UNKOWN_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # I
    .param p4, "errorDesc"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput p3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->actionResult:I

    .line 144
    iput-object p4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->errorDesc:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->actionResult:I

    return v0
.end method

.method public getErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->errorDesc:Ljava/lang/String;

    return-object v0
.end method
