.class Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;
.super Landroid/os/Handler;
.source "AcmsCertificateMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State()[I
    .locals 3

    .prologue
    .line 107
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_b
    :try_start_b
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_c
    :try_start_c
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_d
    :try_start_d
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_e
    :try_start_e
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_f
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_f

    :catch_1
    move-exception v1

    goto :goto_e

    :catch_2
    move-exception v1

    goto :goto_d

    :catch_3
    move-exception v1

    goto :goto_c

    :catch_4
    move-exception v1

    goto :goto_b

    :catch_5
    move-exception v1

    goto :goto_a

    :catch_6
    move-exception v1

    goto :goto_9

    :catch_7
    move-exception v1

    goto :goto_8

    :catch_8
    move-exception v1

    goto :goto_7

    :catch_9
    move-exception v1

    goto :goto_6

    :catch_a
    move-exception v1

    goto :goto_5

    :catch_b
    move-exception v1

    goto/16 :goto_4

    :catch_c
    move-exception v1

    goto/16 :goto_3

    :catch_d
    move-exception v1

    goto/16 :goto_2

    :catch_e
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    .line 107
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private sendMessageToMLAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    .locals 5
    .param p1, "st"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 111
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 112
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 113
    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 115
    .local v0, "isSuccess":Z
    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v2

    .line 118
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 119
    const-string v2, "AcmsCertificateMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incremented Counter Value: sendMessageToMLAwareHandler=>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 126
    iget v10, p1, Landroid/os/Message;->what:I

    sget-object v11, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v11

    if-le v10, v11, :cond_1

    .line 127
    sget-object v9, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 131
    .local v9, "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :goto_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 132
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State()[I

    move-result-object v10

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 276
    :goto_1
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    .line 277
    .local v0, "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 279
    :cond_0
    const-string v10, "AcmsCertificateMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Decremented Counter Value: mMlAwareHandler.handleMessage =>"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p1, Landroid/os/Message;->what:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-void

    .line 129
    .end local v0    # "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    .end local v1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v9    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :cond_1
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v10

    iget v11, p1, Landroid/os/Message;->what:I

    aget-object v9, v10, v11

    .restart local v9    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    goto :goto_0

    .line 135
    .restart local v1    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :pswitch_1
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "mMlAwareHandler - STATE_INSTALLED"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    .line 138
    const-string v11, "DevModePref"

    .line 139
    const/4 v12, 0x0

    .line 138
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 142
    .local v8, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v10, "DevMode"

    .line 143
    const/4 v11, 0x1

    .line 141
    invoke-interface {v8, v10, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 145
    .local v6, "presentDevMode":Z
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 146
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ACMS"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 147
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v10

    const-string v11, "DEVELOPER"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    if-eqz v6, :cond_3

    .line 148
    :cond_2
    const-string v10, "AcmsCertificateMngr"

    .line 149
    const-string v11, "handleAppAdded() Entity tag contains name ACMS or DEVELOPER, So Fetch Cert"

    .line 148
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-direct {p0, v10, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->sendMessageToMLAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_1

    .line 153
    :cond_3
    const-string v10, "AcmsCertificateMngr"

    .line 154
    const-string v11, "handleAppAdded() Entity Tag is empty or does not contain ACMS or DEVELOPER - Treat as MLAware"

    .line 152
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-direct {p0, v10, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->sendMessageToMLAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_1

    .line 161
    .end local v6    # "presentDevMode":Z
    .end local v8    # "sharedPreferences":Landroid/content/SharedPreferences;
    :pswitch_2
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "mMlAwareHandler - STATE_APPCERT_FETCH_PENDING"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 163
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->getIsDevInsert()Z

    move-result v5

    .line 164
    .local v5, "isDevInsert":Z
    const-string v10, "AcmsCertificateMngr"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "isDevInsert Flag is: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 167
    if-eqz v5, :cond_5

    .line 168
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "This is a development app"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v10

    .line 172
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "devIdFromCert":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    .line 176
    const-string v11, "DevIdPref"

    .line 177
    const/4 v12, 0x0

    .line 175
    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 180
    .local v7, "sharedPref":Landroid/content/SharedPreferences;
    const-string v10, "DevId"

    .line 181
    const-string v11, "No Value"

    .line 179
    invoke-interface {v7, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 182
    .local v4, "devIdFromPref":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 183
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 184
    const-string v10, "AcmsCertificateMngr"

    .line 185
    const-string v11, "dev Id from prefs and from certificate are same. Hence proceeding to fetch"

    .line 184
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    move-result-object v10

    .line 187
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    move-result-object v11

    .line 186
    invoke-virtual {v10, v1, v11}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->fetchAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)Z

    goto/16 :goto_1

    .line 190
    :cond_4
    const-string v10, "AcmsCertificateMngr"

    .line 191
    const-string v11, "Shared Pref does not conatin devId or devId from Preference and Self-Signed Certificate does not match. Hence be in same state"

    .line 189
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 193
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 194
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto/16 :goto_1

    .line 198
    .end local v3    # "devIdFromCert":Ljava/lang/String;
    .end local v4    # "devIdFromPref":Ljava/lang/String;
    .end local v7    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_5
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    move-result-object v10

    .line 199
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    move-result-object v11

    .line 198
    invoke-virtual {v10, v1, v11}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->fetchAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)Z

    goto/16 :goto_1

    .line 202
    .end local v5    # "isDevInsert":Z
    :cond_6
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 203
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 204
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 205
    const-string v10, "AcmsCertificateMngr"

    .line 206
    const-string v11, "No data connection - Be in same state until data connection TRUE"

    .line 205
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 211
    :pswitch_3
    const-string v10, "AcmsCertificateMngr"

    .line 212
    const-string v11, "mMlAwareHandler - STATE_APPCERT_FETCH_COMPLETED"

    .line 211
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 215
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 216
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 217
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto/16 :goto_1

    .line 221
    :pswitch_4
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "mMlAwareHandler - STATE_APPCERT_FETCH_RETRY"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 223
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAcmsHttpHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    move-result-object v10

    .line 224
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppCertFetchCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;

    move-result-object v11

    .line 223
    invoke-virtual {v10, v1, v11}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->fetchAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)Z

    goto/16 :goto_1

    .line 226
    :cond_7
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 227
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 228
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 229
    const-string v10, "AcmsCertificateMngr"

    .line 230
    const-string v11, "No data connection - Be in same state until data connection TRUE"

    .line 229
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 235
    :pswitch_5
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "mMlAwareHandler - STATE_APPCERT_NON_CERTIFIED"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 238
    new-instance v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    .line 239
    .local v2, "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 240
    const-wide v10, 0x7fffffffffffffffL

    invoke-virtual {v2, v10, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 241
    const-wide v10, 0x7fffffffffffffffL

    invoke-virtual {v2, v10, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 242
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v2, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 243
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 244
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 245
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateDevApps(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 254
    .end local v2    # "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :goto_2
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    goto/16 :goto_1

    .line 247
    :cond_8
    const-wide v10, 0x7fffffffffffffffL

    invoke-virtual {v1, v10, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 248
    const-wide v10, 0x7fffffffffffffffL

    invoke-virtual {v1, v10, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 249
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 250
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 251
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 252
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto :goto_2

    .line 258
    :pswitch_6
    const-string v10, "AcmsCertificateMngr"

    const-string v11, "mMlAwareHandler - STATE_OCSPCERT_QUERY_PENDING"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 261
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v10

    .line 262
    sget-object v11, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 261
    invoke-virtual {v10, v11, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 264
    :cond_9
    sget-object v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 265
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 266
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v10

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto/16 :goto_1

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
