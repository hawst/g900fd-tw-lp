.class final Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;
.super Ljava/lang/Object;
.source "TMServerDevice.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mDevice:Lcom/samsung/upnp/Device;


# direct methods
.method public constructor <init>(Lcom/samsung/upnp/Device;)V
    .locals 1
    .param p1, "mDvc"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 979
    const-string v0, "TMSUPnP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->LOG_TAG:Ljava/lang/String;

    .line 983
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->mDevice:Lcom/samsung/upnp/Device;

    .line 984
    return-void
.end method


# virtual methods
.method public OnNotiReceived(Ljava/lang/String;)V
    .locals 3
    .param p1, "notiId"    # Ljava/lang/String;

    .prologue
    .line 988
    if-nez p1, :cond_0

    .line 989
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice.OnNotiReceived notiId is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice.OnNotiReceived exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    return-void

    .line 991
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice.OnNotiReceived notiId is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 992
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 991
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->mDevice:Lcom/samsung/upnp/Device;

    const-string v1, "ActiveNotiEvent"

    invoke-static {v0, v1, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 994
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;->LOG_TAG:Ljava/lang/String;

    .line 995
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMServerDevice.OnNotiReceived set state varibale ActiveNotiEvent with value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 996
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 995
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 994
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
