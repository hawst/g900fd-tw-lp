.class public Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;
.super Ljava/lang/Object;
.source "BtAppholder.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "A2dpListener"
.end annotation


# instance fields
.field private mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

.field final synthetic this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)V
    .locals 0
    .param p2, "a2dp"    # Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .prologue
    .line 2300
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 2299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 2302
    return-void
.end method


# virtual methods
.method deinit()V
    .locals 1

    .prologue
    .line 2330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 2331
    return-void
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 2307
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " A2dpProfileManager : A2dpListener :onServiceConnected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2309
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Landroid/bluetooth/BluetoothA2dp;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2311
    const-string v0, "TMSBtAppholder"

    const-string v1, " A2dpProfileManager : A2dpListener :Service Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2312
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    invoke-static {v0, p2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothA2dp;)V

    .line 2314
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    if-eqz v0, :cond_1

    .line 2315
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->onStateChnaged()V

    .line 2317
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 2322
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " A2dpProfileManager : A2dpListener :onServiceDisconnected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2323
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    if-eqz v0, :cond_0

    .line 2324
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->mA2dpProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->onStateChnaged()V

    .line 2326
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothA2dp;)V

    .line 2327
    return-void
.end method
