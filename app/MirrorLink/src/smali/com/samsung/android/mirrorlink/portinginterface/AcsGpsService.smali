.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
.super Ljava/lang/Object;
.source "AcsGpsService.java"


# static fields
.field public static final SUBSCRIPTION_AUTOMATIC:I = 0x3

.field public static final SUBSCRIPTION_INTERVAL_DEFAULT:I = 0xea60

.field public static final SUBSCRIPTION_ONCHANGE:I = 0x2

.field public static final SUBSCRIPTION_REGULAR:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TMSGpsSvc"


# instance fields
.field private isGpsRunning:Z

.field private location:Landroid/location/LocationManager;

.field private mAccuracy:F

.field private mAltitude:D

.field private mBearing:F

.field private mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

.field private mGetObjSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLat:D

.field private mLatDirIdx:I

.field private mLatIdx:I

.field private mLng:D

.field private mLngDirIdx:I

.field private mLngIdx:I

.field private mLocListener:Landroid/location/LocationListener;

.field private mMinTime:J

.field private mNMEADesc:I

.field private mNMEAdata:Ljava/lang/String;

.field private mNMEAtimestamp:J

.field private mProvider:Ljava/lang/String;

.field private mProviderState:I

.field private mSpeed:F

.field private mSubscribeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/location/LocationManager;)V
    .locals 8
    .param p1, "loc"    # Landroid/location/LocationManager;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, -0x40800000    # -1.0f

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    const/4 v3, -0x1

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    .line 51
    const-string v0, "CARGPS"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    .line 69
    iput-boolean v7, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->isGpsRunning:Z

    .line 78
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.AcsGpsService() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 83
    const-string v0, "TMSGpsSvc"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :goto_0
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatIdx:I

    .line 91
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngIdx:I

    .line 92
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatDirIdx:I

    .line 93
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngDirIdx:I

    .line 94
    iput v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    .line 95
    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    .line 96
    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    .line 97
    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    .line 98
    iput v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    .line 99
    iput v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    .line 101
    iput-boolean v7, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->isGpsRunning:Z

    .line 103
    iput v7, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    .line 104
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService$1;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLocListener:Landroid/location/LocationListener;

    .line 123
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mMinTime:J

    .line 125
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    .line 126
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    .line 127
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.AcsGpsService() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void

    .line 87
    :cond_0
    const-string v0, "TMSGpsSvc"

    const-string v1, "location obj is valid "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "location providers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    invoke-virtual {v2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseLatOrLongVal(Ljava/lang/String;Z)D
    .locals 9
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "positive"    # Z

    .prologue
    .line 385
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    .line 386
    .local v1, "val":D
    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    div-double v5, v1, v5

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    div-double v7, v1, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-double v7, v7

    sub-double/2addr v5, v7

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v7

    const-wide/high16 v7, 0x404e000000000000L    # 60.0

    div-double v3, v5, v7

    .line 387
    .local v3, "valPost":D
    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    div-double v5, v1, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-double v5, v5

    add-double v1, v5, v3

    .line 389
    if-nez p2, :cond_0

    .line 390
    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    mul-double/2addr v1, v5

    .line 392
    :cond_0
    const-wide v5, 0x4197d78400000000L    # 1.0E8

    mul-double/2addr v5, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    long-to-double v5, v5

    const-wide v7, 0x4197d78400000000L    # 1.0E8

    div-double v1, v5, v7

    .line 402
    .end local v1    # "val":D
    .end local v3    # "valPost":D
    :goto_0
    return-wide v1

    .line 396
    :catch_0
    move-exception v0

    .line 398
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const-string v5, "TMSGpsSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "NumberFormatException"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-wide/high16 v1, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method private parseNmea()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/high16 v10, -0x40800000    # -1.0f

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 228
    const-string v4, "TMSGpsSvc"

    const-string v5, "AcsGpsService.parseNmea - parsing nmea message"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iput v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatIdx:I

    .line 230
    iput v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngIdx:I

    .line 231
    iput v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatDirIdx:I

    .line 232
    iput v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngDirIdx:I

    .line 233
    iput v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    .line 234
    iput-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    .line 235
    iput-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    .line 236
    iput-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    .line 237
    iput v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    .line 238
    iput v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    .line 240
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAdata:Ljava/lang/String;

    const-string v5, "$"

    invoke-direct {v2, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    .local v2, "st":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-nez v4, :cond_1

    .line 299
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->setLocation()V

    .line 305
    return-void

    .line 242
    :cond_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 243
    .local v0, "mesg":Ljava/lang/String;
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Parsing:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "words":[Ljava/lang/String;
    aget-object v4, v3, v9

    const-string v5, "GPRMC"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 247
    const-string v4, "TMSGpsSvc"

    const-string v5, "words[0]: is GPRMC"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    aget-object v4, v3, v11

    if-eqz v4, :cond_2

    aget-object v4, v3, v11

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 249
    const/4 v4, 0x5

    aget-object v4, v3, v4

    if-eqz v4, :cond_2

    const/4 v4, 0x5

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 251
    iput v11, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatIdx:I

    .line 252
    const/4 v4, 0x4

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatDirIdx:I

    .line 253
    const/4 v4, 0x5

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngIdx:I

    .line 254
    const/4 v4, 0x6

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngDirIdx:I

    .line 256
    :cond_2
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatIdx:I

    if-eq v4, v8, :cond_3

    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngIdx:I

    if-eq v4, v8, :cond_3

    .line 258
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatIdx:I

    aget-object v4, v3, v4

    iget v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLatDirIdx:I

    aget-object v5, v3, v5

    const-string v6, "N"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->parseLatOrLongVal(Ljava/lang/String;Z)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    .line 259
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngIdx:I

    aget-object v4, v3, v4

    iget v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLngDirIdx:I

    aget-object v5, v3, v5

    const-string v6, "E"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->parseLatOrLongVal(Ljava/lang/String;Z)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    .line 260
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mLat:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mLng"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_3
    const/16 v4, 0x8

    aget-object v4, v3, v4

    if-eqz v4, :cond_0

    const/16 v4, 0x8

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 264
    const/16 v4, 0x8

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    .line 265
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mBearing:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    :cond_4
    aget-object v4, v3, v9

    const-string v5, "GPVTG"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 270
    const-string v4, "TMSGpsSvc"

    const-string v5, "words[0]: is GPVTG"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v4, 0x7

    aget-object v4, v3, v4

    if-eqz v4, :cond_0

    const/4 v4, 0x7

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 273
    const/4 v4, 0x7

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    .line 274
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "speedKmPh:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const v1, 0x3e8e38e4

    .line 276
    .local v1, "mul":F
    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    mul-float/2addr v4, v1

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    .line 277
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "speedmPs:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 280
    .end local v1    # "mul":F
    :cond_5
    aget-object v4, v3, v9

    const-string v5, "GPGGA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 282
    const-string v4, "TMSGpsSvc"

    const-string v5, "words[0]: is GPGGA"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/16 v4, 0x9

    aget-object v4, v3, v4

    if-eqz v4, :cond_0

    const/16 v4, 0x9

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 285
    const/16 v4, 0x9

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    .line 286
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "altitude:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :cond_6
    aget-object v4, v3, v9

    const-string v5, "GPGSA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 291
    const-string v4, "TMSGpsSvc"

    const-string v5, "words[0]: is GPGSA"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const/16 v4, 0xf

    aget-object v4, v3, v4

    if-eqz v4, :cond_0

    const/16 v4, 0xf

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 294
    const/16 v4, 0xf

    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    .line 295
    const-string v4, "TMSGpsSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "accuracy:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setLocation()V
    .locals 13

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->isGpsRunning:Z

    if-nez v0, :cond_0

    .line 465
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-nez v0, :cond_1

    .line 410
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 411
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    .line 414
    :cond_1
    new-instance v12, Landroid/location/Location;

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-direct {v12, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 415
    .local v12, "loc":Landroid/location/Location;
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 416
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_2

    .line 417
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 419
    :cond_2
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 420
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 422
    :cond_3
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    .line 423
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSpeed:F

    invoke-virtual {v12, v0}, Landroid/location/Location;->setSpeed(F)V

    .line 425
    :cond_4
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    .line 426
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mBearing:F

    invoke-virtual {v12, v0}, Landroid/location/Location;->setBearing(F)V

    .line 428
    :cond_5
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_6

    .line 429
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAltitude:D

    invoke-virtual {v12, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    .line 431
    :cond_6
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_7

    .line 432
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mAccuracy:F

    invoke-virtual {v12, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 435
    :cond_7
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_8

    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_b

    .line 436
    :cond_8
    const-string v0, "TMSGpsSvc"

    const-string v1, "CARGPS - rxed invalid location"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    .line 439
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    .line 440
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->setTestProviderStatus(Ljava/lang/String;ILandroid/os/Bundle;J)V

    .line 443
    :cond_9
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1, v12}, Landroid/location/LocationManager;->setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    :cond_a
    :goto_1
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setLocation() - updated location information"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 444
    :catch_0
    move-exception v11

    .line 445
    .local v11, "ie":Ljava/lang/IllegalArgumentException;
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setLocation() - Rxed incomplete location details"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    .end local v11    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_b
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLat:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLng:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_a

    .line 451
    const-string v0, "TMSGpsSvc"

    const-string v1, "CARGPS - rxed valid location"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_c

    .line 454
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    .line 455
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProviderState:I

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->setTestProviderStatus(Ljava/lang/String;ILandroid/os/Bundle;J)V

    .line 458
    :cond_c
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1, v12}, Landroid/location/LocationManager;->setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 459
    :catch_1
    move-exception v11

    .line 460
    .restart local v11    # "ie":Ljava/lang/IllegalArgumentException;
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setLocation() - Rxed incomplete location details"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public deInitGps()V
    .locals 2

    .prologue
    .line 182
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.initGpsService() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    .line 184
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.initGpsService() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public getNmeaData(I)V
    .locals 2
    .param p1, "objectId"    # I

    .prologue
    .line 580
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 581
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getNmeaGps()V

    .line 582
    return-void
.end method

.method public getNmeaDescription(I)V
    .locals 2
    .param p1, "objectId"    # I

    .prologue
    .line 598
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 599
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getNmeaDescription()V

    .line 600
    return-void
.end method

.method public getObject(I)V
    .locals 1
    .param p1, "objectId"    # I

    .prologue
    .line 512
    const v0, 0xaac4540

    if-ne p1, v0, :cond_1

    .line 513
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getNmeaData(I)V

    .line 517
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    const v0, -0x62f74e63

    if-ne p1, v0, :cond_0

    .line 515
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getNmeaDescription(I)V

    goto :goto_0
.end method

.method public getResponseForGpsNmeaData()V
    .locals 6

    .prologue
    const v5, 0xaac4540

    .line 585
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v1

    .line 586
    .local v1, "dataConnMngr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v1, :cond_0

    .line 587
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 588
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "0x144a776f"

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAdata:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v2, "0x59413fd1"

    iget-wide v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 590
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v2

    .line 592
    const/4 v3, 0x1

    .line 590
    invoke-virtual {v1, v2, v5, v3, v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 594
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 595
    return-void
.end method

.method public getresponseForGpsNmeaDesc()V
    .locals 5

    .prologue
    const v4, -0x62f74e63

    .line 603
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v1

    .line 604
    .local v1, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v1, :cond_0

    .line 605
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 606
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "0x6e72b167"

    iget v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEADesc:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 607
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v2

    .line 609
    const/4 v3, 0x1

    .line 607
    invoke-virtual {v1, v2, v4, v3, v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 611
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 612
    return-void
.end method

.method public initGps()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.initGpsService() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 135
    const-string v0, "TMSGpsSvc"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_0
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.initGpsService() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public responseForGpsNmeaData(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "result"    # I

    .prologue
    const v5, 0xea60

    const/4 v4, 0x1

    const v2, 0xaac4540

    .line 526
    if-nez p2, :cond_1

    move v3, v4

    .line 528
    .local v3, "flag":Z
    :goto_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    .line 530
    .local v0, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    const-string v1, "TMSGpsSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "responseForGpsNmeaData: type :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    sparse-switch p1, :sswitch_data_0

    .line 576
    :cond_0
    :goto_1
    return-void

    .line 526
    .end local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .end local v3    # "flag":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 534
    .restart local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .restart local v3    # "flag":Z
    :sswitch_0
    if-eqz v0, :cond_0

    .line 536
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyRegisterForService(IZ)V

    goto :goto_1

    .line 540
    :sswitch_1
    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyRegisterForService(IZ)V

    goto :goto_1

    .line 545
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    if-eqz v0, :cond_2

    .line 549
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    .line 552
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 556
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 558
    if-eqz v0, :cond_3

    .line 559
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    .line 560
    const/4 v4, 0x0

    .line 559
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 562
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 566
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    goto :goto_1

    .line 531
    nop

    :sswitch_data_0
    .sparse-switch
        0xb1 -> :sswitch_3
        0xb3 -> :sswitch_4
        0xb4 -> :sswitch_2
        0xb103 -> :sswitch_1
        0xb104 -> :sswitch_0
    .end sparse-switch
.end method

.method public responseForGpsNmeaDesc(II)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "result"    # I

    .prologue
    const v5, 0xea60

    const/4 v4, 0x1

    const v2, -0x62f74e63

    .line 626
    const-string v1, "TMSGpsSvc"

    const-string v6, "AcsGpsService. responseForGpsNmeaDesc"

    invoke-static {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    if-nez p2, :cond_1

    move v3, v4

    .line 629
    .local v3, "flag":Z
    :goto_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    .line 630
    .local v0, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v0, :cond_0

    .line 631
    const/16 v1, 0xb4

    if-ne p1, v1, :cond_2

    .line 632
    const-string v1, "TMSGpsSvc"

    const-string v6, "AcsGpsService.notifySubscribeResponse for NMEADesc: AcsDeviceMngr.SBP_CANCEL"

    invoke-static {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    .line 635
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 649
    :cond_0
    :goto_1
    return-void

    .line 628
    .end local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .end local v3    # "flag":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 637
    .restart local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .restart local v3    # "flag":Z
    :cond_2
    const/16 v1, 0xb2

    if-ne p1, v1, :cond_3

    .line 639
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySetDataObjectResponse(IIZ)V

    goto :goto_1

    .line 642
    :cond_3
    const/16 v1, 0xb3

    if-ne p1, v1, :cond_0

    .line 643
    const-string v1, "TMSGpsSvc"

    const-string v6, "AcsGpsService.notifySubscribeResponse for NMEADesc: AcsDeviceMngr.SBP_SUBSCRIBE"

    invoke-static {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    .line 646
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public setDeviceMngr(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 2
    .param p1, "devMgr"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .prologue
    .line 188
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setDeviceMngr() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 192
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setDeviceMngr() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method public setGpsNMEA(Ljava/lang/String;J)V
    .locals 4
    .param p1, "nmea"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 205
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setGpsNMEA() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAdata:Ljava/lang/String;

    .line 208
    iput-wide p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    .line 209
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService.setGpsNMEA(): NMEAdata"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAdata:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", NMEAtimestamp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setGpsNMEA() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method public setGpsNMEADesc(I)V
    .locals 3
    .param p1, "nmeaDesc"    # I

    .prologue
    .line 216
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setGpsNMEADesc() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEADesc:I

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getresponseForGpsNmeaDesc()V

    .line 220
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService.setGpsNMEADesc(): NMEAdata"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEADesc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setGpsNMEADesc() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public setMinTime(J)V
    .locals 6
    .param p1, "time"    # J

    .prologue
    .line 196
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setMinTime() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mMinTime:J

    .line 199
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mMinTime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 201
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.setMinTime() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public setObject(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "objectId"    # I
    .param p2, "object2"    # Landroid/os/Bundle;

    .prologue
    .line 616
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    .line 617
    .local v0, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v0, :cond_0

    .line 618
    const-string v1, "TMSGpsSvc"

    const-string v2, "Set object is not supported. Hence returning false"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getGpsServiceID()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySetDataObjectResponse(IIZ)V

    .line 623
    :cond_0
    return-void
.end method

.method public startGps()Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 143
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.startGps() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 146
    const-string v0, "TMSGpsSvc"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :goto_0
    return v2

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-nez v0, :cond_1

    .line 151
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.startGps() - getProvider is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    move v3, v2

    move v4, v2

    move v5, v2

    move v7, v6

    move v8, v6

    move v9, v2

    move v10, v2

    invoke-virtual/range {v0 .. v10}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 153
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    .line 156
    :cond_1
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.startGps() - requesting location updates"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mMinTime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 158
    iput-boolean v6, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->isGpsRunning:Z

    .line 160
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.startGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v6

    .line 161
    goto :goto_0
.end method

.method public stopGps()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 165
    const-string v1, "TMSGpsSvc"

    const-string v2, "AcsGpsService.stopGps() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->isGpsRunning:Z

    .line 168
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    if-nez v1, :cond_0

    .line 170
    const-string v1, "TMSGpsSvc"

    const-string v2, "location obj is null "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    return v0

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->location:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->clearTestProviderEnabled(Ljava/lang/String;)V

    .line 177
    :cond_1
    const-string v0, "TMSGpsSvc"

    const-string v1, "AcsGpsService.stopGps() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subScribeRequest(I)V
    .locals 3
    .param p1, "objectId"    # I

    .prologue
    .line 487
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService: subScribeRequest :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    const v0, 0xaac4540

    if-eq p1, v0, :cond_0

    .line 490
    const v0, -0x62f74e63

    if-ne p1, v0, :cond_1

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 492
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    const/4 v1, 0x1

    .line 493
    const v2, 0xea60

    .line 492
    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->subscribeGps(II)V

    .line 499
    :goto_0
    return-void

    .line 496
    :cond_1
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService: Invalid object id :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unsubScribeRequest(I)V
    .locals 3
    .param p1, "objectId"    # I

    .prologue
    .line 502
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService: unsubScribeRequest :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mSubscribeSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->cancelSubscribeGps()V

    .line 509
    :goto_0
    return-void

    .line 507
    :cond_0
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService: Invalid object id :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateNmea(Ljava/lang/String;J)V
    .locals 2
    .param p1, "nmea"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 469
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAdata:Ljava/lang/String;

    .line 470
    iput-wide p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEAtimestamp:J

    .line 471
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->parseNmea()V

    .line 473
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mGetObjSet:Ljava/util/Set;

    const v1, 0xaac4540

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getResponseForGpsNmeaData()V

    .line 476
    :cond_0
    return-void
.end method

.method public updateNmeaDesc(I)V
    .locals 3
    .param p1, "nmeaDesc"    # I

    .prologue
    .line 480
    const-string v0, "TMSGpsSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateNmeaDesc: nmeaDesc :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->mNMEADesc:I

    .line 482
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->getresponseForGpsNmeaDesc()V

    .line 483
    return-void
.end method
