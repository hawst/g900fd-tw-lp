.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;
.super Ljava/lang/Object;
.source "AcsVoiceCallRx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;,
        Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;
    }
.end annotation


# static fields
.field private static final BYTES_READ_PER_FRAME:I = 0x800

.field public static final EVENT_AUDIO_DATA:I = 0x3ec

.field private static final SAMPLING_RATE:I = 0xac44

.field private static final TAG:Ljava/lang/String; = "TMSVoiceCallRx"


# instance fields
.field private mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

.field private mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

.field private mCxt:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    .line 39
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    .line 47
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "AcsVoiceCallRx:Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCxt:Landroid/content/Context;

    .line 49
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "AcsVoiceCallRx:Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;)Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    return-object v0
.end method


# virtual methods
.method public start(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;)Z
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    .prologue
    .line 65
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "start:Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    if-nez v0, :cond_0

    .line 69
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    .line 70
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    .line 71
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->startThread()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V

    .line 75
    :cond_0
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "start:Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public stop()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "stop: Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    if-nez v0, :cond_0

    .line 89
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "stopThread (mCallRecordThread == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->stopThread()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;->access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;)V

    .line 95
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallRecordThread:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$CallRecordThread;

    .line 97
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx;->mCallback:Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;

    .line 99
    const-string v0, "TMSVoiceCallRx"

    const-string v1, "stop: Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x1

    goto :goto_0
.end method
