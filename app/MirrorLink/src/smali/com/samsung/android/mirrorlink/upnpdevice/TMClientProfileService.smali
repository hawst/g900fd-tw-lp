.class public Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
.super Ljava/lang/Object;
.source "TMClientProfileService.java"

# interfaces
.implements Lcom/samsung/upnp/control/ActionListener;
.implements Lcom/samsung/upnp/control/QueryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSClientProSvc"

.field private static final SUPPORTED_MAJOR_VERSION:I = 0x1

.field private static final SUPPORTED_MINOR_VERSION:I = 0x1

.field private static final TM_RTP_IPL:I = 0x12c0

.field private static final TM_RTP_MPL:I = 0x2580

.field public static mClientProfilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mClientProfileEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IClientProfileEventsListener;

.field private mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

.field private mContext:Landroid/content/Context;

.field private mDevInfoMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

.field private mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mKeyMngr:Landroid/app/KeyguardManager;

.field private mMaxNumProfiles:I

.field private mSessionEstablished:Z

.field private mUsedProfile:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)V
    .locals 2
    .param p1, "myDeviceAppManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    .line 83
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 98
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.TMClientProfileService enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 100
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mContext:Landroid/content/Context;

    .line 101
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    .line 103
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    .line 115
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->setDefaultClientProfiles()V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mContext:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 122
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.TMClientProfileService exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method private fillupDefaultCPValues()Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .locals 5

    .prologue
    const/16 v4, 0x78

    const/4 v3, 0x0

    .line 863
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;-><init>()V

    .line 864
    .local v0, "clientprofileNodeDeflt":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 865
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getServerSupportedRtpPayloads()Ljava/util/ArrayList;

    move-result-object v1

    .line 866
    .local v1, "receivedServerSupportedRtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object v3, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    .line 867
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    .line 868
    const-string v2, "unknown"

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    .line 869
    const-string v2, "unknown"

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    .line 870
    const/16 v2, 0x20

    iput v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    .line 871
    iput v4, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    .line 872
    const-string v2, "image/png"

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_mimetype:Ljava/lang/String;

    .line 873
    iput v4, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    .line 874
    iput-object v3, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    .line 875
    iput-object v3, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    .line 876
    iput-object v3, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    .line 877
    if-eqz v1, :cond_0

    .line 878
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    .line 879
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 881
    :cond_0
    const/16 v2, 0x12c0

    iput v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    .line 882
    const/16 v2, 0x2580

    iput v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    .line 883
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->createAppIDList()I

    .line 885
    return-object v0
.end method

.method private findUnUsedProfiles()Ljava/lang/String;
    .locals 5

    .prologue
    .line 290
    const-string v2, "TMSClientProSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMClientProfileService.findUnUsedProfiles enter mUsedProfile = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/4 v1, 0x0

    .line 292
    .local v1, "unUSed":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    if-lt v0, v2, :cond_0

    .line 310
    const-string v2, "TMSClientProSvc"

    const-string v3, "TMClientProfileService.findUnUsedProfiles exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    return-object v1

    .line 294
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 296
    if-nez v1, :cond_2

    .line 298
    const-string v2, "TMSClientProSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMClientProfileService.findUnUsedProfiles found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not used till now.Create new list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 303
    :cond_2
    const-string v2, "TMSClientProSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMClientProfileService.findUnUsedProfiles found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not used till now"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const-string v2, "TMSClientProSvc"

    const-string v3, "TMClientProfileService.findUnUsedProfiles exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private formClientProfileXml(I)Ljava/lang/String;
    .locals 28
    .param p1, "profileID"    # I

    .prologue
    .line 899
    const-string v25, "TMSClientProSvc"

    const-string v26, "TMClientProfileService.formClientProfileXml enter"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const/16 v16, 0x0

    .line 902
    .local v16, "profileNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    if-lt v7, v0, :cond_0

    .line 910
    const/4 v6, 0x0

    .line 912
    .local v6, "finalCP":Ljava/lang/String;
    if-nez v16, :cond_2

    .line 914
    const/16 v25, 0x0

    .line 1126
    :goto_1
    return-object v25

    .line 904
    .end local v6    # "finalCP":Ljava/lang/String;
    :cond_0
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v25, v0

    move/from16 v0, v25

    move/from16 v1, p1

    if-ne v0, v1, :cond_1

    .line 906
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "profileNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    check-cast v16, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 907
    .restart local v16    # "profileNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfileXml found profile with id "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " and client id "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 918
    .restart local v6    # "finalCP":Ljava/lang/String;
    :cond_2
    new-instance v5, Lcom/samsung/xml/Node;

    const-string v25, "clientProfile"

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 926
    .local v5, "clientProfile":Lcom/samsung/xml/Node;
    new-instance v22, Lcom/samsung/xml/Node;

    const-string v25, "clientID"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 927
    .local v22, "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 928
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 930
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_3

    .line 932
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "friendlyName"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 933
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 934
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 937
    :cond_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    .line 939
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "manufacturer"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 940
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 941
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 944
    :cond_4
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_5

    .line 946
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "modelName"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 947
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 948
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 950
    :cond_5
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    .line 952
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "modelNumber"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 953
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 954
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 957
    :cond_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_mimetype:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_7

    .line 959
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "iconPreference"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 960
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    new-instance v23, Lcom/samsung/xml/Node;

    const-string v25, "mimetype"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 961
    .local v23, "tempNode1":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_mimetype:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 962
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 965
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    :cond_7
    const/16 v25, -0x1

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_8

    .line 967
    new-instance v23, Lcom/samsung/xml/Node;

    const-string v25, "width"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 968
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 969
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 972
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    :cond_8
    const/16 v25, -0x1

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_9

    .line 974
    new-instance v23, Lcom/samsung/xml/Node;

    const-string v25, "height"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 975
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 976
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 979
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    :cond_9
    const/16 v25, -0x1

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_a

    .line 981
    new-instance v23, Lcom/samsung/xml/Node;

    const-string v25, "depth"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 982
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 983
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 984
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 987
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    :cond_a
    new-instance v22, Lcom/samsung/xml/Node;

    .end local v22    # "tempNode":Lcom/samsung/xml/Node;
    const-string v25, "connectivity"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 990
    .restart local v22    # "tempNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_b

    .line 992
    new-instance v23, Lcom/samsung/xml/Node;

    const-string v25, "bluetooth"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 993
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    new-instance v24, Lcom/samsung/xml/Node;

    const-string v25, "bdAddr"

    invoke-direct/range {v24 .. v25}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 994
    .local v24, "tempNode2":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 995
    invoke-virtual/range {v23 .. v24}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 996
    new-instance v24, Lcom/samsung/xml/Node;

    .end local v24    # "tempNode2":Lcom/samsung/xml/Node;
    const-string v25, "startConnection"

    invoke-direct/range {v24 .. v25}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 997
    .restart local v24    # "tempNode2":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    move/from16 v25, v0

    if-eqz v25, :cond_12

    .line 999
    const-string v25, "true"

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1005
    :goto_2
    invoke-virtual/range {v23 .. v24}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1006
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1008
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    .end local v24    # "tempNode2":Lcom/samsung/xml/Node;
    :cond_b
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1010
    new-instance v18, Lcom/samsung/xml/Node;

    const-string v25, "rtpStreaming"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1011
    .local v18, "rtpStreamingNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-eqz v25, :cond_c

    .line 1013
    new-instance v15, Lcom/samsung/xml/Node;

    const-string v25, "payloadType"

    move-object/from16 v0, v25

    invoke-direct {v15, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1014
    .local v15, "payloadTypeNode":Lcom/samsung/xml/Node;
    const/16 v17, 0x0

    .line 1015
    .local v17, "pyldStr":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    move/from16 v0, v25

    if-lt v9, v0, :cond_13

    .line 1029
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1030
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1033
    .end local v9    # "j":I
    .end local v15    # "payloadTypeNode":Lcom/samsung/xml/Node;
    .end local v17    # "pyldStr":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    move/from16 v25, v0

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_d

    .line 1035
    new-instance v3, Lcom/samsung/xml/Node;

    const-string v25, "audioIPL"

    move-object/from16 v0, v25

    invoke-direct {v3, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1036
    .local v3, "audioIplNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1037
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1038
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfileXml mAudioIplvalue is "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    .end local v3    # "audioIplNode":Lcom/samsung/xml/Node;
    :cond_d
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    move/from16 v25, v0

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_e

    .line 1042
    new-instance v4, Lcom/samsung/xml/Node;

    const-string v25, "audioMPL"

    move-object/from16 v0, v25

    invoke-direct {v4, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1043
    .local v4, "audioMplNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1044
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1045
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfileXml mAudioMpl is "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    .end local v4    # "audioMplNode":Lcom/samsung/xml/Node;
    :cond_e
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v25

    if-eqz v25, :cond_f

    .line 1050
    const-string v25, "TMSClientProSvc"

    const-string v26, "TMClientProfileService.formClientProfileXml adding rtpstreaming node"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1054
    :cond_f
    const/16 v22, 0x0

    .line 1055
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRules:Ljava/util/Map;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v21

    .line 1056
    .local v21, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 1057
    .local v8, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_15

    .line 1083
    if-eqz v22, :cond_10

    .line 1084
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1086
    :cond_10
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    if-eqz v25, :cond_11

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->bRecvdClientSettings:Z

    move/from16 v25, v0

    if-eqz v25, :cond_11

    .line 1088
    const-string v25, "TMSClientProSvc"

    .line 1089
    const-string v26, "TMClientProfileService.formClientProfileXml proceed to add notification nodes"

    .line 1088
    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    new-instance v20, Lcom/samsung/xml/Node;

    const-string v25, "services"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1091
    .local v20, "servNode":Lcom/samsung/xml/Node;
    new-instance v12, Lcom/samsung/xml/Node;

    const-string v25, "notification"

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1092
    .local v12, "notiNode":Lcom/samsung/xml/Node;
    new-instance v13, Lcom/samsung/xml/Node;

    const-string v25, "notiUiSupport"

    move-object/from16 v0, v25

    invoke-direct {v13, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1093
    .local v13, "notiSupportNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiUiSupport:Z

    move/from16 v25, v0

    if-eqz v25, :cond_18

    .line 1094
    const-string v25, "true"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1098
    :goto_5
    invoke-virtual {v12, v13}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1100
    new-instance v10, Lcom/samsung/xml/Node;

    const-string v25, "maxActions"

    move-object/from16 v0, v25

    invoke-direct {v10, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1101
    .local v10, "maxActNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mMaxActions:I

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v10, v0}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 1102
    invoke-virtual {v12, v10}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1104
    new-instance v2, Lcom/samsung/xml/Node;

    const-string v25, "actionNameMaxLength"

    move-object/from16 v0, v25

    invoke-direct {v2, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1105
    .local v2, "actNameMaxLenNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    .line 1106
    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mActionMaxLen:I

    move/from16 v25, v0

    .line 1105
    move/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 1107
    invoke-virtual {v12, v2}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1109
    new-instance v14, Lcom/samsung/xml/Node;

    const-string v25, "notiTitleMaxLength"

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1110
    .local v14, "notiTitleMaxLen":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    .line 1111
    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiTitleMaxLen:I

    move/from16 v25, v0

    .line 1110
    move/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 1112
    invoke-virtual {v12, v14}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1114
    new-instance v11, Lcom/samsung/xml/Node;

    const-string v25, "notiBodyMaxLength"

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1115
    .local v11, "notiBodyMaxLen":Lcom/samsung/xml/Node;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v25, v0

    .line 1116
    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiBodyMaxLen:I

    move/from16 v25, v0

    .line 1115
    move/from16 v0, v25

    invoke-virtual {v11, v0}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 1117
    invoke-virtual {v12, v11}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1119
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1120
    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1124
    .end local v2    # "actNameMaxLenNode":Lcom/samsung/xml/Node;
    .end local v10    # "maxActNode":Lcom/samsung/xml/Node;
    .end local v11    # "notiBodyMaxLen":Lcom/samsung/xml/Node;
    .end local v12    # "notiNode":Lcom/samsung/xml/Node;
    .end local v13    # "notiSupportNode":Lcom/samsung/xml/Node;
    .end local v14    # "notiTitleMaxLen":Lcom/samsung/xml/Node;
    .end local v20    # "servNode":Lcom/samsung/xml/Node;
    :cond_11
    invoke-virtual {v5}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1125
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfileXml exit clinetProfile = "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v25, v6

    .line 1126
    goto/16 :goto_1

    .line 1003
    .end local v8    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    .end local v18    # "rtpStreamingNode":Lcom/samsung/xml/Node;
    .end local v21    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    .restart local v24    # "tempNode2":Lcom/samsung/xml/Node;
    :cond_12
    const-string v25, "false"

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1017
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    .end local v24    # "tempNode2":Lcom/samsung/xml/Node;
    .restart local v9    # "j":I
    .restart local v15    # "payloadTypeNode":Lcom/samsung/xml/Node;
    .restart local v17    # "pyldStr":Ljava/lang/String;
    .restart local v18    # "rtpStreamingNode":Lcom/samsung/xml/Node;
    :cond_13
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfileXml profileNode.profileNode.mRtpPayloads.size() = "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    const-string v26, "TMSClientProSvc"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v25, "TMClientProfileService.formClientProfileXml profileNode.mRtpPayloads = "

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    if-nez v17, :cond_14

    .line 1021
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1027
    :goto_6
    const-string v25, "TMSClientProSvc"

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "TMClientProfileService.formClientProfile pyldStr = "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    .line 1025
    :cond_14
    new-instance v26, Ljava/lang/StringBuilder;

    const-string v25, ","

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    goto :goto_6

    .line 1060
    .end local v9    # "j":I
    .end local v15    # "payloadTypeNode":Lcom/samsung/xml/Node;
    .end local v17    # "pyldStr":Ljava/lang/String;
    .restart local v8    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    .restart local v21    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;>;"
    :cond_15
    const-string v25, "rule"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v23

    .line 1061
    .restart local v23    # "tempNode1":Lcom/samsung/xml/Node;
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 1063
    .local v19, "rule":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    const-string v25, "ruleId"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v24

    .line 1064
    .restart local v24    # "tempNode2":Lcom/samsung/xml/Node;
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1065
    invoke-virtual/range {v23 .. v24}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1066
    const-string v25, "empty"

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_16

    .line 1068
    const-string v25, "ruleValue"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v24

    .line 1069
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1070
    invoke-virtual/range {v23 .. v24}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1076
    :cond_16
    if-nez v22, :cond_17

    if-eqz v23, :cond_17

    .line 1078
    const-string v25, "contentRules"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v22

    .line 1080
    :cond_17
    invoke-virtual/range {v22 .. v23}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto/16 :goto_4

    .line 1096
    .end local v19    # "rule":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v23    # "tempNode1":Lcom/samsung/xml/Node;
    .end local v24    # "tempNode2":Lcom/samsung/xml/Node;
    .restart local v12    # "notiNode":Lcom/samsung/xml/Node;
    .restart local v13    # "notiSupportNode":Lcom/samsung/xml/Node;
    .restart local v20    # "servNode":Lcom/samsung/xml/Node;
    :cond_18
    const-string v25, "false"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 836
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public static getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .locals 6
    .param p0, "i"    # I

    .prologue
    const/4 v3, 0x0

    .line 1141
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    .line 1142
    .local v2, "profileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 1143
    :cond_0
    const-string v4, "TMSClientProSvc"

    const-string v5, "Profile list is null or size is zero"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 1156
    :cond_1
    :goto_0
    return-object v1

    .line 1146
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1148
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1150
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 1151
    .local v1, "node":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget v4, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    if-eq v4, p0, :cond_1

    .end local v1    # "node":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    :cond_3
    move-object v1, v3

    .line 1156
    goto :goto_0
.end method

.method private getMaxNumProfiles()I
    .locals 2

    .prologue
    .line 350
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.getMaxNumProfiles"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    return v0
.end method

.method private getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1130
    new-instance v0, Lcom/samsung/xml/Node;

    invoke-direct {v0, p1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getStringTokenizer(Lcom/samsung/xml/Node;)Ljava/util/StringTokenizer;
    .locals 3
    .param p1, "nod"    # Lcom/samsung/xml/Node;

    .prologue
    .line 840
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private setClientProfile(Lcom/samsung/upnp/Action;)I
    .locals 49
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 363
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile enter"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const/4 v9, 0x0

    .line 365
    .local v9, "clientProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const/16 v39, -0x1

    .line 367
    .local v39, "profileID":I
    const-string v44, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 368
    .local v5, "arg":Lcom/samsung/upnp/Argument;
    if-nez v5, :cond_0

    .line 370
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile failed No ProfileID"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 372
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 371
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 373
    const/16 v44, -0x1

    .line 831
    :goto_0
    return v44

    .line 376
    :cond_0
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v39

    .line 378
    if-ltz v39, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    move/from16 v44, v0

    move/from16 v0, v39

    move/from16 v1, v44

    if-lt v0, v1, :cond_2

    .line 380
    :cond_1
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 381
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 380
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 383
    const/16 v44, -0x1

    goto :goto_0

    .line 386
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_3

    .line 388
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->size()I

    move-result v44

    move/from16 v0, v17

    move/from16 v1, v44

    if-lt v0, v1, :cond_5

    .line 400
    .end local v17    # "i":I
    :cond_3
    :goto_2
    if-nez v9, :cond_4

    .line 402
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile adding new client profile with id ="

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->fillupDefaultCPValues()Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v9

    .line 404
    move/from16 v0, v39

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    .line 405
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    iput-object v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    .line 407
    :cond_4
    const/4 v13, 0x0

    .line 409
    .local v13, "cpValudeNode":Lcom/samsung/xml/Node;
    const-string v44, "ClientProfile"

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v44

    if-eqz v44, :cond_10

    .line 411
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/Action;->getActionRequest()Lcom/samsung/upnp/control/ActionRequest;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/upnp/control/ActionRequest;->getActionNode()Lcom/samsung/xml/Node;

    move-result-object v3

    .line 412
    .local v3, "actionNode":Lcom/samsung/xml/Node;
    if-eqz v3, :cond_b

    .line 417
    const-string v44, "ClientProfile"

    move-object/from16 v0, v44

    invoke-virtual {v3, v0}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 419
    .local v10, "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    if-nez v10, :cond_7

    .line 422
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 423
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 421
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 424
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 390
    .end local v3    # "actionNode":Lcom/samsung/xml/Node;
    .end local v10    # "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    .end local v13    # "cpValudeNode":Lcom/samsung/xml/Node;
    .restart local v17    # "i":I
    :cond_5
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v44, v0

    move/from16 v0, v44

    move/from16 v1, v39

    if-ne v0, v1, :cond_6

    .line 392
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "clientProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    check-cast v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 393
    .restart local v9    # "clientProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    move/from16 v0, v45

    move-object/from16 v1, v44

    iput-boolean v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->bRecvdClientSettings:Z

    goto/16 :goto_2

    .line 388
    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 426
    .end local v17    # "i":I
    .restart local v3    # "actionNode":Lcom/samsung/xml/Node;
    .restart local v10    # "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    .restart local v13    # "cpValudeNode":Lcom/samsung/xml/Node;
    :cond_7
    invoke-virtual {v10}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 427
    .local v8, "cPValue":Ljava/lang/String;
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v44

    if-nez v44, :cond_9

    .line 431
    :cond_8
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 432
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 429
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 433
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 436
    :cond_9
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile cPValue = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-static {}, Lcom/samsung/soap/SOAP;->getXMLParser()Lcom/samsung/xml/Parser;

    move-result-object v37

    .line 439
    .local v37, "parser":Lcom/samsung/xml/Parser;
    :try_start_0
    const-string v44, "?xml"

    move-object/from16 v0, v44

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v44

    if-eqz v44, :cond_c

    .line 440
    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 441
    const-string v44, "clientProfile"

    invoke-virtual {v13}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_a

    .line 444
    const-string v44, "clientProfile"

    move-object/from16 v0, v44

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 457
    :cond_a
    :goto_3
    if-eqz v13, :cond_e

    .line 459
    const-string v44, "clientProfile"

    invoke-virtual {v13}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_d

    .line 461
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile  found proper xml"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    .end local v8    # "cPValue":Ljava/lang/String;
    .end local v10    # "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    .end local v37    # "parser":Lcom/samsung/xml/Parser;
    :cond_b
    if-nez v13, :cond_f

    .line 487
    const-string v44, "TMSClientProSvc"

    const-string v45, "There are no client profile nodes"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 489
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 488
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 490
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 449
    .restart local v8    # "cPValue":Ljava/lang/String;
    .restart local v10    # "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    .restart local v37    # "parser":Lcom/samsung/xml/Parser;
    :cond_c
    :try_start_1
    new-instance v44, Ljava/lang/StringBuilder;

    const-string v45, "<?xml version=\"1.0\"?>"

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v44

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 450
    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Lcom/samsung/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 451
    const-string v44, "clientProfile"

    invoke-virtual {v13}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_a

    .line 454
    const-string v44, "clientProfile"

    move-object/from16 v0, v44

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 453
    goto :goto_3

    .line 466
    :cond_d
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile  cpValudeNode  is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/samsung/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    .line 473
    :cond_e
    :goto_4
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile  falied found proper xml not found"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 475
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 474
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 476
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 469
    :catch_0
    move-exception v14

    .line 470
    .local v14, "e":Lcom/samsung/xml/ParserException;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile  parse fail = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    invoke-virtual {v14}, Lcom/samsung/xml/ParserException;->printStackTrace()V

    goto :goto_4

    .line 493
    .end local v8    # "cPValue":Ljava/lang/String;
    .end local v10    # "clientProfileXmlNode1":Lcom/samsung/xml/Node;
    .end local v14    # "e":Lcom/samsung/xml/ParserException;
    .end local v37    # "parser":Lcom/samsung/xml/Parser;
    :cond_f
    invoke-virtual {v13}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v32

    .line 494
    .local v32, "noofnodes":I
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile  noofnodes = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const/16 v28, 0x0

    .line 496
    .local v28, "nodenum":I
    :goto_5
    move/from16 v0, v32

    move/from16 v1, v28

    if-ne v0, v1, :cond_12

    .line 768
    :try_start_2
    const-string v44, "android.os.SystemProperties"

    invoke-static/range {v44 .. v44}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v44

    const-string v45, "set"

    const/16 v46, 0x2

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    const-class v48, Ljava/lang/String;

    aput-object v48, v46, v47

    const/16 v47, 0x1

    const-class v48, Ljava/lang/String;

    aput-object v48, v46, v47

    invoke-virtual/range {v44 .. v46}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v18

    .line 769
    .local v18, "listener":Ljava/lang/reflect/Method;
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    move-object/from16 v44, v0

    if-eqz v44, :cond_37

    .line 770
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->length()I

    move-result v44

    const/16 v45, 0x4

    move/from16 v0, v44

    move/from16 v1, v45

    if-le v0, v1, :cond_37

    .line 771
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    const/16 v46, 0x5

    invoke-virtual/range {v44 .. v46}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v44

    const-string v45, "VWAG_"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_37

    .line 773
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "mClientID "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    const/16 v44, 0x0

    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    const-string v47, "net.mirrorlink.clientid"

    aput-object v47, v45, v46

    const/16 v46, 0x1

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v47, v0

    aput-object v47, v45, v46

    move-object/from16 v0, v18

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_4

    .line 790
    .end local v3    # "actionNode":Lcom/samsung/xml/Node;
    .end local v18    # "listener":Ljava/lang/reflect/Method;
    .end local v28    # "nodenum":I
    .end local v32    # "noofnodes":I
    :cond_10
    :goto_6
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_7
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->size()I

    move-result v44

    move/from16 v0, v17

    move/from16 v1, v44

    if-lt v0, v1, :cond_38

    .line 823
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_11

    .line 826
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.add profile id"

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    :cond_11
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile new list of cp id is ="

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile exit"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v44, v39

    .line 831
    goto/16 :goto_0

    .line 498
    .end local v17    # "i":I
    .restart local v3    # "actionNode":Lcom/samsung/xml/Node;
    .restart local v28    # "nodenum":I
    .restart local v32    # "noofnodes":I
    :cond_12
    move/from16 v0, v28

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v25

    .line 500
    .local v25, "node":Lcom/samsung/xml/Node;
    const-string v44, "clientID"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_16

    .line 502
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 509
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile profile with id "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " already present"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_8
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->size()I

    move-result v44

    move/from16 v0, v17

    move/from16 v1, v44

    if-lt v0, v1, :cond_14

    .line 763
    .end local v17    # "i":I
    :cond_13
    :goto_9
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_5

    .line 512
    .restart local v17    # "i":I
    :cond_14
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile_______"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v44, v0

    move/from16 v0, v44

    move/from16 v1, v39

    if-ne v0, v1, :cond_15

    .line 515
    const-string v45, "TMSClientProSvc"

    new-instance v46, Ljava/lang/StringBuilder;

    const-string v44, "TMClientProfileService.setClientProfile mClientProfilesList.get(i).mClientID = "

    move-object/from16 v0, v46

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, v46

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile clientProfile.mClientID = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v44, v0

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_15

    .line 519
    const-string v45, "TMSClientProSvc"

    new-instance v44, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile profile with id "

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v44

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v46, " "

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    .line 520
    const-string v46, "already present.But used by another client with id "

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v46

    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    move-object/from16 v44, v0

    move-object/from16 v0, v46

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    .line 519
    move-object/from16 v0, v45

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 522
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 521
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 523
    const/4 v9, 0x0

    .line 524
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 510
    :cond_15
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_8

    .line 532
    .end local v17    # "i":I
    :cond_16
    const-string v44, "friendlyName"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 534
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    goto/16 :goto_9

    .line 536
    :cond_17
    const-string v44, "manufacturer"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_18

    .line 538
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    goto/16 :goto_9

    .line 540
    :cond_18
    const-string v44, "modelName"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 542
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    goto/16 :goto_9

    .line 544
    :cond_19
    const-string v44, "modelNumber"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1a

    .line 546
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    goto/16 :goto_9

    .line 548
    :cond_1a
    const-string v44, "connectivity"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1f

    .line 550
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v30

    .line 551
    .local v30, "noofNode":I
    const/16 v27, 0x0

    .local v27, "nodecount":I
    :goto_a
    move/from16 v0, v27

    move/from16 v1, v30

    if-ge v0, v1, :cond_13

    .line 553
    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 555
    .local v21, "n":Lcom/samsung/xml/Node;
    if-eqz v21, :cond_1c

    const-string v44, "bluetooth"

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1c

    .line 557
    const-string v44, "bdAddr"

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 558
    .local v6, "bdAddrNode":Lcom/samsung/xml/Node;
    if-eqz v6, :cond_1b

    .line 560
    invoke-virtual {v6}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 561
    .local v7, "btAddr":Ljava/lang/String;
    const-string v44, "0x"

    move-object/from16 v0, v44

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v44

    if-eqz v44, :cond_1d

    .line 563
    const/16 v44, 0x2

    move/from16 v0, v44

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 564
    .local v16, "hexBtAddr":Ljava/lang/String;
    move-object/from16 v0, v16

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    .line 565
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile client BT addr = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    move-object/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    .end local v7    # "btAddr":Ljava/lang/String;
    .end local v16    # "hexBtAddr":Ljava/lang/String;
    :cond_1b
    :goto_b
    const-string v44, "startConnection"

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v11

    .line 573
    .local v11, "connectionNode":Lcom/samsung/xml/Node;
    if-eqz v11, :cond_1e

    const-string v44, "false"

    invoke-virtual {v11}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_1e

    .line 575
    const/16 v44, 0x0

    move/from16 v0, v44

    iput-boolean v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    .line 583
    :goto_c
    iget-boolean v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    move/from16 v44, v0

    invoke-static/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setFlagClientStartConnection(Z)V

    .line 584
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    move-object/from16 v44, v0

    invoke-static/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->SetTMClientMac(Ljava/lang/String;)V

    .line 551
    .end local v6    # "bdAddrNode":Lcom/samsung/xml/Node;
    .end local v11    # "connectionNode":Lcom/samsung/xml/Node;
    :cond_1c
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_a

    .line 569
    .restart local v6    # "bdAddrNode":Lcom/samsung/xml/Node;
    .restart local v7    # "btAddr":Ljava/lang/String;
    :cond_1d
    iput-object v7, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    goto :goto_b

    .line 579
    .end local v7    # "btAddr":Ljava/lang/String;
    .restart local v11    # "connectionNode":Lcom/samsung/xml/Node;
    :cond_1e
    const/16 v44, 0x1

    move/from16 v0, v44

    iput-boolean v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    goto :goto_c

    .line 589
    .end local v6    # "bdAddrNode":Lcom/samsung/xml/Node;
    .end local v11    # "connectionNode":Lcom/samsung/xml/Node;
    .end local v21    # "n":Lcom/samsung/xml/Node;
    .end local v27    # "nodecount":I
    .end local v30    # "noofNode":I
    :cond_1f
    const-string v44, "rtpStreaming"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_24

    .line 591
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v31

    .line 592
    .local v31, "noofNodes":I
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile connectivity rtpStreaming noofNodes = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const/16 v26, 0x0

    .local v26, "node_count":I
    :goto_d
    move/from16 v0, v26

    move/from16 v1, v31

    if-ge v0, v1, :cond_13

    .line 595
    invoke-virtual/range {v25 .. v26}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v24

    .line 597
    .local v24, "nod":Lcom/samsung/xml/Node;
    const-string v44, "payloadType"

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_20

    .line 599
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getStringTokenizer(Lcom/samsung/xml/Node;)Ljava/util/StringTokenizer;

    move-result-object v43

    .line 600
    .local v43, "tokens":Ljava/util/StringTokenizer;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile tokens count is "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, "for string"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getArrayList()Ljava/util/ArrayList;

    move-result-object v40

    .line 603
    .local v40, "rtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_e
    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v44

    if-nez v44, :cond_23

    .line 610
    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v44

    if-eqz v44, :cond_20

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNegotiatedServerRtpPayloads(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v40

    .line 613
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->clear()V

    .line 614
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 615
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile mRtpPayloads.size() = "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    move-object/from16 v46, v0

    invoke-virtual/range {v46 .. v46}, Ljava/util/ArrayList;->size()I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    .end local v40    # "rtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v43    # "tokens":Ljava/util/StringTokenizer;
    :cond_20
    const-string v44, "audioIPL"

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_21

    .line 621
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    .line 622
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile mAudioIpl is set to "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    move/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    :cond_21
    const-string v44, "audioMPL"

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_22

    .line 627
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    .line 628
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile mAudioMpl is set to "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    move/from16 v46, v0

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v44, v0

    iget v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    move/from16 v45, v0

    iget v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    move/from16 v46, v0

    invoke-virtual/range {v44 .. v46}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->setAudioIplAndMpl(II)V

    .line 593
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_d

    .line 605
    .restart local v40    # "rtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v43    # "tokens":Ljava/util/StringTokenizer;
    :cond_23
    invoke-virtual/range {v43 .. v43}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v38

    .line 606
    .local v38, "payldType":Ljava/lang/String;
    const-string v44, " "

    const-string v45, ""

    move-object/from16 v0, v38

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v38

    .line 607
    invoke-static/range {v38 .. v38}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v44

    move-object/from16 v0, v40

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_e

    .line 633
    .end local v24    # "nod":Lcom/samsung/xml/Node;
    .end local v26    # "node_count":I
    .end local v31    # "noofNodes":I
    .end local v38    # "payldType":Ljava/lang/String;
    .end local v40    # "rtpPayloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v43    # "tokens":Ljava/util/StringTokenizer;
    :cond_24
    const-string v44, "contentRules"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_26

    .line 635
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v29

    .line 636
    .local v29, "noofCRNodes":I
    const/16 v27, 0x0

    .restart local v27    # "nodecount":I
    :goto_f
    move/from16 v0, v27

    move/from16 v1, v29

    if-ge v0, v1, :cond_13

    .line 638
    move-object/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 640
    .restart local v21    # "n":Lcom/samsung/xml/Node;
    const-string v44, "rule"

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_25

    .line 642
    const-string v44, "ruleId"

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v41

    .line 643
    .local v41, "ruleidNode":Lcom/samsung/xml/Node;
    if-eqz v41, :cond_25

    .line 645
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRules:Ljava/util/Map;

    move-object/from16 v44, v0

    invoke-virtual/range {v41 .. v41}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v45

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    const-string v46, "empty"

    invoke-interface/range {v44 .. v46}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    const-string v44, "ruleValue"

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v42

    .line 648
    .local v42, "rulevalueNode":Lcom/samsung/xml/Node;
    if-eqz v42, :cond_25

    .line 650
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRules:Ljava/util/Map;

    move-object/from16 v44, v0

    invoke-virtual/range {v41 .. v41}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v45

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-interface/range {v44 .. v46}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    .end local v41    # "ruleidNode":Lcom/samsung/xml/Node;
    .end local v42    # "rulevalueNode":Lcom/samsung/xml/Node;
    :cond_25
    add-int/lit8 v27, v27, 0x1

    goto :goto_f

    .line 656
    .end local v21    # "n":Lcom/samsung/xml/Node;
    .end local v27    # "nodecount":I
    .end local v29    # "noofCRNodes":I
    :cond_26
    const-string v44, "iconPreference"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2a

    .line 658
    const-string v44, "mimetype"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 659
    .restart local v21    # "n":Lcom/samsung/xml/Node;
    const-string v44, "image/png"

    move-object/from16 v0, v44

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_mimetype:Ljava/lang/String;

    .line 661
    const-string v44, "width"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 662
    if-eqz v21, :cond_27

    .line 663
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    .line 664
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, " icon witdh in setclient is "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-static/range {v46 .. v46}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :goto_10
    const-string v44, "height"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 669
    if-eqz v21, :cond_28

    .line 670
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    .line 671
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, " icon height in setclient is "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-static/range {v46 .. v46}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :goto_11
    const-string v44, "depth"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v21

    .line 676
    if-eqz v21, :cond_29

    .line 677
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    goto/16 :goto_9

    .line 666
    :cond_27
    const-string v44, "TMSClientProSvc"

    const-string v45, " icon witdh in setclient is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 673
    :cond_28
    const-string v44, "TMSClientProSvc"

    const-string v45, " icon height in setclient is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11

    .line 679
    :cond_29
    const-string v44, "TMSClientProSvc"

    const-string v45, " icon depth in setclient is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 682
    .end local v21    # "n":Lcom/samsung/xml/Node;
    :cond_2a
    const-string v44, "services"

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_32

    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->isNotificationSupported()Z

    move-result v44

    if-eqz v44, :cond_32

    .line 684
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    const/16 v35, 0x0

    .line 686
    .local v35, "notiSettings":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;
    const-string v44, "notification"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v34

    .line 687
    .local v34, "notiNode":Lcom/samsung/xml/Node;
    if-eqz v34, :cond_13

    .line 689
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v44, v0

    const/16 v45, 0x1

    move/from16 v0, v45

    move-object/from16 v1, v44

    iput-boolean v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->bRecvdClientSettings:Z

    .line 690
    invoke-virtual/range {v34 .. v34}, Lcom/samsung/xml/Node;->getNNodes()I

    move-result v33

    .line 691
    .local v33, "notiEle":I
    const/4 v12, 0x0

    .line 692
    .local v12, "count":I
    :goto_12
    move/from16 v0, v33

    if-lt v12, v0, :cond_2b

    .line 724
    if-eqz v35, :cond_13

    .line 725
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces777"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    move-object/from16 v0, v35

    iput-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    goto/16 :goto_9

    .line 693
    :cond_2b
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces111"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    iget-object v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v35, v0

    .line 695
    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Lcom/samsung/xml/Node;->getNode(I)Lcom/samsung/xml/Node;

    move-result-object v15

    .line 696
    .local v15, "ele":Lcom/samsung/xml/Node;
    const-string v44, "notiUiSupport"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2e

    .line 697
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces222"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    const-string v44, "true"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2c

    .line 699
    const/16 v44, 0x1

    move/from16 v0, v44

    move-object/from16 v1, v35

    iput-boolean v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiUiSupport:Z

    .line 702
    :cond_2c
    iget v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v44, v0

    invoke-static/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->enbleNotiForClient(I)V

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v44, v0

    .line 704
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v44

    .line 703
    move-object/from16 v0, v44

    move-object/from16 v1, v35

    iput-object v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    .line 722
    :cond_2d
    :goto_13
    add-int/lit8 v12, v12, 0x1

    goto :goto_12

    .line 705
    :cond_2e
    const-string v44, "maxActions"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2f

    .line 706
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces666"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    .line 707
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    move-object/from16 v1, v35

    iput v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mMaxActions:I

    goto :goto_13

    .line 709
    :cond_2f
    const-string v44, "actionNameMaxLength"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_30

    .line 710
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces333"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    .line 711
    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    move/from16 v0, v44

    move-object/from16 v1, v35

    iput v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mActionMaxLen:I

    goto :goto_13

    .line 713
    :cond_30
    const-string v44, "notiTitleMaxLength"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_31

    .line 714
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces444"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    .line 715
    move/from16 v0, v44

    move-object/from16 v1, v35

    iput v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiTitleMaxLen:I

    goto :goto_13

    .line 717
    :cond_31
    const-string v44, "notiBodyMaxLength"

    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_2d

    .line 718
    const-string v44, "TMSClientProSvc"

    const-string v45, " inside servoces555"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-virtual {v15}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    .line 719
    move/from16 v0, v44

    move-object/from16 v1, v35

    iput v0, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiBodyMaxLen:I

    goto/16 :goto_13

    .line 730
    .end local v12    # "count":I
    .end local v15    # "ele":Lcom/samsung/xml/Node;
    .end local v33    # "notiEle":I
    .end local v34    # "notiNode":Lcom/samsung/xml/Node;
    .end local v35    # "notiSettings":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;
    :cond_32
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/xml/Node;->getName()Ljava/lang/String;

    move-result-object v44

    const-string v45, "mirrorLinkVersion"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_13

    .line 731
    const-string v44, "majorVersion"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v22

    .line 732
    .local v22, "n1":Lcom/samsung/xml/Node;
    if-nez v22, :cond_33

    .line 733
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile : majorversion node is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 736
    :cond_33
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile Major "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const/16 v19, -0x1

    .line 738
    .local v19, "majorVersion":I
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    if-eqz v44, :cond_34

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v44, 0x1

    move/from16 v0, v19

    move/from16 v1, v44

    if-eq v0, v1, :cond_34

    .line 739
    const-string v44, "TMSClientProSvc"

    const-string v45, "major version is not properly set"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 741
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 740
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 742
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 744
    :cond_34
    move/from16 v0, v19

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    .line 747
    const-string v44, "minorVersion"

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v23

    .line 748
    .local v23, "n2":Lcom/samsung/xml/Node;
    if-nez v23, :cond_35

    .line 749
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile : minorversion node is null"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 753
    :cond_35
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile Minor "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    const/16 v20, -0x1

    .line 755
    .local v20, "minorVersion":I
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    if-eqz v44, :cond_36

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const/16 v44, 0x1

    move/from16 v0, v20

    move/from16 v1, v44

    if-le v0, v1, :cond_36

    .line 756
    const-string v44, "TMSClientProSvc"

    const-string v45, "major version is not properly set"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v44

    .line 758
    sget-object v45, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILE:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual/range {v45 .. v45}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v45

    .line 757
    move-object/from16 v0, p1

    move/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 759
    const/16 v44, -0x1

    goto/16 :goto_0

    .line 761
    :cond_36
    move/from16 v0, v20

    iput v0, v9, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    goto/16 :goto_9

    .line 776
    .end local v19    # "majorVersion":I
    .end local v20    # "minorVersion":I
    .end local v22    # "n1":Lcom/samsung/xml/Node;
    .end local v23    # "n2":Lcom/samsung/xml/Node;
    .end local v25    # "node":Lcom/samsung/xml/Node;
    .restart local v18    # "listener":Ljava/lang/reflect/Method;
    :cond_37
    const/16 v44, 0x0

    const/16 v45, 0x2

    :try_start_3
    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    const-string v47, "net.mirrorlink.clientid"

    aput-object v47, v45, v46

    const/16 v46, 0x1

    const-string v47, "NA"

    aput-object v47, v45, v46

    move-object/from16 v0, v18

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_6

    .line 778
    .end local v18    # "listener":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v14

    .line 779
    .local v14, "e":Ljava/lang/NoSuchMethodException;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TmsService.start() "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 780
    .end local v14    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v14

    .line 781
    .local v14, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TmsService.start() "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 782
    .end local v14    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v14

    .line 783
    .local v14, "e":Ljava/lang/IllegalAccessException;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TmsService.start() "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 784
    .end local v14    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v14

    .line 785
    .local v14, "e":Ljava/lang/ClassNotFoundException;
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TmsService.start() "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 792
    .end local v3    # "actionNode":Lcom/samsung/xml/Node;
    .end local v14    # "e":Ljava/lang/ClassNotFoundException;
    .end local v28    # "nodenum":I
    .end local v32    # "noofnodes":I
    .restart local v17    # "i":I
    :cond_38
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v44

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v44, v0

    move/from16 v0, v44

    move/from16 v1, v39

    if-ne v0, v1, :cond_3d

    .line 795
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v4

    .line 796
    .local v4, "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    if-eqz v4, :cond_39

    .line 798
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.setClientProfile copying appIDs size"

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/LinkedHashSet;->size()I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    invoke-virtual {v4}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v45

    :goto_14
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-nez v44, :cond_3b

    .line 808
    :cond_39
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 809
    sget-object v44, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v44

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 810
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v44

    if-nez v44, :cond_3a

    .line 812
    const-string v44, "TMSClientProSvc"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "TMClientProfileService.add profile id"

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mUsedProfile:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    invoke-static/range {v39 .. v39}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    :cond_3a
    const-string v44, "TMSClientProSvc"

    .line 816
    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "Replacing the existing node with prfoile id "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 817
    move-object/from16 v0, v45

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    .line 816
    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    .line 815
    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const-string v44, "TMSClientProSvc"

    const-string v45, "TMClientProfileService.setClientProfile exit"

    invoke-static/range {v44 .. v45}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v44, v39

    .line 819
    goto/16 :goto_0

    .line 799
    :cond_3b
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Ljava/lang/Integer;

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Integer;->intValue()I

    move-result v36

    .line 801
    .local v36, "oldAppId":I
    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v44

    if-nez v44, :cond_3c

    .line 803
    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->createAppIDList()I

    .line 805
    :cond_3c
    move/from16 v0, v36

    invoke-virtual {v9, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->setAppID(I)I

    goto/16 :goto_14

    .line 790
    .end local v4    # "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    .end local v36    # "oldAppId":I
    :cond_3d
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_7
.end method

.method private setDefaultClientProfiles()V
    .locals 4

    .prologue
    .line 845
    const-string v2, "TMSClientProSvc"

    const-string v3, "TMClientProfileService.setDefaultClientProfile enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    if-lt v1, v2, :cond_0

    .line 857
    const-string v2, "TMSClientProSvc"

    const-string v3, "TMClientProfileService.setDefaultClientProfile exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    return-void

    .line 849
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->fillupDefaultCPValues()Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v0

    .line 850
    .local v0, "clientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iput v1, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    .line 851
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 852
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v3

    .line 851
    iput-object v3, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->mNotiSupportedApps:Ljava/util/ArrayList;

    .line 854
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 846
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public actionControlReceived(Lcom/samsung/upnp/Action;)Z
    .locals 13
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 169
    const-string v8, "TMSClientProSvc"

    const-string v11, "TMClientProfileService.actionControlReceived enter"

    invoke-static {v8, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    if-nez p1, :cond_0

    move v8, v9

    .line 286
    :goto_0
    return v8

    .line 175
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mKeyMngr:Landroid/app/KeyguardManager;

    invoke-virtual {v8}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mKeyMngr:Landroid/app/KeyguardManager;

    invoke-virtual {v8}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 177
    const-string v8, "TMSClientProSvc"

    const-string v10, "TMClientProfileService.actionControlReceived phone secure locked"

    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 179
    sget-object v10, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v10

    .line 178
    invoke-virtual {p1, v8, v10}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    move v8, v9

    .line 180
    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "actionName":Ljava/lang/String;
    const-string v8, "TMSClientProSvc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "TMClientProfileService.actionControlReceived action name = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 184
    invoke-static {v8, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v8, "GetMaxNumProfiles"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 188
    const-string v8, "NumProfilesAllowed"

    invoke-virtual {p1, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 189
    .local v2, "arg":Lcom/samsung/upnp/Argument;
    if-nez v2, :cond_2

    .line 191
    const-string v8, "TMSClientProSvc"

    const-string v10, "TMClientProfileService.actionControlReceived failed No NumProfilesAllowed"

    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 193
    sget-object v10, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v10

    .line 192
    invoke-virtual {p1, v8, v10}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    move v8, v9

    .line 194
    goto :goto_0

    .line 197
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getMaxNumProfiles()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/samsung/upnp/Argument;->setValue(I)V

    move v8, v10

    .line 198
    goto :goto_0

    .line 202
    .end local v2    # "arg":Lcom/samsung/upnp/Argument;
    :cond_3
    const-string v8, "SetClientProfile"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 204
    const/4 v6, -0x1

    .line 205
    .local v6, "profileID":I
    const/4 v8, -0x1

    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->setClientProfile(Lcom/samsung/upnp/Action;)I

    move-result v6

    if-ne v8, v6, :cond_4

    .line 207
    const-string v8, "TMSClientProSvc"

    const-string v10, "TMClientProfileService.actionControlReceived setClientProfile failed"

    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 208
    goto/16 :goto_0

    .line 212
    :cond_4
    const-string v8, "ResultProfile"

    .line 213
    invoke-direct {p0, v6}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->formClientProfileXml(I)Ljava/lang/String;

    move-result-object v9

    .line 212
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfileEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IClientProfileEventsListener;

    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->findUnUsedProfiles()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/samsung/android/mirrorlink/upnpdevice/IClientProfileEventsListener;->OnUnUsedProfileEvent(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->setMLSessionEstablished(Z)V

    .line 218
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v5

    .line 219
    .local v5, "infoNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-eqz v5, :cond_5

    .line 220
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 221
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v8, "CLIENT_IDENTIFIER"

    iget-object v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v8, "CLIENT_FRIENDLY_NAME"

    iget-object v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v8, "CLIENT_MANUFACTURER"

    iget-object v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v8, "CLIENT_MODEL_NAME"

    iget-object v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v8, "CLIENT_MODEL_NUMBER"

    iget-object v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v8, "TMSClientProSvc"

    const-string v9, "TMClientProfileService.actionControlReceived :notifyDeviceInfoChange "

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget v8, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    iget v9, v5, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    invoke-virtual {p0, v8, v9, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->notifyDeviceInfoChange(IILandroid/os/Bundle;)V

    .end local v3    # "bundle":Landroid/os/Bundle;
    :cond_5
    move v8, v10

    .line 233
    goto/16 :goto_0

    .line 237
    .end local v5    # "infoNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v6    # "profileID":I
    :cond_6
    const-string v8, "GetClientProfile"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 239
    const/4 v0, 0x0

    .line 240
    .local v0, "ClientProfile":Ljava/lang/String;
    const/4 v6, 0x0

    .line 241
    .restart local v6    # "profileID":I
    const/4 v7, 0x0

    .line 243
    .local v7, "profileIDFound":Z
    const-string v8, "ProfileID"

    invoke-virtual {p1, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v2

    .line 244
    .restart local v2    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v2, :cond_7

    .line 246
    const-string v8, "TMSClientProSvc"

    const-string v10, "TMClientProfileService.actionControlReceived failed No ProfileID"

    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 248
    sget-object v10, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->OPERATION_REJECTED:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v10

    .line 247
    invoke-virtual {p1, v8, v10}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    move v8, v9

    .line 249
    goto/16 :goto_0

    .line 252
    :cond_7
    invoke-virtual {v2}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v6

    .line 254
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v4, v8, :cond_8

    .line 262
    :goto_2
    if-nez v7, :cond_a

    .line 264
    const-string v8, "TMSClientProSvc"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "TMClientProfileService.actionControlReceived Error in getting prfoile with id="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".Invalid profile id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 264
    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 267
    sget-object v10, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService$eACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v10

    .line 266
    invoke-virtual {p1, v8, v10}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    move v8, v9

    .line 268
    goto/16 :goto_0

    .line 256
    :cond_8
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v8, v8, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    if-ne v8, v6, :cond_9

    .line 258
    const/4 v7, 0x1

    .line 259
    goto :goto_2

    .line 254
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 272
    :cond_a
    invoke-direct {p0, v6}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->formClientProfileXml(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    .line 274
    const-string v8, "TMSClientProSvc"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "TMClientProfileService.actionControlReceived rror in getting prfoile with id="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".Invalid profile id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 274
    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 276
    goto/16 :goto_0

    .line 280
    :cond_b
    const-string v8, "ClientProfile"

    invoke-virtual {p1, v8, v0}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v10

    .line 281
    goto/16 :goto_0

    .line 285
    .end local v0    # "ClientProfile":Ljava/lang/String;
    .end local v2    # "arg":Lcom/samsung/upnp/Argument;
    .end local v4    # "i":I
    .end local v6    # "profileID":I
    .end local v7    # "profileIDFound":Z
    :cond_c
    const-string v8, "TMSClientProSvc"

    const-string v10, "TMClientProfileService.actionControlReceived exit:no action found"

    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 286
    goto/16 :goto_0
.end method

.method public isMirrorLinkSessionEstablished()Z
    .locals 1

    .prologue
    .line 1185
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mSessionEstablished:Z

    return v0
.end method

.method public notifyDeviceInfoChange(IILandroid/os/Bundle;)V
    .locals 3
    .param p1, "major"    # I
    .param p2, "minor"    # I
    .param p3, "b"    # Landroid/os/Bundle;

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    if-eqz v0, :cond_0

    .line 1202
    const-string v0, "TMSClientProSvc"

    .line 1203
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMClientProfileService. sending onEvent for UPDATE_DEVICE_INFO with values Major :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "minor:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1202
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const-string v0, "VERSION_MAJOR"

    invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1205
    const-string v0, "VERSION_MINOR"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1206
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDevInfoMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;->onEvent(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;Ljava/lang/Object;)V

    .line 1209
    :cond_0
    return-void
.end method

.method public queryControlReceived(Lcom/samsung/upnp/StateVariable;)Z
    .locals 2
    .param p1, "stateVar"    # Lcom/samsung/upnp/StateVariable;

    .prologue
    .line 324
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.queryControlReceived enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stateVar"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.queryControlReceived exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public regForClientProfileEvents(Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;)V
    .locals 0
    .param p1, "clientProfileEventing"    # Lcom/samsung/android/mirrorlink/upnpdevice/ClientProfileEventing;

    .prologue
    .line 1135
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfileEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IClientProfileEventsListener;

    .line 1137
    return-void
.end method

.method public registerConMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 1161
    const-string v0, "TMSClientProSvc"

    .line 1162
    const-string v1, "TMClientProfileService. registerConMgrCommonApiCallBack() - Enter"

    .line 1161
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 1164
    return-void
.end method

.method public registerDevMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 1167
    const-string v0, "TMSClientProSvc"

    .line 1168
    const-string v1, "TMClientProfileService. registerDevMgrCommonApiCallBack() - Enter"

    .line 1167
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDevInfoMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 1170
    return-void
.end method

.method public setMLSessionEstablished(Z)V
    .locals 3
    .param p1, "mlStatus"    # Z

    .prologue
    .line 1189
    const-string v0, "TMSClientProSvc"

    .line 1190
    const-string v1, "TMClientProfileService. setMLSessionEstablished() - Enter"

    .line 1189
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mSessionEstablished:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    if-eqz v0, :cond_0

    .line 1192
    const-string v0, "TMSClientProSvc"

    .line 1193
    const-string v1, "TMClientProfileService. sending onEvent for UPDATE_MLSESSION_CHANGE"

    .line 1192
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;->onEvent(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;Ljava/lang/Object;)V

    .line 1197
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mSessionEstablished:Z

    .line 1198
    return-void
.end method

.method public setMaxNumProfiles(I)V
    .locals 2
    .param p1, "MaxNoOfProfiles"    # I

    .prologue
    .line 338
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.setMaxNumProfiles enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iput p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mMaxNumProfiles:I

    .line 340
    const-string v0, "TMSClientProSvc"

    const-string v1, "TMClientProfileService.setMaxNumProfiles enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public unRegisterConMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 1173
    const-string v0, "TMSClientProSvc"

    .line 1174
    const-string v1, "TMClientProfileService. unRegisterConMgrCommonApiCallBack() - Enter"

    .line 1173
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mConMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 1177
    return-void
.end method

.method public unRegisterDevMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 1180
    const-string v0, "TMSClientProSvc"

    .line 1181
    const-string v1, "TMClientProfileService. unRegisterDevMgrCommonApiCallBack() - Enter"

    .line 1180
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mDevInfoMgrCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 1183
    return-void
.end method
