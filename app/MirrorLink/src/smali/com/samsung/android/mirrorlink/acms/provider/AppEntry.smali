.class public Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
.super Ljava/lang/Object;
.source "AppEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;
    }
.end annotation


# static fields
.field private static final MINUTE_IN_MILLISECOND:J = 0xea60L


# instance fields
.field private fetchedAppCerts:[Ljava/security/cert/X509Certificate;

.field private mAppId:Ljava/lang/String;

.field private mCertificateInfo:Ljava/lang/String;

.field private mEntity:Ljava/lang/String;

.field private mGracePeriod:Ljava/lang/Long;

.field private mIsCertified:Z

.field private mIsMemberApp:Z

.field private mIsPending:Z

.field private mIsRevoked:Z

.field private mMaxRetry:Ljava/lang/Integer;

.field private mPackageName:Ljava/lang/String;

.field private mQueryPeriod:Ljava/lang/Long;

.field private mState:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field private type:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getCertificateInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mCertificateInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getDiffOfQueryPeriod()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 191
    const-wide/32 v0, 0x2bf20

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getEntity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mEntity:Ljava/lang/String;

    return-object v0
.end method

.method public getFetchedAppCerts()[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->fetchedAppCerts:[Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public getGracePeriod()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mGracePeriod:Ljava/lang/Long;

    return-object v0
.end method

.method public getMaxRetry()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mMaxRetry:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryPeriod()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mQueryPeriod:Ljava/lang/Long;

    return-object v0
.end method

.method public getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mState:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    return-object v0
.end method

.method public getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->type:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    return-object v0
.end method

.method public isCertified()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsCertified:Z

    return v0
.end method

.method public isMemberApp()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsMemberApp:Z

    return v0
.end method

.method public isPending()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsPending:Z

    return v0
.end method

.method public isRevoked()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsRevoked:Z

    return v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mAppId:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setCertificateInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "certificateInfo"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mCertificateInfo:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setCertificateList([Ljava/security/cert/X509Certificate;)V
    .locals 0
    .param p1, "certs"    # [Ljava/security/cert/X509Certificate;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->fetchedAppCerts:[Ljava/security/cert/X509Certificate;

    .line 108
    return-void
.end method

.method public setEntity(Ljava/lang/String;)V
    .locals 0
    .param p1, "entity"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mEntity:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setGracePeriod(Ljava/lang/Long;)V
    .locals 7
    .param p1, "period"    # Ljava/lang/Long;

    .prologue
    .line 127
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 129
    .local v0, "rightNow":Ljava/util/Calendar;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x36ee80

    mul-long/2addr v3, v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    add-long v1, v3, v5

    .line 131
    .local v1, "time":J
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mGracePeriod:Ljava/lang/Long;

    .line 132
    return-void
.end method

.method public setGracePeriodInMillis(J)V
    .locals 1
    .param p1, "period"    # J

    .prologue
    .line 135
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mGracePeriod:Ljava/lang/Long;

    .line 136
    return-void
.end method

.method public setIsCertified(Z)V
    .locals 0
    .param p1, "isCertified"    # Z

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsCertified:Z

    .line 168
    return-void
.end method

.method public setIsMemberApp(Z)V
    .locals 0
    .param p1, "isMemberApp"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsMemberApp:Z

    .line 176
    return-void
.end method

.method public setIsPending(Z)V
    .locals 0
    .param p1, "isPending"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsPending:Z

    .line 88
    return-void
.end method

.method public setIsRevoked(Z)V
    .locals 0
    .param p1, "isRevoked"    # Z

    .prologue
    .line 183
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mIsRevoked:Z

    .line 184
    return-void
.end method

.method public setMaxRetry(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "maxRetry"    # Ljava/lang/Integer;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mMaxRetry:Ljava/lang/Integer;

    .line 160
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mPackageName:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setQueryPeriod(Ljava/lang/Long;)V
    .locals 7
    .param p1, "period"    # Ljava/lang/Long;

    .prologue
    .line 115
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 117
    .local v0, "rightNow":Ljava/util/Calendar;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x36ee80

    mul-long/2addr v3, v5

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getDiffOfQueryPeriod()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v1, v3, v5

    .line 119
    .local v1, "time":J
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mQueryPeriod:Ljava/lang/Long;

    .line 120
    return-void
.end method

.method public setQueryPeriodInMillis(J)V
    .locals 1
    .param p1, "period"    # J

    .prologue
    .line 139
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mQueryPeriod:Ljava/lang/Long;

    .line 140
    return-void
.end method

.method public setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V
    .locals 0
    .param p1, "state"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->mState:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 80
    return-void
.end method

.method public setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V
    .locals 0
    .param p1, "type"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->type:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .line 152
    return-void
.end method
