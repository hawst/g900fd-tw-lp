.class public Lcom/samsung/android/mirrorlink/util/AppDbProvider;
.super Landroid/content/ContentProvider;
.source "AppDbProvider.java"


# static fields
.field private static final APPDATA:I = 0x1

.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.mirrorlink.provider"

.field private static final BASE_PATH_APP_TABLE:Ljava/lang/String; = "appdata"

.field private static final BASE_PATH_ENTITY_TABLE:Ljava/lang/String; = "EntityData"

.field private static final BASE_PATH_JOIN_TABLE:Ljava/lang/String; = "appdata$EntityData"

.field private static final BASE_PATH_SEQ_TABLE:Ljava/lang/String; = "sqlite_sequence"

.field public static final CONTENT_DELETE_SEQ:Landroid/net/Uri;

.field public static final CONTENT_URI_APPDATA:Landroid/net/Uri;

.field public static final CONTENT_URI_APPENTITY_JOIN:Landroid/net/Uri;

.field public static final CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

.field private static final ENTITYDATA:I = 0x2

.field private static final JOINDATA:I = 0x3

.field private static final SEQ_DATA:I = 0x4

.field private static final TAG:Ljava/lang/String; = "TMSSvc/AcmsDbProvider"

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

.field private mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mWritableDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-string v0, "content://com.samsung.android.mirrorlink.provider/appdata"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 27
    const-string v0, "content://com.samsung.android.mirrorlink.provider/EntityData"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    .line 30
    const-string v0, "content://com.samsung.android.mirrorlink.provider/appdata$EntityData"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPENTITY_JOIN:Landroid/net/Uri;

    .line 33
    const-string v0, "content://com.samsung.android.mirrorlink.provider/sqlite_sequence"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_DELETE_SEQ:Landroid/net/Uri;

    .line 45
    new-instance v0, Landroid/content/UriMatcher;

    .line 46
    const/4 v1, -0x1

    .line 45
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 48
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.mirrorlink.provider"

    const-string v2, "appdata"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 49
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.mirrorlink.provider"

    const-string v2, "EntityData"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.mirrorlink.provider"

    const-string v2, "appdata$EntityData"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 51
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.mirrorlink.provider"

    const-string v2, "sqlite_sequence"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 43
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 15
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 57
    sget-object v2, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 58
    .local v1, "uriType":I
    const/4 v0, 0x0

    .line 59
    .local v0, "rowsDeleted":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 62
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 73
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown URI : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appdata"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 75
    :goto_0
    return v0

    .line 67
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "EntityData"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 68
    goto :goto_0

    .line 70
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "sqlite_sequence"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 71
    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v5, 0x0

    .line 85
    sget-object v3, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 86
    .local v2, "uriType":I
    const-wide/16 v0, 0x0

    .line 87
    .local v0, "id":J
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 90
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 98
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown URI: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 92
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "appdata"

    invoke-virtual {v3, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 93
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "appdata/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 96
    :goto_0
    return-object v3

    .line 95
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "EntityData"

    invoke-virtual {v3, v4, v5, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EntityData/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 104
    const-string v0, "TMSSvc/AcmsDbProvider"

    const-string v1, "Enter on create"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    .line 106
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 108
    const-string v0, "TMSSvc/AcmsDbProvider"

    const-string v1, "Exit on create"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 115
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 116
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v8, 0x0

    .line 117
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 118
    .local v9, "uriType":I
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 121
    :cond_0
    packed-switch v9, :pswitch_data_0

    .line 148
    :goto_0
    return-object v8

    .line 123
    :pswitch_0
    const-string v1, "appdata"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 128
    goto :goto_0

    .line 130
    :pswitch_1
    const-string v1, "EntityData"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 134
    goto :goto_0

    .line 137
    :pswitch_2
    const-string v1, "appdata LEFT OUTER JOIN EntityData ON packageName = pkg_name"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 140
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 141
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 143
    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 153
    sget-object v2, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 154
    .local v1, "uriType":I
    const/4 v0, 0x0

    .line 155
    .local v0, "rowsUpdated":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mAppDbbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 158
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 168
    :goto_0
    return v0

    .line 160
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appdata"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 161
    goto :goto_0

    .line 163
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "EntityData"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 164
    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
