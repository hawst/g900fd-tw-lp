.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AcsAlarmReceiver.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSAlarmRecv"


# instance fields
.field private AcsAlarmTask:Ljava/lang/Runnable;

.field private aTid:I

.field private alarmHandler:Landroid/os/Handler;

.field mNative:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->alarmHandler:Landroid/os/Handler;

    .line 13
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;->getAcsSingleTimer()Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->mNative:Lcom/samsung/android/mirrorlink/portinginterface/AcsSingleTimer;

    .line 17
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver$1;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->AcsAlarmTask:Ljava/lang/Runnable;

    .line 9
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;)I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->aTid:I

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    const-string v0, "TMSAlarmRecv"

    const-string v1, "AcsAlarmReceiver: Entered"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v0, "com.samsung.acs.framework.tid"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->aTid:I

    .line 39
    const-string v0, "TMSAlarmRecv"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsAlarmReceiver: The Value of ATid is :::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->aTid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->alarmHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->AcsAlarmTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->alarmHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsAlarmReceiver;->AcsAlarmTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 43
    return-void
.end method
