.class public Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;
.super Landroid/app/Activity;
.source "AcmsMainActivity.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "AcmsMainActivity"

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 13
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 14
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate() Enter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCreate() Exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 94
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDestroy() Enter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsMainActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDestroy() Exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 98
    return-void
.end method
