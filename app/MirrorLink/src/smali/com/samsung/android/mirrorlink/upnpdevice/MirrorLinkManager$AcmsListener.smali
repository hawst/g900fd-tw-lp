.class Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
.super Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener$Stub;
.source "MirrorLinkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AcmsListener"
.end annotation


# instance fields
.field private final mMirrorlinkMgrRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)V
    .locals 1
    .param p1, "mirrorlinkMgr"    # Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener$Stub;-><init>()V

    .line 193
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->mMirrorlinkMgrRef:Ljava/lang/ref/WeakReference;

    .line 194
    return-void
.end method


# virtual methods
.method public clearMirrorlinkMgr()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->mMirrorlinkMgrRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 198
    return-void
.end method

.method public notifyRevocationCheckResult(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "result"    # I

    .prologue
    .line 202
    const-string v0, "MirrorLinkManager"

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcmsListener.notifyRevocationCheckResult():result"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 203
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->mMirrorlinkMgrRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->mMirrorlinkMgrRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->notifyResult(Ljava/lang/String;I)V

    goto :goto_0
.end method
