.class public Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
.super Ljava/lang/Object;
.source "AcmsRevocationMngr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;,
        Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    }
.end annotation


# static fields
.field private static final DEFAULT_QUERY_PERIOD:J = 0xa8L

.field private static final MAX_RETRY_HOUR:I = 0xfc0

.field private static final NO_PACKAGE_NAME:Ljava/lang/String; = "no_package_to_show"

.field public static final OCSP_RESP_INTERNAL_ERROR:I = 0x2

.field public static final OCSP_RESP_MALFORMED_REQUEST:I = 0x1

.field public static final OCSP_RESP_SIG_REQUIRED:I = 0x5

.field public static final OCSP_RESP_SUCCESSFUL:I = 0x0

.field public static final OCSP_RESP_TRY_LATER:I = 0x3

.field public static final OCSP_RESP_UNAUTHORIZED:I = 0x6

.field private static final ONE_DAY_IN_HOUR:I = 0x18

.field private static final TAG:Ljava/lang/String; = "AcmsRevocationMngr"

.field private static sAcmsRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;


# instance fields
.field private mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

.field private mContext:Landroid/content/Context;

.field private mMlCertifiedHandler:Landroid/os/Handler;

.field private mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

.field private mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;-><init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mMlCertifiedHandler:Landroid/os/Handler;

    .line 115
    const-string v0, "AcmsRevocationMngr"

    const-string v1, "AcmsRevocationMngr"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    .line 117
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 119
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;-><init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    .line 120
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    return-void
.end method

.method public static getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->sAcmsRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->sAcmsRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 130
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->sAcmsRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    return-object v0
.end method

.method private manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    .locals 7
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    .line 166
    const-string v4, "AcmsRevocationMngr"

    const-string v5, "manageAlarm(); Entered"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    if-nez p1, :cond_1

    .line 168
    const-string v4, "AcmsRevocationMngr"

    const-string v5, "appEntry is null"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextQueryPeriod()J

    move-result-wide v2

    .line 173
    .local v2, "queryPeriod":J
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextGracePeriod()J

    move-result-wide v0

    .line 175
    .local v0, "gracePeriod":J
    cmp-long v4, v2, v0

    if-gez v4, :cond_2

    .line 176
    const-string v4, "AcmsRevocationMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Alarm set for query period "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    .line 178
    const-string v4, "AcmsRevocationMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "current query period is least one "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 179
    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 178
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->cancelAlarm(Landroid/content/Context;)V

    .line 181
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    .line 182
    const-string v6, "query period"

    .line 181
    invoke-static {v4, v2, v3, v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->setAlarm(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-nez v4, :cond_0

    .line 186
    const-string v4, "AcmsRevocationMngr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "current grace period is least one "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 186
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->cancelAlarm(Landroid/content/Context;)V

    .line 190
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    .line 191
    const-string v6, "grace period"

    .line 190
    invoke-static {v4, v0, v1, v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->setAlarm(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 731
    const-string v0, "AcmsRevocationMngr"

    const-string v1, "AcmsRevocationMngr: inside cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->sAcmsRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 733
    return-void
.end method

.method public getRevocationHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mMlCertifiedHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 123
    const-string v0, "AcmsRevocationMngr"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    .locals 5
    .param p1, "st"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 712
    const-string v2, "AcmsRevocationMngr"

    const-string v3, "Entered postToMLCertifiedHandler"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mMlCertifiedHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 714
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 715
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 716
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mMlCertifiedHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 718
    .local v0, "isSuccess":Z
    if-eqz v0, :cond_0

    .line 720
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v2

    .line 721
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 722
    const-string v2, "AcmsRevocationMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incremented Counter Value : postToMlCertifiedHandler=>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_0
    return-void
.end method

.method public scheduleAlarm()V
    .locals 9

    .prologue
    .line 134
    const-string v6, "AcmsRevocationMngr"

    const-string v7, "Inside scheduleAlarm"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextQueryPeriod()J

    move-result-wide v3

    .line 137
    .local v3, "queryPeriod":J
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextGracePeriod()J

    move-result-wide v0

    .line 138
    .local v0, "gracePeriod":J
    cmp-long v6, v3, v0

    if-gez v6, :cond_1

    .line 139
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 140
    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextQueryPeriodAppId()Ljava/lang/String;

    move-result-object v5

    .line 141
    .local v5, "queryPeriodApp":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 142
    const-string v6, "AcmsRevocationMngr"

    const-string v7, "Queryperiod app is null: hence ignore"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .end local v5    # "queryPeriodApp":Ljava/lang/String;
    :goto_0
    return-void

    .line 145
    .restart local v5    # "queryPeriodApp":Ljava/lang/String;
    :cond_0
    const-string v6, "AcmsRevocationMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "queryPeriod: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " queryPeriodApp: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 146
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 145
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->cancelAlarm(Landroid/content/Context;)V

    .line 148
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    .line 149
    const-string v7, "query period"

    .line 148
    invoke-static {v6, v3, v4, v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->setAlarm(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    .end local v5    # "queryPeriodApp":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 152
    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getNextGracePeriodAppId()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "gracePeriodApp":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 154
    const-string v6, "AcmsRevocationMngr"

    const-string v7, "gracePerioApp is null: hence ignore"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    const-string v6, "AcmsRevocationMngr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "gracePeriod: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " gracePeriodApp: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 158
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 157
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->cancelAlarm(Landroid/content/Context;)V

    .line 160
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    .line 161
    const-string v7, "grace period"

    .line 160
    invoke-static {v6, v0, v1, v2, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->setAlarm(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateProtocol(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    .locals 6
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    .line 737
    const-string v2, "AcmsRevocationMngr"

    const-string v3, "inside updateProtocol"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v0

    .line 741
    .local v0, "acmsCertificateMngr":Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 742
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 745
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppAdded(Ljava/lang/String;)V

    .line 758
    :goto_0
    return-void

    .line 748
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 749
    .local v1, "cuurentTime":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 750
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 749
    sub-long/2addr v2, v4

    .line 750
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 754
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 753
    invoke-virtual {p0, v2, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 756
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppAdded(Ljava/lang/String;)V

    goto :goto_0
.end method
