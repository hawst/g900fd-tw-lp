.class public Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;
.super Ljava/lang/Object;
.source "PhoneAppActions.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field private imbinder:Landroid/os/IBinder;

.field private mInputManager:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v5, "TMSAppMngr"

    const-string v6, "PhoneAppActions.PhoneAppActions enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    :try_start_0
    const-string v5, "android.os.ServiceManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 32
    .local v4, "serviceManagerClass":Ljava/lang/Class;
    const-string v5, "getService"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 34
    .local v2, "getService":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "input"

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    iput-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->imbinder:Landroid/os/IBinder;

    .line 35
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->imbinder:Landroid/os/IBinder;

    invoke-interface {v5}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 36
    .local v3, "imClass":Ljava/lang/Class;
    invoke-virtual {v3}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    const-string v6, "asInterface"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/os/IBinder;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->imbinder:Landroid/os/IBinder;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->mInputManager:Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5

    .line 52
    .end local v2    # "getService":Ljava/lang/reflect/Method;
    .end local v3    # "imClass":Ljava/lang/Class;
    .end local v4    # "serviceManagerClass":Ljava/lang/Class;
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions RemoteException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions NoSuchMethodException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions InvocationTargetException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions ClassNotFoundException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 46
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_4
    move-exception v1

    .line 47
    .local v1, "e1":Ljava/lang/IllegalAccessException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions IllegalAccessException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    .end local v1    # "e1":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v1

    .line 49
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.PhoneAppActions IllegalArgumentException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private injectEvent(Landroid/view/InputEvent;Z)V
    .locals 10
    .param p1, "event"    # Landroid/view/InputEvent;
    .param p2, "sync"    # Z

    .prologue
    .line 108
    const/4 v2, 0x0

    .line 110
    .local v2, "fieldMode":Ljava/lang/reflect/Field;
    if-nez p2, :cond_0

    .line 111
    :try_start_0
    const-class v5, Landroid/hardware/input/InputManager;

    const-string v6, "INJECT_INPUT_EVENT_MODE_ASYNC"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 118
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 119
    .local v4, "mode":I
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->mInputManager:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "injectInputEvent"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/view/InputEvent;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 120
    .local v3, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 121
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->mInputManager:Ljava/lang/Object;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    .end local v3    # "m":Ljava/lang/reflect/Method;
    .end local v4    # "mode":I
    :goto_1
    return-void

    .line 114
    :cond_0
    const-class v5, Landroid/hardware/input/InputManager;

    const-string v6, "INJECT_INPUT_EVENT_MODE_WAIT_FOR_RESULT"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.injectEvent NoSuchMethodException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 125
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.injectEvent InvocationTargetException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 127
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.injectEvent NoSuchFieldException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 129
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v1

    .line 130
    .local v1, "e1":Ljava/lang/IllegalAccessException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.injectEvent IllegalAccessException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 131
    .end local v1    # "e1":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v1

    .line 132
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PhoneAppActions.injectEvent IllegalArgumentException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public doAction(Ljava/lang/String;)Z
    .locals 9
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v3, "TMSAppMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PhoneAppActions.doAction enter action "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/4 v8, 0x0

    .line 59
    .local v8, "ret":Z
    const-string v3, "AcceptCall"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 62
    .local v1, "now":J
    new-instance v0, Landroid/view/KeyEvent;

    .line 63
    const/4 v5, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x0

    move-wide v3, v1

    .line 62
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 64
    .local v0, "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 66
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 67
    new-instance v0, Landroid/view/KeyEvent;

    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v5, 0x1

    .line 68
    const/4 v6, 0x5

    const/4 v7, 0x0

    move-wide v3, v1

    .line 67
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 69
    .restart local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 70
    const/4 v8, 0x1

    .line 100
    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    .end local v1    # "now":J
    :cond_0
    :goto_0
    const-string v3, "TMSAppMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PhoneAppActions.doAction exit ret = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return v8

    .line 71
    :cond_1
    const-string v3, "RejectCall"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 75
    .restart local v1    # "now":J
    new-instance v0, Landroid/view/KeyEvent;

    .line 76
    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-wide v3, v1

    .line 75
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 77
    .restart local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 80
    new-instance v0, Landroid/view/KeyEvent;

    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v5, 0x1

    .line 81
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-wide v3, v1

    .line 80
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 82
    .restart local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 83
    const/4 v8, 0x1

    .line 84
    goto :goto_0

    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    .end local v1    # "now":J
    :cond_2
    const-string v3, "SilenceCall"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 87
    .restart local v1    # "now":J
    new-instance v0, Landroid/view/KeyEvent;

    .line 88
    const/4 v5, 0x0

    const/16 v6, 0x19

    const/4 v7, 0x0

    move-wide v3, v1

    .line 87
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 89
    .restart local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 91
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 92
    new-instance v0, Landroid/view/KeyEvent;

    .end local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v5, 0x1

    .line 93
    const/16 v6, 0x19

    const/4 v7, 0x0

    move-wide v3, v1

    .line 92
    invoke-direct/range {v0 .. v7}, Landroid/view/KeyEvent;-><init>(JJIII)V

    .line 94
    .restart local v0    # "androidEvent":Landroid/view/KeyEvent;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->injectEvent(Landroid/view/InputEvent;Z)V

    .line 95
    const/4 v8, 0x1

    goto :goto_0
.end method
