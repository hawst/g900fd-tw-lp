.class public Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;
.super Lcom/mirrorlink/android/commonapi/ICertificationManager$Stub;
.source "CertificationManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSCertificationManager"


# instance fields
.field private mCertificationStatusChangeListener:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;

.field private final mClinetListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/ICertificationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/ICertificationManager$Stub;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 28
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    .line 29
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCertificationStatusChangeListener:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;

    .line 30
    return-void
.end method


# virtual methods
.method public getApplicationCertificationInformation(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "entity"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 133
    const-string v2, "TMSCertificationManager"

    .line 134
    const-string v3, "CertificationManager.getApplicationCertificationInformation()"

    .line 133
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "packname":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 138
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v2

    if-nez v2, :cond_2

    .line 140
    :cond_0
    const-string v2, "TMSCertificationManager"

    .line 141
    const-string v3, "CertificationManager.getApplicationCertificationInformation(): returning default values because packname or getUpnpCommonApiUtil is null "

    .line 139
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 144
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "ENTITY"

    .line 145
    const-string v3, "Not Available"

    .line 144
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v2, "CERTIFIED"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    const-string v2, "RESTRICTED"

    .line 148
    const-string v3, "Not Available"

    .line 147
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v2, "NONRESTRICTED"

    .line 150
    const-string v3, "Not Available"

    .line 149
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :goto_0
    if-nez v0, :cond_1

    .line 157
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 158
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "ENTITY"

    .line 159
    const-string v3, "Not Available"

    .line 158
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v2, "CERTIFIED"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 161
    const-string v2, "RESTRICTED"

    .line 162
    const-string v3, "Not Available"

    .line 161
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v2, "NONRESTRICTED"

    .line 164
    const-string v3, "Not Available"

    .line 163
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_1
    return-object v0

    .line 153
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v2

    .line 154
    invoke-virtual {v2, p1, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getApplicationCertificationInformation(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 153
    goto :goto_0
.end method

.method public getApplicationCertificationStatus()Landroid/os/Bundle;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 68
    const-string v2, "TMSCertificationManager"

    .line 69
    const-string v3, "CertificationManager.getApplicationCertificationStatus()"

    .line 68
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "packname":Ljava/lang/String;
    const-string v2, "TMSCertificationManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAppPackageName"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 74
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v2

    if-nez v2, :cond_2

    .line 76
    :cond_0
    const-string v2, "TMSCertificationManager"

    .line 77
    const-string v3, "CertificationManager.getApplicationCertificationStatus(): returning default values because packname or getUpnpCommonApiUtil is null "

    .line 75
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 81
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "HAS_VALID_CERTIFICATE"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 84
    const-string v2, "ADVERTISED_AS_CERTIFIEDAPP"

    .line 83
    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 94
    :goto_0
    if-nez v0, :cond_1

    .line 95
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 96
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "HAS_VALID_CERTIFICATE"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 99
    const-string v2, "ADVERTISED_AS_CERTIFIEDAPP"

    .line 98
    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 102
    :cond_1
    return-object v0

    .line 87
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v2

    .line 88
    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getApplicationCertificationStatus(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 87
    goto :goto_0
.end method

.method public getApplicationCertifyingEntities()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    const-string v3, "TMSCertificationManager"

    .line 108
    const-string v4, "CertificationManager.getApplicationCertifyingEntities()"

    .line 107
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v1, 0x0

    .line 110
    .local v1, "ret":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "packname":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v3

    if-nez v3, :cond_1

    .line 115
    :cond_0
    const-string v3, "TMSCertificationManager"

    .line 116
    const-string v4, "CertificationManager.getApplicationCertifyingEntities() : Package or getUpnpCommonApiUtil is null"

    .line 114
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "Not Available"

    move-object v2, v1

    .line 127
    .end local v1    # "ret":Ljava/lang/String;
    .local v2, "ret":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 120
    .end local v2    # "ret":Ljava/lang/String;
    .restart local v1    # "ret":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v3

    .line 121
    invoke-virtual {v3, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getApplicationCertifyingEntities(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    if-nez v1, :cond_2

    .line 125
    const-string v1, "Not Available"

    :cond_2
    move-object v2, v1

    .line 127
    .end local v1    # "ret":Ljava/lang/String;
    .restart local v2    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 2
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    .line 254
    if-eqz p1, :cond_0

    .line 255
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_1

    .line 256
    const-string v0, "TMSCertificationManager"

    .line 257
    const-string v1, "certificationManager.handleEvent(): engine is set"

    .line 256
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCertificationStatusChangeListener:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager$CertificationStatusChangeListener;

    .line 260
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->registerCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_0

    .line 264
    const-string v0, "TMSCertificationManager"

    .line 265
    const-string v1, "certificationManager.handleEvent() engine is null"

    .line 264
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    .line 268
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->unRegisterCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "TMSCertificationManager"

    .line 61
    const-string v1, "CertificationManager.killListener() : Enter"

    .line 60
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 65
    return-void
.end method

.method public notifyCertStatusForAllPackage()V
    .locals 5

    .prologue
    .line 171
    const-string v3, "TMSCertificationManager"

    .line 172
    const-string v4, "CertificationManager.notifyAllPackage()"

    .line 171
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 174
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 185
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 186
    return-void

    .line 176
    :cond_0
    :try_start_0
    const-string v3, "TMSCertificationManager"

    .line 177
    const-string v4, "CertificationManager.notifyAllPackage() : notifying callback"

    .line 176
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/ICertificationListener;

    invoke-interface {v3}, Lcom/mirrorlink/android/commonapi/ICertificationListener;->onCertificationStatusChanged()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "TMSCertificationManager"

    .line 181
    const-string v4, "CertificationManager.notifyAllPackage() : Exception in notifying callback"

    .line 180
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyCertificationStatusChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "packname"    # Ljava/lang/String;

    .prologue
    .line 188
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 189
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 204
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 205
    return-void

    .line 191
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    .line 193
    .local v3, "obj":Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 194
    const-string v4, "TMSCertificationManager"

    .line 195
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CertificationManager.notifyAudioBlocked() for package:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 195
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/ICertificationListener;

    .line 198
    invoke-interface {v4}, Lcom/mirrorlink/android/commonapi/ICertificationListener;->onCertificationStatusChanged()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/ICertificationListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/ICertificationListener;

    .prologue
    .line 34
    const-string v0, "TMSCertificationManager"

    const-string v1, "CertificationManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 36
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 38
    :cond_0
    const-string v0, "TMSCertificationManager"

    const-string v1, "CertificationManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 210
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSCertificationManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CertificationManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 210
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    if-nez v2, :cond_0

    .line 223
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 216
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 221
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 217
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 218
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->mClinetListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/ICertificationListener;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 216
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
