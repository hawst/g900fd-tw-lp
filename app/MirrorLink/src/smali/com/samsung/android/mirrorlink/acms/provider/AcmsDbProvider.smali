.class public Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;
.super Landroid/content/ContentProvider;
.source "AcmsDbProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.mirrorlink.acms.provider"

.field private static final BASE_PATH:Ljava/lang/String; = "appcertdata"

.field private static final BASE_PATH_OF_DEV_ID_CERT_TABLE:Ljava/lang/String; = "appdevcertdata"

.field private static final BASE_PATH_OF_MLTIME_TABLE:Ljava/lang/String; = "mlConnectTimeTable"

.field private static final BASE_PATH_OF_OCSP_RESPONSE_TABLE:Ljava/lang/String; = "ocspPeriod"

.field public static final CERTIFICATE:I = 0x1

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

.field public static final CONTENT_URI_OF_MLTIME:Landroid/net/Uri;

.field public static final CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

.field public static final DEV_ID_CERTIFICATE:I = 0x2

.field public static final ML_TIME:I = 0x3

.field public static final OCSP_RESPONSE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "AcmsDbProvider"

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

.field private mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mWritableDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 46
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/appcertdata"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 50
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/appdevcertdata"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 60
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/mlConnectTimeTable"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_MLTIME:Landroid/net/Uri;

    .line 65
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/ocspPeriod"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 64
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

    .line 74
    new-instance v0, Landroid/content/UriMatcher;

    .line 75
    const/4 v1, -0x1

    .line 74
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 77
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.mirrorlink.acms.provider"

    const-string v2, "appcertdata"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.mirrorlink.acms.provider"

    const-string v2, "appdevcertdata"

    .line 79
    const/4 v3, 0x2

    .line 78
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 80
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.mirrorlink.acms.provider"

    const-string v2, "mlConnectTimeTable"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.mirrorlink.acms.provider"

    const-string v2, "ocspPeriod"

    .line 82
    const/4 v3, 0x4

    .line 81
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 37
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 153
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 154
    .local v1, "uriType":I
    const/4 v0, 0x0

    .line 155
    .local v0, "rowsDeleted":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 158
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 176
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown URI : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 160
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appcertdata"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 178
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 180
    return v0

    .line 163
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appdevcertdata"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 165
    goto :goto_0

    .line 167
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "mlConnectTimeTable"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 169
    goto :goto_0

    .line 171
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "ocspPeriod"

    invoke-virtual {v2, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 173
    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 108
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 109
    .local v2, "uriType":I
    const-wide/16 v0, 0x0

    .line 111
    .local v0, "id":J
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_0

    .line 112
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 114
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 145
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown URI: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 117
    :pswitch_0
    const-string v4, "maxRetry"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 118
    const-string v4, "show"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 119
    const-string v4, "isRevoked"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 120
    const-string v4, "isMemberApp"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    .line 121
    const-string v4, "AcmsDbProvider"

    const-string v5, "Certificate is wrong!"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-object v3

    .line 124
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "appcertdata"

    invoke-virtual {v4, v5, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 125
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "appcertdata/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 130
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "appdevcertdata"

    invoke-virtual {v4, v5, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 131
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "appdevcertdata/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 135
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "mlConnectTimeTable"

    invoke-virtual {v4, v5, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 136
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 137
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mlConnectTimeTable/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 140
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "ocspPeriod"

    invoke-virtual {v4, v5, v3, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 142
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ocspPeriod/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 90
    :try_start_0
    new-instance v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    .line 91
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 92
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 95
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 186
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 187
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v8, 0x0

    .line 188
    .local v8, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 189
    .local v9, "uriType":I
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 192
    :cond_0
    packed-switch v9, :pswitch_data_0

    .line 231
    :goto_0
    return-object v8

    .line 194
    :pswitch_0
    const-string v1, "appcertdata"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 198
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 202
    :pswitch_1
    const-string v1, "appdevcertdata"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 211
    :pswitch_2
    const-string v1, "mlConnectTimeTable"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 213
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 215
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 220
    :pswitch_3
    const-string v1, "ocspPeriod"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mReadableDb:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 224
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 238
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 239
    .local v1, "uriType":I
    const/4 v0, 0x0

    .line 240
    .local v0, "rowsUpdated":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 241
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mDbhelper:Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProviderHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 243
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 263
    :goto_0
    return v0

    .line 245
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appcertdata"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 246
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 249
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "appdevcertdata"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 251
    goto :goto_0

    .line 253
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "mlConnectTimeTable"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 255
    goto :goto_0

    .line 257
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->mWritableDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "ocspPeriod"

    invoke-virtual {v2, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 259
    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
