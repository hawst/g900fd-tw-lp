.class public Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
.super Ljava/lang/Object;
.source "AcmsServiceMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsServiceMonitor"

.field private static sAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Landroid/app/Service;

.field private mServiceCounter:I

.field private mStartId:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    .line 40
    iput v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mStartId:I

    .line 43
    return-void
.end method

.method public static getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    .locals 2

    .prologue
    .line 46
    const-string v0, "AcmsServiceMonitor"

    const-string v1, "AcmsServiceMonitor.getAcmsSvcMonitor() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-class v1, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    monitor-enter v1

    .line 48
    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->sAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;-><init>()V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->sAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->sAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 122
    const-class v1, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    monitor-enter v1

    .line 123
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mContext:Landroid/content/Context;

    .line 124
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->sAcmsSvcMonitor:Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    .line 122
    monitor-exit v1

    .line 126
    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized decrementSvcCounter()V
    .locals 5

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    .line 77
    const-string v2, "AcmsServiceMonitor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Decrementing - Counter value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    if-nez v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mService:Landroid/app/Service;

    if-eqz v2, :cond_0

    .line 90
    const-string v2, "AcmsServiceMonitor"

    .line 91
    const-string v3, "Counter value is 0: Stopping ACMS service"

    .line 90
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 93
    .local v0, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;

    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mStartId:I

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mService:Landroid/app/Service;

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;-><init>(ILandroid/app/Service;)V

    .line 94
    .local v1, "myRunnable":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    .end local v0    # "mainHandler":Landroid/os/Handler;
    .end local v1    # "myRunnable":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;
    :cond_0
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getSvcCounter()I
    .locals 3

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    const-string v0, "AcmsServiceMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSvcCounter - Counter value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized incrementSvcCounter()V
    .locals 3

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    .line 62
    const-string v0, "AcmsServiceMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incrementing - Counter value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized incrementSvcCounter(Landroid/app/Service;I)V
    .locals 3
    .param p1, "service"    # Landroid/app/Service;
    .param p2, "startId"    # I

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    const-string v0, "AcmsServiceMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter incrementSvcCounter with startId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    .line 68
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mService:Landroid/app/Service;

    .line 69
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mStartId:I

    if-ge v0, p2, :cond_0

    .line 70
    iput p2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mStartId:I

    .line 72
    :cond_0
    const-string v0, "AcmsServiceMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incrementing - Counter value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mServiceCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->mContext:Landroid/content/Context;

    .line 58
    return-void
.end method
