.class public Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;
.super Ljava/lang/Object;
.source "MirrorLinkManagerUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil$CERT_TYPE;,
        Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MirrorLinkManagerUtil"

.field private static final TAG_NONRESTRICTED:Ljava/lang/String; = "nonRestricted"

.field private static final TAG_RESTRICTED:Ljava/lang/String; = "restricted"

.field private static final UNABLE_TO_FETCH:Ljava/lang/String; = "UNABLE TO FETCH"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    .line 60
    return-void
.end method

.method private getLocInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/util/LocationInfo;
    .locals 8
    .param p1, "certInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 395
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 396
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1, v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 397
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 398
    .local v5, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 400
    const/4 v4, 0x0

    .line 402
    .local v4, "text":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/mirrorlink/util/LocationInfo;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/util/LocationInfo;-><init>()V

    .line 403
    .local v2, "locationInfo":Lcom/samsung/android/mirrorlink/util/LocationInfo;
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 404
    .local v0, "eventType":I
    :goto_0
    if-ne v0, v7, :cond_0

    .line 427
    return-object v2

    .line 406
    :cond_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 407
    .local v3, "tagname":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 425
    :goto_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 410
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v4

    .line 411
    goto :goto_1

    .line 414
    :pswitch_1
    const-string v6, "restricted"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 415
    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/LocationInfo;->setRestricted(Ljava/lang/String;)V

    .line 419
    :cond_1
    :goto_2
    const-string v4, ""

    .line 420
    goto :goto_1

    .line 416
    :cond_2
    const-string v6, "nonRestricted"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 417
    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/LocationInfo;->setUnrestricted(Ljava/lang/String;)V

    goto :goto_2

    .line 407
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public clearAllSelected(Ljava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 138
    const-string v7, "MirrorLinkManagerUtil"

    const-string v8, "Enter clearAllSelected"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 140
    :cond_0
    const-string v7, "MirrorLinkManagerUtil"

    const-string v8, "Nothing is there to update"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :goto_0
    return v6

    .line 144
    :cond_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 145
    .local v5, "values":Landroid/content/ContentValues;
    const-string v7, "show"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v3, "qmarksBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-lt v1, v7, :cond_2

    .line 150
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 152
    .local v2, "qmarks":Ljava/lang/String;
    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {p1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 154
    .local v0, "apparray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 155
    sget-object v7, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 156
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "packagename IN ("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 154
    invoke-virtual {v6, v7, v5, v8, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 158
    .local v4, "updatedRows":I
    const-string v6, "MirrorLinkManagerUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Exit clearAllSelected. Updated "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " rows"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v6, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "apparray":[Ljava/lang/String;
    .end local v2    # "qmarks":Ljava/lang/String;
    .end local v4    # "updatedRows":I
    :cond_2
    const-string v7, "?,"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getAllApps()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 89
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Enter getAllApps"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 92
    const-string v5, "packagename"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    .line 90
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 95
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 96
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_0
    return-object v3

    .line 99
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v7, "matchingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 101
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 104
    :cond_1
    const-string v0, "packagename"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 108
    const-string v0, "MirrorLinkManagerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exit getAllApps. sending  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v7

    .line 109
    goto :goto_0
.end method

.method public getAppListBundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 27
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 267
    const-string v2, "MirrorLinkManagerUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Enter getAppListBundle : Package "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    if-nez p1, :cond_0

    .line 270
    const-string v2, "MirrorLinkManagerUtil"

    const-string v3, "Package name is null "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v8, 0x0

    .line 366
    :goto_0
    return-object v8

    .line 274
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 275
    sget-object v3, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 276
    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "certEntities"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 277
    const-string v6, "isCertified"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "certType"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 278
    const-string v6, "queryPeriod"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "gracePeriod"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    .line 279
    const-string v6, "isRevoked"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "certInfo"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    .line 280
    const-string v6, "certState"

    aput-object v6, v4, v5

    .line 281
    const-string v5, "packagename =?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    .line 282
    const/4 v7, 0x0

    .line 274
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 284
    .local v12, "cursor":Landroid/database/Cursor;
    if-nez v12, :cond_1

    .line 285
    const-string v2, "MirrorLinkManagerUtil"

    const-string v3, "cursor is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const/4 v8, 0x0

    goto :goto_0

    .line 288
    :cond_1
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 289
    .local v8, "appBundle":Landroid/os/Bundle;
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_8

    .line 290
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 292
    const-string v2, "certEntities"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 291
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 294
    .local v16, "entities":Ljava/lang/String;
    const-string v2, "isCertified"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 293
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 296
    .local v19, "isCertified":I
    const-string v2, "queryPeriod"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 295
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 298
    .local v21, "queryTime":J
    const-string v2, "gracePeriod"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 297
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 300
    .local v17, "graceTime":J
    const-string v2, "certType"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 299
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 302
    .local v11, "certType":I
    const-string v2, "certInfo"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 301
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 304
    .local v9, "certInfo":Ljava/lang/String;
    const-string v2, "certState"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 303
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 307
    .local v10, "certState":I
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 310
    const/4 v2, 0x1

    move/from16 v0, v19

    if-ne v0, v2, :cond_2

    .line 311
    const-string v23, "VALID"

    .line 318
    .local v23, "status":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x1

    if-le v11, v2, :cond_5

    .line 319
    move-wide/from16 v24, v21

    .line 325
    .local v24, "time":J
    :goto_2
    const-string v2, "VALID"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 326
    new-instance v13, Ljava/util/Date;

    move-wide/from16 v0, v24

    invoke-direct {v13, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 327
    .local v13, "date":Ljava/util/Date;
    new-instance v14, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm"

    invoke-direct {v14, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 328
    .local v14, "df":Ljava/text/SimpleDateFormat;
    invoke-virtual {v14, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v26

    .line 334
    .end local v13    # "date":Ljava/util/Date;
    .end local v14    # "df":Ljava/text/SimpleDateFormat;
    .local v26, "validDate":Ljava/lang/String;
    :goto_3
    const/16 v20, 0x0

    .line 336
    .local v20, "loc":Lcom/samsung/android/mirrorlink/util/LocationInfo;
    :try_start_0
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->getLocInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/util/LocationInfo;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v20

    .line 345
    :goto_4
    const-string v2, "pkgName"

    move-object/from16 v0, p1

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const-string v2, "EntitiesName"

    move-object/from16 v0, v16

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v2, "AppStatus"

    move-object/from16 v0, v23

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v2, "VALID_DATE"

    move-object/from16 v0, v26

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    if-eqz v20, :cond_7

    .line 350
    const-string v2, "RESTRICTED"

    .line 351
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/util/LocationInfo;->getRestricted()Ljava/lang/String;

    move-result-object v3

    .line 350
    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v2, "NONRESTRICTED"

    .line 353
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/util/LocationInfo;->getUnrestricted()Ljava/lang/String;

    move-result-object v3

    .line 352
    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :goto_5
    const-string v2, "MirrorLinkManagerUtil"

    const-string v3, "Exit getAppListBundle"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 312
    .end local v20    # "loc":Lcom/samsung/android/mirrorlink/util/LocationInfo;
    .end local v23    # "status":Ljava/lang/String;
    .end local v24    # "time":J
    .end local v26    # "validDate":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x3

    if-lt v10, v2, :cond_3

    const/16 v2, 0xc

    if-ne v10, v2, :cond_4

    .line 313
    :cond_3
    const-string v23, "UNCHECKED"

    .line 314
    .restart local v23    # "status":Ljava/lang/String;
    goto :goto_1

    .line 315
    .end local v23    # "status":Ljava/lang/String;
    :cond_4
    const-string v23, "NONCERTIFIED"

    .restart local v23    # "status":Ljava/lang/String;
    goto :goto_1

    .line 321
    :cond_5
    move-wide/from16 v24, v17

    .restart local v24    # "time":J
    goto :goto_2

    .line 331
    :cond_6
    const-string v26, "N/A"

    .restart local v26    # "validDate":Ljava/lang/String;
    goto :goto_3

    .line 337
    .restart local v20    # "loc":Lcom/samsung/android/mirrorlink/util/LocationInfo;
    :catch_0
    move-exception v15

    .line 339
    .local v15, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v15}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_4

    .line 340
    .end local v15    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v15

    .line 342
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 355
    .end local v15    # "e":Ljava/io/IOException;
    :cond_7
    const-string v2, "RESTRICTED"

    .line 356
    const-string v3, "UNABLE TO FETCH"

    .line 355
    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v2, "NONRESTRICTED"

    .line 358
    const-string v3, "UNABLE TO FETCH"

    .line 357
    invoke-virtual {v8, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 363
    .end local v9    # "certInfo":Ljava/lang/String;
    .end local v10    # "certState":I
    .end local v11    # "certType":I
    .end local v16    # "entities":Ljava/lang/String;
    .end local v17    # "graceTime":J
    .end local v19    # "isCertified":I
    .end local v20    # "loc":Lcom/samsung/android/mirrorlink/util/LocationInfo;
    .end local v21    # "queryTime":J
    .end local v23    # "status":Ljava/lang/String;
    .end local v24    # "time":J
    .end local v26    # "validDate":Ljava/lang/String;
    :cond_8
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 364
    const-string v2, "MirrorLinkManagerUtil"

    const-string v3, "Exit getAppListBundle"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method public getApplicationShowValue(Ljava/lang/String;)Z
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 163
    const-string v0, "MirrorLinkManagerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getApplicationShowValue : Package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    if-nez p1, :cond_0

    .line 165
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Package name is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return v10

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 169
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    .line 170
    const-string v3, "show"

    aput-object v3, v2, v10

    .line 171
    const-string v3, "packagename=?"

    new-array v4, v9, [Ljava/lang/String;

    .line 172
    aput-object p1, v4, v10

    .line 173
    const/4 v5, 0x0

    .line 168
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 174
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 175
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_1
    const/4 v7, 0x0

    .line 179
    .local v7, "showValue":Z
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 180
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    const-string v0, "show"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 182
    .local v8, "value":I
    if-ne v8, v9, :cond_3

    move v7, v9

    .line 185
    .end local v8    # "value":I
    :cond_2
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 187
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Exit getApplicationShowValue"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v10, v7

    .line 188
    goto :goto_0

    .restart local v8    # "value":I
    :cond_3
    move v7, v10

    .line 182
    goto :goto_1
.end method

.method public getCertificationInfo(Ljava/lang/String;)I
    .locals 13
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x0

    const/4 v12, 0x1

    .line 192
    const-string v0, "MirrorLinkManagerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getCertificationInfo : Package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    if-nez p1, :cond_0

    .line 194
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Package name is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 232
    :goto_0
    return v0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 198
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    .line 199
    const-string v3, "isCertified"

    aput-object v3, v2, v10

    const-string v3, "isMemberApp"

    aput-object v3, v2, v12

    .line 200
    const-string v3, "packagename=?"

    new-array v4, v12, [Ljava/lang/String;

    .line 201
    aput-object p1, v4, v10

    .line 202
    const/4 v5, 0x0

    .line 197
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 205
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 206
    goto :goto_0

    .line 208
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 209
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 210
    const-string v0, "isCertified"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 211
    .local v7, "isCertified":I
    const-string v0, "isMemberApp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 212
    .local v8, "isMemberApp":I
    if-nez v7, :cond_2

    .line 213
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Application is ML_AWARE"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 215
    goto :goto_0

    .line 218
    :cond_2
    if-ne v8, v12, :cond_3

    .line 219
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Application is MEMBER CERTIFIED"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v11

    .line 221
    goto :goto_0

    .line 223
    :cond_3
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Application is CCC CERTIFIED"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v12

    .line 225
    goto :goto_0

    .line 230
    .end local v7    # "isCertified":I
    .end local v8    # "isMemberApp":I
    :cond_4
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Application is NOT PRESENT"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 232
    goto :goto_0
.end method

.method public getCertifyingEntities(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 236
    const-string v0, "MirrorLinkManagerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getCertifyingEntities : Package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    if-nez p1, :cond_0

    .line 238
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Package name is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :goto_0
    return-object v5

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 243
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    .line 244
    const-string v3, "certEntities"

    aput-object v3, v2, v8

    .line 245
    const-string v3, "packagename =?"

    new-array v4, v4, [Ljava/lang/String;

    .line 246
    aput-object p1, v4, v8

    .line 242
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 249
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 250
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 255
    const-string v0, "certEntities"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 256
    .local v7, "entities":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 257
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Exit getCertifyingEntities"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v7

    .line 258
    goto :goto_0

    .line 260
    .end local v7    # "entities":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 261
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Exit getCertifyingEntities"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSelectedApps()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 63
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Enter getSelectedApps"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 66
    const-string v3, "packagename"

    aput-object v3, v2, v9

    const-string v3, "show"

    aput-object v3, v2, v4

    .line 67
    const-string v3, "show=?"

    new-array v4, v4, [Ljava/lang/String;

    .line 68
    const-string v8, "1"

    aput-object v8, v4, v9

    .line 64
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 71
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 72
    const-string v0, "MirrorLinkManagerUtil"

    const-string v1, "Cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :goto_0
    return-object v5

    .line 75
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v7, "matchingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 77
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 80
    :cond_1
    const-string v0, "packagename"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 84
    const-string v0, "MirrorLinkManagerUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exit getSelectedApps. sending  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v7

    .line 85
    goto :goto_0
.end method

.method public setSelectedApps(Ljava/util/List;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 113
    const-string v8, "MirrorLinkManagerUtil"

    const-string v9, "Enter setSelectedApps"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 115
    :cond_0
    const-string v6, "MirrorLinkManagerUtil"

    const-string v8, "Nothing is there to update"

    invoke-static {v6, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 134
    :goto_0
    return v6

    .line 119
    :cond_1
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 120
    .local v5, "values":Landroid/content/ContentValues;
    const-string v8, "show"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .local v3, "qmarksBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-lt v1, v8, :cond_2

    .line 125
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "qmarks":Ljava/lang/String;
    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {p1, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 129
    .local v0, "apparray":[Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 130
    sget-object v8, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 131
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "packagename IN ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 129
    invoke-virtual {v7, v8, v5, v9, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 133
    .local v4, "updatedRows":I
    const-string v7, "MirrorLinkManagerUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Exit setSelectedApps. Updated "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " rows"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    .end local v0    # "apparray":[Ljava/lang/String;
    .end local v2    # "qmarks":Ljava/lang/String;
    .end local v4    # "updatedRows":I
    :cond_2
    const-string v8, "?,"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
