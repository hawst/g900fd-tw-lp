.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SSDPAliveReceiver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;
    }
.end annotation


# static fields
.field private static final MSG_SEND_PACKET:I = 0x1

.field private static mHandler:Landroid/os/Handler;


# instance fields
.field mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

.field private mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;)V
    .locals 3
    .param p1, "tmserverDevice"    # Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .prologue
    .line 1444
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1445
    const-string v0, "TMSEngine"

    const-string v1, "SSDPAliveReceiver enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 1447
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    sget-object v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    .line 1448
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->start()V

    .line 1449
    const-string v0, "TMSEngine"

    const-string v1, "SSDPAliveReceiver exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    return-void
.end method

.method static synthetic access$0(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1440
    sput-object p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$1()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1440
    sget-object v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public clean()V
    .locals 1

    .prologue
    .line 1492
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 1493
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    if-eqz v0, :cond_0

    .line 1494
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->clean()V

    .line 1495
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->interrupt()V

    .line 1497
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1502
    const-string v2, "TMSEngine"

    const-string v3, "SSDPAliveReceiver.onReceive enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1504
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.tmserver.service.SSDP_ALIVE_REQ"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1505
    sget-object v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 1506
    sget-object v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 1507
    .local v1, "msg":Landroid/os/Message;
    sget-object v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1512
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    const-string v2, "TMSEngine"

    const-string v3, "SSDPAliveReceiver.onReceive exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1513
    return-void

    .line 1509
    :cond_1
    const-string v2, "TMSEngine"

    const-string v3, "SSDPAliveReceiver.onReceive (mHandler == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
