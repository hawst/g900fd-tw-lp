.class public Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
.super Ljava/lang/Object;
.source "AcmsRevocationMngr.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OcspCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setOcspPeriod(Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    .locals 6
    .param p1, "ocspData"    # Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;

    .prologue
    const/4 v5, -0x1

    .line 1034
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;-><init>()V

    .line 1036
    .local v0, "ocspResponseEntry":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v1

    .line 1038
    .local v1, "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v3

    .line 1039
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v2

    .line 1041
    .local v2, "respFromDb":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v3

    if-eq v3, v5, :cond_3

    .line 1042
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setQueryTimeperiod(J)V

    .line 1048
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v3

    if-eq v3, v5, :cond_4

    .line 1050
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v3

    int-to-long v3, v3

    .line 1049
    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setDriveGraceTimeperiod(J)V

    .line 1055
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getBaseGracePeriod()I

    move-result v3

    if-eq v3, v5, :cond_5

    .line 1057
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getBaseGracePeriod()I

    move-result v3

    int-to-long v3, v3

    .line 1056
    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setBaseGraceTimeperiod(J)V

    .line 1063
    :cond_2
    :goto_2
    const-string v3, "AcmsRevocationMngr"

    .line 1064
    const-string v4, "OcspCallback.onSuccess() writing into ocspresponse table"

    .line 1063
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->insertOCSPResponse(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;)Z

    .line 1066
    return-object v1

    .line 1043
    :cond_3
    if-eqz v2, :cond_0

    .line 1045
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v3

    .line 1044
    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setQueryTimeperiod(J)V

    goto :goto_0

    .line 1051
    :cond_4
    if-eqz v2, :cond_1

    .line 1053
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v3

    .line 1052
    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setDriveGraceTimeperiod(J)V

    goto :goto_1

    .line 1058
    :cond_5
    if-eqz v2, :cond_2

    .line 1060
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v3

    .line 1059
    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setBaseGraceTimeperiod(J)V

    goto :goto_2
.end method


# virtual methods
.method public notifyResult(Ljava/lang/String;I)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "result"    # I

    .prologue
    .line 767
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OcspCallback.notifyResult() result "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->getAcmsManager(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    move-result-object v1

    .line 770
    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->notifyRevocationCheckResult(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    :goto_0
    return-void

    .line 771
    :catch_0
    move-exception v0

    .line 772
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "notifyResult.RemoteException()  "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onError(Ljava/util/ArrayList;IZ)V
    .locals 10
    .param p2, "errorType"    # I
    .param p3, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 780
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 782
    .local v3, "apps":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 970
    :cond_1
    :goto_1
    return-void

    .line 783
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 786
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v5

    .line 788
    .local v5, "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v0

    .line 789
    .local v0, "acmsCertificateMngr":Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    .line 790
    .local v2, "appId":Ljava/lang/String;
    const-string v7, "AcmsRevocationMngr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "OcspCallback.onError() "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 791
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 790
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    const/4 v6, 0x0

    .line 795
    .local v6, "reduce":I
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 796
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v7, v8, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_1

    .line 800
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v7

    if-gez v7, :cond_6

    .line 801
    const-string v7, "AcmsRevocationMngr"

    const-string v8, "get max retry 0 and ispending true"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 802
    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 821
    :cond_4
    :goto_2
    const/16 v7, 0xa

    if-ne p2, v7, :cond_5

    .line 822
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 823
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v7, v8, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 826
    :cond_5
    const/4 v7, 0x5

    if-ne p2, v7, :cond_a

    .line 827
    const-string v7, "AcmsRevocationMngr"

    const-string v8, "Ignoring the OCSP response"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    if-eqz p3, :cond_9

    .line 830
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 831
    sget-object v8, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v8

    .line 830
    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->notifyResult(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 804
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v7

    if-lez v7, :cond_8

    .line 805
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    if-nez v7, :cond_7

    .line 806
    const/16 v6, 0xa8

    .line 812
    :goto_3
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    .line 813
    invoke-virtual {v7, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v7

    sub-int/2addr v7, v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 812
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    goto :goto_2

    .line 809
    :cond_7
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 810
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v7

    long-to-int v6, v7

    .line 808
    goto :goto_3

    .line 816
    :cond_8
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_4

    .line 817
    const/16 v7, 0xfc0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 818
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    goto :goto_2

    .line 834
    :cond_9
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 839
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 838
    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 908
    :cond_a
    const/16 v7, 0x8

    if-eq p2, v7, :cond_b

    .line 909
    const/16 v7, 0xd

    if-eq p2, v7, :cond_b

    .line 910
    const/4 v7, 0x6

    if-eq p2, v7, :cond_b

    .line 911
    const/16 v7, 0xe

    if-eq p2, v7, :cond_b

    .line 912
    const/4 v7, 0x2

    if-ne p2, v7, :cond_12

    .line 914
    :cond_b
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 913
    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 915
    if-eqz p3, :cond_1

    .line 916
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 917
    sget-object v8, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v8

    .line 916
    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->notifyResult(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 842
    :cond_c
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 843
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v7, v8, :cond_e

    .line 844
    const-string v7, "AcmsRevocationMngr"

    .line 845
    const-string v8, "mMlCertifiedHandler -moving to drive grace"

    .line 844
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 849
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 850
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v7

    .line 848
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 847
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 852
    const-wide v7, 0x7fffffffffffffffL

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 851
    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 901
    :cond_d
    :goto_4
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 903
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_1

    .line 853
    :cond_e
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v7

    .line 854
    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .line 853
    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 854
    if-eqz v7, :cond_f

    .line 855
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v7, v8, :cond_f

    .line 856
    const-string v7, "AcmsRevocationMngr"

    .line 857
    const-string v8, "mMlCertifiedHandler -moving to base grace"

    .line 856
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 861
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 862
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v7

    .line 860
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 859
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 864
    const-wide v7, 0x7fffffffffffffffL

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 863
    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    goto :goto_4

    .line 865
    :cond_f
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v7, v8, :cond_10

    .line 866
    const-string v7, "AcmsRevocationMngr"

    .line 867
    const-string v8, "Wrong nonce-moving to base grace"

    .line 866
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 870
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    .line 871
    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertInfoToNonDriveMode(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Ljava/lang/String;

    move-result-object v7

    .line 870
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 874
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 875
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v7

    .line 873
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 872
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 877
    const-wide v7, 0x7fffffffffffffffL

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 876
    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    goto/16 :goto_4

    .line 878
    :cond_10
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v7, v8, :cond_11

    .line 879
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 880
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 883
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 884
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v7

    .line 882
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 881
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 886
    const-wide v7, 0x7fffffffffffffffL

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 885
    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 887
    const/16 v7, 0xfc0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 888
    :cond_11
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v7, v8, :cond_d

    .line 894
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 895
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v7

    .line 893
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 892
    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 898
    const-wide v7, 0x7fffffffffffffffL

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 897
    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    goto/16 :goto_4

    .line 922
    :cond_12
    const/4 v7, 0x1

    if-ne p2, v7, :cond_13

    .line 923
    const-string v7, "AcmsRevocationMngr"

    const-string v8, "ERROR_BAD_REQUEST"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 932
    :cond_13
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    if-nez v7, :cond_18

    .line 933
    const/16 v6, 0xa8

    .line 940
    :goto_5
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v4

    .line 941
    .local v4, "current_status":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v4, v7, :cond_14

    .line 942
    const-wide/16 v7, 0x18

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriod(Ljava/lang/Long;)V

    .line 945
    :cond_14
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v4, v7, :cond_15

    .line 946
    const-wide/16 v7, 0x18

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 949
    :cond_15
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v4, v7, :cond_16

    .line 950
    const-wide/16 v7, 0x18

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 953
    :cond_16
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    if-ne v4, v7, :cond_17

    .line 954
    int-to-long v7, v6

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 958
    :cond_17
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 959
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    .line 963
    if-eqz p3, :cond_0

    .line 964
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 965
    sget-object v8, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v8

    .line 964
    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->notifyResult(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 936
    .end local v4    # "current_status":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :cond_18
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 937
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v7

    long-to-int v6, v7

    .line 935
    goto :goto_5
.end method

.method public onSuccess(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;Z)V
    .locals 15
    .param p2, "ocspData"    # Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    .param p3, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 975
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 977
    .local v3, "apps":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    .line 1030
    return-void

    .line 978
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 980
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    .line 982
    .local v2, "appId":Ljava/lang/String;
    const-string v11, "AcmsRevocationMngr"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "OcspCallback.onSuccess() "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 983
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->isCertValid()Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 982
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->setOcspPeriod(Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v8

    .line 988
    .local v8, "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    const-string v11, "AcmsRevocationMngr"

    .line 989
    const-string v12, "OcspCallback.onSuccess() writing query and grace period into appcertdata table"

    .line 987
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v7

    .line 994
    .local v7, "ocspEntry":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 996
    .local v9, "queryPeriod":Ljava/lang/Long;
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v11

    .line 995
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 998
    .local v4, "baseGracePeriod":Ljava/lang/Long;
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v11

    .line 997
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1000
    .local v5, "driveGracePeriod":Ljava/lang/Long;
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-nez v11, :cond_2

    .line 1001
    const-wide/16 v11, 0xa8

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 1002
    const-string v11, "AcmsRevocationMngr"

    const-string v12, "Set ZeroQuery."

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v11

    const-string v12, "IsConnectedPref"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 1004
    .local v10, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 1005
    .local v6, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v11, "isZeroQuery"

    const/4 v12, 0x1

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1006
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1009
    .end local v6    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v10    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_2
    invoke-virtual {v1, v9}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriod(Ljava/lang/Long;)V

    .line 1011
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v11

    sget-object v12, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-ne v11, v12, :cond_3

    .line 1012
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    add-long/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 1015
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v11

    sget-object v12, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-ne v11, v12, :cond_4

    .line 1016
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    add-long/2addr v11, v13

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 1019
    :cond_4
    sget-object v11, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 1020
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 1021
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 1022
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 1023
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    sget-object v12, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v11, v12, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 1025
    if-eqz p3, :cond_0

    .line 1026
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 1027
    sget-object v12, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->PASS:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v12}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v12

    .line 1026
    invoke-virtual {p0, v11, v12}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->notifyResult(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method
