.class Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TMDisplayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TMDisplayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GuiEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;)V
    .locals 0

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 499
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 500
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.samsung.android.mirrorlink.action.SHOW_BLACK_SCREEN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 501
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xa

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 528
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 502
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v3, "com.samsung.android.mirrorlink.action.SHOW_BLACK_SCREEN_NOIMAGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 503
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 525
    .end local v0    # "action":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 526
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 504
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v3, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 506
    const-string v3, "IS_DISCONNECT"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x1

    .line 507
    .local v1, "arg1":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xb

    const/4 v6, 0x0

    invoke-static {v4, v5, v1, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto :goto_0

    .line 508
    .end local v1    # "arg1":I
    :cond_4
    const-string v3, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 509
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    const-string v4, "state"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$11(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Ljava/lang/String;)V

    .line 510
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$12(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 511
    const-string v3, "TMSDisplayService"

    const-string v4, "ACTION_PHONE_STATE_CHANGED TelephonyManager.EXTRA_STATE_IDLE"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto :goto_0

    .line 513
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$12(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 514
    const-string v3, "TMSDisplayService"

    const-string v4, "ACTION_PHONE_STATE_CHANGED TelephonyManager.EXTRA_STATE_RINGING"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xd

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 516
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$12(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 517
    const-string v3, "TMSDisplayService"

    const-string v4, "ACTION_PHONE_STATE_CHANGED TelephonyManager.EXTRA_STATE_OFFHOOK"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 519
    :cond_7
    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 520
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$12(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 521
    const-string v3, "TMSDisplayService"

    const-string v4, "CONFIGURATION_CHANGED intent Received."

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
