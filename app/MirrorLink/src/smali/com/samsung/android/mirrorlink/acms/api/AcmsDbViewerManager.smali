.class public Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;
.super Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer$Stub;
.source "AcmsDbViewerManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsDbViewerManager"

.field private static sAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer$Stub;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method public static getAcmsDbViewerMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;
    .locals 1
    .param p0, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->sAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->sAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    .line 34
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->sAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    return-object v0
.end method

.method private handleDevIdCert(Ljava/lang/String;)V
    .locals 6
    .param p1, "devId"    # Ljava/lang/String;

    .prologue
    .line 65
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getAppIdsFromDevId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 67
    .local v2, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_1

    .line 68
    const-string v3, "AcmsDbViewerManager"

    const-string v4, "No app found for given devId"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    return-void

    .line 71
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    .local v1, "appId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    .line 72
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v4

    .line 73
    invoke-virtual {v4, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v0

    .line 74
    .local v0, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    if-nez v0, :cond_3

    .line 75
    const-string v4, "AcmsDbViewerManager"

    .line 76
    const-string v5, "AcmsCore.handleDevIdCert appEntry null for given appId"

    .line 75
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isPending()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 81
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v4

    .line 83
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 82
    invoke-virtual {v4, v5, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 98
    const-string v0, "AcmsDbViewerManager"

    const-string v1, "AcmsDbViewerMnger: cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->sAcmsDbViewerManager:Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;

    .line 101
    return-void
.end method

.method public getDevId()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 53
    const-string v2, "AcmsDbViewerManager"

    const-string v3, "Entered getDevId from Shared Preferences"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    .line 55
    const-string v3, "DevIdPref"

    const/4 v4, 0x0

    .line 54
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 57
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "DevId"

    .line 58
    const-string v3, "No Value"

    .line 56
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "devId":Ljava/lang/String;
    const-string v2, "AcmsDbViewerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DevId is: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-object v0
.end method

.method public setDevId(Ljava/lang/String;)V
    .locals 5
    .param p1, "devId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 39
    const-string v2, "AcmsDbViewerManager"

    const-string v3, "Entered setDevId to Shared Preferences"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->mContext:Landroid/content/Context;

    .line 42
    const-string v3, "DevIdPref"

    const/4 v4, 0x0

    .line 41
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 43
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 44
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DevId"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 45
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 47
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/api/AcmsDbViewerManager;->handleDevIdCert(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public setDevUserSetting(Z)V
    .locals 3
    .param p1, "userSetting"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    const-string v0, "AcmsDbViewerManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Entered setUserSetting with setting value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-static {p1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->setIsDevInsert(Z)V

    .line 95
    return-void
.end method
