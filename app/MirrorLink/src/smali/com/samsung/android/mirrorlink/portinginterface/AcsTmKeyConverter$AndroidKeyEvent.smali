.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
.super Ljava/lang/Object;
.source "AcsTmKeyConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AndroidKeyEvent"
.end annotation


# instance fields
.field public mAndKeyCode:I

.field public mCapsOn:Z

.field public mLineEndKeyPressed:Z

.field public mMetaState:I

.field public mfakeModifierCode:I


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, " AndroidKeyEvent Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    .line 73
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 74
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mCapsOn:Z

    .line 75
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 76
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mLineEndKeyPressed:Z

    .line 77
    return-void
.end method
