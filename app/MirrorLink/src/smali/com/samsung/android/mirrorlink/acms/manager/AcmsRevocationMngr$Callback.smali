.class public interface abstract Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
.super Ljava/lang/Object;
.source "AcmsRevocationMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;
    }
.end annotation


# static fields
.field public static final ERROR_BAD_REQUEST:I = 0x1

.field public static final ERROR_BAD_RESPONSE:I = 0x2

.field public static final ERROR_DATABASE_OFFLINE:I = 0x4

.field public static final ERROR_INTERNAL_ERROR:I = 0x9

.field public static final ERROR_MALFORMED_REQUEST:I = 0x8

.field public static final ERROR_NONCE_INVALID:I = 0x5

.field public static final ERROR_NO_CERT_AVAILABLE:I = 0x3

.field public static final ERROR_SIGNATURE_INVALID:I = 0x6

.field public static final ERROR_SIG_REQUIRED:I = 0xc

.field public static final ERROR_TRY_LATER:I = 0x7

.field public static final ERROR_UNAUTHORIZED:I = 0xd

.field public static final ERROR_WRONG_ROOT:I = 0xe

.field public static final STATUS_INVALID:I = 0xb

.field public static final STATUS_REVOKED:I = 0xa


# virtual methods
.method public abstract notifyResult(Ljava/lang/String;I)V
.end method

.method public abstract onError(Ljava/util/ArrayList;IZ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;IZ)V"
        }
    .end annotation
.end method

.method public abstract onSuccess(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;",
            "Z)V"
        }
    .end annotation
.end method
