.class public Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
.super Ljava/lang/Object;
.source "DisplayConfig.java"


# instance fields
.field appHoriRes:I

.field appPixels:I

.field appVertRes:I

.field distance:I

.field height:I

.field horiRes:I

.field sHoriRes:I

.field sPadCols:I

.field sPadRows:I

.field sVertRes:I

.field vertRes:I

.field width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    return-void
.end method


# virtual methods
.method public getAppHoriRes()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appHoriRes:I

    return v0
.end method

.method public getAppPixels()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appPixels:I

    return v0
.end method

.method public getAppVertRes()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appVertRes:I

    return v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->distance:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->height:I

    return v0
.end method

.method public getHoriRes()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->horiRes:I

    return v0
.end method

.method public getVertRes()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->vertRes:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->width:I

    return v0
.end method

.method public getsHoriRes()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sHoriRes:I

    return v0
.end method

.method public getsPadCols()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sPadCols:I

    return v0
.end method

.method public getsPadRows()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sPadRows:I

    return v0
.end method

.method public getsVertRes()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sVertRes:I

    return v0
.end method

.method public setAppHoriRes(I)V
    .locals 0
    .param p1, "appHoriRes"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appHoriRes:I

    .line 32
    return-void
.end method

.method public setAppPixels(I)V
    .locals 0
    .param p1, "appPixels"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appPixels:I

    .line 83
    return-void
.end method

.method public setAppVertRes(I)V
    .locals 0
    .param p1, "appVertRes"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->appVertRes:I

    .line 40
    return-void
.end method

.method public setDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->distance:I

    .line 211
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 196
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->height:I

    .line 197
    return-void
.end method

.method public setHoriRes(I)V
    .locals 0
    .param p1, "horiRes"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->horiRes:I

    .line 155
    return-void
.end method

.method public setVertRes(I)V
    .locals 0
    .param p1, "vertRes"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->vertRes:I

    .line 169
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->width:I

    .line 183
    return-void
.end method

.method public setsHoriRes(I)V
    .locals 0
    .param p1, "horiRes"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sHoriRes:I

    .line 102
    return-void
.end method

.method public setsPadCols(I)V
    .locals 0
    .param p1, "sPadCols"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sPadCols:I

    .line 69
    return-void
.end method

.method public setsPadRows(I)V
    .locals 0
    .param p1, "sPadRows"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sPadRows:I

    .line 54
    return-void
.end method

.method public setsVertRes(I)V
    .locals 0
    .param p1, "vertRes"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->sVertRes:I

    .line 110
    return-void
.end method
