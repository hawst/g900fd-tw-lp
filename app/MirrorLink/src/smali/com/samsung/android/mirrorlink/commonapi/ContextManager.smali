.class public Lcom/samsung/android/mirrorlink/commonapi/ContextManager;
.super Lcom/mirrorlink/android/commonapi/IContextManager$Stub;
.source "ContextManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSContextManager"


# instance fields
.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private final mListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IContextListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IContextManager$Stub;-><init>()V

    .line 23
    const-string v0, "TMSContextManager"

    const-string v1, "Enter Constructor ContextManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 25
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager$1;-><init>(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setContextManager(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V

    .line 68
    :cond_0
    const-string v0, "TMSContextManager"

    const-string v1, "Exit Constructor ContextManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    return-object v0
.end method


# virtual methods
.method public killListener()V
    .locals 2

    .prologue
    .line 81
    const-string v0, "TMSContextManager"

    .line 82
    const-string v1, "ContextManager.killListener() : Enter"

    .line 81
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 84
    return-void
.end method

.method public notifyAudioBlocked(ILjava/lang/String;)V
    .locals 7
    .param p1, "reason"    # I
    .param p2, "packname"    # Ljava/lang/String;

    .prologue
    .line 88
    const-string v4, "TMSContextManager"

    const-string v5, "In notify On Audio Blocked CallBack API "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    if-nez v4, :cond_0

    .line 112
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 94
    .local v2, "length":I
    const-string v4, "TMSContextManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyAudioBlocked():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_1

    .line 111
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 99
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    .line 101
    .local v3, "obj":Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 102
    const-string v4, "TMSContextManager"

    .line 103
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyAudioBlocked() for package:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 103
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 102
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-interface {v4, p1}, Lcom/mirrorlink/android/commonapi/IContextListener;->onAudioBlocked(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method public notifyFramebufferBlocked(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 7
    .param p1, "reason"    # I
    .param p2, "fbBlockedRect"    # Landroid/os/Bundle;
    .param p3, "packName"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v4, "TMSContextManager"

    const-string v5, "In notify On Frame buffer Blocked CallBack API "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    if-eqz v4, :cond_0

    .line 119
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 120
    .local v2, "length":I
    const-string v4, "TMSContextManager"

    .line 121
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyFramebufferBlocked():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 120
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 139
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 141
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 124
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    .line 125
    .local v3, "obj":Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 128
    :try_start_0
    const-string v4, "TMSContextManager"

    .line 129
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyOnFramebufferBlocked():Notifying Unblocking to particular package :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 129
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 127
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-interface {v4, p1, p2}, Lcom/mirrorlink/android/commonapi/IContextListener;->onFramebufferBlocked(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyOnAudioUnBlocked(Ljava/lang/String;)V
    .locals 7
    .param p1, "packName"    # Ljava/lang/String;

    .prologue
    .line 173
    const-string v4, "TMSContextManager"

    const-string v5, "In notify On Audio UnBlocked CallBack API "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    if-eqz v4, :cond_0

    .line 176
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 177
    .local v2, "length":I
    const-string v4, "TMSContextManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyOnAudioUnBlocked():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 177
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 194
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 196
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 181
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    .line 182
    .local v3, "obj":Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 185
    :try_start_0
    const-string v4, "TMSContextManager"

    .line 186
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyOnAudioUnBlocked():Notifying Unblocking to particular package :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 186
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 184
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-interface {v4}, Lcom/mirrorlink/android/commonapi/IContextListener;->onAudioUnblocked()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyOnFramebufferUnBlocked(Ljava/lang/String;)V
    .locals 7
    .param p1, "packName"    # Ljava/lang/String;

    .prologue
    .line 144
    const-string v4, "TMSContextManager"

    const-string v5, "In notify On Frame buffer UnBlocked CallBack API "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    if-eqz v4, :cond_0

    .line 147
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 149
    .local v2, "length":I
    const-string v4, "TMSContextManager"

    .line 150
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyOnFramebufferUnBlocked():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 150
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 149
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 168
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 170
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 155
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    .line 156
    .local v3, "obj":Ljava/lang/Object;
    check-cast v3, Ljava/lang/String;

    .end local v3    # "obj":Ljava/lang/Object;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 159
    :try_start_0
    const-string v4, "TMSContextManager"

    .line 160
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContextManager.notifyOnFramebufferUnBlocked():Notifying Unblocking to particular package :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 160
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 158
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-interface {v4}, Lcom/mirrorlink/android/commonapi/IContextListener;->onFramebufferUnblocked()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IContextListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IContextListener;

    .prologue
    .line 72
    const-string v0, "TMSContextManager"

    const-string v1, "ContextManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    if-eqz p2, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 76
    :cond_0
    const-string v0, "TMSContextManager"

    const-string v1, "ContextManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public setAudioContextInformation(Z[IZ)V
    .locals 5
    .param p1, "audioContent"    # Z
    .param p2, "audioCategories"    # [I
    .param p3, "handleBlocking"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 227
    const-string v2, "TMSContextManager"

    const-string v3, "ContextManager.setAudioCtxtInfo() Enter "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v1, 0x0

    .line 229
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 230
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, "appName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 232
    const-string v2, "TMSContextManager"

    .line 233
    const-string v3, "setAudioCtxtInfo():UnAdvertised App has called Error"

    .line 232
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .end local v0    # "appName":Ljava/lang/String;
    :goto_0
    return-void

    .line 236
    .restart local v0    # "appName":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v0, p3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setAudioCtxtInfo(Z[ILjava/lang/String;Z)Z

    move-result v1

    .line 242
    const-string v2, "TMSContextManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ContextManager.setAudioCtxtInfo() Exit "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 239
    .end local v0    # "appName":Ljava/lang/String;
    :cond_1
    const-string v2, "TMSContextManager"

    const-string v3, "ContextManager().setAudioContextInformation(): mCommonAPIService.getAudioConnMngr() is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setFramebufferContextInformation(Ljava/util/List;Z)V
    .locals 4
    .param p2, "handleBlocking"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v1, "TMSContextManager"

    const-string v2, "ContextManager().setFbCtxtInfo() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "appName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 205
    const-string v1, "TMSContextManager"

    .line 206
    const-string v2, "setFbCtxtInfo():UnAdvertised App has called Error"

    .line 205
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .end local v0    # "appName":Ljava/lang/String;
    :goto_0
    return-void

    .line 209
    .restart local v0    # "appName":Ljava/lang/String;
    :cond_0
    const-string v1, "TMSContextManager"

    .line 210
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContextManager().setFbCtxtInfo() appName received is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 210
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 209
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setFbCtxtInfo(Ljava/util/List;Ljava/lang/String;Z)Z

    .line 218
    const-string v1, "TMSContextManager"

    const-string v2, "setFbCtxtInfo() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    .end local v0    # "appName":Ljava/lang/String;
    :cond_1
    const-string v1, "TMSContextManager"

    const-string v2, "ContextManager().setFbCtxtInfo(): mCommonAPIService.getVncCore() is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 249
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSContextManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ContextManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    if-nez v2, :cond_0

    .line 265
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 255
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 263
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 256
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 257
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IContextListener;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 258
    const-string v3, "TMSContextManager"

    .line 259
    const-string v4, "ContextManager(). Sending Clear Notification to AudioConnMngr"

    .line 258
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
