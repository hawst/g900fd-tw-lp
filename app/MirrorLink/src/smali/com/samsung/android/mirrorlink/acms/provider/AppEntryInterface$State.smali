.class public final enum Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
.super Ljava/lang/Enum;
.source "AppEntryInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_APPCERT_FETCH_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

.field public static final enum STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1054
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_INSTALLED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1055
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_APPCERT_FETCH_PENDING"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1056
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_APPCERT_FETCH_INPROGRESS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1057
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_APPCERT_FETCH_COMPLETED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1058
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_APPCERT_FETCH_RETRY"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1059
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_APPCERT_NON_CERTIFIED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1060
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_QUERY_PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1061
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_QUERY_INPROGRESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1062
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_QUERY_COMPLETED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1063
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_IN_QUERY_PERIOD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1064
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_IN_BASE_GRACE_PERIOD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1065
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1066
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_MLAWARE_UNCHECKED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1067
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_REVOCATION_CHECK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1068
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    const-string v1, "STATE_OCSPCERT_REVOKED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1053
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
