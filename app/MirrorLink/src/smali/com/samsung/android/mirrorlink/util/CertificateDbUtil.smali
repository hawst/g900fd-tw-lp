.class public Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;
.super Ljava/lang/Object;
.source "CertificateDbUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$CertData;,
        Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$DevIdCertData;,
        Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$MLConnectTimeData;
    }
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.mirrorlink.acms.provider"

.field private static final BASE_PATH_APPENTRY_TABLE:Ljava/lang/String; = "appcertdata"

.field private static final BASE_PATH_MLCONNECTTIME_TABLE:Ljava/lang/String; = "mlConnectTimeTable"

.field private static final BASE_PATH_OF_DEV_ID_CERT_TABLE:Ljava/lang/String; = "appdevcertdata"

.field public static final CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

.field public static final CONTENT_URI_MLTIMEENTRY:Landroid/net/Uri;

.field public static final CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/appcertdata"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 13
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/mlConnectTimeTable"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_MLTIMEENTRY:Landroid/net/Uri;

    .line 16
    const-string v0, "content://com.samsung.mirrorlink.acms.provider/appdevcertdata"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
