.class Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;
.super Ljava/lang/Thread;
.source "AcmsHttpRevokationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RevocationtHttpThread"
.end annotation


# instance fields
.field private mAppEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;

.field private mContext:Landroid/content/Context;

.field private mIsManualRevoc:Z

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;-><init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 131
    const-string v1, "AcmsHttpRevokationHandler"

    const-string v2, "RevocationtHttpThread.run() Enter "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mAppEntries:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mIsManualRevoc:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->httpRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)V

    .line 135
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    .line 136
    .local v0, "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 138
    :cond_0
    const-string v1, "AcmsHttpRevokationHandler"

    const-string v2, "Decremented Counter Value : RevocationtHttpThread.run"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v1, "AcmsHttpRevokationHandler"

    const-string v2, "RevocationtHttpThread.run() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public start(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)V
    .locals 3
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const-string v0, "AcmsHttpRevokationHandler"

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RevocationtHttpThread.start() size= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 116
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mAppEntries:Ljava/util/ArrayList;

    .line 119
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;

    .line 120
    iput-object p3, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mContext:Landroid/content/Context;

    .line 121
    iput-boolean p4, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->mIsManualRevoc:Z

    .line 123
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 124
    const-string v0, "AcmsHttpRevokationHandler"

    const-string v1, "Incremented Counter Value : RevocationtHttpThread.start"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-super {p0}, Ljava/lang/Thread;->start()V

    .line 126
    return-void
.end method
