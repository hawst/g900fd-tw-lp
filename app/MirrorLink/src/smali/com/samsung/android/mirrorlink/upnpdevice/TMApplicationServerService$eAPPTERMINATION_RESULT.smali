.class public final enum Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
.super Ljava/lang/Enum;
.source "TMApplicationServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "eAPPTERMINATION_RESULT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

.field public static final enum REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

.field public static final enum SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

.field public static final enum TERMINATE_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

.field public static final enum TERMINATE_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 122
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    const-string v1, "SOAP_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    const-string v1, "TERMINATE_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    const-string v1, "TERMINATE_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    const-string v1, "REQUEST_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    .line 120
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
