.class public interface abstract Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;
.super Ljava/lang/Object;
.source "IAppsHolderInterface.java"


# static fields
.field public static final APPSTATUS_BACKGRND:I = 0x15

.field public static final APPSTATUS_FOREGRND:I = 0x14

.field public static final APPSTATUS_NOTRUNNING:I = 0x16

.field public static final EVENT_APPSTATUS_CHANGED:I = 0xa

.field public static final EVENT_PKGSTATUS_CHANGED:I = 0xb


# virtual methods
.method public abstract deinit()Z
.end method

.method public abstract getAppList()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppStatus(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
.end method

.method public abstract regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
.end method

.method public abstract startApp(Ljava/lang/String;)Z
.end method

.method public abstract stopApp(Ljava/lang/String;)Z
.end method
