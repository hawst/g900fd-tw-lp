.class public Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
.super Ljava/lang/Object;
.source "OcspServerResponseData.java"


# instance fields
.field private mBaseGracePeriod:I

.field private mDriveGracePeriod:I

.field private mIsCertValid:Z

.field private mQueryPeriod:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBaseGracePeriod()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mBaseGracePeriod:I

    return v0
.end method

.method public getDriveGracePeriod()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mDriveGracePeriod:I

    return v0
.end method

.method public getQueryPeriod()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mQueryPeriod:I

    return v0
.end method

.method public isCertValid()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mIsCertValid:Z

    return v0
.end method

.method public setBaseGracePeriod(I)V
    .locals 0
    .param p1, "baseGracePeriod"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mBaseGracePeriod:I

    .line 43
    return-void
.end method

.method public setCertValid(Z)V
    .locals 0
    .param p1, "isCertValid"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mIsCertValid:Z

    .line 59
    return-void
.end method

.method public setDriveGracePeriod(I)V
    .locals 0
    .param p1, "driveGracePeriod"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mDriveGracePeriod:I

    .line 51
    return-void
.end method

.method public setQueryPeriod(I)V
    .locals 0
    .param p1, "queryPeriod"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->mQueryPeriod:I

    .line 35
    return-void
.end method
