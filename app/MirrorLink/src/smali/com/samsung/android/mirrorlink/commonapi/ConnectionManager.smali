.class public Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
.super Lcom/mirrorlink/android/commonapi/IConnectionManager$Stub;
.source "ConnectionManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSConnectionManager"


# instance fields
.field private mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

.field private mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

.field private final mClientListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mConnectionChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;

.field private mlSessionStatus:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 3
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IConnectionManager$Stub;-><init>()V

    .line 20
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .line 22
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mlSessionStatus:Z

    .line 42
    const-string v0, "TMSConnectionManager"

    const-string v1, "Enter Constructor ConnectionManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$1;-><init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 50
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 51
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;-><init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mConnectionChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;

    .line 52
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setConnectionManager(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V

    .line 55
    :cond_0
    const-string v0, "TMSConnectionManager"

    const-string v1, "Exit Constructor ConnectionManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method private isAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)Z
    .locals 3
    .param p1, "audioConnection"    # Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .prologue
    const/4 v0, 0x1

    .line 225
    const-string v1, "TMSConnectionManager"

    .line 226
    const-string v2, "ConnectionManager.isAudioConnectionsChanged()"

    .line 224
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    if-nez v1, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v0

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioIn()I

    move-result v1

    .line 231
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioIn()I

    move-result v2

    .line 230
    if-ne v1, v2, :cond_0

    .line 233
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioOut()I

    move-result v1

    .line 234
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioOut()I

    move-result v2

    .line 233
    if-ne v1, v2, :cond_0

    .line 241
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPayloadTypes()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->getOldPayload()Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPayloadTypes()Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 242
    if-nez v1, :cond_2

    .line 243
    const-string v1, "TMSConnectionManager"

    const-string v2, "ConnectionManager.isAudioConnectionsChanged() : mAudioConnection.getPayloadTypes is not null "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPhoneAudio()I

    move-result v1

    .line 246
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPhoneAudio()I

    move-result v2

    .line 245
    if-ne v1, v2, :cond_0

    .line 248
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getVoiceControl()I

    move-result v1

    .line 249
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getVoiceControl()I

    move-result v2

    .line 248
    if-ne v1, v2, :cond_0

    .line 253
    const-string v0, "TMSConnectionManager"

    .line 254
    const-string v1, "ConnectionManager.isAudioConnectionsChanged() : returning false"

    .line 253
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDefaultAudioConnections()Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 132
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 133
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "MEDIA_AUDIO_IN"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    const-string v1, "MEDIA_AUDIO_OUT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 137
    const-string v1, "PAYLOAD_TYPES"

    .line 138
    const-string v2, "Not Available"

    .line 137
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v1, "PHONE_AUDIO"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    const-string v1, "VOICE_CONTROL"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    return-object v0
.end method


# virtual methods
.method public getAudioConnections()Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 103
    const-string v2, "TMSConnectionManager"

    const-string v3, "ConnectionManager.getAudioConnections()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    if-nez v2, :cond_1

    .line 106
    :cond_0
    const-string v2, "TMSConnectionManager"

    const-string v3, "getAudioConnections():Tmsengine null, returning default values"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setDefaultAudioConnections()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 128
    .end local v0    # "b":Landroid/os/Bundle;
    .local v1, "b":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 111
    .end local v1    # "b":Landroid/os/Bundle;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->getAudioConnections()Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .line 113
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    if-eqz v2, :cond_2

    .line 114
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v2, "MEDIA_AUDIO_IN"

    .line 116
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioIn()I

    move-result v3

    .line 115
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v2, "MEDIA_AUDIO_OUT"

    .line 118
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioOut()I

    move-result v3

    .line 117
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v2, "PAYLOAD_TYPES"

    .line 120
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPayloadTypes()Ljava/lang/String;

    move-result-object v3

    .line 119
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "PHONE_AUDIO"

    .line 122
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPhoneAudio()I

    move-result v3

    .line 121
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    const-string v2, "VOICE_CONTROL"

    .line 124
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getVoiceControl()I

    move-result v3

    .line 123
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_1
    move-object v1, v0

    .line 128
    .end local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "b":Landroid/os/Bundle;
    goto :goto_0

    .line 126
    .end local v1    # "b":Landroid/os/Bundle;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setDefaultAudioConnections()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1
.end method

.method public getMlSessionStatus()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mlSessionStatus:Z

    return v0
.end method

.method public getRemoteDisplayConnections()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    const-string v2, "TMSConnectionManager"

    const-string v3, "ConnectionManager.getRemoteDisplayConnections()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "ret":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 151
    const-string v2, "TMSConnectionManager"

    const-string v3, "getRemoteDisplayConnections():Tmsengine null, returning default values"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 157
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 154
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getRemoteDisplayConnection()I

    move-result v0

    .line 156
    const-string v2, "TMSConnectionManager"

    const-string v3, "getRemoteDisplayConnections() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 157
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 3
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    .line 328
    if-eqz p1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 330
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getClientProfileService()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v0

    .line 331
    .local v0, "TMClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v1, :cond_1

    .line 332
    if-eqz v0, :cond_0

    .line 334
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mConnectionChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->registerConMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    .line 346
    .end local v0    # "TMClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    :cond_0
    :goto_0
    return-void

    .line 336
    .restart local v0    # "TMClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    :cond_1
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v1, :cond_0

    .line 337
    const-string v1, "TMSConnectionManager"

    .line 338
    const-string v2, "ConnectionManager.handleEvent() engine is null"

    .line 337
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    if-eqz v0, :cond_0

    .line 341
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->unRegisterConMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    goto :goto_0
.end method

.method public isMirrorLinkSessionEstablished()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    const-string v0, "TMSConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConnectionManager.isMirrorLinkSessionEstablished()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mlSessionStatus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->getMlSessionStatus()Z

    move-result v0

    return v0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "TMSConnectionManager"

    .line 70
    const-string v1, "ConnectionManager.killListener() : Enter"

    .line 69
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 72
    return-void
.end method

.method public notifyAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)V
    .locals 7
    .param p1, "audioConn"    # Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .prologue
    .line 179
    const-string v4, "TMSConnectionManager"

    .line 180
    const-string v5, "ConnectionManager.notifyAudioConnectionsChanged() -- Enter"

    .line 179
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v4, :cond_0

    .line 183
    const-string v4, "TMSConnectionManager"

    .line 184
    const-string v5, "ConnectionManager.notifyAudioConnectionsChanged() -- mClientListenerList is not null"

    .line 183
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const/4 v0, 0x0

    .line 186
    .local v0, "b":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->isAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    const-string v4, "TMSConnectionManager"

    .line 188
    const-string v5, "ConnectionManager.notifyAudioConnectionsChanged() -- audioconn is not null"

    .line 187
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 190
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "MEDIA_AUDIO_IN"

    .line 191
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioIn()I

    move-result v5

    .line 190
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 192
    const-string v4, "MEDIA_AUDIO_OUT"

    .line 193
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getMediaAudioOut()I

    move-result v5

    .line 192
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string v4, "PAYLOAD_TYPES"

    .line 195
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPayloadTypes()Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v4, "PHONE_AUDIO"

    .line 197
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getPhoneAudio()I

    move-result v5

    .line 196
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    const-string v4, "VOICE_CONTROL"

    .line 199
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->getVoiceControl()I

    move-result v5

    .line 198
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnection:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .line 202
    const-string v4, "TMSConnectionManager"

    .line 203
    const-string v5, "ConnectionManager.notifyAudioConnectionsChanged() -- begin broadcast"

    .line 202
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 205
    .local v3, "length":I
    const-string v4, "TMSConnectionManager"

    .line 206
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ConnectionManager.notifyAudioConnectionsChanged():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 206
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 205
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 217
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 221
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "i":I
    .end local v3    # "length":I
    :cond_0
    return-void

    .line 211
    .restart local v0    # "b":Landroid/os/Bundle;
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IConnectionListener;

    .line 212
    invoke-interface {v4, v0}, Lcom/mirrorlink/android/commonapi/IConnectionListener;->onAudioConnectionsChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    :catch_0
    move-exception v1

    .line 214
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyMirrorLinkSessionChanged(Z)V
    .locals 6
    .param p1, "mirrolinkSessionIsEstablished"    # Z

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->setMlSessionStatus(Z)V

    .line 261
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 262
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 263
    .local v2, "length":I
    const-string v3, "TMSConnectionManager"

    .line 264
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ConnectionManager.notifyMirrorLinkSessionChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 264
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 263
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 276
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 278
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 269
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IConnectionListener;

    .line 270
    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IConnectionListener;->onMirrorLinkSessionChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyRemoteDisplayConnectionChanged(I)V
    .locals 6
    .param p1, "remoteDisplayConnection"    # I

    .prologue
    .line 281
    const-string v3, "TMSConnectionManager"

    .line 282
    const-string v4, "ConnectionManager.notifyRemoteDisplayConnectionChanged() -- Enter"

    .line 281
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 285
    const-string v3, "TMSConnectionManager"

    .line 286
    const-string v4, "ConnectionManager.notifyRemoteDisplayConnectionChanged() -- mClientListenerList is not Null"

    .line 285
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 288
    .local v2, "length":I
    const-string v3, "TMSConnectionManager"

    .line 289
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ConnectionManager.notifyRemoteDisplayConnectionChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 289
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 288
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 301
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 303
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 294
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IConnectionListener;

    .line 295
    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IConnectionListener;->onRemoteDisplayConnectionChanged(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IConnectionListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IConnectionListener;

    .prologue
    .line 60
    const-string v0, "TMSConnectionManager"

    const-string v1, "ConnectionManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 62
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 64
    :cond_0
    const-string v0, "TMSConnectionManager"

    const-string v1, "ConnectionManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public setAudioConnManager(Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;)V
    .locals 3
    .param p1, "audioConnMngr"    # Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .prologue
    .line 75
    const-string v0, "TMSConnectionManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAudioConnManager "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mAudioConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setConnectionManager(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V

    .line 79
    :cond_0
    return-void
.end method

.method public setMlSessionStatus(Z)V
    .locals 0
    .param p1, "currentMLSessionStatus"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mlSessionStatus:Z

    .line 38
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSConnectionManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ConnectionManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    if-nez v2, :cond_0

    .line 176
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 168
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 175
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 169
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 170
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 169
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 170
    if-eqz v3, :cond_2

    .line 171
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 172
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IConnectionListener;

    .line 171
    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 168
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
