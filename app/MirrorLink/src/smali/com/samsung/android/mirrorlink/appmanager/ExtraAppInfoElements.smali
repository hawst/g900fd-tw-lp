.class public Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;
.super Ljava/lang/Object;
.source "ExtraAppInfoElements.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field public mName:Ljava/lang/String;

.field public mVal:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "chkElement"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ExtraAppInfoElements.ExtraAppInfoElements chkElement = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 35
    const-string v2, "String = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;->mName:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;->mVal:Ljava/lang/String;

    .line 38
    return-void
.end method
