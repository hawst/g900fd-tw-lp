.class final Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;
.super Ljava/lang/Object;
.source "TMServerDevice.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mDevice:Lcom/samsung/upnp/Device;


# direct methods
.method constructor <init>(Lcom/samsung/upnp/Device;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/upnp/Device;

    .prologue
    .line 859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 853
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->mDevice:Lcom/samsung/upnp/Device;

    .line 854
    const-string v0, "TMSUPnP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    .line 861
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice. AppStatusCb enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->mDevice:Lcom/samsung/upnp/Device;

    .line 863
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v1, "TMServerDevice. AppStatusCb exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    return-void
.end method


# virtual methods
.method public OnAppListUpdate(Ljava/lang/String;)V
    .locals 9
    .param p1, "appIDs"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 913
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v6, "TMServerDevice.OnAppListUpdate enter"

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 915
    .local v1, "hexAppIdBuilder":Ljava/lang/StringBuilder;
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 916
    .local v0, "appIdsArray":[Ljava/lang/String;
    array-length v6, v0

    move v4, v5

    :goto_0
    if-lt v4, v6, :cond_0

    .line 919
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v5, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 920
    .local v2, "hexappIds":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->mDevice:Lcom/samsung/upnp/Device;

    const-string v5, "AppListUpdate"

    invoke-static {v4, v5, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 921
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TMServerDevice.onAppStatusChange set state varibale OnAppListUpdate with value "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 922
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 921
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    return-void

    .line 916
    .end local v2    # "hexappIds":Ljava/lang/String;
    :cond_0
    aget-object v3, v0, v4

    .line 917
    .local v3, "id":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 916
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public OnAppStatusUpdate(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 872
    .local p1, "appId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v3, "TMServerDevice. onAppStatusChange enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    if-nez p1, :cond_0

    .line 875
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v3, "TMServerDevice. onAppStatusChange  appId list is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    :goto_0
    return-void

    .line 878
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 879
    .local v1, "stateVarValue":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 880
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 887
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMServerDevice. onAppStatusChange stateVarValue =  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 890
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    .line 891
    const-string v3, "TMServerDevice.onAppStatusChange skip set state varibale AppStatusUpdate"

    .line 890
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    const-string v3, "TMServerDevice. onAppStatusChange exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 882
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "0x"

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 883
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 884
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 895
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->mDevice:Lcom/samsung/upnp/Device;

    .line 896
    const-string v3, "AppStatusUpdate"

    .line 897
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 895
    invoke-static {v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setStateVariable(Lcom/samsung/upnp/Device;Ljava/lang/String;Ljava/lang/String;)Z

    .line 898
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/AppStatusCb;->LOG_TAG:Ljava/lang/String;

    .line 899
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMServerDevice.onAppStatusChange set state varibale AppStatusUpdate with value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 900
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 899
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 898
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
