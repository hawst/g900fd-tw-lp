.class public Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
.super Ljava/lang/Object;
.source "UpnpNotification.java"


# static fields
.field private static final BASEUID:I = 0xa

.field private static final TAG:Ljava/lang/String; = "TMSUPnP"

.field public static volatile mCurrentNotifications:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;",
            ">;"
        }
    .end annotation
.end field

.field public static mNotificationCount:I


# instance fields
.field public mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;",
            ">;"
        }
    .end annotation
.end field

.field public mAppID:I

.field public mMyNotifier:Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

.field public mNotiBody:Ljava/lang/String;

.field public mNotiID:Ljava/lang/String;

.field public mNotiTitle:Ljava/lang/String;

.field public mUid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .param p1, "mRelatedAppName"    # Ljava/lang/String;
    .param p2, "mRelatedAppId"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "TMSUPnP"

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.UpnpNotification enter mRelatedAppName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mRelatedAppId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iput p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mAppID:I

    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->notificationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private static clearNofification(I)Z
    .locals 6
    .param p0, "appId"    # I

    .prologue
    .line 259
    const-string v3, "TMSUPnP"

    .line 260
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UpnpNotification.getNofificationFromNotiId enter appId ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 260
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 259
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 263
    const/4 v1, 0x0

    .local v1, "i":I
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 274
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const-string v3, "TMSUPnP"

    .line 275
    const-string v4, "UpnpNotification.getNofificationFromNotiId clearing failed"

    .line 274
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 264
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    iget v3, v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mAppID:I

    if-ne v3, p0, :cond_2

    .line 265
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    iget-object v2, v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    .line 266
    .local v2, "notiId":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->removeActions(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;)V

    .line 267
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 268
    const-string v3, "TMSUPnP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UpnpNotification.clearNofification clearing success"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->sendNotify(Ljava/lang/String;)V

    .line 270
    const/4 v3, 0x1

    goto :goto_1

    .line 263
    .end local v2    # "notiId":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static clearNofification(Ljava/lang/String;)Z
    .locals 5
    .param p0, "notiID"    # Ljava/lang/String;

    .prologue
    .line 238
    const-string v2, "TMSUPnP"

    .line 239
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UpnpNotification.getNofificationFromNotiId enter notiID ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 239
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 238
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    if-eqz p0, :cond_0

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 242
    const/4 v1, 0x0

    .local v1, "i":I
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 253
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const-string v2, "TMSUPnP"

    .line 254
    const-string v3, "UpnpNotification.getNofificationFromNotiId clearing failed"

    .line 253
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 243
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 244
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->removeActions(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;)V

    .line 245
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 246
    const-string v2, "TMSUPnP"

    .line 247
    const-string v3, "UpnpNotification.getNofificationFromNotiId clearing success"

    .line 246
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->sendNotify(Ljava/lang/String;)V

    .line 249
    const/4 v2, 0x1

    goto :goto_1

    .line 242
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static clearNotification(I)Z
    .locals 3
    .param p0, "appId"    # I

    .prologue
    .line 209
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.clearNotification enter appId ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNofification(I)Z

    move-result v0

    return v0
.end method

.method public static clearNotification(Ljava/lang/String;)Z
    .locals 3
    .param p0, "notiId"    # Ljava/lang/String;

    .prologue
    .line 203
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.clearNotification enter notiId ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNofification(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private genarateUID()V
    .locals 3

    .prologue
    .line 44
    const-string v0, "TMSUPnP"

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.UpnpNotification enter mNotificationCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 46
    sget v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mUid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    if-nez v0, :cond_0

    .line 48
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    .line 50
    :cond_0
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    .line 51
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    .line 52
    return-void
.end method

.method private static getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 1
    .param p0, "nodeName"    # Ljava/lang/String;

    .prologue
    .line 182
    new-instance v0, Lcom/samsung/xml/Node;

    invoke-direct {v0, p0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getNotificationFromNotiId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    .locals 5
    .param p0, "notiID"    # Ljava/lang/String;

    .prologue
    .line 215
    const-string v2, "TMSUPnP"

    .line 216
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UpnpNotification.getNofificationFromNotiId enter notiID ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 216
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 215
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    if-eqz p0, :cond_0

    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 219
    const/4 v1, 0x0

    .local v1, "i":I
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 231
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const-string v2, "TMSUPnP"

    .line 232
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UpnpNotification.getNofificationFromNotiId exit no matching notification for notiid-> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 232
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 230
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 221
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_1
    const-string v3, "TMSUPnP"

    .line 222
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "UpnpNotification.getNofificationFromNotiId mCurrentNotifications.get("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ").mNotiID =  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 224
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 222
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 220
    invoke-static {v3, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 226
    sget-object v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    goto :goto_1

    .line 219
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static removeActions(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;)V
    .locals 6
    .param p0, "upnpNotification"    # Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    .prologue
    .line 280
    const-string v4, "TMSUPnP"

    const-string v5, "UpnpNotification.removeActions enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getActionList()Ljava/util/ArrayList;

    move-result-object v1

    .line 282
    .local v1, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;>;"
    if-nez v1, :cond_0

    .line 283
    const-string v4, "TMSUPnP"

    const-string v5, "UpnpNotification.removeActions actionList = null "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :goto_0
    const-string v4, "TMSUPnP"

    const-string v5, "UpnpNotification.removeActions exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    return-void

    .line 285
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "count":I
    :goto_1
    if-lt v3, v2, :cond_1

    .line 290
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 291
    const/4 v1, 0x0

    goto :goto_0

    .line 286
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    .line 287
    .local v0, "action":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->removeAction()V

    .line 288
    const-string v4, "TMSUPnP"

    const-string v5, "UpnpNotification.removeActions actions removed "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static toXml(Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0, "notiID"    # Ljava/lang/String;

    .prologue
    .line 115
    const-string v17, "TMSUPnP"

    const-string v18, "UpnpNotification.toXml enter "

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v11, 0x0

    .line 118
    .local v11, "noti":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNotificationFromNotiId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    move-result-object v11

    .line 120
    if-nez v11, :cond_0

    .line 121
    const-string v17, "TMSUPnP"

    .line 122
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "UpnpNotification.toXml no matching notification for id "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 123
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " found"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 122
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 121
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/16 v17, 0x0

    .line 179
    :goto_0
    return-object v17

    .line 127
    :cond_0
    new-instance v16, Lcom/samsung/xml/Node;

    const-string v17, "notification"

    invoke-direct/range {v16 .. v17}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 129
    .local v16, "notificationNode":Lcom/samsung/xml/Node;
    new-instance v14, Lcom/samsung/xml/Node;

    const-string v17, "notiID"

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 130
    .local v14, "notiIdNode":Lcom/samsung/xml/Node;
    iget-object v0, v11, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 131
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 133
    new-instance v15, Lcom/samsung/xml/Node;

    const-string v17, "notiTitle"

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 134
    .local v15, "notiTitleNode":Lcom/samsung/xml/Node;
    iget-object v0, v11, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiTitle:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 137
    new-instance v13, Lcom/samsung/xml/Node;

    const-string v17, "notiBody"

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 138
    .local v13, "notiBodyNode":Lcom/samsung/xml/Node;
    iget-object v0, v11, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiBody:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 139
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 141
    new-instance v12, Lcom/samsung/xml/Node;

    const-string v17, "appID"

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 142
    .local v12, "notiAppIDNode":Lcom/samsung/xml/Node;
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "0x"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v11, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mAppID:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 143
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 145
    new-instance v5, Lcom/samsung/xml/Node;

    const-string v17, "actionList"

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 147
    .local v5, "actionListNode":Lcom/samsung/xml/Node;
    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getActionList()Ljava/util/ArrayList;

    move-result-object v4

    .line 149
    .local v4, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;>;"
    if-nez v4, :cond_1

    .line 150
    const-string v17, "TMSUPnP"

    const-string v18, "UpnpNotification.toXml ActionList is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 153
    :cond_1
    const/4 v9, 0x0

    .local v9, "i":I
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    .local v8, "count":I
    :goto_1
    if-lt v9, v8, :cond_2

    .line 177
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 178
    const-string v17, "TMSUPnP"

    const-string v18, "UpnpNotification.toXml exit "

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 154
    :cond_2
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    .line 156
    .local v2, "action":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
    const-string v17, "action"

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v7

    .line 157
    .local v7, "actionNode":Lcom/samsung/xml/Node;
    const-string v17, "actionID"

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 158
    .local v3, "actionIdNode":Lcom/samsung/xml/Node;
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "0x"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->getActionId()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 158
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v7, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 162
    const-string v17, "actionName"

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 163
    .local v6, "actionNameNode":Lcom/samsung/xml/Node;
    iget-object v0, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v7, v6}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 166
    const-string v17, "launchApp"

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 167
    .local v10, "launchAppNode":Lcom/samsung/xml/Node;
    iget-boolean v0, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mLaunchApp:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 168
    const-string v17, "true"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 173
    :goto_2
    invoke-virtual {v7, v10}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 175
    invoke-virtual {v5, v7}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 153
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 171
    :cond_3
    const-string v17, "false"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public addAction(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "launcApp"    # Z

    .prologue
    .line 70
    const-string v1, "TMSUPnP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UpnpNotification.addAction enter name = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 71
    const-string v3, "launcApp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 73
    const-string v1, "TMSUPnP"

    const-string v2, "UpnpNotification.addAction mActionList is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    .line 77
    :cond_0
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    iget v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;-><init>(Ljava/lang/String;ZI)V

    .line 78
    .local v0, "action":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public deleteNoti()V
    .locals 3

    .prologue
    .line 55
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.deleteNoti enter mNotificationCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    sget v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    .line 58
    return-void
.end method

.method public getAction(I)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
    .locals 6
    .param p1, "actID"    # I

    .prologue
    const/4 v3, 0x0

    .line 93
    const-string v2, "TMSUPnP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UpnpNotification.getAction enter actID = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 94
    const-string v5, " mActionList ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 96
    const-string v2, "TMSUPnP"

    const-string v4, "UpnpNotification.getAction mActionList is null"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 107
    :goto_0
    return-object v2

    .line 100
    :cond_0
    const/4 v0, 0x0

    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "size":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 106
    const-string v2, "TMSUPnP"

    const-string v4, "UpnpNotification.getAction exit action not found"

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 107
    goto :goto_0

    .line 101
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->getActionId()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 102
    const-string v2, "TMSUPnP"

    const-string v3, "UpnpNotification.getAction return action "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    goto :goto_0

    .line 100
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getActionList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.getActionList enter mActionList = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mActionList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public invokeAction(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "notiID"    # Ljava/lang/String;
    .param p2, "actionId"    # I

    .prologue
    .line 186
    const-string v3, "TMSUPnP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UpnpNotification.invokeAction enter notiID ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 187
    const-string v5, " actionId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 186
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const/4 v2, 0x0

    .line 189
    .local v2, "ret":Z
    const/4 v0, 0x0

    .line 190
    .local v0, "action":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;
    invoke-static {p1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNotificationFromNotiId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    move-result-object v1

    .line 191
    .local v1, "noti":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    if-eqz v1, :cond_0

    .line 192
    invoke-virtual {p0, p2}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getAction(I)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    iget-object v3, v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mMyNotifier:Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

    invoke-interface {v3, v0}, Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;->invokeNoti(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;)Z

    .line 195
    const/4 v2, 0x1

    .line 198
    :cond_0
    const-string v3, "TMSUPnP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UpnpNotification.invokeAction exit ret =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return v2
.end method

.method public notificationId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    const-string v0, "TMSUPnP"

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpnpNotification.notificationId enter mNotificationCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 63
    sget v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotificationCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mUid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->genarateUID()V

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mUid:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 66
    iget v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mAppID:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setActionFather(Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;)V
    .locals 0
    .param p1, "myNotifier"    # Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mMyNotifier:Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

    .line 112
    return-void
.end method
