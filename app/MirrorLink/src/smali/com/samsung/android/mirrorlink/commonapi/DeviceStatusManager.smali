.class public Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;
.super Lcom/mirrorlink/android/commonapi/IDeviceStatusManager$Stub;
.source "DeviceStatusManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSDeviceStatusManager"


# instance fields
.field private final mClientListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IDeviceStatusManager$Stub;-><init>()V

    .line 21
    const-string v0, "TMSDeviceStatusManager"

    const-string v1, "Enter Constructor DeviceStatusManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager$1;-><init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 50
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 51
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setDeviceStatusManager(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)V

    .line 54
    :cond_0
    const-string v0, "TMSDeviceStatusManager"

    const-string v1, "Exit Constructor DeviceStatusManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    return-object v0
.end method


# virtual methods
.method public isInDriveMode()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "ret":Z
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isInDriveMode(): Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getdriveMode()Z

    move-result v0

    .line 133
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isInDriveMode(): Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 134
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 129
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    const-string v2, "TMSDeviceStatusManager"

    .line 130
    const-string v3, "DeviceStatusManager.isInDriveMode(): Tmsengine is null. returning default value"

    .line 128
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 131
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public isInNightMode()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "ret":Z
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isInNightMode(): Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 143
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getnightMode()Z

    move-result v0

    .line 150
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isInNightMode(): Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 151
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 146
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    const-string v2, "TMSDeviceStatusManager"

    .line 147
    const-string v3, "DeviceStatusManager.isInNightMode(): Tmsengine is null. returning default value"

    .line 145
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 148
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public isMicrophoneOn()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "ret":Z
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isMicrophoneOn(): Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 159
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getMicStatus()Z

    move-result v0

    .line 166
    const-string v2, "TMSDeviceStatusManager"

    const-string v3, "DeviceStatusManager.isMicrophoneOn(): Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 167
    .end local v0    # "ret":Z
    .local v1, "ret":I
    :goto_0
    return v1

    .line 162
    .end local v1    # "ret":I
    .restart local v0    # "ret":Z
    :cond_0
    const-string v2, "TMSDeviceStatusManager"

    .line 163
    const-string v3, "DeviceStatusManager.isMicrophoneOn(): Tmsengine is null. returning default value"

    .line 161
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 164
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "TMSDeviceStatusManager"

    .line 68
    const-string v1, "DeviceStatusInfoManager.killListener() : Enter"

    .line 67
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 70
    return-void
.end method

.method public notifyDriveModeChange(Z)V
    .locals 6
    .param p1, "drivemode"    # Z

    .prologue
    .line 73
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 74
    .local v2, "length":I
    const-string v3, "TMSDeviceStatusManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceStatusManager.notifyDriveModeChange():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 75
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 74
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 85
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 86
    return-void

    .line 79
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;->onDriveModeChange(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyMicrohphoneStatusChanged(Z)V
    .locals 6
    .param p1, "micInput"    # Z

    .prologue
    .line 89
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 90
    .local v2, "length":I
    const-string v3, "TMSDeviceStatusManager"

    .line 91
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceStatusManager.notifyMicrohphoneStatusChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 91
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 90
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 102
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 103
    return-void

    .line 96
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    .line 97
    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;->onMicrophoneStatusChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyNightModeChanged(Z)V
    .locals 6
    .param p1, "nightMode"    # Z

    .prologue
    .line 106
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 107
    .local v2, "length":I
    const-string v3, "TMSDeviceStatusManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceStatusManager.notifyNightModeChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 107
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 118
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 119
    return-void

    .line 112
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;->onNightModeChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    .prologue
    .line 58
    const-string v0, "TMSDeviceStatusManager"

    const-string v1, "DeviceStatusManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 62
    :cond_0
    const-string v0, "TMSDeviceStatusManager"

    const-string v1, "DeviceStatusManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public setMicrophoneOpen(ZZ)Z
    .locals 3
    .param p1, "micInput"    # Z
    .param p2, "voiceInput"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 176
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setMicOpen(ZZ)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 177
    const-string v1, "TMSDeviceStatusManager"

    const-string v2, "DeviceStatusManager.setMicrophoneOpen(): setMicOpen returned true"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v0, 0x1

    .line 185
    :goto_0
    return v0

    .line 180
    :cond_0
    const-string v1, "TMSDeviceStatusManager"

    const-string v2, "DeviceStatusManager.setMicrophoneOpen(): setMicOpen returned false... need to check!"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :cond_1
    const-string v1, "TMSDeviceStatusManager"

    const-string v2, "DeviceStatusManager.setMicrophoneOpen(): condition(Tmsengine && getVncCore) is null. returning default value"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSDeviceStatusManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceStatusInfoManger.UnRegister: PackName= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 192
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    if-nez v2, :cond_0

    .line 207
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 198
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 206
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 199
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 200
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 199
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 200
    if-eqz v3, :cond_2

    .line 201
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 202
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;

    .line 201
    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_2

    .line 198
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
