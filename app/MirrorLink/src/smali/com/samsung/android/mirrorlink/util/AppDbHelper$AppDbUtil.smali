.class public final Lcom/samsung/android/mirrorlink/util/AppDbHelper$AppDbUtil;
.super Ljava/lang/Object;
.source "AppDbHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/util/AppDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppDbUtil"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final COL_CATEGORY:Ljava/lang/String; = "category"

.field public static final COL_CERTIFIED:Ljava/lang/String; = "certified"

.field public static final COL_CERTIFIED_APPID:Ljava/lang/String; = "certified_appId"

.field public static final COL_CERTIFIED_APPUUID:Ljava/lang/String; = "certified_appUUid"

.field public static final COL_CERTIFIED_PROPERTIES:Ljava/lang/String; = "certified_properties"

.field public static final COL_CERTIFIED_SIGNATURE:Ljava/lang/String; = "certified_signature"

.field public static final COL_PACKAGENAME:Ljava/lang/String; = "packageName"

.field public static final DB_NAME:Ljava/lang/String; = "appdb"

.field public static final DB_VERSION:I = 0x1

.field public static final TABLE_NAME:Ljava/lang/String; = "appdata"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 100
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 101
    const-string v2, "packageName"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 102
    const-string v2, "category"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 103
    const-string v2, "certified"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 104
    const-string v2, "certified_appId"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 105
    const-string v2, "certified_appUUid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 106
    const-string v2, "certified_properties"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 107
    const-string v2, "certified_signature"

    aput-object v2, v0, v1

    .line 100
    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper$AppDbUtil;->ALL_COLUMNS:[Ljava/lang/String;

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
