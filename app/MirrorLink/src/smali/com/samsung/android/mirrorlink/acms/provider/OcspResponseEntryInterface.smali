.class public Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
.super Ljava/lang/Object;
.source "OcspResponseEntryInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface$OCSPResponsePeriod;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsOcspResponseEntryInterface"

.field private static sOcspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public static getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->sOcspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->sOcspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    .line 46
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->sOcspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    return-object v0
.end method


# virtual methods
.method public cleanUp()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->sOcspResponseEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    .line 162
    return-void
.end method

.method public getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 75
    new-instance v7, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    invoke-direct {v7}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;-><init>()V

    .line 76
    .local v7, "ocspResponseEntry":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 77
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "getOcspresponseEntry: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :goto_0
    return-object v7

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

    .line 82
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface$OCSPResponsePeriod;->ALL_COLUMNS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    .line 80
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 83
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 84
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "getOcspresponseEntry: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 87
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "getOcspresponseEntry: cursor count is 0"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 91
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 94
    const-string v0, "queryPeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 93
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setQueryTimeperiod(J)V

    .line 96
    const-string v0, "driveGracePeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 95
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setDriveGraceTimeperiod(J)V

    .line 98
    const-string v0, "baseGracePeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 97
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->setBaseGraceTimeperiod(J)V

    .line 100
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public insertOCSPResponse(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;)Z
    .locals 4
    .param p1, "ocspResponseEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 51
    const-string v1, "AcmsOcspResponseEntryInterface"

    const-string v2, "insertOCSPResponse: context is NULL"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const/4 v1, 0x0

    .line 70
    :goto_0
    return v1

    .line 54
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 55
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "AcmsOcspResponseEntryInterface"

    const-string v2, "insertOCSP Response : enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->updateOCSPResponse(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 58
    const-string v1, "AcmsOcspResponseEntryInterface"

    .line 59
    const-string v2, "insertOCSP Response : Response not present, hence inserting"

    .line 58
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "queryPeriod"

    .line 61
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 60
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 62
    const-string v1, "driveGracePeriod"

    .line 63
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 64
    const-string v1, "baseGracePeriod"

    .line 65
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 67
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 68
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

    .line 67
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 70
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateOCSPResponse(Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;)Z
    .locals 10
    .param p1, "ocspResponseEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 107
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "updateOCSPResponse: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 140
    :goto_0
    return v0

    .line 110
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 111
    .local v8, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 111
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 114
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 115
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "updateOCSPResponse: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 116
    goto :goto_0

    .line 118
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 119
    const-string v0, "AcmsOcspResponseEntryInterface"

    .line 120
    const-string v1, "updateOCSPResponse :  already present, hence updating"

    .line 119
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v0, "queryPeriod"

    .line 123
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 122
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 124
    const-string v0, "driveGracePeriod"

    .line 125
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 124
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 126
    const-string v0, "baseGracePeriod"

    .line 127
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 126
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 131
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_OCSP_RESPONSE_TABLE:Landroid/net/Uri;

    .line 130
    invoke-virtual {v0, v1, v8, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 133
    .local v7, "updated":I
    const-string v0, "AcmsOcspResponseEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Row in OCSP Response table"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 140
    const/4 v0, 0x1

    goto :goto_0

    .line 135
    .end local v7    # "updated":I
    :cond_2
    const-string v0, "AcmsOcspResponseEntryInterface"

    const-string v1, "No entry found in OCSP Response Table"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 137
    goto/16 :goto_0
.end method
