.class Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;
.super Landroid/os/Handler;
.source "AcsInternalPresentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    .line 57
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 75
    :goto_0
    return-void

    .line 61
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->access$1(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;Z)V

    .line 62
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->showInternalBlackScreen()V

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    # getter for: Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isMLRunningViewShowing:Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->access$2(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->access$1(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;Z)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    # invokes: Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->showMirrorLinkRunningScreen()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->access$3(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V

    .line 72
    :goto_1
    invoke-static {p0, v2, v1, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {p0, v2}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;->removeMessages(I)V

    goto :goto_1

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
