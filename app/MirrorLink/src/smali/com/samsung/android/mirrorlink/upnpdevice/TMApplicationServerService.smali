.class public Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
.super Ljava/lang/Object;
.source "TMApplicationServerService.java"

# interfaces
.implements Lcom/samsung/upnp/control/ActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;,
        Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;,
        Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    }
.end annotation


# static fields
.field private static final APPSTATUS_BACKGRND:I = 0x15

.field private static final APPSTATUS_FOREGRND:I = 0x14

.field private static final APPSTATUS_NOTRUNNING:I = 0x16

.field private static final APP_LIST:Ljava/lang/String; = "<appList"

.field private static final CERTIFICATION:Ljava/lang/String; = "<certification"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSAppServerSvc"

.field private static final RTP:Ljava/lang/String; = "RTP"

.field private static final TM_APP_CONVERSATIONAL_AUDIO_CATEGORY:I = -0xfffffe0


# instance fields
.field private mBtMac:Ljava/lang/String;

.field private mBtStartConnection:Z

.field private mCdbIp:Ljava/lang/String;

.field private mCdbPort:I

.field private mContext:Landroid/content/Context;

.field private mDapIp:Ljava/lang/String;

.field private mDapPort:I

.field private mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mKeyMngr:Landroid/app/KeyguardManager;

.field private mRtpClientPort:I

.field private mRtpIp:Ljava/lang/String;

.field private mRtpServerPort:I

.field private mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

.field private mVncIp:Ljava/lang/String;

.field private mVncPort:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mDeviceAppManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 133
    const-string v0, "TMSAppServerSvc"

    .line 134
    const-string v1, "TMApplicationServerService. TMApplicationServerService enter "

    .line 133
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 136
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    .line 140
    const-string v0, "TMSAppServerSvc"

    .line 141
    const-string v1, "TMApplicationServerService. TMApplicationServerService exit "

    .line 140
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 148
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    .line 150
    return-void
.end method

.method private declared-synchronized formAppListXml(Lcom/samsung/upnp/Action;)Ljava/lang/String;
    .locals 35
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 874
    monitor-enter p0

    :try_start_0
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService. formAppListXml enter"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const/4 v5, 0x0

    .line 876
    .local v5, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const/16 v27, -0x1

    .line 877
    .local v27, "profileID":I
    const/4 v6, 0x0

    .line 878
    .local v6, "appListFilter":Ljava/lang/String;
    const/16 v28, 0x0

    .line 879
    .local v28, "profileIDFound":Z
    const/16 v26, 0x0

    .line 881
    .local v26, "prfl":I
    const/4 v14, 0x0

    .line 882
    .local v14, "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const/16 v24, 0x0

    .line 883
    .local v24, "ml1_0Session":Z
    const/16 v20, 0x0

    .line 885
    .local v20, "isVWHeadUnit":Z
    const-string v30, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v11

    .line 886
    .local v11, "arg":Lcom/samsung/upnp/Argument;
    if-nez v11, :cond_0

    .line 888
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService.formAppListXml failed No ProfileID"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v30

    .line 890
    sget-object v31, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v31

    .line 889
    move-object/from16 v0, p1

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 891
    const/16 v29, 0x0

    .line 1178
    :goto_0
    monitor-exit p0

    return-object v29

    .line 894
    :cond_0
    :try_start_1
    invoke-virtual {v11}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v27

    .line 897
    :goto_1
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v30

    move/from16 v0, v26

    move/from16 v1, v30

    if-lt v0, v1, :cond_1

    .line 910
    :goto_2
    if-nez v28, :cond_4

    .line 912
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService. formAppListXml Error in starting app.no such client profile ID"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v30

    .line 915
    sget-object v31, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v31

    .line 914
    move-object/from16 v0, p1

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 916
    const/16 v29, 0x0

    goto :goto_0

    .line 899
    :cond_1
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v30

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v30, v0

    move/from16 v0, v30

    move/from16 v1, v27

    if-ne v0, v1, :cond_3

    .line 901
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v30

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    check-cast v14, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 902
    .restart local v14    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget v0, v14, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    move/from16 v30, v0

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    iget v0, v14, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    move/from16 v30, v0

    if-nez v30, :cond_2

    .line 903
    const-string v30, "TMSAppServerSvc"

    const-string v31, "Mirrorlink Client Session version is 1.0"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    const/16 v24, 0x1

    .line 906
    :cond_2
    const/16 v28, 0x1

    .line 907
    goto :goto_2

    .line 897
    :cond_3
    add-int/lit8 v26, v26, 0x1

    goto :goto_1

    .line 920
    :cond_4
    const-string v30, "AppListingFilter"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v11

    .line 921
    if-nez v11, :cond_5

    .line 923
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService.formAppListXml failed No AppListingFilter"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v30

    .line 925
    sget-object v31, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v31

    .line 924
    move-object/from16 v0, p1

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 926
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 929
    :cond_5
    invoke-virtual {v11}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 931
    const-string v30, "TMSAppServerSvc"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "TMApplicationServerService. formAppListXml appListfilter is "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    if-nez v6, :cond_6

    .line 935
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService. formAppListXml Error in SOAP request xml"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v30

    .line 938
    sget-object v31, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v31

    .line 937
    move-object/from16 v0, p1

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 939
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 943
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getAppList(I)Ljava/util/List;

    move-result-object v5

    .line 945
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v6}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->filterAppsByAppInfo(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 947
    .local v18, "filteredAppList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    if-nez v18, :cond_7

    .line 949
    const-string v30, "TMSAppServerSvc"

    const-string v31, "TMApplicationServerService.formAppListXml filteredAppList = null"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 953
    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v25

    .line 954
    .local v25, "noOfApps":I
    const-string v30, "TMSAppServerSvc"

    .line 955
    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "TMApplicationServerService. formAppListXml noOfApps = "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 956
    move-object/from16 v0, v31

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    .line 955
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 954
    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    new-instance v7, Lcom/samsung/xml/Node;

    const-string v30, "appList"

    move-object/from16 v0, v30

    invoke-direct {v7, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 974
    .local v7, "appListNode":Lcom/samsung/xml/Node;
    const-string v30, "TMSAppServerSvc"

    .line 975
    const-string v31, "TMApplicationServerService. formAppListXml getting the app info and forming the xml"

    .line 974
    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->setCurrentClient(Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;)V

    .line 979
    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 983
    .local v12, "clientApps":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    const/4 v13, 0x0

    .line 985
    .local v13, "clientId":Ljava/lang/Object;
    :try_start_2
    const-string v30, "android.os.SystemProperties"

    invoke-static/range {v30 .. v30}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v30

    const-string v31, "get"

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const-class v34, Ljava/lang/String;

    aput-object v34, v32, v33

    invoke-virtual/range {v30 .. v32}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v22

    .line 986
    .local v22, "listener":Ljava/lang/reflect/Method;
    const/16 v30, 0x0

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const-string v33, "net.mirrorlink.clientid"

    aput-object v33, v31, v32

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v13

    .line 997
    .end local v13    # "clientId":Ljava/lang/Object;
    .end local v22    # "listener":Ljava/lang/reflect/Method;
    :goto_3
    :try_start_3
    const-string v30, "VW-Mibstd2"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    .line 998
    const-string v30, "VW MirrorLink"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    .line 999
    const-string v30, "MLC01"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_8

    .line 1000
    const-string v30, "VWAG_VOLKSWAGEN"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1001
    :cond_8
    const/16 v20, 0x1

    .line 1005
    :cond_9
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_4
    move/from16 v0, v19

    move/from16 v1, v25

    if-lt v0, v1, :cond_a

    .line 1174
    invoke-virtual {v7}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1175
    .local v10, "apps":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;

    move-result-object v30

    const-string v31, "appList"

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v0, v10, v1}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->prepareSignedXml(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 1176
    .local v29, "signedAppList":Ljava/lang/String;
    const-string v30, "<appList"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v29

    .line 1178
    goto/16 :goto_0

    .line 987
    .end local v10    # "apps":Ljava/lang/String;
    .end local v19    # "i":I
    .end local v29    # "signedAppList":Ljava/lang/String;
    .restart local v13    # "clientId":Ljava/lang/Object;
    :catch_0
    move-exception v16

    .line 988
    .local v16, "e":Ljava/lang/NoSuchMethodException;
    const-string v30, "TMSAppServerSvc"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "SystemProperties.get() "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 874
    .end local v5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .end local v6    # "appListFilter":Ljava/lang/String;
    .end local v7    # "appListNode":Lcom/samsung/xml/Node;
    .end local v11    # "arg":Lcom/samsung/upnp/Argument;
    .end local v12    # "clientApps":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    .end local v13    # "clientId":Ljava/lang/Object;
    .end local v14    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v16    # "e":Ljava/lang/NoSuchMethodException;
    .end local v18    # "filteredAppList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .end local v20    # "isVWHeadUnit":Z
    .end local v24    # "ml1_0Session":Z
    .end local v25    # "noOfApps":I
    .end local v26    # "prfl":I
    .end local v27    # "profileID":I
    .end local v28    # "profileIDFound":Z
    :catchall_0
    move-exception v30

    monitor-exit p0

    throw v30

    .line 989
    .restart local v5    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .restart local v6    # "appListFilter":Ljava/lang/String;
    .restart local v7    # "appListNode":Lcom/samsung/xml/Node;
    .restart local v11    # "arg":Lcom/samsung/upnp/Argument;
    .restart local v12    # "clientApps":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    .restart local v13    # "clientId":Ljava/lang/Object;
    .restart local v14    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .restart local v18    # "filteredAppList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .restart local v20    # "isVWHeadUnit":Z
    .restart local v24    # "ml1_0Session":Z
    .restart local v25    # "noOfApps":I
    .restart local v26    # "prfl":I
    .restart local v27    # "profileID":I
    .restart local v28    # "profileIDFound":Z
    :catch_1
    move-exception v16

    .line 990
    .local v16, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_4
    const-string v30, "TMSAppServerSvc"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "SystemProperties.get() "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 991
    .end local v16    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v16

    .line 992
    .local v16, "e":Ljava/lang/IllegalAccessException;
    const-string v30, "TMSAppServerSvc"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "SystemProperties.get() "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 993
    .end local v16    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v16

    .line 994
    .local v16, "e":Ljava/lang/ClassNotFoundException;
    const-string v30, "TMSAppServerSvc"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "SystemProperties.get() "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1028
    .end local v13    # "clientId":Ljava/lang/Object;
    .end local v16    # "e":Ljava/lang/ClassNotFoundException;
    .restart local v19    # "i":I
    :cond_a
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v30

    if-nez v30, :cond_c

    .line 1029
    const-string v30, "TMSAppServerSvc"

    const-string v31, "Remoting info is null. Hence moving to next app"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    :cond_b
    :goto_5
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_4

    .line 1033
    :cond_c
    const-string v31, "TMSAppServerSvc"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v32, "formAppListXml app package name ["

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v32, "] "

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    .line 1034
    const-string v32, " "

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 1033
    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    const-string v31, "com.samsung.android.app.mirrorlink"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1036
    const-string v30, "TMSAppServerSvc"

    const-string v31, "MirrorLink itself should not be shown on HU"

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1041
    :cond_d
    if-eqz v20, :cond_e

    const-string v31, "com.android.phone"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1042
    const-string v30, "TMSAppServerSvc"

    const-string v31, "Phone app is set its protocol ID as NONE for VW HU "

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v31, "NONE"

    const/16 v32, 0x0

    const/16 v33, 0x0

    invoke-virtual/range {v30 .. v33}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v31, "free"

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 1049
    :cond_e
    if-eqz v24, :cond_10

    .line 1051
    const-string v31, "CDB"

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v30

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1052
    const-string v30, "TMSAppServerSvc"

    const-string v31, "CDB has to be blocked from ML session 1.0 "

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1056
    :cond_f
    const v31, -0xfffffe0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v30

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    move/from16 v30, v0

    move/from16 v0, v31

    move/from16 v1, v30

    if-ne v0, v1, :cond_10

    .line 1057
    const-string v30, "TMSAppServerSvc"

    const-string v31, "Conversational Audio must be blocked from ML session 1.0 "

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1064
    :cond_10
    const-string v30, "app"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v9

    .line 1066
    .local v9, "appNode":Lcom/samsung/xml/Node;
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    if-eqz v30, :cond_b

    .line 1068
    invoke-interface/range {v18 .. v19}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1073
    .local v23, "mTMSAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v12, v0}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_11

    .line 1075
    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    move/from16 v30, v0

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->setAppID(I)I

    .line 1077
    :cond_11
    const-string v30, "appID"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v4

    .line 1078
    .local v4, "appIDNode":Lcom/samsung/xml/Node;
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "0x"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    move/from16 v31, v0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1079
    invoke-virtual {v9, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1081
    const-string v30, "name"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v8

    .line 1082
    .local v8, "appNameNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v8, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1083
    invoke-virtual {v9, v8}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1085
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v30, :cond_12

    .line 1087
    const-string v30, "description"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v15

    .line 1088
    .local v15, "descriptionNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v15, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1089
    invoke-virtual {v9, v15}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1096
    .end local v15    # "descriptionNode":Lcom/samsung/xml/Node;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->setCurrrntAppInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    .line 1097
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createVariantNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createProviderNameNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createProviderUrlNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1101
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertifcateUrl:Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v30, :cond_13

    .line 1104
    const-string v30, "appCertificateURL"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v3

    .line 1105
    .local v3, "appCrtfctNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertifcateUrl:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1106
    invoke-virtual {v9, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1109
    .end local v3    # "appCrtfctNode":Lcom/samsung/xml/Node;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createIconNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createAllwdPrfIdNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createRemotingInfoNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createAppInfoNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createDisplayInfoNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createAudioInfoNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->createResourceStatusNode()Lcom/samsung/xml/Node;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1160
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mExtraEleList:Ljava/util/List;

    move-object/from16 v30, v0

    if-eqz v30, :cond_14

    .line 1162
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_6
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mExtraEleList:Ljava/util/List;

    move-object/from16 v30, v0

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v21

    move/from16 v1, v30

    if-lt v0, v1, :cond_15

    .line 1169
    .end local v21    # "j":I
    :cond_14
    invoke-virtual {v7, v9}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto/16 :goto_5

    .line 1164
    .restart local v21    # "j":I
    :cond_15
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mExtraEleList:Ljava/util/List;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;->mName:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v17

    .line 1165
    .local v17, "e1":Lcom/samsung/xml/Node;
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mExtraEleList:Ljava/util/List;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/ExtraAppInfoElements;->mVal:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1166
    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1162
    add-int/lit8 v21, v21, 0x1

    goto :goto_6
.end method

.method private formAppStatusListXml(Lcom/samsung/upnp/Action;)Ljava/lang/String;
    .locals 32
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 1621
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml enter"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1622
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 1623
    .local v12, "backgroundAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v23, Ljava/util/LinkedList;

    invoke-direct/range {v23 .. v23}, Ljava/util/LinkedList;-><init>()V

    .line 1624
    .local v23, "notRunningAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v28, Ljava/util/LinkedList;

    invoke-direct/range {v28 .. v28}, Ljava/util/LinkedList;-><init>()V

    .line 1626
    .local v28, "topRunningApp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v19, 0x0

    .line 1627
    .local v19, "isAppfound":Z
    new-instance v25, Ljava/util/LinkedList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getApplicationMap()Ljava/util/Map;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1628
    .local v25, "savedAppList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v29, "AppID"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v11

    .line 1629
    .local v11, "arg":Lcom/samsung/upnp/Argument;
    if-nez v11, :cond_0

    .line 1631
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService.formAppStatusListXml failed No AppID"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1632
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v29

    .line 1633
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v30

    .line 1632
    move-object/from16 v0, p1

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1634
    const/16 v29, 0x0

    .line 1824
    :goto_0
    return-object v29

    .line 1636
    :cond_0
    invoke-virtual {v11}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 1638
    .local v5, "appID":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1640
    .local v7, "appId":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v29

    const/16 v30, 0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-le v0, v1, :cond_4

    const-string v29, "0x"

    const/16 v30, 0x0

    const/16 v31, 0x2

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 1642
    const-string v29, "TMSAppServerSvc"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "TMApplicationServerService.launchApp appid is hex appId = "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    const/16 v29, 0x2

    move/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1644
    .local v17, "hexAppId":Ljava/lang/String;
    const-string v29, "TMSAppServerSvc"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "TMApplicationServerService.launchApp appid is hex hexAppId = "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    const/4 v4, 0x0

    .line 1647
    .local v4, "Id":I
    const/16 v29, 0x10

    :try_start_0
    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1654
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1670
    .end local v4    # "Id":I
    .end local v17    # "hexAppId":Ljava/lang/String;
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v21

    .line 1672
    .local v21, "noOfApps":I
    const-string v29, "*"

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1

    .line 1674
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml need to send all applications status"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v20

    .line 1679
    .local v20, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_2
    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/ListIterator;->hasNext()Z

    move-result v29

    if-nez v29, :cond_6

    .line 1700
    new-instance v9, Lcom/samsung/xml/Node;

    const-string v29, "appStatusList"

    move-object/from16 v0, v29

    invoke-direct {v9, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1715
    .local v9, "appStatusListNode":Lcom/samsung/xml/Node;
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_3
    move/from16 v0, v18

    move/from16 v1, v21

    if-lt v0, v1, :cond_9

    .line 1811
    :cond_3
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml formAppStatusListXml()exit"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1813
    const-string v29, "*"

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_12

    if-nez v19, :cond_12

    .line 1815
    const-string v29, "TMSAppServerSvc"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "TMApplicationServerService. formAppStatusListXml Error in finding app status of app with id = "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1816
    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 1815
    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1817
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v29

    .line 1818
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v30

    .line 1817
    move-object/from16 v0, p1

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1819
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 1648
    .end local v9    # "appStatusListNode":Lcom/samsung/xml/Node;
    .end local v18    # "i":I
    .end local v20    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .end local v21    # "noOfApps":I
    .restart local v4    # "Id":I
    .restart local v17    # "hexAppId":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 1649
    .local v16, "e":Ljava/lang/NumberFormatException;
    const-string v29, "TMSAppServerSvc"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "TMApplicationServerService.actionControlReceived invalid app id exception"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v29

    .line 1651
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v30

    .line 1650
    move-object/from16 v0, p1

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1652
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 1656
    .end local v4    # "Id":I
    .end local v16    # "e":Ljava/lang/NumberFormatException;
    .end local v17    # "hexAppId":Ljava/lang/String;
    :cond_4
    const-string v29, "*"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 1658
    move-object v7, v5

    .line 1659
    goto/16 :goto_1

    .line 1662
    :cond_5
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService.actionControlReceived invalid app id for get app status"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1663
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v29

    .line 1664
    sget-object v30, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v30

    .line 1663
    move-object/from16 v0, p1

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1665
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 1681
    .restart local v20    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    .restart local v21    # "noOfApps":I
    :cond_6
    invoke-interface/range {v20 .. v20}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1682
    .local v8, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    move/from16 v29, v0

    const/16 v30, 0x15

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_7

    .line 1684
    const-string v29, "TMSAppServerSvc"

    const-string v30, " to backgrnd"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685
    iget-object v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687
    :cond_7
    iget v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    move/from16 v29, v0

    const/16 v30, 0x16

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_8

    .line 1689
    const-string v29, "TMSAppServerSvc"

    const-string v30, " to not running"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    iget-object v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692
    :cond_8
    iget v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    move/from16 v29, v0

    const/16 v30, 0x14

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_2

    .line 1694
    const-string v29, "TMSAppServerSvc"

    const-string v30, " to frgrnd"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    iget-object v0, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-interface/range {v28 .. v29}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1717
    .end local v8    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v9    # "appStatusListNode":Lcom/samsung/xml/Node;
    .restart local v18    # "i":I
    :cond_9
    const/4 v10, 0x0

    .line 1718
    .local v10, "appStatusNode":Lcom/samsung/xml/Node;
    const/4 v6, 0x0

    .line 1720
    .local v6, "appIDNode":Lcom/samsung/xml/Node;
    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v29

    iget v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1722
    .local v14, "currentAppId":I
    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-object/from16 v0, v29

    iget-object v15, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1723
    .local v15, "currentAppName":Ljava/lang/String;
    const-string v29, "appStatus"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v10

    .line 1724
    const-string v29, "appID"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 1727
    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "0x"

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1733
    const/16 v22, 0x0

    .local v22, "noOfClientProfile":I
    :goto_4
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v29

    move/from16 v0, v22

    move/from16 v1, v29

    if-lt v0, v1, :cond_b

    .line 1806
    const-string v29, "*"

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_a

    if-nez v19, :cond_3

    .line 1715
    :cond_a
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_3

    .line 1735
    :cond_b
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 1737
    .local v3, "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v13

    .line 1747
    .local v13, "currentAppIDList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    const-string v29, "*"

    move-object/from16 v0, v29

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 1749
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v13, v0}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_e

    .line 1733
    :cond_c
    :goto_5
    add-int/lit8 v22, v22, 0x1

    goto :goto_4

    .line 1754
    :cond_d
    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 1759
    :cond_e
    const-string v29, "status"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v26

    .line 1760
    .local v26, "statusNode":Lcom/samsung/xml/Node;
    const-string v29, "profileID"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v24

    .line 1761
    .local v24, "profileIDNode":Lcom/samsung/xml/Node;
    const-string v29, "statusType"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v27

    .line 1763
    .local v27, "statusTypeNode":Lcom/samsung/xml/Node;
    iget v0, v3, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    move/from16 v29, v0

    move-object/from16 v0, v24

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 1765
    if-eqz v28, :cond_f

    .line 1766
    move-object/from16 v0, v28

    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 1768
    const/16 v19, 0x1

    .line 1769
    const-string v29, "Foreground"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1770
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml currentAppName found in Foreground"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792
    :goto_6
    if-eqz v19, :cond_c

    .line 1794
    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1795
    invoke-virtual/range {v26 .. v27}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1796
    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "0x"

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1797
    invoke-virtual {v10, v6}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1798
    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1799
    invoke-virtual {v9, v10}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto/16 :goto_5

    .line 1772
    :cond_f
    if-eqz v12, :cond_10

    .line 1773
    invoke-interface {v12, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 1775
    const/16 v19, 0x1

    .line 1776
    const-string v29, "Background"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1777
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml currentAppName found in Background"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1779
    :cond_10
    if-eqz v23, :cond_11

    .line 1780
    move-object/from16 v0, v23

    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    .line 1782
    const/16 v19, 0x1

    .line 1783
    const-string v29, "Notrunning"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1784
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml currentAppName found in Notrunning"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1788
    :cond_11
    const/16 v19, 0x1

    .line 1789
    const-string v29, "Notrunning"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1790
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService.xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1823
    .end local v3    # "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v6    # "appIDNode":Lcom/samsung/xml/Node;
    .end local v10    # "appStatusNode":Lcom/samsung/xml/Node;
    .end local v13    # "currentAppIDList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    .end local v14    # "currentAppId":I
    .end local v15    # "currentAppName":Ljava/lang/String;
    .end local v22    # "noOfClientProfile":I
    .end local v24    # "profileIDNode":Lcom/samsung/xml/Node;
    .end local v26    # "statusNode":Lcom/samsung/xml/Node;
    .end local v27    # "statusTypeNode":Lcom/samsung/xml/Node;
    :cond_12
    const-string v29, "TMSAppServerSvc"

    const-string v30, "TMApplicationServerService. formAppStatusListXml "

    invoke-static/range {v29 .. v30}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    invoke-virtual {v9}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v29

    goto/16 :goto_0
.end method

.method private formCertifiedAppInfo(Lcom/samsung/upnp/Action;)Ljava/lang/String;
    .locals 32
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 1291
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    if-eqz v28, :cond_0

    .line 1292
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v28

    if-lez v28, :cond_0

    .line 1293
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_0

    .line 1294
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v28

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    move/from16 v28, v0

    if-nez v28, :cond_0

    .line 1295
    const-string v28, "TMSAppServerSvc"

    const-string v29, "ML client is in 1.0 version. Hence error has to be returned"

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1297
    const/16 v23, 0x0

    .line 1412
    :goto_0
    return-object v23

    .line 1300
    :cond_0
    const-string v28, "AppID"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v8

    .line 1301
    .local v8, "arg":Lcom/samsung/upnp/Argument;
    if-nez v8, :cond_1

    .line 1303
    const-string v28, "TMSAppServerSvc"

    const-string v29, "TMApplicationServerService.formCertifiedAppInfo failed No AppID"

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    .line 1305
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    .line 1304
    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1306
    const/16 v23, 0x0

    goto :goto_0

    .line 1308
    :cond_1
    invoke-virtual {v8}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1310
    .local v4, "appID":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1311
    .local v3, "Id":I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_2

    const-string v28, "0x"

    const/16 v29, 0x0

    const/16 v30, 0x2

    move/from16 v0, v29

    move/from16 v1, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 1313
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "formCertifiedAppInfo appid is hex appID = "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const/16 v28, 0x2

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 1315
    .local v14, "hexAppId":Ljava/lang/String;
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "formCertifiedAppInfo appid is hex hexAppId = "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    const/16 v28, 0x10

    :try_start_0
    move/from16 v0, v28

    invoke-static {v14, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1324
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "formCertifiedAppInfo appid is int = "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v6

    .line 1336
    .local v6, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v6, :cond_3

    .line 1337
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "AppList doesnt exsist "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    .line 1339
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    .line 1338
    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1340
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 1318
    .end local v6    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :catch_0
    move-exception v12

    .line 1319
    .local v12, "e":Ljava/lang/NumberFormatException;
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "formCertifiedAppInfo invalid app id exception"

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    .line 1321
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    .line 1320
    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1322
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 1328
    .end local v12    # "e":Ljava/lang/NumberFormatException;
    .end local v14    # "hexAppId":Ljava/lang/String;
    :cond_2
    const-string v28, "TMSAppServerSvc"

    const-string v29, "formCertifiedAppInfo invalid app id for get app status"

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    .line 1330
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    .line 1329
    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1331
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 1342
    .restart local v6    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v14    # "hexAppId":Ljava/lang/String;
    :cond_3
    iget-boolean v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    move/from16 v28, v0

    if-eqz v28, :cond_4

    iget-object v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-object/from16 v28, v0

    if-nez v28, :cond_5

    .line 1344
    :cond_4
    const-string v28, "TMSAppServerSvc"

    const-string v29, " formCertifiedAppInfo APP ID Found And NOT Certified "

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    const-string v23, ""

    goto/16 :goto_0

    .line 1348
    :cond_5
    iget v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ne v0, v3, :cond_e

    .line 1350
    const/4 v9, 0x0

    .line 1353
    .local v9, "certInfo":Ljava/lang/String;
    new-instance v11, Lcom/samsung/xml/Node;

    const-string v28, "certification"

    move-object/from16 v0, v28

    invoke-direct {v11, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1354
    .local v11, "certification":Lcom/samsung/xml/Node;
    new-instance v5, Lcom/samsung/xml/Node;

    const-string v28, "appID"

    move-object/from16 v0, v28

    invoke-direct {v5, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .local v5, "appId":Lcom/samsung/xml/Node;
    invoke-virtual {v5, v4}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1356
    new-instance v17, Lcom/samsung/xml/Node;

    const-string v28, "nonce"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 1357
    .local v17, "nonce":Lcom/samsung/xml/Node;
    const-string v28, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v28

    if-eqz v28, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v28

    if-eqz v28, :cond_6

    .line 1359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getNonce()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1362
    :cond_6
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1363
    iget-object v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppUUID:Ljava/lang/String;

    move-object/from16 v28, v0

    if-eqz v28, :cond_7

    .line 1364
    new-instance v7, Lcom/samsung/xml/Node;

    const-string v28, "appUUID"

    move-object/from16 v0, v28

    invoke-direct {v7, v0}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .local v7, "appUUID":Lcom/samsung/xml/Node;
    iget-object v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppUUID:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v7, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1366
    .end local v7    # "appUUID":Lcom/samsung/xml/Node;
    :cond_7
    iget-object v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    move-object/from16 v28, v0

    if-nez v28, :cond_9

    .line 1367
    const-string v28, "TMSAppServerSvc"

    const-string v29, " formCertifiedAppInfo No entities present"

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397
    :cond_8
    invoke-virtual {v11}, Lcom/samsung/xml/Node;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1398
    const-string v28, " "

    const-string v29, ""

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 1399
    const-string v28, "\t"

    const-string v29, ""

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 1400
    const-string v28, "\n"

    const-string v29, ""

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 1401
    const-string v28, "TMSAppServerSvc"

    const-string v29, " APP ID Found And Certified "

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;

    move-result-object v22

    .line 1403
    .local v22, "signXml":Lcom/samsung/android/mirrorlink/util/XmlSigning;
    const-string v28, "certification"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v9, v1}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->prepareSignedXml(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1404
    .local v23, "signedCertInfo":Ljava/lang/String;
    const-string v28, "<certification"

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v28

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 1405
    goto/16 :goto_0

    .line 1370
    .end local v22    # "signXml":Lcom/samsung/android/mirrorlink/util/XmlSigning;
    .end local v23    # "signedCertInfo":Ljava/lang/String;
    :cond_9
    iget-object v0, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_8

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 1371
    .local v10, "certiInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    const-string v29, "entity"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v13

    .line 1373
    .local v13, "entity":Lcom/samsung/xml/Node;
    const-string v29, "name"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v15

    .local v15, "name":Lcom/samsung/xml/Node;
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    invoke-virtual {v13, v15}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1374
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    move-object/from16 v29, v0

    if-eqz v29, :cond_a

    .line 1375
    const-string v29, "targetList"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v26

    .local v26, "targetList":Lcom/samsung/xml/Node;
    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1377
    const-string v29, "target"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v25

    .line 1378
    .local v25, "target":Lcom/samsung/xml/Node;
    const-string v27, ""

    .line 1379
    .local v27, "targets":Ljava/lang/String;
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_2
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-nez v30, :cond_c

    .line 1382
    const/16 v29, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v30

    add-int/lit8 v30, v30, -0x1

    move-object/from16 v0, v27

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 1383
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1385
    .end local v25    # "target":Lcom/samsung/xml/Node;
    .end local v26    # "targetList":Lcom/samsung/xml/Node;
    .end local v27    # "targets":Ljava/lang/String;
    :cond_a
    const-string v29, "restricted"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v18

    .local v18, "restricted":Lcom/samsung/xml/Node;
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1386
    const-string v29, "nonRestricted"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v16

    .local v16, "nonRestricted":Lcom/samsung/xml/Node;
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1387
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    move-object/from16 v29, v0

    if-eqz v29, :cond_b

    .line 1388
    const-string v29, "serviceList"

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v21

    .local v21, "serviceList":Lcom/samsung/xml/Node;
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 1390
    iget-object v0, v10, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_3
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-nez v30, :cond_d

    .line 1394
    .end local v21    # "serviceList":Lcom/samsung/xml/Node;
    :cond_b
    invoke-virtual {v11, v13}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto/16 :goto_1

    .line 1379
    .end local v16    # "nonRestricted":Lcom/samsung/xml/Node;
    .end local v18    # "restricted":Lcom/samsung/xml/Node;
    .restart local v25    # "target":Lcom/samsung/xml/Node;
    .restart local v26    # "targetList":Lcom/samsung/xml/Node;
    .restart local v27    # "targets":Ljava/lang/String;
    :cond_c
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 1380
    .local v24, "tar":Ljava/lang/String;
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v31, ","

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    goto/16 :goto_2

    .line 1390
    .end local v24    # "tar":Ljava/lang/String;
    .end local v25    # "target":Lcom/samsung/xml/Node;
    .end local v26    # "targetList":Lcom/samsung/xml/Node;
    .end local v27    # "targets":Ljava/lang/String;
    .restart local v16    # "nonRestricted":Lcom/samsung/xml/Node;
    .restart local v18    # "restricted":Lcom/samsung/xml/Node;
    .restart local v21    # "serviceList":Lcom/samsung/xml/Node;
    :cond_d
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 1391
    .local v19, "serv":Ljava/lang/String;
    const-string v30, "service"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v20

    .local v20, "service":Lcom/samsung/xml/Node;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto :goto_3

    .line 1409
    .end local v5    # "appId":Lcom/samsung/xml/Node;
    .end local v9    # "certInfo":Ljava/lang/String;
    .end local v10    # "certiInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    .end local v11    # "certification":Lcom/samsung/xml/Node;
    .end local v13    # "entity":Lcom/samsung/xml/Node;
    .end local v15    # "name":Lcom/samsung/xml/Node;
    .end local v16    # "nonRestricted":Lcom/samsung/xml/Node;
    .end local v17    # "nonce":Lcom/samsung/xml/Node;
    .end local v18    # "restricted":Lcom/samsung/xml/Node;
    .end local v19    # "serv":Ljava/lang/String;
    .end local v20    # "service":Lcom/samsung/xml/Node;
    .end local v21    # "serviceList":Lcom/samsung/xml/Node;
    :cond_e
    const-string v28, "TMSAppServerSvc"

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "App ID not found "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    sget-object v28, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v28

    .line 1411
    sget-object v29, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v29

    .line 1410
    move-object/from16 v0, p1

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1412
    const/16 v23, 0x0

    goto/16 :goto_0
.end method

.method private formCertifiedAppList(Lcom/samsung/upnp/Action;)Ljava/lang/String;
    .locals 17
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 1186
    const/4 v3, 0x0

    .line 1187
    .local v3, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const/4 v12, -0x1

    .line 1188
    .local v12, "profileID":I
    const/4 v2, 0x0

    .line 1189
    .local v2, "appCertFilter":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1190
    .local v13, "profileIDFound":Z
    const/4 v11, 0x0

    .line 1192
    .local v11, "prfl":I
    const/4 v6, 0x0

    .line 1194
    .local v6, "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const-string v14, "ProfileID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 1195
    .local v5, "arg":Lcom/samsung/upnp/Argument;
    if-nez v5, :cond_0

    .line 1197
    const-string v14, "TMSAppServerSvc"

    const-string v15, "TMApplicationServerService.formAppListXml failed No ProfileID"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v14

    .line 1199
    sget-object v15, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v15

    .line 1198
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1200
    const/4 v14, 0x0

    .line 1282
    :goto_0
    return-object v14

    .line 1203
    :cond_0
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v12

    .line 1206
    :goto_1
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lt v11, v14, :cond_2

    .line 1222
    :cond_1
    if-nez v13, :cond_4

    .line 1224
    const-string v14, "TMSAppServerSvc"

    const-string v15, "formCertifiedAppList Error in starting app.no such client profile ID"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1226
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v14

    .line 1227
    sget-object v15, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v15

    .line 1226
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1228
    const/4 v14, 0x0

    goto :goto_0

    .line 1208
    :cond_2
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v14, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    check-cast v6, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 1209
    .restart local v6    # "crntTMCProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget v14, v6, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    if-ne v14, v12, :cond_3

    .line 1212
    const/4 v13, 0x1

    .line 1213
    iget v14, v6, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_1

    iget v14, v6, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    if-nez v14, :cond_1

    .line 1214
    const-string v14, "TMSAppServerSvc"

    const-string v15, "ML 1.0 cient should not call getCertifiedAppList"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v14

    sget-object v15, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1216
    const/4 v14, 0x0

    goto :goto_0

    .line 1206
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1231
    :cond_4
    const-string v14, "AppCertFilter"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 1232
    if-nez v5, :cond_5

    .line 1234
    const-string v14, "TMSAppServerSvc"

    const-string v15, "TMApplicationServerService.formCertifiedAppList failed No AppCertFilter"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v14

    .line 1236
    sget-object v15, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v15

    .line 1235
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1237
    const/4 v14, 0x0

    goto :goto_0

    .line 1240
    :cond_5
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1241
    const-string v14, "TMSAppServerSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "formCertifiedAppList AppCertFilter is "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    if-nez v2, :cond_6

    .line 1244
    const-string v14, "TMSAppServerSvc"

    const-string v15, "formCertifiedAppList Error in SOAP request xml"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v14

    .line 1246
    sget-object v15, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v15

    .line 1245
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1247
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 1250
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getCertifiedAppList(I)Ljava/util/List;

    move-result-object v3

    .line 1252
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    invoke-virtual {v14, v3, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->filterAppsByAppCertFilter(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 1253
    .local v7, "filteredAppList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    if-nez v7, :cond_7

    .line 1255
    const-string v14, "TMSAppServerSvc"

    const-string v15, "formCertifiedAppList filteredAppList = null"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    const-string v14, ""

    goto/16 :goto_0

    .line 1259
    :cond_7
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    .line 1260
    .local v10, "noOfApps":I
    if-gtz v10, :cond_8

    .line 1262
    const-string v14, "TMSAppServerSvc"

    const-string v15, "formCertifiedAppList filteredAppList = null"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    const-string v14, ""

    goto/16 :goto_0

    .line 1266
    :cond_8
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 1267
    .local v8, "filteredCertifiedAppIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const-string v14, "TMSAppServerSvc"

    const-string v15, "Forming CertifiedAppList"

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_a

    .line 1273
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1274
    .local v4, "applist":Ljava/lang/StringBuilder;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 1275
    .local v9, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_9
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_b

    .line 1281
    const-string v14, "TMSAppServerSvc"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "formCertifiedAppList applist = "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 1268
    .end local v4    # "applist":Ljava/lang/StringBuilder;
    .end local v9    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_a
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1270
    .local v1, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget v15, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v8, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1277
    .end local v1    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v4    # "applist":Ljava/lang/StringBuilder;
    .restart local v9    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_b
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v14, "0x"

    invoke-direct {v15, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1278
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1279
    const-string v14, ","

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method private getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1604
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1182
    new-instance v0, Lcom/samsung/xml/Node;

    invoke-direct {v0, p1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private isAppCertified(Lcom/samsung/upnp/Action;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
    .locals 13
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x2

    .line 1417
    const-string v8, "AppID"

    invoke-virtual {p1, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 1418
    .local v5, "arg":Lcom/samsung/upnp/Argument;
    if-nez v5, :cond_0

    .line 1420
    const-string v8, "TMSAppServerSvc"

    const-string v9, "TMApplicationServerService.isAppCertified failed No AppID"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1421
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1422
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1421
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1423
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    .line 1487
    :goto_0
    return-object v8

    .line 1425
    :cond_0
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 1427
    .local v3, "appID":Ljava/lang/String;
    const-string v8, "AppCertFilter"

    invoke-virtual {p1, v8}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 1428
    if-nez v5, :cond_1

    .line 1430
    const-string v8, "TMSAppServerSvc"

    const-string v9, "TMApplicationServerService.isAppCertified failed No AppCertFilter"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1432
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1431
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1433
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto :goto_0

    .line 1436
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1438
    .local v2, "appCertFilter":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1439
    .local v0, "Id":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v11, :cond_2

    const-string v8, "0x"

    invoke-virtual {v3, v12, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1441
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified appid is hex appID = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    invoke-virtual {v3, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1443
    .local v7, "hexAppId":Ljava/lang/String;
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified appid is hex hexAppId = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1445
    const/16 v8, 0x10

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1452
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified appid is int = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v8, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 1463
    .local v1, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v1, :cond_3

    .line 1464
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified App ID not found "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1465
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1466
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1465
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1467
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0

    .line 1446
    .end local v1    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :catch_0
    move-exception v6

    .line 1447
    .local v6, "e":Ljava/lang/NumberFormatException;
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified invalid app id exception"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1449
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1448
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1450
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0

    .line 1456
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    .end local v7    # "hexAppId":Ljava/lang/String;
    :cond_2
    const-string v8, "TMSAppServerSvc"

    const-string v9, "isAppCertified invalid app id for get app status"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1458
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1457
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1459
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0

    .line 1470
    .restart local v1    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v7    # "hexAppId":Ljava/lang/String;
    :cond_3
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1471
    .local v4, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1472
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "filtering apps for appId "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " appCertFilter :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mTMSAppServerSvcActionHelper:Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;

    invoke-virtual {v8, v4, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->filterAppsByAppCertFilter(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 1474
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_5

    .line 1476
    :cond_4
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isAppCertified AppList doesnt exsist "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v8

    .line 1478
    sget-object v9, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    .line 1477
    invoke-virtual {p1, v8, v9}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 1479
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0

    .line 1482
    :cond_5
    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-boolean v8, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-nez v8, :cond_6

    .line 1483
    const-string v8, "TMSAppServerSvc"

    const-string v9, " isAppCertified APP ID Found And NOT Certified "

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->NotCertified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0

    .line 1486
    :cond_6
    const-string v8, "TMSAppServerSvc"

    const-string v9, " isAppCertified APP ID Found And Certified "

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1487
    sget-object v8, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Certified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    goto/16 :goto_0
.end method

.method private declared-synchronized launchApp(Lcom/samsung/upnp/Action;)Z
    .locals 16
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 663
    monitor-enter p0

    :try_start_0
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp enter"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const/4 v11, -0x1

    .line 666
    .local v11, "profileID":I
    const/4 v5, -0x1

    .line 667
    .local v5, "appToLaunch":I
    const/4 v10, 0x0

    .line 668
    .local v10, "launchStatus":Z
    const/4 v12, 0x0

    .line 669
    .local v12, "profileIDFound":Z
    const/4 v1, 0x0

    .line 677
    .local v1, "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    const-string v13, "ProfileID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v6

    .line 678
    .local v6, "arg":Lcom/samsung/upnp/Argument;
    if-nez v6, :cond_0

    .line 680
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp failed No ProfileID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 682
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 681
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    const/4 v13, 0x0

    .line 807
    :goto_0
    monitor-exit p0

    return v13

    .line 686
    :cond_0
    :try_start_1
    invoke-virtual {v6}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v11

    .line 688
    const-string v13, "AppID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v6

    .line 689
    if-nez v6, :cond_1

    .line 691
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp failed No AppID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 693
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 692
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 694
    const/4 v13, 0x0

    goto :goto_0

    .line 697
    :cond_1
    invoke-virtual {v6}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 700
    .local v2, "appId":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_3

    .line 702
    :cond_2
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp invalid app id from client.Exit"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 704
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 703
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 705
    const/4 v13, 0x0

    goto :goto_0

    .line 707
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x2

    if-le v13, v14, :cond_5

    const-string v13, "0x"

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-virtual {v2, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 709
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApp appid is hex appId = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const/4 v13, 0x2

    invoke-virtual {v2, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 711
    .local v8, "hexAppId":Ljava/lang/String;
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApp appid is hex hexAppId = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 713
    const/16 v13, 0x10

    :try_start_2
    invoke-static {v8, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    .line 729
    :try_start_3
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApp appToLaunch = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    const/4 v13, -0x1

    if-eq v13, v11, :cond_4

    const/4 v13, -0x1

    if-ne v13, v5, :cond_6

    .line 732
    :cond_4
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchAppError in SOAP request xml"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 735
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 734
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 736
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 714
    :catch_0
    move-exception v7

    .line 715
    .local v7, "e":Ljava/lang/NumberFormatException;
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.actionControlReceived invalid app id exception"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 717
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 716
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 718
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 723
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v8    # "hexAppId":Ljava/lang/String;
    :cond_5
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.actionControlReceived invalid app id for launch app"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 725
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 724
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 726
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 740
    .restart local v8    # "hexAppId":Ljava/lang/String;
    :cond_6
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v9, v13, :cond_7

    .line 748
    :goto_2
    if-nez v12, :cond_9

    .line 750
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchAppError in starting app.no such client profile ID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 753
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 752
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 754
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 742
    :cond_7
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v13, v13, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    if-ne v13, v11, :cond_8

    .line 744
    const/4 v12, 0x1

    .line 745
    goto :goto_2

    .line 740
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 757
    :cond_9
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    check-cast v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 760
    .restart local v1    # "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v3

    .line 761
    .local v3, "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 763
    const-string v13, "TMSAppServerSvc"

    const-string v14, "App is present in client profile. Hence can launch"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v13, v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v4

    .line 778
    .local v4, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v4, :cond_b

    .line 780
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApp << "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v15, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " st:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v13, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    if-eqz v13, :cond_b

    .line 783
    const-string v13, "NA"

    iget-object v14, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 785
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 786
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->RESOURCE_BUSY:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 785
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 787
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp BUSY exit"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 767
    .end local v4    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_a
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApp Error in starting app.Client profile"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 768
    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 769
    const-string v15, " doesnot have app with the sent app id "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 767
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 771
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 770
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 772
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp exit"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 793
    .restart local v4    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v13, v5, v11}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->startApp(II)Z

    move-result v10

    .line 795
    if-nez v10, :cond_c

    .line 797
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 798
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->LAUNCH_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 797
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 804
    :cond_c
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.launchApplaunchApp exit. launchStatus =  "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 805
    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 804
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.launchApp exit"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v13, v10

    .line 807
    goto/16 :goto_0

    .line 663
    .end local v1    # "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v2    # "appId":Ljava/lang/String;
    .end local v3    # "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    .end local v4    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v5    # "appToLaunch":I
    .end local v6    # "arg":Lcom/samsung/upnp/Argument;
    .end local v8    # "hexAppId":Ljava/lang/String;
    .end local v9    # "i":I
    .end local v10    # "launchStatus":Z
    .end local v11    # "profileID":I
    .end local v12    # "profileIDFound":Z
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13
.end method

.method private removeUnsupportedMemberApps(Ljava/util/List;I)Ljava/util/List;
    .locals 11
    .param p2, "profileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1555
    .local p1, "allTMSAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v7, "TMSAppServerSvc"

    .line 1556
    const-string v8, "TMApplicationServerService. removeUnsupportedMemberApps enter"

    .line 1555
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 1559
    .local v6, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v1

    .line 1561
    .local v1, "clientNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v1, :cond_2

    .line 1562
    const-string v7, "TMSAppServerSvc"

    .line 1563
    const-string v8, "TMApplicationServerService.removeUnsupportedMemberApps (clientNode == null)"

    .line 1562
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    :goto_0
    return-object p1

    .line 1567
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1569
    .local v0, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-boolean v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eqz v7, :cond_2

    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v7, :cond_2

    .line 1570
    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v7, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v7, :cond_2

    .line 1571
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->getArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 1572
    .local v4, "entityNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v7, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1575
    const-string v7, "TMSAppServerSvc"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Found "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 1576
    const-string v9, " entities in app"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1575
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    const-string v7, "CCC"

    invoke-interface {v4, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1579
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 1580
    const-string v7, "TMSAppServerSvc"

    const-string v8, "Filter apps based on member name"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const/4 v5, 0x0

    .line 1582
    .local v5, "isMemberPresent":Z
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1591
    if-nez v5, :cond_2

    .line 1592
    const-string v7, "TMSAppServerSvc"

    .line 1593
    const-string v8, "Current client is not a signing entity of present application"

    .line 1592
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 1566
    .end local v0    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v4    # "entityNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "isMemberPresent":Z
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_0

    .line 1572
    .restart local v0    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v4    # "entityNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 1573
    .local v3, "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    iget-object v8, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1582
    .end local v3    # "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    .restart local v5    # "isMemberPresent":Z
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1583
    .local v2, "entity":Ljava/lang/String;
    const-string v8, "TMSAppServerSvc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Manufacturer "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1584
    iget-object v10, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Entity "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1585
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1583
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    iget-object v8, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1587
    const/4 v5, 0x1

    goto :goto_2
.end method

.method private terminateApp(Lcom/samsung/upnp/Action;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    .locals 16
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 550
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService. terminateApp enter"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/4 v12, 0x1

    .line 552
    .local v12, "terminateStatus":Z
    const/4 v9, 0x0

    .line 553
    .local v9, "profileID":I
    const/4 v11, 0x0

    .line 554
    .local v11, "stopAppID":I
    const/4 v2, 0x0

    .line 555
    .local v2, "appID":Ljava/lang/String;
    const/4 v10, 0x0

    .line 557
    .local v10, "profileIDFound":Z
    const-string v13, "ProfileID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v4

    .line 558
    .local v4, "arg1":Lcom/samsung/upnp/Argument;
    if-nez v4, :cond_0

    .line 560
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.terminateApp failed No ProfileID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 562
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 561
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 563
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    .line 646
    :goto_0
    return-object v13

    .line 566
    :cond_0
    const-string v13, "AppID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v5

    .line 567
    .local v5, "arg2":Lcom/samsung/upnp/Argument;
    if-nez v5, :cond_1

    .line 569
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.terminateApp failed No AppID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 571
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 570
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 572
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto :goto_0

    .line 575
    :cond_1
    invoke-virtual {v4}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v9

    .line 576
    invoke-virtual {v5}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 578
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x2

    if-le v13, v14, :cond_2

    const-string v13, "0x"

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-virtual {v2, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 580
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.terminateApp appid is hex appId = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const/4 v13, 0x2

    invoke-virtual {v2, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 582
    .local v7, "hexAppId":Ljava/lang/String;
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.terminateApp appid is hex hexAppId = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    const/16 v13, 0x10

    :try_start_0
    invoke-static {v7, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    .line 600
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v8, v13, :cond_3

    .line 608
    :goto_2
    if-nez v10, :cond_5

    .line 610
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.terminateAppError in terminating app.no such client profile ID"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 613
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 612
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 614
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0

    .line 585
    .end local v8    # "i":I
    :catch_0
    move-exception v6

    .line 586
    .local v6, "e":Ljava/lang/NumberFormatException;
    const-string v13, "TMSAppServerSvc"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.actionControlReceived invalid app id exception"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 588
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 587
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 589
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0

    .line 594
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    .end local v7    # "hexAppId":Ljava/lang/String;
    :cond_2
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService.actionControlReceived invalid app id for terminate app"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 596
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 595
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 597
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0

    .line 602
    .restart local v7    # "hexAppId":Ljava/lang/String;
    .restart local v8    # "i":I
    :cond_3
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v13, v13, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    if-ne v13, v9, :cond_4

    .line 604
    const/4 v10, 0x1

    .line 605
    goto :goto_2

    .line 600
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 619
    :cond_5
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 620
    .local v1, "ClientprofileNode_temp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v3

    .line 621
    .local v3, "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    if-eqz v3, :cond_6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 623
    const-string v13, "TMSAppServerSvc"

    const-string v14, "App is present in client profile. Hence can terminate"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v13, v11}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    move-result v12

    .line 637
    if-eqz v12, :cond_7

    .line 639
    const-string v13, "TMSAppServerSvc"

    const-string v14, "TMApplicationServerService. terminateApp enexitter"

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0

    .line 627
    :cond_6
    const-string v13, "TMSAppServerSvc"

    .line 628
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "TMApplicationServerService.terminateApp Error in starting app.Client profile"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 629
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 630
    const-string v15, " doesnot have app with the sent app id "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 628
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 627
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v13

    .line 632
    sget-object v14, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual {v14}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v14

    .line 631
    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 633
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0

    .line 644
    :cond_7
    const-string v13, "TMSAppServerSvc"

    .line 645
    const-string v14, "TMApplicationServerService. terminateApp exit: termiante failed"

    .line 644
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    sget-object v13, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized actionControlReceived(Lcom/samsung/upnp/Action;)Z
    .locals 21
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    const-string v18, "TMSAppServerSvc"

    .line 165
    const-string v19, "TMApplicationServerService. actionControlReceived enter "

    .line 164
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    if-nez p1, :cond_0

    .line 167
    const/4 v14, 0x0

    .line 533
    :goto_0
    monitor-exit p0

    return v14

    .line 170
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v18

    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 172
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.actionControlReceived phone secure locked"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v18

    .line 174
    sget-object v19, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v19

    .line 173
    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 175
    const/4 v14, 0x0

    goto :goto_0

    .line 179
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    .line 181
    .local v5, "actionName":Ljava/lang/String;
    const-string v18, "GetApplicationList"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 183
    const-string v18, "TMSAppServerSvc"

    .line 184
    const-string v19, "TMApplicationServerService. actionControlReceived recvd enter get applist action"

    .line 183
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v16, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-class v19, Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 187
    .local v16, "startIntent2":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 188
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->formAppListXml(Lcom/samsung/upnp/Action;)Ljava/lang/String;

    move-result-object v8

    .line 189
    .local v8, "appList":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 192
    const-string v18, "AppListing"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v18, "TMSAppServerSvc"

    .line 195
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit get applist action done"

    .line 194
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 210
    :cond_2
    const-string v18, "TMSAppServerSvc"

    .line 211
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit getapplist action failed"

    .line 210
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 217
    .end local v8    # "appList":Ljava/lang/String;
    .end local v16    # "startIntent2":Landroid/content/Intent;
    :cond_3
    const-string v18, "LaunchApplication"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_13

    .line 219
    const/4 v14, 0x0

    .line 220
    .local v14, "launchStatus":Z
    const-string v18, "TMSAppServerSvc"

    .line 221
    const-string v19, "TMApplicationServerService. actionControlReceived recvd enter launch app action"

    .line 220
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v18, "TMSAppServerSvc"

    .line 225
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd VncIp "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncIp:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " VncPort "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncPort:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 225
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 224
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->launchApp(Lcom/samsung/upnp/Action;)Z

    move-result v14

    .line 230
    if-eqz v14, :cond_9

    .line 232
    const/4 v9, 0x0

    .line 234
    .local v9, "appToLaunch":I
    const-string v18, "AppID"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 235
    .local v10, "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_4

    .line 237
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.actionControlReceived ACTION_LAUNCH_APPLCATION failed No AppID"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v18

    .line 239
    sget-object v19, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INAVLID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v19

    .line 238
    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 240
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 242
    :cond_4
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 244
    .local v6, "appId":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    const-string v18, "0x"

    const/16 v19, 0x0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 246
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService.launchApp appid is hex appId = "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/16 v18, 0x2

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 248
    .local v12, "hexAppId":Ljava/lang/String;
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService.launchApp appid is hex hexAppId = "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    const/16 v18, 0x10

    :try_start_2
    move/from16 v0, v18

    invoke-static {v12, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 268
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v7

    .line 269
    .local v7, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v7, :cond_6

    .line 270
    const-string v18, "TMSAppServerSvc"

    const-string v19, "appInfo is null"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 252
    .end local v7    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :catch_0
    move-exception v11

    .line 253
    .local v11, "e":Ljava/lang/NumberFormatException;
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService.actionControlReceived invalid app id exception"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v18

    .line 255
    sget-object v19, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v19

    .line 254
    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 256
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 262
    .end local v11    # "e":Ljava/lang/NumberFormatException;
    .end local v12    # "hexAppId":Ljava/lang/String;
    :cond_5
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.actionControlReceived invalid app id for launch app"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v18

    .line 264
    sget-object v19, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v19

    .line 263
    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 265
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 272
    .restart local v7    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v12    # "hexAppId":Ljava/lang/String;
    :cond_6
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v18

    if-nez v18, :cond_7

    .line 273
    const-string v18, "TMSAppServerSvc"

    const-string v19, "appInfo.getRemotingInfo is null"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 275
    :cond_7
    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_8

    .line 276
    const-string v18, "TMSAppServerSvc"

    const-string v19, "appInfo.getRemotingInfo.mprotocolId is null"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 279
    :cond_8
    const-string v18, "VNC"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.samsung.android.mirrorlink.action.SHOW_BLACK_SCREEN"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 282
    const-string v18, "AppURI"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "VNC://"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncIp:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 283
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncPort:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 282
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set VNC remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    .end local v6    # "appId":Ljava/lang/String;
    .end local v7    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v9    # "appToLaunch":I
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v12    # "hexAppId":Ljava/lang/String;
    :cond_9
    :goto_1
    const-string v18, "TMSAppServerSvc"

    .line 337
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit launch app action done"

    .line 336
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 164
    .end local v5    # "actionName":Ljava/lang/String;
    .end local v14    # "launchStatus":Z
    :catchall_0
    move-exception v18

    monitor-exit p0

    throw v18

    .line 286
    .restart local v5    # "actionName":Ljava/lang/String;
    .restart local v6    # "appId":Ljava/lang/String;
    .restart local v7    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v9    # "appToLaunch":I
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    .restart local v12    # "hexAppId":Ljava/lang/String;
    .restart local v14    # "launchStatus":Z
    :cond_a
    :try_start_4
    const-string v18, "RTP Server 0"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_b

    .line 287
    const-string v18, "RTP Server 98"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_b

    .line 288
    const-string v18, "RTP Server 99"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 290
    :cond_b
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "RTP://"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpIp:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpServerPort:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 291
    .local v17, "uri":Ljava/lang/String;
    const-string v18, "AppURI"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set RTP Server remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 294
    .end local v17    # "uri":Ljava/lang/String;
    :cond_c
    const-string v18, "RTP Client 0"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 295
    const-string v18, "RTP Client 98"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 296
    const-string v18, "RTP Client 99"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 297
    const-string v18, "RTP Client z9"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 298
    const-string v18, "RTP Client k9"

    iget-object v0, v7, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 300
    :cond_d
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "RTP://"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpIp:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpClientPort:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 301
    .restart local v17    # "uri":Ljava/lang/String;
    const-string v18, "AppURI"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set RTP Client remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 304
    .end local v17    # "uri":Ljava/lang/String;
    :cond_e
    const-string v18, "DAP"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 306
    const-string v18, "AppURI"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "DAP://"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDapIp:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 307
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDapPort:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 306
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set DAP remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 310
    :cond_f
    const-string v18, "CDB"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 312
    const-string v18, "AppURI"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CDB://"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mCdbIp:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 313
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mCdbPort:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 312
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set CDB remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 316
    :cond_10
    const-string v18, "BTA2DP"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 319
    const-string v18, "AppURI"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "BTA2DP://"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtMac:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 320
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getA2dpSeid()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 319
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set BTA2DP remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 323
    :cond_11
    const-string v18, "BTHFP"

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    .line 326
    const-string v18, "AppURI"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "BTHFP://"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtMac:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 327
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getHfpRfCommChannel()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 326
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd set BTHFP remoting info for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 332
    :cond_12
    const-string v18, "TMSAppServerSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "TMApplicationServerService. actionControlReceived recvd error in setting remoting info in xml for appid "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 342
    .end local v6    # "appId":Ljava/lang/String;
    .end local v7    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v9    # "appToLaunch":I
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v12    # "hexAppId":Ljava/lang/String;
    .end local v14    # "launchStatus":Z
    :cond_13
    const-string v18, "TerminateApplication"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 344
    const-string v18, "TMSAppServerSvc"

    .line 345
    const-string v19, "TMApplicationServerService. actionControlReceived recvd enter recvd terminate app action"

    .line 344
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->terminateApp(Lcom/samsung/upnp/Action;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    move-result-object v15

    .line 349
    .local v15, "ret":Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->SOAP_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_14

    .line 351
    const-string v18, "TMSAppServerSvc"

    .line 352
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit terminate app action failed"

    .line 351
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 356
    :cond_14
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_SUCCESS:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_16

    .line 358
    const-string v18, "TerminationResult"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 359
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_15

    .line 360
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd     ACTION_TERMINATE_APPLCATION No TerminationResult"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 364
    :cond_15
    const-string v18, "true"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 366
    const-string v18, "TMSAppServerSvc"

    .line 367
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit  terminate app action success"

    .line 366
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 371
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_16
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->TERMINATE_FAILED:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_18

    .line 373
    const-string v18, "TerminationResult"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 374
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_17

    .line 375
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd     ACTION_TERMINATE_APPLCATION No TerminationResult"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 379
    :cond_17
    const-string v18, "false"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 381
    const-string v18, "TMSAppServerSvc"

    .line 382
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit terminate app action not complete"

    .line 381
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 386
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_18
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->REQUEST_ERROR:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 388
    const-string v18, "TMSAppServerSvc"

    .line 389
    const-string v19, "TMApplicationServerService. actionControlReceived recvd exit error in request"

    .line 388
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 395
    .end local v15    # "ret":Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPTERMINATION_RESULT;
    :cond_19
    const-string v18, "GetApplicationStatus"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1c

    .line 397
    const-string v18, "TMSAppServerSvc"

    .line 398
    const-string v19, "TMApplicationServerService. actionControlReceived recvdenter get appstatus action"

    .line 397
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->formAppStatusListXml(Lcom/samsung/upnp/Action;)Ljava/lang/String;

    move-result-object v4

    .line 402
    .local v4, "AppStatusList":Ljava/lang/String;
    if-eqz v4, :cond_1b

    .line 404
    const-string v18, "AppStatus"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 405
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_1a

    .line 407
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.terminateApp failed No AppStatus"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 411
    :cond_1a
    invoke-virtual {v10, v4}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 412
    const-string v18, "TMSAppServerSvc"

    .line 413
    const-string v19, "TMApplicationServerService. actionControlReceived recvd Exit appstatus action done"

    .line 412
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 419
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_1b
    const-string v18, "TMSAppServerSvc"

    .line 420
    const-string v19, "TMApplicationServerService. actionControlReceived recvd Exitfinding appstatus failed done"

    .line 419
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 426
    .end local v4    # "AppStatusList":Ljava/lang/String;
    :cond_1c
    const-string v18, "GetCertifiedApplicationsList"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1f

    .line 428
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_CERTIFIED_APPLICATION_LIST Enter"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->formCertifiedAppList(Lcom/samsung/upnp/Action;)Ljava/lang/String;

    move-result-object v3

    .line 431
    .local v3, "AppList":Ljava/lang/String;
    if-eqz v3, :cond_1e

    .line 433
    const-string v18, "CertifiedAppList"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 434
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_1d

    .line 436
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.actionControlReceived +ACTION_GET_CERTIFIED_APPLICATION_LIST failed No CertifiedAppList"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 440
    :cond_1d
    invoke-virtual {v10, v3}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 442
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_CERTIFIED_APPLICATION_LIST Done"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 447
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_1e
    const-string v18, "TMSAppServerSvc"

    .line 448
    const-string v19, " actionControlReceived recvd ACTION_GET_CERTIFIED_APPLICATION_LISTFAILED"

    .line 447
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 454
    .end local v3    # "AppList":Ljava/lang/String;
    :cond_1f
    const-string v18, "GetApplicationCertificateInfo"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_22

    .line 456
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APPLICATION_CERTIFICATION_INFO Enter"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->formCertifiedAppInfo(Lcom/samsung/upnp/Action;)Ljava/lang/String;

    move-result-object v13

    .line 459
    .local v13, "info":Ljava/lang/String;
    if-eqz v13, :cond_21

    .line 461
    const-string v18, "AppCertification"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 462
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_20

    .line 464
    const-string v18, "TMSAppServerSvc"

    const-string v19, "TMApplicationServerService.actionControlReceived +ACTION_GET_APPLICATION_CERTIFICATION_INFO failed No AppCertification"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 468
    :cond_20
    invoke-virtual {v10, v13}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 470
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APPLICATION_CERTIFICATION_INFO Done"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 475
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_21
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APPLICATION_CERTIFICATION_INFO FAILED"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 480
    .end local v13    # "info":Ljava/lang/String;
    :cond_22
    const-string v18, "GetAppCertificationStatus"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_28

    .line 482
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APP_CERTIFICATION_STATUS Enter"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    if-eqz v18, :cond_23

    .line 488
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_23

    .line 489
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_23

    .line 490
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    move/from16 v18, v0

    if-nez v18, :cond_23

    .line 491
    const-string v18, "TMSAppServerSvc"

    const-string v19, "ML client is in 1.0 version. Hence error has to be returned"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorCode()I

    move-result v18

    sget-object v19, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$eAPPACTION_RESULT;->getErrorDesc()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 493
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 497
    :cond_23
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->isAppCertified(Lcom/samsung/upnp/Action;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    move-result-object v15

    .line 498
    .local v15, "ret":Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Certified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    move-object/from16 v0, v18

    if-ne v15, v0, :cond_25

    .line 500
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APP_CERTIFICATION_STATUS Certified"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    const-string v18, "AppCertified"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 502
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_24

    .line 503
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd     ACTION_GET_APP_CERTIFICATION_STATUS No AppCertified"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 507
    :cond_24
    const-string v18, "true"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 508
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 510
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_25
    sget-object v18, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->NotCertified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    move-object/from16 v0, v18

    if-ne v15, v0, :cond_27

    .line 512
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APP_CERTIFICATION_STATUS Not Cerified"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v18, "AppCertified"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 514
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_26

    .line 515
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd     ACTION_GET_APP_CERTIFICATION_STATUS No AppCertified"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 519
    :cond_26
    const-string v18, "false"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 520
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 524
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    :cond_27
    const-string v18, "TMSAppServerSvc"

    const-string v19, " actionControlReceived recvd ACTION_GET_APP_CERTIFICATION_STATUS Done"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 530
    .end local v15    # "ret":Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
    :cond_28
    const-string v18, "TMSAppServerSvc"

    .line 531
    const-string v19, "TMApplicationServerService.  actionControlReceived recvd Exitrecvd action not recognised or error occured"

    .line 530
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 533
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method public getAppList(I)Ljava/util/List;
    .locals 5
    .param p1, "profileID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 832
    const-string v2, "TMSAppServerSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TMApplicationServerService. getAppList enter << profileID"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 838
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    new-instance v0, Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getApplicationMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 839
    .local v0, "allTMSAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 841
    invoke-virtual {p0, v1, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->removeUnsupportedAppByClient(Ljava/util/List;I)Ljava/util/List;

    move-result-object v1

    .line 843
    const-string v2, "TMSAppServerSvc"

    const-string v3, "TMApplicationServerService. getAppList exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    return-object v1
.end method

.method public getBtMacAddress()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1871
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. getBtMacAddress "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1872
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtMac:Ljava/lang/String;

    return-object v0
.end method

.method public getBtStartConnection()Z
    .locals 3

    .prologue
    .line 1876
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. getBtStartConnection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtStartConnection:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtStartConnection:Z

    return v0
.end method

.method public getCertifiedAppList(I)Ljava/util/List;
    .locals 4
    .param p1, "profileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 848
    const-string v1, "TMSAppServerSvc"

    const-string v2, "TMApplicationServerService. getCertifiedAppList enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getCertifiedAppList()Ljava/util/List;

    move-result-object v0

    .line 854
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    if-nez v0, :cond_0

    .line 855
    const-string v1, "TMSAppServerSvc"

    const-string v2, "Received null applist"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    const/4 v1, 0x0

    .line 864
    :goto_0
    return-object v1

    .line 858
    :cond_0
    const-string v1, "TMSAppServerSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMApplicationServerService. getCertifiedAppList found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " certified apps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->removeUnsupportedAppByClient(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 861
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->removeUnsupportedMemberApps(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 862
    const-string v1, "TMSAppServerSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TMApplicationServerService. getCertifiedAppList size of certified apps after removing unsupported apps "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    const-string v1, "TMSAppServerSvc"

    const-string v2, "TMApplicationServerService. getCertifiedAppList exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 864
    goto :goto_0
.end method

.method public initializeAppDB()V
    .locals 2

    .prologue
    .line 815
    const-string v0, "TMSAppServerSvc"

    const-string v1, "TMApplicationServerService.initializeAppDB enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->updateApplicationList()Ljava/util/Map;

    .line 817
    const-string v0, "TMSAppServerSvc"

    const-string v1, "TMApplicationServerService.initializeAppDB exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    return-void
.end method

.method public removeUnsupportedAppByClient(Ljava/util/List;I)Ljava/util/List;
    .locals 10
    .param p2, "profileId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1500
    .local p1, "allTMSAppInfo":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v5, "TMSAppServerSvc"

    const-string v6, "TMApplicationServerService. removeUnsupportedAppByClient enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1502
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v3

    .line 1504
    .local v3, "clientNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v3, :cond_1

    .line 1506
    const-string v5, "TMSAppServerSvc"

    const-string v6, "TMApplicationServerService.removeUnsupportedAppByClient (clientNode == null)"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1548
    :goto_0
    return-object p1

    .line 1511
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1513
    .local v0, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v5

    if-eqz v5, :cond_3

    const-string v5, "RTP"

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1515
    const-string v5, "TMSAppServerSvc"

    const-string v6, "TMApplicationServerService.removeUnsupportedAppByClient enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1516
    iget-object v5, v3, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_2

    .line 1518
    const-string v5, "TMSAppServerSvc"

    const-string v6, "TMApplicationServerService.removeUnsupportedAppByClient  0 rtp payload for this client"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1509
    .end local v0    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1547
    const-string v5, "TMSAppServerSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TMApplicationServerService. removeUnsupportedAppByClient exit.No of apps"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1524
    .restart local v0    # "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_2
    iget-object v5, v3, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1526
    const-string v5, "TMSAppServerSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TMApplicationServerService.removing: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1530
    :cond_3
    iget-boolean v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v5, :cond_1

    .line 1531
    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mBlackListedClients:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 1532
    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mBlackListedClients:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 1534
    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mBlackListedClients:Ljava/lang/String;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1536
    .local v2, "blacklistedClients":[Ljava/lang/String;
    array-length v6, v2

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v6, :cond_1

    aget-object v1, v2, v5

    .line 1537
    .local v1, "blackListedclientId":Ljava/lang/String;
    iget-object v7, v3, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1538
    const-string v7, "TMSAppServerSvc"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Client id is blacklisted. Hence removing "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1539
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1538
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->removeUnsupportedVncApp(Ljava/lang/String;)V

    .line 1541
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 1536
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public setBtParams(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "startConnection"    # Z

    .prologue
    .line 1861
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. setBtParams "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1862
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtMac:Ljava/lang/String;

    .line 1863
    iput-boolean p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mBtStartConnection:Z

    .line 1864
    return-void
.end method

.method public setCdbParams(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1887
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. setCdbParams "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1888
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mCdbIp:Ljava/lang/String;

    .line 1889
    iput p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mCdbPort:I

    .line 1890
    return-void
.end method

.method public setDapParams(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1881
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. setDapParams "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1882
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDapIp:Ljava/lang/String;

    .line 1883
    iput p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mDapPort:I

    .line 1884
    return-void
.end method

.method public setRtpParams(Ljava/lang/String;II)V
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "serverPort"    # I
    .param p3, "clientPort"    # I

    .prologue
    .line 1847
    const-string v0, "TMSAppServerSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMApplicationServerService. setRtpParams "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " server port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " client port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    monitor-enter p0

    .line 1849
    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpIp:Ljava/lang/String;

    .line 1848
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1851
    iput p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpServerPort:I

    .line 1852
    iput p3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mRtpClientPort:I

    .line 1853
    return-void

    .line 1848
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized setVncParams(Ljava/lang/String;I)V
    .locals 1
    .param p1, "ipAddr"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1836
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncIp:Ljava/lang/String;

    .line 1837
    iput p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;->mVncPort:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1839
    monitor-exit p0

    return-void

    .line 1836
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
