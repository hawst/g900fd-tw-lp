.class public Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;
.super Landroid/os/Handler;
.source "TmsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TmsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppStateHandler"
.end annotation


# static fields
.field public static final TMINIT_SERVICE:I = 0x1

.field public static final TM_DEINIT_SERVICE:I = 0x2

.field public static final TM_DESTROY_SERVICE:I = 0x3


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TmsService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/service/TmsService;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 610
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 634
    :goto_0
    return-void

    .line 612
    :pswitch_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.AppStateHandler - TMInitService"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$0(Lcom/samsung/android/mirrorlink/service/TmsService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    const-string v0, "TMSSvc"

    const-string v1, "Deinit in progress. moving start to pending state"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$1(Lcom/samsung/android/mirrorlink/service/TmsService;Z)V

    goto :goto_0

    .line 617
    :cond_0
    const-string v0, "TMSSvc"

    const-string v1, "Can start service normally. No deinit in progress"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$1(Lcom/samsung/android/mirrorlink/service/TmsService;Z)V

    .line 619
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TmsService;->init()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$2(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    goto :goto_0

    .line 623
    :pswitch_1
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.AppStateHandler - TMDeinitService"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$3(Lcom/samsung/android/mirrorlink/service/TmsService;Z)V

    .line 625
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TmsService;->deinit()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$4(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    goto :goto_0

    .line 628
    :pswitch_2
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.AppStateHandler - TMDestroyService"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TmsService;->destroy()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/service/TmsService;->access$5(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    goto :goto_0

    .line 610
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
