.class Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;
.super Landroid/os/Handler;
.source "AcmsRevocationMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State()[I
    .locals 3

    .prologue
    .line 197
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_INSTALLED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_b
    :try_start_b
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_c
    :try_start_c
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_d
    :try_start_d
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_e
    :try_start_e
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_f
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_f

    :catch_1
    move-exception v1

    goto :goto_e

    :catch_2
    move-exception v1

    goto :goto_d

    :catch_3
    move-exception v1

    goto :goto_c

    :catch_4
    move-exception v1

    goto :goto_b

    :catch_5
    move-exception v1

    goto :goto_a

    :catch_6
    move-exception v1

    goto :goto_9

    :catch_7
    move-exception v1

    goto :goto_8

    :catch_8
    move-exception v1

    goto :goto_7

    :catch_9
    move-exception v1

    goto :goto_6

    :catch_a
    move-exception v1

    goto :goto_5

    :catch_b
    move-exception v1

    goto/16 :goto_4

    :catch_c
    move-exception v1

    goto/16 :goto_3

    :catch_d
    move-exception v1

    goto/16 :goto_2

    :catch_e
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    .line 197
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private sendMessageToMLCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    .locals 4
    .param p1, "st"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 201
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 202
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 203
    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    .line 204
    .local v0, "isSuccess":Z
    if-eqz v0, :cond_0

    .line 206
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v2

    .line 207
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 208
    const-string v2, "AcmsRevocationMngr"

    const-string v3, "Incremented Counter Value : sendMessageToMLCertifiedHandler"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 216
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    if-le v1, v2, :cond_4

    .line 217
    sget-object v20, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 221
    .local v20, "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :goto_0
    const/4 v10, 0x0

    .line 222
    .local v10, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const/4 v9, 0x0

    .line 223
    .local v9, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/16 v17, 0x0

    .line 225
    .local v17, "isManualRevoc":Z
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_REVOCATION_CHECK:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-ne v0, v1, :cond_5

    .line 226
    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    check-cast v9, Ljava/util/ArrayList;

    .line 227
    .restart local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/16 v17, 0x1

    .line 232
    :goto_1
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-eq v0, v1, :cond_0

    .line 233
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-eq v0, v1, :cond_0

    .line 234
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-eq v0, v1, :cond_0

    .line 235
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-eq v0, v1, :cond_0

    .line 236
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, v20

    if-ne v0, v1, :cond_1

    .line 237
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .restart local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v18

    .line 244
    .local v18, "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v7

    .line 245
    .local v7, "acmsCertificateMngr":Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->$SWITCH_TABLE$com$samsung$android$mirrorlink$acms$provider$AppEntryInterface$State()[I

    move-result-object v1

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 703
    :cond_2
    :goto_2
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v8

    .line 704
    .local v8, "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    if-eqz v8, :cond_3

    .line 705
    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 706
    :cond_3
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decremented Counter Value : mMlCertifiedHandler.handleMessage=>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    return-void

    .line 219
    .end local v7    # "acmsCertificateMngr":Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    .end local v8    # "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    .end local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    .end local v10    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v17    # "isManualRevoc":Z
    .end local v18    # "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    .end local v20    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :cond_4
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v1

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    aget-object v20, v1, v2

    .restart local v20    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    goto/16 :goto_0

    .line 229
    .restart local v9    # "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    .restart local v10    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .restart local v17    # "isManualRevoc":Z
    :cond_5
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v10    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    check-cast v10, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .restart local v10    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    goto :goto_1

    .line 247
    .restart local v7    # "acmsCertificateMngr":Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    .restart local v18    # "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    :pswitch_0
    const-string v1, "AcmsRevocationMngr"

    .line 248
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_QUERY_PENDING"

    .line 247
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    if-nez v10, :cond_6

    .line 251
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 255
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 256
    if-eqz v9, :cond_7

    .line 257
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 257
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    .line 263
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_INPROGRESS:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->sendMessageToMLCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_2

    .line 260
    :cond_7
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "appEntries is null."

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 265
    :cond_8
    const-string v1, "AcmsRevocationMngr"

    .line 266
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_QUERY_PENDING: Data connectivity is OFF"

    .line 265
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 268
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto/16 :goto_2

    .line 272
    :pswitch_1
    const-string v1, "AcmsRevocationMngr"

    .line 273
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_QUERY_INPROGRESS"

    .line 272
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 277
    :pswitch_2
    const-string v1, "AcmsRevocationMngr"

    .line 278
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_QUERY_COMPLETED"

    .line 277
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    if-nez v10, :cond_9

    .line 281
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 285
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    .line 286
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 287
    .local v19, "packageName":Ljava/lang/String;
    if-nez v19, :cond_a

    .line 288
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "package name is null: Hence ignore"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 293
    :cond_a
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 294
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 295
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 296
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_QUERY_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 297
    const/16 v16, 0x0

    .line 300
    .local v16, "flagSelfSign":Z
    :try_start_0
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 302
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v1

    .line 303
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 304
    .local v13, "devId":Ljava/lang/String;
    invoke-virtual {v7, v13}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v12

    .line 305
    .local v12, "certs":[Ljava/security/cert/X509Certificate;
    if-nez v12, :cond_c

    .line 306
    const-string v1, "AcmsRevocationMngr"

    .line 307
    const-string v2, "certs is null. No developerid cert present in keystore for this app"

    .line 306
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto/16 :goto_2

    .line 354
    .end local v12    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v13    # "devId":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 356
    .local v15, "e":Ljava/security/cert/CertificateExpiredException;
    const-string v1, "AcmsRevocationMngr"

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Certificate expired for package"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 358
    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 359
    const-string v3, "..hence go and refetch the certificate (Update Protocol)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 357
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 355
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 361
    if-eqz v16, :cond_11

    .line 363
    const-string v1, "AcmsRevocationMngr"

    .line 364
    const-string v2, "Application development certificate is expired. Hence make only this app as non-certified"

    .line 362
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 366
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 367
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 379
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->updateProtocol(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    .line 380
    invoke-virtual {v15}, Ljava/security/cert/CertificateExpiredException;->printStackTrace()V

    goto/16 :goto_2

    .line 310
    .end local v15    # "e":Ljava/security/cert/CertificateExpiredException;
    .restart local v12    # "certs":[Ljava/security/cert/X509Certificate;
    .restart local v13    # "devId":Ljava/lang/String;
    :cond_c
    :try_start_1
    array-length v1, v12

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v12, v1

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 314
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 313
    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v12

    .line 315
    if-nez v12, :cond_d

    .line 316
    const-string v1, "AcmsRevocationMngr"

    .line 317
    const-string v2, "certs is null. No selfsigned cert present in keystore for this app"

    .line 316
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_2

    .line 382
    .end local v12    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v13    # "devId":Ljava/lang/String;
    :catch_1
    move-exception v15

    .line 384
    .local v15, "e":Ljava/security/cert/CertificateNotYetValidException;
    const-string v1, "AcmsRevocationMngr"

    .line 385
    const-string v2, "Issue date in the certificate is in future. It should be less than the current time"

    .line 383
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 386
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 389
    invoke-virtual {v15}, Ljava/security/cert/CertificateNotYetValidException;->printStackTrace()V

    goto/16 :goto_2

    .line 320
    .end local v15    # "e":Ljava/security/cert/CertificateNotYetValidException;
    .restart local v12    # "certs":[Ljava/security/cert/X509Certificate;
    .restart local v13    # "devId":Ljava/lang/String;
    :cond_d
    const/16 v16, 0x1

    .line 325
    const/4 v1, 0x0

    :try_start_2
    aget-object v1, v12, v1

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 329
    const/4 v1, 0x0

    aget-object v1, v12, v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v14

    .line 330
    .local v14, "devIdFromCert":Ljava/lang/String;
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DevId from the cert is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 331
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 330
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DevId from DB is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    if-eqz v14, :cond_e

    .line 335
    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 336
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DevId from the cert is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 337
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 336
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->updateProtocol(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    :try_end_2
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 396
    .end local v12    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v13    # "devId":Ljava/lang/String;
    .end local v14    # "devIdFromCert":Ljava/lang/String;
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 398
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_2

    .line 343
    :cond_f
    :try_start_3
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v12

    .line 344
    .restart local v12    # "certs":[Ljava/security/cert/X509Certificate;
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App id is : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    if-nez v12, :cond_10

    .line 346
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "Cert is null. No cert keystore"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    .line 391
    .end local v12    # "certs":[Ljava/security/cert/X509Certificate;
    :catch_2
    move-exception v15

    .line 392
    .local v15, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v15}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_4

    .line 350
    .end local v15    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v12    # "certs":[Ljava/security/cert/X509Certificate;
    :cond_10
    :try_start_4
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Certs lenth is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v12

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    array-length v1, v12

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v12, v1

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->checkValidity()V
    :try_end_4
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    .line 393
    .end local v12    # "certs":[Ljava/security/cert/X509Certificate;
    :catch_3
    move-exception v15

    .line 394
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 369
    .local v15, "e":Ljava/security/cert/CertificateExpiredException;
    :cond_11
    const-string v1, "AcmsRevocationMngr"

    .line 370
    const-string v2, "Dev ID certificate is expired. Hence make all app as non-certified"

    .line 369
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    new-instance v11, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-direct {v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    .line 372
    .local v11, "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 373
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 374
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 375
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateDevApps(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto/16 :goto_3

    .line 403
    .end local v11    # "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v15    # "e":Ljava/security/cert/CertificateExpiredException;
    .end local v16    # "flagSelfSign":Z
    .end local v19    # "packageName":Ljava/lang/String;
    :pswitch_3
    const-string v1, "AcmsRevocationMngr"

    .line 404
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_IN_QUERY_PERIOD"

    .line 403
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    if-nez v10, :cond_12

    .line 407
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 411
    :cond_12
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v5

    .line 412
    .local v5, "queryTime":J
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 415
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 416
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->getMlConnectTimeEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;

    move-result-object v3

    .line 417
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectTimeEntryInterface;->getRecentMLConnectionTime()J

    move-result-wide v3

    .line 414
    invoke-static/range {v1 .. v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isMlConnectedInQueryPeriod(JJJ)Z

    move-result v1

    .line 417
    if-eqz v1, :cond_13

    .line 418
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 419
    if-eqz v9, :cond_2

    .line 421
    const-string v1, "AcmsRevocationMngr"

    .line 422
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_IN_QUERY_PERIOD - ML is connected during querry Period"

    .line 420
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v1, "AcmsRevocationMngr"

    .line 424
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_IN_QUERY_PERIOD data connectivity is on"

    .line 423
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 426
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 431
    :cond_13
    const-string v1, "AcmsRevocationMngr"

    .line 432
    const-string v2, "mMlCertifiedHandler moved to base grace or drive grace"

    .line 431
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 434
    const-string v1, "AcmsRevocationMngr"

    .line 435
    const-string v2, "mMlCertifiedHandler -moving to drive grace"

    .line 434
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 437
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 440
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    .line 441
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getDriveGraceTimeperiod()J

    move-result-wide v1

    .line 439
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 438
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 443
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 444
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getDiffOfQueryPeriod()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 442
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 446
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 445
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 462
    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 465
    const-string v1, "AcmsRevocationMngr"

    .line 466
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mMlCertifiedHandler - current query period: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 467
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 468
    const-string v3, " grace period: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 469
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 466
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 464
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v1, "AcmsRevocationMngr"

    .line 471
    const-string v2, "mMlCertifiedHandler -moving to manageAlarm()"

    .line 470
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_2

    .line 448
    :cond_14
    const-string v1, "AcmsRevocationMngr"

    .line 449
    const-string v2, "mMlCertifiedHandler -moving to base grace"

    .line 448
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 451
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 454
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    .line 455
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v1

    .line 453
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 452
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 457
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 458
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getDiffOfQueryPeriod()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    .line 456
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 460
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 459
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    goto/16 :goto_5

    .line 478
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 479
    if-eqz v9, :cond_2

    .line 480
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 480
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 487
    :cond_16
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 486
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 493
    .end local v5    # "queryTime":J
    :pswitch_4
    const-string v1, "AcmsRevocationMngr"

    .line 494
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD"

    .line 493
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 496
    if-eqz v9, :cond_17

    .line 497
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 497
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 500
    :cond_17
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "appEntries is null."

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 504
    :cond_18
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "mMlCertifiedHandler -moving to base grace"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    if-nez v10, :cond_19

    .line 506
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence break"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 509
    :cond_19
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 510
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    .line 511
    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertInfoToNonDriveMode(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Ljava/lang/String;

    move-result-object v1

    .line 510
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 512
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 515
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    .line 516
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v1

    .line 514
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 513
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 518
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 517
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 519
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 521
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_2

    .line 529
    :pswitch_5
    const-string v1, "AcmsRevocationMngr"

    .line 530
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_IN_BASE_GRACE_PERIOD"

    .line 529
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 532
    if-eqz v9, :cond_1a

    .line 533
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 534
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 533
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 536
    :cond_1a
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "appEntries is null."

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 541
    :cond_1b
    const-string v1, "AcmsRevocationMngr"

    .line 542
    const-string v2, "mMlCertifiedHandler -certificate has crossed grace period..now its unchecked and will be tried till 6 month"

    .line 540
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    if-nez v10, :cond_1c

    .line 544
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 547
    :cond_1c
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 548
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 551
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    .line 552
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getBaseGraceTimeperiod()J

    move-result-wide v1

    .line 550
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 549
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 554
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 553
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 555
    const/16 v1, 0xfc0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 556
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 558
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_2

    .line 564
    :pswitch_6
    const-string v1, "AcmsRevocationMngr"

    .line 565
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_MLAWARE_UNCHECKED"

    .line 564
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    if-nez v10, :cond_1d

    .line 567
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 570
    :cond_1d
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 572
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_1f

    .line 574
    const-string v1, "AcmsRevocationMngr"

    .line 575
    const-string v2, "Max retry is 0 or negative hence setting querry and grace to max and rescheduling alarm"

    .line 573
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 576
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 585
    :cond_1e
    :goto_6
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max Retry is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 587
    if-eqz v9, :cond_20

    .line 588
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 589
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 588
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 578
    :cond_1f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1e

    .line 579
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    .line 580
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v1

    .line 582
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v2

    .line 583
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v2

    long-to-int v2, v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 579
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    goto :goto_6

    .line 591
    :cond_20
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "appEntries is null."

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 595
    :cond_21
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 601
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v1

    .line 602
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v1

    .line 600
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 599
    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriod(Ljava/lang/Long;)V

    .line 605
    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 604
    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 606
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 608
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # invokes: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->manageAlarm(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V
    invoke-static {v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_2

    .line 614
    :pswitch_7
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "mMlCertifiedHandler - STATE_REVOCATION_CHECK"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDataConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 617
    if-eqz v9, :cond_22

    .line 618
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mRevokationHandler:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$1(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;

    move-result-object v1

    .line 619
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v3

    .line 618
    move/from16 v0, v17

    invoke-virtual {v1, v9, v2, v3, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z

    goto/16 :goto_2

    .line 621
    :cond_22
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "appEntries is null."

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 625
    :cond_23
    const-string v1, "AcmsRevocationMngr"

    .line 626
    const-string v2, "mMlCertifiedHandler - data Connection not available "

    .line 625
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mOcspCallback:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$2(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;

    move-result-object v1

    const-string v2, "no_package_to_show"

    .line 628
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ERROR_NO_DATA_CONNECTIVITY:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v3

    .line 627
    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$OcspCallback;->notifyResult(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 635
    :pswitch_8
    const-string v1, "AcmsRevocationMngr"

    .line 636
    const-string v2, "mMlCertifiedHandler - STATE_OCSPCERT_REVOKED: Hence go and refetch the certificate"

    .line 634
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    if-nez v10, :cond_24

    .line 638
    const-string v1, "AcmsRevocationMngr"

    const-string v2, "App Entry is null. Hence return"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 641
    :cond_24
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 643
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Is Revoked is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 647
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v1

    .line 648
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 651
    .restart local v13    # "devId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v1

    .line 652
    invoke-virtual {v1, v13}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->deleteFromKeyStore(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 653
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 654
    const-string v3, " entry deleted from keystore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 653
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :goto_7
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 661
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 656
    :cond_25
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 657
    const-string v3, " entry not found in keystore "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 656
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 664
    .end local v13    # "devId":Ljava/lang/String;
    :cond_26
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 665
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 666
    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 667
    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 669
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CCC"

    const-string v3, "DEVELOPER"

    .line 668
    invoke-virtual {v7, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->replaceEntityInAppCertInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 671
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Is Revoked is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 674
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 673
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 679
    :cond_27
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Is Revoked is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 682
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 681
    invoke-virtual {v7, v1, v10}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 684
    :cond_28
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 685
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 686
    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 687
    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v10, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 688
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 689
    const-string v1, "AcmsRevocationMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Is Revoked is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$1;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 692
    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 691
    invoke-virtual {v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppAdded(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
