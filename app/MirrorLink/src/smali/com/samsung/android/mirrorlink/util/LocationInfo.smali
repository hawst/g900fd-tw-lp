.class Lcom/samsung/android/mirrorlink/util/LocationInfo;
.super Ljava/lang/Object;
.source "MirrorLinkManagerUtil.java"


# instance fields
.field private mRestricted:Ljava/lang/String;

.field private mUnrestricted:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRestricted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/LocationInfo;->mRestricted:Ljava/lang/String;

    return-object v0
.end method

.method public getUnrestricted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/LocationInfo;->mUnrestricted:Ljava/lang/String;

    return-object v0
.end method

.method public setRestricted(Ljava/lang/String;)V
    .locals 0
    .param p1, "Content"    # Ljava/lang/String;

    .prologue
    .line 443
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/LocationInfo;->mRestricted:Ljava/lang/String;

    .line 444
    return-void
.end method

.method public setUnrestricted(Ljava/lang/String;)V
    .locals 0
    .param p1, "rootContent"    # Ljava/lang/String;

    .prologue
    .line 447
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/LocationInfo;->mUnrestricted:Ljava/lang/String;

    .line 448
    return-void
.end method
