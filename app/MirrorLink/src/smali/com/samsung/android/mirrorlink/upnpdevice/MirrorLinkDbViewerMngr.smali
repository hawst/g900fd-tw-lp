.class public Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;
.super Lcom/mirrorlink/android/service/IMirrorLinkDbviewer$Stub;
.source "MirrorLinkDbViewerMngr.java"


# static fields
.field private static final ACMS_SERVICE_INTENT:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.api.IAcmsDbviewer"

.field private static final TAG:Ljava/lang/String; = "TMServer/MirrorLinkDbViewerMngr"


# instance fields
.field private mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

.field private mContext:Landroid/content/Context;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/mirrorlink/android/service/IMirrorLinkDbviewer$Stub;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    return-void
.end method


# virtual methods
.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 60
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    .line 61
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 62
    return-void
.end method

.method public getDevId()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    const-string v1, "TMServer/MirrorLinkDbViewerMngr"

    const-string v2, "Enter getDevId "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "devId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    invoke-interface {v1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;->getDevId()Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 83
    :cond_0
    const-string v1, "TMServer/MirrorLinkDbViewerMngr"

    const-string v2, "Acms Db Service is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    .line 32
    const-string v1, "TMServer/MirrorLinkDbViewerMngr"

    const-string v2, "Enter Init()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    if-nez v1, :cond_0

    .line 35
    const-string v1, "TMServer/MirrorLinkDbViewerMngr"

    const-string v2, "mServiceConnection is null. So creating"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    new-instance v1, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr$1;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 50
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.mirrorlink.acms.api.IAcmsDbviewer"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 53
    const/4 v3, 0x1

    .line 52
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 54
    return-void
.end method

.method public setDevId(Ljava/lang/String;)V
    .locals 2
    .param p1, "devId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    const-string v0, "TMServer/MirrorLinkDbViewerMngr"

    const-string v1, "Enter setDevId "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    invoke-interface {v0, p1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;->setDevId(Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    const-string v0, "TMServer/MirrorLinkDbViewerMngr"

    const-string v1, "Acms Db Service is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDevUserSetting(Z)V
    .locals 2
    .param p1, "userSetting"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    const-string v0, "TMServer/MirrorLinkDbViewerMngr"

    const-string v1, "Enter setDevUserSetting "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->mAcmsDbService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;

    invoke-interface {v0, p1}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsDbviewer;->setDevUserSetting(Z)V

    .line 100
    :goto_0
    return-void

    .line 97
    :cond_0
    const-string v0, "TMServer/MirrorLinkDbViewerMngr"

    const-string v1, "Acms Db Service is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
