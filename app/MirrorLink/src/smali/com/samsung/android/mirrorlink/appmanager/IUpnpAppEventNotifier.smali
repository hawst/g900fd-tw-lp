.class public interface abstract Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
.super Ljava/lang/Object;
.source "IUpnpAppEventNotifier.java"


# virtual methods
.method public abstract getRelatedAppId()I
.end method

.method public abstract getRelatedAppName()Ljava/lang/String;
.end method

.method public abstract getSupportedClients()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract invokeNoti(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;)Z
.end method

.method public abstract motinor(Z)Z
.end method

.method public abstract registerNotiHandler(Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;)V
.end method

.method public abstract setClient(ILjava/util/ArrayList;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation
.end method
