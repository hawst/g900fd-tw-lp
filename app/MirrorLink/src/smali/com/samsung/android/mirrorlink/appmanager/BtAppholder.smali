.class public Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.super Ljava/lang/Object;
.source "BtAppholder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;,
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;,
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;,
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;
    }
.end annotation


# static fields
.field private static final ACS_ADAPTER_STATE_A2DP_CONNECTED:I = 0xc

.field private static final ACS_ADAPTER_STATE_A2DP_DISCONNECTED:I = 0xd

.field private static final ACS_ADAPTER_STATE_BOND_STATE_BONDED:I = 0xb

.field private static final ACS_ADAPTER_STATE_BOND_STATE_BONDING:I = 0xa

.field private static final ACS_ADAPTER_STATE_BOND_STATE_NONE:I = 0x9

.field private static final ACS_ADAPTER_STATE_HFP_CONNECTED:I = 0xe

.field private static final ACS_ADAPTER_STATE_HFP_DISCONNECTED:I = 0xf

.field private static final ACS_ADAPTER_STATE_INVALID:I = 0x0

.field private static final ACS_ADAPTER_STATE_OFF:I = 0x3

.field private static final ACS_ADAPTER_STATE_ON:I = 0x1

.field private static final CHARSET_UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final DEFAULT_DISCOVERABLE_PERIOD:I = 0x78

.field private static final DEFAULT_S3_A2DP_SEID:I = 0x1

.field private static final DEFAULT_S3_HFP_RFCOMM_CHANNEL:I = 0xa

.field private static final LOG_TAG:Ljava/lang/String; = "TMSBtAppholder"

.field private static final TM_APP_BT_A2DP_CONTENT_CATEGORY:I = 0x2

.field private static final TM_APP_BT_HFP_CONTENT_CATEGORY:I = 0x1

.field public static final TM_BLUETOOH_INITED:I = 0x0

.field private static final TM_BLUETOOTH_DIRECTION_BI:Ljava/lang/String; = "bi"

.field private static final TM_BLUETOOTH_DIRECTION_OUT:Ljava/lang/String; = "out"

.field private static final TM_BLUETOOTH_HOLDERNAME:Ljava/lang/String; = "BtAppHolder"

.field private static final TM_BLUETOOTH_PROTOID_A2DP:Ljava/lang/String; = "BTA2DP"

.field private static final TM_BLUETOOTH_PROTOID_HFP:Ljava/lang/String; = "BTHFP"

.field private static final TM_BLUETOOTH_RESOURCE_STATUS_BUSY:Ljava/lang/String; = "busy"

.field private static final TM_BLUETOOTH_RESOURCE_STATUS_FREE:Ljava/lang/String; = "free"

.field private static final TM_BLUETOOTH_RESOURCE_STATUS_NA:Ljava/lang/String; = "NA"

.field private static final TM_EVENT_ENABLE_DISCOVERABLE:I = 0x5

.field private static final TM_EVENT_RESTART_ADAPTER:I = 0x6

.field private static final TM_EVENT_STATE_CHANGE_DELAYED:I = 0x4

.field private static final TM_EVENT_TMSERVER_MAC_SET:I = 0x3

.field private static final TM_TRUST_LEVEL:I = 0x80

.field private static mFlagClientStartConnection:Z

.field private static mFlagServerStartConnection:Z

.field private static mTmClientMac:Ljava/lang/String;


# instance fields
.field private final TM_BLUETOOTH_FORMAT:Ljava/lang/String;

.field private mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

.field private mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppIsRunningA2dp:Z

.field private mAppIsRunningHfp:Z

.field private mAppNameA2dp:Ljava/lang/String;

.field private mAppNameHfp:Ljava/lang/String;

.field private mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCntxt:Landroid/content/Context;

.field public mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mCurAcsAdapterState:I

.field private mCurDiscoverablePeriod:I

.field private mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

.field private mDescriptionA2dp:Ljava/lang/String;

.field private mDescriptionHfp:Ljava/lang/String;

.field private mExtListner:Landroid/os/Handler;

.field private mFlagBluetoothOnbyTMApp:Z

.field private mFlagMacReadBtoff:Z

.field private mFlagSelfMacFound:Z

.field private mHandler:Landroid/os/Handler;

.field private mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

.field private mOverridenHfpDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mOverridingEnable:Z

.field mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

.field private mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

.field private mTmServerMac:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->TM_BLUETOOTH_FORMAT:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 420
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$1;-><init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    .line 396
    const-string v0, "TMSBtAppholder"

    const-string v1, "Enter : BtAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    .line 399
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 400
    const-string v0, "TMSBtAppholder"

    const-string v1, "ERROR !!! No Context"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :goto_0
    return-void

    .line 403
    :cond_0
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    invoke-direct {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    .line 404
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .line 411
    const-string v0, "TMSBtAppholder"

    const-string v1, "Exit : BtAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static SetTMClientMac(Ljava/lang/String;)V
    .locals 3
    .param p0, "mac"    # Ljava/lang/String;

    .prologue
    .line 1138
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " SetTMClientMac : Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    if-eqz p0, :cond_2

    .line 1141
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 1143
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " SetTMClientMac : Wrong MAC address len "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    :goto_0
    return-void

    .line 1146
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    .line 1147
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->addMacOctetSeperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1148
    invoke-static {p0}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1150
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " SetTMClientMac : checkBluetoothAddress FALIED "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1153
    :cond_1
    sput-object p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    .line 1155
    :cond_2
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " SetTMClientMac: Exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 918
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)I
    .locals 1

    .prologue
    .line 334
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getAcsBondedState(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$16()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$17(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1236
    invoke-static {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isMacTMClient(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)V
    .locals 0

    .prologue
    .line 204
    iput p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurDiscoverablePeriod:I

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;II)Z
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->checkMakeDiscoverable(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurDiscoverablePeriod:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Landroid/bluetooth/BluetoothAdapter;II)V
    .locals 0

    .prologue
    .line 2266
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->callBluetoothAdapterSetScanMode(Landroid/bluetooth/BluetoothAdapter;II)V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z
    .locals 1

    .prologue
    .line 1214
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z
    .locals 1

    .prologue
    .line 1341
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 868
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleDeviceFound(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)I
    .locals 1

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getAcsState(I)I

    move-result v0

    return v0
.end method

.method private static addMacOctetSeperator(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 2841
    if-nez p0, :cond_0

    .line 2843
    const-string v5, "TMSBtAppholder"

    const-string v6, "addMacOctetSeperator : INVALID INPUT"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2844
    const/4 v5, 0x0

    .line 2860
    :goto_0
    return-object v5

    .line 2846
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 2847
    .local v4, "length":I
    const-string v5, "TMSBtAppholder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " addMacOctetSeperator: length "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2848
    add-int/lit8 v5, v4, 0x5

    new-array v0, v5, [C

    .line 2850
    .local v0, "buf":[C
    const/4 v1, 0x0

    .line 2851
    .local v1, "bufIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, v1

    .end local v1    # "bufIndex":I
    .local v2, "bufIndex":I
    :goto_1
    if-lt v3, v4, :cond_1

    .line 2860
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0

    .line 2853
    :cond_1
    rem-int/lit8 v5, v3, 0x2

    if-nez v5, :cond_2

    if-eqz v3, :cond_2

    .line 2855
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bufIndex":I
    .restart local v1    # "bufIndex":I
    const/16 v5, 0x3a

    aput-char v5, v0, v2

    .line 2857
    :goto_2
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bufIndex":I
    .restart local v2    # "bufIndex":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, v0, v1

    .line 2851
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .end local v2    # "bufIndex":I
    .restart local v1    # "bufIndex":I
    goto :goto_2
.end method

.method private callBluetoothAdapterSetScanMode(Landroid/bluetooth/BluetoothAdapter;II)V
    .locals 7
    .param p1, "adapter"    # Landroid/bluetooth/BluetoothAdapter;
    .param p2, "type"    # I
    .param p3, "durationSeconds"    # I

    .prologue
    .line 2270
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setScanMode"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2271
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2279
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 2272
    :catch_0
    move-exception v0

    .line 2273
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothAdapterSetScanMode."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2274
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2275
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothAdapterSetScanMode."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2276
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2277
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothAdapterSetScanMode."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callBluetoothDeviceCreateBond(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2240
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "createBond"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2241
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2249
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 2242
    :catch_0
    move-exception v0

    .line 2243
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothDeviceCreateBond."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2244
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2245
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothDeviceCreateBond."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2246
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2247
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothDeviceCreateBond."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkMakeDiscoverable(II)Z
    .locals 4
    .param p1, "scanMode"    # I
    .param p2, "timeout"    # I

    .prologue
    .line 368
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter : checkMakeDiscoverable scamode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " timeout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "makeDiscoverable":Z
    const/16 v1, 0x17

    if-eq p1, v1, :cond_1

    .line 372
    const/4 v0, 0x1

    .line 383
    :cond_0
    :goto_0
    const-string v1, "TMSBtAppholder"

    const-string v2, "Exit : checkMakeDiscoverable"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    return v0

    .line 376
    :cond_1
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " checkMakeDiscoverable : Discoverable period 120 cur "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    const/16 v1, 0x78

    if-ge p2, v1, :cond_0

    .line 380
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private deInitAdapter()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1303
    const-string v1, "TMSBtAppholder"

    const-string v2, "Enter: deInitAdapter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    if-eqz v1, :cond_1

    .line 1306
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1307
    const-string v1, "TMSBtAppholder"

    const-string v2, "deInitAdapter : unregisterReceiver"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->resetStartConfig()V

    .line 1316
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyAudioDeviceConnected()Z

    move-result v0

    .line 1318
    .local v0, "anyConnected":Z
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->deinitDeviceProfileManagers()V

    .line 1320
    if-nez v0, :cond_0

    .line 1322
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOff()Z

    .line 1327
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    .line 1328
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1329
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurAcsAdapterState:I

    .line 1330
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    .line 1331
    sput-object v3, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    .line 1332
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    .line 1334
    const-string v1, "TMSBtAppholder"

    const-string v2, "Exit: deInitAdapter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    return-void

    .line 1311
    .end local v0    # "anyConnected":Z
    :cond_1
    const-string v1, "TMSBtAppholder"

    const-string v2, "(mReceiver == null ??)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deinitDeviceProfileManagers()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2178
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v0, :cond_0

    .line 2180
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->deinit()V

    .line 2181
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 2183
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    if-eqz v0, :cond_1

    .line 2185
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->deinit()V

    .line 2186
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 2188
    :cond_1
    return-void
.end method

.method private fillAppStatus()V
    .locals 4

    .prologue
    const/16 v3, 0x16

    const/16 v2, 0x14

    .line 1638
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-nez v0, :cond_1

    .line 1640
    :cond_0
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus Variavles are null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    :goto_0
    return-void

    .line 1644
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyA2dpDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1646
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOverrideEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1648
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "busy"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 1654
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    if-eqz v0, :cond_2

    .line 1656
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->isTmConnected()Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1658
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus A2DP : HU is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1664
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 1674
    :cond_2
    :goto_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyHfpDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1676
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOverrideEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1678
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "busy"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 1684
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v0, :cond_3

    .line 1686
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1688
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus HFP : HU is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1694
    :goto_5
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 1703
    :cond_3
    :goto_6
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " HFP ---> APP STATUS "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RESOURCE STATUS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " A2DP --> APP STATUS "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RESOURCE STATUS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1706
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1652
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "NA"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    goto/16 :goto_1

    .line 1662
    :cond_5
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus A2DP : HS is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1669
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 1670
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 1671
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus A2DP :Nothing is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1682
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "NA"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    goto/16 :goto_4

    .line 1692
    :cond_8
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus HFP : HS is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1699
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 1700
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 1701
    const-string v0, "TMSBtAppholder"

    const-string v1, "fillAppStatus HFP :Nothing is Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method private geFlagBluetoothOnbyTMApp()Z
    .locals 3

    .prologue
    .line 276
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "geFlagBluetoothOnbyTMApp = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagBluetoothOnbyTMApp:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagBluetoothOnbyTMApp:Z

    return v0
.end method

.method public static getA2dpSeid()I
    .locals 2

    .prologue
    .line 296
    const-string v0, "TMSBtAppholder"

    const-string v1, "getA2dpSeid = 1"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/4 v0, 0x1

    return v0
.end method

.method private getAcsBondedState(I)I
    .locals 1
    .param p1, "bondstate"    # I

    .prologue
    .line 336
    packed-switch p1, :pswitch_data_0

    .line 345
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 339
    :pswitch_0
    const/16 v0, 0xa

    goto :goto_0

    .line 341
    :pswitch_1
    const/16 v0, 0xb

    goto :goto_0

    .line 343
    :pswitch_2
    const/16 v0, 0x9

    goto :goto_0

    .line 336
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAcsState(I)I
    .locals 1
    .param p1, "btState"    # I

    .prologue
    .line 317
    packed-switch p1, :pswitch_data_0

    .line 324
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 320
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 322
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getHfpRfCommChannel()I
    .locals 2

    .prologue
    .line 306
    const-string v0, "TMSBtAppholder"

    const-string v1, "getHfpRfCommChannel = 10"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const/16 v0, 0xa

    return v0
.end method

.method private handleDeviceFound(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 870
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 871
    .local v0, "mac":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 872
    .local v1, "name":Ljava/lang/String;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleDeviceFound Enter device = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isMacTMClient(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getFlagStartConnection()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 875
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 880
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isPairedDevice(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 881
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v2, :cond_2

    .line 883
    :cond_0
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleDeviceFound : createBond device = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->callBluetoothDeviceCreateBond(Landroid/bluetooth/BluetoothDevice;)V

    .line 910
    :cond_1
    :goto_0
    const-string v2, "TMSBtAppholder"

    const-string v3, "handleDeviceFound exit "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    return-void

    .line 892
    :cond_2
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleDeviceFound :mAppIsRunningHfp :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 893
    const-string v4, " mAppIsRunningA2dp : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 892
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v2, :cond_3

    .line 897
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v2, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 898
    const-string v2, "TMSBtAppholder"

    const-string v3, "handleDeviceFound :hfp connect failed retry"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v2, :cond_1

    .line 905
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v2, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method

.method private handleStateChange(ILjava/lang/Object;)V
    .locals 12
    .param p1, "curState"    # I
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/16 v11, 0x16

    const/16 v10, 0x14

    const/4 v9, 0x2

    .line 920
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "handleStateChange curState = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    iput p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurAcsAdapterState:I

    .line 924
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyA2dpDeviceConnected()Z

    move-result v6

    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyHfpDeviceConnected()Z

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->informCommonApi(ZZ)V

    .line 926
    iget v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCurAcsAdapterState:I

    sparse-switch v6, :sswitch_data_0

    .line 1089
    const-string v6, "TMSBtAppholder"

    const-string v7, " Unhandled "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    :cond_0
    :goto_0
    return-void

    .line 930
    :sswitch_0
    const-string v6, "TMSBtAppholder"

    const-string v7, " handleStateChange :ACS_ADAPTER_STATE_ON : "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v5

    .line 933
    .local v5, "temp":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    if-nez v6, :cond_1

    if-nez v5, :cond_1

    .line 935
    const-string v6, "TMSBtAppholder"

    const-string v7, " handleStateChange :ACS_ADAPTER_STATE_ON : No MAC Error !"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOff()Z

    .line 937
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 938
    .local v4, "msg":Landroid/os/Message;
    const/4 v6, 0x6

    iput v6, v4, Landroid/os/Message;->what:I

    .line 939
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x3e8

    invoke-virtual {v6, v4, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 943
    .end local v4    # "msg":Landroid/os/Message;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    if-nez v6, :cond_2

    .line 946
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->writeMacToStorage(Ljava/lang/String;)V
    invoke-static {v6, v5}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;Ljava/lang/String;)V

    .line 947
    invoke-virtual {p0, v5}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->SetTMServerMac(Ljava/lang/String;)V

    .line 948
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setStartConfig()V

    goto :goto_0

    .line 953
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto :goto_0

    .line 960
    .end local v5    # "temp":Ljava/lang/String;
    :sswitch_1
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 963
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 964
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ACS_ADAPTER_STATE_BOND_STATE_BONDED : mAppIsRunningHfp :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 965
    iget-boolean v8, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "mAppIsRunningA2dp : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 964
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isMacTMClient(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 969
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getFlagStartConnection()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 971
    iget-boolean v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v6, :cond_4

    .line 973
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v6

    if-ne v6, v9, :cond_3

    .line 974
    const-string v6, "TMSBtAppholder"

    const-string v7, "BluetoothProfile HFP Already connected"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 977
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v6, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z

    .line 979
    :cond_4
    iget-boolean v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v6, :cond_0

    .line 981
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Landroid/bluetooth/BluetoothA2dp;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/bluetooth/BluetoothA2dp;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v6

    if-ne v6, v9, :cond_5

    .line 982
    const-string v6, "TMSBtAppholder"

    const-string v7, "BluetoothProfile A2DP Already connected"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 985
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v6, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 995
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :sswitch_2
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_A2DP_CONNECTED"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-direct {p0, v6, v10}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->informAppStatusChange(Ljava/lang/String;I)V

    .line 998
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 1000
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1001
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 1002
    .local v3, "deviceMac":Ljava/lang/String;
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ACS_ADAPTER_STATE_A2DP_CONNECTED :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Client "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1009
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "deviceMac":Ljava/lang/String;
    :sswitch_3
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_HFP_CONNECTED"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-direct {p0, v6, v10}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->informAppStatusChange(Ljava/lang/String;I)V

    .line 1011
    const/4 v3, 0x0

    .line 1012
    .restart local v3    # "deviceMac":Ljava/lang/String;
    if-eqz p2, :cond_6

    move-object v1, p2

    .line 1014
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1015
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 1016
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ACS_ADAPTER_STATE_HFP_CONNECTED :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Client "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_6
    if-eqz v3, :cond_0

    sget-object v6, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1020
    sget-object v6, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1023
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_HFP_CONNECTED : Not TM Client Connected"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget-object v7, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 1025
    .local v0, "connecteddevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v6, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 1026
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v6, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 1033
    .end local v0    # "connecteddevice":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "deviceMac":Ljava/lang/String;
    :sswitch_4
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_A2DP_DISCONNECTED :"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-direct {p0, v6, v11}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->informAppStatusChange(Ljava/lang/String;I)V

    .line 1035
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 1037
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1038
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 1039
    .restart local v3    # "deviceMac":Ljava/lang/String;
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ACS_ADAPTER_STATE_A2DP_DISCONNECTED :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Client "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1045
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "deviceMac":Ljava/lang/String;
    :sswitch_5
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_HFP_DISCONNECTED :"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-direct {p0, v6, v11}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->informAppStatusChange(Ljava/lang/String;I)V

    .line 1047
    const/4 v3, 0x0

    .line 1048
    .restart local v3    # "deviceMac":Ljava/lang/String;
    if-eqz p2, :cond_7

    move-object v1, p2

    .line 1050
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1051
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 1052
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ACS_ADAPTER_STATE_HFP_DISCONNECTED :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Client "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_7
    if-eqz v3, :cond_0

    sget-object v6, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1056
    sget-object v6, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1058
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridenHfpDeviceList:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 1060
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v6, :cond_0

    .line 1062
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridenHfpDeviceList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 1064
    .local v2, "deviceEle":Landroid/bluetooth/BluetoothDevice;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v7, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_1

    .line 1072
    .end local v2    # "deviceEle":Landroid/bluetooth/BluetoothDevice;
    :cond_8
    const-string v6, "TMSBtAppholder"

    const-string v7, "ACS_ADAPTER_STATE_HFP_DISCONNECTED : Not TM Client Disconnected"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073
    iget-boolean v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v6, :cond_0

    .line 1075
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1077
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto/16 :goto_0

    .line 1081
    :cond_9
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z

    goto/16 :goto_0

    .line 926
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_4
        0xe -> :sswitch_3
        0xf -> :sswitch_5
    .end sparse-switch
.end method

.method private informAppStatusChange(Ljava/lang/String;I)V
    .locals 3
    .param p1, "app"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 1747
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->fillAppStatus()V

    .line 1748
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    if-eqz v1, :cond_0

    .line 1749
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1750
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v1, :cond_0

    .line 1751
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v1, p1, v2}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 1752
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1753
    .local v0, "newAppInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1754
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onPackageStatusChange(Ljava/lang/String;Ljava/util/Map;)V

    .line 1767
    .end local v0    # "newAppInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_0
    :goto_0
    return-void

    .line 1757
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1758
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v1, :cond_0

    .line 1759
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v1, p1, v2}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 1760
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1761
    .restart local v0    # "newAppInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1762
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onPackageStatusChange(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method private informCommonApi(ZZ)V
    .locals 3
    .param p1, "a2dpConn"    # Z
    .param p2, "hfpConn"    # Z

    .prologue
    .line 1733
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "informCommonApi Enter a2dp "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hfp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1735
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setBtA2dpInfo(Z)V

    .line 1736
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setBtHfpInfo(Z)V

    .line 1738
    :cond_0
    const-string v0, "TMSBtAppholder"

    const-string v1, "informCommonApi Exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1739
    return-void
.end method

.method private initAdapter()Z
    .locals 5

    .prologue
    .line 1261
    const-string v2, "TMSBtAppholder"

    const-string v3, "Enter : initAdapter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1263
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v2, :cond_0

    .line 1265
    const-string v2, "TMSBtAppholder"

    const-string v3, "BluetoothAdapter.getDefaultAdapter() Bluetooth is not Supported"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const/4 v2, 0x0

    .line 1295
    :goto_0
    return v2

    .line 1269
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->registerBtListner()V

    .line 1271
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 1272
    .local v0, "fwAddress":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->getMacAddress()Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;)Ljava/lang/String;

    move-result-object v1

    .line 1273
    .local v1, "storageAddress":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1275
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "initAdapter : Bluetooth Fw Address "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->writeMacToStorage(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;Ljava/lang/String;)V

    .line 1277
    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->SetTMServerMac(Ljava/lang/String;)V

    .line 1278
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setStartConfig()V

    .line 1289
    :cond_1
    :goto_1
    const-string v2, "TMSBtAppholder"

    const-string v3, "initAdapter : Bluetooth Adapter turn On "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z

    .line 1292
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->initDeviceProfileManagers()V

    .line 1294
    const-string v2, "TMSBtAppholder"

    const-string v3, "Exit :initAdapter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    const/4 v2, 0x1

    goto :goto_0

    .line 1280
    :cond_2
    if-eqz v1, :cond_1

    .line 1282
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "initAdapter : Bluetooth Storage Address "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->SetTMServerMac(Ljava/lang/String;)V

    .line 1284
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setStartConfig()V

    goto :goto_1
.end method

.method private initDeviceProfileManagers()V
    .locals 3

    .prologue
    .line 2166
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 2167
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->init()V

    .line 2169
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 2170
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->init()V

    .line 2171
    return-void
.end method

.method private isAnyA2dpDeviceConnected()Z
    .locals 2

    .prologue
    .line 2206
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    if-eqz v1, :cond_0

    .line 2208
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Ljava/util/List;

    move-result-object v0

    .line 2209
    .local v0, "listA2dp":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    .line 2211
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2213
    const/4 v1, 0x1

    .line 2217
    .end local v0    # "listA2dp":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isAnyAudioDeviceConnected()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2192
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyA2dpDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2201
    :cond_0
    :goto_0
    return v0

    .line 2197
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyHfpDeviceConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2201
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAnyHfpDeviceConnected()Z
    .locals 2

    .prologue
    .line 2222
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v1, :cond_0

    .line 2224
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->getConnectedDevices()Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Ljava/util/List;

    move-result-object v0

    .line 2225
    .local v0, "listHfp":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    .line 2227
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2229
    const/4 v1, 0x1

    .line 2233
    .end local v0    # "listHfp":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isMacTMClient(Ljava/lang/String;)Z
    .locals 4
    .param p0, "mac"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1238
    sget-object v1, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1240
    const-string v1, "TMSBtAppholder"

    const-string v2, "isMacTMClient(mTmClientMac == null)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    :goto_0
    return v0

    .line 1244
    :cond_0
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isMacTMClient mTmClientMac ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1246
    sget-object v1, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1248
    const-string v1, "TMSBtAppholder"

    const-string v2, "isMacTMClient(mTmClientMac.equalsIgnoreCase(mac) == false)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1252
    :cond_1
    const-string v0, "TMSBtAppholder"

    const-string v1, "isMacTMClient == True"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1216
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v2, :cond_0

    .line 1218
    const-string v2, "TMSBtAppholder"

    const-string v3, "(mBluetoothAdapter == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    :goto_0
    return v1

    .line 1221
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    .line 1222
    .local v0, "ret":Z
    if-eqz v0, :cond_1

    .line 1224
    const-string v1, "TMSBtAppholder"

    const-string v2, "mBluetoothAdapter.isEnabled == true"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    const/4 v1, 0x1

    goto :goto_0

    .line 1227
    :cond_1
    const-string v2, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.isEnabled == false"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isOverrideEnabled()Z
    .locals 3

    .prologue
    .line 2130
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isOverrideEnabled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridingEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2131
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridingEnable:Z

    return v0
.end method

.method private isPairedDevice(Ljava/lang/String;)Z
    .locals 6
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 494
    const-string v3, "TMSBtAppholder"

    const-string v4, " isPairedDevice : Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    .line 496
    .local v0, "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v0, :cond_0

    .line 498
    const-string v3, "TMSBtAppholder"

    const-string v4, " isPairedDevice : NULL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :goto_0
    return v2

    .line 505
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 514
    const-string v3, "TMSBtAppholder"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " isPairedDevice : Exit size = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 507
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 509
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " isPairedDevice : TM Addr "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 510
    const-string v4, " Name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 509
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private launchAppBtA2dp()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1518
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v2, :cond_0

    .line 1520
    const-string v2, "TMSBtAppholder"

    const-string v3, "launchAppBtA2dp : A2dp is already launched "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    :goto_0
    return v1

    .line 1524
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    .line 1525
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyA2dpDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1527
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->isTmConnected()Z
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1529
    const-string v2, "TMSBtAppholder"

    const-string v3, "launchAppBtA2dp:(mA2dpManager.isTmConnected() == true)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V

    goto :goto_0

    .line 1534
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOverrideEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1536
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Ljava/util/List;

    move-result-object v0

    .line 1537
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->saveOverrideA2dpData(Ljava/util/List;)V

    .line 1538
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disConnectDevices()V
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)V

    .line 1539
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto :goto_0

    .line 1543
    .end local v0    # "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    const-string v1, "TMSBtAppholder"

    const-string v2, "launchAppBtA2dp: Keep existing connection"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    const/4 v1, 0x0

    goto :goto_0

    .line 1550
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1552
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto :goto_0

    .line 1556
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z

    goto :goto_0
.end method

.method private launchAppBtHfp()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1587
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v2, :cond_0

    .line 1589
    const-string v2, "TMSBtAppholder"

    const-string v3, "launchAppBtHfp : Hfp is already launched"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1628
    :goto_0
    return v1

    .line 1592
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    .line 1593
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyHfpDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1595
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1597
    const-string v2, "TMSBtAppholder"

    const-string v3, "launchAppBtHfp:(mHfpManager.isTmConnected() == true)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V

    goto :goto_0

    .line 1602
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOverrideEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1604
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->getConnectedDevices()Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Ljava/util/List;

    move-result-object v0

    .line 1605
    .local v0, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->saveOverrideHfpData(Ljava/util/List;)V

    .line 1606
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disConnectDevices()V
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)V

    .line 1608
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto :goto_0

    .line 1612
    .end local v0    # "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    const-string v1, "TMSBtAppholder"

    const-string v2, "launchAppBtHfp: Keep existing connection"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1613
    const/4 v1, 0x0

    goto :goto_0

    .line 1619
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1621
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->startAppWhenAdapterOn()V

    goto :goto_0

    .line 1625
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOn()Z

    goto :goto_0
.end method

.method private readAppNames()V
    .locals 1

    .prologue
    .line 1411
    const-string v0, "Bluetooth HFP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    .line 1412
    const-string v0, "Bluetooth A2DP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    .line 1414
    const-string v0, "Bluetooth HFP Audio"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDescriptionHfp:Ljava/lang/String;

    .line 1415
    const-string v0, "Bluetooth A2DP Audio Server"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDescriptionA2dp:Ljava/lang/String;

    .line 1416
    return-void
.end method

.method private readPrefences()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 354
    const-string v2, "TMSBtAppholder"

    const-string v3, "Enter : readPrefences"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 357
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "bt_ConnectionInitateEnable"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 358
    .local v0, "ConnectionPref":Z
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ConnectionPref "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setFlagServerStartConnection(Z)V

    .line 361
    const-string v2, "bt_OverridingEnable"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridingEnable:Z

    .line 362
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OverrindingEnable "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridingEnable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v2, "TMSBtAppholder"

    const-string v3, "Exit : readPrefences"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    return-void
.end method

.method private registerBtListner()V
    .locals 3

    .prologue
    .line 526
    const-string v1, "TMSBtAppholder"

    const-string v2, "Enter : registerBtListner"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 530
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 531
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 532
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 533
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 535
    const-string v1, "android.bluetooth.device.action.UUID"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 537
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;-><init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    .line 539
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mReceiver:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 541
    const-string v1, "TMSBtAppholder"

    const-string v2, "Exit : registerBtListner"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    return-void
.end method

.method private removeMacOctetSeperator(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "in"    # Ljava/lang/String;

    .prologue
    .line 2821
    if-nez p1, :cond_0

    .line 2823
    const-string v5, "TMSBtAppholder"

    const-string v6, "removeMacOctetSeperator : INVALID INPUT"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2824
    const/4 v5, 0x0

    .line 2836
    :goto_0
    return-object v5

    .line 2826
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 2827
    .local v4, "length":I
    add-int/lit8 v5, v4, -0x5

    new-array v0, v5, [C

    .line 2828
    .local v0, "buf":[C
    const/4 v1, 0x0

    .line 2829
    .local v1, "bufIndex":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v2, v1

    .end local v1    # "bufIndex":I
    .local v2, "bufIndex":I
    :goto_1
    if-lt v3, v4, :cond_1

    .line 2836
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0

    .line 2831
    :cond_1
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3a

    if-eq v5, v6, :cond_2

    .line 2833
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bufIndex":I
    .restart local v1    # "bufIndex":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, v0, v2

    .line 2829
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "bufIndex":I
    .restart local v2    # "bufIndex":I
    goto :goto_1

    :cond_2
    move v1, v2

    .end local v2    # "bufIndex":I
    .restart local v1    # "bufIndex":I
    goto :goto_2
.end method

.method private resetStartConfig()V
    .locals 2

    .prologue
    .line 1118
    const-string v0, "TMSBtAppholder"

    const-string v1, " resetStartConfig : Enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    const-string v0, "TMSBtAppholder"

    const-string v1, " resetStartConfig : Exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    return-void
.end method

.method private saveOverrideA2dpData(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2077
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "saveOverrideA2dpData mac "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2078
    return-void
.end method

.method private saveOverrideHfpData(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2050
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mOverridenHfpDeviceList:Ljava/util/List;

    .line 2051
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " saveOverrideHfpData "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2052
    return-void
.end method

.method private setFlagBluetoothOnbyTMApp(Z)V
    .locals 3
    .param p1, "flagBluetoothOnbyTMApp"    # Z

    .prologue
    .line 286
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFlagBluetoothOnbyTMApp = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagBluetoothOnbyTMApp:Z

    .line 288
    return-void
.end method

.method public static setFlagClientStartConnection(Z)V
    .locals 3
    .param p0, "flag"    # Z

    .prologue
    .line 256
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFlagClientStartConnection = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    sput-boolean p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagClientStartConnection:Z

    .line 258
    return-void
.end method

.method public static setFlagServerStartConnection(Z)V
    .locals 3
    .param p0, "flag"    # Z

    .prologue
    .line 240
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setFlagServerStartConnection = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sput-boolean p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagServerStartConnection:Z

    .line 242
    return-void
.end method

.method private setStartConfig()V
    .locals 2

    .prologue
    .line 1098
    const-string v0, "TMSBtAppholder"

    const-string v1, " setStartConfig : Enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1113
    const-string v0, "TMSBtAppholder"

    const-string v1, " setStartConfig : Exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    return-void
.end method

.method private startAppWhenAdapterOn()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    .line 1422
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " startAppWhenAdapterOn HFP:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " A2DP:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 1426
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:(mTmClientMac == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1511
    :cond_0
    :goto_0
    return-void

    .line 1430
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v2, :cond_b

    .line 1432
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isOn()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1435
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getFlagStartConnection()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1437
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget-object v3, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 1438
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_3

    .line 1440
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:(device == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1444
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-eqz v2, :cond_6

    .line 1446
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v2

    if-eq v2, v5, :cond_4

    .line 1447
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:create bond :2"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->callBluetoothDeviceCreateBond(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 1451
    :cond_4
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:mHfpManager.connect"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v2, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1453
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:HFP connect failed start discovery"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    :cond_5
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v2, :cond_0

    .line 1462
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->isTmConnected()Z
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-eqz v2, :cond_8

    .line 1464
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v2

    if-eq v2, v5, :cond_7

    .line 1465
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:create bond :1"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->callBluetoothDeviceCreateBond(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 1469
    :cond_7
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn:mA2dpManager.connect"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1470
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v2, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 1477
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 1478
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 1479
    const-string v2, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.startDiscovery()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1486
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getScanMode()I

    move-result v2

    .line 1487
    const/16 v3, 0x17

    .line 1486
    if-eq v2, v3, :cond_0

    .line 1489
    const-string v2, "TMSBtAppholder"

    const-string v3, "startAppWhenAdapterOn : sending TM_EVENT_ENABLE_DISCOVERABLE "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1493
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x5

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1494
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1502
    .end local v1    # "msg":Landroid/os/Message;
    :cond_a
    const-string v2, "TMSBtAppholder"

    const-string v3, "Not On"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1508
    :cond_b
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Apps "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private stopAppBtA2dp()V
    .locals 3

    .prologue
    .line 1567
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    .line 1568
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1570
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 1573
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    if-nez v0, :cond_1

    .line 1574
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyAudioDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1576
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOff()Z

    .line 1578
    :cond_1
    return-void
.end method

.method private stopAppBtHfp()V
    .locals 3

    .prologue
    .line 1714
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z

    .line 1715
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1717
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 1720
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z

    if-nez v0, :cond_1

    .line 1721
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isAnyAudioDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1723
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOff()Z

    .line 1725
    :cond_1
    return-void
.end method

.method private turnOff()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1374
    const-string v3, "TMSBtAppholder"

    const-string v4, "turnOff"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1375
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v3, :cond_0

    .line 1377
    const-string v2, "TMSBtAppholder"

    const-string v3, "(mBluetoothAdapter == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    :goto_0
    return v1

    .line 1381
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    .line 1382
    .local v0, "ret":Z
    if-nez v0, :cond_1

    .line 1384
    const-string v1, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.isEnabled == true"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 1385
    goto :goto_0

    .line 1388
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->geFlagBluetoothOnbyTMApp()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1390
    const-string v2, "TMSBtAppholder"

    const-string v3, "geFlagBluetoothOnbyTMApp.== false"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1394
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    .line 1395
    if-nez v0, :cond_3

    .line 1397
    const-string v2, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.disable == false"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1401
    :cond_3
    const/16 v1, 0xa

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getAcsState(I)I

    move-result v1

    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V

    .line 1402
    const-string v1, "TMSBtAppholder"

    const-string v3, "turnOff"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 1403
    goto :goto_0
.end method

.method private turnOn()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1343
    const-string v3, "TMSBtAppholder"

    const-string v4, "turnOn"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v3, :cond_0

    .line 1346
    const-string v2, "TMSBtAppholder"

    const-string v3, "(mBluetoothAdapter == null)"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    :goto_0
    return v1

    .line 1349
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    .line 1350
    .local v0, "ret":Z
    if-eqz v0, :cond_1

    .line 1352
    const-string v1, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.isEnabled == true"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 1353
    goto :goto_0

    .line 1356
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    .line 1357
    if-nez v0, :cond_2

    .line 1359
    const-string v2, "TMSBtAppholder"

    const-string v3, "mBluetoothAdapter.enable == false"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1363
    :cond_2
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setFlagBluetoothOnbyTMApp(Z)V

    .line 1364
    const-string v1, "TMSBtAppholder"

    const-string v3, "turnOn"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 1365
    goto :goto_0
.end method


# virtual methods
.method public SetTMServerMac(Ljava/lang/String;)V
    .locals 5
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1174
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " SetTMServerMac : Enter "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    if-eqz p1, :cond_1

    .line 1177
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    .line 1178
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagMacReadBtoff:Z

    if-eqz v1, :cond_0

    .line 1180
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->turnOff()Z

    .line 1182
    :cond_0
    iput-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagMacReadBtoff:Z

    .line 1185
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagSelfMacFound:Z

    if-nez v1, :cond_1

    .line 1187
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mExtListner:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 1189
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mExtListner:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1190
    .local v0, "msg":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->what:I

    .line 1191
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mExtListner:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1195
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    const-string v1, "TMSBtAppholder"

    const-string v2, " SetTMServerMac: Exit "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    return-void
.end method

.method public deinit()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1777
    const-string v0, "TMSBtAppholder"

    const-string v1, "BtAppHolder deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->deInitAdapter()V

    .line 1780
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mExtListner:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1781
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1782
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1783
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 1785
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    .line 1786
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 1787
    const/4 v0, 0x0

    return v0
.end method

.method public getAppList()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1997
    const-string v0, "TMSBtAppholder"

    const-string v1, "BtAppHolder.getAppList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1998
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->fillAppStatus()V

    .line 1999
    const-string v0, "TMSBtAppholder"

    const-string v1, "BtAppHolder.getAppList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1968
    if-nez p1, :cond_0

    .line 1969
    const-string v1, "TMSBtAppholder"

    const-string v2, "getAppStatus(packageName == null)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978
    :goto_0
    return-object v0

    .line 1972
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->fillAppStatus()V

    .line 1973
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1974
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "getAppStatus: packageName found returning status : "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1977
    :cond_1
    const-string v1, "TMSBtAppholder"

    const-string v2, "getAppStatus: packageName not found returning null "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFlagClientStartConnection()Z
    .locals 3

    .prologue
    .line 246
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFlagClientStartConnection = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagClientStartConnection:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    sget-boolean v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagClientStartConnection:Z

    return v0
.end method

.method public getFlagServerStartConnection()Z
    .locals 3

    .prologue
    .line 230
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFlagServerStartConnection = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagServerStartConnection:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    sget-boolean v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagServerStartConnection:Z

    return v0
.end method

.method public getFlagStartConnection()Z
    .locals 3

    .prologue
    .line 262
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFlagStartConnection = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagClientStartConnection:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    sget-boolean v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagClientStartConnection:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getServerMac()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1204
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " getServerMac : mTmServerMac "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmServerMac:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->removeMacOctetSeperator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1206
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " getServerMac : noColon "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    return-object v0
.end method

.method public getTMClientMac()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1164
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getTMClientMac : Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    sget-object v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;

    return-object v0
.end method

.method public init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
    .locals 9
    .param p1, "appmngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "mCntxt"    # Landroid/content/Context;

    .prologue
    const/16 v8, 0x80

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1840
    const-string v1, "TMSBtAppholder"

    const-string v2, "BtAppHolder.init enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mSelfMacAddressStorage:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    if-nez v1, :cond_1

    .line 1842
    :cond_0
    const-string v1, "TMSBtAppholder"

    const-string v2, "mSelfMacAddressStorage is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1944
    :goto_0
    return v6

    .line 1846
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->readPrefences()V

    .line 1847
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->readAppNames()V

    .line 1849
    iput-boolean v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagMacReadBtoff:Z

    .line 1850
    iput-boolean v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mFlagSelfMacFound:Z

    .line 1851
    invoke-direct {p0, v6}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setFlagBluetoothOnbyTMApp(Z)V

    .line 1853
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->initAdapter()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1855
    const-string v1, "TMSBtAppholder"

    const-string v2, "init Failed"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1860
    :cond_2
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1861
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1863
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 1864
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1865
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDescriptionA2dp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 1867
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 1868
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1869
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDescriptionHfp:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 1871
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1872
    const-string v2, "BTA2DP"

    .line 1873
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->TM_BLUETOOTH_FORMAT:Ljava/lang/String;

    .line 1874
    const-string v4, "out"

    .line 1871
    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v2, "application"

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 1877
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 1878
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 1879
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 1880
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 1881
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 1882
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 1883
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 1886
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1887
    const-string v2, "BTHFP"

    .line 1888
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->TM_BLUETOOTH_FORMAT:Ljava/lang/String;

    .line 1889
    const-string v4, "bi"

    .line 1886
    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1891
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v2, "phone"

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 1892
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 1893
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 1894
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 1895
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 1896
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 1897
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 1898
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 1899
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    if-nez v1, :cond_3

    .line 1900
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 1903
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->fillAppStatus()V

    .line 1907
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1908
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1909
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v1

    long-to-int v0, v1

    .line 1910
    .local v0, "rowNum":I
    if-eq v0, v5, :cond_7

    .line 1911
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1912
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1923
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1924
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v1, v7}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1925
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mDdbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v1

    long-to-int v0, v1

    .line 1926
    if-ne v0, v5, :cond_5

    .line 1927
    const-string v1, "TMSBtAppholder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error inserting into db package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1930
    :cond_5
    if-eq v0, v5, :cond_8

    .line 1931
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1932
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1941
    :cond_6
    :goto_2
    const-string v1, "BtAppHolder"

    invoke-virtual {p1, v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 1943
    const-string v1, "TMSBtAppholder"

    const-string v2, "BtAppHolder.init exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1915
    :cond_7
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    .line 1916
    if-eqz v0, :cond_4

    .line 1917
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1918
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoA2dp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1934
    :cond_8
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    .line 1935
    if-eqz v0, :cond_6

    .line 1936
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1937
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppInfoHfp:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
    .locals 2
    .param p1, "appStatusListener"    # Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .prologue
    .line 1955
    const-string v0, "TMSBtAppholder"

    const-string v1, "regAppStatusEventListener Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .line 1957
    const/4 v0, 0x1

    return v0
.end method

.method public setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 0
    .param p1, "ca"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 1728
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 1729
    return-void
.end method

.method public setListner(Landroid/os/Handler;)V
    .locals 0
    .param p1, "h"    # Landroid/os/Handler;

    .prologue
    .line 1988
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mExtListner:Landroid/os/Handler;

    .line 1989
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2011
    const-string v1, "TMSBtAppholder"

    const-string v2, "Enter startApp "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    const/4 v0, 0x0

    .line 2013
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2015
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->launchAppBtA2dp()Z

    move-result v0

    .line 2017
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2019
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->launchAppBtHfp()Z

    move-result v0

    .line 2021
    :cond_1
    return v0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2032
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameA2dp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2034
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->stopAppBtA2dp()V

    .line 2036
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppNameHfp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2038
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->stopAppBtHfp()V

    .line 2040
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
