.class public Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;
.super Landroid/os/Handler;
.source "PhoneAppEventsListner.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;


# static fields
.field public static final CALL_EVENT:I = 0x1

.field private static final INCOMINGCALL_NOTI_BODY:Ljava/lang/String; = "From: "

.field private static final INCOMINGCALL_NOTI_TITLE:Ljava/lang/String; = "Incoming call"

.field private static final TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field private mCallFilter:Landroid/content/IntentFilter;

.field private mCallStateReceiver:Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;

.field private mCntxt:Landroid/content/Context;

.field private mIsMonitoring:Z

.field public mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

.field public mNotifications:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneAppActions:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;

.field private mRelatedAppId:I

.field private mRelatedAppName:Ljava/lang/String;

.field private mSupportedClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "cntxt"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appId"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppName:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppId:I

    .line 42
    const-string v0, "TMSAppMngr"

    const-string v1, "PhoneAppEventsListner.PhoneAppEventsListner enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCntxt:Landroid/content/Context;

    .line 44
    iput p3, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppId:I

    .line 45
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppName:Ljava/lang/String;

    .line 46
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallFilter:Landroid/content/IntentFilter;

    .line 47
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;-><init>(Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallStateReceiver:Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotifications:Ljava/util/Map;

    .line 50
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mPhoneAppActions:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;

    .line 52
    return-void
.end method


# virtual methods
.method public getRelatedAppId()I
    .locals 3

    .prologue
    .line 136
    const-string v0, "TMSAppMngr"

    .line 137
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PhoneAppEventsListner.getRelatedAppId enter mRelatedAppId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    iget v2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 137
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppId:I

    return v0
.end method

.method public getRelatedAppName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    const-string v0, "TMSAppMngr"

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PhoneAppEventsListner.getRelatedAppName enter mRelatedAppName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 129
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedClients()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    const-string v0, "TMSAppMngr"

    .line 109
    const-string v1, "PhoneAppEventsListner.getSupportedClients.setClient enter"

    .line 108
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 145
    const-string v3, "TMSAppMngr"

    const-string v4, "PhoneAppEventsListner.handleMessage enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 149
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 150
    .local v0, "bndl":Landroid/os/Bundle;
    const-string v3, "callid"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "callid":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    .line 153
    const-string v3, "com.android.phone"

    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->getRelatedAppId()I

    move-result v4

    .line 152
    invoke-direct {v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;-><init>(Ljava/lang/String;I)V

    .line 155
    .local v2, "noti":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    invoke-virtual {v2, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->setActionFather(Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;)V

    .line 156
    const-string v3, "AcceptCall"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->addAction(Ljava/lang/String;Z)V

    .line 157
    const-string v3, "RejectCall"

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->addAction(Ljava/lang/String;Z)V

    .line 158
    const-string v3, "SilenceCall"

    invoke-virtual {v2, v3, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->addAction(Ljava/lang/String;Z)V

    .line 159
    const-string v3, "Incoming call"

    iput-object v3, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiTitle:Ljava/lang/String;

    .line 160
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "From: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiBody:Ljava/lang/String;

    .line 163
    sget-object v3, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotifications:Ljava/util/Map;

    iget-object v4, v2, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

    if-eqz v3, :cond_0

    .line 166
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

    invoke-interface {v3, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;->OnAppNotiReceived(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;)V

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public invokeNoti(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    .prologue
    .line 119
    const-string v1, "TMSAppMngr"

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PhoneAppEventsListner.invokeNoti enter action.mActionName= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 121
    iget-object v3, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 120
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x0

    .line 124
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mPhoneAppActions:Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;

    iget-object v2, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppActions;->doAction(Ljava/lang/String;)Z

    move-result v0

    .line 125
    return v0
.end method

.method public motinor(Z)Z
    .locals 3
    .param p1, "startMonitoring"    # Z

    .prologue
    .line 55
    const-string v0, "TMSAppMngr"

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PhoneAppEventsListner.motinor enter startMonitoring =  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 57
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mIsMonitoring = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 56
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCntxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallStateReceiver:Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    .line 65
    :cond_0
    :goto_0
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PhoneAppEventsListner.motinor exit mIsMonitoring = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    return v0

    .line 61
    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCntxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mCallStateReceiver:Lcom/samsung/android/mirrorlink/appmanager/CallStateReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mIsMonitoring:Z

    goto :goto_0
.end method

.method public registerNotiHandler(Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;)V
    .locals 2
    .param p1, "notificationListener"    # Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

    .prologue
    .line 114
    const-string v0, "TMSAppMngr"

    const-string v1, "PhoneAppEventsListner.registerNotiHandler enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

    .line 116
    return-void
.end method

.method public setClient(ILjava/util/ArrayList;)Z
    .locals 4
    .param p1, "clientID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 80
    .local p2, "apps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "TMSAppMngr"

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PhoneAppEventsListner.setClient.setClient enter clientID=  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 81
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x0

    .line 84
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    .line 89
    :goto_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mRelatedAppId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 90
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_0
    const/4 v0, 0x1

    .line 102
    :cond_1
    :goto_1
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PhoneAppEventsListner.setClient.setClient exit ret=  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return v0

    .line 87
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 95
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 96
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 98
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 99
    const/4 v0, 0x0

    goto :goto_1
.end method
