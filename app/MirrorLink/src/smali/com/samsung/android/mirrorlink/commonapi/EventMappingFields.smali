.class public Lcom/samsung/android/mirrorlink/commonapi/EventMappingFields;
.super Ljava/lang/Object;
.source "EventMappingFields.java"


# instance fields
.field localEvent:J

.field remoteEvent:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method


# virtual methods
.method public getLocalEvent()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingFields;->localEvent:J

    return-wide v0
.end method

.method public getRemoteEvent()J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingFields;->remoteEvent:J

    return-wide v0
.end method

.method public setLocalEvent(J)V
    .locals 0
    .param p1, "localEvent"    # J

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingFields;->localEvent:J

    .line 41
    return-void
.end method

.method public setRemoteEvent(J)V
    .locals 0
    .param p1, "remoteEvent"    # J

    .prologue
    .line 26
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingFields;->remoteEvent:J

    .line 27
    return-void
.end method
