.class Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;
.super Ljava/lang/Object;
.source "TMNotificationServerService.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;)V
    .locals 2
    .param p1, "notiSrvSvc"    # Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;

    .prologue
    .line 500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 498
    const-string v0, "TMSUPnP"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;->TAG:Ljava/lang/String;

    .line 501
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;->TAG:Ljava/lang/String;

    const-string v1, "TMNotificationServerService.NotificationListener enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    return-void
.end method


# virtual methods
.method public OnAppNotiReceived(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;)V
    .locals 3
    .param p1, "notification"    # Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;->TAG:Ljava/lang/String;

    .line 506
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMNotificationServerService.OnAppNotiReceived notiId is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 507
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 506
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 505
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;

    iget-object v1, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mNotiID:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;->OnNotiReceived(Ljava/lang/String;)V

    .line 510
    return-void
.end method
