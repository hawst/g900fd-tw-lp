.class public Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;
.super Lcom/mirrorlink/android/commonapi/IDataServicesManager$Stub;
.source "DataServicesManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSDataServicesManager"


# instance fields
.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

.field private final mListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IDataServicesListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonapi"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IDataServicesManager$Stub;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 23
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 27
    const-string v0, "TMSDataServicesManager"

    const-string v1, "Enter Constructor DataServicesManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager$1;-><init>(Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    .line 36
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 37
    const-string v0, "TMSDataServicesManager"

    const-string v1, "Exit Constructor DataServicesManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public getAvailableServices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.getAvailableServices() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "TMSDataServicesManager"

    const-string v1, "getAvailableServices() : mDataConnMngr is not null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getClientServiceList()Ljava/util/List;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 77
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "getAvailableServices() : mDataConnMngr is null return default "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getObject(II)V
    .locals 2
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 153
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.getObject() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "TMSDataServicesManager"

    .line 157
    const-string v1, "getObject() : mDataConnMngr is not null. so getting Object"

    .line 156
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getObject(II)V

    .line 161
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.getObject() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 2
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    .line 302
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_1

    .line 303
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager tmsengine is created"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_0

    .line 305
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager tmsengine is destroyed"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 49
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.killListener() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 51
    return-void
.end method

.method public notifyAvailableServicesChanged(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "services":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v3, "TMSDataServicesManager"

    .line 185
    const-string v4, "DataServicesManager.notifyAvailableServicesChanged()"

    .line 184
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 188
    .local v2, "length":I
    const-string v3, "TMSDataServicesManager"

    .line 189
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.notifyAvailableServicesChanged():length "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 189
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 188
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 202
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 203
    return-void

    .line 194
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDataServicesListener;->onAvailableServicesChanged(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 198
    const-string v3, "TMSDataServicesManager"

    .line 199
    const-string v4, "RemoteException in notifyAvailableServicesChanged"

    .line 198
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V
    .locals 6
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z
    .param p4, "object"    # Landroid/os/Bundle;

    .prologue
    .line 278
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 279
    .local v2, "length":I
    const-string v3, "TMSDataServicesManager"

    .line 280
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.notifyGetDataObjectResponse():length "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 280
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 279
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 293
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 294
    const-string v3, "TMSDataServicesManager"

    const-string v4, "DataServicesManager.notifyGetDataObjectResponse()"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    return-void

    .line 285
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-interface {v3, p1, p2, p3, p4}, Lcom/mirrorlink/android/commonapi/IDataServicesListener;->onGetDataObjectResponse(IIZLandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 289
    const-string v3, "TMSDataServicesManager"

    .line 290
    const-string v4, "RemoteException in notifyGetDataObjectResponse"

    .line 289
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifyRegisterForService(IZ)V
    .locals 6
    .param p1, "serviceId"    # I
    .param p2, "success"    # Z

    .prologue
    .line 207
    const-string v3, "TMSDataServicesManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.notifyRegisterForService() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 209
    .local v2, "length":I
    const-string v3, "TMSDataServicesManager"

    .line 210
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.notifyRegisterForService():length "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 210
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 209
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 223
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 225
    return-void

    .line 215
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-interface {v3, p1, p2}, Lcom/mirrorlink/android/commonapi/IDataServicesListener;->onRegisterForService(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 219
    const-string v3, "TMSDataServicesManager"

    .line 220
    const-string v4, "RemoteException in notifyRegisterForService"

    .line 219
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifySetDataObjectResponse(IIZ)V
    .locals 6
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z

    .prologue
    .line 255
    const-string v3, "TMSDataServicesManager"

    const-string v4, "DataServicesManager.notifySetDataObjectResponse()"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 258
    .local v2, "length":I
    const-string v3, "TMSDataServicesManager"

    .line 259
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.notifySetDataObjectResponse():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 260
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 259
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 258
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 272
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 273
    return-void

    .line 264
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-interface {v3, p1, p2, p3}, Lcom/mirrorlink/android/commonapi/IDataServicesListener;->onSetDataObjectResponse(IIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 268
    const-string v3, "TMSDataServicesManager"

    .line 269
    const-string v4, "RemoteException in notifySetDataObjectResponse"

    .line 268
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public notifySubscribeResponse(IIZBI)V
    .locals 9
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "success"    # Z
    .param p4, "subscriptionType"    # B
    .param p5, "interval"    # I

    .prologue
    .line 230
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.notifySubscribeResponse()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v8

    .line 233
    .local v8, "length":I
    const-string v0, "TMSDataServicesManager"

    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataServicesManager.notifySubscribeResponse():length"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 234
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v8, :cond_0

    .line 248
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 250
    return-void

    .line 239
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v7}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mirrorlink/android/commonapi/IDataServicesListener;->onSubscribeResponse(IIZII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 242
    :catch_0
    move-exception v6

    .line 243
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    .line 244
    const-string v0, "TMSDataServicesManager"

    .line 245
    const-string v1, "RemoteException in notifySubscribeResponse"

    .line 244
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDataServicesListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    .prologue
    .line 41
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 45
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public registerToService(III)V
    .locals 2
    .param p1, "serviceId"    # I
    .param p2, "majorVer"    # I
    .param p3, "minorVer"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.registerToService() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "TMSDataServicesManager"

    .line 88
    const-string v1, "registerToService() : mDataConnMngr is not null. so Registering"

    .line 87
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->register(III)V

    .line 91
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.registerToService() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public setDataConnMngr(Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;)V
    .locals 2
    .param p1, "dataConnMngr"    # Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .prologue
    .line 55
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.setDataConnMngr() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->cleanUp()V

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    .line 60
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->setDataServicesManager(Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;)V

    .line 63
    :cond_1
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.setDataConnMngr() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public setObject(IILandroid/os/Bundle;)V
    .locals 2
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .param p3, "object"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 138
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.setObject() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "TMSDataServicesManager"

    .line 142
    const-string v1, "setObject() : mDataConnMngr is not null. so setting Object"

    .line 141
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->setObject(IILandroid/os/Bundle;)V

    .line 147
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.setObject() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public subscribeObject(II)V
    .locals 3
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 110
    const-string v0, "TMSDataServicesManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataServicesManager.subscribeObject() : Enter with servId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "objid ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 113
    const-string v0, "TMSDataServicesManager"

    .line 114
    const-string v1, "subscribeObject() : mDataConnMngr is not null. so Subscribing"

    .line 113
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->subscribeObject(II)V

    .line 118
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.subscribeObject() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 167
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSDataServicesManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataServicesManager.unregister():packname"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    if-nez v2, :cond_0

    .line 178
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 172
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 177
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 173
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 174
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 172
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public unregisterFromService(I)V
    .locals 3
    .param p1, "serviceId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 96
    const-string v0, "TMSDataServicesManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DataServicesManager.unregisterFromService() : Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "TMSDataServicesManager"

    .line 100
    const-string v1, "unregisterFromService() : mDataConnMngr is not null. so UnRegistering"

    .line 99
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->unRegister(I)V

    .line 104
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.unregisterFromService() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public unsubscribeObject(II)V
    .locals 2
    .param p1, "serviceId"    # I
    .param p2, "objectId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 124
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.unsubscribeObject() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "TMSDataServicesManager"

    .line 128
    const-string v1, "unsubscribeObject() : mDataConnMngr is not null. so UnSubscribing"

    .line 127
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->unSubscribeObject(II)V

    .line 132
    :cond_0
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.unsubscribeObject() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    return-void
.end method
