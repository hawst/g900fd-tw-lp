.class public final enum Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;
.super Ljava/lang/Enum;
.source "ICommonApiCallBack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_AUDIO_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_CLIENT_PIXEL_FORMAT_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_DISPLAY_CONFIG_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_DRIVEMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_FB_BLOCKING:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_MIC_OPEN:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_NIGHTMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

.field public static final enum UPDATE_REMOTE_DISPLAY_CONNECTION:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_DEVICE_INFO"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 7
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_REMOTE_DISPLAY_CONNECTION"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_REMOTE_DISPLAY_CONNECTION:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 8
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_MLSESSION_CHANGE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 9
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_DISPLAY_CONFIG_CHANGED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DISPLAY_CONFIG_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 10
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_CLIENT_PIXEL_FORMAT_CHANGED"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CLIENT_PIXEL_FORMAT_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 11
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_AUDIO_CHANGE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_AUDIO_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 12
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_DRIVEMODE_CHANGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DRIVEMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 13
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_MIC_OPEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MIC_OPEN:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 14
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_NIGHTMODE_CHANGE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_NIGHTMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 15
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_FB_BLOCKING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_FB_BLOCKING:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 16
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    const-string v1, "UPDATE_CERTIFICATION_STATUS_CHANGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    .line 5
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_REMOTE_DISPLAY_CONNECTION:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DISPLAY_CONFIG_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CLIENT_PIXEL_FORMAT_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_AUDIO_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DRIVEMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MIC_OPEN:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_NIGHTMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_FB_BLOCKING:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
