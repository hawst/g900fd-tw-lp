.class public final Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface$OCSPResponsePeriod;
.super Ljava/lang/Object;
.source "OcspResponseEntryInterface.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OCSPResponsePeriod"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final BASE_GRACE_PERIOD:Ljava/lang/String; = "baseGracePeriod"

.field public static final DRIVE_GRACE_PERIOD:Ljava/lang/String; = "driveGracePeriod"

.field public static final QUERY_PERIOD:Ljava/lang/String; = "queryPeriod"

.field public static final TABLE_NAME:Ljava/lang/String; = "ocspPeriod"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 150
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "queryPeriod"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 151
    const-string v2, "driveGracePeriod"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 152
    const-string v2, "baseGracePeriod"

    aput-object v2, v0, v1

    .line 150
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface$OCSPResponsePeriod;->ALL_COLUMNS:[Ljava/lang/String;

    .line 156
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
