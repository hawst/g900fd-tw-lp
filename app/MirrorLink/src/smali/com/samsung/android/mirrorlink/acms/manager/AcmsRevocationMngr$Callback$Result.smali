.class public final enum Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;
.super Ljava/lang/Enum;
.source "AcmsRevocationMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

.field public static final enum ERROR_NO_DATA_CONNECTIVITY:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

.field public static final enum FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

.field public static final enum NO_VALIDAPPS_TO_PERFORM_REVOCATION:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

.field public static final enum PASS:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    const-string v1, "ERROR_NO_DATA_CONNECTIVITY"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ERROR_NO_DATA_CONNECTIVITY:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    const-string v1, "PASS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->PASS:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    const-string v1, "NO_VALIDAPPS_TO_PERFORM_REVOCATION"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->NO_VALIDAPPS_TO_PERFORM_REVOCATION:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    .line 94
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ERROR_NO_DATA_CONNECTIVITY:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->PASS:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->FAIL:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->NO_VALIDAPPS_TO_PERFORM_REVOCATION:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
