.class public Lcom/samsung/android/mirrorlink/service/TMDisplayService;
.super Landroid/app/Service;
.source "TMDisplayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;,
        Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;
    }
.end annotation


# static fields
.field private static final EXTRA_ML_STATE:Ljava/lang/String; = "mlstatus"

.field private static final INTENT_ML_STATE:Ljava/lang/String; = "com.samsung.android.mirrorlink.ML_STATE"

.field private static final MIRRORLINK_TM_SERVICE_INTENT:Ljava/lang/String; = "com.samsung.android.mirrorlink.service.TMS_SERVICE_ACTION"

.field static final MSG_SELF_DISMISS_BLACK_SCREEN:I = 0xb

.field static final MSG_SELF_DISMISS_BLACK_SCREEN_WHEN_RINGING:I = 0xd

.field static final MSG_SELF_SHOW_BLACK_SCREEN:I = 0xa

.field static final MSG_SELF_SHOW_BLACK_SCREEN_DELAY:I = 0xe

.field static final MSG_SELF_SHOW_BLACK_SCREEN_NOIMAGE:I = 0xf

.field static final MSG_SELF_UNBIND_NOTIFY:I = 0xc


# instance fields
.field private final GUARD_DELAY:J

.field private final ML_STARTED:I

.field private final ML_STOPPED:I

.field private final TAG:Ljava/lang/String;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

.field private mHDFiled:I

.field private mIsBound:Z

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private mMWFiled:I

.field final mMessenger:Landroid/os/Messenger;

.field private mPhoneState:Ljava/lang/String;

.field private mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

.field mService:Landroid/os/Messenger;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 39
    const-string v0, "TMSDisplayService"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->TAG:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    .line 44
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->ML_STARTED:I

    .line 51
    iput v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->ML_STOPPED:I

    .line 54
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->GUARD_DELAY:J

    .line 57
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    .line 63
    sget-object v0, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;

    .line 66
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 68
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 91
    iput v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mHDFiled:I

    .line 94
    iput v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMWFiled:I

    .line 189
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;-><init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    .line 194
    new-instance v0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;-><init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mConnection:Landroid/content/ServiceConnection;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->updateNCM()V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPhoneState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->disableNotification()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 554
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsWakeLockAcquire()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 532
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsDisableKeyguard()V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->createPresentation()V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->notifyMirroLinkDisconnect()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->showInternalBlackScreenWithRunningImage(Z)V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->dismissInternalBlackScreen()V

    return-void
.end method

.method private createPresentation()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 401
    const-string v8, "TMSDisplayService"

    const-string v9, "createPresentation - Enter"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 403
    .local v0, "cxt":Landroid/content/Context;
    const-string v8, "display"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/DisplayManager;

    .line 404
    .local v2, "displayManager":Landroid/hardware/display/DisplayManager;
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v1

    .line 406
    .local v1, "display":Landroid/view/Display;
    new-instance v8, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9, v1}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    iput-object v8, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    .line 407
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 408
    .local v7, "window":Landroid/view/Window;
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/Window;->requestFeature(I)Z

    .line 409
    const v8, 0x106000c

    invoke-virtual {v7, v8}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 411
    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 415
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    :try_start_0
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "TYPE_DISPLAY_OVERLAY"

    invoke-virtual {v8, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 416
    .local v6, "overlay":Ljava/lang/reflect/Field;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    iput v8, v5, Landroid/view/WindowManager$LayoutParams;->type:I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 424
    .end local v6    # "overlay":Ljava/lang/reflect/Field;
    :goto_0
    iget v8, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v8, v8, 0x488

    iput v8, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 427
    invoke-virtual {v7, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 429
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    if-nez v8, :cond_0

    .line 430
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 431
    .local v4, "intentFilter":Landroid/content/IntentFilter;
    const-string v8, "com.samsung.android.mirrorlink.action.SHOW_BLACK_SCREEN"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 432
    const-string v8, "com.samsung.android.mirrorlink.action.SHOW_BLACK_SCREEN_NOIMAGE"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 433
    const-string v8, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 434
    const-string v8, "android.intent.action.PHONE_STATE"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 435
    const-string v8, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 436
    new-instance v8, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    invoke-direct {v8, p0, v10}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;)V

    iput-object v8, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    .line 437
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    invoke-virtual {p0, v8, v4}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 439
    .end local v4    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void

    .line 417
    :catch_0
    move-exception v3

    .line 418
    .local v3, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 419
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v3

    .line 420
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 421
    .end local v3    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v3

    .line 422
    .local v3, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v3}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0
.end method

.method private disableNotification()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "statusbar"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 245
    .local v3, "service":Ljava/lang/Object;
    const-string v5, "android.app.StatusBarManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 246
    .local v4, "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v5, Landroid/view/View;

    const-string v6, "STATUS_BAR_DISABLE_EXPAND"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 247
    .local v1, "flag":Ljava/lang/reflect/Field;
    const-class v5, Landroid/app/StatusBarManager;

    const-string v6, "DISABLE_NOTIFICATION_TICKER"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 248
    .local v2, "flag2":Ljava/lang/reflect/Field;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ML get to DISABLE_NOTIFICATION_TICKER cur f1: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " f2: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v5, "disable"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    or-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v3, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    const-string v5, "TMSDisplayService"

    const-string v6, "ML set to DISABLE_NOTIFICATION_TICKER as disable!"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    .line 263
    .end local v1    # "flag":Ljava/lang/reflect/Field;
    .end local v2    # "flag2":Ljava/lang/reflect/Field;
    .end local v3    # "service":Ljava/lang/Object;
    .end local v4    # "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "heads_up_notifications_enabled"

    invoke-static {v5, v6, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mHDFiled:I

    .line 264
    iget v5, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mHDFiled:I

    if-ne v5, v11, :cond_0

    .line 265
    const-string v5, "TMSDisplayService"

    const-string v6, "ML need to change heads_up_notifications_enabled flag and should restore!"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "heads_up_notifications_enabled"

    invoke-static {v5, v6, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "multi_window_enabled"

    invoke-static {v5, v6, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMWFiled:I

    .line 270
    iget v5, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMWFiled:I

    if-ne v5, v11, :cond_1

    .line 271
    const-string v5, "TMSDisplayService"

    const-string v6, "ML need to change multi_window_enabled flag and should restore!"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "multi_window_enabled"

    invoke-static {v5, v6, v10}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 274
    :cond_1
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "show : IllegalAccessException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 253
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "show : NoSuchFieldException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 255
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v0

    .line 256
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "show : InvocationTargetException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 257
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v0

    .line 258
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "show : NoSuchMethodException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v5, "TMSDisplayService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "show : ClassNotFoundException "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private dismissInternalBlackScreen()V
    .locals 2

    .prologue
    .line 442
    const-string v0, "TMSDisplayService"

    const-string v1, "dismiss internalblackscreen & isshowing : "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->isCurrentRunningForCts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    const-string v0, "TMSDisplayService"

    const-string v1, "internal black screen doesn\'t show because cts mode is enabled"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->dismiss()V

    goto :goto_0
.end method

.method private isCurrentRunningForCts()Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 477
    const-string v3, ""

    .line 479
    .local v3, "ret":Ljava/lang/String;
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v7, "get"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v4, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 480
    .local v2, "listener":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "scmm.mirrorlink.ctsmode"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "0"

    aput-object v9, v7, v8

    invoke-virtual {v2, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 491
    .end local v2    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v5, :cond_0

    move v4, v5

    :goto_1
    return v4

    .line 481
    :catch_0
    move-exception v1

    .line 482
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "TMSDisplayService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TmsService.showInternalBlackScreen "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 483
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 484
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "TMSDisplayService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TmsService.showInternalBlackScreen "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 485
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v1

    .line 486
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "TMSDisplayService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TmsService.showInternalBlackScreen "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 487
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 488
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "TMSDisplayService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TmsService.showInternalBlackScreen "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_0
    move v4, v6

    .line 491
    goto :goto_1
.end method

.method private notifyMirroLinkDisconnect()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->dismissInternalBlackScreen()V

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->restoreNotification()V

    .line 312
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "set"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 313
    .local v2, "listener":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "net.mirrorlink.on"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "0"

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "net.mirrorlink.clientid"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "NA"

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 325
    .end local v2    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 326
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.samsung.android.app.mirrorlink.sip"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 328
    .local v3, "ml":Landroid/os/Bundle;
    const-string v4, "mlconnected"

    invoke-virtual {v3, v4, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 329
    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 332
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    if-eqz v4, :cond_0

    .line 333
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    invoke-virtual {p0, v4}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 334
    iput-object v10, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    .line 337
    :cond_0
    invoke-virtual {p0, v9}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->setPhoneInCarMode(Z)V

    .line 338
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsReenableKeyguard()V

    .line 339
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsWakeLockRelease()V

    .line 340
    return-void

    .line 315
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "ml":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "TMSDisplayService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TmsService.deinit() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 318
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "TMSDisplayService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TmsService.deinit() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 319
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "TMSDisplayService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TmsService.deinit() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 321
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private restoreNotification()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 277
    iget v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMWFiled:I

    if-ne v3, v5, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "multi_window_enabled"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 279
    const-string v3, "TMSDisplayService"

    const-string v4, "ML changed multi_window_enabled flag so restore!"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_0
    iget v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mHDFiled:I

    if-ne v3, v5, :cond_1

    .line 283
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "heads_up_notifications_enabled"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 284
    const-string v3, "TMSDisplayService"

    const-string v4, "ML changed heads_up_notifications_enabled flag so restore!"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_1
    iput v6, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mHDFiled:I

    .line 288
    iput v6, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMWFiled:I

    .line 291
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "statusbar"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 292
    .local v1, "service":Ljava/lang/Object;
    const-string v3, "android.app.StatusBarManager"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 293
    .local v2, "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "disable"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 303
    .end local v1    # "service":Ljava/lang/Object;
    .end local v2    # "statusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    return-void

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dismiss : IllegalAccessException "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 296
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dismiss : InvocationTargetException "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dismiss : NoSuchMethodException "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v0

    .line 301
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dismiss : ClassNotFoundException "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendMLStatusToOtherApps(Z)V
    .locals 4
    .param p1, "mlstatus"    # Z

    .prologue
    .line 606
    const-string v1, "TMSDisplayService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TmsService:sendMLStatusToOtherApps: enter : mlstatus = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.mirrorlink.ML_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 609
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 610
    if-eqz p1, :cond_0

    .line 611
    const-string v1, "mlstatus"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 615
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 617
    const-string v1, "TMSDisplayService"

    const-string v2, "TmsService:sendMLStatusToOtherApps:exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    return-void

    .line 613
    :cond_0
    const-string v1, "mlstatus"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private showInternalBlackScreenWithRunningImage(Z)V
    .locals 4
    .param p1, "runningImage"    # Z

    .prologue
    .line 456
    const-string v1, "TMSDisplayService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showInternalBlackScreen("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->isCurrentRunningForCts()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 459
    const-string v1, "TMSDisplayService"

    const-string v2, "internal black screen doesn\'t show because cts mode is enabled"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 464
    .local v0, "keyGMngr":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-nez v1, :cond_3

    .line 465
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mPresentation:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->show(Z)V

    goto :goto_0

    .line 469
    :cond_3
    const-string v1, "TMSDisplayService"

    const-string v2, "SecureLocked & keyguardRestriced, showInternalBlackScreen() is ignored"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private tmsDisableKeyguard()V
    .locals 3

    .prologue
    .line 533
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v1, :cond_1

    .line 534
    const-string v1, "TMSDisplayService"

    const-string v2, "mKeyguardLock is already. check!"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 537
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 538
    .local v0, "km":Landroid/app/KeyguardManager;
    const-string v1, "TMSDisplayService"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 539
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v1}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V

    .line 541
    const-string v1, "TMSDisplayService"

    const-string v2, "mKeyguardLock is disableKeyguard!"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private tmsReenableKeyguard()V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 549
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 551
    :cond_0
    return-void
.end method

.method private tmsWakeLockAcquire()V
    .locals 3

    .prologue
    .line 556
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 557
    const-string v1, "TMSDisplayService"

    const-string v2, "mWakeLock is already. check!"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_0
    :goto_0
    return-void

    .line 561
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 562
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x10000006

    const-string v2, "TMSDisplayService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 563
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 564
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 565
    const-string v1, "TMSDisplayService"

    const-string v2, "mWakeLock is acquire!"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private tmsWakeLockRelease()V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 572
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 573
    const-string v0, "TMSDisplayService"

    const-string v1, "mWakeLock is release!"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :cond_0
    return-void
.end method

.method private updateNCM()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 343
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setNcmReady(ZZ)V

    .line 344
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 346
    .local v2, "mCm":Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 349
    .local v3, "mCmClass":Ljava/lang/Class;
    :try_start_0
    const-string v4, "setNcmTethering"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 350
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "TMSDisplayService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not connect to channel."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method doBindService()V
    .locals 4

    .prologue
    .line 360
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 361
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.mirrorlink.service.TMS_SERVICE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z

    .line 365
    const-string v1, "TMSDisplayService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Binding:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 397
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 370
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 371
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 386
    const-string v0, "TMSDisplayService"

    const-string v1, "onDestroy - Enter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 389
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mGuiEventReceiver:Lcom/samsung/android/mirrorlink/service/TMDisplayService$GuiEventReceiver;

    .line 391
    :cond_0
    const-string v0, "TMSDisplayService"

    const-string v1, "onDestroy - Exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 393
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 375
    const-string v1, "TMSDisplayService"

    const-string v2, "Binding to TmsService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const/4 v0, 0x1

    .line 377
    .local v0, "ret":I
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->doBindService()V

    .line 378
    if-eqz p1, :cond_0

    .line 379
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    .line 381
    :cond_0
    return v0
.end method

.method public setPhoneInCarMode(Z)V
    .locals 6
    .param p1, "mode"    # Z

    .prologue
    .line 578
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsService.setPhoneInCarMode - enter mode = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.DOCK_EVENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 581
    .local v2, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 585
    :try_start_0
    const-class v3, Landroid/content/Intent;

    const-string v4, "EXTRA_DOCK_STATE_MIRRORLINK"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 586
    .local v0, "dock":Ljava/lang/reflect/Field;
    const-string v3, "android.intent.extra.DOCK_STATE"

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 597
    .end local v0    # "dock":Ljava/lang/reflect/Field;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 599
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->sendMLStatusToOtherApps(Z)V

    .line 601
    const-string v3, "TMSDisplayService"

    const-string v4, "TmsService.setPhoneInCarMode - exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    return-void

    .line 587
    :catch_0
    move-exception v1

    .line 588
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 589
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 590
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 591
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v1

    .line 592
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 595
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const-string v3, "android.intent.extra.DOCK_STATE"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
