.class public final enum Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;
.super Ljava/lang/Enum;
.source "TMNotificationServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "eNOTI_ACTION_ERROR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

.field public static final enum BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

.field public static final enum DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

.field public static final enum INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

.field public static final enum INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;


# instance fields
.field errorDesc:Ljava/lang/String;

.field launchResult:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    const-string v1, "BAD_APPID"

    const/16 v2, 0x32a

    const-string v3, "Bad AppId"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    const-string v1, "ACTION_FALIED"

    const/16 v2, 0x32d

    const-string v3, "Action Failed"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    const-string v1, "DEVICE_LOCKED"

    .line 34
    const/16 v2, 0x32f

    const-string v3, "Device Locked"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    const-string v1, "INVALID_PROFILEID"

    const/16 v2, 0x33e

    .line 35
    const-string v3, "Invalid Profile ID"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    const-string v1, "INVALID_ARGUMENT"

    const/16 v2, 0x334

    const-string v3, "Invalid Argument"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    aput-object v1, v0, v8

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # I
    .param p4, "errorDesc"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->launchResult:I

    .line 42
    iput-object p4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->errorDesc:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->launchResult:I

    return v0
.end method

.method public getErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->errorDesc:Ljava/lang/String;

    return-object v0
.end method
