.class public Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
.super Ljava/lang/Object;
.source "ClientprofileNode.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSUPnP"


# instance fields
.field public mAudioIpl:I

.field public mAudioMpl:I

.field public mBdAddr:Ljava/lang/String;

.field public mBtStartConnection:Z

.field public mClientID:Ljava/lang/String;

.field private mClientprofileAppIDList:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mFriendlyName:Ljava/lang/String;

.field public mIcon_depth:I

.field public mIcon_height:I

.field public mIcon_mimetype:Ljava/lang/String;

.field public mIcon_width:I

.field public mManufacturer:Ljava/lang/String;

.field public mMirrorLinkVersionMajor:I

.field public mMirrorLinkVersionMinor:I

.field public mModelName:Ljava/lang/String;

.field public mModelNumber:Ljava/lang/String;

.field public mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

.field public mProfileID:I

.field public mRtpPayloads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mRules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "TMSUPnP"

    const-string v1, "ClientprofileNode. ClientprofileNode enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    .line 60
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    .line 61
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    .line 62
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_mimetype:Ljava/lang/String;

    .line 63
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    .line 64
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    .line 65
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    .line 66
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBdAddr:Ljava/lang/String;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mBtStartConnection:Z

    .line 68
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioIpl:I

    .line 69
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mAudioMpl:I

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRtpPayloads:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mRules:Ljava/util/Map;

    .line 72
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    .line 73
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    .line 74
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    .line 75
    iput v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    .line 76
    const-string v0, "TMSUPnP"

    const-string v1, "ClientprofileNode. ClientprofileNode exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method


# virtual methods
.method public declared-synchronized clearAppIDList()I
    .locals 2

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSUPnP"

    const-string v1, "ClientprofileNode. clearAppIDList "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized createAppIDList()I
    .locals 2

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSUPnP"

    const-string v1, "ClientprofileNode. createAppIDList"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAppIDS()Ljava/util/LinkedHashSet;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 102
    .local v1, "ret":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    if-nez v2, :cond_1

    .line 104
    const-string v2, "TMSUPnP"

    const-string v3, "ClientprofileNode. getAppIDS null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :cond_0
    monitor-exit p0

    return-object v1

    .line 108
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 110
    .local v0, "appid":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 100
    .end local v0    # "appid":I
    .end local v1    # "ret":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setAppID(I)I
    .locals 3
    .param p1, "appID"    # I

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ClientprofileNode. setAppID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientprofileAppIDList:Ljava/util/LinkedHashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
