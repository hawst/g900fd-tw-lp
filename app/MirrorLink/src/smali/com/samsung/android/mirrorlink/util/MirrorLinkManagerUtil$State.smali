.class public Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil$State;
.super Ljava/lang/Object;
.source "MirrorLinkManagerUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/util/MirrorLinkManagerUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation


# static fields
.field public static final STATE_APPCERT_FETCH_COMPLETED:I = 0x3

.field public static final STATE_APPCERT_FETCH_INPROGRESS:I = 0x2

.field public static final STATE_APPCERT_FETCH_PENDING:I = 0x1

.field public static final STATE_APPCERT_FETCH_RETRY:I = 0x4

.field public static final STATE_APPCERT_NON_CERTIFIED:I = 0x5

.field public static final STATE_INSTALLED:I = 0x0

.field public static final STATE_OCSPCERT_IN_BASE_GRACE_PERIOD:I = 0xa

.field public static final STATE_OCSPCERT_IN_DRIVE_GRACE_PERIOD:I = 0xb

.field public static final STATE_OCSPCERT_IN_QUERY_PERIOD:I = 0x9

.field public static final STATE_OCSPCERT_MLAWARE_UNCHECKED:I = 0xc

.field public static final STATE_OCSPCERT_QUERY_COMPLETED:I = 0x8

.field public static final STATE_OCSPCERT_QUERY_INPROGRESS:I = 0x7

.field public static final STATE_OCSPCERT_QUERY_PENDING:I = 0x6

.field public static final STATE_OCSPCERT_REVOKED:I = 0xd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
