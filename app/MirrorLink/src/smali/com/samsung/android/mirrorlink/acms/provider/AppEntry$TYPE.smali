.class public final enum Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;
.super Ljava/lang/Enum;
.source "AppEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

.field public static final enum DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

.field public static final enum DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

.field public static final enum DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    const-string v1, "BASE_CERTIFIED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    const-string v1, "DRIVE_CERTIFIED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    const-string v1, "DEV_BASE_CERTIFIED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    const-string v1, "DEV_DRIVE_CERTIFIED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .line 186
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
