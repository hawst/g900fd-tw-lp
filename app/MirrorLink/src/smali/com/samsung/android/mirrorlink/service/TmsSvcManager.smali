.class public Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
.super Ljava/lang/Object;
.source "TmsSvcManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSSvc"

.field public static final TM_DEINIT:I = 0x2

.field public static final TM_INIT:I = 0x1


# instance fields
.field private mAppStateHandler:Landroid/os/Handler;

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

.field private mCxt:Landroid/content/Context;

.field private mLooperHandler:Landroid/os/Handler;

.field private mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "TMSSvc"

    const-string v1, "System Loading tmserver Library"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v0, "tmserver"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 53
    const-string v0, "TMSSvc"

    const-string v1, "System Loading tmsjni Library"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "tmsjni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.TmsSvcManager() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCxt:Landroid/content/Context;

    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCxt:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCxt:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 71
    new-instance v1, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;-><init>(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->startThread()V

    .line 73
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 75
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.TmsSvcManager() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    return-object v0
.end method

.method private startThread()V
    .locals 2

    .prologue
    .line 79
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.startThread() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->start()V

    .line 83
    :cond_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.startThread() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method


# virtual methods
.method public clean()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 88
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.clean() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.clean() Before clearing the thread making engine to null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    if-eqz v0, :cond_1

    .line 96
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.stopCommonService Called"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->stopCommonService()V

    .line 98
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->terminate()V

    .line 102
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    .line 103
    const-string v0, "TMSSvc"

    const-string v1, "TmsSvcManager.clean() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public deinit()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.deinit() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 147
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.deinit():mLooperHandler is null - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :goto_0
    return v3

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 152
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 153
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 155
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.deinit() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public init()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 111
    const-string v3, "TMSSvc"

    const-string v4, "TmsSvcManager.init() - Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const/4 v1, 0x0

    .line 115
    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mThread:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x5

    if-lt v1, v3, :cond_2

    .line 124
    :cond_1
    const-string v3, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsSvcManager.init() - i:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    if-nez v3, :cond_3

    .line 127
    const-string v3, "TMSSvc"

    const-string v4, "TmsSvcManager.init() -(mLooperHandler == null)"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :goto_1
    return v6

    .line 118
    :cond_2
    const-wide/16 v3, 0xa

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "TMSSvc"

    const-string v4, "TmsSvcManager.sleep exception"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 131
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 132
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x1

    iput v3, v2, Landroid/os/Message;->what:I

    .line 133
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mLooperHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 135
    const-string v3, "TMSSvc"

    const-string v4, "TmsSvcManager.init() - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setAppStateHAndler(Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;

    .line 161
    return-void
.end method
