.class public Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;
.super Lcom/mirrorlink/android/commonapi/IDisplayManager$Stub;
.source "DisplayManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSDisplayManager"


# instance fields
.field private final mClientListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IDisplayListener;",
            ">;"
        }
    .end annotation
.end field

.field private mClientPixelFormat:Landroid/os/Bundle;

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IDisplayManager$Stub;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 31
    const-string v0, "TMSDisplayManager"

    const-string v1, "Enter Constructor DisplayManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 33
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 34
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setDisplayManager(Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;)V

    .line 36
    :cond_0
    const-string v0, "TMSDisplayManager"

    const-string v1, "Exit Constructor DisplayManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method private isClientPixelFormatChanged(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "pixelFormat"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 239
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 260
    :cond_0
    :goto_0
    return v0

    .line 242
    :cond_1
    const-string v1, "BITS_PER_PIXEL"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 243
    const-string v3, "BITS_PER_PIXEL"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 242
    if-ne v1, v2, :cond_0

    .line 245
    const-string v1, "BLUE_MAX"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 246
    const-string v3, "BLUE_MAX"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 245
    if-ne v1, v2, :cond_0

    .line 248
    const-string v1, "DEPTH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 249
    const-string v3, "BLUE_MAX"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 248
    if-ne v1, v2, :cond_0

    .line 251
    const-string v1, "GREEN_MAX"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 252
    const-string v3, "GREEN_MAX"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 251
    if-ne v1, v2, :cond_0

    .line 254
    const-string v1, "RED_MAX"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 255
    const-string v3, "RED_MAX"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 254
    if-ne v1, v2, :cond_0

    .line 260
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDefaultDisplayConfiguration()Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 112
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "APP_PIXEL_HEIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    const-string v1, "APP_PIXEL_WIDTH"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string v1, "APP_PIXELS_PER_CLIENT_MM"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v1, "SERVER_PAD_COLUMNS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 116
    const-string v1, "SERVER_PAD_ROWS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v1, "SERVER_PIXEL_HEIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v1, "SERVER_PIXEL_WIDTH"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v1, "CLIENT_PIXEL_HEIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    const-string v1, "CLIENT_PIXEL_WIDTH"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    const-string v1, "DISTANCE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 122
    const-string v1, "MM_HEIGHT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    const-string v1, "MM_WIDTH"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    return-object v0
.end method

.method private setDefaultPixelFormat()Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 146
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 147
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "BITS_PER_PIXEL"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v1, "BLUE_MAX"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 149
    const-string v1, "DEPTH"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    const-string v1, "GREEN_MAX"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v1, "RED_MAX"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    return-object v0
.end method


# virtual methods
.method public getClientPixelFormat()Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 129
    const-string v2, "TMSDisplayManager"

    const-string v3, "getClientPixelFormat() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getClientPixelFormat()Landroid/os/Bundle;

    move-result-object v0

    .line 138
    if-nez v0, :cond_0

    .line 139
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->setDefaultPixelFormat()Landroid/os/Bundle;

    move-result-object v0

    .line 141
    :cond_0
    const-string v2, "TMSDisplayManager"

    const-string v3, "getClientPixelFormat() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 142
    .end local v0    # "b":Landroid/os/Bundle;
    .local v1, "b":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 134
    .end local v1    # "b":Landroid/os/Bundle;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    const-string v2, "TMSDisplayManager"

    const-string v3, "getClientPixelFormat() :Tmsengine null, returning default values"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->setDefaultPixelFormat()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 136
    .end local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "b":Landroid/os/Bundle;
    goto :goto_0
.end method

.method public getDisplayConfiguration()Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    const-string v3, "TMSDisplayManager"

    const-string v4, "getDisplayConfiguration() Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const/4 v2, 0x0

    .line 72
    .local v2, "info":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v0, "b":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getDisplayConfig()Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v2

    .line 81
    if-eqz v2, :cond_1

    .line 82
    const-string v3, "APP_PIXEL_HEIGHT"

    .line 83
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppVertRes()I

    move-result v4

    .line 82
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    const-string v3, "APP_PIXEL_WIDTH"

    .line 85
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppHoriRes()I

    move-result v4

    .line 84
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    const-string v3, "APP_PIXELS_PER_CLIENT_MM"

    .line 87
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppPixels()I

    move-result v4

    .line 86
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    const-string v3, "SERVER_PAD_COLUMNS"

    .line 89
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadCols()I

    move-result v4

    .line 88
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    const-string v3, "SERVER_PAD_ROWS"

    .line 91
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadRows()I

    move-result v4

    .line 90
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    const-string v3, "SERVER_PIXEL_HEIGHT"

    .line 93
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsVertRes()I

    move-result v4

    .line 92
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 94
    const-string v3, "SERVER_PIXEL_WIDTH"

    .line 95
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsHoriRes()I

    move-result v4

    .line 94
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    const-string v3, "CLIENT_PIXEL_HEIGHT"

    .line 97
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getVertRes()I

    move-result v4

    .line 96
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v3, "CLIENT_PIXEL_WIDTH"

    .line 99
    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHoriRes()I

    move-result v4

    .line 98
    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    const-string v3, "DISTANCE"

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getDistance()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    const-string v3, "MM_HEIGHT"

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHeight()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    const-string v3, "MM_WIDTH"

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getWidth()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    :goto_0
    const-string v3, "TMSDisplayManager"

    const-string v4, "getDisplayConfiguration() Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 107
    .end local v0    # "b":Landroid/os/Bundle;
    .local v1, "b":Landroid/os/Bundle;
    :goto_1
    return-object v1

    .line 77
    .end local v1    # "b":Landroid/os/Bundle;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_0
    const-string v3, "TMSDisplayManager"

    const-string v4, "getDisplayConfiguration():Tmsengine null, returning default values"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->setDefaultDisplayConfiguration()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 79
    .end local v0    # "b":Landroid/os/Bundle;
    .restart local v1    # "b":Landroid/os/Bundle;
    goto :goto_1

    .line 104
    .end local v1    # "b":Landroid/os/Bundle;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->setDefaultDisplayConfiguration()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "TMSDisplayManager"

    .line 59
    const-string v1, "DisplayManager.killListener() : Enter"

    .line 58
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 61
    return-void
.end method

.method public notifyDisplayConfigurationChanged(Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;)V
    .locals 8
    .param p1, "displayConfiguration"    # Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .prologue
    const/4 v7, 0x0

    .line 178
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 179
    .local v3, "length":I
    const-string v4, "TMSDisplayManager"

    .line 180
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DisplayManager.notifyDisplayConfigurationChanged():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 180
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 179
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 183
    .local v0, "b":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 184
    const-string v4, "APP_PIXEL_HEIGHT"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 185
    const-string v4, "APP_PIXEL_WIDTH"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    const-string v4, "APP_PIXELS_PER_CLIENT_MM"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    const-string v4, "SERVER_PAD_COLUMNS"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 188
    const-string v4, "SERVER_PAD_ROWS"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 189
    const-string v4, "SERVER_PIXEL_HEIGHT"

    .line 190
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsVertRes()I

    move-result v5

    .line 189
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 191
    const-string v4, "SERVER_PIXEL_WIDTH"

    .line 192
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsVertRes()I

    move-result v5

    .line 191
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    const-string v4, "CLIENT_PIXEL_HEIGHT"

    .line 194
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getVertRes()I

    move-result v5

    .line 193
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    const-string v4, "CLIENT_PIXEL_WIDTH"

    .line 196
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHoriRes()I

    move-result v5

    .line 195
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 197
    const-string v4, "DISTANCE"

    .line 198
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getDistance()I

    move-result v5

    .line 197
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    const-string v4, "MM_HEIGHT"

    .line 200
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHeight()I

    move-result v5

    .line 199
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    const-string v4, "MM_WIDTH"

    .line 202
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getWidth()I

    move-result v5

    .line 201
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 213
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 214
    return-void

    .line 207
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IDisplayListener;

    .line 208
    invoke-interface {v4, v0}, Lcom/mirrorlink/android/commonapi/IDisplayListener;->onDisplayConfigurationChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyPixelFormatChanged(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "pixelFormat"    # Landroid/os/Bundle;

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->isClientPixelFormatChanged(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 218
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientPixelFormat:Landroid/os/Bundle;

    .line 219
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 220
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 221
    .local v2, "length":I
    const-string v3, "TMSDisplayManager"

    .line 222
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DisplayManager.notifyPixelFormatChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 222
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 221
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 232
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 236
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    return-void

    .line 226
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDisplayListener;

    .line 227
    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDisplayListener;->onPixelFormatChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDisplayListener;)V
    .locals 4
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDisplayListener;

    .prologue
    .line 49
    const-string v1, "TMSDisplayManager"

    const-string v2, "DisplayManager.registerCallBack()"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "ret":Z
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    move-result v0

    .line 54
    :cond_0
    const-string v1, "TMSDisplayManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DisplayManager.registerCallBack() exit"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 289
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSDisplayManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DisplayManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    if-nez v2, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 294
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 295
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_2

    .line 302
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 296
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 297
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 296
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 297
    if-eqz v3, :cond_3

    .line 298
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 299
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDisplayListener;

    .line 298
    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 295
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
