.class public Lcom/samsung/android/mirrorlink/util/AppDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "AppDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/util/AppDbHelper$AppDbUtil;,
        Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;,
        Lcom/samsung/android/mirrorlink/util/AppDbHelper$EntityTable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AppDbHelper"

.field public static sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    const-string v0, "appdb"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 17
    const-string v0, "AppDbHelper"

    const-string v1, "Exit Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static declared-synchronized deinit()V
    .locals 3

    .prologue
    .line 28
    const-class v1, Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    monitor-enter v1

    :try_start_0
    const-string v0, "AppDbHelper"

    const-string v2, "Enter deinit"

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->close()V

    .line 32
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    monitor-exit v1

    return-void

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const-class v1, Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;

    .line 24
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->sAppDbHelper:Lcom/samsung/android/mirrorlink/util/AppDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 38
    const-string v0, "CREATE TABLE appdata( _ID INTEGER PRIMARY KEY AUTOINCREMENT, packageName TEXT NOT NULL UNIQUE , category NUMERIC NOT NULL, certified NUMERIC default \'0\', certified_appId TEXT, certified_appUUid TEXT ,certified_properties TEXT ,certified_signature TEXT  ) "

    .line 49
    .local v0, "createTable":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    const-string v1, "AppDbHelper"

    const-string v2, "Created table appdata"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v1, "AppDbHelper"

    const-string v2, "Creating Entity table"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "CREATE TABLE EntityData ( pkg_name TEXT NOT NULL REFERENCES appdata(packageName) ON DELETE CASCADE, e_name TEXT NOT NULL, targetList TEXT, restricted TEXT, non_restricted TEXT, serviceList TEXT, PRIMARY KEY(pkg_name,e_name ) ) "

    .line 63
    const-string v1, "AppDbHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "creating table "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    const-string v1, "AppDbHelper"

    const-string v2, "Created EntityDatatable"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 72
    const-string v0, "PRAGMA foreign_keys = ON"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 77
    const-string v0, "AppDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading database appdb . Existing contents will be lost ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]->["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v0, "DROP TABLE IF EXISTS appdata"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 81
    return-void
.end method
