.class Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;
.super Landroid/os/Handler;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcsDeviceMngrHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 2139
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 2140
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2141
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngrHandler() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2143
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceMngrHandler() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2148
    const-string v0, "TMSDevMngr"

    .line 2149
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage() - Enter - msg.what:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2150
    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2149
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2148
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2151
    const-string v0, "TMSDevMngr"

    .line 2152
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage() - Enter - msg.arg1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2153
    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2152
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2151
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2155
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 2167
    const-string v0, "TMSDevMngr"

    .line 2168
    const-string v1, "handleMessage() - Unknown message type"

    .line 2167
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "handleMessage() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2171
    return-void

    .line 2163
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceMngrHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->callNativeOnEvent(Landroid/os/Message;)V

    goto :goto_0

    .line 2155
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x8 -> :sswitch_0
        0x100 -> :sswitch_0
        0x200 -> :sswitch_0
    .end sparse-switch
.end method
