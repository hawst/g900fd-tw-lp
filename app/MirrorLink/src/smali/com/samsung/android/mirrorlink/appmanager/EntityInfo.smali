.class public Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;
.super Ljava/lang/Object;
.source "EntityInfo.java"


# instance fields
.field private mAppID:Ljava/lang/String;

.field private mAppUUID:Ljava/lang/String;

.field private mEntityName:Ljava/lang/String;

.field private mNonRestricted:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mProperties:Ljava/lang/String;

.field private mRestricted:Ljava/lang/String;

.field private mServices:Ljava/lang/String;

.field private mSignature:Ljava/lang/String;

.field private mTargets:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method public getAppUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mAppUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getEntityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mEntityName:Ljava/lang/String;

    return-object v0
.end method

.method public getNonRestricted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mNonRestricted:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getProperties()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mProperties:Ljava/lang/String;

    return-object v0
.end method

.method public getRestricted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mRestricted:Ljava/lang/String;

    return-object v0
.end method

.method public getServices()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mServices:Ljava/lang/String;

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mSignature:Ljava/lang/String;

    return-object v0
.end method

.method public getTargets()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mTargets:Ljava/lang/String;

    return-object v0
.end method

.method public setAppID(Ljava/lang/String;)V
    .locals 0
    .param p1, "appID"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mAppID:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setAppUUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "appUUID"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mAppUUID:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setEntityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "entityName"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mEntityName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setNonRestricted(Ljava/lang/String;)V
    .locals 0
    .param p1, "nonRestricted"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mNonRestricted:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mPackageName:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setProperties(Ljava/lang/String;)V
    .locals 0
    .param p1, "properties"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mProperties:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setRestricted(Ljava/lang/String;)V
    .locals 0
    .param p1, "restricted"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mRestricted:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setServices(Ljava/lang/String;)V
    .locals 0
    .param p1, "services"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mServices:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setSignature(Ljava/lang/String;)V
    .locals 0
    .param p1, "signature"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mSignature:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setTargets(Ljava/lang/String;)V
    .locals 0
    .param p1, "targets"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->mTargets:Ljava/lang/String;

    .line 69
    return-void
.end method
