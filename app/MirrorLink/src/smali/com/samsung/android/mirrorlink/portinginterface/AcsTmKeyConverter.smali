.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;
.super Ljava/lang/Object;
.source "AcsTmKeyConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    }
.end annotation


# static fields
.field public static final AND_DEVICEID_CODE_INVALID:I = -0x1

.field public static final AND_FACK_KEY_CODE_INVALID:I = -0x1

.field public static final AND_FLAGS_CODE_INVALID:I = -0x1

.field public static final AND_KEY_CODE_INVALID:I = -0x1

.field public static final AND_METASTATE_CODE_INVALID:I = -0x1

.field public static final AND_REPEAT_CODE_INVALID:I = -0x1

.field public static final AND_SCANCODE_CODE_INVALID:I = -0x1

.field public static final AND_SOURCE_CODE_INVALID:I = -0x1

.field private static LOG_TAG:Ljava/lang/String; = null

.field public static final TM_CODE_KNOB_MAX:I = 0x300000ff

.field public static final TM_CODE_KNOB_MIN:I = 0x30000000

.field private static final TM_CODE_LOWER_CASE_A:I = 0x61

.field private static final TM_CODE_LOWER_CASE_Z:I = 0x7a

.field private static final TM_CODE_NUMBER_0:I = 0x30

.field private static final TM_CODE_NUMBER_9:I = 0x39

.field private static final TM_CODE_UPPER_CASE_A:I = 0x41

.field private static final TM_CODE_UPPER_CASE_Z:I = 0x5a


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "TMSKeyConverter"

    sput-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getAndroidKeyEvent(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V
    .locals 4
    .param p0, "andKeyEvent"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .param p1, "tmKeyCode"    # I

    .prologue
    const v3, 0xffb0

    .line 90
    const/16 v0, 0x61

    if-lt p1, v0, :cond_0

    .line 91
    const/16 v0, 0x7a

    if-gt p1, v0, :cond_0

    .line 93
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent SMALL CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    add-int/lit8 v0, p1, -0x61

    add-int/lit8 v0, v0, 0x1d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 139
    :goto_0
    return-void

    .line 99
    :cond_0
    const/16 v0, 0x41

    if-lt p1, v0, :cond_1

    .line 100
    const/16 v0, 0x5a

    if-gt p1, v0, :cond_1

    .line 102
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent UPPER CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    add-int/lit8 v0, p1, -0x41

    add-int/lit8 v0, v0, 0x1d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mCapsOn:Z

    goto :goto_0

    .line 110
    :cond_1
    const/16 v0, 0x30

    if-lt p1, v0, :cond_2

    .line 111
    const/16 v0, 0x39

    if-gt p1, v0, :cond_2

    .line 113
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent NUMBER CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    add-int/lit8 v0, p1, -0x30

    add-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 119
    :cond_2
    const/high16 v0, 0x30000000

    if-gt v0, p1, :cond_3

    .line 120
    const v0, 0x3000ffff

    if-lt v0, p1, :cond_3

    .line 122
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent DEVICE CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getDeviceKeyCode(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V

    goto :goto_0

    .line 128
    :cond_3
    if-lt p1, v3, :cond_4

    .line 129
    const v0, 0xffb9

    if-gt p1, v0, :cond_4

    .line 130
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent X11 -NUMBER CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sub-int v0, p1, v3

    add-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 135
    :cond_4
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " getAndroidKeyEvent X11 CASE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static {p0, p1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->getX11Map(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V

    goto/16 :goto_0
.end method

.method private static getDeviceKeyCode(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V
    .locals 5
    .param p0, "andKeyEvent"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .param p1, "tmKeyCode"    # I

    .prologue
    const/16 v4, 0xa4

    const/16 v3, 0x55

    const/16 v2, 0x3d

    const/16 v1, 0x17

    const/4 v0, -0x1

    .line 148
    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 150
    sparse-switch p1, :sswitch_data_0

    .line 437
    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 440
    :goto_0
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDeviceKeyCode : tmKeyCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " android code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return-void

    .line 158
    :sswitch_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 162
    :sswitch_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 166
    :sswitch_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 170
    :sswitch_3
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 174
    :sswitch_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 179
    :sswitch_5
    const/16 v0, 0xbb

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 184
    :sswitch_6
    const/16 v0, 0x42

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 188
    :sswitch_7
    const/16 v0, 0x43

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 193
    :sswitch_8
    const/16 v0, 0xa8

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 197
    :sswitch_9
    const/16 v0, 0xa9

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 202
    :sswitch_a
    const/16 v0, 0x1c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 208
    :sswitch_b
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 212
    :sswitch_c
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 216
    :sswitch_d
    const/16 v0, 0x54

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 220
    :sswitch_e
    const/16 v0, 0x52

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 226
    :sswitch_f
    const/16 v0, 0x83

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 229
    :sswitch_10
    const/16 v0, 0x84

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 232
    :sswitch_11
    const/16 v0, 0x85

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 235
    :sswitch_12
    const/16 v0, 0x86

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 238
    :sswitch_13
    const/16 v0, 0x87

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 241
    :sswitch_14
    const/16 v0, 0x88

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 244
    :sswitch_15
    const/16 v0, 0x89

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 247
    :sswitch_16
    const/16 v0, 0x8a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 250
    :sswitch_17
    const/16 v0, 0x8b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 253
    :sswitch_18
    const/16 v0, 0x8c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 256
    :sswitch_19
    const/16 v0, 0x8d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 259
    :sswitch_1a
    const/16 v0, 0x8e

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 263
    :sswitch_1b
    const/16 v0, 0x39

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 269
    :sswitch_1c
    const/4 v0, 0x7

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 273
    :sswitch_1d
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 277
    :sswitch_1e
    const/16 v0, 0x9

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 281
    :sswitch_1f
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 285
    :sswitch_20
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 289
    :sswitch_21
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 293
    :sswitch_22
    const/16 v0, 0xd

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 297
    :sswitch_23
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 301
    :sswitch_24
    const/16 v0, 0xf

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 305
    :sswitch_25
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 309
    :sswitch_26
    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 313
    :sswitch_27
    const/16 v0, 0x12

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 326
    :sswitch_28
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 332
    :sswitch_29
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 337
    :sswitch_2a
    const/16 v0, 0x56

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 341
    :sswitch_2b
    const/16 v0, 0x5a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 345
    :sswitch_2c
    const/16 v0, 0x59

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 349
    :sswitch_2d
    const/16 v0, 0x57

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 353
    :sswitch_2e
    const/16 v0, 0x58

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 357
    :sswitch_2f
    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 363
    :sswitch_30
    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 367
    :sswitch_31
    const/16 v0, 0x1b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 374
    :sswitch_32
    const/16 v0, 0x16

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 377
    :sswitch_33
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 380
    :sswitch_34
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 384
    :sswitch_35
    const/16 v0, 0xbc

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 387
    :sswitch_36
    const/16 v0, 0xbd

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 390
    :sswitch_37
    const/16 v0, 0xbe

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 393
    :sswitch_38
    const/16 v0, 0xbf

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 397
    :sswitch_39
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 401
    :sswitch_3a
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 411
    :sswitch_3b
    const/16 v0, 0x66

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 415
    :sswitch_3c
    const/16 v0, 0x67

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 419
    :sswitch_3d
    const/16 v0, 0x68

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 423
    :sswitch_3e
    const/16 v0, 0x69

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 428
    :sswitch_3f
    iput v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 432
    :sswitch_40
    iput v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 433
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 150
    :sswitch_data_0
    .sparse-switch
        0x30000000 -> :sswitch_32
        0x30000001 -> :sswitch_33
        0x30000002 -> :sswitch_34
        0x30000003 -> :sswitch_35
        0x30000004 -> :sswitch_36
        0x30000005 -> :sswitch_39
        0x30000006 -> :sswitch_37
        0x30000007 -> :sswitch_38
        0x30000008 -> :sswitch_3a
        0x3000000a -> :sswitch_3b
        0x3000000b -> :sswitch_3c
        0x3000000c -> :sswitch_3d
        0x3000000d -> :sswitch_3e
        0x3000000e -> :sswitch_3f
        0x3000000f -> :sswitch_40
        0x30000100 -> :sswitch_1c
        0x30000101 -> :sswitch_1d
        0x30000102 -> :sswitch_1e
        0x30000103 -> :sswitch_1f
        0x30000104 -> :sswitch_20
        0x30000105 -> :sswitch_21
        0x30000106 -> :sswitch_22
        0x30000107 -> :sswitch_23
        0x30000108 -> :sswitch_24
        0x30000109 -> :sswitch_25
        0x3000010a -> :sswitch_26
        0x3000010b -> :sswitch_27
        0x30000200 -> :sswitch_0
        0x30000201 -> :sswitch_1
        0x30000202 -> :sswitch_2
        0x30000203 -> :sswitch_3
        0x30000204 -> :sswitch_4
        0x30000205 -> :sswitch_5
        0x30000206 -> :sswitch_6
        0x30000207 -> :sswitch_7
        0x30000208 -> :sswitch_8
        0x30000209 -> :sswitch_9
        0x3000020a -> :sswitch_a
        0x3000020c -> :sswitch_b
        0x3000020d -> :sswitch_c
        0x3000020e -> :sswitch_d
        0x3000020f -> :sswitch_e
        0x30000300 -> :sswitch_f
        0x30000301 -> :sswitch_10
        0x30000302 -> :sswitch_11
        0x30000303 -> :sswitch_12
        0x30000304 -> :sswitch_13
        0x30000305 -> :sswitch_14
        0x30000306 -> :sswitch_15
        0x30000307 -> :sswitch_16
        0x30000308 -> :sswitch_17
        0x30000309 -> :sswitch_18
        0x30000310 -> :sswitch_19
        0x30000311 -> :sswitch_1a
        0x300003ff -> :sswitch_1b
        0x30000400 -> :sswitch_28
        0x30000401 -> :sswitch_29
        0x30000402 -> :sswitch_2a
        0x30000403 -> :sswitch_2b
        0x30000404 -> :sswitch_2c
        0x30000405 -> :sswitch_2d
        0x30000406 -> :sswitch_2e
        0x30000407 -> :sswitch_2f
        0x30000408 -> :sswitch_30
        0x30000409 -> :sswitch_31
    .end sparse-switch
.end method

.method public static getKnobBaseValue(I)I
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 915
    and-int/lit16 v0, p0, -0xf1

    return v0
.end method

.method public static getKnobMvalue(I)I
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 906
    and-int/lit8 v0, p0, 0xf

    return v0
.end method

.method public static getKnobNvalue(I)I
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 897
    and-int/lit16 v0, p0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    return v0
.end method

.method private static getX11Map(Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;I)V
    .locals 6
    .param p0, "andKeyEvent"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;
    .param p1, "keycode"    # I

    .prologue
    const/16 v5, 0x48

    const/16 v4, 0x47

    const/16 v3, 0x45

    const/16 v2, 0x44

    const/16 v1, 0x3b

    .line 450
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 452
    sparse-switch p1, :sswitch_data_0

    .line 885
    sget-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No Mapping Found in X11 map for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    :goto_0
    return-void

    .line 455
    :sswitch_0
    const/16 v0, 0x43

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 458
    :sswitch_1
    const/16 v0, 0x1c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 463
    :sswitch_2
    const/16 v0, 0x3d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 469
    :sswitch_3
    const/16 v0, 0x42

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 474
    :sswitch_4
    const/16 v0, 0x7f

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 480
    :sswitch_5
    const/16 v0, 0x74

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 485
    :sswitch_6
    const/16 v0, 0x78

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 490
    :sswitch_7
    const/16 v0, 0x6f

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 496
    :sswitch_8
    const/16 v0, 0x70

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 506
    :sswitch_9
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 511
    :sswitch_a
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 516
    :sswitch_b
    const/16 v0, 0x16

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 521
    :sswitch_c
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 528
    :sswitch_d
    const/16 v0, 0x5c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 535
    :sswitch_e
    const/16 v0, 0x5d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 542
    :sswitch_f
    const/16 v0, 0x7b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 550
    :sswitch_10
    const/16 v0, 0x7a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 556
    :sswitch_11
    const/16 v0, 0x7c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 560
    :sswitch_12
    const/16 v0, 0x52

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 564
    :sswitch_13
    const/16 v0, 0x54

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 569
    :sswitch_14
    const/16 v0, 0x79

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 574
    :sswitch_15
    const/16 v0, 0x8f

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 579
    :sswitch_16
    const/16 v0, 0x3e

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 585
    :sswitch_17
    const/16 v0, 0x83

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 591
    :sswitch_18
    const/16 v0, 0x84

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto :goto_0

    .line 597
    :sswitch_19
    const/16 v0, 0x85

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 603
    :sswitch_1a
    const/16 v0, 0x86

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 608
    :sswitch_1b
    const/16 v0, 0x87

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 613
    :sswitch_1c
    const/16 v0, 0x88

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 618
    :sswitch_1d
    const/16 v0, 0x89

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 623
    :sswitch_1e
    const/16 v0, 0x8a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 628
    :sswitch_1f
    const/16 v0, 0x8b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 633
    :sswitch_20
    const/16 v0, 0x8c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 638
    :sswitch_21
    const/16 v0, 0x8d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 643
    :sswitch_22
    const/16 v0, 0x8e

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 648
    :sswitch_23
    const/16 v0, 0xa1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 653
    :sswitch_24
    const/16 v0, 0x9b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 658
    :sswitch_25
    const/16 v0, 0x9d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 663
    :sswitch_26
    const/16 v0, 0x9f

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 668
    :sswitch_27
    const/16 v0, 0x9c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 673
    :sswitch_28
    const/16 v0, 0x9e

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 678
    :sswitch_29
    const/16 v0, 0x9a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 682
    :sswitch_2a
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 683
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 687
    :sswitch_2b
    const/16 v0, 0x3c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 688
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 693
    :sswitch_2c
    const/16 v0, 0x71

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 694
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 699
    :sswitch_2d
    const/16 v0, 0x72

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 700
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 705
    :sswitch_2e
    const/16 v0, 0x73

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 706
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 711
    :sswitch_2f
    const/16 v0, 0x75

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 712
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 717
    :sswitch_30
    const/16 v0, 0x76

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 718
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 722
    :sswitch_31
    const/16 v0, 0x39

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 723
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 727
    :sswitch_32
    const/16 v0, 0x3a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    .line 728
    iget v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mMetaState:I

    goto/16 :goto_0

    .line 733
    :sswitch_33
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 734
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 738
    :sswitch_34
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 739
    const/16 v0, 0x4b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 743
    :sswitch_35
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 744
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 748
    :sswitch_36
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 749
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 753
    :sswitch_37
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 754
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 758
    :sswitch_38
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 759
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 763
    :sswitch_39
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 764
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 768
    :sswitch_3a
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 769
    const/4 v0, 0x7

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 773
    :sswitch_3b
    const/16 v0, 0x4b

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 778
    :sswitch_3c
    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 782
    :sswitch_3d
    const/16 v0, 0x51

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 786
    :sswitch_3e
    const/16 v0, 0x37

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 790
    :sswitch_3f
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 794
    :sswitch_40
    const/16 v0, 0x38

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 799
    :sswitch_41
    const/16 v0, 0x4c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 803
    :sswitch_42
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 804
    const/16 v0, 0x4a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 808
    :sswitch_43
    const/16 v0, 0x4a

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 817
    :sswitch_44
    const/16 v0, 0x46

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 826
    :sswitch_45
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 827
    const/16 v0, 0x4c

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 831
    :sswitch_46
    const/16 v0, 0x4d

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 835
    :sswitch_47
    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 839
    :sswitch_48
    const/16 v0, 0x49

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 843
    :sswitch_49
    iput v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 847
    :sswitch_4a
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 848
    const/16 v0, 0xd

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 852
    :sswitch_4b
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 853
    iput v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 857
    :sswitch_4c
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 858
    iput v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 862
    :sswitch_4d
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 863
    const/16 v0, 0x49

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 867
    :sswitch_4e
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 868
    iput v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 872
    :sswitch_4f
    iput v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mfakeModifierCode:I

    .line 873
    iput v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 878
    :sswitch_50
    iput v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 882
    :sswitch_51
    const/16 v0, 0x12

    iput v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsTmKeyConverter$AndroidKeyEvent;->mAndKeyCode:I

    goto/16 :goto_0

    .line 452
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_16
        0x21 -> :sswitch_33
        0x22 -> :sswitch_34
        0x23 -> :sswitch_35
        0x24 -> :sswitch_36
        0x25 -> :sswitch_37
        0x26 -> :sswitch_38
        0x27 -> :sswitch_3b
        0x28 -> :sswitch_39
        0x29 -> :sswitch_3a
        0x2a -> :sswitch_3c
        0x2b -> :sswitch_3d
        0x2c -> :sswitch_3e
        0x2d -> :sswitch_3f
        0x2e -> :sswitch_40
        0x2f -> :sswitch_41
        0x3a -> :sswitch_42
        0x3b -> :sswitch_43
        0x3d -> :sswitch_44
        0x3f -> :sswitch_45
        0x40 -> :sswitch_46
        0x5b -> :sswitch_47
        0x5c -> :sswitch_48
        0x5d -> :sswitch_49
        0x5e -> :sswitch_4a
        0x5f -> :sswitch_4b
        0x60 -> :sswitch_50
        0x7b -> :sswitch_4c
        0x7c -> :sswitch_4d
        0x7d -> :sswitch_4e
        0x7e -> :sswitch_4f
        0xa3 -> :sswitch_51
        0xd7 -> :sswitch_3c
        0xf7 -> :sswitch_41
        0xff08 -> :sswitch_0
        0xff09 -> :sswitch_2
        0xff0a -> :sswitch_3
        0xff0b -> :sswitch_1
        0xff0d -> :sswitch_3
        0xff13 -> :sswitch_4
        0xff14 -> :sswitch_5
        0xff15 -> :sswitch_6
        0xff1b -> :sswitch_7
        0xff50 -> :sswitch_10
        0xff51 -> :sswitch_9
        0xff52 -> :sswitch_a
        0xff53 -> :sswitch_b
        0xff54 -> :sswitch_c
        0xff55 -> :sswitch_d
        0xff57 -> :sswitch_f
        0xff58 -> :sswitch_10
        0xff63 -> :sswitch_11
        0xff67 -> :sswitch_12
        0xff68 -> :sswitch_13
        0xff6b -> :sswitch_14
        0xff7f -> :sswitch_15
        0xff80 -> :sswitch_16
        0xff89 -> :sswitch_2
        0xff8d -> :sswitch_3
        0xff91 -> :sswitch_17
        0xff92 -> :sswitch_18
        0xff93 -> :sswitch_19
        0xff94 -> :sswitch_1a
        0xff95 -> :sswitch_10
        0xff96 -> :sswitch_9
        0xff97 -> :sswitch_a
        0xff98 -> :sswitch_b
        0xff99 -> :sswitch_c
        0xff9a -> :sswitch_d
        0xff9b -> :sswitch_e
        0xff9c -> :sswitch_f
        0xff9d -> :sswitch_10
        0xff9e -> :sswitch_11
        0xff9f -> :sswitch_8
        0xffaa -> :sswitch_24
        0xffab -> :sswitch_25
        0xffac -> :sswitch_26
        0xffad -> :sswitch_27
        0xffae -> :sswitch_28
        0xffaf -> :sswitch_29
        0xffbd -> :sswitch_23
        0xffbe -> :sswitch_17
        0xffbf -> :sswitch_18
        0xffc0 -> :sswitch_19
        0xffc1 -> :sswitch_1a
        0xffc2 -> :sswitch_1b
        0xffc3 -> :sswitch_1c
        0xffc4 -> :sswitch_1d
        0xffc5 -> :sswitch_1e
        0xffc6 -> :sswitch_1f
        0xffc7 -> :sswitch_20
        0xffc8 -> :sswitch_21
        0xffc9 -> :sswitch_22
        0xffe1 -> :sswitch_2a
        0xffe2 -> :sswitch_2b
        0xffe3 -> :sswitch_2c
        0xffe4 -> :sswitch_2d
        0xffe5 -> :sswitch_2e
        0xffe7 -> :sswitch_2f
        0xffe8 -> :sswitch_30
        0xffe9 -> :sswitch_31
        0xffea -> :sswitch_32
        0xffff -> :sswitch_8
    .end sparse-switch
.end method
