.class Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;
.super Landroid/os/AsyncTask;
.source "AcmsPackageEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "asyncstart"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsPackageEventListener"


# instance fields
.field context:Landroid/content/Context;

.field intent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->context:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->intent:Landroid/content/Intent;

    .line 48
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "pageList"    # [Ljava/lang/Void;

    .prologue
    .line 52
    const-string v0, "AcmsPackageEventListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->context:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/mirrorlink/acms/api/AcmsService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/receivers/AcmsPackageEventListener$asyncstart;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 55
    const/4 v0, 0x0

    return-object v0
.end method
