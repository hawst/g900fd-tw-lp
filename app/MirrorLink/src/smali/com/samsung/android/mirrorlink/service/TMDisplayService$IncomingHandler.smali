.class Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;
.super Landroid/os/Handler;
.source "TMDisplayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TMDisplayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v10, 0xa

    const/4 v6, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 102
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 180
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 104
    :pswitch_1
    const-string v3, "TMSDisplayService"

    const-string v4, "Got the message TmsService started"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :try_start_0
    const-string v3, "android.os.SystemProperties"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "set"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 108
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "net.mirrorlink.on"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    .line 119
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->disableNotification()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$2(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 120
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsWakeLockAcquire()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$3(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 121
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->tmsDisableKeyguard()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$4(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 123
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-virtual {v3, v9}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->setPhoneInCarMode(Z)V

    .line 124
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->createPresentation()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$5(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 125
    invoke-static {p0, v10, v8, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsService.start() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 111
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsService.start() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 113
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsService.start() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v3, "TMSDisplayService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TmsService.start() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 128
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :pswitch_2
    const-string v3, "TMSDisplayService"

    const-string v4, "Got the message TmsService stopped2"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->notifyMirroLinkDisconnect()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$6(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 130
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$0(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$7(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Landroid/content/ServiceConnection;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 132
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-static {v3, v8}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$1(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V

    .line 134
    :cond_1
    invoke-virtual {p0, v5}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 136
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->stopSelf()V

    goto/16 :goto_0

    .line 139
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->showInternalBlackScreenWithRunningImage(Z)V
    invoke-static {v3, v9}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$8(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V

    goto/16 :goto_0

    .line 143
    :pswitch_4
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->showInternalBlackScreenWithRunningImage(Z)V
    invoke-static {v3, v8}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$8(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V

    goto/16 :goto_0

    .line 146
    :pswitch_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->dismissInternalBlackScreen()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$9(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 147
    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v9, :cond_0

    .line 148
    const-string v3, "TMSDisplayService"

    const-string v4, "Its a USB Disconnect, So STOP TMDisplaService"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    .line 152
    .local v2, "msgToTms":Landroid/os/Message;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    iput-object v3, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 153
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    if-eqz v3, :cond_2

    .line 155
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    invoke-virtual {v3, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_4

    .line 162
    :cond_2
    :goto_2
    const/4 v3, 0x4

    invoke-static {p0, v3, v8, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 156
    :catch_4
    move-exception v0

    .line 158
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 167
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v2    # "msgToTms":Landroid/os/Message;
    :pswitch_6
    const/16 v3, 0xb

    invoke-static {v5, v3, v8, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 170
    :pswitch_7
    const-string v3, "TMSDisplayService"

    const-string v4, "Got the message self unbind"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->notifyMirroLinkDisconnect()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$6(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 172
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # invokes: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->updateNCM()V
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$10(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V

    .line 173
    invoke-virtual {p0, v5}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 174
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->stopSelf()V

    goto/16 :goto_0

    .line 177
    :pswitch_8
    invoke-static {v5, v10, v8, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/mirrorlink/service/TMDisplayService$IncomingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method
