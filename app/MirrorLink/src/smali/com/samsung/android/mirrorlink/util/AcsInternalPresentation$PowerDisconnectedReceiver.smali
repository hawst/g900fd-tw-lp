.class Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AcsInternalPresentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerDisconnectedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;-><init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    # getter for: Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->access$0(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 277
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;->this$0:Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->dismiss()V

    .line 279
    :cond_0
    return-void
.end method
