.class public Lcom/samsung/android/mirrorlink/util/AppDbInterface;
.super Ljava/lang/Object;
.source "AppDbInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/util/AppDbInterface$UpdateResult;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Upnp/AppDbInterface"

.field public static sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface :In private constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private dbExists()Z
    .locals 3

    .prologue
    .line 411
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 412
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    const-string v2, "appdb"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 413
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 414
    const/4 v1, 0x1

    .line 418
    .end local v0    # "dbFile":Ljava/io/File;
    :goto_0
    return v1

    .line 417
    :cond_0
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "Db does not exists"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized deinit()V
    .locals 3

    .prologue
    .line 57
    const-class v1, Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    monitor-enter v1

    :try_start_0
    const-string v0, "Upnp/AppDbInterface"

    const-string v2, "Enter deinit Appdbinterface"

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/AppDbHelper;->deinit()V

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    monitor-exit v1

    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const-class v1, Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    .line 38
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    if-eqz p0, :cond_1

    .line 39
    const-string v0, "Upnp/AppDbInterface"

    const-string v2, "Previously set context was null. Hence updating with new context"

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iput-object p0, v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    .line 42
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->sAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getIdFromURI(Landroid/net/Uri;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "lastpath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 123
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 126
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private declared-synchronized updateAppdata(Landroid/content/ContentValues;)J
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v1, -0x1

    .line 178
    monitor-enter p0

    :try_start_0
    const-string v3, "Upnp/AppDbInterface"

    const-string v4, "Enter update AppData"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 180
    const-string v3, "Upnp/AppDbInterface"

    const-string v4, "AppDbInterface.updateAppdata() : Context is null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :goto_0
    monitor-exit p0

    return-wide v1

    .line 183
    :cond_0
    :try_start_1
    const-string v3, "packageName"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 185
    const-string v3, "Upnp/AppDbInterface"

    const-string v4, "AppDbInterface.updateAppdata() : package name is null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 178
    .end local v0    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 188
    .restart local v0    # "packageName":Ljava/lang/String;
    :cond_1
    :try_start_2
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "Exit update"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 191
    const-string v3, "packageName=?"

    .line 192
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    .line 190
    invoke-virtual {v1, v2, p1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    int-to-long v1, v1

    goto :goto_0
.end method

.method private declared-synchronized updateEntityInfoIfExists(Landroid/content/ContentValues;)Z
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 479
    monitor-enter p0

    :try_start_0
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Enter updateIfExists for entity info"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 481
    .local v9, "packageName":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 482
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v10

    .line 516
    :goto_0
    monitor-exit p0

    return v0

    .line 485
    :cond_0
    if-nez v9, :cond_1

    .line 486
    :try_start_1
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "No package name specified"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v10

    .line 487
    goto :goto_0

    .line 489
    :cond_1
    const-string v0, "e_name"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 490
    .local v8, "entityName":Ljava/lang/String;
    if-nez v8, :cond_2

    .line 491
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "No entity name specified"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v10

    .line 492
    goto :goto_0

    .line 494
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 495
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pkg_name"

    aput-object v4, v2, v3

    .line 496
    const-string v3, "pkg_name=? AND e_name=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 497
    aput-object v9, v4, v5

    const/4 v5, 0x1

    aput-object v8, v4, v5

    .line 498
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 495
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 499
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_3

    .line 500
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v10

    .line 501
    goto :goto_0

    .line 504
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 505
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "No row found with given package name"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 507
    goto :goto_0

    .line 509
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 510
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package name and entity already exists. Needs to update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v0, "pkg_name"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    .line 513
    const-string v2, "pkg_name=? AND e_name=?"

    .line 514
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    const/4 v4, 0x1

    aput-object v8, v3, v4

    .line 512
    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v11

    .line 516
    goto/16 :goto_0

    .line 479
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "entityName":Ljava/lang/String;
    .end local v9    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateIfAppInfoExists(Landroid/content/ContentValues;Z)I
    .locals 11
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "certState"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 130
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Enter isUpdateNeeded "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 132
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "updateIfAppInfoExists: Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 174
    :goto_0
    return v0

    .line 135
    :cond_0
    const-string v0, "packageName"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 137
    .local v7, "packageName":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 138
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "updateIfAppInfoExists : No package name specified"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 139
    goto :goto_0

    .line 141
    :cond_1
    const/4 v6, 0x0

    .line 142
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 143
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 144
    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "packageName"

    aput-object v3, v2, v10

    .line 145
    const-string v3, "certified"

    aput-object v3, v2, v9

    .line 146
    const-string v3, "packageName=?"

    .line 147
    new-array v4, v9, [Ljava/lang/String;

    aput-object v7, v4, v10

    const/4 v5, 0x0

    .line 142
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 149
    if-nez v6, :cond_2

    .line 150
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "updateIfAppInfoExists: cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 151
    goto :goto_0

    .line 153
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 154
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "updateIfAppInfoExists : No app found in table with given package name"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 156
    goto :goto_0

    .line 158
    :cond_3
    const-string v0, "category"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "category"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_4

    .line 159
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Update should not be called for non vnc app"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 161
    goto :goto_0

    .line 163
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 166
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Certificate state for"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is changed with value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->updateAppdata(Landroid/content/ContentValues;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 168
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 169
    goto/16 :goto_0

    .line 172
    :cond_5
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Certificate state not changed "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 174
    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 6

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 47
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :goto_0
    monitor-exit p0

    return-void

    .line 51
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 52
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_DELETE_SEQ:Landroid/net/Uri;

    const-string v2, "name=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "appdata"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public delete(I)Z
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 196
    const-string v3, "Upnp/AppDbInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Enter delete appId"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->dbExists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 208
    :goto_0
    return v1

    .line 200
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 201
    sget-object v4, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    const-string v5, "_id=?"

    .line 202
    new-array v6, v2, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 200
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 203
    .local v0, "rows":I
    if-lez v0, :cond_1

    .line 204
    const-string v1, "Upnp/AppDbInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exit delete appId with "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 205
    goto :goto_0

    .line 207
    :cond_1
    const-string v2, "Upnp/AppDbInterface"

    const-string v3, "Exit delete with no rows deleted"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public delete(Ljava/lang/String;)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 214
    const-string v3, "Upnp/AppDbInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Enter delete package name"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->dbExists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 225
    :goto_0
    return v1

    .line 219
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    const-string v5, "packageName=?"

    new-array v6, v2, [Ljava/lang/String;

    aput-object p1, v6, v1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 220
    .local v0, "rows":I
    if-lez v0, :cond_1

    .line 221
    const-string v1, "Upnp/AppDbInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exit delete package name with "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 222
    goto :goto_0

    .line 224
    :cond_1
    const-string v2, "Upnp/AppDbInterface"

    const-string v3, "Exit delete with no rows deleted"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteCategory(I)Z
    .locals 8
    .param p1, "category"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 248
    const-string v3, "Upnp/AppDbInterface"

    const-string v4, "Enter delete Category"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->dbExists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 262
    :goto_0
    return v1

    .line 252
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 253
    sget-object v4, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 254
    const-string v5, "category=? "

    .line 255
    new-array v6, v2, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 252
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 257
    .local v0, "rows":I
    if-lez v0, :cond_1

    .line 258
    const-string v1, "Upnp/AppDbInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exit delete category name with "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 259
    goto :goto_0

    .line 261
    :cond_1
    const-string v2, "Upnp/AppDbInterface"

    const-string v3, "Exit delete with no rows deleted"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteCategory(Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;Z)Z
    .locals 8
    .param p1, "category"    # Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;
    .param p2, "flagCertified"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 229
    const-string v1, "Upnp/AppDbInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Enter delete Category name"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->dbExists()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 244
    :goto_0
    return v1

    .line 233
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 234
    sget-object v5, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 235
    const-string v6, "category=? AND certified=?"

    .line 237
    const/4 v1, 0x2

    new-array v7, v1, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v2

    .line 238
    if-eqz p2, :cond_1

    const-string v1, "1"

    :goto_1
    aput-object v1, v7, v3

    .line 234
    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 239
    .local v0, "rows":I
    if-lez v0, :cond_2

    .line 240
    const-string v1, "Upnp/AppDbInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Exit delete category name with "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "deleted"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v3

    .line 241
    goto :goto_0

    .line 238
    .end local v0    # "rows":I
    :cond_1
    const-string v1, "0"

    goto :goto_1

    .line 243
    .restart local v0    # "rows":I
    :cond_2
    const-string v1, "Upnp/AppDbInterface"

    const-string v3, "Exit delete with no rows deleted"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 244
    goto :goto_0
.end method

.method public deleteEntity(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 521
    const-string v1, "Upnp/AppDbInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter delete entity for package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 523
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "AppDbInterface.deleteEntity() : Context is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 527
    sget-object v2, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    const-string v3, "pkg_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 528
    aput-object p1, v4, v5

    .line 527
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 531
    .local v0, "rows":I
    const-string v1, "Upnp/AppDbInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exit delete Entity by deleting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAppId(Ljava/lang/String;)I
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 266
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Enter getAppId of a package"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->dbExists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    :goto_0
    return v7

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 271
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    .line 272
    const-string v3, "packageName=?"

    .line 273
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    .line 271
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v8

    .line 274
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 275
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is null or empty. Exit getAppId of a package with -1 return value"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 283
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 284
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exit getAppId of a package with id  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 286
    .local v7, "appId":I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getAppIdFromPackageName(Ljava/lang/String;)I
    .locals 9
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 387
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getPackageNameFromAppId for packagename "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 389
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface.getAppIdFromPackageName() : Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :goto_0
    return v8

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "rowid"

    aput-object v3, v2, v8

    const-string v3, "packageName=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 393
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 394
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is null or empty. Exit getPackageNameFromAppId with null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 398
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 401
    :cond_2
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "entries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 403
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 404
    .local v8, "res":I
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exit getPackageNameFromAppId with row num "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getApplicationIdentifierFromAcms(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 630
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Enter getApplicationIdentifierFromAcms"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 632
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    :goto_0
    return-object v5

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 636
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    .line 637
    const-string v3, "appId"

    aput-object v3, v2, v8

    .line 638
    const-string v3, "packagename=?"

    new-array v4, v4, [Ljava/lang/String;

    .line 639
    aput-object p1, v4, v8

    .line 635
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 642
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 643
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 646
    :cond_1
    const/4 v6, 0x0

    .line 648
    .local v6, "appId":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 649
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Given package name is present in acms db"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 651
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 656
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 665
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exit getApplicationIdentifierFromAcms. Returning appid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .line 666
    goto :goto_0
.end method

.method public getAppsFromCategory(Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;)Ljava/util/List;
    .locals 10
    .param p1, "category"    # Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/mirrorlink/util/AppDbHelper$Category;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 292
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getAppsFromCategory for category "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 294
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface.getAppsFromCategory() : Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :goto_0
    return-object v5

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 298
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 299
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "packageName"

    aput-object v3, v2, v9

    .line 300
    const-string v3, "category=?"

    .line 301
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    move-object v6, v5

    .line 298
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 303
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 304
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is null or empty. Exit getAppsFromCategory of a category with null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 308
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 311
    :cond_2
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "entries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 313
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 315
    .local v8, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const-string v0, "packageName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 317
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 318
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Exit getAppsFromCategory"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v8

    .line 319
    goto :goto_0
.end method

.method public getCertifiedAppsFromCategory(I)Ljava/util/List;
    .locals 11
    .param p1, "category"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 323
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getCertifiedAppsFromCategory for category "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 325
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface.getCertifiedAppsFromCategory() : Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :goto_0
    return-object v5

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 329
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 330
    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "packageName"

    aput-object v3, v2, v10

    .line 331
    const-string v3, "category=? AND certified =?"

    .line 333
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    .line 334
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    move-object v6, v5

    .line 329
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 335
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 336
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is null or empty. Exit getCertifiedAppsFromCategory of a category with null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 339
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 340
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 343
    :cond_2
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " entries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 345
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 347
    .local v8, "res":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const-string v0, "packageName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 350
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 351
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Exit getCertifiedAppsFromCategory"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v8

    .line 352
    goto :goto_0
.end method

.method public getFilteredCertifiedEntries(Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;)Ljava/util/List;
    .locals 12
    .param p1, "eInfo"    # Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v5, 0x0

    .line 536
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Enter getFilteredCertifiedEntries "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    if-nez p1, :cond_0

    .line 538
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "No information is provided "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    :goto_0
    return-object v5

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 542
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface.getFilteredCertifiedEntries() : Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 551
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 552
    .local v9, "queryParams":Ljava/lang/StringBuilder;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 553
    .local v10, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getAppID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 554
    const-string v0, "certified_appId=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getAppID()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getAppUUID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 558
    const-string v0, "certified_appUUid=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getAppUUID()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 562
    const-string v0, "e_name=? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getTargets()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 566
    const-string v0, "targetList like ? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getTargets()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getRestricted()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 570
    const-string v0, "restricted like ? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getRestricted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 573
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getNonRestricted()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 574
    const-string v0, "non_restricted like ? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getNonRestricted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 577
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getServices()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 578
    const-string v0, "serviceList like ? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getServices()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 581
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getProperties()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 582
    const-string v0, "certified_properties like ? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getProperties()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 585
    :cond_9
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getSignature()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 586
    const-string v0, "certified_signature =? AND "

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getSignature()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 590
    :cond_a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v11, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 591
    .local v3, "selection":Ljava/lang/String;
    new-array v0, v11, [Ljava/lang/String;

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 593
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Selection : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPENTITY_JOIN:Landroid/net/Uri;

    .line 596
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 597
    const-string v6, "pkg_name"

    aput-object v6, v2, v11

    move-object v6, v5

    .line 595
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 600
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_b

    .line 601
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Error in query "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 604
    :cond_b
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_c

    .line 605
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is returned with 0 rows"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 610
    :cond_c
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 611
    .local v8, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 613
    :cond_d
    const-string v0, "pkg_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 615
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 617
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 619
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Exit getFilteredCertifiedEntries"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v8

    .line 620
    goto/16 :goto_0
.end method

.method public getPackageNameFromAppId(I)Ljava/lang/String;
    .locals 10
    .param p1, "appId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 357
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter getPackageNameFromAppId for appId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 359
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "AppDbInterface.getPackageNameFromAppId() : Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :goto_0
    return-object v5

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 363
    sget-object v1, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    .line 364
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "packageName"

    aput-object v3, v2, v9

    .line 365
    const-string v3, "_id=?"

    .line 366
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    move-object v6, v5

    .line 363
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 368
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 369
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Cursor is null or empty. Exit getPackageNameFromAppId with null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 372
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 373
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 376
    :cond_2
    const-string v0, "Upnp/AppDbInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "entries."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 378
    const-string v0, "packageName"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 379
    .local v8, "res":Ljava/lang/String;
    const-string v0, "Upnp/AppDbInterface"

    const-string v1, "Exit getPackageNameFromAppId"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v8

    .line 381
    goto :goto_0
.end method

.method public insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J
    .locals 8
    .param p1, "info"    # Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .prologue
    const-wide/16 v1, -0x1

    .line 63
    const-string v5, "Upnp/AppDbInterface"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Enter insert "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v5, :cond_1

    .line 65
    const-string v5, "Upnp/AppDbInterface"

    const-string v6, "AppDbInterface.insert() : mContext equals null "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    :goto_0
    return-wide v1

    .line 69
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 71
    .local v4, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCategory()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 72
    const-string v5, "category"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCategory()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 75
    const-string v5, "packageName"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->isAppCertified()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 78
    const-string v5, "certified"

    const-string v6, "1"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCertifiedAppId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 83
    const-string v5, "certified_appId"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCertifiedAppId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCertifiedAppUUID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 86
    const-string v5, "certified_appUUid"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getCertifiedAppUUID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getProperties()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 89
    const-string v5, "certified_properties"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getProperties()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getSignature()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 92
    const-string v5, "certified_signature"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getSignature()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->isAppCertified()Z

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->updateIfAppInfoExists(Landroid/content/ContentValues;Z)I

    move-result v0

    .line 95
    .local v0, "result":I
    if-nez v0, :cond_9

    .line 96
    const-string v5, "Upnp/AppDbInterface"

    const-string v6, "AppDbInterface.insert() : App is updated due to certifed state change"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-wide/16 v1, 0x0

    goto/16 :goto_0

    .line 80
    .end local v0    # "result":I
    :cond_8
    const-string v5, "certified"

    const-string v6, "0"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 99
    .restart local v0    # "result":I
    :cond_9
    const/4 v5, 0x2

    if-ne v0, v5, :cond_0

    .line 100
    const-string v5, "Upnp/AppDbInterface"

    const-string v6, "AppDbInterface.insert() : values are not updated. Hence inserting to db"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-wide/16 v1, -0x1

    .line 103
    .local v1, "ret":J
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_APPDATA:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 104
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_a

    .line 105
    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getIdFromURI(Landroid/net/Uri;)I

    move-result v5

    int-to-long v1, v5

    .line 106
    const/4 v3, 0x0

    .line 107
    goto/16 :goto_0

    .line 109
    :cond_a
    const-string v5, "Upnp/AppDbInterface"

    const-string v6, "AppDbInterface.insert() : uri is null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public declared-synchronized insert(Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;)V
    .locals 4
    .param p1, "eInfo"    # Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;

    .prologue
    .line 432
    monitor-enter p0

    :try_start_0
    const-string v1, "Upnp/AppDbInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Enter entity insert for package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 433
    const-string v3, "And entity name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 432
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 435
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "AppDbInterface.insert() : Context is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    :goto_0
    monitor-exit p0

    return-void

    .line 438
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 439
    .local v0, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 440
    const-string v1, "pkg_name"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 446
    const-string v1, "e_name"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getTargets()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 452
    const-string v1, "targetList"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getTargets()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getRestricted()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 455
    const-string v1, "restricted"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getRestricted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getNonRestricted()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 458
    const-string v1, "non_restricted"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getNonRestricted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getServices()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 461
    const-string v1, "serviceList"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getServices()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->updateEntityInfoIfExists(Landroid/content/ContentValues;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 465
    const-string v1, "pkg_name"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 467
    sget-object v2, Lcom/samsung/android/mirrorlink/util/AppDbProvider;->CONTENT_URI_ENTITYDATA:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 469
    const-string v1, "Upnp/AppDbInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inserted entity "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->getEntityName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    :cond_5
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "Exit Entity insert "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 432
    .end local v0    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 442
    .restart local v0    # "values":Landroid/content/ContentValues;
    :cond_6
    :try_start_2
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "Required field packagename not provided. Hence not inserting"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 448
    :cond_7
    const-string v1, "Upnp/AppDbInterface"

    const-string v2, "Required field entity name not provided. Hence not inserting"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
