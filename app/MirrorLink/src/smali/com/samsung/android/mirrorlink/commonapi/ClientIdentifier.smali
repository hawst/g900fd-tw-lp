.class public Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;
.super Ljava/lang/Object;
.source "ClientIdentifier.java"


# instance fields
.field private clientId:Ljava/lang/String;

.field private friendlyName:Ljava/lang/String;

.field private manufacturer:Ljava/lang/String;

.field private modelName:Ljava/lang/String;

.field private modelNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->friendlyName:Ljava/lang/String;

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->manufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->modelName:Ljava/lang/String;

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->modelNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setClientId(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->clientId:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setFriendlyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "friendlyName"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->friendlyName:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setManufacturer(Ljava/lang/String;)V
    .locals 0
    .param p1, "manufacturer"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->manufacturer:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0
    .param p1, "modelName"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->modelName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setModelNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "modelNumber"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ClientIdentifier;->modelNumber:Ljava/lang/String;

    .line 69
    return-void
.end method
