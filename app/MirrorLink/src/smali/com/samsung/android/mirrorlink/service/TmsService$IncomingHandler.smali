.class Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;
.super Landroid/os/Handler;
.source "TmsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TmsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TmsService;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/service/TmsService;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 315
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 355
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 357
    :goto_0
    return-void

    .line 318
    :sswitch_0
    const-string v1, "TMSSvc"

    const-string v2, "MSG_REGISTER_CLIENT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    iget-object v2, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    monitor-enter v2

    .line 327
    :try_start_0
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v3, 0x0

    .line 328
    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 327
    invoke-static {v3, v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :goto_1
    :try_start_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 329
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 337
    .end local v0    # "e":Landroid/os/RemoteException;
    :sswitch_1
    const-string v1, "TMSSvc"

    const-string v2, "MSG_UNREGISTER_CLIENT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 344
    :sswitch_2
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 345
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    const/4 v2, 0x0

    .line 346
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget v4, v4, Lcom/samsung/android/mirrorlink/service/TmsService;->mValue:I

    const/4 v5, 0x0

    .line 345
    invoke-static {v2, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 350
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 347
    :catch_1
    move-exception v0

    .line 348
    .restart local v0    # "e":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 350
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 349
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_1
    move-exception v1

    .line 350
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;->this$0:Lcom/samsung/android/mirrorlink/service/TmsService;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 351
    throw v1

    .line 315
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method
