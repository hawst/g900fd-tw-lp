.class Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;
.super Landroid/os/Handler;
.source "AcmsCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcmsHandler"
.end annotation


# static fields
.field private static final IS_CONNECTED:Ljava/lang/String; = "isConnected"

.field private static final IS_CONNECTED_PREF:Ljava/lang/String; = "IsConnectedPref"

.field private static final IS_ZEROQUERY:Ljava/lang/String; = "isZeroQuery"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 130
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 131
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Landroid/os/Looper;Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;-><init>(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Landroid/os/Looper;)V

    return-void
.end method

.method private checkAllAppsRevocation()V
    .locals 9

    .prologue
    .line 264
    const-string v5, "AcmsCore"

    const-string v6, "checkAllAppsRevocation: Enter "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspResponseEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;

    move-result-object v4

    .line 269
    .local v4, "ocspResponseEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;
    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntryInterface;->getOcspresponseEntry()Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;

    move-result-object v3

    .line 271
    .local v3, "ocspResponseEntry":Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;
    if-eqz v3, :cond_1

    .line 272
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/OcspResponseEntry;->getQueryTimeperiod()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_1

    .line 273
    const-string v5, "AcmsCore"

    const-string v6, "Querry period is 0. Check revocation for all apps"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAllApps()Ljava/util/List;

    move-result-object v0

    .line 277
    .local v0, "allAppEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-eqz v0, :cond_1

    .line 278
    const-string v5, "AcmsCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "checkAllAppsRevocation: No of Apps: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 279
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 278
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 300
    .end local v0    # "allAppEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_1
    const-string v5, "AcmsCore"

    const-string v6, "checkAllAppsRevocation: Exit "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    return-void

    .line 280
    .restart local v0    # "allAppEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 282
    .local v1, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v7

    .line 281
    invoke-direct {p0, v1, v6, v7}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->getCerts(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 283
    .local v2, "certs":[Ljava/security/cert/X509Certificate;
    if-eqz v2, :cond_0

    .line 285
    const-string v6, "AcmsCore"

    .line 286
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Checking Revocation for: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 287
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 286
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 284
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v6

    .line 289
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v6

    .line 292
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 291
    invoke-virtual {v6, v7, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private checkForInstalledApps()V
    .locals 9

    .prologue
    .line 195
    const-string v6, "AcmsCore"

    const-string v7, "checkForInstalledApps apps Enter"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 197
    .local v3, "mPkgMngr":Landroid/content/pm/PackageManager;
    const/4 v4, 0x0

    .line 198
    .local v4, "pkges":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "com.mirrorlink.android.service.ACCESS_PERMISSION"

    aput-object v7, v5, v6

    .line 200
    .local v5, "temp":[Ljava/lang/String;
    const/16 v6, 0x1000

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getPackagesHoldingPermissions([Ljava/lang/String;I)Ljava/util/List;

    move-result-object v4

    .line 201
    const-string v6, "AcmsCore"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No of apps with Mirrorlink Access_Permission are "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 220
    const-string v6, "AcmsCore"

    const-string v7, "checkForInstalledApps apps Exit"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-void

    .line 204
    :cond_0
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 205
    .local v2, "info":Landroid/content/pm/PackageInfo;
    iget-object v6, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->isSelfSignedCertPresent(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 207
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v6

    .line 206
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v6

    .line 208
    iget-object v7, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 207
    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->isPkgNamePresent(Ljava/lang/String;)Z

    move-result v6

    .line 208
    if-nez v6, :cond_1

    .line 209
    const-string v6, "AcmsCore"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "pkgName: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 210
    const-string v8, "sending ACMS_APP_INSTALLED"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 209
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->postToAcmsCoreHandler(Ljava/lang/String;I)V

    .line 214
    :cond_1
    const-wide/16 v6, 0x2bc

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method private getCerts(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 3
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "appId"    # Ljava/lang/String;

    .prologue
    .line 306
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 308
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v2

    .line 309
    invoke-virtual {v2, p3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "devId":Ljava/lang/String;
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v2

    .line 311
    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 316
    .end local v1    # "devId":Ljava/lang/String;
    .local v0, "certs":[Ljava/security/cert/X509Certificate;
    :goto_0
    return-object v0

    .line 313
    .end local v0    # "certs":[Ljava/security/cert/X509Certificate;
    :cond_0
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v2

    .line 314
    invoke-virtual {v2, p3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 313
    .restart local v0    # "certs":[Ljava/security/cert/X509Certificate;
    goto :goto_0
.end method

.method private isSelfSignedCertPresent(Ljava/lang/String;)Z
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 225
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 226
    const-string v5, "com.mirrorlink.android.service.ACCESS_PERMISSION"

    .line 225
    invoke-virtual {v4, v5, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 260
    :cond_0
    :goto_0
    return v3

    .line 229
    :cond_1
    const/4 v2, 0x0

    .line 231
    .local v2, "r":Landroid/content/res/Resources;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 232
    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 233
    invoke-virtual {v2}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 234
    .local v0, "am":Landroid/content/res/AssetManager;
    const-string v4, ""

    invoke-virtual {v0, v4}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 235
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->SELF_SIGNED_CERT_FILENAME:Ljava/lang/String;

    .line 234
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 235
    if-eqz v4, :cond_2

    .line 236
    const-string v4, "AcmsCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Self sign certificate exist for pkgName"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 236
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {v2}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 239
    invoke-virtual {v2}, Landroid/content/res/Resources;->finishPreloading()V

    .line 240
    const/4 v3, 0x1

    goto :goto_0

    .line 246
    :cond_2
    const-string v4, "AcmsCore"

    .line 247
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Self sign certificate does not exist for pkgName"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 248
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 247
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 246
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 256
    .end local v0    # "am":Landroid/content/res/AssetManager;
    :goto_1
    if-eqz v2, :cond_0

    .line 257
    invoke-virtual {v2}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 258
    invoke-virtual {v2}, Landroid/content/res/Resources;->finishPreloading()V

    goto :goto_0

    .line 249
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 251
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 252
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    .line 135
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 188
    :cond_0
    :goto_0
    :pswitch_0
    const-string v5, "AcmsCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Decremented Counter Value : AcmsCoreHandler.handleMessage=>"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    .line 190
    .local v0, "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    if-eqz v0, :cond_1

    .line 191
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 192
    :cond_1
    return-void

    .line 137
    .end local v0    # "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    .line 138
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v6

    .line 137
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$1(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V

    goto :goto_0

    .line 142
    :pswitch_2
    const-string v5, "AcmsCore"

    const-string v6, "AcmsCore: ACMS_APP_INSTALLED"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->isSelfSignedCertPresent(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 145
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v5

    .line 144
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v6

    .line 146
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 145
    invoke-virtual {v6, v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->isPkgNamePresent(Ljava/lang/String;)Z

    move-result v5

    .line 146
    if-nez v5, :cond_2

    .line 147
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$2(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v6

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_2
    const-string v5, "AcmsCore"

    const-string v6, "This is NOT a valid MirrorLink app"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 154
    :pswitch_3
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$2(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v6

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleAppRemoved(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_4
    const-string v5, "AcmsCore"

    const-string v6, "MirrorLink Connected!"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "IsConnectedPref"

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 160
    .local v2, "isZeroQueryPref":Landroid/content/SharedPreferences;
    const-string v5, "isZeroQuery"

    invoke-interface {v2, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 161
    const-string v5, "AcmsCore"

    const-string v6, "ZeroQuery received. Check all apps revocation."

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->checkAllAppsRevocation()V

    .line 163
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 164
    .local v4, "zeroquery_editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "isZeroQuery"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 165
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 170
    .end local v2    # "isZeroQueryPref":Landroid/content/SharedPreferences;
    .end local v4    # "zeroquery_editor":Landroid/content/SharedPreferences$Editor;
    :pswitch_5
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mApplicationContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$0(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "IsConnectedPref"

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 171
    .local v3, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v5, "isConnected"

    invoke-interface {v3, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_3

    .line 172
    const-string v5, "AcmsCore"

    const-string v6, "Checking installed apps for the first time"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->checkForInstalledApps()V

    .line 174
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 175
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "isConnected"

    const/4 v6, 0x1

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 176
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 178
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$2(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handlePendingJobs()V

    .line 179
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore$AcmsHandler;->this$0:Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;

    # getter for: Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->mCertificateMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;->access$2(Lcom/samsung/android/mirrorlink/acms/api/AcmsCore;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->handleExpiredApps()V

    goto/16 :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
