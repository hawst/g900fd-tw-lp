.class Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;
.super Ljava/lang/Thread;
.source "DapCertificateProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSGetDevCertThread"


# instance fields
.field private body:Ljava/lang/String;

.field private mCtxt:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->body:Ljava/lang/String;

    .line 238
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->mCtxt:Landroid/content/Context;

    .line 239
    return-void
.end method


# virtual methods
.method public getContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 409
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 410
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1, v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 411
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 412
    .local v4, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 414
    const/4 v3, 0x0

    .line 417
    .local v3, "text":Ljava/lang/String;
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 418
    .local v0, "eventType":I
    :goto_0
    if-ne v0, v6, :cond_0

    .line 438
    const/4 v3, 0x0

    .end local v3    # "text":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 420
    .restart local v3    # "text":Ljava/lang/String;
    :cond_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 421
    .local v2, "tagname":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 436
    :cond_1
    :goto_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 424
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 425
    goto :goto_2

    .line 428
    :pswitch_1
    const-string v5, "content"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public run()V
    .locals 14

    .prologue
    .line 326
    :try_start_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 327
    .local v0, "client":Lorg/apache/http/client/HttpClient;
    new-instance v5, Lorg/apache/http/client/methods/HttpPost;

    .line 328
    const-string v11, "https://eu-mlca.samsungosp.com/v2/ca/devicecert"

    .line 327
    invoke-direct {v5, v11}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 330
    .local v5, "post":Lorg/apache/http/client/methods/HttpPost;
    const-string v11, "x-osp-appId"

    const-string v12, "2095nt95l7"

    invoke-virtual {v5, v11, v12}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v11, "Authorization"

    const-string v12, "Basic MjA5NW50OTVsNzo2NjNBRDkzOTc3MDg1QzQyNjg2QzUzRUY0QzlCNjcxNw=="

    invoke-virtual {v5, v11, v12}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->body:Ljava/lang/String;

    invoke-direct {v3, v11}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 334
    .local v3, "input":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v5, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 335
    const-string v11, "text/xml"

    invoke-virtual {v3, v11}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 336
    invoke-interface {v0, v5}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 338
    .local v8, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    .line 339
    .local v10, "statusLine":Lorg/apache/http/StatusLine;
    invoke-interface {v10}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    .line 340
    .local v9, "statusCode":I
    const-string v11, "TMSGetDevCertThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Post Status Code is: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const/16 v11, 0xc8

    if-ne v9, v11, :cond_3

    .line 343
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    .line 344
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v12

    const-string v13, "UTF-8"

    invoke-direct {v11, v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 343
    invoke-direct {v6, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 345
    .local v6, "rd":Ljava/io/BufferedReader;
    const-string v4, ""

    .line 346
    .local v4, "line":Ljava/lang/String;
    const-string v7, ""

    .line 347
    .local v7, "readLine":Ljava/lang/String;
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 351
    invoke-virtual {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->getContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "content":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    if-eqz v11, :cond_2

    if-eqz v1, :cond_2

    .line 356
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    const-string v12, "UTF-8"

    invoke-virtual {v1, v12}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v12

    .line 357
    const-string v13, "UTF-8"

    invoke-virtual {v1, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    array-length v13, v13

    .line 356
    invoke-virtual {v11, v12, v13}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendGetDevCertResponse([BI)V

    .line 405
    .end local v0    # "client":Lorg/apache/http/client/HttpClient;
    .end local v1    # "content":Ljava/lang/String;
    .end local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v6    # "rd":Ljava/io/BufferedReader;
    .end local v7    # "readLine":Ljava/lang/String;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "statusCode":I
    .end local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_0
    :goto_1
    return-void

    .line 349
    .restart local v0    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v6    # "rd":Ljava/io/BufferedReader;
    .restart local v7    # "readLine":Ljava/lang/String;
    .restart local v8    # "response":Lorg/apache/http/HttpResponse;
    .restart local v9    # "statusCode":I
    .restart local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_1
    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 359
    .restart local v1    # "content":Ljava/lang/String;
    :cond_2
    const-string v11, "TMSGetDevCertThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Post Content is "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 381
    .end local v0    # "client":Lorg/apache/http/client/HttpClient;
    .end local v1    # "content":Ljava/lang/String;
    .end local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v6    # "rd":Ljava/io/BufferedReader;
    .end local v7    # "readLine":Ljava/lang/String;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "statusCode":I
    .end local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_0
    move-exception v2

    .line 383
    .local v2, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v2}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 384
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 386
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V

    goto :goto_1

    .line 364
    .end local v2    # "e":Lorg/apache/http/client/ClientProtocolException;
    .restart local v0    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .restart local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v8    # "response":Lorg/apache/http/HttpResponse;
    .restart local v9    # "statusCode":I
    .restart local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_3
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    .line 365
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v12

    const-string v13, "UTF-8"

    invoke-direct {v11, v12, v13}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 364
    invoke-direct {v6, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 366
    .restart local v6    # "rd":Ljava/io/BufferedReader;
    const-string v4, ""

    .line 367
    .restart local v4    # "line":Ljava/lang/String;
    const-string v7, ""

    .line 368
    .restart local v7    # "readLine":Ljava/lang/String;
    :goto_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 372
    invoke-virtual {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->getContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 374
    .restart local v1    # "content":Ljava/lang/String;
    const-string v11, "TMSGetDevCertThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Post Content is "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 377
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 388
    .end local v0    # "client":Lorg/apache/http/client/HttpClient;
    .end local v1    # "content":Ljava/lang/String;
    .end local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v6    # "rd":Ljava/io/BufferedReader;
    .end local v7    # "readLine":Ljava/lang/String;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "statusCode":I
    .end local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_1
    move-exception v2

    .line 390
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 391
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 393
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V

    goto/16 :goto_1

    .line 369
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v6    # "rd":Ljava/io/BufferedReader;
    .restart local v7    # "readLine":Ljava/lang/String;
    .restart local v8    # "response":Lorg/apache/http/HttpResponse;
    .restart local v9    # "statusCode":I
    .restart local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_4
    :try_start_2
    const-string v11, "TMSGetDevCertThread"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Post Response is :"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v7

    goto :goto_2

    .line 395
    .end local v0    # "client":Lorg/apache/http/client/HttpClient;
    .end local v3    # "input":Lorg/apache/http/entity/StringEntity;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v6    # "rd":Ljava/io/BufferedReader;
    .end local v7    # "readLine":Ljava/lang/String;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "statusCode":I
    .end local v10    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_2
    move-exception v2

    .line 397
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 398
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 400
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V

    goto/16 :goto_1
.end method

.method public setQueryString(Ljava/lang/String;)V
    .locals 1
    .param p1, "queryString"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/GetDevCertThread;->body:Ljava/lang/String;

    .line 246
    return-void
.end method
