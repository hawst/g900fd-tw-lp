.class Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;
.super Landroid/os/Handler;
.source "TmsSvcManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    .line 187
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 189
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcMainThread.handleMessage() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 271
    :cond_0
    :goto_0
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcMainThread.handleMessage() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    return-void

    .line 194
    :pswitch_0
    const-string v1, "TMSSvc"

    const-string v2, "mLooperHandler : TM_INIT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    if-nez v1, :cond_1

    .line 196
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.init() -New Engine Init"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    new-instance v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v3

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCxt:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->looper:Landroid/os/Looper;
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$2(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 198
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v2

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setAppStateHandler(Landroid/os/Handler;)V

    .line 199
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->init()I

    .line 200
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$2(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)V

    .line 201
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    goto :goto_0

    .line 205
    :cond_1
    const-string v1, "TMSSvc"

    const-string v2, "TmsSvcManager.init() -Old Engine"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->init()I

    goto/16 :goto_0

    .line 211
    :pswitch_1
    const-string v1, "TMSSvc"

    const-string v2, "mLooperHandler : TM_DEINIT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 214
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$0(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->deinit()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 216
    const-string v1, "TMSSvc"

    const-string v2, "Successfully deinited"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$2(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 218
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 219
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 233
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 234
    const-string v1, "TMSSvc"

    const-string v2, "Sending Destroy service message"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 236
    .local v0, "msg1":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 237
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    .end local v0    # "msg1":Landroid/os/Message;
    :cond_3
    const-string v1, "TMSSvc"

    const-string v2, "Successfully called TMDestroyService"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    :cond_4
    const-string v1, "TMSSvc"

    const-string v2, "mLooperHandler : TM_DEINIT - mTmsEngine.deinit() return false"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 260
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 261
    const-string v1, "TMSSvc"

    const-string v2, "Sending Destroy service message"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 263
    .restart local v0    # "msg1":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 264
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread$1;->this$1:Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->this$0:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;->access$3(Lcom/samsung/android/mirrorlink/service/TmsSvcManager$TmsSvcMainThread;)Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    move-result-object v1

    # getter for: Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->access$1(Lcom/samsung/android/mirrorlink/service/TmsSvcManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
