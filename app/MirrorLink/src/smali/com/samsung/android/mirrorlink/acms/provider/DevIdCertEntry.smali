.class public Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;
.super Ljava/lang/Object;
.source "DevIdCertEntry.java"


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mClientIds:Ljava/lang/String;

.field private mDevId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getClientIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mClientIds:Ljava/lang/String;

    return-object v0
.end method

.method public getDevId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mDevId:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mAppId:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setClientIds(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientIds"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mClientIds:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setDevId(Ljava/lang/String;)V
    .locals 0
    .param p1, "devId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->mDevId:Ljava/lang/String;

    .line 35
    return-void
.end method
