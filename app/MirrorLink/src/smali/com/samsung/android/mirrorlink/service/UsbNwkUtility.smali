.class public Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;
.super Ljava/lang/Object;
.source "UsbNwkUtility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSSvc"

.field public static final USB_FILE_PATH:Ljava/lang/String; = "/sys/class/android_usb/android0/terminal_version"


# instance fields
.field private ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

.field private EXTRA_ACTIVE_TETHER:Ljava/lang/String;

.field private TETHER_ERROR_NO_ERROR:I

.field private mCm:Landroid/net/ConnectivityManager;

.field private mCmClass:Ljava/lang/Class;

.field private mCntxt:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mTetherActive:Z

.field private mTetherEventRecvr:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "cntxt"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherActive:Z

    .line 33
    iput v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->TETHER_ERROR_NO_ERROR:I

    .line 34
    const-string v1, "android.net.conn.TETHER_STATE_CHANGED"

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    .line 35
    const-string v1, "activeArray"

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;

    .line 44
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.UsbNwkUtility enter "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCntxt:Landroid/content/Context;

    .line 46
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 47
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCntxt:Landroid/content/Context;

    .line 50
    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    .line 51
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    .line 53
    new-instance v1, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;-><init>(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherEventRecvr:Landroid/content/BroadcastReceiver;

    .line 54
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherEventRecvr:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->getTmServerAppVerision()V

    .line 57
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$4()Z
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isNcmReady()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherActive:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->startTMServerService()V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;Z)V
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherActive:Z

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V
    .locals 0

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->stopTMServerService()V

    return-void
.end method

.method private find_usbIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "ifaces"    # [Ljava/lang/String;
    .param p2, "regexes"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 370
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 371
    :cond_0
    const-string v3, "TMSSvc"

    const-string v4, "UsbNwkUtility.find_usbIface() invalid input"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 382
    :cond_1
    :goto_0
    return-object v0

    .line 375
    :cond_2
    array-length v6, p1

    move v5, v4

    :goto_1
    if-lt v5, v6, :cond_3

    move-object v0, v2

    .line 382
    goto :goto_0

    .line 375
    :cond_3
    aget-object v0, p1, v5

    .line 376
    .local v0, "iface":Ljava/lang/String;
    array-length v7, p2

    move v3, v4

    :goto_2
    if-lt v3, v7, :cond_4

    .line 375
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 376
    :cond_4
    aget-object v1, p2, v3

    .line 377
    .local v1, "regex":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 376
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static isNcmReady()Z
    .locals 13

    .prologue
    .line 103
    const-string v10, "TMSSvc"

    const-string v11, "UsbNwkUtility.isNcmReady - Enter"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x0

    .line 107
    .local v0, "bNcmReady":Z
    const/4 v4, 0x0

    .line 112
    .local v4, "fin":Ljava/io/FileInputStream;
    new-instance v7, Ljava/lang/StringBuffer;

    const-string v10, ""

    invoke-direct {v7, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 114
    .local v7, "strContent":Ljava/lang/StringBuffer;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    const-string v10, "/sys/class/android_usb/android0/terminal_version"

    invoke-direct {v5, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 115
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .local v5, "fin":Ljava/io/FileInputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/FileInputStream;->read()I

    move-result v1

    .local v1, "ch":I
    const/4 v10, -0x1

    if-ne v1, v10, :cond_2

    .line 118
    const-string v10, "TMSSvc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility.isNcmReady() usb file content is  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 118
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 121
    .local v2, "content":Ljava/lang/String;
    new-instance v8, Ljava/util/StringTokenizer;

    const-string v10, " "

    invoke-direct {v8, v2, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .local v8, "tokenizer":Ljava/util/StringTokenizer;
    :cond_0
    :goto_1
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->hasMoreElements()Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result v10

    if-nez v10, :cond_3

    move-object v4, v5

    .line 154
    .end local v1    # "ch":I
    .end local v2    # "content":Ljava/lang/String;
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "tokenizer":Ljava/util/StringTokenizer;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :goto_2
    if-eqz v4, :cond_1

    .line 155
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 160
    :cond_1
    :goto_3
    const-string v10, "TMSSvc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility.isNcmReady() Exit ,bNcmReady-  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 160
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return v0

    .line 116
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "ch":I
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :cond_2
    int-to-char v10, v1

    :try_start_3
    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 135
    .end local v1    # "ch":I
    :catch_0
    move-exception v3

    move-object v4, v5

    .line 136
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :goto_4
    const-string v10, "TMSSvc"

    .line 137
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility.isNcmReady() FileNotFoundException for not opening usb file"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 137
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 136
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 124
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v1    # "ch":I
    .restart local v2    # "content":Ljava/lang/String;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "tokenizer":Ljava/util/StringTokenizer;
    :cond_3
    :try_start_4
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 125
    .local v9, "val":Ljava/lang/String;
    const-string v10, "major"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 126
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 127
    .local v6, "majorVal":Ljava/lang/String;
    const-string v10, "1"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 128
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 129
    const-string v10, "minor"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v10

    if-eqz v10, :cond_0

    .line 130
    const/4 v0, 0x1

    goto :goto_1

    .line 140
    .end local v1    # "ch":I
    .end local v2    # "content":Ljava/lang/String;
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "majorVal":Ljava/lang/String;
    .end local v8    # "tokenizer":Ljava/util/StringTokenizer;
    .end local v9    # "val":Ljava/lang/String;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 141
    .local v3, "e":Ljava/io/IOException;
    :goto_5
    const-string v10, "TMSSvc"

    .line 142
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility.isNcmReady() IOException for not opening usb file"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 142
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 141
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 156
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 157
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "TMSSvc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility.isNcmReady() file close exception: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 157
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 140
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    :catch_3
    move-exception v3

    move-object v4, v5

    .end local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    goto :goto_5

    .line 135
    :catch_4
    move-exception v3

    goto :goto_4
.end method

.method private isUsbTethered()Z
    .locals 9

    .prologue
    .line 216
    const-string v6, "TMSSvc"

    const-string v7, "UsbNwkUtility.isUsbTethered - Enter"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const/4 v1, 0x0

    .line 222
    .local v1, "bTethered":Z
    const/4 v5, 0x0

    .line 223
    .local v5, "regexes":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 226
    .local v3, "ifaces":[Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    const-string v7, "getTetherableUsbRegexs"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 227
    .local v4, "listener":Ljava/lang/reflect/Method;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, [Ljava/lang/String;

    move-object v5, v0

    .line 235
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    const-string v7, "getTetheredIfaces"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 236
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, [Ljava/lang/String;

    move-object v3, v0

    .line 243
    invoke-direct {p0, v3, v5}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->find_usbIface([Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    if-eqz v6, :cond_0

    .line 244
    const/4 v1, 0x1

    .line 255
    .end local v4    # "listener":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    const-string v6, "TMSSvc"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.isUsbTethered() Exit, bTethered-  "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 256
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 255
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    return v1

    .line 246
    :catch_0
    move-exception v2

    .line 247
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    const-string v6, "TMSSvc"

    const-string v7, "UsbNwkUtility.isUsbTethered() NoSuchMethodException"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 248
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .line 249
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v6, "TMSSvc"

    .line 250
    const-string v7, "UsbNwkUtility.isUsbTethered() InvocationTargetException"

    .line 249
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 251
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v2

    .line 252
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v6, "TMSSvc"

    .line 253
    const-string v7, "UsbNwkUtility.isUsbTethered() IllegalAccessException"

    .line 252
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setNcmReady(ZZ)V
    .locals 8
    .param p0, "bEnableNCM"    # Z
    .param p1, "bOverwrite"    # Z

    .prologue
    .line 169
    const-string v5, "TMSSvc"

    const-string v6, "UsbNwkUtility.setNcmReady - Enter"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const/4 v3, 0x0

    .line 173
    .local v3, "outStream":Ljava/io/PrintWriter;
    :try_start_0
    const-string v2, "/sys/class/android_usb/android0/terminal_version"

    .line 174
    .local v2, "file_path":Ljava/lang/String;
    const-string v5, "TMSSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UsbNwkUtility.setNcmReady() bOverwrite -  "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 174
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    if-nez p1, :cond_0

    invoke-static {}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isNcmReady()Z

    move-result v5

    if-nez v5, :cond_2

    .line 178
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 179
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 180
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 181
    .restart local v1    # "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 182
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 183
    const-string v5, "TMSSvc"

    const-string v6, "New file created"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_1
    :goto_0
    new-instance v4, Ljava/io/PrintWriter;

    const-string v5, "UTF-8"

    invoke-direct {v4, v2, v5}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v3    # "outStream":Ljava/io/PrintWriter;
    .local v4, "outStream":Ljava/io/PrintWriter;
    if-eqz p0, :cond_5

    .line 193
    :try_start_1
    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 197
    :goto_1
    const-string v5, "TMSSvc"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UsbNwkUtility.setNcmReady: bEnableNCM- "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " updated successfully"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 197
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 206
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "file_path":Ljava/lang/String;
    .end local v4    # "outStream":Ljava/io/PrintWriter;
    .restart local v3    # "outStream":Ljava/io/PrintWriter;
    :cond_2
    :goto_2
    if-eqz v3, :cond_3

    .line 207
    invoke-virtual {v3}, Ljava/io/PrintWriter;->close()V

    .line 208
    :cond_3
    const-string v5, "TMSSvc"

    const-string v6, "UsbNwkUtility.setNcmReady - exit"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void

    .line 185
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "file_path":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v5, "TMSSvc"

    .line 186
    const-string v6, "File already exist. Hence, not created"

    .line 185
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 200
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "file_path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 202
    const-string v5, "TMSSvc"

    .line 203
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UsbNwkUtility.setNcmReady Could not update file content: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 203
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 202
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 195
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "outStream":Ljava/io/PrintWriter;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "file_path":Ljava/lang/String;
    .restart local v4    # "outStream":Ljava/io/PrintWriter;
    :cond_5
    :try_start_3
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 200
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "outStream":Ljava/io/PrintWriter;
    .restart local v3    # "outStream":Ljava/io/PrintWriter;
    goto :goto_3
.end method

.method private startTMServerService()V
    .locals 3

    .prologue
    .line 386
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.startTMServerService - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 389
    const-string v1, "TMSSvc"

    const-string v2, "Sending message TM_INIT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 391
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 392
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 395
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.startTMServerService - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method private stopTMServerService()V
    .locals 3

    .prologue
    .line 399
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.stopTMServerService - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 402
    const-string v1, "TMSSvc"

    const-string v2, "Sending message TM_DEINIT"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 404
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 405
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 407
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherEventRecvr:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 408
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherEventRecvr:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 409
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherEventRecvr:Landroid/content/BroadcastReceiver;

    .line 411
    :cond_1
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.stopTMServerService - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    return-void
.end method

.method private tetherUsb(ZZ)Z
    .locals 11
    .param p1, "bTethered"    # Z
    .param p2, "bNcmReady"    # Z

    .prologue
    const/4 v9, 0x1

    .line 266
    const/4 v4, 0x0

    .line 267
    .local v4, "result":Z
    const/4 v0, 0x0

    .line 269
    .local v0, "bStartService":Z
    const-string v6, "TMSSvc"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb() Enter , bTethered-  "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 270
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  bNcmReady-  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 269
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 278
    const/4 v0, 0x1

    .line 280
    :cond_0
    const-string v6, "TMSSvc"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb() bStartService-  "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 280
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    if-eqz v0, :cond_1

    .line 284
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->startTMServerService()V

    .line 285
    const/4 v4, 0x1

    .line 341
    :goto_0
    const-string v6, "TMSSvc"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb() exit result = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return v4

    .line 287
    :cond_1
    const/4 v5, 0x0

    .line 289
    .local v5, "ret":Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isUsbConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 290
    invoke-static {v9, p2}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setNcmReady(ZZ)V

    .line 292
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    const-string v7, "setNcmTethering"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    .line 293
    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    .line 292
    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 294
    .local v3, "listener":Ljava/lang/reflect/Method;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    .line 320
    .end local v3    # "listener":Ljava/lang/reflect/Method;
    .end local v5    # "ret":Ljava/lang/Object;
    :goto_1
    const/4 v2, -0x1

    .line 321
    .local v2, "intRet":I
    if-eqz v5, :cond_2

    .line 322
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 324
    :cond_2
    iget v6, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->TETHER_ERROR_NO_ERROR:I

    if-eq v2, v6, :cond_3

    .line 325
    const-string v6, "TMSSvc"

    const-string v7, "tehtering mode  FAIL"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const/4 v4, 0x0

    .line 327
    goto :goto_0

    .line 295
    .end local v2    # "intRet":I
    .restart local v5    # "ret":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 297
    const-string v6, "TMSSvc"

    .line 298
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb Could not connect to channel."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 299
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 298
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 297
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const/4 v4, 0x0

    goto :goto_1

    .line 301
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 302
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 303
    const-string v6, "TMSSvc"

    .line 304
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb Could not connect to channel."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 305
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 304
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 303
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v4, 0x0

    goto :goto_1

    .line 307
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v1

    .line 308
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 309
    const-string v6, "TMSSvc"

    .line 310
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb Could not connect to channel."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 311
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 310
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 309
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const/4 v4, 0x0

    goto :goto_1

    .line 313
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 314
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 315
    const-string v6, "TMSSvc"

    .line 316
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "UsbNwkUtility.tetherUsb Could not connect to channel."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 317
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 316
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 315
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v4, 0x0

    goto :goto_1

    .line 329
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v5    # "ret":Ljava/lang/Object;
    .restart local v2    # "intRet":I
    :cond_3
    const-string v6, "TMSSvc"

    .line 330
    const-string v7, "UsbNwkUtility.tetherUsb  tehtering mode called successfully.Check the tethering state changed intent..."

    .line 328
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const/4 v4, 0x1

    .line 333
    goto/16 :goto_0

    .line 335
    .end local v2    # "intRet":I
    .restart local v5    # "ret":Ljava/lang/Object;
    :cond_4
    const-string v6, "TMSSvc"

    .line 336
    const-string v7, "UsbNwkUtility.tetherUsb() setNcmTethering is not called, because usb disconnected"

    .line 334
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v6, "TMSSvc"

    const-string v7, "Usb is not connected. Stopping the service"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->stopTMServerService()V

    goto/16 :goto_0
.end method


# virtual methods
.method public doTether()Z
    .locals 3

    .prologue
    .line 60
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.doTether enter "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v0, 0x0

    .line 62
    .local v0, "ret":Z
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isUsbConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isUsbTethered()Z

    move-result v1

    invoke-static {}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isNcmReady()Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->tetherUsb(ZZ)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v1, "TMSSvc"

    const-string v2, "Usb is not connected. Stopping the service"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->stopTMServerService()V

    .line 67
    const-string v1, "TMSSvc"

    const-string v2, "UsbNwkUtility.doTether - USB is not connected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public doUnTether()Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 73
    const-string v4, "TMSSvc"

    const-string v5, "UsbNwkUtility.doUnTether enter "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isUsbConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;

    const-string v5, "setNcmTethering"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    .line 78
    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    .line 77
    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 79
    .local v1, "listener":Ljava/lang/reflect/Method;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setNcmReady(ZZ)V

    .line 98
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 83
    :cond_0
    const-string v2, "TMSSvc"

    .line 84
    const-string v4, "UsbNwkUtility.tetherUsb() setNcmTethering is not called - USB is not connected"

    .line 82
    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move v2, v3

    .line 85
    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UsbNwkUtility.doUnTether error."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 92
    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UsbNwkUtility.doUnTether error."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 95
    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UsbNwkUtility.doUnTether error."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 98
    goto :goto_0
.end method

.method public getTmServerAppVerision()V
    .locals 5

    .prologue
    .line 524
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCntxt:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.samsung.android.app.mirrorlink"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 525
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    const-string v2, "TMSSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TmServerApp/ VersionName: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v2, "TMSSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TmServerApp/ VersionCode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    .end local v1    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "TMSSvc"

    const-string v3, "TmServerApp is not present"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isUsbConnected()Z
    .locals 6

    .prologue
    .line 347
    const/4 v0, 0x0

    .line 348
    .local v0, "connected":Z
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 349
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 351
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCntxt:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 352
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 353
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "connected"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 354
    const-string v3, "TMSSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "android.hardware.usb.action.USB_STATE: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    :goto_0
    return v0

    .line 356
    :cond_0
    const-string v3, "TMSSvc"

    .line 357
    const-string v4, "android.hardware.usb.action.USB_STATE is not present"

    .line 356
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStateHandler(Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;)V
    .locals 0
    .param p1, "appStateHandler"    # Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    .prologue
    .line 519
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mHandler:Landroid/os/Handler;

    .line 520
    return-void
.end method
