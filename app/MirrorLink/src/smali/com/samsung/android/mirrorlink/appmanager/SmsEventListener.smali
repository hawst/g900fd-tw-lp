.class Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;
.super Landroid/content/BroadcastReceiver;
.source "MessageAppEventListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field public mMessageAppEventListener:Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;)V
    .locals 2
    .param p1, "messageAppEventListener"    # Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;

    .prologue
    .line 170
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 171
    const-string v0, "TMSAppMngr"

    const-string v1, "SmsEventListener.SmsEventListener enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;->mMessageAppEventListener:Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;

    .line 174
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 177
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SmsEventListener.onReceive intent.getAction() = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 177
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v5, "android.provider.Telephony.SMS_RECEIVED"

    .line 180
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 181
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 182
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 184
    .local v2, "callid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 186
    .local v4, "smsBody":Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 187
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sput-object v5, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->mCurrentNotifications:Ljava/util/ArrayList;

    .line 190
    :cond_0
    if-eqz v1, :cond_2

    .line 196
    const-string v5, "TMSAppMngr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SmsEventListener.onReceive callid="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;->mMessageAppEventListener:Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;

    .line 203
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 204
    .local v3, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 205
    .local v0, "bndl":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 206
    const-string v2, "unknown"

    .line 207
    :cond_1
    const-string v5, "callid"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v5, "smsbody"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 210
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;->mMessageAppEventListener:Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;

    invoke-virtual {v5, v3}, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->sendMessage(Landroid/os/Message;)Z

    .line 215
    .end local v0    # "bndl":Landroid/os/Bundle;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "callid":Ljava/lang/String;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "smsBody":Ljava/lang/String;
    :cond_2
    return-void
.end method
