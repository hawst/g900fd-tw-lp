.class public Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
.super Ljava/lang/Object;
.source "UserAppHolder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;,
        Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;
    }
.end annotation


# static fields
.field public static final APPSTATUS_DESTROYED:I = 0x4

.field public static final APPSTATUS_INITIALIZING:I = 0x1

.field public static final APPSTATUS_PAUSED:I = 0x2

.field public static final APPSTATUS_STOPPED:I = 0x3

.field private static final DEFAULT_HOLDER:Ljava/lang/String; = "defaultholder"

.field private static final DESTROYED:Ljava/lang/String; = "DESTROYED"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSUserAppHolder"

.field private static final NEW_APP_STATUS:Ljava/lang/String; = "newappStatus"

.field private static final OLD_APP_STATUS:Ljava/lang/String; = "oldappStatus"

.field private static final PACKAGE_NAMES:Ljava/lang/String; = "PackageNames"

.field private static final PAUSED:Ljava/lang/String; = "PAUSED"

.field private static final RESUMED:Ljava/lang/String; = "RESUMED"

.field private static final STOPPING:Ljava/lang/String; = "STOPPING"

.field private static mActivtyMngr:Landroid/app/ActivityManager;


# instance fields
.field private mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

.field mActivityStateHandler:Landroid/os/Handler;

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppListStatusCb:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

.field private mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

.field private mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

.field private mCntxt:Landroid/content/Context;

.field private mCurrentTopApp:Ljava/lang/String;

.field private mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

.field private mFirstStartAppReq:Z

.field mHandler:Landroid/os/Handler;

.field private mLaunchedApps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

.field private mPkgMngr:Landroid/content/pm/PackageManager;

.field private mPrevTopApp:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 98
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    .line 99
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityStateHandler:Landroid/os/Handler;

    .line 100
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPrevTopApp:Ljava/lang/String;

    .line 674
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$1;-><init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    .line 112
    const-string v0, "TMSUserAppHolder"

    const-string v1, "UserAppHolder() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mFirstStartAppReq:Z

    .line 114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mLaunchedApps:Ljava/util/HashSet;

    .line 115
    const-string v0, "TMSUserAppHolder"

    const-string v1, "UserAppHolder() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method static synthetic access$0()Landroid/app/ActivityManager;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPrevTopApp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Landroid/content/pm/ApplicationInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 1

    .prologue
    .line 954
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getNewTMSPkg(Landroid/content/pm/ApplicationInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 855
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->regsiterAppForPkgStatus(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPrevTopApp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;

    return-void
.end method

.method private deregisterCertifiedApps()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAppListMap()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAppListMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 227
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    .line 228
    return-void
.end method

.method private findSpecialApps(Ljava/lang/String;)V
    .locals 10
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 359
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "findSpecialApps enter pkgName = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const/4 v4, 0x0

    .line 363
    .local v4, "scmPkgInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 368
    :goto_0
    if-eqz v4, :cond_3

    .line 370
    const-string v5, "TMSUserAppHolder"

    const-string v6, "findSpecialApps fill up default app info"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 373
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 378
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :goto_1
    iput-object p1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 379
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 380
    .local v2, "dispName":Ljava/lang/String;
    const/16 v5, 0x26

    const/16 v6, 0x5f

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 381
    iget-boolean v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eqz v5, :cond_2

    .line 382
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Other details will already be filled up for certified app "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    if-eqz v5, :cond_0

    .line 384
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    iget-object v6, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    .end local v1    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v2    # "dispName":Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 364
    :catch_0
    move-exception v3

    .line 365
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "findSpecialApps  = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 375
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    .restart local v1    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    goto :goto_1

    .line 388
    .restart local v2    # "dispName":Ljava/lang/String;
    :cond_2
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-direct {p0, v5, v1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->setIconDetails(Landroid/content/pm/ApplicationInfo;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    .line 389
    const-string v5, "VNC"

    invoke-virtual {v1, v5, v9, v9}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 391
    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 392
    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 393
    const-string v5, "application"

    iput-object v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 394
    iput v8, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 396
    const-string v5, "mixed"

    iput-object v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    .line 397
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v6, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 398
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v5, v8}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 399
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v5

    long-to-int v0, v5

    .line 400
    .local v0, "appId":I
    const/4 v5, -0x1

    if-eq v0, v5, :cond_4

    if-eqz v0, :cond_4

    .line 402
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Adding package "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "appId "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 404
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    .end local v0    # "appId":I
    .end local v1    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v2    # "dispName":Ljava/lang/String;
    :cond_3
    :goto_3
    const-string v5, "TMSUserAppHolder"

    const-string v6, "findSpecialApps exit "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 406
    .restart local v0    # "appId":I
    .restart local v1    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .restart local v2    # "dispName":Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v5

    iget-object v6, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    .line 407
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "AppDbInterface.getAppIdFromPackageName app id"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    if-eqz v0, :cond_3

    .line 409
    const-string v5, "TMSUserAppHolder"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Adding package "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "appId "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iput v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 411
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method

.method private getAddedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 339
    .local p1, "updatedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v2, "TMSUserAppHolder"

    const-string v3, "Enter getAddedApps"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 341
    .local v1, "newAddedApps":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_1

    .line 342
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 350
    :cond_1
    const-string v2, "TMSUserAppHolder"

    const-string v3, "Exit getAddedApps"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    return-object v1

    .line 342
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 343
    .local v0, "app":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    const-string v3, "TMSUserAppHolder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "New app found in acms app list."

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method private getNewTMSPkg(Landroid/content/pm/ApplicationInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 4
    .param p1, "newAppInfo"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    const/4 v2, 0x0

    .line 955
    const-string v1, "TMSUserAppHolder"

    const-string v3, "getNewTMSPkg enter"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 957
    .local v0, "crntAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_0

    move-object v0, v2

    .line 971
    .end local v0    # "crntAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :goto_0
    return-object v0

    .line 960
    .restart local v0    # "crntAppInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_0
    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 961
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    .line 962
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAcmsAppsManager()Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    move-result-object v1

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getActivityLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 961
    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 965
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    invoke-virtual {p1, v1}, Landroid/content/pm/ApplicationInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 964
    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 966
    iput-object p0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppHolder:Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;

    .line 969
    const-string v1, "VNC"

    invoke-virtual {v0, v1, v2, v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    const-string v1, "TMSUserAppHolder"

    const-string v2, "getNewTMSPkg exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getRemovedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 294
    .local p1, "updatedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v3, "TMSUserAppHolder"

    const-string v4, "Enter getRemovedApps"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v2, "removedApps":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 298
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 306
    const-string v3, "TMSUserAppHolder"

    const-string v4, "Exit getRemovedApps"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    return-object v2

    .line 299
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 300
    .local v0, "app":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 301
    const-string v3, "com.samsung.android.app.mirrorlink"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 302
    const-string v4, "TMSUserAppHolder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "Removed app found in acms app list."

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ","

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getUpdatedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "updatedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v3, "TMSUserAppHolder"

    const-string v4, "Enter getUpdatedApps"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v2, "updatedApps":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 320
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 332
    const-string v3, "TMSUserAppHolder"

    const-string v4, "Exit getUpdatedApps"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-object v2

    .line 321
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 323
    .local v0, "app":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 324
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 325
    const-string v4, "TMSUserAppHolder"

    .line 326
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "Updated app found in acms app list."

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 325
    invoke-static {v4, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ","

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private goHome()V
    .locals 3

    .prologue
    .line 978
    const-string v1, "TMSUserAppHolder"

    const-string v2, "goHome enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 980
    .local v0, "startMain":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 981
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 982
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 983
    const-string v1, "TMSUserAppHolder"

    const-string v2, "goHome exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    return-void
.end method

.method private registerCertifiedApps()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->init()V

    .line 234
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAppListMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 237
    :cond_1
    return-void
.end method

.method private regsiterAppForPkgStatus(Ljava/lang/String;)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 856
    const-string v2, "TMSUserAppHolder"

    .line 857
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " UserAppHolder.regsiterAppForPkgStatus enter pkgName = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 858
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 857
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 856
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    if-nez p1, :cond_0

    .line 861
    const-string v2, "TMSUserAppHolder"

    .line 862
    const-string v3, "regsiterAppForPkgStatus Invalid packname"

    .line 861
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    :goto_0
    return-void

    .line 869
    :cond_0
    :try_start_0
    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 870
    const-string v3, "regsiterAppStatusEvent"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    .line 869
    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 871
    .local v1, "listener":Ljava/lang/reflect/Method;
    sget-object v2, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 880
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_1
    const-string v2, "TMSUserAppHolder"

    const-string v3, "regsiterAppForPkgStatus exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 873
    :catch_0
    move-exception v0

    .line 874
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSUserAppHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to channel."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 875
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 876
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSUserAppHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to channel."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 877
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 878
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSUserAppHolder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to channel."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setIconDetails(Landroid/content/pm/ApplicationInfo;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 3
    .param p1, "androidAppInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 824
    const-string v1, "TMSUserAppHolder"

    const-string v2, "setIconDetails enter "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;-><init>()V

    .line 826
    .local v0, "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    .line 827
    const-string v1, "image/png"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    .line 828
    iget-object v1, p2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 829
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    .line 831
    :cond_0
    iget-object v1, p2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 832
    const-string v1, "TMSUserAppHolder"

    .line 833
    const-string v2, "setIconDetails exit "

    .line 832
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    return-void
.end method

.method private updateAppLaunched(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 987
    const-string v0, "TMSUserAppHolder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateAppLaunched enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mLaunchedApps:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 989
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->setVNCenable(Z)V

    .line 991
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mLaunchedApps:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 992
    const-string v0, "TMSUserAppHolder"

    const-string v1, "updateAppLaunched exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    return-void
.end method

.method private updateAppTerminated(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 996
    const-string v0, "TMSUserAppHolder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateAppTerminated enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mLaunchedApps:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 999
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mLaunchedApps:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->setVNCenable(Z)V

    .line 1002
    :cond_0
    const-string v0, "TMSUserAppHolder"

    const-string v1, "updateAppTerminated exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    return-void
.end method


# virtual methods
.method public deinit()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    const-string v0, "TMSUserAppHolder"

    const-string v1, "deinit() enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->stopThread()V

    .line 190
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    .line 191
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 193
    sput-object v2, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 194
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    .line 196
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAcmsAppsManager()Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->deRegisterCallback()V

    .line 197
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->deinit()Z

    .line 198
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->deregisterCertifiedApps()V

    .line 199
    const-string v0, "TMSUserAppHolder"

    const-string v1, "deinit() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public fillAppStatus()V
    .locals 11

    .prologue
    .line 889
    const-string v8, "TMSUserAppHolder"

    const-string v9, " UserAppHolder.getRunningApps enter "

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 892
    .local v6, "runningPackages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 893
    invoke-virtual {v8}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    .line 894
    .local v5, "runnigAppProcessinfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v5, :cond_0

    .line 895
    const-string v8, "TMSUserAppHolder"

    const-string v9, " UserAppHolder.getRunningApps is null"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    :goto_0
    return-void

    .line 898
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-lt v0, v8, :cond_1

    .line 934
    const-string v8, "TMSUserAppHolder"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getRunningApps :SIZE "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 935
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 934
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    const-string v8, "TMSUserAppHolder"

    const-string v9, " UserAppHolder.getRunningApps exit "

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 901
    :cond_1
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 902
    .local v4, "runAppProInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v8, v8

    if-lt v1, v8, :cond_2

    .line 898
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 904
    :cond_2
    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v3, v8, v1

    .line 906
    .local v3, "packName":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    if-lt v2, v8, :cond_3

    .line 902
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 908
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 910
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 915
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v9, 0x15

    iput v9, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 916
    sget-object v8, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v7

    .line 918
    .local v7, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v7, :cond_4

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 919
    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 921
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v9, 0x14

    iput v9, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    .line 927
    :cond_4
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 906
    .end local v7    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public getAppList()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    const-string v1, "TMSUserAppHolder"

    const-string v2, "getAppList enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    .line 212
    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 214
    .local v0, "deviceAppInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    const-string v1, "com.samsung.android.app.mirrorlink"

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->findSpecialApps(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->fillAppStatus()V

    .line 217
    const-string v1, "TMSUserAppHolder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " UserAppHolder.getAppList size installed = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size Acs ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 217
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v1, "TMSUserAppHolder"

    const-string v2, "getAppList exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    return-object v1
.end method

.method public getAppStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 634
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
    .locals 5
    .param p1, "appmngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "cntxt"    # Landroid/content/Context;

    .prologue
    .line 128
    const-string v1, "TMSUserAppHolder"

    const-string v2, "init() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    .line 131
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 132
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->registerCertifiedApps()V

    .line 137
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 138
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    .line 139
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    .line 140
    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 139
    check-cast v1, Landroid/app/ActivityManager;

    sput-object v1, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 141
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;-><init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    .line 142
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$ActivityStateHandler;-><init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityStateHandler:Landroid/os/Handler;

    .line 143
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityStateHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    .line 144
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .line 148
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 149
    .local v0, "pkgStatusIntenttFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 150
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 151
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPackageStatusReceiver:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 156
    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$2;-><init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppListStatusCb:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    .line 173
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const-string v2, "defaultholder"

    invoke-virtual {v1, v2, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 176
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAcmsAppsManager()Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppListStatusCb:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->registerCallback(Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;)V

    .line 179
    const-string v1, "TMSUserAppHolder"

    const-string v2, "init() exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const/4 v1, 0x1

    return v1
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
    .locals 2
    .param p1, "appStatusListener"    # Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .prologue
    .line 844
    const-string v0, "TMSUserAppHolder"

    const-string v1, "regAppStatusEventListener enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppStatusListener:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .line 847
    const-string v0, "TMSUserAppHolder"

    const-string v1, "regAppStatusEventListener exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    const/4 v0, 0x1

    return v0
.end method

.method public removeUnsupportedVncApp(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 940
    const-string v0, "TMSUserAppHolder"

    const-string v1, "Enter removeUnsupportedVncApp"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->removeUnsupportedApp(Ljava/lang/String;)V

    .line 945
    :cond_0
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 14
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 428
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    .line 429
    const-string v12, "phone"

    invoke-virtual {v9, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 428
    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 432
    .local v8, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v8}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v9

    if-ne v9, v11, :cond_0

    .line 433
    const-string v9, "TMSUserAppHolder"

    const-string v11, "Not launching app since user is on call"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 555
    :goto_0
    return v9

    .line 437
    :cond_0
    if-nez p1, :cond_1

    .line 438
    const-string v9, "TMSUserAppHolder"

    const-string v11, "startApp invalid input params"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 439
    goto :goto_0

    .line 442
    :cond_1
    const-string v9, "TMSUserAppHolder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "startApp enter "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    new-instance v6, Landroid/content/Intent;

    const-string v9, "com.mirrorlink.android.app.LAUNCH"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 446
    .local v6, "filterIntent":Landroid/content/Intent;
    invoke-virtual {v6, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v9, "android.intent.category.DEFAULT"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const/high16 v9, 0x30020000

    invoke-virtual {v6, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 452
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mPkgMngr:Landroid/content/pm/PackageManager;

    .line 453
    const/16 v12, 0x80

    .line 452
    invoke-virtual {v9, v6, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 457
    .local v0, "allMatches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const-string v9, "com.sec.android.app.twlauncher"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 458
    const-string v9, "TMSUserAppHolder"

    const-string v10, "startApp launching home app"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->goHome()V

    .line 460
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->updateAppLaunched(Ljava/lang/String;)V

    move v9, v11

    .line 461
    goto :goto_0

    .line 464
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "com.android.phone"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 465
    const-string v9, "TMSUserAppHolder"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "startApp allMatches.size() "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 466
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 465
    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 467
    goto :goto_0

    .line 469
    :cond_3
    const/4 v3, 0x0

    .line 470
    .local v3, "contactActivity":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;->getBuildVersion()I

    move-result v9

    const/16 v12, 0xe

    if-lt v9, v12, :cond_7

    .line 471
    const-string v3, "com.android.contacts.activities.PeopleActivity"

    .line 476
    :goto_1
    const-string v9, "com.android.contacts"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 477
    new-instance v2, Landroid/content/ComponentName;

    const-string v9, "com.android.contacts"

    invoke-direct {v2, v9, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    .local v2, "com":Landroid/content/ComponentName;
    const-string v9, "TMSUserAppHolder"

    .line 480
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "startApp Activity: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " App: com.android.contacts"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 479
    invoke-static {v9, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_2
    invoke-virtual {v6, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 505
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-virtual {v9, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 506
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->updateAppLaunched(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 520
    iget-boolean v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mFirstStartAppReq:Z

    if-eqz v9, :cond_b

    .line 521
    monitor-enter p0

    .line 522
    :try_start_1
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    if-eqz v9, :cond_4

    .line 523
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    invoke-virtual {v9, p1}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->start(Ljava/lang/String;)V

    .line 521
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 546
    :cond_5
    :goto_3
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v9, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 547
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-boolean v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mFirstStartAppReq:Z

    if-eqz v9, :cond_6

    .line 549
    const-string v9, "TMSUserAppHolder"

    const-string v12, "startApp sending initial app values to vnc"

    invoke-static {v9, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v9, v1, v10}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->sendAppValuesToVNC(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Z)V

    .line 552
    :cond_6
    iput-boolean v10, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mFirstStartAppReq:Z

    .line 554
    const-string v9, "TMSUserAppHolder"

    const-string v10, "startApp exit"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v11

    .line 555
    goto/16 :goto_0

    .line 473
    .end local v1    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v2    # "com":Landroid/content/ComponentName;
    :cond_7
    const-string v3, "com.sec.android.app.contacts.ContactsEntryActivity"

    goto :goto_1

    .line 481
    :cond_8
    const-string v9, "com.cooliris.media"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 482
    new-instance v2, Landroid/content/ComponentName;

    const-string v9, "com.cooliris.media"

    .line 483
    const-string v12, "com.cooliris.media.Gallery"

    .line 482
    invoke-direct {v2, v9, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    .restart local v2    # "com":Landroid/content/ComponentName;
    const-string v9, "TMSUserAppHolder"

    .line 486
    const-string v12, "startApp Activity: com.cooliris.media.Gallery App: com.cooliris.media"

    .line 485
    invoke-static {v9, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 488
    .end local v2    # "com":Landroid/content/ComponentName;
    :cond_9
    const-string v9, "com.android.phone"

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 489
    const-string v9, "TMSUserAppHolder"

    const-string v12, "startApp phone"

    invoke-static {v9, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    new-instance v2, Landroid/content/ComponentName;

    const-string v9, "com.android.phone"

    .line 491
    const-string v12, "com.android.phone.InCallScreen"

    .line 490
    invoke-direct {v2, v9, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    .restart local v2    # "com":Landroid/content/ComponentName;
    goto :goto_2

    .line 493
    .end local v2    # "com":Landroid/content/ComponentName;
    :cond_a
    const-string v12, "TMSUserAppHolder"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v9, "startApp Activity: "

    invoke-direct {v13, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 494
    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    iget-object v9, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, " App: "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 495
    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    iget-object v9, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 493
    invoke-static {v12, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    new-instance v2, Landroid/content/ComponentName;

    .line 498
    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    iget-object v9, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 499
    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    iget-object v9, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 497
    invoke-direct {v2, v12, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v2    # "com":Landroid/content/ComponentName;
    goto/16 :goto_2

    .line 507
    :catch_0
    move-exception v5

    .line 508
    .local v5, "e":Landroid/content/ActivityNotFoundException;
    const-string v9, "TMSUserAppHolder"

    const-string v11, "startApp Unable to launch. ActivityNotFoundException"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 509
    goto/16 :goto_0

    .line 510
    .end local v5    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v5

    .line 511
    .local v5, "e":Ljava/lang/SecurityException;
    const-string v9, "TMSUserAppHolder"

    .line 512
    const-string v11, "startApp Launcher does not have the permission to launch . Make sure to create a MAIN intent-filter for the corresponding activity or use the exported attribute for this activity. "

    .line 511
    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v10

    .line 516
    goto/16 :goto_0

    .line 521
    .end local v5    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v9

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v9

    .line 527
    :cond_b
    sget-object v9, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    if-eqz v9, :cond_5

    .line 528
    sget-object v9, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    const/high16 v12, 0x10000000

    invoke-virtual {v9, v11, v12}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v7

    .line 530
    .local v7, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    const/4 v4, 0x0

    .line 531
    .local v4, "curTopPkgName":Ljava/lang/String;
    if-eqz v7, :cond_c

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_c

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v9, v9, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v9, :cond_c

    .line 533
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v9, v9, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 536
    :cond_c
    if-eqz v4, :cond_5

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 537
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityMonitor:Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->sendCurActivity()V

    goto/16 :goto_3
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 564
    const-string v7, "TMSUserAppHolder"

    const-string v8, "stopApp enter"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const/4 v5, 0x0

    .line 566
    .local v5, "ret":Z
    if-nez p1, :cond_0

    .line 568
    const-string v7, "TMSUserAppHolder"

    const-string v8, "stopApp invalid input params"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v5

    .line 624
    .end local v5    # "ret":Z
    .local v6, "ret":I
    :goto_0
    return v6

    .line 575
    .end local v6    # "ret":I
    .restart local v5    # "ret":Z
    :cond_0
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.mirrorlink.android.app.TERMINATE"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 576
    .local v2, "intent":Landroid/content/Intent;
    const-string v7, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const/high16 v7, 0x30000000

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 581
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 582
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCntxt:Landroid/content/Context;

    invoke-virtual {v7, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    .line 585
    const-wide/16 v7, 0x64

    :try_start_1
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4

    .line 591
    :goto_1
    :try_start_2
    sget-object v7, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 592
    const-string v8, "forceStopPackage"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Ljava/lang/String;

    aput-object v11, v9, v10

    .line 591
    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 593
    .local v3, "listener":Ljava/lang/reflect/Method;
    sget-object v7, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    const/4 v5, 0x1

    .line 595
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->updateAppTerminated(Ljava/lang/String;)V

    .line 597
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCurrentTopApp:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 599
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityStateHandler:Landroid/os/Handler;

    invoke-virtual {v7}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 600
    .local v4, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 601
    .local v0, "bndl":Landroid/os/Bundle;
    const-string v7, "AppName"

    invoke-virtual {v0, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const-string v7, "state"

    const-string v8, "notrunning"

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const/16 v7, 0x64

    iput v7, v4, Landroid/os/Message;->what:I

    .line 604
    invoke-virtual {v4, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 605
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mActivityStateHandler:Landroid/os/Handler;

    invoke-virtual {v7, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_4

    .line 623
    .end local v0    # "bndl":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "listener":Ljava/lang/reflect/Method;
    .end local v4    # "msg":Landroid/os/Message;
    :cond_1
    :goto_2
    const-string v7, "TMSUserAppHolder"

    const-string v8, "stopApp exit"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v5

    .line 624
    .restart local v6    # "ret":I
    goto :goto_0

    .line 586
    .end local v6    # "ret":I
    .restart local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 587
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v7, "TMSUserAppHolder"

    const-string v8, "Buffer time to terminate app is over"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 608
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v1

    .line 609
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const/4 v5, 0x0

    goto :goto_2

    .line 611
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 613
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v7, "TMSUserAppHolder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Could not connect to channel."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const/4 v5, 0x0

    goto :goto_2

    .line 615
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v1

    .line 616
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v7, "TMSUserAppHolder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Could not connect to channel."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const/4 v5, 0x0

    goto :goto_2

    .line 618
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v1

    .line 619
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "TMSUserAppHolder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Could not connect to channel."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public updateCertifiedApps()V
    .locals 15

    .prologue
    const-wide/16 v13, 0x1388

    const/16 v12, 0xb

    const/4 v11, 0x0

    .line 241
    const-string v8, "TMSUserAppHolder"

    const-string v9, "Update certified apps enter"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->init()V

    .line 243
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mCertifiedAppListGenerator:Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->getAppListMap()Ljava/util/Map;

    move-result-object v7

    .line 244
    .local v7, "updatedMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getAddedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 245
    .local v4, "newAddedApps":Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 246
    const-string v8, "TMSUserAppHolder"

    const-string v9, "Some apps are added/selected"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4, v11, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "addedApps":Ljava/lang/String;
    const-string v8, "TMSUserAppHolder"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Added apps"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 250
    .local v3, "msgbundle":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 251
    .local v2, "msg":Landroid/os/Message;
    const-string v8, "PackageNames"

    invoke-virtual {v3, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v8, "State"

    const-string v9, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 254
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v2, v13, v14}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 257
    .end local v0    # "addedApps":Ljava/lang/String;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "msgbundle":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getRemovedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 258
    .local v5, "newRemovedApps":Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_1

    .line 259
    const-string v8, "TMSUserAppHolder"

    const-string v9, "Some apps are removed/deselected"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v11, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 261
    .restart local v0    # "addedApps":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 262
    .restart local v3    # "msgbundle":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 263
    .restart local v2    # "msg":Landroid/os/Message;
    const-string v8, "PackageNames"

    invoke-virtual {v3, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v8, "State"

    const-string v9, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 266
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v2, v13, v14}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 269
    .end local v0    # "addedApps":Ljava/lang/String;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "msgbundle":Landroid/os/Bundle;
    :cond_1
    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getUpdatedApps(Ljava/util/Map;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 270
    .local v6, "newUpdatedApps":Ljava/lang/StringBuilder;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 271
    const-string v8, "TMSUserAppHolder"

    const-string v9, "Some apps are updated"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v11, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 273
    .restart local v0    # "addedApps":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 274
    .restart local v3    # "msgbundle":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 275
    .restart local v2    # "msg":Landroid/os/Message;
    const-string v8, "PackageNames"

    invoke-virtual {v3, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v8, "State"

    const-string v9, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 278
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v2, v13, v14}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 282
    .end local v0    # "addedApps":Ljava/lang/String;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "msgbundle":Landroid/os/Bundle;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_3

    .line 286
    const-string v8, "TMSUserAppHolder"

    const-string v9, "Update certified apps exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    return-void

    .line 282
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 283
    .local v1, "app":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    const-string v10, "TMSUserAppHolder"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v8, "app id "

    invoke-direct {v11, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v8, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " package "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
