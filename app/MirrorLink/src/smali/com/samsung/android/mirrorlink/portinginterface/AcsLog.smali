.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;
.super Ljava/lang/Object;
.source "AcsLog.java"


# static fields
.field private static final DATE_FORMAT_NOW:Ljava/lang/String; = "yyyy-MM-dd HH:mm:ss.S"

.field private static buf:Ljava/io/BufferedWriter; = null

.field static final enableFileLog:Z = false

.field static final enableLog:Z = true

.field static logFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    sput-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->logFile:Ljava/io/File;

    .line 39
    sput-object v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    .line 72
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static Logger(Ljava/lang/String;)V
    .locals 4
    .param p0, "log"    # Ljava/lang/String;

    .prologue
    .line 80
    :try_start_0
    const-string v1, "ACSLOG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " message is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    if-nez v1, :cond_0

    .line 83
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->createBuf()V

    .line 85
    :cond_0
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    if-eqz v1, :cond_1

    .line 87
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    invoke-virtual {v1, p0}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 88
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 89
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_1
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private static createBuf()V
    .locals 6

    .prologue
    .line 53
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    new-instance v3, Ljava/io/FileOutputStream;

    sget-object v4, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->logFile:Ljava/io/File;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const-string v4, "UTF-8"

    invoke-direct {v2, v3, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    sput-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    :goto_0
    return-void

    .line 54
    .end local v0    # "e":Ljava/io/IOException;
    :catch_0
    move-exception v0

    .line 56
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return-void
.end method

.method private static now()Ljava/lang/String;
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 44
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.S"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 45
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static stopLogger()V
    .locals 2

    .prologue
    .line 170
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    if-eqz v1, :cond_0

    .line 172
    :try_start_0
    sget-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .local v0, "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->buf:Ljava/io/BufferedWriter;

    .line 179
    return-void

    .line 173
    .end local v0    # "e":Ljava/io/IOException;
    :catch_0
    move-exception v0

    .line 175
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    return-void
.end method
