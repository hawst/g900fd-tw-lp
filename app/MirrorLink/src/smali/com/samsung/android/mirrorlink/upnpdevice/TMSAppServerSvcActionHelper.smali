.class public Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;
.super Ljava/lang/Object;
.source "TMSAppServerSvcActionHelper.java"


# static fields
.field private static final HEXA_EXPRSN:Ljava/lang/String; = "([a-fA-F-0-9]*)"


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

.field private mOptnlEle:Ljava/lang/String;

.field private mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 565
    new-instance v0, Lcom/samsung/xml/Node;

    invoke-direct {v0, p1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getPackageNamesForMatchingFilter(Ljava/util/Map;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "appcertFilter":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Enter getPackageNamesForMatchingFilter "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;-><init>()V

    .line 237
    .local v0, "eInfo":Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 263
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getFilteredCertifiedEntries(Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;)Ljava/util/List;

    move-result-object v3

    .line 264
    .local v3, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v3, :cond_a

    .line 265
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v6, "None of package matches"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const/4 v3, 0x0

    .line 269
    .end local v3    # "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v3

    .line 237
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 238
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 239
    .local v2, "key":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 240
    .local v4, "val":Ljava/lang/String;
    const-string v6, "name"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 241
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setEntityName(Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_2
    const-string v6, "restricted"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 243
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setRestricted(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :cond_3
    const-string v6, "nonRestricted"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 245
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setNonRestricted(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_4
    const-string v6, "targetList@target"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 247
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setTargets(Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :cond_5
    const-string v6, "serviceList@service"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 249
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setServices(Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_6
    const-string v6, "properties"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 251
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_7
    const-string v6, "signature"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 253
    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setSignature(Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_8
    const-string v6, "appID"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 255
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setAppID(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    :cond_9
    const-string v6, "appUUID"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 257
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setAppUUID(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "val":Ljava/lang/String;
    .restart local v3    # "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Returining packages "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private includeOptionalNode(Ljava/lang/String;)Z
    .locals 2
    .param p1, "element"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "*"

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private includeOptionalNode(Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "elements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    .line 41
    const-string v2, "*"

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    :goto_0
    return v1

    .line 43
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 47
    const/4 v1, 0x0

    goto :goto_0

    .line 43
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 44
    .local v0, "string":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0
.end method

.method private isOptionalAppElement(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v0, "appID"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "remotingInfo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public createAllwdPrfIdNode()Lcom/samsung/xml/Node;
    .locals 5

    .prologue
    .line 569
    const/4 v0, 0x0

    .line 571
    .local v0, "allowedProfileIDsNode":Lcom/samsung/xml/Node;
    const-string v4, "allowedProfileIDs"

    invoke-direct {p0, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 573
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "allowedProfileIDsNode":Lcom/samsung/xml/Node;
    const-string v4, "allowedProfileIDs"

    invoke-direct {v0, v4}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 575
    .restart local v0    # "allowedProfileIDsNode":Lcom/samsung/xml/Node;
    const/4 v1, 0x0

    .line 576
    .local v1, "allwdProfileIDs":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    sget-object v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 593
    if-eqz v1, :cond_0

    .line 595
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 598
    .end local v1    # "allwdProfileIDs":Ljava/lang/String;
    .end local v3    # "j":I
    :cond_0
    return-object v0

    .line 578
    .restart local v1    # "allwdProfileIDs":Ljava/lang/String;
    .restart local v3    # "j":I
    :cond_1
    sget-object v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->getAppIDS()Ljava/util/LinkedHashSet;

    move-result-object v2

    .line 579
    .local v2, "appIds":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v4, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 581
    if-nez v1, :cond_3

    .line 583
    sget-object v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v4, v4, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 576
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 587
    :cond_3
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 588
    sget-object v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->mClientProfilesList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v4, v4, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mProfileID:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public createAppInfoNode()Lcom/samsung/xml/Node;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 643
    const/4 v2, 0x0

    .line 644
    .local v2, "tmsAppInfoNode":Lcom/samsung/xml/Node;
    const-string v3, "appInfo"

    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 646
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    if-eq v5, v3, :cond_0

    const-string v3, "appCategory"

    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 648
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "tmsAppInfoNode":Lcom/samsung/xml/Node;
    const-string v3, "appInfo"

    invoke-direct {v2, v3}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 649
    .restart local v2    # "tmsAppInfoNode":Lcom/samsung/xml/Node;
    new-instance v0, Lcom/samsung/xml/Node;

    const-string v3, "appCategory"

    invoke-direct {v0, v3}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 650
    .local v0, "appCategoryNode":Lcom/samsung/xml/Node;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v4, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 651
    invoke-virtual {v2, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 653
    .end local v0    # "appCategoryNode":Lcom/samsung/xml/Node;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    if-eq v5, v3, :cond_2

    const-string v3, "trustLevel"

    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 655
    if-nez v2, :cond_1

    .line 657
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "tmsAppInfoNode":Lcom/samsung/xml/Node;
    const-string v3, "appInfo"

    invoke-direct {v2, v3}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 659
    .restart local v2    # "tmsAppInfoNode":Lcom/samsung/xml/Node;
    :cond_1
    new-instance v1, Lcom/samsung/xml/Node;

    const-string v3, "trustLevel"

    invoke-direct {v1, v3}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 660
    .local v1, "apptrustLevelNode":Lcom/samsung/xml/Node;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 661
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v4, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 660
    invoke-virtual {v1, v3}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 662
    invoke-virtual {v2, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 665
    .end local v1    # "apptrustLevelNode":Lcom/samsung/xml/Node;
    :cond_2
    return-object v2
.end method

.method public createAudioInfoNode()Lcom/samsung/xml/Node;
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 721
    const/4 v0, 0x0

    .line 722
    .local v0, "audioInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "audioInfo"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 724
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    if-eq v7, v5, :cond_0

    const-string v5, "contentCategory"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 726
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "audioInfo"

    invoke-direct {v0, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 727
    .restart local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    new-instance v2, Lcom/samsung/xml/Node;

    const-string v5, "contentCategory"

    invoke-direct {v2, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 728
    .local v2, "contentCategoryNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 729
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 728
    invoke-virtual {v2, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 730
    invoke-virtual {v0, v2}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 733
    .end local v2    # "contentCategoryNode":Lcom/samsung/xml/Node;
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentRules:I

    if-eq v7, v5, :cond_2

    const-string v5, "contentRules"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 735
    if-nez v0, :cond_1

    .line 737
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "audioInfo"

    invoke-direct {v0, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 739
    .restart local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    :cond_1
    new-instance v3, Lcom/samsung/xml/Node;

    const-string v5, "contentRules"

    invoke-direct {v3, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 740
    .local v3, "contentRulesNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 741
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentRules:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 740
    invoke-virtual {v3, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 742
    invoke-virtual {v0, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 745
    .end local v3    # "contentRulesNode":Lcom/samsung/xml/Node;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    if-eq v7, v5, :cond_4

    const-string v5, "trustLevel"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 747
    if-nez v0, :cond_3

    .line 749
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "audioInfo"

    invoke-direct {v0, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 751
    .restart local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    :cond_3
    new-instance v4, Lcom/samsung/xml/Node;

    const-string v5, "trustLevel"

    invoke-direct {v4, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 752
    .local v4, "trustLevelNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 753
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 752
    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 754
    invoke-virtual {v0, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 756
    .end local v4    # "trustLevelNode":Lcom/samsung/xml/Node;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    if-eqz v5, :cond_6

    const-string v5, "audioType"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 758
    if-nez v0, :cond_5

    .line 760
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "audioInfo"

    invoke-direct {v0, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 762
    .restart local v0    # "audioInfoNode":Lcom/samsung/xml/Node;
    :cond_5
    new-instance v1, Lcom/samsung/xml/Node;

    const-string v5, "audioType"

    invoke-direct {v1, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 763
    .local v1, "audioTypeNode":Lcom/samsung/xml/Node;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 764
    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 767
    .end local v1    # "audioTypeNode":Lcom/samsung/xml/Node;
    :cond_6
    return-object v0
.end method

.method public createDisplayInfoNode()Lcom/samsung/xml/Node;
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 670
    const/4 v2, 0x0

    .line 671
    .local v2, "displayInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "displayInfo"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 673
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    if-eq v7, v5, :cond_0

    const-string v5, "contentCategory"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 675
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "displayInfo"

    invoke-direct {v2, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 676
    .restart local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    new-instance v0, Lcom/samsung/xml/Node;

    const-string v5, "contentCategory"

    invoke-direct {v0, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 677
    .local v0, "contentCategoryNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 678
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 677
    invoke-virtual {v0, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 679
    invoke-virtual {v2, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 682
    .end local v0    # "contentCategoryNode":Lcom/samsung/xml/Node;
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    if-eq v7, v5, :cond_2

    const-string v5, "contentRules"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 684
    if-nez v2, :cond_1

    .line 686
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "displayInfo"

    invoke-direct {v2, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 688
    .restart local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    :cond_1
    new-instance v1, Lcom/samsung/xml/Node;

    const-string v5, "contentRules"

    invoke-direct {v1, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 689
    .local v1, "contentRulesNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 690
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 689
    invoke-virtual {v1, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 691
    invoke-virtual {v2, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 694
    .end local v1    # "contentRulesNode":Lcom/samsung/xml/Node;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    if-eqz v5, :cond_4

    const-string v5, "orientation"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 696
    if-nez v2, :cond_3

    .line 698
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "displayInfo"

    invoke-direct {v2, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 700
    .restart local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    :cond_3
    new-instance v3, Lcom/samsung/xml/Node;

    const-string v5, "orientation"

    invoke-direct {v3, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 701
    .local v3, "orientationNode":Lcom/samsung/xml/Node;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 702
    invoke-virtual {v2, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 704
    .end local v3    # "orientationNode":Lcom/samsung/xml/Node;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v5, v5, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    if-eq v7, v5, :cond_6

    const-string v5, "trustLevel"

    invoke-direct {p0, v5}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 706
    if-nez v2, :cond_5

    .line 708
    new-instance v2, Lcom/samsung/xml/Node;

    .end local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    const-string v5, "displayInfo"

    invoke-direct {v2, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 710
    .restart local v2    # "displayInfoNode":Lcom/samsung/xml/Node;
    :cond_5
    new-instance v4, Lcom/samsung/xml/Node;

    const-string v5, "trustLevel"

    invoke-direct {v4, v5}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 711
    .local v4, "trustLevelNode":Lcom/samsung/xml/Node;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 712
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 711
    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 713
    invoke-virtual {v2, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 716
    .end local v4    # "trustLevelNode":Lcom/samsung/xml/Node;
    :cond_6
    return-object v2
.end method

.method public createIconNode()Lcom/samsung/xml/Node;
    .locals 12

    .prologue
    const/4 v11, -0x1

    .line 514
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 515
    .local v2, "iconEles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v9, "icon"

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    const-string v9, "mimetype"

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    const/4 v4, 0x0

    .line 519
    .local v4, "iconListNode":Lcom/samsung/xml/Node;
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/util/ArrayList;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v9, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-eqz v9, :cond_0

    .line 521
    new-instance v4, Lcom/samsung/xml/Node;

    .end local v4    # "iconListNode":Lcom/samsung/xml/Node;
    const-string v9, "iconList"

    invoke-direct {v4, v9}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 522
    .restart local v4    # "iconListNode":Lcom/samsung/xml/Node;
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v9, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 561
    :cond_0
    return-object v4

    .line 522
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    .line 523
    .local v3, "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    const-string v10, "icon"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v5

    .line 524
    .local v5, "iconNode":Lcom/samsung/xml/Node;
    const-string v10, "mimetype"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v7

    .line 526
    .local v7, "mimetypeNode":Lcom/samsung/xml/Node;
    iget-object v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    invoke-virtual {v7, v10}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 528
    const-string v10, "width"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v8

    .line 529
    .local v8, "widthNode":Lcom/samsung/xml/Node;
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    if-eq v11, v10, :cond_2

    .line 530
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_width:I

    invoke-virtual {v8, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 534
    :goto_1
    const-string v10, "height"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v1

    .line 535
    .local v1, "heightNode":Lcom/samsung/xml/Node;
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    if-eq v11, v10, :cond_3

    .line 536
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_height:I

    invoke-virtual {v1, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 540
    :goto_2
    const-string v10, "depth"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v0

    .line 541
    .local v0, "depthNode":Lcom/samsung/xml/Node;
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    if-eq v11, v10, :cond_4

    .line 542
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    iget v10, v10, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mIcon_depth:I

    invoke-virtual {v0, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 547
    :goto_3
    const-string v10, "url"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getNode(Ljava/lang/String;)Lcom/samsung/xml/Node;

    move-result-object v6

    .line 550
    .local v6, "iconurlNode":Lcom/samsung/xml/Node;
    iget-object v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    invoke-virtual {v6, v10}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 552
    invoke-virtual {v5, v7}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 553
    invoke-virtual {v5, v8}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 554
    invoke-virtual {v5, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 555
    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 556
    invoke-virtual {v5, v6}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 557
    invoke-virtual {v4, v5}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    goto :goto_0

    .line 532
    .end local v0    # "depthNode":Lcom/samsung/xml/Node;
    .end local v1    # "heightNode":Lcom/samsung/xml/Node;
    .end local v6    # "iconurlNode":Lcom/samsung/xml/Node;
    :cond_2
    iget v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mWidth:I

    invoke-virtual {v8, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    goto :goto_1

    .line 538
    .restart local v1    # "heightNode":Lcom/samsung/xml/Node;
    :cond_3
    iget v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mHeight:I

    invoke-virtual {v1, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    goto :goto_2

    .line 544
    .restart local v0    # "depthNode":Lcom/samsung/xml/Node;
    :cond_4
    iget v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mDepth:I

    invoke-virtual {v0, v10}, Lcom/samsung/xml/Node;->setValue(I)V

    goto :goto_3
.end method

.method public createProviderNameNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 497
    .local v0, "providerNameNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderName:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "providerName"

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "providerNameNode":Lcom/samsung/xml/Node;
    const-string v1, "providerName"

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 499
    .restart local v0    # "providerNameNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 501
    :cond_0
    return-object v0
.end method

.method public createProviderUrlNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 505
    const/4 v0, 0x0

    .line 506
    .local v0, "providerUrlNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderURL:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "providerURL"

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "providerUrlNode":Lcom/samsung/xml/Node;
    const-string v1, "providerURL"

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 508
    .restart local v0    # "providerUrlNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 510
    :cond_0
    return-object v0
.end method

.method public createRemotingInfoNode()Lcom/samsung/xml/Node;
    .locals 8

    .prologue
    .line 603
    const/4 v5, 0x0

    .line 604
    .local v5, "remotingInfoNode":Lcom/samsung/xml/Node;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v6

    .line 605
    .local v6, "rmtngInfo":Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 607
    new-instance v5, Lcom/samsung/xml/Node;

    .end local v5    # "remotingInfoNode":Lcom/samsung/xml/Node;
    const-string v7, "remotingInfo"

    invoke-direct {v5, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 608
    .restart local v5    # "remotingInfoNode":Lcom/samsung/xml/Node;
    new-instance v4, Lcom/samsung/xml/Node;

    const-string v7, "protocolID"

    invoke-direct {v4, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 609
    .local v4, "protocolIDNode":Lcom/samsung/xml/Node;
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 610
    invoke-virtual {v5, v4}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 612
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 614
    new-instance v3, Lcom/samsung/xml/Node;

    const-string v7, "format"

    invoke-direct {v3, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 615
    .local v3, "formatNode":Lcom/samsung/xml/Node;
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 616
    invoke-virtual {v5, v3}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 618
    .end local v3    # "formatNode":Lcom/samsung/xml/Node;
    :cond_0
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 620
    new-instance v2, Lcom/samsung/xml/Node;

    const-string v7, "direction"

    invoke-direct {v2, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 621
    .local v2, "directionNode":Lcom/samsung/xml/Node;
    iget-object v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 622
    invoke-virtual {v5, v2}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 624
    .end local v2    # "directionNode":Lcom/samsung/xml/Node;
    :cond_1
    iget v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    if-eqz v7, :cond_2

    .line 626
    new-instance v0, Lcom/samsung/xml/Node;

    const-string v7, "audioIPL"

    invoke-direct {v0, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 627
    .local v0, "audioIplNode":Lcom/samsung/xml/Node;
    iget v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    invoke-virtual {v0, v7}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 628
    invoke-virtual {v5, v0}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 630
    .end local v0    # "audioIplNode":Lcom/samsung/xml/Node;
    :cond_2
    iget v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    if-eqz v7, :cond_3

    .line 632
    new-instance v1, Lcom/samsung/xml/Node;

    const-string v7, "audioMPL"

    invoke-direct {v1, v7}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 633
    .local v1, "audioMplNode":Lcom/samsung/xml/Node;
    iget v7, v6, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    invoke-virtual {v1, v7}, Lcom/samsung/xml/Node;->setValue(I)V

    .line 634
    invoke-virtual {v5, v1}, Lcom/samsung/xml/Node;->addNode(Lcom/samsung/xml/Node;)V

    .line 638
    .end local v1    # "audioMplNode":Lcom/samsung/xml/Node;
    .end local v4    # "protocolIDNode":Lcom/samsung/xml/Node;
    :cond_3
    return-object v5
.end method

.method public createResourceStatusNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 773
    const/4 v0, 0x0

    .line 774
    .local v0, "resourceStatusNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "resourceStatus"

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 776
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "resourceStatusNode":Lcom/samsung/xml/Node;
    const-string v1, "resourceStatus"

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 777
    .restart local v0    # "resourceStatusNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 779
    :cond_0
    return-object v0
.end method

.method public createVariantNode()Lcom/samsung/xml/Node;
    .locals 2

    .prologue
    .line 486
    const/4 v0, 0x0

    .line 487
    .local v0, "variantNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mVariant:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "variant"

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->includeOptionalNode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    new-instance v0, Lcom/samsung/xml/Node;

    .end local v0    # "variantNode":Lcom/samsung/xml/Node;
    const-string v1, "variant"

    invoke-direct {v0, v1}, Lcom/samsung/xml/Node;-><init>(Ljava/lang/String;)V

    .line 489
    .restart local v0    # "variantNode":Lcom/samsung/xml/Node;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mVariant:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/xml/Node;->setValue(Ljava/lang/String;)V

    .line 491
    :cond_0
    return-object v0
.end method

.method public filterAppsByAppCertFilter(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p2, "appCertFilter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 157
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "TMApplicationServerService. filterAppsByAppCertFilter enter appCertFilter = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 157
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 156
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    if-nez p1, :cond_0

    .line 161
    const/4 p1, 0x0

    .line 231
    .end local p1    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :goto_0
    return-object p1

    .line 164
    .restart local p1    # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_0
    const-string v10, "\""

    invoke-virtual {p2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 165
    const-string v10, "\""

    invoke-virtual {p2, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 166
    const/4 v10, 0x1

    .line 167
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 166
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 170
    :cond_1
    const-string v10, "*"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, ""

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_3

    .line 171
    :cond_2
    const-string v10, "*"

    iput-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    goto :goto_0

    .line 175
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .local v8, "strBuldr":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/util/StringTokenizer;

    const-string v10, ","

    invoke-direct {v9, p2, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .local v9, "tokenizer":Ljava/util/StringTokenizer;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 182
    .local v3, "appcertFilter":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v10

    if-nez v10, :cond_5

    .line 217
    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->getPackageNamesForMatchingFilter(Ljava/util/Map;)Ljava/util/List;

    move-result-object v6

    .line 218
    .local v6, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v6, :cond_8

    .line 219
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v11, "Matching package names is empty"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 p1, 0x0

    goto :goto_0

    .line 183
    .end local v6    # "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "kv":Ljava/lang/String;
    const/16 v10, 0x3d

    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 185
    .local v7, "pos":I
    const/4 v10, -0x1

    if-eq v7, v10, :cond_4

    .line 188
    const/4 v10, 0x0

    invoke-virtual {v5, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "appCertFilterKey":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 191
    add-int/lit8 v10, v7, 0x1

    invoke-virtual {v5, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "appCertFilterValue":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 193
    const-string v10, "\""

    invoke-virtual {v1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 194
    const-string v10, "\""

    invoke-virtual {v1, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 195
    const/4 v10, 0x1

    .line 196
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 195
    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 200
    :cond_6
    const-string v10, "*"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 201
    const-string v10, "*"

    const-string v11, ""

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    :cond_7
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v10

    if-nez v10, :cond_4

    .line 207
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v11, "filter size is zero. hence returnining all apps"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 223
    .end local v0    # "appCertFilterKey":Ljava/lang/String;
    .end local v1    # "appCertFilterValue":Ljava/lang/String;
    .end local v5    # "kv":Ljava/lang/String;
    .end local v7    # "pos":I
    .restart local v6    # "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_8
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v4, "filteredList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_9
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_a

    .line 229
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    .line 230
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v11, "TMApplicationServerService.filterAppsByAppInfo exit"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v4

    .line 231
    goto/16 :goto_0

    .line 224
    :cond_a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 225
    .local v2, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    iget-object v11, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v6, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 226
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public filterAppsByAppInfo(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p2, "appListFilterExp"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 80
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "TMApplicationServerService. filterAppsByAppInfo enter appListFilterExp = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 80
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 79
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    if-nez p1, :cond_0

    .line 84
    const/4 v4, 0x0

    .line 145
    :goto_0
    return-object v4

    .line 87
    :cond_0
    const-string v10, "\""

    invoke-virtual {p2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 88
    const-string v10, "\""

    invoke-virtual {p2, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 89
    const/4 v10, 0x1

    .line 90
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 89
    invoke-virtual {p2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 92
    :cond_1
    const-string v10, "*"

    invoke-virtual {v10, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_3

    .line 93
    :cond_2
    const-string v10, "*"

    iput-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    move-object v4, p1

    .line 94
    goto :goto_0

    .line 97
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v8, "strBuldr":Ljava/lang/StringBuilder;
    new-instance v9, Ljava/util/StringTokenizer;

    const-string v10, ","

    invoke-direct {v9, p2, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .local v9, "tokenizer":Ljava/util/StringTokenizer;
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 103
    .local v4, "filteredList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 105
    .local v1, "appListFilter":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    :goto_1
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v10

    if-nez v10, :cond_5

    .line 129
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 131
    .local v5, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :goto_2
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_8

    .line 143
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mOptnlEle:Ljava/lang/String;

    .line 144
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v11, "TMApplicationServerService.filterAppsByAppInfo exit"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v5    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_5
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 107
    .local v6, "kv":Ljava/lang/String;
    const/16 v10, 0x3d

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 108
    .local v7, "pos":I
    const/4 v10, -0x1

    if-eq v7, v10, :cond_4

    .line 111
    const/4 v10, 0x0

    invoke-virtual {v6, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 112
    .local v2, "appListFilterKey":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 114
    add-int/lit8 v10, v7, 0x1

    invoke-virtual {v6, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "appListFilterValue":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 116
    const-string v10, "\""

    invoke-virtual {v3, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 117
    const-string v10, "\""

    invoke-virtual {v3, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 118
    const/4 v10, 0x1

    .line 119
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    .line 118
    invoke-virtual {v3, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 121
    :cond_6
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isOptionalAppElement(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 124
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 126
    :cond_7
    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto :goto_1

    .line 132
    .end local v2    # "appListFilterKey":Ljava/lang/String;
    .end local v3    # "appListFilterValue":Ljava/lang/String;
    .end local v6    # "kv":Ljava/lang/String;
    .end local v7    # "pos":I
    .restart local v5    # "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    :cond_8
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 134
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppInfoMatching(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Ljava/util/Map;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 135
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 139
    :cond_9
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 140
    const-string v11, "TMApplicationServerService.filterAppsByAppInfo (isAppInfoMatching (appInfo, appListFilter) != true)"

    .line 138
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method isAppInfoMatching(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Ljava/util/Map;)Z
    .locals 14
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 280
    .local p2, "appFilter":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v12, "TMApplicationServerService.isAppInfoMatching enter"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const/4 v4, 0x1

    .line 282
    .local v4, "isMatching":Z
    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    .line 285
    .local v8, "s":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 287
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    .line 452
    :goto_1
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 453
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "TMApplicationServerService.isAppInfoMatching exit isMatching = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 453
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 452
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    return v4

    .line 289
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 292
    .local v7, "m":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 295
    .local v6, "key":Ljava/lang/String;
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 297
    .local v10, "value":Ljava/lang/String;
    const-string v11, "name"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 298
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 299
    const-string v12, "(.*)"

    .line 298
    invoke-virtual {p0, v10, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 299
    if-nez v11, :cond_0

    .line 300
    const/4 v4, 0x0

    .line 301
    goto :goto_1

    .line 303
    :cond_2
    const-string v11, "description"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 304
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    const-string v12, "(.*)"

    invoke-virtual {p0, v10, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 305
    const/4 v4, 0x0

    .line 306
    goto :goto_1

    .line 309
    :cond_3
    const-string v11, "providerName"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 310
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderName:Ljava/lang/String;

    const-string v12, "(.*)"

    invoke-virtual {p0, v10, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 311
    const/4 v4, 0x0

    .line 312
    goto :goto_1

    .line 314
    :cond_4
    const-string v11, "icon@mimetype"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 315
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-nez v11, :cond_5

    .line 316
    const/4 v4, 0x0

    .line 317
    goto :goto_1

    .line 319
    :cond_5
    const/4 v3, 0x0

    .line 320
    .local v3, "isIconMatching":Z
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 326
    :goto_2
    if-nez v3, :cond_0

    .line 327
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v12, "No icon mimetype matched"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v4, 0x0

    .line 329
    goto/16 :goto_1

    .line 320
    :cond_7
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    .line 321
    .local v2, "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    iget-object v12, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    const-string v13, "(.*)"

    invoke-virtual {p0, v10, v12, v13}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 322
    const/4 v3, 0x1

    .line 323
    goto :goto_2

    .line 331
    .end local v2    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    .end local v3    # "isIconMatching":Z
    :cond_8
    const-string v11, "icon@width"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 332
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-nez v11, :cond_9

    .line 333
    const/4 v4, 0x0

    .line 334
    goto/16 :goto_1

    .line 336
    :cond_9
    const/4 v3, 0x0

    .line 337
    .restart local v3    # "isIconMatching":Z
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_b

    .line 343
    :goto_3
    if-nez v3, :cond_0

    .line 344
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v12, "No icon width matched"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const/4 v4, 0x0

    .line 346
    goto/16 :goto_1

    .line 337
    :cond_b
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    .line 338
    .restart local v2    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    iget v12, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mWidth:I

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-ne v12, v13, :cond_a

    .line 339
    const/4 v3, 0x1

    .line 340
    goto :goto_3

    .line 348
    .end local v2    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    .end local v3    # "isIconMatching":Z
    :cond_c
    const-string v11, "icon@height"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 349
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-nez v11, :cond_d

    .line 350
    const/4 v4, 0x0

    .line 351
    goto/16 :goto_1

    .line 353
    :cond_d
    const/4 v3, 0x0

    .line 354
    .restart local v3    # "isIconMatching":Z
    iget-object v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_e
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_f

    .line 360
    :goto_4
    if-nez v3, :cond_0

    .line 361
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    const-string v12, "No icon width matched"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const/4 v4, 0x0

    .line 363
    goto/16 :goto_1

    .line 354
    :cond_f
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    .line 355
    .restart local v2    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    iget v12, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mHeight:I

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-ne v12, v13, :cond_e

    .line 356
    const/4 v3, 0x1

    .line 357
    goto :goto_4

    .line 365
    .end local v2    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    .end local v3    # "isIconMatching":Z
    :cond_10
    const-string v11, "remotingInfo@protocolID"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 366
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v11

    if-nez v11, :cond_11

    .line 367
    const/4 v4, 0x0

    .line 368
    goto/16 :goto_0

    .line 370
    :cond_11
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v11

    iget-object v11, v11, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    const-string v12, "(.*)"

    .line 369
    invoke-virtual {p0, v10, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 370
    if-nez v11, :cond_0

    .line 371
    const/4 v4, 0x0

    .line 372
    goto/16 :goto_1

    .line 374
    :cond_12
    const-string v11, "appInfo@appCategory"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 376
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 375
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "catgry":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "([a-fA-F-0-9]*)"

    .line 377
    invoke-virtual {p0, v0, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 378
    if-nez v11, :cond_0

    .line 379
    const/4 v4, 0x0

    .line 380
    goto/16 :goto_1

    .line 382
    .end local v0    # "catgry":Ljava/lang/String;
    :cond_13
    const-string v11, "appInfo@trustlevel"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 384
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 383
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v9

    .line 386
    .local v9, "trustlevel":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "([a-fA-F-0-9]*)"

    .line 385
    invoke-virtual {p0, v9, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 386
    if-nez v11, :cond_0

    .line 387
    const/4 v4, 0x0

    .line 388
    goto/16 :goto_1

    .line 390
    .end local v9    # "trustlevel":Ljava/lang/String;
    :cond_14
    const-string v11, "displayInfo@contentCategory"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 392
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 391
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 394
    .restart local v0    # "catgry":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 395
    const-string v12, "([a-fA-F-0-9]*)"

    .line 393
    invoke-virtual {p0, v0, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 395
    if-nez v11, :cond_0

    .line 396
    const/4 v4, 0x0

    .line 397
    goto/16 :goto_1

    .line 399
    .end local v0    # "catgry":Ljava/lang/String;
    :cond_15
    const-string v11, "displayInfo@contentRules"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_16

    .line 401
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 400
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    .line 403
    .local v1, "cntentRule":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 404
    const-string v12, "([a-fA-F-0-9]*)"

    .line 402
    invoke-virtual {p0, v1, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 404
    if-nez v11, :cond_0

    .line 405
    const/4 v4, 0x0

    .line 406
    goto/16 :goto_1

    .line 408
    .end local v1    # "cntentRule":Ljava/lang/String;
    :cond_16
    const-string v11, "displayInfo@trustlevel"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 410
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 409
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v9

    .line 412
    .restart local v9    # "trustlevel":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 413
    const-string v12, "([a-fA-F-0-9]*)"

    .line 411
    invoke-virtual {p0, v9, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 413
    if-nez v11, :cond_0

    .line 414
    const/4 v4, 0x0

    .line 415
    goto/16 :goto_1

    .line 417
    .end local v9    # "trustlevel":Ljava/lang/String;
    :cond_17
    const-string v11, "audioinfo@contentCategory"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 419
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 418
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 421
    .restart local v0    # "catgry":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 422
    const-string v12, "([a-fA-F-0-9]*)"

    .line 420
    invoke-virtual {p0, v0, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 422
    if-nez v11, :cond_0

    .line 423
    const/4 v4, 0x0

    .line 424
    goto/16 :goto_1

    .line 426
    .end local v0    # "catgry":Ljava/lang/String;
    :cond_18
    const-string v11, "displayInfo@contentRules"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 428
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 427
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    .line 430
    .restart local v1    # "cntentRule":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    .line 431
    const-string v12, "([a-fA-F-0-9]*)"

    .line 429
    invoke-virtual {p0, v1, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 431
    if-nez v11, :cond_0

    .line 432
    const/4 v4, 0x0

    .line 433
    goto/16 :goto_1

    .line 435
    .end local v1    # "cntentRule":Ljava/lang/String;
    :cond_19
    const-string v11, "audioinfo@trustlevel"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1a

    .line 437
    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    .line 436
    invoke-static {v11, v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v9

    .line 439
    .restart local v9    # "trustlevel":Ljava/lang/String;
    iget v11, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "([a-fA-F-0-9]*)"

    .line 438
    invoke-virtual {p0, v9, v11, v12}, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    .line 439
    if-nez v11, :cond_0

    .line 440
    const/4 v4, 0x0

    .line 441
    goto/16 :goto_1

    .line 444
    .end local v9    # "trustlevel":Ljava/lang/String;
    :cond_1a
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 445
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "TMApplicationServerService.isAppInfoMatching key "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 446
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " cannot be found"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 445
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 444
    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const/4 v4, 0x0

    .line 448
    goto/16 :goto_1
.end method

.method isAppMatching(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "exp"    # Ljava/lang/String;
    .param p2, "input"    # Ljava/lang/String;
    .param p3, "replcString"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 460
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v2

    .line 462
    :cond_1
    const-string v3, "\\*"

    invoke-virtual {p1, v3, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 463
    const/4 v3, 0x2

    invoke-static {p1, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 464
    .local v1, "patrn":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 465
    .local v0, "mtchr":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_2

    .line 466
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 467
    const-string v4, "TMApplicationServerService.isAppMatching exit false"

    .line 466
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 470
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->LOG_TAG:Ljava/lang/String;

    .line 471
    const-string v3, "TMApplicationServerService.isAppMatching exit true"

    .line 470
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setAppListFilter(Ljava/lang/String;)V
    .locals 0
    .param p1, "filter"    # Ljava/lang/String;

    .prologue
    .line 69
    return-void
.end method

.method public setCurrentClient(Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;)V
    .locals 0
    .param p1, "crntTMCProfile"    # Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mCrntTMCProfile:Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    .line 479
    return-void
.end method

.method public setCurrrntAppInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 0
    .param p1, "tmsAppInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMSAppServerSvcActionHelper;->mTMSAppInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 484
    return-void
.end method
