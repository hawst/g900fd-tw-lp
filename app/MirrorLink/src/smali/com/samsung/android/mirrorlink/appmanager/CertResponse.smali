.class Lcom/samsung/android/mirrorlink/appmanager/CertResponse;
.super Ljava/lang/Object;
.source "DapCertificateProvider.java"


# instance fields
.field private mContent:Ljava/lang/String;

.field private mNonce:Ljava/lang/String;

.field private mRootContent:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mContent:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mNonce:Ljava/lang/String;

    return-object v0
.end method

.method public getRootContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mRootContent:Ljava/lang/String;

    return-object v0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "Content"    # Ljava/lang/String;

    .prologue
    .line 456
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mContent:Ljava/lang/String;

    .line 457
    return-void
.end method

.method public setNonce(Ljava/lang/String;)V
    .locals 0
    .param p1, "Nonce"    # Ljava/lang/String;

    .prologue
    .line 468
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mNonce:Ljava/lang/String;

    .line 469
    return-void
.end method

.method public setRootContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "rootContent"    # Ljava/lang/String;

    .prologue
    .line 460
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->mRootContent:Ljava/lang/String;

    .line 461
    return-void
.end method
