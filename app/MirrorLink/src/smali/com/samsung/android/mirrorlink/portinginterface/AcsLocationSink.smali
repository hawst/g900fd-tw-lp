.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
.super Ljava/lang/Object;
.source "AcsLocationSink.java"


# static fields
.field public static final SUBSCRIPTION_AUTOMATIC:I = 0x3

.field public static final SUBSCRIPTION_INTERVAL_DEFAULT:I = 0xea60

.field public static final SUBSCRIPTION_ONCHANGE:I = 0x2

.field public static final SUBSCRIPTION_REGULAR:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TMSLocSink"


# instance fields
.field private mAccuracy:F

.field private mAltitude:D

.field private mAltitudeAccuracy:D

.field private mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

.field private mGetObjSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHeading:D

.field private mLatitude:D

.field private mLocListener:Landroid/location/LocationListener;

.field private mLocation:Landroid/location/LocationManager;

.field private mLongitude:D

.field private mMinTime:J

.field private mProvider:Ljava/lang/String;

.field private mSpeed:F

.field private mSubscribeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTimestamp:J


# direct methods
.method public constructor <init>(Landroid/location/LocationManager;)V
    .locals 3
    .param p1, "loc"    # Landroid/location/LocationManager;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    .line 70
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.AcsLocationSink() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    .line 72
    const-string v0, "CARGPS"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 75
    const-string v0, "TMSLocSink"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :goto_0
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink$1;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocListener:Landroid/location/LocationListener;

    .line 102
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    .line 103
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    .line 104
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.AcsLocationSink() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void

    .line 79
    :cond_0
    const-string v0, "TMSLocSink"

    const-string v1, "location obj is valid "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "location providers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    invoke-virtual {v2}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public deInitLocationSink()V
    .locals 2

    .prologue
    .line 154
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.deInitLocationSink() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    .line 157
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.deInitLocationSink() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public getObject(I)V
    .locals 3
    .param p1, "objectId"    # I

    .prologue
    .line 318
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getObject Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getGeoLocation()V

    .line 321
    const-string v0, "TMSLocSink"

    const-string v1, "getObject Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return-void
.end method

.method public getResponseForLocationData()V
    .locals 7

    .prologue
    const v6, 0x572a6461

    .line 330
    const-string v3, "TMSLocSink"

    const-string v4, "getResponseForLocationData Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v1

    .line 332
    .local v1, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v1, :cond_0

    .line 333
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 334
    .local v2, "location":Landroid/os/Bundle;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 336
    .local v0, "coord":Landroid/os/Bundle;
    const-string v3, "0x64f8f3f1"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLatitude:D

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 338
    const-string v3, "0x7581892a"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLongitude:D

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 340
    const-string v3, "0x970e9047"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitude:D

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 342
    const-string v3, "0x5ec654de"

    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAccuracy:F

    float-to-double v4, v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 344
    const-string v3, "0xc28b9440"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitudeAccuracy:D

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 346
    const-string v3, "0x813c675d"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mHeading:D

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 348
    const-string v3, "0x23234962"

    iget v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSpeed:F

    float-to-double v4, v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 350
    const-string v3, "0x59413fd1"

    iget-wide v4, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mTimestamp:J

    long-to-double v4, v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 352
    const-string v3, "0xbad026d0"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 354
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v3

    .line 356
    const/4 v4, 0x1

    .line 354
    invoke-virtual {v1, v3, v6, v4, v2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 358
    .end local v0    # "coord":Landroid/os/Bundle;
    .end local v2    # "location":Landroid/os/Bundle;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 359
    return-void
.end method

.method public initLocationSink()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.initLocationSink() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 111
    const-string v0, "TMSLocSink"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.initLocationSink() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public responseForLocation(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "result"    # I

    .prologue
    const v5, 0xea60

    const/4 v7, -0x1

    const/4 v4, 0x1

    const v2, 0x572a6461

    .line 260
    if-nez p2, :cond_1

    move v3, v4

    .line 261
    .local v3, "flag":Z
    :goto_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    .line 262
    .local v0, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    sparse-switch p1, :sswitch_data_0

    .line 315
    :cond_0
    :goto_1
    return-void

    .line 260
    .end local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .end local v3    # "flag":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 265
    .restart local v0    # "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .restart local v3    # "flag":Z
    :sswitch_0
    if-eqz v0, :cond_0

    .line 266
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyRegisterForService(IZ)V

    goto :goto_1

    .line 270
    :sswitch_1
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyRegisterForService(IZ)V

    goto :goto_1

    .line 275
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    if-eqz v0, :cond_2

    .line 277
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 284
    :sswitch_3
    const-string v1, "TMSLocSink"

    const-string v4, "LOCATION_OBJECT_UID SBP_GET sucess"

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    const-string v1, "TMSLocSink"

    const-string v4, "getlist has LOCATION_OBJECT_UID SBP_GET "

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    if-eqz v0, :cond_3

    if-nez v3, :cond_3

    .line 288
    const-string v1, "TMSLocSink"

    const-string v4, "LOCATION_OBJECT_UID get failed"

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    .line 290
    const/4 v4, 0x0

    .line 289
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifyGetDataObjectResponse(IIZLandroid/os/Bundle;)V

    .line 292
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 296
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    if-eqz v0, :cond_0

    .line 298
    if-eqz v3, :cond_4

    .line 299
    const-string v1, "TMSLocSink"

    const-string v6, "LOCATION_OBJECT_UID subscribe sucess"

    invoke-static {v1, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    goto/16 :goto_1

    .line 303
    :cond_4
    const-string v1, "TMSLocSink"

    const-string v4, "LOCATION_OBJECT_UID subscribe failed"

    invoke-static {v1, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    move v4, v7

    move v5, v7

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySubscribeResponse(IIZBI)V

    .line 306
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 262
    :sswitch_data_0
    .sparse-switch
        0xb1 -> :sswitch_3
        0xb3 -> :sswitch_4
        0xb4 -> :sswitch_2
        0xb103 -> :sswitch_1
        0xb104 -> :sswitch_0
    .end sparse-switch
.end method

.method public setDeviceMngr(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 3
    .param p1, "devMgr"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .prologue
    .line 161
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsLocationSink.setDeviceMngr() - Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 165
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.setDeviceMngr() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 24
    .param p1, "locationData"    # Ljava/lang/String;

    .prologue
    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v2

    if-nez v2, :cond_0

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x5

    invoke-virtual/range {v2 .. v12}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    .line 182
    :cond_0
    if-nez p1, :cond_1

    .line 184
    const-string v2, "TMSLocSink"

    const-string v3, "AcsLocationSink.setLocation() - invalid data"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 187
    :cond_1
    new-instance v21, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 188
    .local v21, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 189
    const-string v2, "latitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 190
    .local v18, "latitute":Ljava/lang/String;
    if-eqz v18, :cond_2

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 191
    const-string v2, "latitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLatitude:D

    .line 193
    :cond_2
    const-string v2, "longitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 194
    .local v20, "longitude":Ljava/lang/String;
    if-eqz v20, :cond_3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    .line 195
    const-string v2, "longitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLongitude:D

    .line 197
    :cond_3
    const-string v2, "altitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 198
    .local v14, "altitude":Ljava/lang/String;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    .line 199
    const-string v2, "altitude"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitude:D

    .line 201
    :cond_4
    const-string v2, "accuracy"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 202
    .local v13, "accuracy":Ljava/lang/String;
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    .line 203
    const-string v2, "accuracy"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAccuracy:F

    .line 205
    :cond_5
    const-string v2, "altitudeAccuracy"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 206
    .local v15, "altitudeAccuracy":Ljava/lang/String;
    if-eqz v15, :cond_6

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_6

    .line 207
    const-string v2, "altitudeAccuracy"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitudeAccuracy:D

    .line 209
    :cond_6
    const-string v2, "heading"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 210
    .local v16, "heading":Ljava/lang/String;
    if-eqz v16, :cond_7

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    .line 211
    const-string v2, "heading"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mHeading:D

    .line 213
    :cond_7
    const-string v2, "speed"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 214
    .local v22, "speed":Ljava/lang/String;
    if-eqz v22, :cond_8

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    .line 215
    const-string v2, "speed"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSpeed:F

    .line 217
    :cond_8
    const-string v2, "timestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 218
    .local v23, "timestamp":Ljava/lang/String;
    if-eqz v23, :cond_9

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_9

    .line 219
    const-string v2, "timestamp"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mTimestamp:J

    .line 221
    :cond_9
    const-string v2, "TMSLocSink"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AcsLocationSink.setLocation(): mLatitude-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLatitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->mLongitude->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLongitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->mAccuracy->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAccuracy:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 222
    const-string v4, "->mAltitudeAccuracy->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitudeAccuracy:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->mHeading->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mHeading:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->mSpeed->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSpeed:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->mTimestamp->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mTimestamp:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 221
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    new-instance v19, Landroid/location/Location;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 225
    .local v19, "loc":Landroid/location/Location;
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLatitude:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_a

    .line 226
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLatitude:D

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 228
    :cond_a
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLongitude:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_b

    .line 229
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLongitude:D

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 231
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAccuracy:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_c

    .line 232
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAccuracy:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/location/Location;->setAccuracy(F)V

    .line 234
    :cond_c
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitude:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_d

    .line 235
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mAltitude:D

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setAltitude(D)V

    .line 237
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSpeed:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_e

    .line 238
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSpeed:F

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/location/Location;->setSpeed(F)V

    .line 241
    :cond_e
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mTimestamp:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 242
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mTimestamp:J

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 245
    :cond_f
    const-string v2, "TMSLocSink"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Location Obj ID in mGetObjSet"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v4, 0x572a6461

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mGetObjSet:Ljava/util/Set;

    const v3, 0x572a6461

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    const v3, 0x572a6461

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 248
    :cond_10
    const-string v2, "TMSLocSink"

    const-string v3, "calling getResponseForLocationData"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->getResponseForLocationData()V

    .line 252
    :cond_11
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0}, Landroid/location/LocationManager;->setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_1
    const-string v2, "TMSLocSink"

    const-string v3, "AcsGpsService.setLocation() - updated location information"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 253
    :catch_0
    move-exception v17

    .line 254
    .local v17, "ie":Ljava/lang/IllegalArgumentException;
    const-string v2, "TMSLocSink"

    const-string v3, "AcsGpsService.setLocation() - Rxed incomplete location details"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setMinTime(J)V
    .locals 6
    .param p1, "time"    # J

    .prologue
    .line 169
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.setMinTime() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mMinTime:J

    .line 172
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mMinTime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 174
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.setMinTime() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public setObject(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "objectId"    # I
    .param p2, "object2"    # Landroid/os/Bundle;

    .prologue
    .line 377
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    .line 378
    .local v0, "dataConnMgr":Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    if-eqz v0, :cond_0

    .line 379
    const-string v1, "TMSLocSink"

    const-string v2, "Set object is not supported. Hence returning false"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->getLocationServiceID()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;->notifySetDataObjectResponse(IIZ)V

    .line 383
    :cond_0
    return-void
.end method

.method public startLocationSink()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 119
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.startLocationSink() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 122
    const-string v0, "TMSLocSink"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return v2

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-nez v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    const/4 v10, 0x5

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    invoke-virtual/range {v0 .. v10}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1, v11}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    iget-wide v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mMinTime:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 132
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.startLocationSink() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v11

    .line 133
    goto :goto_0
.end method

.method public stopLocationSink()Z
    .locals 2

    .prologue
    .line 137
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.stopLocationSink() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 141
    const-string v0, "TMSLocSink"

    const-string v1, "location obj is null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x0

    .line 150
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 145
    const-string v0, "TMSLocSink"

    const-string v1, "mlocation.getProvider is not null "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mLocation:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mProvider:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeTestProvider(Ljava/lang/String;)V

    .line 149
    :cond_1
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink.stopLocationSink() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public subScribeRequest(I)V
    .locals 3
    .param p1, "objectId"    # I

    .prologue
    .line 362
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsLocationSink: subScribeRequest :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const v0, 0x572a6461

    if-ne p1, v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    const/4 v1, 0x1

    const v2, 0xea60

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->subscribeLocation(II)V

    .line 373
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v0, "TMSLocSink"

    const-string v1, "AcsLocationSink: mDevMgr = null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    :cond_1
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsLocationSink: Invalid object id :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unsubScribeRequest(I)V
    .locals 3
    .param p1, "objectId"    # I

    .prologue
    .line 386
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsGpsService: unsubScribeRequest :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mSubscribeSet:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->cancelSubcribeLocation()V

    .line 393
    :goto_0
    return-void

    .line 391
    :cond_0
    const-string v0, "TMSLocSink"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsLocationSink: Invalid object id :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
