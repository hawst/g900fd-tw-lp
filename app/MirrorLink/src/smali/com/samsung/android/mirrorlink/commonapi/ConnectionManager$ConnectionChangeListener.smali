.class Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;
.super Ljava/lang/Object;
.source "ConnectionManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionChangeListener"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode()[I
    .locals 3

    .prologue
    .line 305
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->values()[Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_AUDIO_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CLIENT_PIXEL_FORMAT_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DEVICE_INFO:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DISPLAY_CONFIG_CHANGED:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_DRIVEMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_FB_BLOCKING:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MIC_OPEN:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_MLSESSION_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_NIGHTMODE_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_REMOTE_DISPLAY_CONNECTION:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;)V
    .locals 0

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;-><init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V

    return-void
.end method


# virtual methods
.method public onEvent(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 312
    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;->$SWITCH_TABLE$com$samsung$android$mirrorlink$commonapi$ICommonApiCallBack$ResultCode()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 322
    .end local p2    # "obj":Ljava/lang/Object;
    :goto_0
    return-void

    .line 314
    .restart local p2    # "obj":Ljava/lang/Object;
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$ConnectionChangeListener;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "obj":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyMirrorLinkSessionChanged(Z)V

    goto :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
