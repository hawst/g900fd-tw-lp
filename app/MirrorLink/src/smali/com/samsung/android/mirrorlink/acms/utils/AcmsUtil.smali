.class public Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;
.super Ljava/lang/Object;
.source "AcmsUtil.java"


# static fields
.field public static final ACMS_APP_ADDED:Ljava/lang/String; = "android.intent.action.PACKAGE_ADDED"

.field public static final ACMS_APP_CHANGED:Ljava/lang/String; = "android.intent.action.PACKAGE_CHANGED"

.field public static final ACMS_APP_CONN_CHANGE:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field public static final ACMS_APP_INSTALL:Ljava/lang/String; = "android.intent.action.PACKAGE_INSTALL"

.field public static final ACMS_APP_REMOVED:Ljava/lang/String; = "android.intent.action.PACKAGE_REMOVED"

.field public static final ACMS_APP_REPLACED:Ljava/lang/String; = "android.intent.action.PACKAGE_REPLACED"

.field public static final ACMS_APP_TIMEOUT:Ljava/lang/String; = "com.samsung.android.mirrorlink.acms.ALARM_TIMEOUT_EVENT"

.field public static final ACMS_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACMS_CORE_INIT:I = 0x1

.field public static final ACMS_EXTRA_ML_STATE:Ljava/lang/String; = "mlstatus"

.field public static final ACMS_INTENT_ML_STATE:Ljava/lang/String; = "com.samsung.android.mirrorlink.ML_STATE"

.field public static final ACMS_ML_STARTED:I = 0x1

.field public static final ACMS_PENDING_ALARM:I = 0x1

.field private static final ANDROID_OS:Ljava/lang/String; = "Android"

.field public static final APP_CHANGED:I = 0x4

.field public static final APP_INSTALLED:I = 0x2

.field public static final APP_REMOVED:I = 0x5

.field public static final APP_REPLACED:I = 0x6

.field private static final BUILD_TRACK:Ljava/lang/String; = "Karajan"

.field public static CCC_ROOT_CERT_FILENAME:Ljava/lang/String; = null

.field public static final CERT_EXTENSION_AIA:Ljava/lang/String; = "1.3.6.1.5.5.7.1.1"

.field public static final CERT_EXTENSION_APINFO_XML:Ljava/lang/String; = "1.3.6.1.4.1.41577.2.1"

.field public static final CERT_EXTENSION_CLIENT_ID:Ljava/lang/String; = "1.3.6.1.4.1.41577.3.3"

.field public static final CERT_EXTENSION_DEV_ID:Ljava/lang/String; = "1.3.6.1.4.1.41577.3.1"

.field public static final CERT_EXTENSION_DEV_SERVER_ID:Ljava/lang/String; = "1.3.6.1.4.1.41577.3.2"

.field private static final CHARSET_US_ASCII:Ljava/lang/String; = "US-ASCII"

.field public static final CHECK_ML_APP:I = 0x9

.field public static CTS_ROOT_CERT_FILENAME:Ljava/lang/String; = null

.field public static final DEVID_PREFERENCES:Ljava/lang/String; = "DevIdPref"

.field public static final DEVID_PREFERENCES_DEFAULT:Ljava/lang/String; = "No Value"

.field public static final DEVID_PREFERENCES_KEY:Ljava/lang/String; = "DevId"

.field public static final DEVMODE_PREFERENCES:Ljava/lang/String; = "DevModePref"

.field public static final DEVMODE_PREFERENCES_DEFAULT:Z = true

.field public static final DEVMODE_PREFERENCES_KEY:Ljava/lang/String; = "DevMode"

.field public static final EXTRA_APPID:Ljava/lang/String; = "android.mirrorlink.acms.extra.APPID"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "mirrorlink.acms.extra.PACKAGE_NAME"

.field public static final EXTRA_SCHEDULE_REASON:Ljava/lang/String; = "android.mirrorlink.acms.extra.SCHEDULE_REASON"

.field public static final FETCH_RETRY_QUERY_PERIOD:Ljava/lang/String; = "fetch retry query period"

.field public static final GRACE_PERIOD:Ljava/lang/String; = "grace period"

.field public static final KEYSTORE_ALGORITHM:Ljava/lang/String; = "RSA"

.field public static final KEYSTORE_INSTANCE:Ljava/lang/String; = "PKCS12"

.field public static final KEYSTORE_NAME:Ljava/lang/String; = "Acms_Keystore"

.field public static final KEYSTORE_PWD:Ljava/lang/String; = "8Q!@hs1#{1:2c$h"

.field private static final LOG_TAG:Ljava/lang/String; = "AcmsUtil"

.field private static final MIRROR_LINK:Ljava/lang/String; = "MirrorLink"

.field public static final ML_STATE_CHANGE:I = 0x8

.field public static final QUERY_PERIOD:Ljava/lang/String; = "query period"

.field public static final RESTICTED_GRACE_PERIOD_EXPIRED:I = 0x1

.field public static final REVOCATION_CHECK_ACTION:Ljava/lang/String; = "mirrorlink.acms.intent.action.REVOCATION_CHECK"

.field public static SELF_SIGNED_CERT_FILENAME:Ljava/lang/String; = null

.field private static final UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final UNRESTICTED_GRACE_PERIOD_EXPIRED:I = 0x2

.field public static isDevIdInsertFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "self-signed.ccc.crt"

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->SELF_SIGNED_CERT_FILENAME:Ljava/lang/String;

    .line 50
    const-string v0, "ccc.crt"

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CCC_ROOT_CERT_FILENAME:Ljava/lang/String;

    .line 51
    const-string v0, "cts-root.crt"

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CTS_ROOT_CERT_FILENAME:Ljava/lang/String;

    .line 106
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevIdInsertFlag:Z

    .line 115
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelAlarm(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 206
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.mirrorlink.acms.ALARM_TIMEOUT_EVENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 210
    .local v2, "sender":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 209
    check-cast v0, Landroid/app/AlarmManager;

    .line 211
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 212
    return-void
.end method

.method public static getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 278
    const/4 v3, 0x0

    .line 280
    .local v3, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 281
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    .end local v2    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    if-nez v3, :cond_0

    .line 287
    const-string v3, "Unknown"

    .line 290
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 291
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MirrorLink"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Karajan"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 292
    const-string v5, "Android"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 290
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "defaultUserAgent":Ljava/lang/String;
    return-object v0

    .line 282
    .end local v0    # "defaultUserAgent":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 284
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getIsDevInsert()Z
    .locals 3

    .prologue
    .line 271
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getIsDevInsert: Value is  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevIdInsertFlag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    sget-boolean v0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevIdInsertFlag:Z

    return v0
.end method

.method public static getStringFromInputStream(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 7
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 237
    const/4 v0, 0x0

    .line 238
    .local v0, "br":Ljava/io/BufferedReader;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    .local v4, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    .line 244
    const-string v6, "US-ASCII"

    invoke-static {v6}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 243
    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 252
    if-eqz v1, :cond_3

    .line 254
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v0, v1

    .line 261
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 246
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_1
    :try_start_3
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 249
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 250
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 252
    if-eqz v0, :cond_0

    .line 254
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 255
    :catch_1
    move-exception v2

    .line 256
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 251
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 252
    :goto_3
    if-eqz v0, :cond_2

    .line 254
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 259
    :cond_2
    :goto_4
    throw v5

    .line 255
    :catch_2
    move-exception v2

    .line 256
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 255
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 256
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 251
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 249
    :catch_4
    move-exception v2

    goto :goto_2
.end method

.method public static isDataConnected(Landroid/content/Context;)Z
    .locals 7
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 176
    const/4 v2, 0x0

    .line 179
    .local v2, "isConnected":Z
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 178
    check-cast v1, Landroid/net/ConnectivityManager;

    .line 180
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 181
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    .line 183
    const-string v4, "AcmsUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isDataConnected() "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 186
    .end local v2    # "isConnected":Z
    .local v3, "isConnected":I
    :goto_0
    return v3

    .end local v3    # "isConnected":I
    .restart local v2    # "isConnected":Z
    :cond_0
    move v3, v2

    .restart local v3    # "isConnected":I
    goto :goto_0
.end method

.method public static isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z
    .locals 1
    .param p0, "appType"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .prologue
    .line 230
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-eq p0, v0, :cond_0

    .line 231
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    if-eq p0, v0, :cond_0

    .line 230
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isMlConnectedInQueryPeriod(JJJ)Z
    .locals 8
    .param p0, "scheduledTime"    # J
    .param p2, "mlConnectedTime"    # J
    .param p4, "queryPeriod"    # J

    .prologue
    .line 216
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 217
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 218
    const/4 v3, 0x5

    long-to-double v4, p4

    const-wide/high16 v6, 0x4038000000000000L    # 24.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    neg-int v4, v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 219
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 220
    .local v1, "oldTime":J
    const-string v3, "AcmsUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mlConnectedTime is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OldTime is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Schedule time is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-wide/16 v3, -0x1

    cmp-long v3, p2, v3

    if-eqz v3, :cond_0

    cmp-long v3, p2, v1

    if-lez v3, :cond_0

    .line 222
    cmp-long v3, p2, p0

    if-gez v3, :cond_0

    .line 223
    const/4 v3, 0x1

    .line 225
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static printCert(Ljava/security/cert/X509Certificate;)V
    .locals 3
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 125
    const-string v0, "AcmsUtil"

    const-string v1, "-------------CERITFICATE---------------"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() issued : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getNotBefore()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() SN# "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "printCert() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSigAlgName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v0, "AcmsUtil"

    const-string v1, "----------------------------------------"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public static printCerts([Ljava/security/cert/X509Certificate;)V
    .locals 3
    .param p0, "certs"    # [Ljava/security/cert/X509Certificate;

    .prologue
    .line 118
    const-string v1, "AcmsUtil"

    const-string v2, "-------------CERITFICATE CHAIN----------------"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 122
    return-void

    .line 119
    :cond_0
    aget-object v0, p0, v1

    .line 120
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->printCert(Ljava/security/cert/X509Certificate;)V

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static setAlarm(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "schedulingReason"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 191
    const-string v3, "AcmsUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SetAlarm: Setting The alarm for appId "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 192
    const-string v5, "Reason "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 191
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 194
    check-cast v0, Landroid/app/AlarmManager;

    .line 196
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.mirrorlink.acms.ALARM_TIMEOUT_EVENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 197
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.mirrorlink.acms.extra.APPID"

    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v3, "android.mirrorlink.acms.extra.SCHEDULE_REASON"

    invoke-virtual {v1, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const/high16 v3, 0x10000000

    .line 199
    invoke-static {p0, v6, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 202
    .local v2, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v6, p1, p2, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 203
    return-void
.end method

.method public static setIsDevInsert(Z)V
    .locals 3
    .param p0, "value"    # Z

    .prologue
    .line 266
    const-string v0, "AcmsUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setIsDevInsert to  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sput-boolean p0, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevIdInsertFlag:Z

    .line 268
    return-void
.end method
