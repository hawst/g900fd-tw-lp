.class public Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;
.super Ljava/lang/Object;
.source "AcmsAppsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;,
        Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$CertificateTags;
    }
.end annotation


# static fields
.field private static final CERTIFICATE_TRUSTLEVEL:I = 0xa0

.field private static final REGISTERED_TRUSTLEVEL:I = 0x80

.field private static final STATE_OCSPCERT_MLAWARE_UNCHECKED:I = 0xc

.field private static final TAG:Ljava/lang/String; = "AcmsAppsManager"


# instance fields
.field private mAcmsApps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

.field private mContext:Landroid/content/Context;

.field private final mCurPolicy:I

.field private mUnsupportedAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    .line 69
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    const-string v2, "PreferencePolicy"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 71
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "policy"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mCurPolicy:I

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mUnsupportedAppList:Ljava/util/List;

    .line 73
    return-void
.end method

.method private checkFordeletedApps(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    .local p1, "newApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "AcmsAppsManager"

    const-string v3, "Enter checkFordeletedApps to remove entries"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 505
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 519
    const-string v2, "AcmsAppsManager"

    const-string v3, "Exit updateMap"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    return-void

    .line 506
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 507
    .local v0, "appEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 508
    const-string v3, "AcmsAppsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v2, "Package "

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 509
    const-string v4, " is removed. Hence removing from map"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 508
    invoke-static {v3, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v2, "AcmsAppsManager"

    const-string v3, "App entry is removed from acms. Hence need to update appList"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    if-eqz v2, :cond_2

    .line 513
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v3, v2}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;->onRemoved(Ljava/lang/String;)V

    .line 515
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->delete(Ljava/lang/String;)Z

    .line 516
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method private fillDefaultAppInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Landroid/content/pm/ResolveInfo;)V
    .locals 5
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .param p2, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    const/4 v4, 0x0

    .line 477
    const-string v1, "AcmsAppsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fillDefaultAppInfo enter for app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iput v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 479
    iput v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 480
    iput v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 481
    const-string v1, "application"

    iput-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 482
    iput v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 483
    const-string v1, "mixed"

    iput-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    .line 484
    if-eqz p2, :cond_1

    .line 485
    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v0, v1, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    .line 486
    .local v0, "orientation":I
    if-eqz v0, :cond_0

    .line 487
    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 488
    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 489
    :cond_0
    const-string v1, "landscape"

    iput-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    .line 498
    .end local v0    # "orientation":I
    :cond_1
    :goto_0
    const-string v1, "AcmsAppsManager"

    const-string v2, "fillDefaultAppInfo exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    return-void

    .line 490
    .restart local v0    # "orientation":I
    :cond_2
    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 491
    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    .line 492
    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 493
    :cond_3
    const-string v1, "portrait"

    iput-object v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    goto :goto_0
.end method

.method private fillFromXml(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 17
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .param p2, "dbInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 525
    const-string v14, "AcmsAppsManager"

    const-string v15, "Enter fillFromXml "

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v7

    .line 527
    .local v7, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v14, 0x1

    invoke-virtual {v7, v14}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 528
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v13

    .line 529
    .local v13, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v14, Ljava/io/StringReader;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v13, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 531
    const/4 v12, 0x0

    .line 533
    .local v12, "text":Ljava/lang/String;
    const/4 v3, 0x0

    .line 534
    .local v3, "displayInfo":Z
    const/4 v2, 0x0

    .line 535
    .local v2, "audioInfo":Z
    const/4 v5, 0x0

    .line 536
    .local v5, "entityTag":Z
    const/4 v9, 0x0

    .line 537
    .local v9, "isIconInfo":Z
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 538
    .local v6, "eventType":I
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    iput-object v15, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    .line 539
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getAppCertEntityInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    move-result-object v1

    .line 540
    .local v1, "appCertificationInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getLinkedList()Ljava/util/LinkedList;

    move-result-object v14

    iput-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    .line 541
    const/4 v8, 0x0

    .line 542
    .local v8, "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    :goto_0
    const/4 v14, 0x1

    if-ne v6, v14, :cond_0

    .line 671
    return-object p1

    .line 543
    :cond_0
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    .line 544
    .local v11, "tagname":Ljava/lang/String;
    packed-switch v6, :pswitch_data_0

    .line 668
    :cond_1
    :goto_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    goto :goto_0

    .line 546
    :pswitch_0
    const-string v14, "displayInfo"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 547
    const/4 v3, 0x1

    .line 548
    goto :goto_1

    :cond_2
    const-string v14, "audioInfo"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 549
    const/4 v2, 0x1

    .line 550
    goto :goto_1

    :cond_3
    const-string v14, "iconList"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 551
    const-string v14, "AcmsAppsManager"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Initializing icon list "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getArrayList()Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p1

    iput-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    goto :goto_1

    .line 553
    :cond_4
    const-string v14, "entity"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 554
    const/4 v5, 0x1

    .line 555
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getAppCertEntityInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    move-result-object v1

    .line 556
    goto :goto_1

    :cond_5
    const-string v14, "targetList"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    if-eqz v5, :cond_6

    .line 557
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getLinkedList()Ljava/util/LinkedList;

    move-result-object v14

    iput-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    goto :goto_1

    .line 558
    :cond_6
    const-string v14, "serviceList"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    if-eqz v5, :cond_7

    .line 559
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getLinkedList()Ljava/util/LinkedList;

    move-result-object v14

    iput-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    goto :goto_1

    .line 560
    :cond_7
    const-string v14, "icon"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 561
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getIconInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    move-result-object v8

    .line 562
    const/4 v9, 0x1

    .line 564
    goto/16 :goto_1

    .line 566
    :pswitch_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v12

    .line 567
    goto/16 :goto_1

    .line 570
    :pswitch_2
    const-string v14, "providerName"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 571
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderName:Ljava/lang/String;

    .line 661
    :cond_8
    :goto_2
    const-string v12, ""

    .line 662
    goto/16 :goto_1

    .line 572
    :cond_9
    const-string v14, "appCategory"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 573
    if-eqz v12, :cond_8

    .line 574
    const/4 v14, 0x2

    invoke-virtual {v12, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x10

    invoke-static {v14, v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v14

    long-to-int v14, v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    goto :goto_2

    .line 577
    :cond_a
    const-string v14, "variant"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 578
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mVariant:Ljava/lang/String;

    goto :goto_2

    .line 579
    :cond_b
    const-string v14, "contentCategory"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 580
    if-eqz v3, :cond_d

    .line 582
    if-eqz v12, :cond_c

    .line 584
    const/4 v14, 0x2

    :try_start_0
    invoke-virtual {v12, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x10

    .line 583
    invoke-static {v14, v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v14

    long-to-int v14, v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 592
    :cond_c
    :goto_3
    const/4 v3, 0x0

    .line 593
    goto :goto_2

    .line 587
    :catch_0
    move-exception v4

    .line 588
    .local v4, "e":Ljava/lang/NumberFormatException;
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    goto :goto_3

    .line 589
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v10

    .line 590
    .local v10, "se":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    goto :goto_3

    .line 593
    .end local v10    # "se":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_d
    if-eqz v2, :cond_8

    .line 595
    if-eqz v12, :cond_e

    .line 597
    const/4 v14, 0x2

    :try_start_1
    invoke-virtual {v12, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x10

    .line 596
    invoke-static {v14, v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v14

    long-to-int v14, v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_3

    .line 604
    :cond_e
    :goto_4
    const/4 v2, 0x0

    .line 606
    goto :goto_2

    .line 599
    :catch_2
    move-exception v4

    .line 600
    .restart local v4    # "e":Ljava/lang/NumberFormatException;
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    goto :goto_4

    .line 601
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v10

    .line 602
    .restart local v10    # "se":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    iput v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    goto :goto_4

    .line 606
    .end local v10    # "se":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_f
    const-string v14, "audioType"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 607
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    goto/16 :goto_2

    .line 608
    :cond_10
    const-string v14, "appIdentifier"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 609
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iput-object v12, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppId:Ljava/lang/String;

    goto/16 :goto_2

    .line 610
    :cond_11
    const-string v14, "appUUID"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_12

    .line 611
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iput-object v12, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppUUID:Ljava/lang/String;

    goto/16 :goto_2

    .line 612
    :cond_12
    const-string v14, "properties"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 613
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iput-object v12, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mProperties:Ljava/lang/String;

    goto/16 :goto_2

    .line 614
    :cond_13
    const-string v14, "orientation"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_14

    .line 615
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mOrientation:Ljava/lang/String;

    goto/16 :goto_2

    .line 616
    :cond_14
    const-string v14, "providerURL"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_15

    .line 617
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mProviderURL:Ljava/lang/String;

    goto/16 :goto_2

    .line 618
    :cond_15
    const-string v14, "description"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_16

    .line 619
    move-object/from16 v0, p1

    iput-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    goto/16 :goto_2

    .line 620
    :cond_16
    const-string v14, "mimetype"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17

    if-eqz v9, :cond_17

    .line 621
    iput-object v12, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    goto/16 :goto_2

    .line 622
    :cond_17
    const-string v14, "height"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_18

    if-eqz v9, :cond_18

    .line 623
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iput v14, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mHeight:I

    goto/16 :goto_2

    .line 624
    :cond_18
    const-string v14, "width"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_19

    if-eqz v9, :cond_19

    .line 625
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iput v14, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mWidth:I

    goto/16 :goto_2

    .line 626
    :cond_19
    const-string v14, "depth"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1a

    if-eqz v9, :cond_1a

    .line 627
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    iput v14, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mDepth:I

    goto/16 :goto_2

    .line 628
    :cond_1a
    const-string v14, "url"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1b

    if-eqz v9, :cond_1b

    .line 629
    iput-object v12, v8, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    goto/16 :goto_2

    .line 630
    :cond_1b
    const-string v14, "icon"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1c

    .line 631
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v14, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 632
    const/4 v9, 0x0

    .line 633
    goto/16 :goto_2

    :cond_1c
    if-eqz v5, :cond_1d

    const-string v14, "name"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1d

    .line 634
    iput-object v12, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    goto/16 :goto_2

    .line 635
    :cond_1d
    if-eqz v5, :cond_1e

    const-string v14, "target"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1e

    .line 636
    if-eqz v12, :cond_8

    .line 637
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getLinkedList()Ljava/util/LinkedList;

    move-result-object v14

    iput-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    .line 638
    const-string v14, ","

    invoke-virtual {v12, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    iput-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    goto/16 :goto_2

    .line 640
    :cond_1e
    if-eqz v5, :cond_1f

    const-string v14, "service"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1f

    .line 641
    iget-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    if-eqz v14, :cond_8

    .line 642
    iget-object v14, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    invoke-interface {v14, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 644
    :cond_1f
    if-eqz v5, :cond_20

    const-string v14, "restricted"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_20

    .line 645
    iput-object v12, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    goto/16 :goto_2

    .line 646
    :cond_20
    if-eqz v5, :cond_21

    const-string v14, "nonRestricted"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_21

    .line 647
    iput-object v12, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    goto/16 :goto_2

    .line 648
    :cond_21
    const-string v14, "Signature"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_22

    .line 649
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iput-object v12, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mSignature:Ljava/lang/String;

    goto/16 :goto_2

    .line 650
    :cond_22
    const-string v14, "audioInfo"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_23

    .line 651
    const/4 v2, 0x0

    .line 653
    goto/16 :goto_2

    :cond_23
    const-string v14, "displayInfo"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_24

    .line 654
    const/4 v3, 0x0

    .line 656
    goto/16 :goto_2

    :cond_24
    if-eqz v5, :cond_8

    const-string v14, "entity"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 657
    const/4 v5, 0x0

    .line 658
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v14, v14, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 659
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 544
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private fillIconInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 5
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 280
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    if-nez v2, :cond_1

    .line 281
    const-string v2, "AcmsAppsManager"

    const-string v3, "Icon list is null. Filling default icon details"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    .line 283
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getIconInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    move-result-object v1

    .line 284
    .local v1, "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    .line 285
    const-string v2, "image/png"

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    .line 286
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    .end local v1    # "iconInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    :cond_0
    return-object p1

    .line 289
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 290
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    const-string v3, "image/png"

    iput-object v3, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mMimetype:Ljava/lang/String;

    .line 291
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mIconList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;->mUrl:Ljava/lang/String;

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getAppCertEntityInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    .locals 1

    .prologue
    .line 675
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;-><init>()V

    return-object v0
.end method

.method private getAppCertInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;-><init>()V

    return-object v0
.end method

.method private getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 207
    const-string v6, "AcmsAppsManager"

    const-string v7, "Get Application Label"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/4 v4, 0x0

    .line 209
    .local v4, "localizedLabel":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 212
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/16 v6, 0x80

    :try_start_0
    invoke-virtual {v5, p1, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 214
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v2, Landroid/content/res/Configuration;

    invoke-direct {v2}, Landroid/content/res/Configuration;-><init>()V

    .line 216
    .local v2, "config":Landroid/content/res/Configuration;
    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    invoke-direct {v6, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v6, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 217
    invoke-virtual {v5, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .line 218
    .local v1, "appRes":Landroid/content/res/Resources;
    if-eqz v1, :cond_0

    .line 219
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 221
    if-eqz v0, :cond_0

    iget v6, v0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    if-eqz v6, :cond_0

    .line 222
    iget v6, v0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :cond_0
    move-object v6, v4

    .line 235
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "appRes":Landroid/content/res/Resources;
    .end local v2    # "config":Landroid/content/res/Configuration;
    :goto_0
    return-object v6

    .line 225
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "AcmsAppsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Error getting Application Info"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 228
    sget-object v7, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 229
    const-string v8, "packagename=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    .line 230
    aput-object p1, v9, v10

    .line 227
    invoke-virtual {v6, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 232
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private getClientIds(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 374
    if-nez p1, :cond_0

    .line 375
    const-string v6, ""

    .line 392
    :goto_0
    return-object v6

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 378
    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    .line 379
    const-string v3, "clientIds"

    aput-object v3, v2, v5

    .line 380
    const-string v3, "appId=?"

    new-array v4, v4, [Ljava/lang/String;

    .line 381
    aput-object p1, v4, v5

    .line 382
    const/4 v5, 0x0

    .line 377
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 383
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 384
    const-string v6, ""

    goto :goto_0

    .line 386
    :cond_1
    const-string v6, ""

    .line 387
    .local v6, "clientIds":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 388
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 389
    const-string v0, "clientIds"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 391
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private getEntitiInfo()Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;
    .locals 1

    .prologue
    .line 473
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;-><init>()V

    return-object v0
.end method

.method private getIconInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;
    .locals 1

    .prologue
    .line 679
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$IconInfo;-><init>()V

    return-object v0
.end method

.method private getLinkedList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 683
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    return-object v0
.end method

.method private getResolveInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Landroid/content/pm/ResolveInfo;
    .locals 13
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    const/4 v12, 0x0

    .line 317
    const/4 v4, 0x0

    .line 318
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    const/4 v2, 0x0

    .line 319
    .local v2, "checked":Z
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 320
    .local v6, "mPkgMngr":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 322
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v7, Landroid/content/Intent;

    const-string v10, "com.mirrorlink.android.app.LAUNCH"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 323
    .local v7, "mainIntent":Landroid/content/Intent;
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 324
    invoke-virtual {v6, v7, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 326
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    if-lt v3, v10, :cond_1

    .line 339
    :goto_1
    if-nez v2, :cond_0

    .line 340
    const-string v10, "AcmsAppsManager"

    const-string v11, "This is not a launchable app"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    new-instance v5, Landroid/content/Intent;

    const-string v10, "android.intent.action.VIEW"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .local v5, "intent":Landroid/content/Intent;
    const/high16 v10, 0x30000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 343
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/high16 v11, 0x10000

    invoke-virtual {v10, v5, v11}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 345
    if-nez v4, :cond_3

    .line 346
    const-string v10, "AcmsAppsManager"

    const-string v11, "AcmsAppsManager.getResolveInfo is null"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_2
    return-object v4

    .line 327
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 328
    .local v8, "temp_info":Landroid/content/pm/ResolveInfo;
    iget-object v0, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 329
    .local v0, "activity":Landroid/content/pm/ActivityInfo;
    iget-object v10, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v11, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 330
    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.mirrorlink.android.app.LAUNCH"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331
    .local v9, "temp_mainIntent":Landroid/content/Intent;
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    iget-object v10, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v10, v9, v12}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 334
    const/4 v2, 0x1

    .line 335
    goto :goto_1

    .line 326
    .end local v9    # "temp_mainIntent":Landroid/content/Intent;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 348
    .end local v0    # "activity":Landroid/content/pm/ActivityInfo;
    .end local v8    # "temp_info":Landroid/content/pm/ResolveInfo;
    .restart local v5    # "intent":Landroid/content/Intent;
    :cond_3
    const-string v10, "NA"

    iput-object v10, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    goto :goto_2
.end method

.method private getTmsAppInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    return-object v0
.end method

.method private insertIntoAppData(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 5
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 396
    const-string v2, "AcmsAppsManager"

    const-string v3, "Enter insert App data "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    .line 398
    .local v0, "dbInfo":Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 399
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 400
    iget-boolean v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setAppCertified(Z)V

    .line 401
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v2, :cond_0

    .line 402
    const-string v2, "AcmsAppsManager"

    const-string v3, "Certinfo is not null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCertifiedAppId(Ljava/lang/String;)V

    .line 404
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCertifiedAppUUID(Ljava/lang/String;)V

    .line 405
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mProperties:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setProperties(Ljava/lang/String;)V

    .line 406
    iget-object v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v2, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mSignature:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setSignature(Ljava/lang/String;)V

    .line 408
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v2

    long-to-int v1, v2

    .line 409
    .local v1, "rowNum":I
    const-string v2, "AcmsAppsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Insert to db certified app "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "certified : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 410
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->isAppCertified()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 409
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-nez v1, :cond_2

    .line 413
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v1

    .line 414
    if-nez v1, :cond_2

    .line 415
    const-string v2, "AcmsAppsManager"

    const-string v3, "Entry not found in db and not able to insert app"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :goto_0
    return-void

    .line 420
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    if-eqz v2, :cond_3

    .line 421
    const-string v2, "AcmsAppsManager"

    const-string v3, "App state is updated in acms "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;->onUpdated(Ljava/lang/String;)V

    .line 424
    :cond_3
    iput v1, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 425
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    iget-object v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    const-string v2, "AcmsAppsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " Added package : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with appID"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const-string v2, "AcmsAppsManager"

    const-string v3, "Exit insert AppData"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertIntoEntityData(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V
    .locals 11
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    const/4 v10, 0x0

    .line 432
    const-string v6, "AcmsAppsManager"

    const-string v7, "Enter insert Entity data "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-object v6, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v6, :cond_0

    iget-object v6, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 434
    iget-object v6, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 435
    :cond_0
    const-string v6, "AcmsAppsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No entity fields present for current app "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :goto_0
    return-void

    .line 439
    :cond_1
    iget-object v6, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v6, v6, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 468
    const-string v6, "AcmsAppsManager"

    const-string v7, "Exit insert EntityData"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 440
    .local v1, "info":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getEntitiInfo()Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;

    move-result-object v0

    .line 441
    .local v0, "eInfo":Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;
    iget-object v7, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setPackageName(Ljava/lang/String;)V

    .line 442
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setEntityName(Ljava/lang/String;)V

    .line 443
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    if-eqz v7, :cond_3

    .line 444
    const-string v5, ""

    .line 445
    .local v5, "targets":Ljava/lang/String;
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mTargetList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_5

    .line 449
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 450
    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setTargets(Ljava/lang/String;)V

    .line 452
    .end local v5    # "targets":Ljava/lang/String;
    :cond_3
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setRestricted(Ljava/lang/String;)V

    .line 453
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setNonRestricted(Ljava/lang/String;)V

    .line 454
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    if-eqz v7, :cond_4

    .line 455
    const-string v3, ""

    .line 456
    .local v3, "services":Ljava/lang/String;
    iget-object v7, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mServiceList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_6

    .line 459
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 460
    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;->setServices(Ljava/lang/String;)V

    .line 463
    .end local v3    # "services":Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/EntityInfo;)V

    goto :goto_1

    .line 445
    .restart local v5    # "targets":Ljava/lang/String;
    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 446
    .local v4, "target":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 456
    .end local v4    # "target":Ljava/lang/String;
    .end local v5    # "targets":Ljava/lang/String;
    .restart local v3    # "services":Ljava/lang/String;
    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 457
    .local v2, "service":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3
.end method

.method private queryBasedOnPolicy()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 355
    const-string v0, "AcmsAppsManager"

    const-string v1, "Enter queryBasedOnPolicy "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mCurPolicy:I

    if-nez v0, :cond_0

    .line 357
    const-string v0, "AcmsAppsManager"

    const-string v1, "Current policy is samsung policy"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v0, "AcmsAppsManager"

    const-string v1, "Enter queryBasedOnPolicy "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 360
    sget-object v2, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    const-string v3, "show=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 361
    const-string v7, "1"

    aput-object v7, v4, v6

    .line 359
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 367
    :goto_0
    return-object v0

    .line 365
    :cond_0
    const-string v0, "AcmsAppsManager"

    const-string v1, "Current policy is CCC policy"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v0, "AcmsAppsManager"

    const-string v1, "Enter queryBasedOnPolicy "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;->CONTENT_URI_APP_ENTRY:Landroid/net/Uri;

    .line 368
    sget-object v4, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    .line 367
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private setTrustLevelBasedonCertifiedStatus(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .locals 4
    .param p1, "appInfo"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    const/16 v3, 0xa0

    const/16 v2, 0x80

    .line 299
    const-string v0, "AcmsAppsManager"

    const-string v1, "Setting trust level"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-boolean v0, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "AcmsAppsManager"

    .line 302
    const-string v1, "Application is certified. Hence giving Application certificate trust level"

    .line 301
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iput v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 304
    iput v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 305
    iput v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 313
    :goto_0
    return-object p1

    .line 307
    :cond_0
    const-string v0, "AcmsAppsManager"

    .line 308
    const-string v1, "Application is not known as certified. Hence giving registered trust level"

    .line 307
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iput v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 310
    iput v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 311
    iput v2, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    goto :goto_0
.end method


# virtual methods
.method public deRegisterCallback()V
    .locals 2

    .prologue
    .line 706
    const-string v0, "AcmsAppsManager"

    const-string v1, "deRegisterCallback AppListChangeCallBack enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    .line 708
    const-string v0, "AcmsAppsManager"

    const-string v1, "deRegisterCallback AppListChangeCallBack exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    return-void
.end method

.method public getActivityLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 241
    const/4 v5, 0x0

    .line 242
    .local v5, "label":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 243
    .local v6, "pckMngr":Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.mirrorlink.android.app.LAUNCH"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 244
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v4, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const/4 v8, 0x0

    invoke-virtual {v6, v4, v8}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 246
    .local v7, "resInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v7, :cond_0

    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v8, :cond_0

    .line 247
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 248
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-nez v8, :cond_1

    .line 249
    :cond_0
    const-string v8, "AcmsAppsManager"

    .line 250
    const-string v9, "Activity label name not found. Using application label"

    .line 249
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 275
    :goto_0
    return-object v8

    .line 253
    :cond_1
    new-instance v2, Landroid/content/ComponentName;

    .line 254
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 253
    invoke-direct {v2, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    .local v2, "component":Landroid/content/ComponentName;
    const/high16 v8, 0x20000000

    .line 256
    :try_start_0
    invoke-virtual {v6, v2, v8}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 258
    .local v0, "actInfo":Landroid/content/pm/ActivityInfo;
    invoke-virtual {v6, v2}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v1

    .line 259
    .local v1, "appRes":Landroid/content/res/Resources;
    iget v8, v0, Landroid/content/pm/ActivityInfo;->labelRes:I

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 261
    if-nez v5, :cond_2

    .line 262
    const-string v8, "AcmsAppsManager"

    .line 263
    const-string v9, "Activity label name not found. Using application label"

    .line 262
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    goto :goto_0

    .line 266
    .end local v0    # "actInfo":Landroid/content/pm/ActivityInfo;
    .end local v1    # "appRes":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 267
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v8, "AcmsAppsManager"

    .line 268
    const-string v9, "Activity label name not found. Using application label"

    .line 267
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 270
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 271
    .local v3, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v8, "AcmsAppsManager"

    .line 272
    const-string v9, "Activity label resource not found. Using application label"

    .line 271
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getApplicationLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .end local v3    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v0    # "actInfo":Landroid/content/pm/ActivityInfo;
    .restart local v1    # "appRes":Landroid/content/res/Resources;
    :cond_2
    move-object v8, v5

    .line 275
    goto :goto_0
.end method

.method public queryApps()Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v10, 0x1

    .line 76
    const-string v9, "AcmsAppsManager"

    const-string v11, "Enter queryApps "

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    if-nez v9, :cond_0

    .line 80
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->queryBasedOnPolicy()Landroid/database/Cursor;

    move-result-object v3

    .line 83
    .local v3, "cursor":Landroid/database/Cursor;
    if-nez v3, :cond_1

    .line 84
    const-string v9, "AcmsAppsManager"

    const-string v10, "cursor is null"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    .line 194
    :goto_0
    return-object v9

    .line 87
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v6, "newApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_3

    .line 89
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 92
    :cond_2
    const-string v9, "packagename"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 91
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 93
    .local v7, "pkgName":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mUnsupportedAppList:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 94
    const-string v9, "AcmsAppsManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Client does not support this app"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v0, 0x0

    .line 184
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 186
    .end local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v7    # "pkgName":Ljava/lang/String;
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 187
    invoke-direct {p0, v6}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->checkFordeletedApps(Ljava/util/List;)V

    .line 188
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    if-nez v9, :cond_e

    .line 189
    :cond_4
    const-string v9, "AcmsAppsManager"

    const-string v10, "AcmsAppsManager.getApps returning null apps "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    goto :goto_0

    .line 98
    .restart local v7    # "pkgName":Ljava/lang/String;
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getTmsAppInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 99
    .restart local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getAppCertInfo()Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    .line 101
    const-string v9, "isCertified"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 100
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 101
    if-ne v9, v10, :cond_a

    move v9, v10

    .line 100
    :goto_2
    iput-boolean v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    .line 102
    iput-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 104
    const-string v9, "certType"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 105
    .local v8, "type":I
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iput v8, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->certType:I

    .line 106
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget v9, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->certType:I

    const/4 v11, 0x2

    if-eq v9, v11, :cond_6

    .line 107
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget v9, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->certType:I

    const/4 v11, 0x3

    if-ne v9, v11, :cond_7

    .line 108
    :cond_6
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    .line 109
    const-string v11, "appId"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 108
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getClientIds(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v9, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mBlackListedClients:Ljava/lang/String;

    .line 113
    :cond_7
    const-string v9, "certState"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 115
    .local v2, "certState":I
    iget-boolean v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-nez v9, :cond_8

    const/16 v9, 0xc

    if-ne v2, v9, :cond_9

    .line 116
    :cond_8
    const-string v9, "AcmsAppsManager"

    const-string v11, "Application is certified. Hence adding url"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "/"

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, ".crt"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertifcateUrl:Ljava/lang/String;

    .line 120
    :cond_9
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0, v9}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getActivityLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 121
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    if-nez v9, :cond_b

    .line 122
    const-string v9, "AcmsAppsManager"

    const-string v11, "Error in fetching app label"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x0

    .line 124
    goto/16 :goto_1

    .line 101
    .end local v2    # "certState":I
    .end local v8    # "type":I
    :cond_a
    const/4 v9, 0x0

    goto :goto_2

    .line 142
    .restart local v2    # "certState":I
    .restart local v8    # "type":I
    :cond_b
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->setTrustLevelBasedonCertifiedStatus(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 145
    const-string v9, "certInfo"

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 144
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 146
    .local v1, "appInfoDb":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->getResolveInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 147
    .local v5, "info":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_d

    .line 148
    const-string v9, "AcmsAppsManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "queryApps : reading package : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->fillFromXml(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 172
    :goto_3
    if-eqz v5, :cond_c

    .line 174
    const-string v9, "AcmsAppsManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Adding app "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to category VNC"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v9, "VNC"

    invoke-virtual {v0, v9, v13, v13}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v9, "AcmsAppsManager"

    const-string v11, "Fill icon info to VNC app"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->fillIconInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 179
    :cond_c
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v9, "AcmsAppsManager"

    const-string v11, "Insert AppData values"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->insertIntoAppData(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    .line 182
    const-string v9, "AcmsAppsManager"

    const-string v11, "queryApps : Insert Entity data values"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->insertIntoEntityData(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)V

    goto/16 :goto_1

    .line 152
    :catch_0
    move-exception v4

    .line 153
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v9, "AcmsAppsManager"

    const-string v11, "AcmsAppsManager.queryApps AppInfoXml corrupted"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 155
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v4

    .line 157
    .local v4, "e":Ljava/io/IOException;
    const-string v9, "AcmsAppsManager"

    .line 158
    const-string v11, "AcmsAppsManager.queryApps IOException while parsing appInfo xml "

    .line 156
    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 160
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 162
    .local v4, "e":Ljava/lang/NumberFormatException;
    const-string v9, "AcmsAppsManager"

    .line 163
    const-string v11, "AcmsAppsManager.queryApps NumberFormatException while reading content category"

    .line 161
    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 168
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :cond_d
    const-string v9, "AcmsAppsManager"

    const-string v11, "Appinfo not available from app certificate. Hence filling default values"

    invoke-static {v9, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0, v0, v5}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->fillDefaultAppInfo(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;Landroid/content/pm/ResolveInfo;)V

    goto :goto_3

    .line 193
    .end local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    .end local v1    # "appInfoDb":Ljava/lang/String;
    .end local v2    # "certState":I
    .end local v5    # "info":Landroid/content/pm/ResolveInfo;
    .end local v7    # "pkgName":Ljava/lang/String;
    .end local v8    # "type":I
    :cond_e
    const-string v9, "AcmsAppsManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "AcmsAppsManager.getApps returning"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " apps"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    goto/16 :goto_0
.end method

.method public registerCallback(Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;)V
    .locals 2
    .param p1, "cb"    # Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    .prologue
    .line 700
    const-string v0, "AcmsAppsManager"

    const-string v1, "Register AppListChangeCallBack enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAppListChangeCallBack:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager$AppListChangeCallBack;

    .line 702
    const-string v0, "AcmsAppsManager"

    const-string v1, "Register AppListChangeCallBack exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    return-void
.end method

.method public removeUnsupportedApp(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 691
    const-string v0, "AcmsAppsManager"

    const-string v1, "Enter removeUnsupportedVncApp"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mAcmsApps:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 694
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->delete(Ljava/lang/String;)Z

    .line 695
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->mUnsupportedAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 697
    :cond_0
    return-void
.end method
