.class Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;
.super Ljava/lang/Thread;
.source "AcmsHttpCertificateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppCertHttpThread"
.end annotation


# instance fields
.field private mAppEntry:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

.field private mCallback:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;-><init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 104
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->mAppEntry:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "id":Ljava/lang/String;
    const-string v3, "AcmsHttpCertificateHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "HttpThread.run() Enter appID= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->this$0:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->mAppEntry:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->mCallback:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;

    # invokes: Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->httpgetAppCert(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V
    invoke-static {v3, v4, v5}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;->access$0(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 115
    :goto_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    .line 116
    .local v0, "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->decrementSvcCounter()V

    .line 118
    :cond_0
    const-string v3, "AcmsHttpCertificateHandler"

    const-string v4, "Decremented Counter Value : AppCertHttpThread.run"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v3, "AcmsHttpCertificateHandler"

    const-string v4, "HttpThread.run() Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void

    .line 108
    .end local v0    # "acmsServiceMonitor":Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 110
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v1

    .line 111
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public start(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;)V
    .locals 3
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;

    .prologue
    .line 92
    const-string v0, "AcmsHttpCertificateHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HttpThread.start() appID= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->mAppEntry:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 94
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertHttpThread;->mCallback:Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;

    .line 97
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->getAcmsSvcMonitor()Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;->incrementSvcCounter()V

    .line 98
    const-string v0, "AcmsHttpCertificateHandler"

    const-string v1, "Incremented Counter Value : AppCertHttpThread.start"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0}, Ljava/lang/Thread;->start()V

    .line 100
    return-void
.end method
