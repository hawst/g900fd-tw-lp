.class public Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;
.super Ljava/lang/Object;
.source "KeyStoreHelper.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AcmsKeyStoreHelper"

.field private static sIsCreateKeyStoreHelper:Z

.field private static sKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;


# instance fields
.field private isKeyStoreLoaded:Z

.field private final mContext:Landroid/content/Context;

.field private final mDirKeyStore:Ljava/lang/String;

.field private final mFilenameKeyStore:Ljava/lang/String;

.field private mKeyStore:Ljava/security/KeyStore;

.field private mPrivateKey:Ljava/security/PrivateKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sIsCreateKeyStoreHelper:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mContext:Landroid/content/Context;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreLoaded:Z

    .line 57
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mDirKeyStore:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mDirKeyStore:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Acms_Keystore"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mFilenameKeyStore:Ljava/lang/String;

    .line 61
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sIsCreateKeyStoreHelper:Z

    .line 63
    return-void
.end method

.method private createAndLoadKeyStoreFile()Z
    .locals 9

    .prologue
    .line 191
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " createAndLoadKeyStoreFile Enter"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v6, :cond_0

    .line 193
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " mKeyStore is null"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const/4 v5, 0x0

    .line 234
    :goto_0
    return v5

    .line 196
    :cond_0
    const/4 v5, 0x1

    .line 197
    .local v5, "returnVal":Z
    new-instance v0, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mDirKeyStore:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    .local v0, "certDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 200
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, "Directory was not present. hence created"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :goto_1
    new-instance v2, Ljava/io/File;

    const-string v6, "Acms_Keystore"

    invoke-direct {v2, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 206
    .local v2, "f":Ljava/io/File;
    const/4 v3, 0x0

    .line 208
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    const/4 v7, 0x0

    const-string v8, ""

    invoke-virtual {v8}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 209
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    const-string v7, "8Q!@hs1#{1:2c$h"

    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 224
    if-eqz v4, :cond_4

    .line 226
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9

    move-object v3, v4

    .line 233
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_2
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " createAndLoadKeyStoreFile Exit"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, "Directory already present"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 211
    .restart local v2    # "f":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Ljava/security/KeyStoreException;
    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213
    const/4 v5, 0x0

    .line 224
    if-eqz v3, :cond_1

    .line 226
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 227
    :catch_1
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    goto :goto_2

    .line 214
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 215
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :goto_4
    :try_start_5
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 216
    const/4 v5, 0x0

    .line 224
    if-eqz v3, :cond_1

    .line 226
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 227
    :catch_3
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    goto :goto_2

    .line 217
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 218
    .local v1, "e":Ljava/security/cert/CertificateException;
    :goto_5
    :try_start_7
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 219
    const/4 v5, 0x0

    .line 224
    if-eqz v3, :cond_1

    .line 226
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_2

    .line 227
    :catch_5
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    goto :goto_2

    .line 220
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 221
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 222
    const/4 v5, 0x0

    .line 224
    if-eqz v3, :cond_1

    .line 226
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_2

    .line 227
    :catch_7
    move-exception v1

    .line 228
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    goto :goto_2

    .line 223
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 224
    :goto_7
    if-eqz v3, :cond_3

    .line 226
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 232
    :cond_3
    :goto_8
    throw v6

    .line 227
    :catch_8
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    goto :goto_8

    .line 227
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_9
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 229
    const/4 v5, 0x0

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 223
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_7

    .line 220
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v1

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 217
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v1

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 214
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v1

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 211
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v1

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :cond_4
    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method private createKeyStoreInstance()Z
    .locals 2

    .prologue
    .line 119
    :try_start_0
    const-string v1, "PKCS12"

    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V

    .line 122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    .line 123
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createPrivateKey()Z
    .locals 5

    .prologue
    .line 238
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mPrivateKey:Ljava/security/PrivateKey;

    if-nez v4, :cond_0

    .line 242
    :try_start_0
    const-string v4, "RSA"

    invoke-static {v4}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v2

    .line 243
    .local v2, "keyPairGen":Ljava/security/KeyPairGenerator;
    invoke-virtual {v2}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    .line 244
    .local v1, "keyPair":Ljava/security/KeyPair;
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v3

    .line 245
    .local v3, "privKey":Ljava/security/PrivateKey;
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mPrivateKey:Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v1    # "keyPair":Ljava/security/KeyPair;
    .end local v2    # "keyPairGen":Ljava/security/KeyPairGenerator;
    .end local v3    # "privKey":Ljava/security/PrivateKey;
    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 248
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getKeyStoreHelper(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    sget-boolean v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sIsCreateKeyStoreHelper:Z

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    .line 67
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->createPrivateKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const-string v0, "AcmsKeyStoreHelper"

    const-string v1, "Error in Private key creation"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    return-object v0
.end method

.method private getPrivateKey()Ljava/security/PrivateKey;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mPrivateKey:Ljava/security/PrivateKey;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->createPrivateKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mPrivateKey:Ljava/security/PrivateKey;

    goto :goto_0
.end method

.method private isKeyStoreFilePresent()Z
    .locals 2

    .prologue
    .line 129
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mFilenameKeyStore:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    const/4 v1, 0x1

    .line 133
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isKeyStoreInstancePresent()Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    .line 111
    :cond_0
    const-string v0, "AcmsKeyStoreHelper"

    .line 112
    const-string v1, "keyStore instance already created. No need to create again"

    .line 111
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private printKeyStoreAliases()V
    .locals 4

    .prologue
    .line 262
    const-string v2, "AcmsKeyStoreHelper"

    const-string v3, "Aliases in keystore are:"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v2}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    .line 267
    .local v1, "str":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    .end local v1    # "str":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_1
    return-void

    .line 268
    .restart local v1    # "str":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_0
    const-string v3, "AcmsKeyStoreHelper"

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 271
    .end local v1    # "str":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_1
.end method

.method private storeToKeyStore()V
    .locals 5

    .prologue
    .line 79
    const/4 v1, 0x0

    .line 81
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v3, :cond_0

    .line 82
    const-string v3, "PKCS12"

    invoke-static {v3}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    .line 84
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mFilenameKeyStore:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    const-string v4, "8Q!@hs1#{1:2c$h"

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 97
    if-eqz v2, :cond_3

    .line 99
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9

    move-object v1, v2

    .line 105
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/security/KeyStoreException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    if-eqz v1, :cond_1

    .line 99
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 91
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 97
    if-eqz v1, :cond_1

    .line 99
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 100
    :catch_3
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 93
    .local v0, "e":Ljava/security/cert/CertificateException;
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 97
    if-eqz v1, :cond_1

    .line 99
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_0

    .line 100
    :catch_5
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 94
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 95
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_9
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 97
    if-eqz v1, :cond_1

    .line 99
    :try_start_a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_0

    .line 100
    :catch_7
    move-exception v0

    .line 101
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 97
    :goto_5
    if-eqz v1, :cond_2

    .line 99
    :try_start_b
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 104
    :cond_2
    :goto_6
    throw v3

    .line 100
    :catch_8
    move-exception v0

    .line 101
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 100
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_9
    move-exception v0

    .line 101
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 96
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .line 94
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v0

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 92
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v0

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 90
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v0

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 88
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v0

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 402
    const-string v0, "AcmsKeyStoreHelper"

    const-string v1, "KeyStoreHelper: inside cleanup"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->sKeyStoreHelper:Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;

    .line 404
    return-void
.end method

.method public deleteCertChain(Ljava/lang/String;)Z
    .locals 5
    .param p1, "Id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 323
    const-string v2, "AcmsKeyStoreHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " deleteCertChain Entry for Id:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    if-nez p1, :cond_1

    .line 325
    const-string v2, "AcmsKeyStoreHelper"

    const-string v3, " deleteCertChain Id is NULL"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_0
    :goto_0
    return v1

    .line 328
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreLoaded:Z

    if-nez v2, :cond_2

    .line 329
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreFilePresent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 330
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getKeyStoreForApplication()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v2, :cond_4

    .line 340
    const-string v2, "AcmsKeyStoreHelper"

    const-string v3, " mKeystore is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v0}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v0    # "e":Ljava/security/KeyStoreException;
    :cond_3
    const-string v2, "AcmsKeyStoreHelper"

    const-string v3, " No entries in keyStore"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v2, p1}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 344
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->storeToKeyStore()V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0

    .line 349
    const-string v1, "AcmsKeyStoreHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " deleteCertChain Exit for Id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCertChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 9
    .param p1, "Id"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 354
    const-string v5, "AcmsKeyStoreHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, " getCertChain Entry for Id:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    if-nez p1, :cond_0

    .line 356
    const-string v5, "AcmsKeyStoreHelper"

    const-string v7, " getCertChain Id is NULL"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 398
    :goto_0
    return-object v0

    .line 359
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreLoaded:Z

    if-nez v5, :cond_2

    .line 360
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreFilePresent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 361
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getKeyStoreForApplication()Z

    move-result v5

    if-nez v5, :cond_2

    move-object v0, v6

    .line 362
    goto :goto_0

    :cond_1
    move-object v0, v6

    .line 365
    goto :goto_0

    .line 368
    :cond_2
    const/4 v0, 0x0

    .line 369
    .local v0, "certChain":[Ljava/security/cert/X509Certificate;
    const/4 v1, 0x0

    .line 372
    .local v1, "certs":[Ljava/security/cert/Certificate;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v5, :cond_3

    .line 373
    const-string v5, "AcmsKeyStoreHelper"

    const-string v7, " mKeystore is null"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 374
    goto :goto_0

    .line 376
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v5, p1}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v1

    .line 378
    if-nez v1, :cond_4

    .line 379
    const-string v5, "AcmsKeyStoreHelper"

    const-string v7, "Given appId not present in Keystore"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v5, "AcmsKeyStoreHelper"

    const-string v7, "Application can crash anytime !!!!!!!!"

    invoke-static {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 381
    goto :goto_0

    .line 383
    :cond_4
    array-length v5, v1

    new-array v0, v5, [Ljava/security/cert/X509Certificate;

    .line 388
    const/4 v3, 0x0

    .local v3, "i":I
    array-length v5, v1
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v4, v5, -0x1

    .local v4, "j":I
    :goto_1
    if-gez v4, :cond_5

    .line 396
    const-string v5, "AcmsKeyStoreHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " getCertChain Exit for Id:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_5
    :try_start_1
    aget-object v5, v1, v3

    check-cast v5, Ljava/security/cert/X509Certificate;

    aput-object v5, v0, v4
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0

    .line 390
    add-int/lit8 v3, v3, 0x1

    .line 388
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 392
    .end local v3    # "i":I
    .end local v4    # "j":I
    :catch_0
    move-exception v2

    .line 393
    .local v2, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v2}, Ljava/security/KeyStoreException;->printStackTrace()V

    move-object v0, v6

    .line 394
    goto :goto_0
.end method

.method public getKeyStore()Ljava/security/KeyStore;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    return-object v0
.end method

.method public getKeyStoreForApplication()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 138
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " getKeyStoreForApplication Enter"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v4, 0x1

    .line 142
    .local v4, "returnVal":Z
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreInstancePresent()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->createKeyStoreInstance()Z

    move-result v6

    if-nez v6, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v5

    .line 145
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreFilePresent()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 146
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, "Key Store exist"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v2, 0x0

    .line 148
    .local v2, "fis":Ljava/io/FileInputStream;
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mFilenameKeyStore:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v1, "f":Ljava/io/File;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v6, :cond_2

    .line 151
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " mKeystore is null"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    if-eqz v2, :cond_0

    .line 171
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 154
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_2
    const-string v5, "AcmsKeyStoreHelper"

    const-string v6, "Hence loading the keystore file"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_3
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    const-string v6, "8Q!@hs1#{1:2c$h"

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 169
    if-eqz v3, :cond_3

    .line 171
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8

    .line 183
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    :cond_3
    :goto_1
    if-eqz v4, :cond_4

    .line 184
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreLoaded:Z

    .line 186
    :cond_4
    const-string v5, "AcmsKeyStoreHelper"

    const-string v6, " getKeyStoreForApplication Exit"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v4

    .line 187
    goto :goto_0

    .line 159
    .restart local v1    # "f":Ljava/io/File;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 160
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 161
    const/4 v4, 0x0

    .line 169
    if-eqz v2, :cond_3

    .line 171
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 172
    :catch_2
    move-exception v0

    .line 173
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 162
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 163
    .local v0, "e":Ljava/security/cert/CertificateException;
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 164
    const/4 v4, 0x0

    .line 169
    if-eqz v2, :cond_3

    .line 171
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    .line 172
    :catch_4
    move-exception v0

    .line 173
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 165
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 166
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_9
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 167
    const/4 v4, 0x0

    .line 169
    if-eqz v2, :cond_3

    .line 171
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_1

    .line 172
    :catch_6
    move-exception v0

    .line 173
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 168
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 169
    :goto_5
    if-eqz v2, :cond_5

    .line 171
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 176
    :cond_5
    :goto_6
    throw v5

    .line 172
    :catch_7
    move-exception v0

    .line 173
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 172
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v0

    .line 173
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 178
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "f":Ljava/io/File;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    :cond_6
    const-string v6, "AcmsKeyStoreHelper"

    const-string v7, " KeyStore File do not exist. Hence Creating"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->createAndLoadKeyStoreFile()Z

    move-result v6

    if-nez v6, :cond_3

    goto/16 :goto_0

    .line 168
    .restart local v1    # "f":Ljava/io/File;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 165
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_9
    move-exception v0

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 162
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v0

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 159
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_b
    move-exception v0

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public storeCertChain(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z
    .locals 8
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "certChain"    # [Ljava/security/cert/X509Certificate;

    .prologue
    const/4 v4, 0x0

    .line 278
    const-string v5, "AcmsKeyStoreHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " storeCertChain Entry for Id:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-boolean v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->isKeyStoreLoaded:Z

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getKeyStoreForApplication()Z

    move-result v5

    if-nez v5, :cond_0

    .line 319
    :goto_0
    return v4

    .line 283
    :cond_0
    if-nez p1, :cond_1

    .line 284
    const-string v5, "AcmsKeyStoreHelper"

    const-string v6, "appId is NULL"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v5

    if-nez v5, :cond_2

    .line 287
    const-string v5, "AcmsKeyStoreHelper"

    const-string v6, "Private key is NULL"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 294
    :cond_2
    array-length v5, p2

    new-array v0, v5, [Ljava/security/cert/X509Certificate;

    .line 295
    .local v0, "certs":[Ljava/security/cert/Certificate;
    array-length v5, p2

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-gez v2, :cond_3

    .line 305
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    if-nez v5, :cond_4

    .line 306
    const-string v5, "AcmsKeyStoreHelper"

    const-string v6, " mKeystore is null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 313
    :catch_0
    move-exception v1

    .line 314
    .local v1, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_0

    .line 296
    .end local v1    # "e":Ljava/security/KeyStoreException;
    :cond_3
    aget-object v5, p2, v2

    aput-object v5, v0, v3

    .line 297
    add-int/lit8 v3, v3, 0x1

    .line 295
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 309
    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->mKeyStore:Ljava/security/KeyStore;

    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v6

    .line 310
    const-string v7, "8Q!@hs1#{1:2c$h"

    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 309
    invoke-virtual {v5, p1, v6, v7, v0}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 311
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/KeyStoreHelper;->storeToKeyStore()V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0

    .line 318
    const-string v4, "AcmsKeyStoreHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " storeCertChain Exit for Id:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v4, 0x1

    goto :goto_0
.end method
