.class public Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;
.super Ljava/lang/Object;
.source "BtAppholder.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HfpListener"
.end annotation


# instance fields
.field private mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

.field final synthetic this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)V
    .locals 0
    .param p2, "hfp"    # Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .prologue
    .line 2546
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 2545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2547
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 2548
    return-void
.end method


# virtual methods
.method public deinit()V
    .locals 1

    .prologue
    .line 2574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    .line 2575
    return-void
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 2553
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " HeadsetProfileManager: HfpListener : onServiceConnected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2554
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2556
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    invoke-static {v0, p2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothHeadset;)V

    .line 2557
    const-string v0, "TMSBtAppholder"

    const-string v1, " HeadsetProfileManager: HfpListener :Service Connected"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2559
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v0, :cond_1

    .line 2560
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->onStateChnaged()V

    .line 2562
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "profile"    # I

    .prologue
    .line 2567
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    if-eqz v0, :cond_0

    .line 2568
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->mHeadsetProfileManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->onStateChnaged()V

    .line 2570
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->this$1:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothHeadset;)V

    .line 2571
    return-void
.end method
