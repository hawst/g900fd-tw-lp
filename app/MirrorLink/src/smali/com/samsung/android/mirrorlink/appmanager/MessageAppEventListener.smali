.class public Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;
.super Landroid/os/Handler;
.source "MessageAppEventListener.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;


# static fields
.field public static final MSG_EVENT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field private mCntxt:Landroid/content/Context;

.field private mIsMonitoring:Z

.field private mMessagingAppActions:Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;

.field private mMsgFilter:Landroid/content/IntentFilter;

.field private mRelatedAppId:I

.field private mRelatedAppName:Ljava/lang/String;

.field private mSmsEventListener:Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;

.field private mSupportedClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "cntxt"    # Landroid/content/Context;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appId"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppName:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppId:I

    .line 38
    const-string v0, "TMSAppMngr"

    const-string v1, "MessageAppEventListener.MessageAppEventListener enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mCntxt:Landroid/content/Context;

    .line 40
    iput p3, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppId:I

    .line 41
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppName:Ljava/lang/String;

    .line 42
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mMsgFilter:Landroid/content/IntentFilter;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mMsgFilter:Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;-><init>(Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSmsEventListener:Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;

    .line 46
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mMessagingAppActions:Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;

    .line 48
    return-void
.end method


# virtual methods
.method public getRelatedAppId()I
    .locals 3

    .prologue
    .line 132
    const-string v0, "TMSAppMngr"

    .line 133
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MessageAppEventListener.getRelatedAppId enter mRelatedAppId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 134
    iget v2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 133
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppId:I

    return v0
.end method

.method public getRelatedAppName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    const-string v0, "TMSAppMngr"

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MessageAppEventListener.getRelatedAppName enter mRelatedAppName = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 127
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedClients()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    const-string v0, "TMSAppMngr"

    .line 105
    const-string v1, "MessageAppEventListener.getSupportedClients.setClient enter"

    .line 104
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 140
    const-string v0, "TMSAppMngr"

    const-string v1, "MessageAppEventListener.handleMessage enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget v0, p1, Landroid/os/Message;->what:I

    .line 161
    return-void
.end method

.method public invokeNoti(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;)Z
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;

    .prologue
    .line 115
    const-string v1, "TMSAppMngr"

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MessageAppEventListener.invokeNoti enter action.mActionName= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 117
    iget-object v3, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 116
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 115
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    .line 120
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mMessagingAppActions:Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;

    iget-object v2, p1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotiAction;->mActionName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/MessagingAppActions;->doAction(Ljava/lang/String;)Z

    move-result v0

    .line 121
    return v0
.end method

.method public motinor(Z)Z
    .locals 3
    .param p1, "startMonitoring"    # Z

    .prologue
    .line 51
    const-string v0, "TMSAppMngr"

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MessageAppEventListener.motinor enter startMonitoring =  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 53
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mIsMonitoring = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mCntxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSmsEventListener:Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mMsgFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    .line 61
    :cond_0
    :goto_0
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MessageAppEventListener.motinor exit mIsMonitoring = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 62
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    return v0

    .line 57
    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mCntxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSmsEventListener:Lcom/samsung/android/mirrorlink/appmanager/SmsEventListener;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mIsMonitoring:Z

    goto :goto_0
.end method

.method public registerNotiHandler(Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;)V
    .locals 2
    .param p1, "notificationListener"    # Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;

    .prologue
    .line 110
    const-string v0, "TMSAppMngr"

    const-string v1, "MessageAppEventListener.registerNotiHandler enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public setClient(ILjava/util/ArrayList;)Z
    .locals 4
    .param p1, "clientID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 76
    .local p2, "apps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "TMSAppMngr"

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MessageAppEventListener.setClient.setClient enter clientID=  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 77
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    .line 85
    :goto_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mRelatedAppId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 86
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_0
    const/4 v0, 0x1

    .line 98
    :cond_1
    :goto_1
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MessageAppEventListener.setClient.setClient exit ret=  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 99
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return v0

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 91
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 92
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 94
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/MessageAppEventListener;->mSupportedClients:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 95
    const/4 v0, 0x0

    goto :goto_1
.end method
