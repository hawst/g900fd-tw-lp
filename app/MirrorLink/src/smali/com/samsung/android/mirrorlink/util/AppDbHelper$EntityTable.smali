.class public final Lcom/samsung/android/mirrorlink/util/AppDbHelper$EntityTable;
.super Ljava/lang/Object;
.source "AppDbHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/util/AppDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EntityTable"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final COL_NAME:Ljava/lang/String; = "e_name"

.field public static final COL_NON_RESTRICTED:Ljava/lang/String; = "non_restricted"

.field public static final COL_PKG_NAME:Ljava/lang/String; = "pkg_name"

.field public static final COL_RESTRICTED:Ljava/lang/String; = "restricted"

.field public static final COL_SERVICELIST:Ljava/lang/String; = "serviceList"

.field public static final COL_TARGETLIST:Ljava/lang/String; = "targetList"

.field public static final TABLE_NAME:Ljava/lang/String; = "EntityData"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 138
    const-string v2, "pkg_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 139
    const-string v2, "e_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 140
    const-string v2, "targetList"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 141
    const-string v2, "restricted"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 142
    const-string v2, "non_restricted"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 143
    const-string v2, "serviceList"

    aput-object v2, v0, v1

    .line 137
    sput-object v0, Lcom/samsung/android/mirrorlink/util/AppDbHelper$EntityTable;->ALL_COLUMNS:[Ljava/lang/String;

    .line 145
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
