.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;
.super Landroid/os/Handler;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;->this$2:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    .line 1466
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1468
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1475
    const-string v0, "TMSEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SSDPAliveReceiver.run() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    :cond_0
    :goto_0
    return-void

    .line 1470
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;->this$2:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->access$0(Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1471
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread$1;->this$2:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;->access$0(Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->forceSendAnnounce()V

    goto :goto_0

    .line 1468
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
