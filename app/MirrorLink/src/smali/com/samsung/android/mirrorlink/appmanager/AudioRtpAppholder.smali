.class public Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;
.super Ljava/lang/Object;
.source "AudioRtpAppholder.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;
.implements Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;


# static fields
.field private static final ACTION_TYPE_START:I = 0x1

.field private static final ACTION_TYPE_STOP:I = 0x2

.field private static final AUDIO_TMS_ACTION:Ljava/lang/String; = "com.sec.tms.audio.server"

.field private static final AUDIO_TMS_APP_RTP_CLIENT:Ljava/lang/String; = "client"

.field private static final AUDIO_TMS_APP_RTP_SERVER:Ljava/lang/String; = "server"

.field private static final AUDIO_TMS_EXTRA_APP:Ljava/lang/String; = "app"

.field private static final AUDIO_TMS_EXTRA_STATE:Ljava/lang/String; = "state"

.field private static final EXTRA_ACTION_TYPE:Ljava/lang/String; = "com.samsung.sec.android.media.EXTRA_ACTION_TYPE"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSAudioRtpAppholder"

.field private static final RTP_REQUEST_ACTION:Ljava/lang/String; = "com.sec.musicplayer.rtpaction"

.field private static final TM_APP_AUDIOTYPE_APPLICATION:Ljava/lang/String; = "application"

.field private static final TM_APP_AUDIOTYPE_PHONE:Ljava/lang/String; = "phone"

.field private static final TM_APP_RTP_CLIENT_CATEGORY:I = -0xffffffe

.field private static final TM_APP_RTP_CLIENT_CONTENT_CATEGORY_BOTH:I = 0x11

.field private static final TM_APP_RTP_CLIENT_CONTENT_CATEGORY_PHONE:I = 0x1

.field private static final TM_APP_RTP_CLIENT_CONTENT_CATEGORY_VOICE:I = 0x10

.field private static final TM_APP_RTP_SERVER_CATEGORY:I = -0xfffffff

.field private static final TM_APP_RTP_SERVER_CONTENT_CATEGORY:I = 0x2

.field private static final TM_AUDIO_RTP_HOLDERNAME:Ljava/lang/String; = "AudioRtpAppholder"

.field private static final TM_DIRECTION_IN:Ljava/lang/String; = "in"

.field private static final TM_DIRECTION_OUT:Ljava/lang/String; = "out"

.field private static final TM_FORMAT_98:Ljava/lang/String; = "98"

.field private static final TM_FORMAT_99:Ljava/lang/String; = "99"

.field private static final TM_PROTOCOL_ID:Ljava/lang/String; = "RTP"

.field private static final TM_RESOURCE_STATUS_FREE:Ljava/lang/String; = "free"

.field private static final TM_RTP_IPL:I = 0x12c0

.field private static final TM_RTP_MPL:I = 0x2580

.field private static final TM_TRUST_LEVEL:I = 0x80


# instance fields
.field private cntxt:Landroid/content/Context;

.field private mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

.field private mAppInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

.field private mAppIsRunningRTPClient:Z

.field private mAppIsRunningRTPServer:Z

.field private mAppNameRTPClient_98:Ljava/lang/String;

.field private mAppNameRTPClient_99:Ljava/lang/String;

.field private mAppNameRTPClient_99_2:Ljava/lang/String;

.field private mAppNameRTPClient_99_3:Ljava/lang/String;

.field private mAppNameRTPServer_98:Ljava/lang/String;

.field private mAppNameRTPServer_99:Ljava/lang/String;

.field private mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

.field private mAudioIpl:I

.field private mAudioMpl:I

.field public mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

.field private mDescriptionRTPClient:Ljava/lang/String;

.field private mDescriptionRTPServer:Ljava/lang/String;

.field private mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    const/16 v0, 0x12c0

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioIpl:I

    .line 157
    const/16 v0, 0x2580

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioMpl:I

    .line 175
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "Enter : AudioRtpAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 180
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "ERROR !!! No Context"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppDbInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    .line 184
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    .line 185
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->readAppNames()V

    .line 187
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "Exit : AudioRtpAppholder"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private informCommonApi(ZIZ)V
    .locals 5
    .param p1, "isServer"    # Z
    .param p2, "payload"    # I
    .param p3, "conn"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 227
    const-string v0, "TMSAudioRtpAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "informCommonApi Enter isServer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " payload "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conn "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    if-eqz p3, :cond_2

    .line 230
    if-eqz p1, :cond_1

    .line 231
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, v4, p2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpServerInfo(ZI)V

    .line 243
    :cond_0
    :goto_0
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "informCommonApi Exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, v4, p2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpClientInfo(ZI)V

    goto :goto_0

    .line 236
    :cond_2
    if-eqz p1, :cond_3

    .line 237
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpServerInfo(Z)V

    goto :goto_0

    .line 239
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpClientInfo(Z)V

    goto :goto_0
.end method

.method private launchAppRTPClient(Ljava/lang/String;)V
    .locals 11
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 388
    iget-boolean v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    if-eqz v7, :cond_0

    .line 389
    const-string v7, "TMSAudioRtpAppholder"

    const-string v8, "launchAppRTPClient (mAppIsRunningRTPClient == true)"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :goto_0
    return-void

    .line 393
    :cond_0
    iput-boolean v10, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    .line 395
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "AppName "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const/16 v0, 0x63

    .line 402
    .local v0, "iPayload":I
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 403
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v7, "rtp_client_payload_key"

    const-string v8, "*"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 404
    .local v2, "payload":Ljava/lang/String;
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "payload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v7, "*"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 406
    const-string v7, "99"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 407
    const/16 v0, 0x63

    .line 422
    :goto_1
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    new-instance v1, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 424
    .local v1, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v7, "RtpClientPayLoad"

    invoke-virtual {v1, v7, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 428
    const-string v7, "rtp_remote_ip_key"

    const-string v8, "127.0.0.1"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 429
    .local v3, "remoteIp":Ljava/lang/String;
    const-string v7, "rtp_remote_port_key"

    const-string v8, "9000"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 430
    .local v4, "remotePort":Ljava/lang/String;
    const-string v7, "rtp_client_self_port_key"

    const-string v8, "10600"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 432
    .local v5, "selfPort":Ljava/lang/String;
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remoteIp "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remotePort "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "selfPort "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v7, "RtpSelfPort"

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 437
    const-string v7, "RtpRemoteIpAddr"

    invoke-virtual {v1, v7, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v7, "RtpRemotePort"

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 442
    const-string v7, "RtpMpl"

    iget v8, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioMpl:I

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 443
    const-string v7, "RtpIpl"

    iget v8, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioIpl:I

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 445
    const/4 v7, 0x0

    invoke-direct {p0, v7, v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->informCommonApi(ZIZ)V

    .line 447
    const-string v7, "client"

    invoke-direct {p0, v7, v10}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setAudioFlingerDevice(Ljava/lang/String;Z)V

    .line 449
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->rtpClientStart(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 408
    .end local v1    # "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    .end local v3    # "remoteIp":Ljava/lang/String;
    .end local v4    # "remotePort":Ljava/lang/String;
    .end local v5    # "selfPort":Ljava/lang/String;
    :cond_1
    const-string v7, "z9"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 409
    const/16 v0, 0x63

    .line 410
    goto/16 :goto_1

    :cond_2
    const-string v7, "k9"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 411
    const/16 v0, 0x63

    .line 412
    goto/16 :goto_1

    :cond_3
    const-string v7, "98"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 413
    const/16 v0, 0x62

    .line 414
    goto/16 :goto_1

    .line 415
    :cond_4
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 418
    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 419
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private launchAppRTPServer(Ljava/lang/String;)V
    .locals 11
    .param p1, "AppName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 313
    iput-boolean v10, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPServer:Z

    .line 319
    const/16 v0, 0x63

    .line 320
    .local v0, "iPayload":I
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 321
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v7, "rtp_server_payload_key"

    const-string v8, "*"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 322
    .local v2, "payload":Ljava/lang/String;
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "payload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v7, "*"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 324
    const-string v7, "99"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 325
    const/16 v0, 0x63

    .line 336
    :goto_0
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    new-instance v1, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 338
    .local v1, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v7, "RtpServerPayLoad"

    invoke-virtual {v1, v7, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 342
    const-string v7, "rtp_remote_ip_key"

    const-string v8, "127.0.0.1"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 343
    .local v3, "remoteIp":Ljava/lang/String;
    const-string v7, "rtp_remote_port_key"

    const-string v8, "9000"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 344
    .local v4, "remotePort":Ljava/lang/String;
    const-string v7, "rtp_server_self_port_key"

    const-string v8, "10500"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 346
    .local v5, "selfPort":Ljava/lang/String;
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remoteIp "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remotePort "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "selfPort "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v7, "RtpSelfPort"

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 351
    const-string v7, "RtpRemoteIpAddr"

    invoke-virtual {v1, v7, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v7, "RtpRemotePort"

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 356
    const-string v7, "RtpMpl"

    iget v8, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioMpl:I

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 357
    const-string v7, "RtpIpl"

    iget v8, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioIpl:I

    invoke-virtual {v1, v7, v8}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 360
    const-string v7, "server"

    invoke-direct {p0, v7, v10}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setAudioFlingerDevice(Ljava/lang/String;Z)V

    .line 362
    invoke-direct {p0, v10, v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->informCommonApi(ZIZ)V

    .line 367
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->rtpServerStart(Ljava/lang/String;)I

    .line 368
    return-void

    .line 326
    .end local v1    # "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    .end local v3    # "remoteIp":Ljava/lang/String;
    .end local v4    # "remotePort":Ljava/lang/String;
    .end local v5    # "selfPort":Ljava/lang/String;
    :cond_0
    const-string v7, "98"

    invoke-virtual {p1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 327
    const/16 v0, 0x62

    .line 328
    goto/16 :goto_0

    .line 329
    :cond_1
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 332
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 333
    const-string v7, "TMSAudioRtpAppholder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "iPayload "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private readAppNames()V
    .locals 1

    .prologue
    .line 209
    const-string v0, "RTP Server 99"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_99:Ljava/lang/String;

    .line 210
    const-string v0, "RTP Server 98"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_98:Ljava/lang/String;

    .line 212
    const-string v0, "RTP Client 98"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_98:Ljava/lang/String;

    .line 213
    const-string v0, "RTP Client 99"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99:Ljava/lang/String;

    .line 214
    const-string v0, "RTP Client k9"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_2:Ljava/lang/String;

    .line 215
    const-string v0, "RTP Client z9"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_3:Ljava/lang/String;

    .line 218
    const-string v0, "RTP Audio Client"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPClient:Ljava/lang/String;

    .line 219
    const-string v0, "RTP Audio Server"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPServer:Ljava/lang/String;

    .line 220
    return-void
.end method

.method private setAudioFlingerDevice(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "app"    # Ljava/lang/String;
    .param p2, "set"    # Z

    .prologue
    .line 926
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.tms.audio.server"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 927
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 928
    const-string v1, "app"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 930
    if-eqz p2, :cond_0

    .line 931
    const-string v1, "state"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 935
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 936
    return-void

    .line 933
    :cond_0
    const-string v1, "state"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private setConfig()V
    .locals 14

    .prologue
    .line 255
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-static {v11}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 257
    .local v10, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v11, "rtp_remote_ip_key"

    const-string v12, "127.0.0.1"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 258
    .local v7, "remoteIp":Ljava/lang/String;
    const-string v11, "rtp_remote_port_key"

    const-string v12, "9000"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 259
    .local v8, "remotePort":Ljava/lang/String;
    const-string v11, "rtp_server_self_port_key"

    const-string v12, "10500"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 260
    .local v9, "serverSelfPort":Ljava/lang/String;
    const-string v11, "rtp_client_self_port_key"

    const-string v12, "10600"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "clientSelfPort":Ljava/lang/String;
    const-string v11, "rtp_server_payload_key"

    const-string v12, "*"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "payload":Ljava/lang/String;
    const-string v11, "*"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 266
    const-string v6, "99"

    .line 269
    :cond_0
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "remoteIp "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "remotePort "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "serverSelfPort "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "clientSelfPort "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "payload "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 277
    .local v3, "iRemoteport":I
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 278
    .local v4, "iServerSelfPort":I
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 279
    .local v1, "iClientSelfPort":I
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 281
    .local v2, "iPayload":I
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "iRemoteport "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "iServerSelfPort "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "iClientSelfPort "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v11, "TMSAudioRtpAppholder"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "iPayload "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    if-nez v7, :cond_1

    .line 287
    const-string v11, "TMSAudioRtpAppholder"

    const-string v12, "setConfig:Params Are NULL"

    invoke-static {v11, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :goto_0
    return-void

    .line 291
    :cond_1
    new-instance v5, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v5}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 294
    .local v5, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v11, "RtpServerSelfPort"

    invoke-virtual {v5, v11, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 295
    const-string v11, "RtpClientSelfPort"

    invoke-virtual {v5, v11, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 296
    const-string v11, "RtpRemoteIpAddr"

    invoke-virtual {v5, v11, v7}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v11, "RtpRemotePort"

    invoke-virtual {v5, v11, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 298
    const-string v11, "RtpPreferredPayLoad"

    invoke-virtual {v5, v11, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 299
    const-string v11, "RtpMpl"

    iget v12, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioMpl:I

    invoke-virtual {v5, v11, v12}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 300
    const-string v11, "RtpIpl"

    iget v12, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioIpl:I

    invoke-virtual {v5, v11, v12}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 302
    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setRtpParams(Ljava/lang/String;)I

    .line 304
    const/4 v5, 0x0

    .line 306
    goto :goto_0
.end method

.method private setMusicPlayerPath(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 944
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.musicplayer.rtpaction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 945
    .local v0, "j":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 946
    const-string v1, "com.samsung.sec.android.media.EXTRA_ACTION_TYPE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 950
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->cntxt:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 951
    return-void

    .line 948
    :cond_0
    const-string v1, "com.samsung.sec.android.media.EXTRA_ACTION_TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private stopAppRTPClient()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 460
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    if-nez v0, :cond_0

    .line 462
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "stopAppRTPClient (mAppIsRunningRTPClient == false)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :goto_0
    return-void

    .line 465
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    .line 466
    invoke-direct {p0, v1, v1, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->informCommonApi(ZIZ)V

    .line 467
    const-string v0, "client"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setAudioFlingerDevice(Ljava/lang/String;Z)V

    .line 469
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->rtpClientStop()I

    goto :goto_0
.end method

.method private stopAppRTPServer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 374
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPServer:Z

    if-nez v0, :cond_0

    .line 376
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "stopAppRTPServer (mAppIsRunningRTPServer == false)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 379
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPServer:Z

    .line 380
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->informCommonApi(ZIZ)V

    .line 381
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->rtpServerStop()I

    .line 383
    const-string v0, "server"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setAudioFlingerDevice(Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public deinit()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 518
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPServer:Z

    if-eqz v0, :cond_0

    .line 519
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->stopAppRTPServer()V

    .line 522
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    if-eqz v0, :cond_1

    .line 523
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->stopAppRTPClient()V

    .line 526
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPServer:Z

    .line 527
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppIsRunningRTPClient:Z

    .line 528
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 529
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 531
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 532
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 533
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 534
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 535
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 538
    return v2
.end method

.method public destroy()Z
    .locals 1

    .prologue
    .line 546
    const/4 v0, 0x0

    return v0
.end method

.method public getAppList()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 885
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.getAppList enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.getAppList exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAppStatus(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 860
    if-nez p1, :cond_0

    .line 861
    const-string v1, "TMSAudioRtpAppholder"

    const-string v2, "getAppStatus(packageName == null)"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    :goto_0
    return-object v0

    .line 871
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 872
    const-string v0, "TMSAudioRtpAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAppStatus : found package - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 875
    :cond_1
    const-string v1, "TMSAudioRtpAppholder"

    const-string v2, "getAppStatus : not found package. returning null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getNotRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getRunningApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getServerSupportedRtpPayloads()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1127
    .local v0, "payloads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v1, "98"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1128
    const-string v1, "99"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1130
    return-object v0
.end method

.method public informAppStatusChange(Ljava/lang/String;I)V
    .locals 2
    .param p1, "app"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 478
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    if-eqz v0, :cond_4

    .line 483
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_99:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 484
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 491
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_98:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 496
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_2

    .line 497
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 499
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 500
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 503
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_3:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 504
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_4

    .line 505
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    .line 510
    :cond_4
    return-void

    .line 487
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_98:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppStatus:I

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;->onAppStatusChange(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z
    .locals 11
    .param p1, "appmngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "cntxt"    # Landroid/content/Context;

    .prologue
    const/16 v5, 0x2580

    const/16 v4, 0x12c0

    const/4 v10, 0x1

    const/16 v9, 0x80

    const/4 v8, -0x1

    .line 575
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.init enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    if-nez v0, :cond_1

    .line 578
    :cond_0
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "mAppDbInterface is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const/4 v0, 0x0

    .line 841
    :goto_0
    return v0

    .line 581
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setConfig()V

    .line 583
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 584
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 586
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 587
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 588
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 589
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 591
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 592
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    .line 600
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_99:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 601
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_99:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 602
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPServer:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 604
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 605
    const-string v1, "RTP"

    .line 606
    const-string v2, "99"

    .line 607
    const-string v3, "out"

    .line 604
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 611
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "application"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 612
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xfffffff

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 613
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 614
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 615
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 616
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 617
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/4 v1, 0x2

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 618
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 619
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 621
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 622
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 623
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 624
    .local v6, "rowNum":J
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_9

    .line 625
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 626
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_98:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 643
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPServer_98:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 644
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPServer:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 646
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 647
    const-string v1, "RTP"

    .line 648
    const-string v2, "98"

    .line 649
    const-string v3, "out"

    .line 646
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 653
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "application"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 654
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xfffffff

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 655
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 656
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 657
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 658
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 659
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/4 v1, 0x2

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 660
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 661
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 663
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 665
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 666
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_a

    .line 667
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 668
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 683
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 684
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPClient:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 687
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 688
    const-string v1, "RTP"

    .line 689
    const-string v2, "99"

    .line 690
    const-string v3, "in"

    .line 687
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 694
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "phone"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 695
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xffffffe

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 696
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 697
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 698
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 699
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 700
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v1, 0x10

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 701
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 702
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 704
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 705
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 706
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 707
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_b

    .line 708
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 709
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 719
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 720
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_2:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 721
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPClient:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 724
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 725
    const-string v1, "RTP"

    .line 726
    const-string v2, "99"

    .line 727
    const-string v3, "in"

    .line 724
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 731
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "phone"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 732
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xffffffe

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 733
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 734
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 735
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 736
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 737
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 738
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 739
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 741
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 743
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 744
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_c

    .line 745
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 746
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    :cond_6
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 757
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_99_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 758
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPClient:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 761
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 762
    const-string v1, "RTP"

    .line 763
    const-string v2, "99"

    .line 764
    const-string v3, "in"

    .line 761
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 768
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "phone"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 769
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xffffffe

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 770
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 771
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 772
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 773
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 774
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v1, 0x11

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 775
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 776
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 778
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 779
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 780
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 781
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_d

    .line 782
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 783
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    :cond_7
    :goto_5
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_98:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppDisplayName:Ljava/lang/String;

    .line 798
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppNameRTPClient_98:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 799
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDescriptionRTPClient:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDescription:Ljava/lang/String;

    .line 802
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 803
    const-string v1, "RTP"

    .line 804
    const-string v2, "98"

    .line 805
    const-string v3, "in"

    .line 802
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 809
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "phone"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioType:Ljava/lang/String;

    .line 810
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const v1, -0xffffffe

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCategory:I

    .line 811
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    .line 812
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentCategory:I

    .line 813
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoContentRules:I

    .line 814
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    .line 815
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const/16 v1, 0x10

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoContentCategory:I

    .line 816
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iput v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAudioInfoTustLevel:I

    .line 817
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "free"

    iput-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mResourceStatus:Ljava/lang/String;

    .line 819
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 820
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 821
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 822
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_e

    .line 823
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 824
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 837
    :cond_8
    :goto_6
    const-string v0, "AudioRtpAppholder"

    invoke-virtual {p1, v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerAppHolder(Ljava/lang/String;Lcom/samsung/android/mirrorlink/appmanager/IAppsHolderInterface;)V

    .line 838
    invoke-virtual {p1, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerIRtpParamsHolder(Lcom/samsung/android/mirrorlink/appmanager/IRtpParams;)V

    .line 840
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.init exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 628
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 629
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_3

    .line 630
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 631
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 670
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 671
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_4

    .line 672
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 673
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 711
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 712
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_5

    .line 713
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 714
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 748
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 749
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_6

    .line 750
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 751
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 785
    :cond_d
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 786
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_7

    .line 787
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 788
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 826
    :cond_e
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 827
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_8

    .line 828
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 829
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6
.end method

.method public regAppStatusEventListener(Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;)Z
    .locals 1
    .param p1, "appStatusListener"    # Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .prologue
    .line 850
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppStatusChangeCb:Lcom/samsung/android/mirrorlink/appmanager/IAppStatusListener;

    .line 851
    const/4 v0, 0x1

    return v0
.end method

.method public setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 0
    .param p1, "ca"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 224
    return-void
.end method

.method public setIplAndMpl(II)Ljava/util/List;
    .locals 9
    .param p1, "audioIpl"    # I
    .param p2, "audioMpl"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 959
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.setIPLandMPL enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 961
    .local v8, "updatedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v0

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    if-ne p1, v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->getRemotingInfo()Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;

    move-result-object v0

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    if-ne p2, v0, :cond_0

    .line 964
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, "AudioRtpAppholder.setIPLandMPL ipl mpl equal to old values.Return"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    :goto_0
    return-object v8

    .line 967
    :cond_0
    iput p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioIpl:I

    .line 968
    iput p2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAudioMpl:I

    .line 970
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 972
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 976
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 985
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "RTP"

    const-string v2, "99"

    const-string v3, "out"

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 987
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    const-string v1, "RTP"

    const-string v2, "98"

    const-string v3, "out"

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 990
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 991
    const-string v1, "RTP"

    .line 992
    const-string v2, "98"

    .line 993
    const-string v3, "in"

    move v4, p1

    move v5, p2

    .line 990
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 996
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 997
    const-string v1, "RTP"

    .line 998
    const-string v2, "99"

    .line 999
    const-string v3, "in"

    move v4, p1

    move v5, p2

    .line 996
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1002
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1003
    const-string v1, "RTP"

    .line 1004
    const-string v2, "99"

    .line 1005
    const-string v3, "in"

    move v4, p1

    move v5, p2

    .line 1002
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .line 1009
    const-string v1, "RTP"

    .line 1010
    const-string v2, "99"

    .line 1011
    const-string v3, "in"

    move v4, p1

    move v5, p2

    .line 1008
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->setRemotingInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1016
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1017
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1018
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1019
    .local v6, "rowNum":J
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_7

    .line 1020
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1021
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1022
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1033
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1034
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1035
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1036
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_8

    .line 1037
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1038
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1039
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1049
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1050
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1051
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1052
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_9

    .line 1053
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1054
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1055
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1066
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1068
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_a

    .line 1069
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1070
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1071
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1081
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1082
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1083
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1084
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_b

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1086
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    :cond_5
    :goto_5
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setPackageName(Ljava/lang/String;)V

    .line 1098
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->setCategory(I)V

    .line 1099
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mDbInfo:Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->insert(Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;)J

    move-result-wide v6

    .line 1100
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-eqz v0, :cond_c

    .line 1101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1102
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1103
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1114
    :cond_6
    :goto_6
    const-string v0, "TMSAudioRtpAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioRtpAppholder.setIPLandMPL exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "values ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1024
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1025
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_1

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1027
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1028
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1041
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1042
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_2

    .line 1043
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1044
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 1057
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1058
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_3

    .line 1059
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1060
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 1073
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1074
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_4

    .line 1075
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1076
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_2:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 1089
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1090
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_5

    .line 1091
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1092
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1093
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPClient_99_3:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    .line 1105
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppDbInterface:Lcom/samsung/android/mirrorlink/util/AppDbInterface;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/AppDbInterface;->getAppIdFromPackageName(Ljava/lang/String;)I

    move-result v0

    int-to-long v6, v0

    .line 1106
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_6

    .line 1107
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    long-to-int v1, v6

    iput v1, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 1108
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget v0, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1109
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mAppInfoRTPServer_98:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_6
.end method

.method public setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 2
    .param p1, "nativeIface"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 200
    const-string v0, "TMSAudioRtpAppholder"

    const-string v1, " setnativeinterface "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->mNativeIface:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 202
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 897
    const-string v0, "Server"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 898
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->launchAppRTPServer(Ljava/lang/String;)V

    .line 900
    :cond_0
    const-string v0, "Client"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 901
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->launchAppRTPClient(Ljava/lang/String;)V

    .line 903
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 914
    const-string v0, "TMSAudioRtpAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopApp for terminate RTP << "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    const-string v0, "Server"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->stopAppRTPServer()V

    .line 919
    :cond_0
    const-string v0, "Client"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 920
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->stopAppRTPClient()V

    .line 922
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
