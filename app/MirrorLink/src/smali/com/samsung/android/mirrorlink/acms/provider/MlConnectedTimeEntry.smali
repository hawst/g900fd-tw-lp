.class public Lcom/samsung/android/mirrorlink/acms/provider/MlConnectedTimeEntry;
.super Ljava/lang/Object;
.source "MlConnectedTimeEntry.java"


# instance fields
.field private mConnectedTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getConnectedTime()J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectedTimeEntry;->mConnectedTime:J

    return-wide v0
.end method

.method public setConnectedTime(J)V
    .locals 0
    .param p1, "connectedTime"    # J

    .prologue
    .line 30
    iput-wide p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/MlConnectedTimeEntry;->mConnectedTime:J

    .line 31
    return-void
.end method
