.class public interface abstract Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;
.super Ljava/lang/Object;
.source "AcmsHttpCertificateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AppCertCallback"
.end annotation


# static fields
.field public static final ERROR_BAD_REQUEST:I = 0x1

.field public static final ERROR_BAD_RESPONSE:I = 0x2

.field public static final ERROR_DATABASE_OFFLINE:I = 0x4

.field public static final ERROR_NO_CERT_AVAILABLE:I = 0x3


# virtual methods
.method public abstract onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V
.end method

.method public abstract onSuccess(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;[Ljava/security/cert/X509Certificate;)V
.end method
