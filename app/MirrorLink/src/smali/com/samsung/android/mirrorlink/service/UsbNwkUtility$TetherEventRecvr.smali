.class Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;
.super Landroid/content/BroadcastReceiver;
.source "UsbNwkUtility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TetherEventRecvr"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;)V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;-><init>(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 423
    const-string v10, "TMSSvc"

    const-string v11, "UsbNwkUtility.onReceive tether - enter"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 426
    .local v1, "action":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$0(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 427
    const-string v10, "TMSSvc"

    .line 428
    const-string v11, "UsbNwkUtility.onReceive tether - ACTION_TETHER_STATE_CHANGED"

    .line 427
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$1(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 432
    .local v2, "activeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 433
    .local v6, "mUsbRegexs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$2(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Landroid/net/ConnectivityManager;

    move-result-object v10

    if-eqz v10, :cond_7

    if-eqz v2, :cond_7

    .line 435
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$3(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/Class;

    move-result-object v10

    .line 436
    const-string v11, "getTetherableUsbRegexs"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Class;

    invoke-virtual {v10, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 437
    .local v5, "listener":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$2(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Landroid/net/ConnectivityManager;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v5, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "mUsbRegexs":[Ljava/lang/String;
    check-cast v6, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 454
    .restart local v6    # "mUsbRegexs":[Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v12

    array-length v13, v12

    const/4 v10, 0x0

    :goto_0
    if-lt v10, v13, :cond_1

    .line 480
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherActive:Z
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$5(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 482
    const-string v10, "TMSSvc"

    .line 483
    const-string v11, "UsbNwkUtility.onReceive stop server tether - "

    .line 482
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$7(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;Z)V

    .line 495
    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCmClass:Ljava/lang/Class;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$3(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Ljava/lang/Class;

    move-result-object v10

    const-string v11, "setNcmTethering"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Class;

    const/4 v13, 0x0

    sget-object v14, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 496
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mCm:Landroid/net/ConnectivityManager;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$2(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Landroid/net/ConnectivityManager;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v5, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 500
    :goto_1
    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setNcmReady(ZZ)V

    .line 501
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # invokes: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->stopTMServerService()V
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$8(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V

    .line 515
    .end local v2    # "activeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "listener":Ljava/lang/reflect/Method;
    .end local v6    # "mUsbRegexs":[Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 438
    .restart local v2    # "activeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v4

    .line 439
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    const-string v10, "TMSSvc"

    .line 440
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility Could not connect to channel."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 441
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 440
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 439
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 443
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v4

    .line 444
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v10, "TMSSvc"

    .line 445
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility Could not connect to channel."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 446
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 445
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 444
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 448
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v4

    .line 449
    .local v4, "e":Ljava/lang/IllegalAccessException;
    const-string v10, "TMSSvc"

    .line 450
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "UsbNwkUtility Could not connect to channel."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 451
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 450
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 449
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 454
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    .restart local v5    # "listener":Ljava/lang/reflect/Method;
    .restart local v6    # "mUsbRegexs":[Ljava/lang/String;
    :cond_1
    aget-object v7, v12, v10

    .local v7, "o":Ljava/lang/Object;
    move-object v9, v7

    .line 455
    check-cast v9, Ljava/lang/String;

    .line 456
    .local v9, "s":Ljava/lang/String;
    array-length v14, v6

    const/4 v11, 0x0

    :goto_3
    if-lt v11, v14, :cond_2

    .line 454
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 456
    :cond_2
    aget-object v8, v6, v11

    .line 457
    .local v8, "regex":Ljava/lang/String;
    invoke-virtual {v9, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 458
    # invokes: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isNcmReady()Z
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$4()Z

    move-result v3

    .line 459
    .local v3, "bNcmReady":Z
    if-eqz v3, :cond_4

    .line 460
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # getter for: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->mTetherActive:Z
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$5(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 461
    const-string v10, "TMSSvc"

    .line 462
    const-string v11, "UsbNwkUtility.onReceive Already Tethered. Received notification again!!!"

    .line 461
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # invokes: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->startTMServerService()V
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$6(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V

    .line 468
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$7(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;Z)V

    goto :goto_2

    .line 464
    :cond_3
    const-string v10, "TMSSvc"

    .line 465
    const-string v11, "UsbNwkUtility.onReceive start server tether - "

    .line 464
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 471
    :cond_4
    const-string v10, "TMSSvc"

    .line 472
    const-string v11, "UsbNwkUtility.onReceive bNcmReady is false "

    .line 471
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 456
    .end local v3    # "bNcmReady":Z
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 497
    .end local v7    # "o":Ljava/lang/Object;
    .end local v8    # "regex":Ljava/lang/String;
    .end local v9    # "s":Ljava/lang/String;
    :catch_3
    move-exception v4

    .line 498
    .local v4, "e":Ljava/lang/Exception;
    const-string v10, "TMSSvc"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Could not connect to channel."

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 503
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_6
    const-string v10, "TMSSvc"

    .line 504
    const-string v11, "UsbNwkUtility.onReceive mTetherActive is false "

    .line 503
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->isUsbConnected()Z

    move-result v10

    if-nez v10, :cond_0

    .line 506
    const-string v10, "TMSSvc"

    const-string v11, "Usb is not connected. Stopping the service"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility$TetherEventRecvr;->this$0:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    # invokes: Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->stopTMServerService()V
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->access$8(Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;)V

    goto/16 :goto_2

    .line 512
    .end local v5    # "listener":Ljava/lang/reflect/Method;
    :cond_7
    const-string v10, "TMSSvc"

    const-string v11, "UsbNwkUtility.onReceive mCM is NULL "

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
