.class Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$1;
.super Landroid/os/RemoteCallbackList;
.source "ConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Lcom/mirrorlink/android/commonapi/IConnectionListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 43
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/mirrorlink/android/commonapi/IConnectionListener;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager$1;->onCallbackDied(Lcom/mirrorlink/android/commonapi/IConnectionListener;Ljava/lang/Object;)V

    return-void
.end method

.method public onCallbackDied(Lcom/mirrorlink/android/commonapi/IConnectionListener;Ljava/lang/Object;)V
    .locals 2
    .param p1, "callback"    # Lcom/mirrorlink/android/commonapi/IConnectionListener;
    .param p2, "packName"    # Ljava/lang/Object;

    .prologue
    .line 46
    const-string v0, "TMSConnectionManager"

    const-string v1, "ConnectionManager.onCallbackDied()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-super {p0, p1}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;)V

    .line 48
    return-void
.end method
