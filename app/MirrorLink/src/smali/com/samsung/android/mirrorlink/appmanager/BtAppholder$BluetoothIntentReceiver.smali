.class Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BtAppholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothIntentReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;)V
    .locals 0

    .prologue
    .line 547
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;-><init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 552
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 553
    .local v1, "action":Ljava/lang/String;
    const-string v9, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 555
    const-string v9, "TMSBtAppholder"

    const-string v10, " ACTION_FOUND "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const-string v9, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 557
    .local v4, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v4, :cond_0

    .line 559
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleDeviceFound(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v9, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$8(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;Landroid/bluetooth/BluetoothDevice;)V

    .line 562
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    const-string v9, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 564
    const-string v9, "TMSBtAppholder"

    const-string v10, "  ACTION_STATE_CHANGED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v9, "android.bluetooth.adapter.extra.STATE"

    const/high16 v10, -0x80000000

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 566
    .local v3, "curState":I
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getAcsState(I)I
    invoke-static {v10, v3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$9(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)I

    move-result v10

    const/4 v11, 0x0

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v11}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    .line 568
    .end local v3    # "curState":I
    :cond_1
    const-string v9, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 570
    const-string v9, "TMSBtAppholder"

    const-string v10, " ACTION_BOND_STATE_CHANGED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v9, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 572
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    const-string v9, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v10, -0x80000000

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 573
    .local v2, "bondState":I
    if-eqz v4, :cond_2

    .line 575
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v10, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getAcsBondedState(I)I
    invoke-static {v10, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$10(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;I)I

    move-result v10

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    .line 578
    .end local v2    # "bondState":I
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    const-string v9, "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 580
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothAdapter.ACTION_SCAN_MODE_CHANGED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v9, "android.bluetooth.adapter.extra.SCAN_MODE"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 584
    .local v7, "scanMode":I
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getFlagStartConnection()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 586
    const-string v9, "TMSBtAppholder"

    const-string v10, " No Action Needed"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    .end local v7    # "scanMode":I
    :cond_3
    :goto_0
    const-string v9, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 609
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string v9, "android.bluetooth.profile.extra.STATE"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 612
    .local v0, "CurState":I
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "BluetoothA2dp.EXTRA_SINK_STATE = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    const-string v9, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 614
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v4, :cond_4

    .line 616
    packed-switch v0, :pswitch_data_0

    .line 630
    .end local v0    # "CurState":I
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_4
    :goto_1
    :pswitch_0
    const-string v9, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 632
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v9, "android.bluetooth.profile.extra.STATE"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 635
    .restart local v0    # "CurState":I
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "BluetoothHeadset.EXTRA_SINK_STATE = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const-string v9, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 637
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v4, :cond_5

    .line 639
    packed-switch v0, :pswitch_data_1

    .line 653
    .end local v0    # "CurState":I
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_5
    :goto_2
    :pswitch_1
    const-string v9, "android.bluetooth.device.action.UUID"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 655
    const-string v9, "TMSBtAppholder"

    const-string v10, "BluetoothDevice.ACTION_UUID action"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v9, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    .line 658
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v4, :cond_c

    .line 660
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 661
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 662
    const-string v9, "TMSBtAppholder"

    const-string v10, " ACTION_UUID : found mTmClientMac device"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$14(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    move-result-object v9

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 664
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$12(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 665
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$14(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    move-result-object v9

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v9, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z

    .line 667
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$11(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 668
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$13(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    move-result-object v9

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v9, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 702
    .end local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_7
    :goto_3
    return-void

    .line 590
    .restart local v7    # "scanMode":I
    :cond_8
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " Action Needed if still not connected mAppIsRunningA2dp "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$11(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " Action Needed if still not connected mAppIsRunningHfp "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z
    invoke-static {v11}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$12(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningA2dp:Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$11(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mA2dpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$13(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;

    move-result-object v9

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->isTmConnected()Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 593
    :cond_9
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mAppIsRunningHfp:Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$12(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHfpManager:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$14(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;

    move-result-object v9

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 595
    :cond_a
    const-string v9, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 596
    .local v8, "timeout":I
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->checkMakeDiscoverable(II)Z
    invoke-static {v9, v7, v8}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;II)Z

    move-result v5

    .line 597
    .local v5, "makeDiscoverable":Z
    if-eqz v5, :cond_3

    .line 599
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " Action Needed Scan Mode "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$15(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/os/Handler;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 601
    .local v6, "msg":Landroid/os/Message;
    const/4 v9, 0x5

    iput v9, v6, Landroid/os/Message;->what:I

    .line 602
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$15(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;)Landroid/os/Handler;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    invoke-virtual {v9, v6, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 619
    .end local v5    # "makeDiscoverable":Z
    .end local v6    # "msg":Landroid/os/Message;
    .end local v7    # "scanMode":I
    .end local v8    # "timeout":I
    .restart local v0    # "CurState":I
    .restart local v4    # "device":Landroid/bluetooth/BluetoothDevice;
    :pswitch_2
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothProfile.STATE_DISCONNECTED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    const/16 v10, 0xd

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 623
    :pswitch_3
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothProfile.STATE_CONNECTED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    const/16 v10, 0xc

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 642
    :pswitch_4
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothProfile.STATE_DISCONNECTED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    const/16 v10, 0xf

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    goto/16 :goto_2

    .line 646
    :pswitch_5
    const-string v9, "TMSBtAppholder"

    const-string v10, " BluetoothProfile.STATE_CONNECTED "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$BluetoothIntentReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    const/16 v10, 0xe

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->handleStateChange(ILjava/lang/Object;)V
    invoke-static {v9, v10, v4}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;ILjava/lang/Object;)V

    goto/16 :goto_2

    .line 671
    .end local v0    # "CurState":I
    :cond_b
    const-string v9, "TMSBtAppholder"

    const-string v10, " mTmClientMac device already connected return"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 699
    :cond_c
    const-string v9, "TMSBtAppholder"

    const-string v10, "ACTION_UUID : device is null"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 616
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 639
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method
