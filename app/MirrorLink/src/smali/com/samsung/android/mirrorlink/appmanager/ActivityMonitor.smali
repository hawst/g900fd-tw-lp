.class public Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;
.super Ljava/lang/Thread;
.source "ActivityMonitor.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSActMonitor"


# instance fields
.field mActivityStateHandler:Landroid/os/Handler;

.field private mActivtyMngr:Landroid/app/ActivityManager;

.field private mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mCount:I

.field private mCurrTopApp:Ljava/lang/String;

.field mRun:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V
    .locals 2
    .param p1, "cntxt"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "appMngr"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 39
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivityStateHandler:Landroid/os/Handler;

    .line 41
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mRun:Z

    .line 43
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 44
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I

    .line 47
    const-string v0, "TMSActMonitor"

    const-string v1, "ActvityMonitor.handleMessage enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivtyMngr:Landroid/app/ActivityManager;

    .line 49
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivityStateHandler:Landroid/os/Handler;

    .line 50
    iput-object p3, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    .line 52
    const-string v0, "TMS_ACTIVITY_MONITOR"

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->setName(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method private sendActivityStateChangedEvent(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivityStateHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 114
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    .local v0, "bndl":Landroid/os/Bundle;
    const-string v2, "state"

    const-string v3, "dummy"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v2, "AppName"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/16 v2, 0x64

    iput v2, v1, Landroid/os/Message;->what:I

    .line 118
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 119
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    .line 120
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivityStateHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 121
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 57
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 58
    :goto_0
    iget-boolean v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mRun:Z

    if-nez v4, :cond_0

    .line 108
    :goto_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 109
    return-void

    .line 64
    :cond_0
    monitor-enter p0

    .line 65
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mActivtyMngr:Landroid/app/ActivityManager;

    const/4 v5, 0x1

    const/high16 v6, 0x10000000

    invoke-virtual {v4, v5, v6}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v3

    .line 66
    .local v3, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    const/4 v2, 0x0

    .line 67
    .local v2, "pkgName":Ljava/lang/String;
    if-eqz v3, :cond_1

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v4, :cond_1

    .line 69
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 72
    :cond_1
    if-nez v2, :cond_2

    .line 73
    const-string v4, "TMSActMonitor"

    const-string v5, "pkgName is null check!"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    monitor-exit p0

    goto :goto_1

    .line 64
    .end local v2    # "pkgName":Ljava/lang/String;
    .end local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 76
    .restart local v2    # "pkgName":Ljava/lang/String;
    .restart local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v4, v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 78
    .local v1, "isPkgValid":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 80
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I

    .line 83
    const-string v4, ""

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 84
    const-string v4, "TMSActMonitor"

    const-string v5, "ActvityMonitor.run currentTopApp is empty"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    .line 87
    :cond_3
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->sendActivityStateChangedEvent(Ljava/lang/String;)V

    .line 64
    :cond_4
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    const-wide/16 v4, 0x64

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 89
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    if-nez v1, :cond_4

    :try_start_3
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 90
    iget v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I

    .line 94
    iget v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I

    const/4 v5, 0x2

    if-le v4, v5, :cond_4

    .line 95
    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->sendActivityStateChangedEvent(Ljava/lang/String;)V

    .line 96
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCount:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized sendCurActivity()V
    .locals 2

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSActMonitor"

    const-string v1, "set currentTopApp to empty so ActivityMonitor send appstatus at the next loop of run()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mCurrTopApp:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    monitor-exit p0

    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized start()V
    .locals 1

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mRun:Z

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mRun:Z

    .line 143
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_0
    monitor-exit p0

    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public start(Ljava/lang/String;)V
    .locals 4
    .param p1, "startPkgName"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->sendActivityStateChangedEvent(Ljava/lang/String;)V

    .line 125
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor$1;-><init>(Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;)V

    .line 130
    const-wide/16 v2, 0x64

    .line 125
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    return-void
.end method

.method public declared-synchronized stopThread()V
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/ActivityMonitor;->mRun:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
