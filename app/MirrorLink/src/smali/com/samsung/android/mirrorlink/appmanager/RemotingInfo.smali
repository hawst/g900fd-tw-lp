.class public Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;
.super Ljava/lang/Object;
.source "RemotingInfo.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field public mAudioIpl:I

.field public mAudioMpl:I

.field public mdirection:Ljava/lang/String;

.field public mformat:Ljava/lang/String;

.field public mprotocolId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "protocolId"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "direction"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 33
    iput v3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 34
    iput v3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    .line 66
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " RemotingInfo.RemotingInfo enter protocolId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "format = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "direction = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 68
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 72
    iput v3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 73
    iput v3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    .line 74
    const-string v0, "TMSAppMngr"

    const-string v1, " RemotingInfo.RemotingInfo exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "protocolId"    # Ljava/lang/String;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "direction"    # Ljava/lang/String;
    .param p4, "audioIpl"    # I
    .param p5, "audioMpl"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 34
    iput v1, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    .line 47
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " RemotingInfo.RemotingInfo enter protocolId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "format = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "direction = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 49
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mprotocolId:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mformat:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mdirection:Ljava/lang/String;

    .line 53
    iput p4, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioIpl:I

    .line 54
    iput p5, p0, Lcom/samsung/android/mirrorlink/appmanager/RemotingInfo;->mAudioMpl:I

    .line 55
    const-string v0, "TMSAppMngr"

    const-string v1, " RemotingInfo.RemotingInfo exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
