.class public Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;
.super Ljava/lang/Object;
.source "AcmsHttpRevokationHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;
    }
.end annotation


# static fields
.field private static final CERT_EXTENSION_AIA:Ljava/lang/String; = "1.3.6.1.5.5.7.1.1"

.field private static final CHARSET_US_ASCII:Ljava/lang/String; = "US-ASCII"

.field private static final CHARSET_UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final HTTP:Ljava/lang/String; = "http"

.field private static final LDAP:Ljava/lang/String; = "ldap"

.field private static final NO_PACKAGE_NAME:Ljava/lang/String; = "no_package_to_show"

.field private static final OCSP_RESP_EXTENSION_BASE_GRACE_PERIOD:Ljava/lang/String; = "1.3.6.1.4.1.41577.1.3"

.field private static final OCSP_RESP_EXTENSION_DRIVE_GRACE_PERIOD:Ljava/lang/String; = "1.3.6.1.4.1.41577.1.2"

.field private static final OCSP_RESP_EXTENSION_NONCE:Ljava/lang/String; = "1.3.6.1.5.5.7.48.1.2"

.field private static final OCSP_RESP_EXTENSION_QUERY_PERIOD:Ljava/lang/String; = "1.3.6.1.4.1.41577.1.1"

.field private static final TAG:Ljava/lang/String; = "AcmsHttpRevokationHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkResponseSignature(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Z
    .locals 9
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "rootCert"    # Ljava/security/cert/X509Certificate;
    .param p4, "basicOcspResp"    # Lorg/bouncycastle/ocsp/BasicOCSPResp;
    .param p5, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Ljava/security/cert/X509Certificate;",
            "Lorg/bouncycastle/ocsp/BasicOCSPResp;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/4 v8, 0x6

    const/4 v4, 0x0

    .line 521
    if-nez p4, :cond_0

    .line 564
    :goto_0
    return v4

    .line 528
    :cond_0
    invoke-virtual {p4}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getSignatureAlgName()Ljava/lang/String;

    move-result-object v2

    .line 529
    .local v2, "respAlgName":Ljava/lang/String;
    const-string v5, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "respAlgName= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getSigAlgName()Ljava/lang/String;

    move-result-object v3

    .line 532
    .local v3, "rootCertAlgName":Ljava/lang/String;
    const-string v5, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "rootCertAlgName= "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 535
    invoke-interface {p2, p1, v8, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 540
    :cond_1
    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v1

    .line 543
    .local v1, "issuerKey":Ljava/security/PublicKey;
    :try_start_0
    sget-object v5, Lorg/bouncycastle/jce/provider/BouncyCastleProvider;->PROVIDER_NAME:Ljava/lang/String;

    .line 542
    invoke-virtual {p4, v1, v5}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->verify(Ljava/security/PublicKey;Ljava/lang/String;)Z

    move-result v5

    .line 543
    if-nez v5, :cond_2

    .line 544
    const-string v5, "AcmsHttpRevokationHandler"

    const-string v6, "checkRevocation() Ocsp Revocation Not signed"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const/4 v5, 0x6

    invoke-interface {p2, p1, v5, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V
    :try_end_0
    .catch Lorg/bouncycastle/ocsp/OCSPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 550
    :catch_0
    move-exception v0

    .line 551
    .local v0, "e":Lorg/bouncycastle/ocsp/OCSPException;
    invoke-virtual {v0}, Lorg/bouncycastle/ocsp/OCSPException;->printStackTrace()V

    .line 552
    const-string v5, "AcmsHttpRevokationHandler"

    const-string v6, "checkRevocation() OCSPException Exception"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-interface {p2, p1, v8, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 556
    .end local v0    # "e":Lorg/bouncycastle/ocsp/OCSPException;
    :catch_1
    move-exception v0

    .line 557
    .local v0, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    .line 558
    const-string v5, "AcmsHttpRevokationHandler"

    const-string v6, "checkRevocation() Ocsp Signing Exception"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-interface {p2, p1, v8, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 563
    .end local v0    # "e":Ljava/security/NoSuchProviderException;
    :cond_2
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, " checkResponseSignature successfull "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private checkResponseStatus(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Lorg/bouncycastle/ocsp/OCSPResp;Z)Z
    .locals 6
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "ocspResp"    # Lorg/bouncycastle/ocsp/OCSPResp;
    .param p4, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Lorg/bouncycastle/ocsp/OCSPResp;",
            "Z)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 480
    invoke-virtual {p3}, Lorg/bouncycastle/ocsp/OCSPResp;->getStatus()I

    move-result v0

    .line 481
    .local v0, "status":I
    const-string v3, "AcmsHttpRevokationHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ocspResp.getStatus(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    if-eqz v0, :cond_4

    .line 483
    const-string v3, "AcmsHttpRevokationHandler"

    const-string v4, "ocsp response is not success"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    .line 486
    const/4 v2, 0x7

    invoke-interface {p2, p1, v2, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 515
    :goto_0
    return v1

    .line 490
    :cond_0
    if-ne v0, v2, :cond_1

    .line 491
    const/16 v2, 0x8

    invoke-interface {p2, p1, v2, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 496
    :cond_1
    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 497
    const/16 v2, 0x9

    invoke-interface {p2, p1, v2, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 502
    :cond_2
    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    .line 503
    const/16 v2, 0xc

    invoke-interface {p2, p1, v2, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 508
    :cond_3
    const/4 v3, 0x6

    if-ne v0, v3, :cond_4

    .line 509
    const/16 v2, 0xd

    invoke-interface {p2, p1, v2, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 514
    :cond_4
    const-string v1, "AcmsHttpRevokationHandler"

    const-string v3, " OCSP_RESP_SUCCESSFUL "

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 515
    goto :goto_0
.end method

.method private extractExtensionValues(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Ljava/math/BigInteger;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    .locals 10
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "requestNonce"    # Ljava/math/BigInteger;
    .param p4, "basicOcspResp"    # Lorg/bouncycastle/ocsp/BasicOCSPResp;
    .param p5, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Ljava/math/BigInteger;",
            "Lorg/bouncycastle/ocsp/BasicOCSPResp;",
            "Z)",
            "Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;"
        }
    .end annotation

    .prologue
    .line 623
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const-string v6, "AcmsHttpRevokationHandler"

    .line 624
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "checkRevocation() Version "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getVersion()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 623
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v6, "AcmsHttpRevokationHandler"

    .line 628
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "checkRevocation() ResponderId "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 629
    invoke-virtual {p4}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getResponderId()Lorg/bouncycastle/ocsp/RespID;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 628
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 626
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    invoke-virtual {p4}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getNonCriticalExtensionOIDs()Ljava/util/Set;

    move-result-object v4

    .line 639
    .local v4, "ocspNonCritOids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v5, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;

    invoke-direct {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;-><init>()V

    .line 640
    .local v5, "respData":Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    new-instance v2, Lorg/bouncycastle/asn1/DEROctetString;

    invoke-virtual {p3}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v6

    invoke-direct {v2, v6}, Lorg/bouncycastle/asn1/DEROctetString;-><init>([B)V

    .line 642
    .local v2, "nonceArr":Lorg/bouncycastle/asn1/DEROctetString;
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 697
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v6

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->setDriveGracePeriod(I)V

    .line 698
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getBaseGracePeriod()I

    move-result v6

    .line 699
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v7

    sub-int/2addr v6, v7

    .line 700
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v7

    sub-int/2addr v6, v7

    .line 698
    invoke-virtual {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->setBaseGracePeriod(I)V

    .line 701
    const-string v6, "AcmsHttpRevokationHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "checkRevocation() \n Query["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 702
    const-string v8, "] \n Base["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getBaseGracePeriod()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] \n Drive["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 703
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 701
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    .end local v5    # "respData":Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    :goto_1
    return-object v5

    .line 642
    .restart local v5    # "respData":Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 643
    .local v1, "nonCritOid":Ljava/lang/String;
    const-string v7, "AcmsHttpRevokationHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() nonCritOid "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    invoke-virtual {p4, v1}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v0

    .line 646
    .local v0, "arr":[B
    if-nez v0, :cond_2

    .line 647
    const-string v7, "AcmsHttpRevokationHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " No Value "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 651
    :cond_2
    const-string v7, "1.3.6.1.4.1.41577.1.1"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 652
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getIntegerFromOcspExtensionValue([B)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->setQueryPeriod(I)V

    .line 653
    const-string v7, "AcmsHttpRevokationHandler"

    .line 654
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() OCSP_RESP_EXTENSION_QUERY_PERIOD="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 655
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getQueryPeriod()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 654
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 653
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :cond_3
    const-string v7, "1.3.6.1.4.1.41577.1.3"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 659
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getIntegerFromOcspExtensionValue([B)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->setBaseGracePeriod(I)V

    .line 660
    const-string v7, "AcmsHttpRevokationHandler"

    .line 661
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() OCSP_RESP_EXTENSION_BASE_GRACE_PERIOD="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 662
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getBaseGracePeriod()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 661
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 660
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_4
    const-string v7, "1.3.6.1.4.1.41577.1.2"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 666
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getIntegerFromOcspExtensionValue([B)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->setDriveGracePeriod(I)V

    .line 667
    const-string v7, "AcmsHttpRevokationHandler"

    .line 668
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() OCSP_RESP_EXTENSION_DRIVE_GRACE_PERIOD="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 669
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;->getDriveGracePeriod()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 668
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 667
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :cond_5
    const-string v7, "1.3.6.1.5.5.7.48.1.2"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 678
    invoke-virtual {v2}, Lorg/bouncycastle/asn1/DEROctetString;->getDEREncoded()[B

    move-result-object v3

    .line 680
    .local v3, "nonceBytes":[B
    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    if-nez v7, :cond_0

    .line 681
    const-string v6, "AcmsHttpRevokationHandler"

    .line 682
    const-string v7, "checkRevocation() OCSP_RESP_EXTENSION_NONCE Error "

    .line 681
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const/4 v6, 0x5

    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 685
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method private getAppListEntries()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method private getCerts(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 3
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "appId"    # Ljava/lang/String;

    .prologue
    .line 389
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 391
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v2

    .line 392
    invoke-virtual {v2, p3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 393
    .local v1, "devId":Ljava/lang/String;
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v2

    .line 394
    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 399
    .end local v1    # "devId":Ljava/lang/String;
    .local v0, "certs":[Ljava/security/cert/X509Certificate;
    :goto_0
    return-object v0

    .line 396
    .end local v0    # "certs":[Ljava/security/cert/X509Certificate;
    :cond_0
    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v2

    .line 397
    invoke-virtual {v2, p3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 396
    .restart local v0    # "certs":[Ljava/security/cert/X509Certificate;
    goto :goto_0
.end method

.method private getIntegerFromOcspExtensionValue([B)I
    .locals 11
    .param p1, "arr"    # [B

    .prologue
    const/4 v4, -0x1

    .line 708
    const/4 v7, 0x0

    aget-byte v7, p1, v7

    const/4 v8, 0x4

    if-eq v7, v8, :cond_1

    .line 709
    const-string v7, "AcmsHttpRevokationHandler"

    const-string v8, " getIntegerFromOcspExtensionValue: Not Octect String"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :cond_0
    :goto_0
    return v4

    .line 712
    :cond_1
    const/4 v5, 0x0

    .line 714
    .local v5, "s":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/lang/String;

    .end local v5    # "s":Ljava/lang/String;
    const-string v7, "UTF-8"

    invoke-direct {v5, p1, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "UTF-8"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 720
    .local v0, "arrOctetDecoded":[B
    const-string v7, "AcmsHttpRevokationHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Array in string from OCSP Response: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    const/4 v4, 0x0

    .line 726
    .local v4, "ret":I
    const/4 v2, 0x2

    .local v2, "i":I
    :goto_1
    array-length v7, v0

    if-ge v2, v7, :cond_0

    .line 727
    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    array-length v9, v0

    sub-int/2addr v9, v2

    add-int/lit8 v9, v9, -0x1

    int-to-double v9, v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    double-to-int v3, v7

    .line 728
    .local v3, "radix":I
    aget-byte v7, v0, v2

    add-int/lit8 v6, v7, -0x30

    .line 729
    .local v6, "value":I
    mul-int v7, v6, v3

    add-int/2addr v4, v7

    .line 726
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 715
    .end local v0    # "arrOctetDecoded":[B
    .end local v2    # "i":I
    .end local v3    # "radix":I
    .end local v4    # "ret":I
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "value":I
    :catch_0
    move-exception v1

    .line 716
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method private getRequestGenerator(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/ocsp/OCSPReqGenerator;)Lorg/bouncycastle/ocsp/OCSPReqGenerator;
    .locals 3
    .param p1, "interCert"    # Ljava/security/cert/X509Certificate;
    .param p2, "rootCert"    # Ljava/security/cert/X509Certificate;
    .param p3, "gen"    # Lorg/bouncycastle/ocsp/OCSPReqGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 379
    new-instance v1, Lorg/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v1}, Lorg/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v1}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 380
    new-instance v0, Lorg/bouncycastle/ocsp/CertificateID;

    const-string v1, "1.3.14.3.2.26"

    .line 381
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v2

    .line 380
    invoke-direct {v0, v1, p2, v2}, Lorg/bouncycastle/ocsp/CertificateID;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/math/BigInteger;)V

    .line 382
    .local v0, "id":Lorg/bouncycastle/ocsp/CertificateID;
    invoke-virtual {p3, v0}, Lorg/bouncycastle/ocsp/OCSPReqGenerator;->addRequest(Lorg/bouncycastle/ocsp/CertificateID;)V

    .line 383
    return-object p3
.end method

.method private getResponseForRequest(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;[BLorg/bouncycastle/ocsp/OCSPReq;ZLandroid/content/Context;)Ljava/net/HttpURLConnection;
    .locals 17
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "aia"    # [B
    .param p4, "request"    # Lorg/bouncycastle/ocsp/OCSPReq;
    .param p5, "isManualRevoc"    # Z
    .param p6, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "[B",
            "Lorg/bouncycastle/ocsp/OCSPReq;",
            "Z",
            "Landroid/content/Context;",
            ")",
            "Ljava/net/HttpURLConnection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .prologue
    .line 406
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/4 v11, 0x0

    .line 407
    .local v11, "serviceAddr":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 410
    :try_start_0
    new-instance v10, Ljava/lang/String;

    const-string v13, "US-ASCII"

    move-object/from16 v0, p3

    invoke-direct {v10, v0, v13}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 416
    .local v10, "s":Ljava/lang/String;
    const-string v13, "http"

    invoke-virtual {v10, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 417
    .local v8, "i":I
    const/4 v13, -0x1

    if-ne v8, v13, :cond_2

    .line 418
    const-string v13, "ldap"

    invoke-virtual {v10, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 422
    :goto_0
    const/4 v13, -0x1

    if-eq v8, v13, :cond_0

    .line 423
    new-instance v11, Ljava/lang/String;

    .end local v11    # "serviceAddr":Ljava/lang/String;
    add-int/lit8 v13, v8, -0x1

    aget-byte v13, p3, v13

    .line 424
    const-string v14, "US-ASCII"

    invoke-static {v14}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v14

    .line 423
    move-object/from16 v0, p3

    invoke-direct {v11, v0, v8, v13, v14}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 427
    .end local v8    # "i":I
    .end local v10    # "s":Ljava/lang/String;
    .restart local v11    # "serviceAddr":Ljava/lang/String;
    :cond_0
    if-eqz v11, :cond_1

    const-string v13, "http"

    invoke-virtual {v11, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 428
    :cond_1
    const/4 v4, 0x0

    .line 470
    :goto_1
    return-object v4

    .line 411
    :catch_0
    move-exception v6

    .line 412
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v13, Ljava/lang/IllegalStateException;

    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 420
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v8    # "i":I
    .restart local v10    # "s":Ljava/lang/String;
    :cond_2
    const-string v13, "AcmsHttpRevokationHandler"

    const-string v14, "httpRevocation index of http is not -1 "

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 433
    .end local v8    # "i":I
    .end local v10    # "s":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    .line 434
    .local v3, "array":[B
    invoke-virtual/range {p4 .. p4}, Lorg/bouncycastle/ocsp/OCSPReq;->getEncoded()[B

    move-result-object v3

    .line 436
    const-string v13, "AcmsHttpRevokationHandler"

    .line 437
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "OCSP Request array: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 438
    new-instance v15, Ljava/lang/String;

    const-string v16, "US-ASCII"

    invoke-static/range {v16 .. v16}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v15, v3, v0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 437
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 435
    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const/4 v4, 0x0

    .line 440
    .local v4, "con":Ljava/net/HttpURLConnection;
    new-instance v12, Ljava/net/URL;

    invoke-direct {v12, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 441
    .local v12, "url":Ljava/net/URL;
    invoke-virtual {v12}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    .end local v4    # "con":Ljava/net/HttpURLConnection;
    check-cast v4, Ljava/net/HttpURLConnection;

    .line 442
    .restart local v4    # "con":Ljava/net/HttpURLConnection;
    const-string v13, "Content-Type"

    const-string v14, "application/ocsp-request"

    invoke-virtual {v4, v13, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v13, "Accept"

    const-string v14, "application/ocsp-response"

    invoke-virtual {v4, v13, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v13, "User-Agent"

    invoke-static/range {p6 .. p6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v13, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 446
    const-string v13, "AcmsHttpRevokationHandler"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "OCSP Request URL: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    .line 448
    .local v9, "out":Ljava/io/OutputStream;
    new-instance v5, Ljava/io/DataOutputStream;

    .line 449
    new-instance v13, Ljava/io/BufferedOutputStream;

    invoke-direct {v13, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 448
    invoke-direct {v5, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 451
    .local v5, "dataOut":Ljava/io/DataOutputStream;
    const-string v13, "AcmsHttpRevokationHandler"

    const-string v14, "checkRevocation() Sending Request "

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-virtual {v5, v3}, Ljava/io/DataOutputStream;->write([B)V

    .line 454
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->flush()V

    .line 455
    invoke-virtual {v5}, Ljava/io/DataOutputStream;->close()V

    .line 456
    const-string v13, "AcmsHttpRevokationHandler"

    const-string v14, "checkRevocation() Received Response "

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    .line 462
    .local v7, "httpStatusCode":I
    div-int/lit8 v13, v7, 0x64

    const/4 v14, 0x2

    if-eq v13, v14, :cond_4

    .line 463
    const-string v13, "AcmsHttpRevokationHandler"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Http Error "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const/4 v13, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move/from16 v2, p5

    invoke-interface {v0, v1, v13, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 467
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 469
    :cond_4
    const-string v13, "AcmsHttpRevokationHandler"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "checkRevocation() Http Resp "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private isOCSPCertValid(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Z
    .locals 7
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "basicOcspResp"    # Lorg/bouncycastle/ocsp/BasicOCSPResp;
    .param p5, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Landroid/content/Context;",
            "Lorg/bouncycastle/ocsp/BasicOCSPResp;",
            "Z)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/4 v6, 0x2

    const/4 v3, 0x0

    .line 576
    :try_start_0
    const-string v4, "BC"

    invoke-virtual {p4, v4}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getCerts(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 577
    .local v2, "ocspCerts":[Ljava/security/cert/X509Certificate;
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;

    invoke-direct {v0, p3}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;-><init>(Landroid/content/Context;)V

    .line 579
    .local v0, "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    array-length v4, v2

    if-lez v4, :cond_0

    .line 580
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->verifyOCSPChain([Ljava/security/cert/X509Certificate;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 581
    const-string v4, "AcmsHttpRevokationHandler"

    .line 582
    const-string v5, "checkRevocation() verifyOCSPChain for OCSP failed"

    .line 581
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const/16 v4, 0xe

    invoke-interface {p2, p1, v4, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_4

    .line 613
    .end local v0    # "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    .end local v2    # "ocspCerts":[Ljava/security/cert/X509Certificate;
    :goto_0
    return v3

    .line 587
    :catch_0
    move-exception v1

    .line 588
    .local v1, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    .line 589
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, "checkRevocation() NoSuchProviderException Error "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 592
    .end local v1    # "e":Ljava/security/NoSuchProviderException;
    :catch_1
    move-exception v1

    .line 593
    .local v1, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 594
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, "checkRevocation() InvalidKeyException Error "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 597
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v1

    .line 598
    .local v1, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    .line 599
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, "checkRevocation() CertificateException Error "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 602
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_3
    move-exception v1

    .line 603
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 604
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, "checkRevocation() NoSuchAlgorithmException Error "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 607
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_4
    move-exception v1

    .line 608
    .local v1, "e":Ljava/security/SignatureException;
    invoke-virtual {v1}, Ljava/security/SignatureException;->printStackTrace()V

    .line 609
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v5, "checkRevocation() SignatureException Error "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    invoke-interface {p2, p1, v6, p5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto :goto_0

    .line 613
    .end local v1    # "e":Ljava/security/SignatureException;
    .restart local v0    # "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    .restart local v2    # "ocspCerts":[Ljava/security/cert/X509Certificate;
    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isUpdated(Ljava/util/Date;Ljava/util/Date;)Z
    .locals 2
    .param p1, "nextUpdate"    # Ljava/util/Date;
    .param p2, "producedAt"    # Ljava/util/Date;

    .prologue
    .line 361
    if-nez p1, :cond_0

    .line 366
    const-string v0, "AcmsHttpRevokationHandler"

    const-string v1, "Next Update is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const/4 v0, 0x0

    .line 369
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public checkRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)Z
    .locals 3
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "isManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Landroid/content/Context;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const-string v0, "AcmsHttpRevokationHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkRevocation() Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;-><init>(Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler$RevocationtHttpThread;->start(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)V

    .line 103
    const-string v0, "AcmsHttpRevokationHandler"

    const-string v1, "checkRevocation Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public httpRevocation(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Z)V
    .locals 49
    .param p2, "cb"    # Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "mIsManualRevoc"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;",
            "Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "appEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    if-nez p1, :cond_1

    .line 150
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "AppEntries is null: Hence ignore"

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .line 155
    .local v25, "apps":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    new-instance v32, Lorg/bouncycastle/ocsp/OCSPReqGenerator;

    invoke-direct/range {v32 .. v32}, Lorg/bouncycastle/ocsp/OCSPReqGenerator;-><init>()V

    .line 156
    .local v32, "gen":Lorg/bouncycastle/ocsp/OCSPReqGenerator;
    const/16 v43, 0x0

    .line 157
    .local v43, "rootCert":Ljava/security/cert/X509Certificate;
    const/4 v7, 0x0

    .line 159
    .local v7, "authorityInfoAcess":[B
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v5, "newAppEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    const/16 v35, 0x1

    .line 163
    .local v35, "isAllCertsNull":Z
    :cond_2
    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 196
    if-eqz v35, :cond_5

    .line 197
    const-string v4, "AcmsHttpRevokationHandler"

    .line 198
    const-string v6, "None of the apps have entry in KeyStore. Hence Revocation wont be performed. "

    .line 197
    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    if-eqz p4, :cond_0

    .line 201
    :try_start_0
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->getAcmsManager(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;

    move-result-object v4

    const-string v6, "no_package_to_show"

    .line 202
    sget-object v9, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->NO_VALIDAPPS_TO_PERFORM_REVOCATION:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback$Result;->ordinal()I

    move-result v9

    .line 201
    invoke-virtual {v4, v6, v9}, Lcom/samsung/android/mirrorlink/acms/api/AcmsManager;->notifyRevocationCheckResult(Ljava/lang/String;I)V

    .line 203
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "notifyResult.RemoteException()  "

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v30

    .line 207
    .local v30, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v30 .. v30}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 165
    .end local v30    # "e":Landroid/os/RemoteException;
    :cond_3
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 166
    .local v20, "appEnt":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 167
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "Packagename is null: Hence ignore"

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 171
    :cond_4
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v22

    .line 173
    .local v22, "appId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, p3

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getCerts(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v28

    .line 174
    .local v28, "certs":[Ljava/security/cert/X509Certificate;
    if-eqz v28, :cond_2

    move-object/from16 v0, v28

    array-length v4, v0

    const/4 v6, 0x2

    if-lt v4, v6, :cond_2

    .line 178
    const/16 v35, 0x0

    .line 179
    const-string v4, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() Number of certs "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    array-length v9, v0

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    move-object/from16 v0, v28

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v34, v28, v4

    .line 182
    .local v34, "interCert":Ljava/security/cert/X509Certificate;
    move-object/from16 v0, v28

    array-length v4, v0

    add-int/lit8 v4, v4, -0x2

    aget-object v43, v28, v4

    .line 184
    const-string v4, "1.3.6.1.5.5.7.1.1"

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v7

    .line 187
    :try_start_1
    const-string v4, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation(): Generating request for:  "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v43

    move-object/from16 v3, v32

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getRequestGenerator(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/ocsp/OCSPReqGenerator;)Lorg/bouncycastle/ocsp/OCSPReqGenerator;

    move-result-object v32

    .line 189
    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/bouncycastle/ocsp/OCSPException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 190
    :catch_1
    move-exception v30

    .line 191
    .local v30, "e":Lorg/bouncycastle/ocsp/OCSPException;
    const/4 v4, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-interface {v0, v1, v4, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 192
    invoke-virtual/range {v30 .. v30}, Lorg/bouncycastle/ocsp/OCSPException;->printStackTrace()V

    goto/16 :goto_1

    .line 211
    .end local v20    # "appEnt":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v22    # "appId":Ljava/lang/String;
    .end local v28    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v30    # "e":Lorg/bouncycastle/ocsp/OCSPException;
    .end local v34    # "interCert":Ljava/security/cert/X509Certificate;
    :cond_5
    new-instance v12, Ljava/math/BigInteger;

    const/16 v4, 0x80

    new-instance v6, Ljava/util/Random;

    invoke-direct {v6}, Ljava/util/Random;-><init>()V

    invoke-direct {v12, v4, v6}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    .line 217
    .local v12, "requestNonce":Ljava/math/BigInteger;
    const-string v4, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "requestNonce= "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v39, Ljava/util/Vector;

    invoke-direct/range {v39 .. v39}, Ljava/util/Vector;-><init>()V

    .line 219
    .local v39, "oids":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;>;"
    new-instance v48, Ljava/util/Vector;

    invoke-direct/range {v48 .. v48}, Ljava/util/Vector;-><init>()V

    .line 221
    .local v48, "values":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/bouncycastle/asn1/x509/X509Extension;>;"
    sget-object v4, Lorg/bouncycastle/asn1/ocsp/OCSPObjectIdentifiers;->id_pkix_ocsp_nonce:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 222
    new-instance v4, Lorg/bouncycastle/asn1/x509/X509Extension;

    const/4 v6, 0x0

    new-instance v9, Lorg/bouncycastle/asn1/DEROctetString;

    .line 223
    invoke-virtual {v12}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/bouncycastle/asn1/DEROctetString;-><init>([B)V

    invoke-direct {v4, v6, v9}, Lorg/bouncycastle/asn1/x509/X509Extension;-><init>(ZLorg/bouncycastle/asn1/ASN1OctetString;)V

    .line 222
    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v4, Lorg/bouncycastle/asn1/x509/X509Extensions;

    move-object/from16 v0, v39

    move-object/from16 v1, v48

    invoke-direct {v4, v0, v1}, Lorg/bouncycastle/asn1/x509/X509Extensions;-><init>(Ljava/util/Vector;Ljava/util/Vector;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/bouncycastle/ocsp/OCSPReqGenerator;->setRequestExtensions(Lorg/bouncycastle/asn1/x509/X509Extensions;)V

    .line 231
    :try_start_2
    invoke-virtual/range {v32 .. v32}, Lorg/bouncycastle/ocsp/OCSPReqGenerator;->generate()Lorg/bouncycastle/ocsp/OCSPReq;

    move-result-object v8

    .local v8, "request":Lorg/bouncycastle/ocsp/OCSPReq;
    move-object/from16 v4, p0

    move-object/from16 v6, p2

    move/from16 v9, p4

    move-object/from16 v10, p3

    .line 232
    invoke-direct/range {v4 .. v10}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getResponseForRequest(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;[BLorg/bouncycastle/ocsp/OCSPReq;ZLandroid/content/Context;)Ljava/net/HttpURLConnection;

    move-result-object v29

    .line 234
    .local v29, "con":Ljava/net/HttpURLConnection;
    if-nez v29, :cond_7

    .line 235
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "Response is null for OCSP Request "

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/bouncycastle/ocsp/OCSPException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 345
    .end local v8    # "request":Lorg/bouncycastle/ocsp/OCSPReq;
    .end local v29    # "con":Ljava/net/HttpURLConnection;
    :catch_2
    move-exception v31

    .line 346
    .local v31, "e1":Lorg/bouncycastle/ocsp/OCSPException;
    const/4 v4, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-interface {v0, v1, v4, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 347
    invoke-virtual/range {v31 .. v31}, Lorg/bouncycastle/ocsp/OCSPException;->printStackTrace()V

    .line 352
    .end local v31    # "e1":Lorg/bouncycastle/ocsp/OCSPException;
    :cond_6
    :goto_2
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "-------------OCSP END---------------"

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    .restart local v8    # "request":Lorg/bouncycastle/ocsp/OCSPReq;
    .restart local v29    # "con":Ljava/net/HttpURLConnection;
    :cond_7
    :try_start_3
    invoke-virtual/range {v29 .. v29}, Ljava/net/HttpURLConnection;->getContent()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Ljava/io/InputStream;

    .line 240
    .local v33, "in":Ljava/io/InputStream;
    new-instance v38, Lorg/bouncycastle/ocsp/OCSPResp;

    move-object/from16 v0, v38

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lorg/bouncycastle/ocsp/OCSPResp;-><init>(Ljava/io/InputStream;)V

    .line 241
    .local v38, "ocspResp":Lorg/bouncycastle/ocsp/OCSPResp;
    if-eqz v33, :cond_8

    .line 242
    invoke-virtual/range {v33 .. v33}, Ljava/io/InputStream;->close()V

    .line 245
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v38

    move/from16 v3, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkResponseStatus(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Lorg/bouncycastle/ocsp/OCSPResp;Z)Z

    move-result v41

    .line 247
    .local v41, "resp":Z
    if-nez v41, :cond_9

    .line 248
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "Response Status is not good"

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/bouncycastle/ocsp/OCSPException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 348
    .end local v8    # "request":Lorg/bouncycastle/ocsp/OCSPReq;
    .end local v29    # "con":Ljava/net/HttpURLConnection;
    .end local v33    # "in":Ljava/io/InputStream;
    .end local v38    # "ocspResp":Lorg/bouncycastle/ocsp/OCSPResp;
    .end local v41    # "resp":Z
    :catch_3
    move-exception v30

    .line 349
    .local v30, "e":Ljava/io/IOException;
    const/4 v4, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move/from16 v2, p4

    invoke-interface {v0, v1, v4, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 350
    invoke-virtual/range {v30 .. v30}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 253
    .end local v30    # "e":Ljava/io/IOException;
    .restart local v8    # "request":Lorg/bouncycastle/ocsp/OCSPReq;
    .restart local v29    # "con":Ljava/net/HttpURLConnection;
    .restart local v33    # "in":Ljava/io/InputStream;
    .restart local v38    # "ocspResp":Lorg/bouncycastle/ocsp/OCSPResp;
    .restart local v41    # "resp":Z
    :cond_9
    :try_start_4
    invoke-virtual/range {v38 .. v38}, Lorg/bouncycastle/ocsp/OCSPResp;->getResponseObject()Ljava/lang/Object;

    move-result-object v13

    .line 252
    check-cast v13, Lorg/bouncycastle/ocsp/BasicOCSPResp;

    .line 254
    .local v13, "basicOcspResp":Lorg/bouncycastle/ocsp/BasicOCSPResp;
    if-nez v13, :cond_a

    .line 255
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "Response is not proper for OCSP Request "

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_a
    invoke-virtual {v13}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getResponses()[Lorg/bouncycastle/ocsp/SingleResp;

    move-result-object v47

    .line 260
    .local v47, "singleResps":[Lorg/bouncycastle/ocsp/SingleResp;
    const-string v4, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() Number of responses "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, v47

    array-length v9, v0

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 260
    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v47

    array-length v6, v0

    if-eq v4, v6, :cond_b

    .line 263
    const-string v4, "AcmsHttpRevokationHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() Number of responses and Number of request are not same"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 264
    move-object/from16 v0, v47

    array-length v9, v0

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 263
    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move-object/from16 v9, p0

    move-object v10, v5

    move-object/from16 v11, p2

    move/from16 v14, p4

    .line 268
    invoke-direct/range {v9 .. v14}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->extractExtensionValues(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Ljava/math/BigInteger;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;

    move-result-object v42

    .line 271
    .local v42, "respData":Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;
    if-eqz v42, :cond_0

    move-object/from16 v14, p0

    move-object v15, v5

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, v13

    move/from16 v19, p4

    .line 275
    invoke-direct/range {v14 .. v19}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->isOCSPCertValid(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Landroid/content/Context;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Z

    move-result v36

    .line 277
    .local v36, "isOCSPcertValid":Z
    if-eqz v36, :cond_0

    move-object/from16 v14, p0

    move-object v15, v5

    move-object/from16 v16, p2

    move-object/from16 v17, v43

    move-object/from16 v18, v13

    move/from16 v19, p4

    .line 282
    invoke-direct/range {v14 .. v19}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->checkResponseSignature(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/ocsp/BasicOCSPResp;Z)Z

    move-result v41

    .line 284
    if-nez v41, :cond_c

    .line 285
    const-string v4, "AcmsHttpRevokationHandler"

    const-string v6, "Response signature is not proper for app "

    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :cond_c
    invoke-virtual {v13}, Lorg/bouncycastle/ocsp/BasicOCSPResp;->getProducedAt()Ljava/util/Date;

    move-result-object v40

    .line 290
    .local v40, "producedAt":Ljava/util/Date;
    const-string v4, "AcmsHttpRevokationHandler"

    .line 291
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "checkRevocation() Produced at "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v40 .. v40}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 290
    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .line 295
    .local v23, "appList":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    move-object/from16 v0, v47

    array-length v6, v0

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v6, :cond_6

    aget-object v46, v47, v4

    .line 296
    .local v46, "singleResp":Lorg/bouncycastle/ocsp/SingleResp;
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 297
    invoke-virtual/range {v46 .. v46}, Lorg/bouncycastle/ocsp/SingleResp;->getCertID()Lorg/bouncycastle/ocsp/CertificateID;

    move-result-object v26

    .line 298
    .local v26, "certId":Lorg/bouncycastle/ocsp/CertificateID;
    invoke-virtual/range {v26 .. v26}, Lorg/bouncycastle/ocsp/CertificateID;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v45

    .line 299
    .local v45, "serialNumber":Ljava/math/BigInteger;
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .line 300
    .local v21, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v22

    .line 302
    .restart local v22    # "appId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p3

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getCerts(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v28

    .line 303
    .restart local v28    # "certs":[Ljava/security/cert/X509Certificate;
    if-nez v28, :cond_e

    .line 295
    .end local v21    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .end local v22    # "appId":Ljava/lang/String;
    .end local v26    # "certId":Lorg/bouncycastle/ocsp/CertificateID;
    .end local v28    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v45    # "serialNumber":Ljava/math/BigInteger;
    :cond_d
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 306
    .restart local v21    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .restart local v22    # "appId":Ljava/lang/String;
    .restart local v26    # "certId":Lorg/bouncycastle/ocsp/CertificateID;
    .restart local v28    # "certs":[Ljava/security/cert/X509Certificate;
    .restart local v45    # "serialNumber":Ljava/math/BigInteger;
    :cond_e
    const-string v9, "AcmsHttpRevokationHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "checkRevocation() Number of certs "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    array-length v11, v0

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    move-object/from16 v0, v28

    array-length v9, v0

    add-int/lit8 v9, v9, -0x1

    aget-object v34, v28, v9

    .line 310
    .restart local v34    # "interCert":Ljava/security/cert/X509Certificate;
    invoke-virtual/range {v45 .. v45}, Ljava/math/BigInteger;->intValue()I

    move-result v9

    invoke-virtual/range {v34 .. v34}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v10}, Ljava/math/BigInteger;->intValue()I

    move-result v10

    if-ne v9, v10, :cond_f

    .line 311
    const-string v9, "AcmsHttpRevokationHandler"

    const-string v10, " Serial Number Matching the cert being validated"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_f
    invoke-virtual/range {v46 .. v46}, Lorg/bouncycastle/ocsp/SingleResp;->getNextUpdate()Ljava/util/Date;

    move-result-object v37

    .line 315
    .local v37, "nextUpdate":Ljava/util/Date;
    invoke-virtual/range {v46 .. v46}, Lorg/bouncycastle/ocsp/SingleResp;->getCertStatus()Ljava/lang/Object;

    move-result-object v27

    .line 314
    check-cast v27, Lorg/bouncycastle/ocsp/CertificateStatus;

    .line 316
    .local v27, "certStatus":Lorg/bouncycastle/ocsp/CertificateStatus;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->getAppListEntries()Ljava/util/ArrayList;

    move-result-object v24

    .line 317
    .local v24, "appListEntries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    if-nez v27, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->isUpdated(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v9

    if-nez v9, :cond_10

    .line 319
    const-string v9, "AcmsHttpRevokationHandler"

    const-string v10, "Cert Status is Good "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move-object/from16 v2, v42

    move/from16 v3, p4

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onSuccess(Ljava/util/ArrayList;Lcom/samsung/android/mirrorlink/acms/utils/OcspServerResponseData;Z)V

    goto :goto_4

    .line 322
    :cond_10
    if-nez v27, :cond_11

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpRevokationHandler;->isUpdated(Ljava/util/Date;Ljava/util/Date;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 323
    invoke-static/range {p3 .. p3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->updateProtocol(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)V

    goto/16 :goto_4

    .line 326
    :cond_11
    move-object/from16 v0, v27

    instance-of v9, v0, Lorg/bouncycastle/ocsp/RevokedStatus;

    if-eqz v9, :cond_13

    .line 327
    move-object/from16 v0, v27

    check-cast v0, Lorg/bouncycastle/ocsp/RevokedStatus;

    move-object/from16 v44, v0

    .line 328
    .local v44, "rs":Lorg/bouncycastle/ocsp/RevokedStatus;
    invoke-virtual/range {v44 .. v44}, Lorg/bouncycastle/ocsp/RevokedStatus;->hasRevocationReason()Z

    move-result v9

    if-eqz v9, :cond_12

    .line 329
    const-string v9, "AcmsHttpRevokationHandler"

    .line 330
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Revocation Reason "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v44 .. v44}, Lorg/bouncycastle/ocsp/RevokedStatus;->getRevocationReason()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 329
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_12
    const-string v9, "AcmsHttpRevokationHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Revocation Date "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 333
    invoke-virtual/range {v44 .. v44}, Lorg/bouncycastle/ocsp/RevokedStatus;->getRevocationTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 332
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const/16 v9, 0xa

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, p4

    invoke-interface {v0, v1, v9, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    goto/16 :goto_4

    .line 335
    .end local v44    # "rs":Lorg/bouncycastle/ocsp/RevokedStatus;
    :cond_13
    move-object/from16 v0, v27

    instance-of v9, v0, Lorg/bouncycastle/ocsp/UnknownStatus;

    if-eqz v9, :cond_14

    .line 336
    const/16 v9, 0xb

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, p4

    invoke-interface {v0, v1, v9, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V

    .line 337
    const-string v9, "AcmsHttpRevokationHandler"

    const-string v10, "Unknown Status "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 339
    :cond_14
    const-string v9, "AcmsHttpRevokationHandler"

    const-string v10, "INVALID Status "

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const/16 v9, 0xb

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, p4

    invoke-interface {v0, v1, v9, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr$Callback;->onError(Ljava/util/ArrayList;IZ)V
    :try_end_4
    .catch Lorg/bouncycastle/ocsp/OCSPException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_4
.end method
