.class public Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
.super Ljava/lang/Object;
.source "CertificateValidator.java"


# static fields
.field private static final ISSUER_CN:Ljava/lang/String; = "CN=ACMS CA"

.field private static final LOG_TAG:Ljava/lang/String; = "AcmsCertificateValidator"

.field private static final PLATFORM_ANDROID:Ljava/lang/String; = "Android"

.field private static final RUNTIME_NATIVE:Ljava/lang/String; = "Native"


# instance fields
.field private cccRootCert:Ljava/security/cert/X509Certificate;

.field private ctsRootCert:Ljava/security/cert/X509Certificate;

.field private mContext:Landroid/content/Context;

.field private sPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    .line 58
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    .line 63
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->setRootCert()Z

    .line 65
    return-void
.end method

.method private setRootCert()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 68
    const-string v5, "AcmsCertificateValidator"

    const-string v6, "setRootCert() Getting The root cert from asset "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    if-nez v5, :cond_1

    .line 70
    const-string v5, "AcmsCertificateValidator"

    const-string v6, "setRootCert(): context passed is null "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    :goto_0
    return v4

    .line 73
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->sPackageManager:Landroid/content/pm/PackageManager;

    .line 76
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->sPackageManager:Landroid/content/pm/PackageManager;

    if-nez v5, :cond_2

    .line 77
    const-string v5, "AcmsCertificateValidator"

    const-string v6, "setRootCert(): sPackageManager is null"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 81
    .local v3, "r":Landroid/content/res/Resources;
    if-nez v3, :cond_3

    .line 82
    const-string v5, "AcmsCertificateValidator"

    const-string v6, "Resources is null. return"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_3
    invoke-virtual {v3}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 86
    .local v0, "am":Landroid/content/res/AssetManager;
    if-nez v0, :cond_4

    .line 87
    const-string v5, "AcmsCertificateValidator"

    const-string v6, "Assets Manager is null. return"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_4
    const/4 v2, 0x0

    .line 93
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CCC_ROOT_CERT_FILENAME:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 94
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v5

    .line 95
    invoke-virtual {v5, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getCertificateFromInputStream(Ljava/io/InputStream;)Ljava/security/cert/X509Certificate;

    move-result-object v5

    .line 94
    iput-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    if-eqz v2, :cond_5

    .line 116
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 124
    :cond_5
    :goto_1
    :try_start_2
    sget-object v5, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CTS_ROOT_CERT_FILENAME:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 125
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v5

    .line 126
    invoke-virtual {v5, v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getCertificateFromInputStream(Ljava/io/InputStream;)Ljava/security/cert/X509Certificate;

    move-result-object v5

    .line 125
    iput-object v5, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145
    if-eqz v2, :cond_6

    .line 147
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b

    .line 155
    :cond_6
    :goto_2
    if-eqz v3, :cond_7

    .line 156
    invoke-virtual {v3}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 157
    invoke-virtual {v3}, Landroid/content/res/Resources;->finishPreloading()V

    .line 160
    :cond_7
    const/4 v4, 0x1

    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    const-string v5, "AcmsCertificateValidator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " Unable to open the file "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 98
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CCC_ROOT_CERT_FILENAME:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 97
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 100
    if-eqz v3, :cond_8

    .line 101
    invoke-virtual {v3}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 102
    invoke-virtual {v3}, Landroid/content/res/Resources;->finishPreloading()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 114
    :cond_8
    if-eqz v2, :cond_0

    .line 116
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 117
    :catch_1
    move-exception v1

    .line 118
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 105
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 106
    .local v1, "e":Ljava/security/cert/CertificateException;
    :try_start_6
    const-string v5, "AcmsCertificateValidator"

    const-string v6, " CCC Root Cert is expired "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    .line 108
    if-eqz v3, :cond_9

    .line 109
    invoke-virtual {v3}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 110
    invoke-virtual {v3}, Landroid/content/res/Resources;->finishPreloading()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 114
    :cond_9
    if-eqz v2, :cond_0

    .line 116
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_0

    .line 117
    :catch_3
    move-exception v1

    .line 118
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 113
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 114
    if-eqz v2, :cond_a

    .line 116
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 121
    :cond_a
    :goto_3
    throw v4

    .line 117
    :catch_4
    move-exception v1

    .line 118
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 117
    .end local v1    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 118
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 127
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 128
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_9
    const-string v5, "AcmsCertificateValidator"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " Unable to open the file "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 129
    sget-object v7, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->CTS_ROOT_CERT_FILENAME:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 128
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 131
    if-eqz v3, :cond_b

    .line 132
    invoke-virtual {v3}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 133
    invoke-virtual {v3}, Landroid/content/res/Resources;->finishPreloading()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 145
    :cond_b
    if-eqz v2, :cond_0

    .line 147
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    goto/16 :goto_0

    .line 148
    :catch_7
    move-exception v1

    .line 149
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 136
    .end local v1    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v1

    .line 137
    .local v1, "e":Ljava/security/cert/CertificateException;
    :try_start_b
    const-string v5, "AcmsCertificateValidator"

    const-string v6, " CTS Root Cert is expired "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    .line 139
    if-eqz v3, :cond_c

    .line 140
    invoke-virtual {v3}, Landroid/content/res/Resources;->flushLayoutCache()V

    .line 141
    invoke-virtual {v3}, Landroid/content/res/Resources;->finishPreloading()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 145
    :cond_c
    if-eqz v2, :cond_0

    .line 147
    :try_start_c
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    goto/16 :goto_0

    .line 148
    :catch_9
    move-exception v1

    .line 149
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 144
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v4

    .line 145
    if-eqz v2, :cond_d

    .line 147
    :try_start_d
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 152
    :cond_d
    :goto_4
    throw v4

    .line 148
    :catch_a
    move-exception v1

    .line 149
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 148
    .end local v1    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v1

    .line 149
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2
.end method


# virtual methods
.method public validateCert(Ljava/security/cert/X509Certificate;)Z
    .locals 5
    .param p1, "c"    # Ljava/security/cert/X509Certificate;

    .prologue
    const/4 v1, 0x0

    .line 166
    if-nez p1, :cond_0

    .line 167
    const-string v2, "AcmsCertificateValidator"

    const-string v3, "Input Certificate is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :goto_0
    return v1

    .line 170
    :cond_0
    const-string v2, "AcmsCertificateValidator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "validateCert() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :try_start_0
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->checkValidity()V
    :try_end_0
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_1

    .line 184
    const/4 v1, 0x1

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/security/cert/CertificateExpiredException;
    const-string v2, "AcmsCertificateValidator"

    const-string v3, "Self-signed certificate is expired."

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v0}, Ljava/security/cert/CertificateExpiredException;->printStackTrace()V

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/security/cert/CertificateExpiredException;
    :catch_1
    move-exception v0

    .line 178
    .local v0, "e":Ljava/security/cert/CertificateNotYetValidException;
    const-string v2, "AcmsCertificateValidator"

    .line 179
    const-string v3, "Self-signed certificate\'s issue date is in future Hence its invalid."

    .line 178
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0}, Ljava/security/cert/CertificateNotYetValidException;->printStackTrace()V

    goto :goto_0
.end method

.method public validateCertChain([Ljava/security/cert/X509Certificate;Ljava/lang/String;)Z
    .locals 22
    .param p1, "certs"    # [Ljava/security/cert/X509Certificate;
    .param p2, "appInfoXmlString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/SignatureException;,
            Landroid/content/pm/PackageManager$NameNotFoundException;,
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 193
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "validateCertChain() Enter "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    sget-object v8, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 196
    .local v8, "build":Ljava/lang/String;
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 198
    .local v16, "sdkBuildVersion":I
    if-nez p2, :cond_0

    .line 199
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "AppInfo xml is null; Hence return"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/16 v17, 0x0

    .line 308
    :goto_0
    return v17

    .line 203
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDataFromAppInfo(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/utils/AppData;

    move-result-object v3

    .line 205
    .local v3, "appData":Lcom/samsung/android/mirrorlink/acms/utils/AppData;
    const-string v17, "Android"

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getPlatformId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 206
    const-string v17, "AcmsCertificateValidator"

    .line 207
    const-string v18, "validateCert() Fails. The application is not for Android Platform"

    .line 206
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/16 v17, 0x0

    goto :goto_0

    .line 210
    :cond_1
    const-string v17, "Native"

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getRunTimeId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 211
    const-string v17, "AcmsCertificateValidator"

    .line 212
    const-string v18, "validateCert() Fails. The application runtime Id is not Native"

    .line 211
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/16 v17, 0x0

    goto :goto_0

    .line 216
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getBlackListedPlatform()Ljava/lang/String;

    move-result-object v17

    if-nez v17, :cond_3

    .line 218
    const-string v17, "AcmsCertificateValidator"

    .line 219
    const-string v18, "validateCert() Fails. blacklistedplatform tag is not present in the xml"

    .line 217
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/16 v17, 0x0

    goto :goto_0

    .line 222
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->getBlackListedPlatform()Ljava/lang/String;

    move-result-object v6

    .line 223
    .local v6, "blckLstdPltfrmsList":Ljava/lang/String;
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "blckLstdPltfrms":[Ljava/lang/String;
    array-length v0, v5

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_4

    .line 246
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v17, p1, v17

    .line 247
    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v2

    .line 248
    .local v2, "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    if-nez v2, :cond_6

    .line 249
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Issuer CN is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const/16 v17, 0x0

    goto :goto_0

    .line 224
    .end local v2    # "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    :cond_4
    aget-object v7, v5, v17

    .line 227
    .local v7, "blckLstePltfm":Ljava/lang/String;
    :try_start_0
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 228
    .local v4, "blckListPlatform":I
    move/from16 v0, v16

    if-ne v0, v4, :cond_5

    .line 230
    const-string v19, "AcmsCertificateValidator"

    .line 231
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "validateCert() Fails. The device platform version is a blacklisted version for this application"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 229
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 234
    .end local v4    # "blckListPlatform":I
    :catch_0
    move-exception v12

    .line 235
    .local v12, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 237
    const-string v17, "AcmsCertificateValidator"

    .line 238
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "validateCert() Fails. The device platform version is a blacklisted version for this application"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 236
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 224
    .end local v12    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 252
    .end local v7    # "blckLstePltfm":Ljava/lang/String;
    .restart local v2    # "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    :cond_6
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Issuer CN: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "CN=ACMS CA"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 254
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Issuer CN: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 255
    const-string v19, " does not contain ACMS CA. Hence returning false"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 254
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 259
    :cond_7
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_8

    .line 260
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Only One Certificate Received"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    new-instance v17, Ljava/lang/IllegalArgumentException;

    invoke-direct/range {v17 .. v17}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v17

    .line 264
    :cond_8
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->printCerts([Ljava/security/cert/X509Certificate;)V

    .line 266
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_2
    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_b

    .line 269
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_9

    .line 270
    const-string v17, "AcmsCertificateValidator"

    .line 271
    const-string v18, "validateCert() Entered to verify the cert chain"

    .line 270
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v14, v17, -0x1

    .local v14, "i":I
    :goto_3
    if-gtz v14, :cond_c

    .line 280
    .end local v14    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    if-nez v17, :cond_d

    .line 281
    :cond_a
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "root Cert is null: Might be Manupulated"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 266
    :cond_b
    aget-object v10, p1, v17

    .line 267
    .local v10, "cert":Ljava/security/cert/X509Certificate;
    invoke-virtual {v10}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 266
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 274
    .end local v10    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v14    # "i":I
    :cond_c
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "validateCert() Checking validity "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    add-int/lit8 v17, v14, -0x1

    aget-object v15, p1, v17

    .line 276
    .local v15, "issuer":Ljava/security/cert/X509Certificate;
    aget-object v17, p1, v14

    invoke-virtual {v15}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 273
    add-int/lit8 v14, v14, -0x1

    goto :goto_3

    .line 288
    .end local v14    # "i":I
    .end local v15    # "issuer":Ljava/security/cert/X509Certificate;
    :cond_d
    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v13

    .line 289
    .local v13, "endcertPrincipal":Ljavax/security/auth/x500/X500Principal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v9

    .line 290
    .local v9, "ccccertPrincipal":Ljavax/security/auth/x500/X500Principal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v11

    .line 292
    .local v11, "ctscertPrincipal":Ljavax/security/auth/x500/X500Principal;
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Verifying Root Cert"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v13, v11}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 294
    const-string v17, "AcmsCertificateValidator"

    .line 295
    const-string v18, "Root is CTS: verifying last certificate and root certificate"

    .line 294
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/16 v17, 0x0

    aget-object v17, p1, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 298
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Root Cert Verification Success"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 300
    :cond_e
    invoke-virtual {v13, v9}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 301
    const-string v17, "AcmsCertificateValidator"

    .line 302
    const-string v18, "Root is CCC: verifying last certificate and root certificate"

    .line 301
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/16 v17, 0x0

    aget-object v17, p1, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 305
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Root Cert Verification Success"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 308
    :cond_f
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method public validateDevIdCertChain([Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    .locals 20
    .param p1, "certs"    # [Ljava/security/cert/X509Certificate;
    .param p2, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/cert/CertificateExpiredException;,
            Ljava/security/cert/CertificateNotYetValidException;,
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/SignatureException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 370
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "validateDevIdCertChain() "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_0
    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_2

    .line 375
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_0

    .line 376
    const-string v17, "AcmsCertificateValidator"

    .line 377
    const-string v18, "validateDevIdCertChain() Entered to verify the cert chain"

    .line 376
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v11, v17, -0x1

    .local v11, "i":I
    :goto_1
    if-gtz v11, :cond_3

    .line 389
    .end local v11    # "i":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    if-nez v17, :cond_4

    .line 390
    :cond_1
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "root Cert is null: Might be Manupulated"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/16 v17, 0x0

    .line 478
    :goto_2
    return v17

    .line 372
    :cond_2
    aget-object v5, p1, v17

    .line 373
    .local v5, "cert":Ljava/security/cert/X509Certificate;
    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 372
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 380
    .end local v5    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v11    # "i":I
    :cond_3
    const-string v17, "AcmsCertificateValidator"

    .line 381
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "validateDevIdCertChain() Checking validity "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 380
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    add-int/lit8 v17, v11, -0x1

    aget-object v12, p1, v17

    .line 383
    .local v12, "issuer":Ljava/security/cert/X509Certificate;
    aget-object v17, p1, v11

    invoke-virtual {v12}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 379
    add-int/lit8 v11, v11, -0x1

    goto :goto_1

    .line 394
    .end local v11    # "i":I
    .end local v12    # "issuer":Ljava/security/cert/X509Certificate;
    :cond_4
    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v10

    .line 395
    .local v10, "endcertPrincipal":Ljavax/security/auth/x500/X500Principal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v4

    .line 396
    .local v4, "ccccertPrincipal":Ljavax/security/auth/x500/X500Principal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v6

    .line 398
    .local v6, "ctscertPrincipal":Ljavax/security/auth/x500/X500Principal;
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Verifying Root Cert"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-virtual {v10, v6}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 400
    const-string v17, "AcmsCertificateValidator"

    .line 401
    const-string v18, "Root is CTS: verifying last certificate and root certificate"

    .line 400
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const/16 v17, 0x0

    aget-object v17, p1, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 404
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Root Cert Verification Success"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :goto_3
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v17, p1, v17

    .line 418
    invoke-virtual/range {v17 .. v17}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v2

    .line 419
    .local v2, "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    if-nez v2, :cond_7

    .line 420
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Issuer CN is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 406
    .end local v2    # "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    :cond_5
    invoke-virtual {v10, v4}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 407
    const-string v17, "AcmsCertificateValidator"

    .line 408
    const-string v18, "Root is CCC: verifying last certificate and root certificate"

    .line 407
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const/16 v17, 0x0

    aget-object v17, p1, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 411
    const-string v17, "AcmsCertificateValidator"

    const-string v18, "Root Cert Verification Success"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 413
    :cond_6
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 423
    .restart local v2    # "appCertPrincipal":Ljavax/security/auth/x500/X500Principal;
    :cond_7
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Issuer CN: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "CN=ACMS CA"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_8

    .line 425
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Issuer CN: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 426
    const-string v19, " does not contain ACMS CA. Hence returning false"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 425
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 432
    :cond_8
    :try_start_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v17, p1, v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getServerIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v14

    .line 433
    .local v14, "sId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 434
    const-string v18, "phone"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    .line 433
    check-cast v13, Landroid/telephony/TelephonyManager;

    .line 435
    .local v13, "mngr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v16

    .line 437
    .local v16, "serverId":Ljava/lang/String;
    if-eqz v16, :cond_9

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_a

    .line 438
    :cond_9
    const-string v17, "AcmsCertificateValidator"

    .line 439
    const-string v18, "validateDevIdCertChain(): Device Id of the current Device is null"

    .line 438
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 442
    :cond_a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x4

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 444
    if-eqz v14, :cond_b

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_c

    .line 446
    :cond_b
    const-string v17, "AcmsCertificateValidator"

    .line 447
    const-string v18, "validateDevIdCertChain(): Device Server Id is not in the Server Id list of DevId Cert "

    .line 445
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 450
    :cond_c
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v3

    .line 452
    .local v3, "appId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getFromKeyStore(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v5

    .line 453
    .local v5, "cert":[Ljava/security/cert/X509Certificate;
    if-nez v5, :cond_d

    .line 454
    const-string v17, "AcmsCertificateValidator"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "No Entry present in keystore for the appId: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 457
    :cond_d
    const/16 v17, 0x0

    aget-object v15, v5, v17

    .line 459
    .local v15, "selSigned":Ljava/security/cert/X509Certificate;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v8

    .line 461
    .local v8, "devloperId":Ljava/lang/String;
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v17, p1, v17

    .line 460
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/mirrorlink/acms/utils/AppInfoParser;->getDevIdFromCert(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v7

    .line 462
    .local v7, "devId":Ljava/lang/String;
    if-eqz v7, :cond_e

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_f

    .line 464
    :cond_e
    const-string v17, "AcmsCertificateValidator"

    .line 465
    const-string v18, "validateDevIdCertChain(): DevId from Self-signed Cert and from DevId Cert are not matching "

    .line 463
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 466
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 469
    .end local v3    # "appId":Ljava/lang/String;
    .end local v5    # "cert":[Ljava/security/cert/X509Certificate;
    .end local v7    # "devId":Ljava/lang/String;
    .end local v8    # "devloperId":Ljava/lang/String;
    .end local v13    # "mngr":Landroid/telephony/TelephonyManager;
    .end local v14    # "sId":Ljava/lang/String;
    .end local v15    # "selSigned":Ljava/security/cert/X509Certificate;
    .end local v16    # "serverId":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 470
    .local v9, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v9}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 471
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 472
    .end local v9    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v9

    .line 473
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 474
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 478
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v3    # "appId":Ljava/lang/String;
    .restart local v5    # "cert":[Ljava/security/cert/X509Certificate;
    .restart local v7    # "devId":Ljava/lang/String;
    .restart local v8    # "devloperId":Ljava/lang/String;
    .restart local v13    # "mngr":Landroid/telephony/TelephonyManager;
    .restart local v14    # "sId":Ljava/lang/String;
    .restart local v15    # "selSigned":Ljava/security/cert/X509Certificate;
    .restart local v16    # "serverId":Ljava/lang/String;
    :cond_f
    const/16 v17, 0x1

    goto/16 :goto_2
.end method

.method public verifyOCSPChain([Ljava/security/cert/X509Certificate;)Z
    .locals 11
    .param p1, "certs"    # [Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 316
    const-string v8, "AcmsCertificateValidator"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "verifyOCSPChain() "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, p1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-static {p1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->printCerts([Ljava/security/cert/X509Certificate;)V

    .line 320
    array-length v9, p1

    move v8, v7

    :goto_0
    if-lt v8, v9, :cond_2

    .line 323
    array-length v8, p1

    const/4 v9, 0x2

    if-lt v8, v9, :cond_0

    .line 324
    const-string v8, "AcmsCertificateValidator"

    .line 325
    const-string v9, "verifyOCSPChain() Entered to verify the cert chain"

    .line 324
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    array-length v8, p1

    add-int/lit8 v4, v8, -0x1

    .local v4, "i":I
    :goto_1
    if-gtz v4, :cond_3

    .line 333
    .end local v4    # "i":I
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    if-nez v8, :cond_4

    .line 334
    :cond_1
    const-string v6, "AcmsCertificateValidator"

    const-string v8, "root Cert is null: Might be Manupulated"

    invoke-static {v6, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 361
    :goto_2
    return v6

    .line 320
    :cond_2
    aget-object v1, p1, v8

    .line 321
    .local v1, "cert":Ljava/security/cert/X509Certificate;
    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 320
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 328
    .end local v1    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v4    # "i":I
    :cond_3
    const-string v8, "AcmsCertificateValidator"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "verifyOCSPChain() Checking validity "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    add-int/lit8 v8, v4, -0x1

    aget-object v5, p1, v8

    .line 330
    .local v5, "issuer":Ljava/security/cert/X509Certificate;
    aget-object v8, p1, v4

    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 327
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 341
    .end local v4    # "i":I
    .end local v5    # "issuer":Ljava/security/cert/X509Certificate;
    :cond_4
    aget-object v8, p1, v7

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v3

    .line 342
    .local v3, "endcertPrincipal":Ljavax/security/auth/x500/X500Principal;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    .line 343
    .local v0, "ccccertPrincipal":Ljavax/security/auth/x500/X500Principal;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v2

    .line 345
    .local v2, "ctscertPrincipal":Ljavax/security/auth/x500/X500Principal;
    const-string v8, "AcmsCertificateValidator"

    const-string v9, "Verifying Root Cert"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-virtual {v3, v2}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 347
    const-string v8, "AcmsCertificateValidator"

    .line 348
    const-string v9, "Root is CTS: verifying last certificate and root certificate"

    .line 347
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    aget-object v7, p1, v7

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 350
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->ctsRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 351
    const-string v7, "AcmsCertificateValidator"

    const-string v8, "Root Cert Verification Success"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 353
    :cond_5
    invoke-virtual {v3, v0}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 354
    const-string v8, "AcmsCertificateValidator"

    .line 355
    const-string v9, "Root is CCC: verifying last certificate and root certificate"

    .line 354
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    aget-object v7, p1, v7

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 357
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->cccRootCert:Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V

    .line 358
    const-string v7, "AcmsCertificateValidator"

    const-string v8, "Root Cert Verification Success"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    move v6, v7

    .line 361
    goto/16 :goto_2
.end method
