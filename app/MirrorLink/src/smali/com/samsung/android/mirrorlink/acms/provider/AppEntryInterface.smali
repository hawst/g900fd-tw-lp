.class public Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
.super Ljava/lang/Object;
.source "AppEntryInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;,
        Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    }
.end annotation


# static fields
.field public static final CONTENT_CATEGORY:Ljava/lang/String; = "contentCategory"

.field public static final RESTRICTED_MODE:Ljava/lang/String; = "restricted"

.field private static final TAG:Ljava/lang/String; = "AcmsAppEntryInterface"

.field private static sAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    .line 45
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 46
    const-string v3, "com.samsung.android.app.mirrorlink"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 47
    const-string v3, "com.samsung.android.drivelink.stub"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 48
    const-string v3, "com.sec.android.inputmethod"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 49
    const-string v3, "com.sec.android.inputmethod.iwnnime.japan/.standardcommon.IWnnLanguageSwitcher"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 50
    const-string v3, "com.samsung.inputmethod/.SamsungIME"

    aput-object v3, v1, v2

    .line 52
    .local v1, "pkgName":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 57
    return-void

    .line 53
    :cond_0
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->checkIfPkgNameExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 54
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->insertToPkgNameContent(Ljava/lang/String;)V

    .line 52
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkIfPkgNameExists(Ljava/lang/String;)Z
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 68
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->CONTENT_URI_PKGNAMES:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "pkgname"

    aput-object v3, v2, v5

    .line 70
    const-string v3, "pkgname=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    .line 71
    const/4 v5, 0x0

    .line 68
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 72
    .local v6, "cursor2":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 73
    .local v7, "ret":Z
    if-nez v6, :cond_0

    move v8, v7

    .line 94
    .end local v7    # "ret":Z
    .local v8, "ret":I
    :goto_0
    return v8

    .line 77
    .end local v8    # "ret":I
    .restart local v7    # "ret":Z
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 78
    const/4 v7, 0x1

    .line 79
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 93
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v8, v7

    .line 94
    .restart local v8    # "ret":I
    goto :goto_0

    .line 91
    .end local v8    # "ret":I
    :cond_1
    const-string v0, "AcmsAppEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " now updating certified pkgname value:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .locals 1

    .prologue
    .line 269
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    return-object v0
.end method

.method public static declared-synchronized getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const-class v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->sAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->sAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 63
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->sAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getEndTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "</"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStartTag(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1115
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private insertToPkgNameContent(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 99
    if-nez p1, :cond_0

    .line 100
    const-string v1, "AcmsAppEntryInterface"

    const-string v2, "ERROR: trying to insert NULL pkgName in database"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_0
    return-void

    .line 103
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 104
    .local v0, "pkgvalues":Landroid/content/ContentValues;
    const-string v1, "AcmsAppEntryInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "inserting certified pkgname value:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v1, "pkgname"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 108
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->CONTENT_URI_PKGNAMES:Landroid/net/Uri;

    .line 107
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public cleanup()V
    .locals 2

    .prologue
    .line 1122
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "AppEntryInterface: Inside CleanUp"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->sAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    .line 1124
    return-void
.end method

.method public deleteCertificate(Ljava/lang/String;)Z
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    if-nez p1, :cond_0

    .line 168
    const-string v4, "AcmsAppEntryInterface"

    const-string v5, "App not found in DB Hence ignore"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :goto_0
    return v3

    .line 171
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->getAcmsCertificateMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    move-result-object v5

    .line 172
    invoke-virtual {v5, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->deleteFromKeyStore(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 173
    const-string v5, "AcmsAppEntryInterface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " entry deleted from keystore "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "pkgname":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 180
    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "appId=?"

    .line 181
    new-array v8, v4, [Ljava/lang/String;

    aput-object p1, v8, v3

    .line 179
    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 183
    .local v2, "rows":I
    const-string v5, "AcmsAppEntryInterface"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "delete pkgname:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 185
    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AcmsPkgNameProvide;->CONTENT_URI_PKGNAMES:Landroid/net/Uri;

    const-string v7, "pkgname=?"

    .line 186
    new-array v8, v4, [Ljava/lang/String;

    aput-object v0, v8, v3

    .line 184
    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 188
    .local v1, "pkgrows":I
    if-nez v1, :cond_1

    .line 189
    const-string v5, "AcmsAppEntryInterface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " is not found in packagenamedb"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_1
    if-nez v2, :cond_3

    .line 193
    const-string v4, "AcmsAppEntryInterface"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " appId Not found in db"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 175
    .end local v0    # "pkgname":Ljava/lang/String;
    .end local v1    # "pkgrows":I
    .end local v2    # "rows":I
    :cond_2
    const-string v5, "AcmsAppEntryInterface"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " entry not found in keystore "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .restart local v0    # "pkgname":Ljava/lang/String;
    .restart local v1    # "pkgrows":I
    .restart local v2    # "rows":I
    :cond_3
    move v3, v4

    .line 197
    goto/16 :goto_0
.end method

.method public getAllApps()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 651
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 652
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllApps: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    :goto_0
    return-object v3

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 656
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    .line 655
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 658
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 659
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllApps: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 662
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 663
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllApps: Cursor.getCount is zero"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 667
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 668
    .local v6, "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 671
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v7

    .line 673
    .local v7, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 672
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 675
    const-string v0, "certInfo"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 674
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 677
    const-string v0, "certState"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 676
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 679
    const-string v0, "certEntities"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 678
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 681
    const-string v0, "packagename"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 680
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 683
    const-string v0, "isPending"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 682
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v9

    :goto_1
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 685
    const-string v0, "queryPeriod"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 684
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 687
    const-string v0, "gracePeriod"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 686
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 688
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 689
    const-string v1, "certType"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 688
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 691
    const-string v0, "maxRetry"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 690
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 693
    const-string v0, "isCertified"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 692
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v9

    :goto_2
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 696
    const-string v0, "isRevoked"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 695
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v9

    :goto_3
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 698
    const-string v0, "isMemberApp"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 697
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    :goto_4
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 701
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 703
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 704
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 705
    goto/16 :goto_0

    :cond_4
    move v0, v10

    .line 683
    goto/16 :goto_1

    :cond_5
    move v0, v10

    .line 693
    goto :goto_2

    :cond_6
    move v0, v10

    .line 696
    goto :goto_3

    :cond_7
    move v0, v10

    .line 699
    goto :goto_4
.end method

.method public getAllCertifiedApps()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 592
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 593
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllCertifiedApps: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :goto_0
    return-object v5

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 597
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 598
    const-string v3, "isCertified=?"

    new-array v4, v10, [Ljava/lang/String;

    const-string v11, "1"

    aput-object v11, v4, v9

    .line 596
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 599
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 600
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllCertifiedApps: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 603
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 604
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllCertifiedApps: Cursor.getCount is zero"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 608
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 609
    .local v6, "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 612
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v7

    .line 614
    .local v7, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 613
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 616
    const-string v0, "certInfo"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 615
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 618
    const-string v0, "certState"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 617
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 620
    const-string v0, "certEntities"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 619
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 622
    const-string v0, "packagename"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 621
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 624
    const-string v0, "isPending"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 623
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v9

    :goto_1
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 626
    const-string v0, "queryPeriod"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 625
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 628
    const-string v0, "gracePeriod"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 627
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 629
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 630
    const-string v1, "certType"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 629
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 632
    const-string v0, "maxRetry"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 631
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 634
    const-string v0, "isCertified"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 633
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v9

    :goto_2
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 637
    const-string v0, "isRevoked"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 636
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v9

    :goto_3
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 639
    const-string v0, "isMemberApp"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 638
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    :goto_4
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 642
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 645
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v5, v6

    .line 646
    goto/16 :goto_0

    :cond_4
    move v0, v10

    .line 624
    goto/16 :goto_1

    :cond_5
    move v0, v10

    .line 634
    goto :goto_2

    :cond_6
    move v0, v10

    .line 637
    goto :goto_3

    :cond_7
    move v0, v10

    .line 640
    goto :goto_4
.end method

.method public getAllUncheckedApps()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 710
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 711
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllUncheckedApps: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :goto_0
    return-object v3

    .line 714
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 715
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    .line 714
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 717
    .local v9, "cursor":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 718
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllUncheckedApps: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 721
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 722
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAllUncheckedApps: Cursor.getCount is zero"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 726
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 727
    .local v6, "appEntries":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 730
    :cond_3
    const-string v0, "certState"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 729
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 731
    .local v8, "certState":I
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v0

    if-lt v8, v0, :cond_4

    .line 732
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 733
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v0

    .line 732
    if-ne v8, v0, :cond_5

    .line 734
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v7

    .line 736
    .local v7, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 735
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 738
    const-string v0, "certInfo"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 737
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 740
    const-string v0, "certState"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 739
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 742
    const-string v0, "certEntities"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 741
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 744
    const-string v0, "packagename"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 743
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 746
    const-string v0, "isPending"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 745
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v10

    :goto_1
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 749
    const-string v0, "queryPeriod"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 748
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 751
    const-string v0, "gracePeriod"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 750
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 752
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 753
    const-string v1, "certType"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 752
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 755
    const-string v0, "maxRetry"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 754
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 757
    const-string v0, "isCertified"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 756
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v10

    :goto_2
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 761
    const-string v0, "isRevoked"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 760
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v10

    :goto_3
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 764
    const-string v0, "isMemberApp"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 763
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v10

    :goto_4
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 767
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 770
    .end local v7    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 771
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 772
    goto/16 :goto_0

    .restart local v7    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_6
    move v0, v11

    .line 747
    goto/16 :goto_1

    :cond_7
    move v0, v11

    .line 758
    goto :goto_2

    :cond_8
    move v0, v11

    .line 762
    goto :goto_3

    :cond_9
    move v0, v11

    .line 765
    goto :goto_4
.end method

.method public getAppEntryFromPackage(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 489
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 490
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAppEntryFromPackage: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :goto_0
    return-object v5

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 494
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 495
    const-string v3, "packagename=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 493
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 497
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 498
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAppEntryFromPackage: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 501
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_6

    .line 502
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 503
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v7

    .line 505
    .local v7, "entry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 504
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 507
    const-string v0, "certInfo"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 506
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 509
    const-string v0, "certState"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 508
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 511
    const-string v0, "certEntities"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 510
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 513
    const-string v0, "packagename"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 512
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 515
    const-string v0, "isPending"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 514
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 517
    const-string v0, "queryPeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 516
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 519
    const-string v0, "gracePeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 518
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 520
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 521
    const-string v1, "certType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 520
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 523
    const-string v0, "maxRetry"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 522
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 525
    const-string v0, "isCertified"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 524
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v8

    :goto_2
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 528
    const-string v0, "isRevoked"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 527
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v8

    :goto_3
    invoke-virtual {v7, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 530
    const-string v0, "isMemberApp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 529
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    :goto_4
    invoke-virtual {v7, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 533
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v7

    .line 534
    goto/16 :goto_0

    :cond_2
    move v0, v9

    .line 515
    goto :goto_1

    :cond_3
    move v0, v9

    .line 525
    goto :goto_2

    :cond_4
    move v0, v9

    .line 528
    goto :goto_3

    :cond_5
    move v8, v9

    .line 531
    goto :goto_4

    .line 536
    .end local v7    # "entry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .locals 10
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 541
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 542
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAppEntryfromAppId: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :goto_0
    return-object v5

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 546
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 547
    const-string v3, "appId=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 545
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 548
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 549
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAppEntryfromAppId: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 552
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_6

    .line 553
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 554
    new-instance v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-direct {v6}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    .line 556
    .local v6, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 555
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 558
    const-string v0, "certInfo"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 557
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 560
    const-string v0, "certState"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 559
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 562
    const-string v0, "certEntities"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 561
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 564
    const-string v0, "packagename"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 563
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 566
    const-string v0, "isPending"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 565
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 568
    const-string v0, "queryPeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 567
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 570
    const-string v0, "gracePeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 569
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 571
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 572
    const-string v1, "certType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 571
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 574
    const-string v0, "maxRetry"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 573
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 576
    const-string v0, "isCertified"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 575
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v8

    :goto_2
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 579
    const-string v0, "isRevoked"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 578
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v8

    :goto_3
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 581
    const-string v0, "isMemberApp"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 580
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    :goto_4
    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 584
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v6

    .line 585
    goto/16 :goto_0

    :cond_2
    move v0, v9

    .line 566
    goto :goto_1

    :cond_3
    move v0, v9

    .line 576
    goto :goto_2

    :cond_4
    move v0, v9

    .line 579
    goto :goto_3

    :cond_5
    move v8, v9

    .line 582
    goto :goto_4

    .line 587
    .end local v6    # "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getAppId(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1023
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1024
    :cond_0
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "Package or context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    :goto_0
    return-object v5

    .line 1027
    :cond_1
    const/4 v6, 0x0

    .line 1028
    .local v6, "appId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1029
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "appId"

    aput-object v3, v2, v8

    .line 1030
    const-string v3, "packagename=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 1028
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1031
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 1032
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getAppId: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1035
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 1036
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1037
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1039
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v6

    .line 1040
    goto :goto_0
.end method

.method public getCertificateInfoForAppId(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1005
    const/4 v6, 0x0

    .line 1006
    .local v6, "certInfo":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1007
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 1008
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "certInfo"

    aput-object v3, v2, v8

    .line 1009
    const-string v3, "appId=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 1006
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1010
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_0

    .line 1019
    :goto_0
    return-object v5

    .line 1013
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1014
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1016
    const-string v0, "certInfo"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1015
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1018
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v6

    .line 1019
    goto :goto_0
.end method

.method public getExpiredApps(J)Ljava/util/List;
    .locals 12
    .param p1, "time"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 425
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 426
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getExpiredApps: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :goto_0
    return-object v5

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 431
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 432
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 433
    const-string v3, "queryPeriod<?  OR gracePeriod<?"

    .line 435
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v9

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v10

    .line 430
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 437
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 438
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getExpiredApps: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 442
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getExpiredApps: cursor size is 0"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 446
    :cond_2
    const-string v0, "AcmsAppEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No of Expired apps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 448
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 451
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v6

    .line 453
    .local v6, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 452
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 455
    const-string v0, "certInfo"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 454
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 457
    const-string v0, "certState"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 456
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 459
    const-string v0, "certEntities"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 458
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 461
    const-string v0, "packagename"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 460
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 463
    const-string v0, "isPending"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 462
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v9

    :goto_1
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 465
    const-string v0, "queryPeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 464
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 467
    const-string v0, "gracePeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 466
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 468
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 469
    const-string v1, "certType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 468
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 471
    const-string v0, "maxRetry"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 470
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 473
    const-string v0, "isCertified"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 472
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v9

    :goto_2
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 476
    const-string v0, "isRevoked"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 475
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v9

    :goto_3
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 478
    const-string v0, "isMemberApp"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 477
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    :goto_4
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 481
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 484
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v8

    .line 485
    goto/16 :goto_0

    :cond_4
    move v0, v10

    .line 463
    goto/16 :goto_1

    :cond_5
    move v0, v10

    .line 473
    goto :goto_2

    :cond_6
    move v0, v10

    .line 476
    goto :goto_3

    :cond_7
    move v0, v10

    .line 479
    goto :goto_4
.end method

.method public getMaxRetry(Ljava/lang/String;)I
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 897
    const/4 v7, 0x0

    .line 898
    .local v7, "maxRetry":I
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 899
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getMaxRetry: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 915
    :goto_0
    return v0

    .line 902
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 903
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 904
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "maxRetry"

    aput-object v3, v2, v8

    const-string v3, "appId=?"

    .line 905
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    const/4 v5, 0x0

    .line 902
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 907
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    move v0, v8

    .line 908
    goto :goto_0

    .line 910
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 911
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 912
    const-string v0, "maxRetry"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 914
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 915
    goto :goto_0
.end method

.method public getNextGracePeriod()J
    .locals 12

    .prologue
    const-wide/16 v9, 0x0

    const/4 v3, 0x0

    const/4 v11, 0x1

    .line 824
    const-wide/16 v7, 0x0

    .line 825
    .local v7, "gracePeriod":J
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 826
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getNextGracePeriod: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v9

    .line 844
    :goto_0
    return-wide v0

    .line 829
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 830
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 831
    new-array v2, v11, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "gracePeriod"

    aput-object v5, v2, v4

    .line 832
    const-string v5, "gracePeriod ASC LIMIT 1"

    move-object v4, v3

    .line 829
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 833
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 834
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "Error occured in query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v9

    .line 835
    goto :goto_0

    .line 836
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v11, :cond_2

    .line 837
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "No results for query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v0, v7

    .line 844
    goto :goto_0

    .line 839
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 841
    const-string v0, "gracePeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 840
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    goto :goto_1
.end method

.method public getNextGracePeriodAppId()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 848
    const/4 v6, 0x0

    .line 849
    .local v6, "appId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 850
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getNextGracePeriodAppId: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :goto_0
    return-object v3

    .line 853
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 854
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 855
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "appId"

    aput-object v5, v2, v4

    const-string v4, "gracePeriod"

    aput-object v4, v2, v8

    .line 856
    const-string v5, "gracePeriod ASC LIMIT 1"

    move-object v4, v3

    .line 853
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 857
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 858
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "Error occured in query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 860
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v8, :cond_2

    .line 861
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "No results for query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 867
    goto :goto_0

    .line 863
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 864
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public getNextQueryPeriod()J
    .locals 12

    .prologue
    const-wide/16 v9, 0x0

    const/4 v3, 0x0

    const/4 v11, 0x1

    .line 777
    const-wide/16 v7, 0x0

    .line 778
    .local v7, "queryPeriod":J
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 779
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getNextQueryPeriod: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v9

    .line 797
    :goto_0
    return-wide v0

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 783
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 784
    new-array v2, v11, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "queryPeriod"

    aput-object v5, v2, v4

    .line 785
    const-string v5, "queryPeriod ASC LIMIT 1"

    move-object v4, v3

    .line 782
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 786
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 787
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "Error occured in query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v9

    .line 788
    goto :goto_0

    .line 789
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v11, :cond_2

    .line 790
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "No results for query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v0, v7

    .line 797
    goto :goto_0

    .line 792
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 794
    const-string v0, "queryPeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 793
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    goto :goto_1
.end method

.method public getNextQueryPeriodAppId()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 801
    const/4 v6, 0x0

    .line 802
    .local v6, "appId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 803
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getNextQueryPeriodAppId: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    :goto_0
    return-object v3

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 807
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 808
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "appId"

    aput-object v5, v2, v4

    const-string v4, "queryPeriod"

    aput-object v4, v2, v8

    .line 809
    const-string v5, "queryPeriod ASC LIMIT 1"

    move-object v4, v3

    .line 806
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 810
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 811
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "Error occured in query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 813
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v8, :cond_2

    .line 814
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "No results for query : getNextScheduledEntry"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 820
    goto :goto_0

    .line 816
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 817
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method public getPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 919
    if-nez p1, :cond_1

    .line 920
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "AppID is null: Hence ignore"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    :cond_0
    :goto_0
    return-object v5

    .line 923
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 924
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getPackageName: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 927
    :cond_2
    const/4 v7, 0x0

    .line 928
    .local v7, "pkgName":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 929
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 930
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "packagename"

    aput-object v3, v2, v8

    const-string v3, "appId=?"

    .line 931
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 928
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 933
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 936
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    .line 937
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 939
    const-string v0, "packagename"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 938
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 941
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v7

    .line 942
    goto :goto_0
.end method

.method public getPendingJobs()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 367
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 368
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getPendingJobs: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :goto_0
    return-object v5

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 372
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 373
    const-string v3, "isPending=?"

    new-array v4, v10, [Ljava/lang/String;

    const-string v11, "1"

    aput-object v11, v4, v9

    .line 371
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 374
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 375
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getPendingJobs: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 378
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 379
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getPendingJobs: cursor size is 0"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 383
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 384
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 387
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;>;"
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntry()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object v6

    .line 389
    .local v6, "appEntry":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 388
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 391
    const-string v0, "certInfo"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 390
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setCertificateInfo(Ljava/lang/String;)V

    .line 393
    const-string v0, "certState"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 392
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 395
    const-string v0, "certEntities"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 394
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setEntity(Ljava/lang/String;)V

    .line 397
    const-string v0, "packagename"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 396
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setPackageName(Ljava/lang/String;)V

    .line 399
    const-string v0, "isPending"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 398
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v9

    :goto_1
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 401
    const-string v0, "queryPeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 400
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriodInMillis(J)V

    .line 403
    const-string v0, "gracePeriod"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 402
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setGracePeriodInMillis(J)V

    .line 404
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    .line 405
    const-string v1, "certType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 404
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setType(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)V

    .line 407
    const-string v0, "maxRetry"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 406
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 409
    const-string v0, "isCertified"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 408
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v9

    :goto_2
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 412
    const-string v0, "isRevoked"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 411
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v9

    :goto_3
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsRevoked(Z)V

    .line 414
    const-string v0, "isMemberApp"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 413
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    :goto_4
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsMemberApp(Z)V

    .line 417
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 420
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v8

    .line 421
    goto/16 :goto_0

    :cond_4
    move v0, v10

    .line 399
    goto/16 :goto_1

    :cond_5
    move v0, v10

    .line 409
    goto :goto_2

    :cond_6
    move v0, v10

    .line 412
    goto :goto_3

    :cond_7
    move v0, v10

    .line 415
    goto :goto_4
.end method

.method public getQueryPeriod(Ljava/lang/String;)J
    .locals 11
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const-wide/16 v9, 0x0

    .line 871
    const-wide/16 v7, 0x0

    .line 872
    .local v7, "queryPeriod":J
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 873
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "getQueryPeriod: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v9

    .line 893
    :goto_0
    return-wide v0

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 877
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 878
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "queryPeriod"

    aput-object v3, v2, v5

    const-string v3, "appId=?"

    .line 879
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    const/4 v5, 0x0

    .line 876
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 880
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    move-wide v0, v9

    .line 881
    goto :goto_0

    .line 883
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 884
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v0, v9

    .line 885
    goto :goto_0

    .line 888
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 891
    const-string v0, "queryPeriod"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 890
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 892
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-wide v0, v7

    .line 893
    goto :goto_0
.end method

.method public getState(I)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1045
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    if-le p1, v1, :cond_0

    .line 1046
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 1050
    .local v0, "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :goto_0
    return-object v0

    .line 1048
    .end local v0    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    :cond_0
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->values()[Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v1

    aget-object v0, v1, p1

    .restart local v0    # "st":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;
    goto :goto_0
.end method

.method public insertCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    .locals 7
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 112
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 113
    .local v1, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "pkgName":Ljava/lang/String;
    const-string v2, "AcmsAppEntryInterface"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "insertCertificate : enter2:  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 119
    const-string v2, "AcmsAppEntryInterface"

    .line 120
    const-string v5, "insertCertificate : Certificate not present, hence inserting"

    .line 119
    invoke-static {v2, v5}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "appId"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "certInfo"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getCertificateInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "certState"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v5

    .line 124
    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 123
    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 126
    const-string v2, "certEntities"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    const-string v2, "packagename"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v5, "isPending"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isPending()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 130
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v2

    if-nez v2, :cond_5

    .line 131
    const-string v2, "certType"

    sget-object v5, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 136
    const-string v2, "queryPeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 138
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 139
    const-string v2, "gracePeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 142
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 143
    const-string v2, "maxRetry"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    :cond_3
    const-string v5, "isCertified"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isCertified()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 148
    const-string v5, "isRevoked"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v3

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    const-string v5, "isMemberApp"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isMemberApp()Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v3

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v2, :cond_9

    .line 153
    const-string v2, "AcmsAppEntryInterface"

    const-string v3, "Context is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_5
    return v4

    :cond_4
    move v2, v4

    .line 129
    goto/16 :goto_0

    .line 133
    :cond_5
    const-string v2, "certType"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_6
    move v2, v4

    .line 146
    goto :goto_2

    :cond_7
    move v2, v4

    .line 148
    goto :goto_3

    :cond_8
    move v2, v4

    .line 150
    goto :goto_4

    .line 156
    :cond_9
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->checkIfPkgNameExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 157
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->insertToPkgNameContent(Ljava/lang/String;)V

    .line 159
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 160
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 159
    invoke-virtual {v2, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :cond_b
    move v4, v3

    .line 163
    goto :goto_5
.end method

.method public isPkgNamePresent(Ljava/lang/String;)Z
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 985
    const/4 v7, 0x0

    .line 986
    .local v7, "returnVal":Z
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    move v0, v8

    .line 1001
    :goto_0
    return v0

    .line 989
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 990
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 991
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "packagename"

    aput-object v3, v2, v8

    .line 992
    const-string v3, "packagename=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    const/4 v5, 0x0

    .line 989
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 993
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    move v0, v8

    .line 994
    goto :goto_0

    .line 997
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 998
    const/4 v7, 0x1

    .line 1000
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 1001
    goto :goto_0
.end method

.method public updateCertInfoToNonDriveMode(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Ljava/lang/String;
    .locals 14
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    const/16 v13, 0x10

    .line 1072
    const-string v10, "AcmsAppEntryInterface"

    const-string v11, "updateCertInfoToNonDriveMode"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v0

    .line 1076
    .local v0, "appEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getCertificateInfoForAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1077
    .local v1, "certInfo":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1078
    const-string v10, "AcmsAppEntryInterface"

    .line 1079
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "no certificate info is present in DB for this appId"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1080
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 1079
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1078
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    const/4 v10, 0x0

    .line 1110
    :goto_0
    return-object v10

    .line 1083
    :cond_0
    const-string v10, "AcmsAppEntryInterface"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CertInfo: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    const-string v10, "contentCategory"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getStartTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1085
    .local v9, "startTag":Ljava/lang/String;
    const-string v10, "contentCategory"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getEndTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1086
    .local v4, "endTag":Ljava/lang/String;
    invoke-virtual {v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 1087
    .local v8, "startIndex":I
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v8, v10

    .line 1088
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 1089
    .local v6, "lastIndex":I
    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1090
    .local v3, "contentCategory":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1091
    .local v2, "contCategoryInt":I
    const-string v10, "0x"

    invoke-virtual {v3, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1092
    const/4 v10, 0x2

    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 1096
    :goto_1
    const v10, -0x10001

    and-int/2addr v2, v10

    .line 1097
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "0x"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1098
    .local v5, "finalContCatry":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1099
    const-string v10, "AcmsAppEntryInterface"

    const-string v11, "new CertInfo after content category update:"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const-string v10, "AcmsAppEntryInterface"

    invoke-static {v10, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    const-string v10, "restricted"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getStartTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1102
    const-string v10, "restricted"

    invoke-direct {p0, v10}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getEndTag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1103
    invoke-virtual {v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 1104
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v8, v10

    .line 1105
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 1106
    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1107
    .local v7, "restricMode":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1108
    const-string v10, "AcmsAppEntryInterface"

    const-string v11, "new CertInfo after restricted mode update:"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    const-string v10, "AcmsAppEntryInterface"

    invoke-static {v10, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v10, v1

    .line 1110
    goto/16 :goto_0

    .line 1094
    .end local v5    # "finalContCatry":Ljava/lang/String;
    .end local v7    # "restricMode":Ljava/lang/String;
    :cond_1
    invoke-static {v3, v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    goto/16 :goto_1
.end method

.method public updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    .locals 11
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 201
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 203
    .local v8, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 204
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "updateCertificate: context is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :goto_0
    return v10

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 208
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "appId"

    aput-object v3, v2, v10

    .line 209
    const-string v3, "appId=?"

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    .line 210
    const/4 v5, 0x0

    .line 207
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 211
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 212
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "updateCertificate: cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_c

    .line 216
    const-string v0, "AcmsAppEntryInterface"

    .line 217
    const-string v1, "updateCertificate : Certificate already present, hence updating"

    .line 216
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getCertificateInfo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 219
    const-string v0, "certInfo"

    .line 220
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getCertificateInfo()Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 223
    const-string v0, "certState"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v1

    .line 224
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 223
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 226
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 227
    const-string v0, "certEntities"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getEntity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_4
    const-string v0, "isPending"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isPending()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 230
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 231
    const-string v0, "certType"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 234
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 235
    const-string v0, "queryPeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 237
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 238
    const-string v0, "gracePeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 241
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 242
    const-string v0, "maxRetry"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    :cond_8
    const-string v1, "isCertified"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isCertified()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v9

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 246
    const-string v1, "isRevoked"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v9

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248
    const-string v1, "isMemberApp"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isMemberApp()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v9

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 251
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 252
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "appId=?"

    .line 253
    new-array v3, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    .line 251
    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 255
    .local v7, "updated":I
    const-string v0, "AcmsAppEntryInterface"

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "row : appId ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 257
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 256
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v10, v9

    .line 264
    goto/16 :goto_0

    .end local v7    # "updated":I
    :cond_9
    move v0, v10

    .line 244
    goto :goto_1

    :cond_a
    move v0, v10

    .line 246
    goto :goto_2

    :cond_b
    move v0, v10

    .line 248
    goto :goto_3

    .line 259
    :cond_c
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 260
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "updateCertificate: Given appId not found!!"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateDevApps(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    .locals 14
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 276
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 277
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "updateDevApps: Context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :goto_0
    return v13

    .line 280
    :cond_0
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 281
    .local v11, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 282
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 283
    new-array v2, v12, [Ljava/lang/String;

    const-string v3, "certType"

    aput-object v3, v2, v13

    const-string v3, "appId=?"

    .line 284
    new-array v4, v12, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v13

    const/4 v5, 0x0

    .line 281
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 285
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 286
    .local v10, "updated":I
    if-nez v8, :cond_1

    .line 287
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "updateDevApps: Cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_c

    .line 291
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 292
    const-string v0, "certType"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_BASE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .line 293
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v1

    .line 292
    if-eq v0, v1, :cond_2

    .line 294
    const-string v0, "certType"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->DEV_DRIVE_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    .line 295
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v1

    .line 294
    if-ne v0, v1, :cond_b

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v0

    .line 299
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 298
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 300
    .local v9, "devId":Ljava/lang/String;
    const-string v0, "AcmsAppEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "devId  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v0

    .line 304
    invoke-virtual {v0, v9}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getAppIdsFromDevId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 306
    .local v7, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v7, :cond_3

    .line 307
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "No app found for given devId"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 312
    :cond_3
    const-string v0, "AcmsAppEntryInterface"

    .line 313
    const-string v1, "updateCertificate : Certificate present, updating"

    .line 312
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 315
    const-string v0, "certState"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getState()Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    move-result-object v1

    .line 316
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 315
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 319
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 320
    const-string v0, "queryPeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getQueryPeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 322
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 323
    const-string v0, "gracePeriod"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 326
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 327
    const-string v0, "maxRetry"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getMaxRetry()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 331
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 332
    const-string v0, "certType"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334
    :cond_8
    const-string v0, "isCertified"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isCertified()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 335
    const-string v0, "isPending"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isPending()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 337
    const-string v1, "isRevoked"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v12

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 339
    const-string v0, "AcmsAppEntryInterface"

    const-string v1, "appIds  "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_a

    .line 347
    const-string v0, "AcmsAppEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v13, v12

    .line 363
    goto/16 :goto_0

    :cond_9
    move v0, v13

    .line 337
    goto :goto_1

    .line 340
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 341
    .local v6, "app":Ljava/lang/String;
    const-string v1, "AcmsAppEntryInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updating for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 344
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI:Landroid/net/Uri;

    .line 345
    const-string v3, "appId=?"

    new-array v4, v12, [Ljava/lang/String;

    aput-object v6, v4, v13

    .line 343
    invoke-virtual {v1, v2, v11, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    goto :goto_2

    .line 349
    .end local v6    # "app":Ljava/lang/String;
    .end local v7    # "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "devId":Ljava/lang/String;
    :cond_b
    const-string v0, "AcmsAppEntryInterface"

    .line 350
    const-string v1, "updateCertificate: Given certificate is not developer certificate "

    .line 349
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 356
    :cond_c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 357
    const-string v0, "AcmsAppEntryInterface"

    .line 358
    const-string v1, "updateCertificate: Given appId not present in the database "

    .line 357
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
