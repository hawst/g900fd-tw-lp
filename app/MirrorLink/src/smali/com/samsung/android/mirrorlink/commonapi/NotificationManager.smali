.class public Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;
.super Lcom/mirrorlink/android/commonapi/INotificationManager$Stub;
.source "NotificationManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSNotificationManager"


# instance fields
.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private final mListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/INotificationListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/INotificationManager$Stub;-><init>()V

    .line 25
    const-string v0, "TMSNotificationManager"

    const-string v1, "Enter Constructor NotificationManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 27
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager$1;-><init>(Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    .line 34
    const-string v0, "TMSNotificationManager"

    const-string v1, "Exit Constructor NotificationManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public cancelNotification(I)Z
    .locals 1
    .param p1, "notificationId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public getNotificationConfiguration()Landroid/os/Bundle;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationEnabled()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 127
    const-string v0, "TMSNotificationManager"

    .line 128
    const-string v1, "NotificationManager.killListener() : Enter"

    .line 127
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 130
    return-void
.end method

.method public notifyNotificationActionReceived(II)V
    .locals 6
    .param p1, "notificationId"    # I
    .param p2, "actionId"    # I

    .prologue
    .line 77
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 78
    .local v2, "length":I
    const-string v3, "TMSNotificationManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NotificationManager.notifyNotificationActionReceived():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 90
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 91
    return-void

    .line 83
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/INotificationListener;

    .line 84
    invoke-interface {v3, p1, p2}, Lcom/mirrorlink/android/commonapi/INotificationListener;->onNotificationActionReceived(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyNotificationConfigurationChanged(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "notificationConfiguration"    # Landroid/os/Bundle;

    .prologue
    .line 94
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 95
    .local v2, "length":I
    const-string v3, "TMSNotificationManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NotificationManager.notifyNotificationConfigurationChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 106
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 107
    return-void

    .line 100
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/INotificationListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/INotificationListener;->onNotificationConfigurationChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyNotificationEnabledChanged(Z)V
    .locals 6
    .param p1, "notiEnabled"    # Z

    .prologue
    .line 110
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 111
    .local v2, "length":I
    const-string v3, "TMSNotificationManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NotificationManager.notifyNotificationEnabledChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 122
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 123
    return-void

    .line 116
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/INotificationListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/INotificationListener;->onNotificationEnabledChanged(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public sendClientNotification(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)I
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "iconUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 59
    .local p4, "actionList":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public sendVncNotification()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public setNotificationSupported(Z)V
    .locals 0
    .param p1, "notificationSupported"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 41
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSNotificationManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EventMappingManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 135
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    if-nez v2, :cond_0

    .line 148
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 141
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 147
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 143
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/NotificationManager;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/INotificationListener;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_2

    .line 141
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
