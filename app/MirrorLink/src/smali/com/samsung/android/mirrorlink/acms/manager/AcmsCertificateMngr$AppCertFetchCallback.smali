.class public Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;
.super Ljava/lang/Object;
.source "AcmsCertificateMngr.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/acms/http/AcmsHttpCertificateHandler$AppCertCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppCertFetchCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)V
    .locals 0

    .prologue
    .line 715
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isCertChainValid([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z
    .locals 5
    .param p1, "certs"    # [Ljava/security/cert/X509Certificate;
    .param p2, "appInfo"    # Ljava/lang/String;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    .prologue
    const/4 v2, 0x0

    .line 954
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;

    .line 955
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v3

    .line 954
    invoke-direct {v0, v3}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;-><init>(Landroid/content/Context;)V

    .line 957
    .local v0, "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    :try_start_0
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->validateCertChain([Ljava/security/cert/X509Certificate;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 958
    const-string v3, "AcmsCertificateMngr"

    .line 959
    const-string v4, "validateCertChain Failed: hence making it non-certified "

    .line 958
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_7

    .line 1018
    :goto_0
    return v2

    .line 964
    :catch_0
    move-exception v1

    .line 965
    .local v1, "e":Ljava/security/InvalidKeyException;
    const-string v3, "AcmsCertificateMngr"

    .line 966
    const-string v4, "validateCertChain:InvalidKeyException, hence making it non-certified "

    .line 965
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 969
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0

    .line 971
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_1
    move-exception v1

    .line 972
    .local v1, "e":Ljava/security/cert/CertificateException;
    const-string v3, "AcmsCertificateMngr"

    .line 973
    const-string v4, "validateCertChain: CertificateException, hence making it non-certified "

    .line 972
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 976
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0

    .line 978
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_2
    move-exception v1

    .line 979
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v3, "AcmsCertificateMngr"

    .line 980
    const-string v4, "validateCertChain: NoSuchAlgorithmException, hence making it non-certified "

    .line 979
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 983
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 985
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_3
    move-exception v1

    .line 986
    .local v1, "e":Ljava/security/NoSuchProviderException;
    const-string v3, "AcmsCertificateMngr"

    .line 987
    const-string v4, "validateCertChain:NoSuchProviderException, hence making it non-certified "

    .line 986
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 990
    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_0

    .line 992
    .end local v1    # "e":Ljava/security/NoSuchProviderException;
    :catch_4
    move-exception v1

    .line 993
    .local v1, "e":Ljava/security/SignatureException;
    const-string v3, "AcmsCertificateMngr"

    .line 994
    const-string v4, "validateCertChain:SignatureException, hence making it non-certified "

    .line 993
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 997
    invoke-virtual {v1}, Ljava/security/SignatureException;->printStackTrace()V

    goto :goto_0

    .line 999
    .end local v1    # "e":Ljava/security/SignatureException;
    :catch_5
    move-exception v1

    .line 1000
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "AcmsCertificateMngr"

    .line 1001
    const-string v4, "validateCertChain:SignatureException, hence making it non-certified "

    .line 1000
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 1004
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1006
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_6
    move-exception v1

    .line 1007
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1009
    .end local v1    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 1011
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v3, "AcmsCertificateMngr"

    .line 1012
    const-string v4, "validateCertChain Failed:XmlPullParserException, hence making it non-certified "

    .line 1010
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p4}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 1015
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto/16 :goto_0

    .line 1018
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_0
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private isDevCertChainValid(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;[Ljava/security/cert/X509Certificate;Ljava/lang/String;)Z
    .locals 8
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "certs"    # [Ljava/security/cert/X509Certificate;
    .param p3, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 863
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    move-result-object v2

    .line 864
    .local v2, "devIdCertEntryInterface":Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;

    .line 865
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v5

    .line 864
    invoke-direct {v0, v5}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;-><init>(Landroid/content/Context;)V

    .line 866
    .local v0, "certificateValidator":Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;
    invoke-virtual {v2, p3}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 867
    .local v1, "devId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    invoke-virtual {v5, v1, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->addToKeyStore(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 868
    const-string v5, "AcmsCertificateMngr"

    .line 869
    const-string v6, "DevIdCert Chain Added to the Keystore successfully "

    .line 868
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_0
    :try_start_0
    invoke-virtual {v0, p2, p1}, Lcom/samsung/android/mirrorlink/acms/utils/CertificateValidator;->validateDevIdCertChain([Ljava/security/cert/X509Certificate;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 876
    const-string v5, "AcmsCertificateMngr"

    .line 877
    const-string v6, "DevId certificate is valid. Hence replacing entity field in appCertInfo from Developer to CCC"

    .line 875
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    const-string v6, "DEVELOPER"

    const-string v7, "CCC"

    invoke-virtual {v5, p3, v6, v7}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->replaceEntityInAppCertInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 883
    const/4 v4, 0x1

    .line 948
    :goto_0
    return v4

    .line 885
    :cond_1
    const-string v5, "AcmsCertificateMngr"

    .line 886
    const-string v6, "validateCertChain Failed: hence moving to Non-Certified "

    .line 885
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_7

    goto :goto_0

    .line 892
    :catch_0
    move-exception v3

    .line 893
    .local v3, "e":Ljava/security/InvalidKeyException;
    const-string v5, "AcmsCertificateMngr"

    .line 894
    const-string v6, "validateCertChain:InvalidKeyException, hence moving to Non-Certified "

    .line 893
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 897
    invoke-virtual {v3}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0

    .line 899
    .end local v3    # "e":Ljava/security/InvalidKeyException;
    :catch_1
    move-exception v3

    .line 900
    .local v3, "e":Ljava/security/cert/CertificateExpiredException;
    const-string v5, "AcmsCertificateMngr"

    .line 901
    const-string v6, "validateCertChain: CertificateExpiredException, hence moving to Non-Certified"

    .line 900
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 904
    invoke-virtual {v3}, Ljava/security/cert/CertificateExpiredException;->printStackTrace()V

    goto :goto_0

    .line 906
    .end local v3    # "e":Ljava/security/cert/CertificateExpiredException;
    :catch_2
    move-exception v3

    .line 908
    .local v3, "e":Ljava/security/cert/CertificateNotYetValidException;
    const-string v5, "AcmsCertificateMngr"

    .line 909
    const-string v6, "validateCertChain: CertificateNotYetValidException, hence moving to Non-Certified"

    .line 907
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 912
    invoke-virtual {v3}, Ljava/security/cert/CertificateNotYetValidException;->printStackTrace()V

    goto :goto_0

    .line 914
    .end local v3    # "e":Ljava/security/cert/CertificateNotYetValidException;
    :catch_3
    move-exception v3

    .line 915
    .local v3, "e":Ljava/security/cert/CertificateException;
    const-string v5, "AcmsCertificateMngr"

    .line 916
    const-string v6, "validateCertChain: CertificateException, hence moving to Non-Certified"

    .line 915
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 919
    invoke-virtual {v3}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0

    .line 921
    .end local v3    # "e":Ljava/security/cert/CertificateException;
    :catch_4
    move-exception v3

    .line 922
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v5, "AcmsCertificateMngr"

    .line 923
    const-string v6, "validateCertChain: NoSuchAlgorithmException, hence moving to Non-Certified"

    .line 922
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 926
    invoke-virtual {v3}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 928
    .end local v3    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_5
    move-exception v3

    .line 929
    .local v3, "e":Ljava/security/NoSuchProviderException;
    const-string v5, "AcmsCertificateMngr"

    .line 930
    const-string v6, "validateCertChain:NoSuchProviderException, hence moving to Non-Certified "

    .line 929
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 933
    invoke-virtual {v3}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto/16 :goto_0

    .line 935
    .end local v3    # "e":Ljava/security/NoSuchProviderException;
    :catch_6
    move-exception v3

    .line 936
    .local v3, "e":Ljava/security/SignatureException;
    const-string v5, "AcmsCertificateMngr"

    .line 937
    const-string v6, "validateCertChain:SignatureException, hence moving to Non-Certified "

    .line 936
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 940
    invoke-virtual {v3}, Ljava/security/SignatureException;->printStackTrace()V

    goto/16 :goto_0

    .line 942
    .end local v3    # "e":Ljava/security/SignatureException;
    :catch_7
    move-exception v3

    .line 943
    .local v3, "e":Ljavax/crypto/BadPaddingException;
    const-string v5, "AcmsCertificateMngr"

    .line 944
    const-string v6, "validateCertChain:BadPaddingException, hence moving to Non-Certified "

    .line 943
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v6, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v5, v6, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 947
    invoke-virtual {v3}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public onError(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;I)V
    .locals 12
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "errorType"    # I

    .prologue
    const-wide/16 v10, 0x18

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 720
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 721
    .local v1, "appId":Ljava/lang/String;
    const-string v3, "AcmsCertificateMngr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AppCertFetchCallback.onError() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 722
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 721
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 725
    if-ne p2, v9, :cond_1

    .line 729
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;-><init>()V

    .line 730
    .local v0, "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setAppId(Ljava/lang/String;)V

    .line 731
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 732
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 733
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateDevApps(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 734
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 735
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriod(Ljava/lang/Long;)V

    .line 736
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 737
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 816
    .end local v0    # "appEntryTemp":Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 748
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_4

    .line 749
    const/16 v3, 0xfc1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 750
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 765
    :cond_3
    :goto_1
    const/4 v3, 0x3

    if-ne p2, v3, :cond_8

    .line 767
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 768
    .local v2, "currentTime":Ljava/util/Calendar;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isRevoked()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 771
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    .line 772
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_REVOKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 771
    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 753
    .end local v2    # "currentTime":Ljava/util/Calendar;
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_5

    .line 754
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->isPending()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 755
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0

    .line 760
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_3

    .line 761
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getMaxRetry(Ljava/lang/String;)I

    move-result v3

    add-int/lit16 v3, v3, -0xa8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setMaxRetry(Ljava/lang/Integer;)V

    .line 762
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    goto :goto_1

    .line 774
    .restart local v2    # "currentTime":Ljava/util/Calendar;
    :cond_6
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 775
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getGracePeriod()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 774
    sub-long/2addr v3, v5

    .line 775
    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_7

    .line 778
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mRevocationMngr:Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$4(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    .line 779
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_MLAWARE_UNCHECKED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    .line 778
    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->postToMlCertifiedHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 783
    :cond_7
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 784
    invoke-virtual {p1, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 785
    invoke-virtual {p1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 786
    const-wide/16 v3, 0xa8

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriod(Ljava/lang/Long;)V

    .line 787
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 788
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 793
    .end local v2    # "currentTime":Ljava/util/Calendar;
    :cond_8
    if-ne p2, v9, :cond_9

    .line 794
    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_RETRY:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 795
    invoke-virtual {p1, v8}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsPending(Z)V

    .line 796
    invoke-virtual {p1, v7}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 797
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setQueryPeriod(Ljava/lang/Long;)V

    .line 798
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 799
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$0(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->getAcmsRevocationMngr(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsRevocationMngr;->scheduleAlarm()V

    .line 803
    :cond_9
    if-ne p2, v8, :cond_a

    .line 804
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 806
    :cond_a
    const/4 v3, 0x2

    if-ne p2, v3, :cond_b

    .line 807
    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$5()I

    move-result v3

    if-lez v3, :cond_b

    .line 808
    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$5()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$6(I)V

    .line 809
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 811
    :cond_b
    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->RETRY_COUNT:I
    invoke-static {}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$5()I

    move-result v3

    if-nez v3, :cond_0

    .line 812
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_NON_CERTIFIED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v3, v4, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onSuccess(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;[Ljava/security/cert/X509Certificate;)V
    .locals 5
    .param p1, "appEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;
    .param p2, "certs"    # [Ljava/security/cert/X509Certificate;

    .prologue
    .line 820
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getCertificateInfo()Ljava/lang/String;

    move-result-object v1

    .line 821
    .local v1, "appInfo":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 822
    .local v0, "appId":Ljava/lang/String;
    const-string v2, "AcmsCertificateMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AppCertFetchCallback.onSuccess() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 823
    array-length v4, p2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 822
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_APPCERT_FETCH_COMPLETED:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v2, v3, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    .line 827
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->getType()Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsUtil;->isDevelopmentApp(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry$TYPE;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 828
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->isDevCertChainValid(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;[Ljava/security/cert/X509Certificate;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 858
    :goto_0
    return-void

    .line 831
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->getAppEntryfromAppId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;

    move-result-object p1

    .line 832
    if-nez p1, :cond_3

    .line 833
    const-string v2, "AcmsCertificateMngr"

    .line 834
    const-string v3, "AppCertFetchCallback.onSuccess() appEntry null for given appId"

    .line 833
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 840
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    invoke-virtual {v2, v0, p2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->addToKeyStore(Ljava/lang/String;[Ljava/security/cert/X509Certificate;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 841
    const-string v2, "AcmsCertificateMngr"

    .line 842
    const-string v3, "AppCertFetchCallback.onSuccess() Cert Chain Added to keystore successfully "

    .line 841
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    :cond_2
    invoke-direct {p0, p2, v1, v0, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->isCertChainValid([Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 846
    const-string v2, "AcmsCertificateMngr"

    .line 847
    const-string v3, "AppCertFetchCallback.onSuccess() Certchain invalid for appId"

    .line 846
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 854
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setIsCertified(Z)V

    .line 855
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {p1, v2}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;->setState(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;)V

    .line 856
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    # getter for: Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->mAppEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->access$3(Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;)Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;->updateCertificate(Lcom/samsung/android/mirrorlink/acms/provider/AppEntry;)Z

    .line 857
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr$AppCertFetchCallback;->this$0:Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;

    sget-object v3, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;->STATE_OCSPCERT_QUERY_PENDING:Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;

    invoke-virtual {v2, v3, p1}, Lcom/samsung/android/mirrorlink/acms/manager/AcmsCertificateMngr;->postToMlAwareHandler(Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$State;Ljava/lang/Object;)V

    goto :goto_0
.end method
