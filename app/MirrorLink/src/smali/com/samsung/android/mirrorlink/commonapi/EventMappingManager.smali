.class public Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;
.super Lcom/mirrorlink/android/commonapi/IEventMappingManager$Stub;
.source "EventMappingManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSEventMappingManager"


# instance fields
.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private final mListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IEventMappingListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IEventMappingManager$Stub;-><init>()V

    .line 25
    const-string v0, "TMSEventMappingManager"

    const-string v1, "Entered Constructor EventMappingManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setEventMappingManager(Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;)V

    .line 31
    :cond_0
    const-string v0, "TMSEventMappingManager"

    const-string v1, "Exit Constructor EventMappingManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public getEventConfiguration()Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 49
    const-string v1, "TMSEventMappingManager"

    const-string v2, "getEventConfiguration() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "info":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getEventConfig()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    :goto_0
    const-string v1, "TMSEventMappingManager"

    const-string v2, "getEventConfiguration() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-object v0

    .line 54
    :cond_0
    const-string v1, "TMSEventMappingManager"

    const-string v2, "getEventConfiguration(): Tmsengine is null. So, returning default values for event config"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v1, "KEYBOARD_LANGUAGE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "KEYBOARD_COUNTRY"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v1, "UI_LANGUAGE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v1, "UI_COUNTRY"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "KNOB_KEY_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    const-string v1, "DEVICE_KEY_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    const-string v1, "MULTIMEDIA_KEY_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    const-string v1, "FUNC_KEY_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    const-string v1, "ITU_KEY_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 64
    const-string v1, "TOUCH_SUPPORT"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    const-string v1, "PRESSURE_MASK"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getEventMappings()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 74
    const-string v2, "TMSEventMappingManager"

    const-string v3, "getEventMappings() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x0

    .line 76
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 77
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getEventMapping()Ljava/util/List;

    move-result-object v1

    .line 86
    :goto_0
    return-object v1

    .line 79
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "TMSEventMappingManager"

    const-string v3, "getEventMappings(): Tmsengine is null. So, returning default values for EventMappings"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v2, "LOCAL_EVENT"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v2, "REMOTE_EVENT"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .restart local v1    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "TMSEventMappingManager"

    .line 44
    const-string v1, "EventMappingManager.killListener() : Enter"

    .line 43
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 46
    return-void
.end method

.method public notifyEventConfigurationChanged(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "eventConfiguration"    # Landroid/os/Bundle;

    .prologue
    .line 131
    const-string v3, "TMSEventMappingManager"

    .line 132
    const-string v4, "EventMappingManager.notifyEventConfigurationChanged() -- Enter"

    .line 131
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 134
    .local v2, "length":I
    const-string v3, "TMSEventMappingManager"

    .line 135
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EventMappingManager.notifyEventConfigurationChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 135
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 134
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 146
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 147
    return-void

    .line 140
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IEventMappingListener;

    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IEventMappingListener;->onEventConfigurationChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyEventMappingChanged(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "eventMapping"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v7, 0x0

    .line 108
    const-string v4, "TMSEventMappingManager"

    .line 109
    const-string v5, "EventMappingManager.notifyEventMappingChanged() -- Enter"

    .line 108
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v3

    .line 111
    .local v3, "length":I
    const-string v4, "TMSEventMappingManager"

    .line 112
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "EventMappingManager.notifyEventMappingChanged():length"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 112
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 111
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    move-object v0, p1

    .line 115
    .local v0, "b":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 116
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "b":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 117
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "LOCAL_EVENT"

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 118
    const-string v4, "REMOTE_EVENT"

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 120
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 127
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 128
    return-void

    .line 122
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/mirrorlink/android/commonapi/IEventMappingListener;

    invoke-interface {v4, v0}, Lcom/mirrorlink/android/commonapi/IEventMappingListener;->onEventMappingChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 123
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IEventMappingListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IEventMappingListener;

    .prologue
    .line 35
    const-string v0, "TMSEventMappingManager"

    const-string v1, " EventMappingManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 37
    const-string v0, "TMSEventMappingManager"

    const-string v1, "EventMappingManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSEventMappingManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EventMappingManager.unregister():packname= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    if-nez v2, :cond_0

    .line 105
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 98
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 104
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 100
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->mListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IEventMappingListener;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_2

    .line 98
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
