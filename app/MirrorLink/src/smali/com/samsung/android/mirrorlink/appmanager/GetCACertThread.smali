.class Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;
.super Ljava/lang/Thread;
.source "DapCertificateProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSGetCACertThread"


# instance fields
.field certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 99
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    .line 96
    return-void
.end method


# virtual methods
.method public getFromResponse(Lcom/samsung/android/mirrorlink/appmanager/CertResponse;Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/CertResponse;
    .locals 7
    .param p1, "caCert"    # Lcom/samsung/android/mirrorlink/appmanager/CertResponse;
    .param p2, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 189
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 190
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1, v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 191
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 192
    .local v4, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v5, Ljava/io/StringReader;

    invoke-direct {v5, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 194
    const/4 v3, 0x0

    .line 196
    .local v3, "text":Ljava/lang/String;
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 197
    .local v0, "eventType":I
    :goto_0
    if-ne v0, v6, :cond_0

    .line 221
    return-object p1

    .line 199
    :cond_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "tagname":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 219
    :cond_1
    :goto_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 203
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 204
    goto :goto_1

    .line 207
    :pswitch_1
    const-string v5, "content"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 208
    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->setContent(Ljava/lang/String;)V

    goto :goto_1

    .line 209
    :cond_2
    const-string v5, "nonce"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 210
    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->setNonce(Ljava/lang/String;)V

    goto :goto_1

    .line 211
    :cond_3
    const-string v5, "rootContent"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 212
    invoke-virtual {p1, v3}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->setRootContent(Ljava/lang/String;)V

    goto :goto_1

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public run()V
    .locals 17

    .prologue
    .line 123
    :try_start_0
    new-instance v8, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v8}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 124
    .local v8, "client":Lorg/apache/http/client/HttpClient;
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    const-string v1, "https://eu-mlca.samsungosp.com/v2/ca/scep?operation=GetCACert"

    invoke-direct {v12, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 125
    .local v12, "request":Lorg/apache/http/client/methods/HttpGet;
    const-string v1, "x-osp-appId"

    const-string v2, "2095nt95l7"

    invoke-virtual {v12, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v1, "Authorization"

    const-string v2, "Basic MjA5NW50OTVsNzo2NjNBRDkzOTc3MDg1QzQyNjg2QzUzRUY0QzlCNjcxNw=="

    invoke-virtual {v12, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-interface {v8, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 128
    .local v13, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v15

    .line 129
    .local v15, "statusLine":Lorg/apache/http/StatusLine;
    invoke-interface {v15}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    .line 130
    .local v14, "statusCode":I
    const-string v1, "TMSGetCACertThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Status Code is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/16 v1, 0xc8

    if-ne v14, v1, :cond_3

    .line 132
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    .line 133
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 132
    invoke-direct {v11, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 134
    .local v11, "rd":Ljava/io/BufferedReader;
    const-string v10, ""

    .line 135
    .local v10, "line":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 140
    :cond_0
    if-eqz v10, :cond_1

    .line 141
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->getFromResponse(Lcom/samsung/android/mirrorlink/appmanager/CertResponse;Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    .line 147
    :cond_1
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 148
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getRootContent()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 149
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getContent()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getNonce()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 151
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getRootContent()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getRootContent()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3

    .line 154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getContent()Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 155
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v5}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getContent()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    array-length v5, v5

    .line 156
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getNonce()Ljava/lang/String;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 157
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/mirrorlink/appmanager/GetCACertThread;->certResponse:Lcom/samsung/android/mirrorlink/appmanager/CertResponse;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/CertResponse;->getNonce()Ljava/lang/String;

    move-result-object v7

    const-string v16, "UTF-8"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    array-length v7, v7

    .line 151
    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendGetCACertResp([BI[BI[BI)V

    .line 185
    .end local v8    # "client":Lorg/apache/http/client/HttpClient;
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "rd":Ljava/io/BufferedReader;
    .end local v12    # "request":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "statusCode":I
    .end local v15    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_2
    :goto_0
    return-void

    .line 163
    .restart local v8    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "request":Lorg/apache/http/client/methods/HttpGet;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "statusCode":I
    .restart local v15    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_3
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 165
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 168
    .end local v8    # "client":Lorg/apache/http/client/HttpClient;
    .end local v12    # "request":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "statusCode":I
    .end local v15    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_0
    move-exception v9

    .line 169
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 170
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 172
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V

    goto :goto_0

    .line 174
    .end local v9    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 176
    .local v9, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v9}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 177
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 179
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendFetchCertNegativeResp()V

    goto :goto_0
.end method
