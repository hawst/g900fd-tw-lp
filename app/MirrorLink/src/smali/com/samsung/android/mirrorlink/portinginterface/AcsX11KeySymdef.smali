.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsX11KeySymdef;
.super Ljava/lang/Object;
.source "AcsX11KeySymdef.java"


# static fields
.field public static final ACS_X11_0:I = 0x30

.field public static final ACS_X11_1:I = 0x31

.field public static final ACS_X11_2:I = 0x32

.field public static final ACS_X11_3:I = 0x33

.field public static final ACS_X11_4:I = 0x34

.field public static final ACS_X11_5:I = 0x35

.field public static final ACS_X11_6:I = 0x36

.field public static final ACS_X11_7:I = 0x37

.field public static final ACS_X11_8:I = 0x38

.field public static final ACS_X11_9:I = 0x39

.field public static final ACS_X11_A:I = 0x41

.field public static final ACS_X11_AE:I = 0xc6

.field public static final ACS_X11_Aacute:I = 0xc1

.field public static final ACS_X11_Acircumflex:I = 0xc2

.field public static final ACS_X11_Adiaeresis:I = 0xc4

.field public static final ACS_X11_Agrave:I = 0xc0

.field public static final ACS_X11_Alt_L:I = 0xffe9

.field public static final ACS_X11_Alt_R:I = 0xffea

.field public static final ACS_X11_Aring:I = 0xc5

.field public static final ACS_X11_Atilde:I = 0xc3

.field public static final ACS_X11_B:I = 0x42

.field public static final ACS_X11_BackSpace:I = 0xff08

.field public static final ACS_X11_Begin:I = 0xff58

.field public static final ACS_X11_Break:I = 0xff6b

.field public static final ACS_X11_C:I = 0x43

.field public static final ACS_X11_Cancel:I = 0xff69

.field public static final ACS_X11_Caps_Lock:I = 0xffe5

.field public static final ACS_X11_Ccedilla:I = 0xc7

.field public static final ACS_X11_Clear:I = 0xff0b

.field public static final ACS_X11_Codeinput:I = 0xff37

.field public static final ACS_X11_Control_L:I = 0xffe3

.field public static final ACS_X11_Control_R:I = 0xffe4

.field public static final ACS_X11_D:I = 0x44

.field public static final ACS_X11_Delete:I = 0xffff

.field public static final ACS_X11_Down:I = 0xff54

.field public static final ACS_X11_E:I = 0x45

.field public static final ACS_X11_ETH:I = 0xd0

.field public static final ACS_X11_Eacute:I = 0xc9

.field public static final ACS_X11_Ecircumflex:I = 0xca

.field public static final ACS_X11_Ediaeresis:I = 0xcb

.field public static final ACS_X11_Egrave:I = 0xc8

.field public static final ACS_X11_Eisu_Shift:I = 0xff2f

.field public static final ACS_X11_Eisu_toggle:I = 0xff30

.field public static final ACS_X11_End:I = 0xff57

.field public static final ACS_X11_Escape:I = 0xff1b

.field public static final ACS_X11_Eth:I = 0xd0

.field public static final ACS_X11_Execute:I = 0xff62

.field public static final ACS_X11_F:I = 0x46

.field public static final ACS_X11_F1:I = 0xffbe

.field public static final ACS_X11_F10:I = 0xffc7

.field public static final ACS_X11_F11:I = 0xffc8

.field public static final ACS_X11_F12:I = 0xffc9

.field public static final ACS_X11_F13:I = 0xffca

.field public static final ACS_X11_F14:I = 0xffcb

.field public static final ACS_X11_F15:I = 0xffcc

.field public static final ACS_X11_F16:I = 0xffcd

.field public static final ACS_X11_F17:I = 0xffce

.field public static final ACS_X11_F18:I = 0xffcf

.field public static final ACS_X11_F19:I = 0xffd0

.field public static final ACS_X11_F2:I = 0xffbf

.field public static final ACS_X11_F20:I = 0xffd1

.field public static final ACS_X11_F21:I = 0xffd2

.field public static final ACS_X11_F22:I = 0xffd3

.field public static final ACS_X11_F23:I = 0xffd4

.field public static final ACS_X11_F24:I = 0xffd5

.field public static final ACS_X11_F25:I = 0xffd6

.field public static final ACS_X11_F26:I = 0xffd7

.field public static final ACS_X11_F27:I = 0xffd8

.field public static final ACS_X11_F28:I = 0xffd9

.field public static final ACS_X11_F29:I = 0xffda

.field public static final ACS_X11_F3:I = 0xffc0

.field public static final ACS_X11_F30:I = 0xffdb

.field public static final ACS_X11_F31:I = 0xffdc

.field public static final ACS_X11_F32:I = 0xffdd

.field public static final ACS_X11_F33:I = 0xffde

.field public static final ACS_X11_F34:I = 0xffdf

.field public static final ACS_X11_F35:I = 0xffe0

.field public static final ACS_X11_F4:I = 0xffc1

.field public static final ACS_X11_F5:I = 0xffc2

.field public static final ACS_X11_F6:I = 0xffc3

.field public static final ACS_X11_F7:I = 0xffc4

.field public static final ACS_X11_F8:I = 0xffc5

.field public static final ACS_X11_F9:I = 0xffc6

.field public static final ACS_X11_Find:I = 0xff68

.field public static final ACS_X11_G:I = 0x47

.field public static final ACS_X11_H:I = 0x48

.field public static final ACS_X11_Hankaku:I = 0xff29

.field public static final ACS_X11_Help:I = 0xff6a

.field public static final ACS_X11_Henkan:I = 0xff23

.field public static final ACS_X11_Henkan_Mode:I = 0xff23

.field public static final ACS_X11_Hiragana:I = 0xff25

.field public static final ACS_X11_Hiragana_Katakana:I = 0xff27

.field public static final ACS_X11_Home:I = 0xff50

.field public static final ACS_X11_Hyper_L:I = 0xffed

.field public static final ACS_X11_Hyper_R:I = 0xffee

.field public static final ACS_X11_I:I = 0x49

.field public static final ACS_X11_Iacute:I = 0xcd

.field public static final ACS_X11_Icircumflex:I = 0xce

.field public static final ACS_X11_Idiaeresis:I = 0xcf

.field public static final ACS_X11_Igrave:I = 0xcc

.field public static final ACS_X11_Insert:I = 0xff63

.field public static final ACS_X11_J:I = 0x4a

.field public static final ACS_X11_K:I = 0x4b

.field public static final ACS_X11_KP_0:I = 0xffb0

.field public static final ACS_X11_KP_1:I = 0xffb1

.field public static final ACS_X11_KP_2:I = 0xffb2

.field public static final ACS_X11_KP_3:I = 0xffb3

.field public static final ACS_X11_KP_4:I = 0xffb4

.field public static final ACS_X11_KP_5:I = 0xffb5

.field public static final ACS_X11_KP_6:I = 0xffb6

.field public static final ACS_X11_KP_7:I = 0xffb7

.field public static final ACS_X11_KP_8:I = 0xffb8

.field public static final ACS_X11_KP_9:I = 0xffb9

.field public static final ACS_X11_KP_Add:I = 0xffab

.field public static final ACS_X11_KP_Begin:I = 0xff9d

.field public static final ACS_X11_KP_Decimal:I = 0xffae

.field public static final ACS_X11_KP_Delete:I = 0xff9f

.field public static final ACS_X11_KP_Divide:I = 0xffaf

.field public static final ACS_X11_KP_Down:I = 0xff99

.field public static final ACS_X11_KP_End:I = 0xff9c

.field public static final ACS_X11_KP_Enter:I = 0xff8d

.field public static final ACS_X11_KP_Equal:I = 0xffbd

.field public static final ACS_X11_KP_F1:I = 0xff91

.field public static final ACS_X11_KP_F2:I = 0xff92

.field public static final ACS_X11_KP_F3:I = 0xff93

.field public static final ACS_X11_KP_F4:I = 0xff94

.field public static final ACS_X11_KP_Home:I = 0xff95

.field public static final ACS_X11_KP_Insert:I = 0xff9e

.field public static final ACS_X11_KP_Left:I = 0xff96

.field public static final ACS_X11_KP_Multiply:I = 0xffaa

.field public static final ACS_X11_KP_Next:I = 0xff9b

.field public static final ACS_X11_KP_Page_Down:I = 0xff9b

.field public static final ACS_X11_KP_Page_Up:I = 0xff9a

.field public static final ACS_X11_KP_Prior:I = 0xff9a

.field public static final ACS_X11_KP_Right:I = 0xff98

.field public static final ACS_X11_KP_Separator:I = 0xffac

.field public static final ACS_X11_KP_Space:I = 0xff80

.field public static final ACS_X11_KP_Subtract:I = 0xffad

.field public static final ACS_X11_KP_Tab:I = 0xff89

.field public static final ACS_X11_KP_Up:I = 0xff97

.field public static final ACS_X11_Kana_Lock:I = 0xff2d

.field public static final ACS_X11_Kana_Shift:I = 0xff2e

.field public static final ACS_X11_Kanji:I = 0xff21

.field public static final ACS_X11_Kanji_Bangou:I = 0xff37

.field public static final ACS_X11_Katakana:I = 0xff26

.field public static final ACS_X11_L:I = 0x4c

.field public static final ACS_X11_L1:I = 0xffc8

.field public static final ACS_X11_L10:I = 0xffd1

.field public static final ACS_X11_L2:I = 0xffc9

.field public static final ACS_X11_L3:I = 0xffca

.field public static final ACS_X11_L4:I = 0xffcb

.field public static final ACS_X11_L5:I = 0xffcc

.field public static final ACS_X11_L6:I = 0xffcd

.field public static final ACS_X11_L7:I = 0xffce

.field public static final ACS_X11_L8:I = 0xffcf

.field public static final ACS_X11_L9:I = 0xffd0

.field public static final ACS_X11_Left:I = 0xff51

.field public static final ACS_X11_Linefeed:I = 0xff0a

.field public static final ACS_X11_M:I = 0x4d

.field public static final ACS_X11_Mae_Koho:I = 0xff3e

.field public static final ACS_X11_Massyo:I = 0xff2c

.field public static final ACS_X11_Menu:I = 0xff67

.field public static final ACS_X11_Meta_L:I = 0xffe7

.field public static final ACS_X11_Meta_R:I = 0xffe8

.field public static final ACS_X11_Mode_switch:I = 0xff7e

.field public static final ACS_X11_Muhenkan:I = 0xff22

.field public static final ACS_X11_Multi_key:I = 0xff20

.field public static final ACS_X11_MultipleCandidate:I = 0xff3d

.field public static final ACS_X11_N:I = 0x4e

.field public static final ACS_X11_Next:I = 0xff56

.field public static final ACS_X11_Ntilde:I = 0xd1

.field public static final ACS_X11_Num_Lock:I = 0xff7f

.field public static final ACS_X11_O:I = 0x4f

.field public static final ACS_X11_Oacute:I = 0xd3

.field public static final ACS_X11_Ocircumflex:I = 0xd4

.field public static final ACS_X11_Odiaeresis:I = 0xd6

.field public static final ACS_X11_Ograve:I = 0xd2

.field public static final ACS_X11_Ooblique:I = 0xd8

.field public static final ACS_X11_Oslash:I = 0xd8

.field public static final ACS_X11_Otilde:I = 0xd5

.field public static final ACS_X11_P:I = 0x50

.field public static final ACS_X11_Page_Down:I = 0xff56

.field public static final ACS_X11_Page_Up:I = 0xff55

.field public static final ACS_X11_Pause:I = 0xff13

.field public static final ACS_X11_PreviousCandidate:I = 0xff3e

.field public static final ACS_X11_Print:I = 0xff61

.field public static final ACS_X11_Prior:I = 0xff55

.field public static final ACS_X11_Q:I = 0x51

.field public static final ACS_X11_R:I = 0x52

.field public static final ACS_X11_R1:I = 0xffd2

.field public static final ACS_X11_R10:I = 0xffdb

.field public static final ACS_X11_R11:I = 0xffdc

.field public static final ACS_X11_R12:I = 0xffdd

.field public static final ACS_X11_R13:I = 0xffde

.field public static final ACS_X11_R14:I = 0xffdf

.field public static final ACS_X11_R15:I = 0xffe0

.field public static final ACS_X11_R2:I = 0xffd3

.field public static final ACS_X11_R3:I = 0xffd4

.field public static final ACS_X11_R4:I = 0xffd5

.field public static final ACS_X11_R5:I = 0xffd6

.field public static final ACS_X11_R6:I = 0xffd7

.field public static final ACS_X11_R7:I = 0xffd8

.field public static final ACS_X11_R8:I = 0xffd9

.field public static final ACS_X11_R9:I = 0xffda

.field public static final ACS_X11_Redo:I = 0xff66

.field public static final ACS_X11_Return:I = 0xff0d

.field public static final ACS_X11_Right:I = 0xff53

.field public static final ACS_X11_Romaji:I = 0xff24

.field public static final ACS_X11_S:I = 0x53

.field public static final ACS_X11_Scroll_Lock:I = 0xff14

.field public static final ACS_X11_Select:I = 0xff60

.field public static final ACS_X11_Shift_L:I = 0xffe1

.field public static final ACS_X11_Shift_Lock:I = 0xffe6

.field public static final ACS_X11_Shift_R:I = 0xffe2

.field public static final ACS_X11_SingleCandidate:I = 0xff3c

.field public static final ACS_X11_Super_L:I = 0xffeb

.field public static final ACS_X11_Super_R:I = 0xffec

.field public static final ACS_X11_Sys_Req:I = 0xff15

.field public static final ACS_X11_T:I = 0x54

.field public static final ACS_X11_THORN:I = 0xde

.field public static final ACS_X11_Tab:I = 0xff09

.field public static final ACS_X11_Thorn:I = 0xde

.field public static final ACS_X11_Touroku:I = 0xff2b

.field public static final ACS_X11_U:I = 0x55

.field public static final ACS_X11_Uacute:I = 0xda

.field public static final ACS_X11_Ucircumflex:I = 0xdb

.field public static final ACS_X11_Udiaeresis:I = 0xdc

.field public static final ACS_X11_Ugrave:I = 0xd9

.field public static final ACS_X11_Undo:I = 0xff65

.field public static final ACS_X11_Up:I = 0xff52

.field public static final ACS_X11_V:I = 0x56

.field public static final ACS_X11_VoidSymbol:I = 0xffffff

.field public static final ACS_X11_W:I = 0x57

.field public static final ACS_X11_X:I = 0x58

.field public static final ACS_X11_Y:I = 0x59

.field public static final ACS_X11_Yacute:I = 0xdd

.field public static final ACS_X11_Z:I = 0x5a

.field public static final ACS_X11_Zen_Koho:I = 0xff3d

.field public static final ACS_X11_Zenkaku:I = 0xff28

.field public static final ACS_X11_Zenkaku_Hankaku:I = 0xff2a

.field public static final ACS_X11_a:I = 0x61

.field public static final ACS_X11_aacute:I = 0xe1

.field public static final ACS_X11_acircumflex:I = 0xe2

.field public static final ACS_X11_acute:I = 0xb4

.field public static final ACS_X11_adiaeresis:I = 0xe4

.field public static final ACS_X11_ae:I = 0xe6

.field public static final ACS_X11_agrave:I = 0xe0

.field public static final ACS_X11_ampersand:I = 0x26

.field public static final ACS_X11_apostrophe:I = 0x27

.field public static final ACS_X11_aring:I = 0xe5

.field public static final ACS_X11_asciicircum:I = 0x5e

.field public static final ACS_X11_asciitilde:I = 0x7e

.field public static final ACS_X11_asterisk:I = 0x2a

.field public static final ACS_X11_at:I = 0x40

.field public static final ACS_X11_atilde:I = 0xe3

.field public static final ACS_X11_b:I = 0x62

.field public static final ACS_X11_backslash:I = 0x5c

.field public static final ACS_X11_bar:I = 0x7c

.field public static final ACS_X11_braceleft:I = 0x7b

.field public static final ACS_X11_braceright:I = 0x7d

.field public static final ACS_X11_bracketleft:I = 0x5b

.field public static final ACS_X11_bracketright:I = 0x5d

.field public static final ACS_X11_brokenbar:I = 0xa6

.field public static final ACS_X11_c:I = 0x63

.field public static final ACS_X11_ccedilla:I = 0xe7

.field public static final ACS_X11_cedilla:I = 0xb8

.field public static final ACS_X11_cent:I = 0xa2

.field public static final ACS_X11_colon:I = 0x3a

.field public static final ACS_X11_comma:I = 0x2c

.field public static final ACS_X11_copyright:I = 0xa9

.field public static final ACS_X11_currency:I = 0xa4

.field public static final ACS_X11_d:I = 0x64

.field public static final ACS_X11_degree:I = 0xb0

.field public static final ACS_X11_diaeresis:I = 0xa8

.field public static final ACS_X11_division:I = 0xf7

.field public static final ACS_X11_dollar:I = 0x24

.field public static final ACS_X11_e:I = 0x65

.field public static final ACS_X11_eacute:I = 0xe9

.field public static final ACS_X11_ecircumflex:I = 0xea

.field public static final ACS_X11_ediaeresis:I = 0xeb

.field public static final ACS_X11_egrave:I = 0xe8

.field public static final ACS_X11_equal:I = 0x3d

.field public static final ACS_X11_eth:I = 0xf0

.field public static final ACS_X11_exclam:I = 0x21

.field public static final ACS_X11_exclamdown:I = 0xa1

.field public static final ACS_X11_f:I = 0x66

.field public static final ACS_X11_g:I = 0x67

.field public static final ACS_X11_grave:I = 0x60

.field public static final ACS_X11_greater:I = 0x3e

.field public static final ACS_X11_guillemotleft:I = 0xab

.field public static final ACS_X11_guillemotright:I = 0xbb

.field public static final ACS_X11_h:I = 0x68

.field public static final ACS_X11_hyphen:I = 0xad

.field public static final ACS_X11_i:I = 0x69

.field public static final ACS_X11_iacute:I = 0xed

.field public static final ACS_X11_icircumflex:I = 0xee

.field public static final ACS_X11_idiaeresis:I = 0xef

.field public static final ACS_X11_igrave:I = 0xec

.field public static final ACS_X11_j:I = 0x6a

.field public static final ACS_X11_k:I = 0x6b

.field public static final ACS_X11_l:I = 0x6c

.field public static final ACS_X11_less:I = 0x3c

.field public static final ACS_X11_m:I = 0x6d

.field public static final ACS_X11_macron:I = 0xaf

.field public static final ACS_X11_masculine:I = 0xba

.field public static final ACS_X11_minus:I = 0x2d

.field public static final ACS_X11_mu:I = 0xb5

.field public static final ACS_X11_multiply:I = 0xd7

.field public static final ACS_X11_n:I = 0x6e

.field public static final ACS_X11_nobreakspace:I = 0xa0

.field public static final ACS_X11_notsign:I = 0xac

.field public static final ACS_X11_ntilde:I = 0xf1

.field public static final ACS_X11_numbersign:I = 0x23

.field public static final ACS_X11_o:I = 0x6f

.field public static final ACS_X11_oacute:I = 0xf3

.field public static final ACS_X11_ocircumflex:I = 0xf4

.field public static final ACS_X11_odiaeresis:I = 0xf6

.field public static final ACS_X11_ograve:I = 0xf2

.field public static final ACS_X11_onehalf:I = 0xbd

.field public static final ACS_X11_onequarter:I = 0xbc

.field public static final ACS_X11_onesuperior:I = 0xb9

.field public static final ACS_X11_ooblique:I = 0xf8

.field public static final ACS_X11_ordfeminine:I = 0xaa

.field public static final ACS_X11_oslash:I = 0xf8

.field public static final ACS_X11_otilde:I = 0xf5

.field public static final ACS_X11_p:I = 0x70

.field public static final ACS_X11_paragraph:I = 0xb6

.field public static final ACS_X11_parenleft:I = 0x28

.field public static final ACS_X11_parenright:I = 0x29

.field public static final ACS_X11_percent:I = 0x25

.field public static final ACS_X11_period:I = 0x2e

.field public static final ACS_X11_periodcentered:I = 0xb7

.field public static final ACS_X11_plus:I = 0x2b

.field public static final ACS_X11_plusminus:I = 0xb1

.field public static final ACS_X11_q:I = 0x71

.field public static final ACS_X11_question:I = 0x3f

.field public static final ACS_X11_questiondown:I = 0xbf

.field public static final ACS_X11_quotedbl:I = 0x22

.field public static final ACS_X11_quoteleft:I = 0x60

.field public static final ACS_X11_quoteright:I = 0x27

.field public static final ACS_X11_r:I = 0x72

.field public static final ACS_X11_registered:I = 0xae

.field public static final ACS_X11_s:I = 0x73

.field public static final ACS_X11_script_switch:I = 0xff7e

.field public static final ACS_X11_section:I = 0xa7

.field public static final ACS_X11_semicolon:I = 0x3b

.field public static final ACS_X11_slash:I = 0x2f

.field public static final ACS_X11_space:I = 0x20

.field public static final ACS_X11_ssharp:I = 0xdf

.field public static final ACS_X11_sterling:I = 0xa3

.field public static final ACS_X11_t:I = 0x74

.field public static final ACS_X11_thorn:I = 0xfe

.field public static final ACS_X11_threequarters:I = 0xbe

.field public static final ACS_X11_threesuperior:I = 0xb3

.field public static final ACS_X11_twosuperior:I = 0xb2

.field public static final ACS_X11_u:I = 0x75

.field public static final ACS_X11_uacute:I = 0xfa

.field public static final ACS_X11_ucircumflex:I = 0xfb

.field public static final ACS_X11_udiaeresis:I = 0xfc

.field public static final ACS_X11_ugrave:I = 0xf9

.field public static final ACS_X11_underscore:I = 0x5f

.field public static final ACS_X11_v:I = 0x76

.field public static final ACS_X11_w:I = 0x77

.field public static final ACS_X11_x:I = 0x78

.field public static final ACS_X11_y:I = 0x79

.field public static final ACS_X11_yacute:I = 0xfd

.field public static final ACS_X11_ydiaeresis:I = 0xff

.field public static final ACS_X11_yen:I = 0xa5

.field public static final ACS_X11_z:I = 0x7a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
