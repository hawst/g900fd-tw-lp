.class public Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;
.super Ljava/lang/Object;
.source "TmsDbInfo.java"


# instance fields
.field private mAppCertified:Z

.field private mAppID:I

.field private mCategory:I

.field private mCertifiedAppId:Ljava/lang/String;

.field private mCertifiedAppUUID:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mProperties:Ljava/lang/String;

.field private mSignature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCategory:I

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "category"    # I
    .param p3, "appCertified"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mPackageName:Ljava/lang/String;

    .line 32
    iput p2, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCategory:I

    .line 33
    iput-boolean p3, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mAppCertified:Z

    .line 34
    return-void
.end method


# virtual methods
.method public getAppID()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mAppID:I

    return v0
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCategory:I

    return v0
.end method

.method public getCertifiedAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCertifiedAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getCertifiedAppUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCertifiedAppUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getProperties()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mProperties:Ljava/lang/String;

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mSignature:Ljava/lang/String;

    return-object v0
.end method

.method public isAppCertified()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mAppCertified:Z

    return v0
.end method

.method public setAppCertified(Z)V
    .locals 0
    .param p1, "mAppCertified"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mAppCertified:Z

    .line 62
    return-void
.end method

.method public setCategory(I)V
    .locals 0
    .param p1, "category"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCategory:I

    .line 54
    return-void
.end method

.method public setCertifiedAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "certifiedAppId"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCertifiedAppId:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setCertifiedAppUUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "certifiedAppUUID"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mCertifiedAppUUID:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mPackageName:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setProperties(Ljava/lang/String;)V
    .locals 0
    .param p1, "properties"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mProperties:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setSignature(Ljava/lang/String;)V
    .locals 0
    .param p1, "signature"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TmsDbInfo;->mSignature:Ljava/lang/String;

    .line 94
    return-void
.end method
