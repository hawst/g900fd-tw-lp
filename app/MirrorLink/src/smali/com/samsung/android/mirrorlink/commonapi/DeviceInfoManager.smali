.class public Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
.super Lcom/mirrorlink/android/commonapi/IDeviceInfoManager$Stub;
.source "DeviceInfoManager.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_MAJOR_VERSION:I = 0x1

.field private static final DEFAULT_MINOR_VERSION:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "TMSDeviceInfoManager"


# instance fields
.field private final mClientListenerList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mClientProfileInfo:Landroid/os/Bundle;

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mDeviceInfoChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

.field private majorVersionValue:I

.field private minorVersionValue:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 3
    .param p1, "service"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/IDeviceInfoManager$Stub;-><init>()V

    .line 23
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mDeviceInfoChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 25
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 27
    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->majorVersionValue:I

    .line 29
    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->minorVersionValue:I

    .line 38
    const-string v0, "TMSDeviceInfoManager"

    const-string v1, "Enter Constructor DeviceInfoManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 40
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 41
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;-><init>(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager$DeviceInfoChangeListener;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mDeviceInfoChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->setDeviceManager(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;)V

    .line 45
    :cond_0
    const-string v0, "TMSDeviceInfoManager"

    const-string v1, "Exit Constructor DeviceInfoManager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->isClientInfoChanged(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    return-void
.end method

.method private isClientInfoChanged(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v0

    .line 198
    :cond_1
    const-string v1, "VERSION_MAJOR"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 199
    const-string v3, "VERSION_MAJOR"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 198
    if-ne v1, v2, :cond_0

    .line 201
    const-string v1, "VERSION_MINOR"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 202
    const-string v3, "VERSION_MINOR"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 201
    if-ne v1, v2, :cond_0

    .line 205
    const-string v1, "CLIENT_IDENTIFIER"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 207
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 208
    const-string v3, "CLIENT_IDENTIFIER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 208
    if-eqz v1, :cond_0

    .line 211
    const-string v1, "CLIENT_FRIENDLY_NAME"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 213
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 214
    const-string v3, "CLIENT_FRIENDLY_NAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 212
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 214
    if-eqz v1, :cond_0

    .line 217
    const-string v1, "CLIENT_MANUFACTURER"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 219
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 220
    const-string v3, "CLIENT_MANUFACTURER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 218
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 220
    if-eqz v1, :cond_0

    .line 223
    const-string v1, "CLIENT_MODEL_NAME"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 225
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 226
    const-string v3, "CLIENT_MODEL_NAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 226
    if-eqz v1, :cond_0

    .line 229
    const-string v1, "CLIENT_MODEL_NUMBER"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 231
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 232
    const-string v3, "CLIENT_MODEL_NUMBER"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 232
    if-eqz v1, :cond_0

    .line 236
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public getMajorVersionValue()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->majorVersionValue:I

    return v0
.end method

.method public getMinorVersionValue()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->minorVersionValue:I

    return v0
.end method

.method public getMirrorLinkClientInformation()Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 109
    const-string v2, "TMSDeviceInfoManager"

    const-string v3, "DeviceInfoManager.getMirrorLinkClientInformation()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 114
    .local v1, "infoNode":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 115
    invoke-static {v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v1

    .line 132
    if-eqz v1, :cond_6

    .line 133
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 134
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v3, "CLIENT_IDENTIFIER"

    .line 135
    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mClientID:Ljava/lang/String;

    .line 134
    :goto_0
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v3, "CLIENT_FRIENDLY_NAME"

    .line 138
    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mFriendlyName:Ljava/lang/String;

    .line 137
    :goto_1
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v3, "CLIENT_MANUFACTURER"

    .line 141
    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mManufacturer:Ljava/lang/String;

    .line 140
    :goto_2
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v3, "CLIENT_MODEL_NAME"

    .line 144
    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelName:Ljava/lang/String;

    .line 143
    :goto_3
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v3, "CLIENT_MODEL_NUMBER"

    .line 147
    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mModelNumber:Ljava/lang/String;

    .line 146
    :goto_4
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v0

    .line 151
    :goto_5
    return-object v2

    .line 117
    :cond_0
    const-string v2, "TMSDeviceInfoManager"

    .line 118
    const-string v3, "DeviceInfoManager:TMClientProfileService.getClientProfileFromId(0) is null: returning Default CLient value"

    .line 117
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 120
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v2, "CLIENT_IDENTIFIER"

    .line 121
    const-string v3, "Not Available"

    .line 120
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "CLIENT_FRIENDLY_NAME"

    .line 123
    const-string v3, "Not Available"

    .line 122
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v2, "CLIENT_MANUFACTURER"

    .line 125
    const-string v3, "Not Available"

    .line 124
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v2, "CLIENT_MODEL_NAME"

    .line 127
    const-string v3, "Not Available"

    .line 126
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v2, "CLIENT_MODEL_NUMBER"

    .line 129
    const-string v3, "Not Available"

    .line 128
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v0

    .line 130
    goto :goto_5

    .line 136
    :cond_1
    const-string v2, "Not Available"

    goto :goto_0

    .line 139
    :cond_2
    const-string v2, "Not Available"

    goto :goto_1

    .line 142
    :cond_3
    const-string v2, "Not Available"

    goto :goto_2

    .line 145
    :cond_4
    const-string v2, "Not Available"

    goto :goto_3

    .line 148
    :cond_5
    const-string v2, "Not Available"

    goto :goto_4

    .line 151
    :cond_6
    const/4 v2, 0x0

    goto :goto_5
.end method

.method public getMirrorLinkSessionVersionMajor()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    const-string v2, "TMSDeviceInfoManager"

    .line 66
    const-string v3, "DeviceInfoManager.getMirrorLinkSessionVersionMajor()"

    .line 65
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const/4 v1, -0x1

    .line 69
    .local v1, "ret":I
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v0

    .line 70
    .local v0, "receivedClientProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 72
    iget v1, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMajor:I

    .line 78
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 79
    :cond_0
    const/4 v1, 0x1

    .line 81
    :cond_1
    iput v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->majorVersionValue:I

    .line 82
    return v1

    .line 74
    :cond_2
    const-string v2, "TMSDeviceInfoManager"

    .line 75
    const-string v3, "DeviceInfoManager:TMClientProfileService.getClientProfileFromId(0) is null: returning Default Major version value"

    .line 74
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getMirrorLinkSessionVersionMinor()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    const-string v2, "TMSDeviceInfoManager"

    .line 88
    const-string v3, "DeviceInfoManager.getMirrorLinkConnectionVersionMinor()"

    .line 87
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v1, -0x1

    .line 90
    .local v1, "ret":I
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v0

    .line 91
    .local v0, "receivedClientProfile":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 93
    iget v1, v0, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mMirrorLinkVersionMinor:I

    .line 99
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 100
    :cond_0
    const/4 v1, 0x0

    .line 102
    :cond_1
    iput v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->minorVersionValue:I

    .line 103
    return v1

    .line 95
    :cond_2
    const-string v2, "TMSDeviceInfoManager"

    .line 96
    const-string v3, "DeviceInfoManager:TMClientProfileService.getClientProfileFromId(0) is null: returning Default Minor version value"

    .line 95
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServerVirtualKeyboardSupport()Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 157
    const-string v2, "TMSDeviceInfoManager"

    const-string v3, "DeviceInfoManager.getServerVirtualKeyboardSupport()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    if-nez v2, :cond_1

    .line 160
    :cond_0
    const-string v2, "TMSDeviceInfoManager"

    const-string v3, "DeviceInfoManager.getServerVirtualKeyboardSupport(): Engine is null . returning default"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v2, "AVAILABLE"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    const-string v2, "TOUCH_SUPPORT"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 163
    const-string v2, "KNOB_SUPPORT"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 164
    const-string v2, "DRIVE_MODE"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v1, v0

    .line 168
    .end local v0    # "b":Landroid/os/Bundle;
    .local v1, "b":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 167
    .end local v1    # "b":Ljava/lang/Object;
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getVirtualKeyboardSupport()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 168
    .restart local v1    # "b":Ljava/lang/Object;
    goto :goto_0
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 4
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    const/4 v3, 0x0

    .line 241
    if-eqz p1, :cond_0

    .line 242
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v1, :cond_1

    .line 243
    const-string v1, "TMSDeviceInfoManager"

    .line 244
    const-string v2, "DeviceInfoManager.handleEvent(): engine is set"

    .line 243
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getClientProfileService()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v0

    .line 246
    .local v0, "receivedClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    if-eqz v0, :cond_0

    .line 248
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mDeviceInfoChangeListener:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 247
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->registerDevMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    .line 278
    .end local v0    # "receivedClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v1, :cond_0

    .line 252
    const-string v1, "TMSDeviceInfoManager"

    .line 253
    const-string v2, "DeviceInfoManager.handleEvent(): engine is reset"

    .line 252
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientProfileInfo:Landroid/os/Bundle;

    .line 268
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getClientProfileService()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v0

    .line 269
    .restart local v0    # "receivedClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->unRegisterDevMgrCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V

    .line 271
    const-string v1, "TMSDeviceInfoManager"

    .line 272
    const-string v2, "DeviceInfoManager.notifyDeviceInfoChanged() with null value"

    .line 271
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0, v3}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->notifyDeviceInfoChanged(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public killListener()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "TMSDeviceInfoManager"

    .line 59
    const-string v1, "DeviceInfoManager.killListener() : Enter"

    .line 58
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 61
    return-void
.end method

.method public notifyDeviceInfoChanged(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 173
    const-string v3, "TMSDeviceInfoManager"

    .line 174
    const-string v4, "DeviceInfoManager.notifyDeviceInfoChanged() : Enter"

    .line 173
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 176
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 177
    .local v2, "length":I
    const-string v3, "TMSDeviceInfoManager"

    .line 178
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceInfoManager.notifyDeviceInfoChanged():length"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 179
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 178
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 177
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 188
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 190
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_0
    const-string v3, "TMSDeviceInfoManager"

    .line 191
    const-string v4, "DeviceInfoManager.notifyDeviceInfoChanged() : Exit "

    .line 190
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void

    .line 182
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;

    .line 183
    invoke-interface {v3, p1}, Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;->onDeviceInfoChanged(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;)V
    .locals 2
    .param p1, "packname"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;

    .prologue
    .line 49
    const-string v0, "TMSDeviceInfoManager"

    const-string v1, "DeviceInfoManager.registerCallBack()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p2, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 53
    :cond_0
    const-string v0, "TMSDeviceInfoManager"

    const-string v1, "DeviceInfoManager.registerCallBack() exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public unregister()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 283
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getAppPackageName()Ljava/lang/String;

    move-result-object v2

    .line 284
    .local v2, "packname":Ljava/lang/String;
    const-string v3, "TMSDeviceInfoManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DeviceInfoManger.UnRegister: PackName= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    if-nez v2, :cond_0

    .line 298
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v1

    .line 289
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_1

    .line 297
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 290
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 291
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 290
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 291
    if-eqz v3, :cond_2

    .line 292
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->mClientListenerList:Landroid/os/RemoteCallbackList;

    .line 293
    invoke-virtual {v3, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;

    .line 292
    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_2

    .line 289
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
