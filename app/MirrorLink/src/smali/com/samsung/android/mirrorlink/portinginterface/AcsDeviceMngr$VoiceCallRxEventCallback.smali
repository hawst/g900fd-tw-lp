.class Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;
.super Ljava/lang/Object;
.source "AcsDeviceMngr.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/portinginterface/AcsVoiceCallRx$EventCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VoiceCallRxEventCallback"
.end annotation


# instance fields
.field private mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0
    .param p1, "acsDeviceMngr"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .prologue
    .line 2671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2672
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 2673
    return-void
.end method


# virtual methods
.method public onEvent(IILjava/lang/Object;)V
    .locals 5
    .param p1, "event"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # Ljava/lang/Object;

    .prologue
    .line 2683
    packed-switch p1, :pswitch_data_0

    .line 2695
    const-string v2, "TMSDevMngr"

    const-string v3, "VoiceCallRxEventCallback unhandled"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2697
    :goto_0
    return-void

    .line 2687
    :pswitch_0
    move v1, p2

    .line 2688
    .local v1, "len":I
    const-string v2, "TMSDevMngr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "VoiceCallRxEventCallback EVENT_AUDIO_DATA "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2689
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2688
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p3

    .line 2690
    check-cast v0, [B

    .line 2691
    .local v0, "data":[B
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$VoiceCallRxEventCallback;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendCallAudioData([BI)V

    goto :goto_0

    .line 2683
    nop

    :pswitch_data_0
    .packed-switch 0x3ec
        :pswitch_0
    .end packed-switch
.end method
