.class final enum Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
.super Ljava/lang/Enum;
.source "TMApplicationServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "CertificationStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Certified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

.field public static final enum Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

.field public static final enum NotCertified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    const-string v1, "Certified"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Certified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    .line 116
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    const-string v1, "NotCertified"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->NotCertified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    .line 117
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    const-string v1, "Invalid"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    .line 113
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Certified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->NotCertified:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->Invalid:Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;->ENUM$VALUES:[Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService$CertificationStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
