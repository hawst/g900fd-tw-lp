.class public final Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$DevIdCertData;
.super Ljava/lang/Object;
.source "CertificateDbUtil.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/util/CertificateDbUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DevIdCertData"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final APP_ID:Ljava/lang/String; = "appId"

.field public static final CLIENT_IDS:Ljava/lang/String; = "clientIds"

.field public static final DEV_ID:Ljava/lang/String; = "devId"

.field public static final TABLE_NAME:Ljava/lang/String; = "appdevcertdata"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "appId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 70
    const-string v2, "devId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "clientIds"

    aput-object v2, v0, v1

    .line 69
    sput-object v0, Lcom/samsung/android/mirrorlink/util/CertificateDbUtil$DevIdCertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
