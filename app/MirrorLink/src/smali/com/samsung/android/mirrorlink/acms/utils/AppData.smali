.class public Lcom/samsung/android/mirrorlink/acms/utils/AppData;
.super Ljava/lang/Object;
.source "AppData.java"


# instance fields
.field private mAppCertEntities:Ljava/lang/String;

.field private mAppId:Ljava/lang/String;

.field private mBlackListedPlatform:Ljava/lang/String;

.field private mPlatformId:Ljava/lang/String;

.field private mRestricted:Ljava/lang/String;

.field private mRunTimeId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppCertEntities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mAppCertEntities:Ljava/lang/String;

    return-object v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getBlackListedPlatform()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mBlackListedPlatform:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mPlatformId:Ljava/lang/String;

    return-object v0
.end method

.method public getRestricted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mRestricted:Ljava/lang/String;

    return-object v0
.end method

.method public getRunTimeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mRunTimeId:Ljava/lang/String;

    return-object v0
.end method

.method public setAppCertEntities(Ljava/lang/String;)V
    .locals 0
    .param p1, "appCertEntities"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mAppCertEntities:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mAppId:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setBlackListedPlatform(Ljava/lang/String;)V
    .locals 0
    .param p1, "blackListedPlatform"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mBlackListedPlatform:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setPlatformId(Ljava/lang/String;)V
    .locals 0
    .param p1, "platformId"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mPlatformId:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setRestricted(Ljava/lang/String;)V
    .locals 0
    .param p1, "restricted"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mRestricted:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setRunTimeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "runTimeId"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/utils/AppData;->mRunTimeId:Ljava/lang/String;

    .line 71
    return-void
.end method
