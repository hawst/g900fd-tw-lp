.class Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;
.super Landroid/os/Handler;
.source "TMSAppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 1133
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1139
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1158
    const-string v1, "TMSAppMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "check message ! "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    :goto_0
    return-void

    .line 1143
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1145
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppNameList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$1(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;->OnAppStatusUpdate(Ljava/util/List;)V

    .line 1147
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$2(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Ljava/util/List;)V

    goto :goto_0

    .line 1152
    :pswitch_1
    const-string v1, "TMSAppMngr"

    const-string v2, "handleMessage inside SEND_APPLISTSTATUS"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "AppID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1154
    .local v0, "appIds":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$1;->this$0:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    # getter for: Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->mAppStatusEventsListener:Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->access$0(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/IAppStatusEventsListener;->OnAppListUpdate(Ljava/lang/String;)V

    goto :goto_0

    .line 1139
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
