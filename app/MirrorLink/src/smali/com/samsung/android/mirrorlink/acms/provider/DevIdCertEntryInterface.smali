.class public Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
.super Ljava/lang/Object;
.source "DevIdCertEntryInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface$DevIdCertData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AcmsDevIdCertEntryInterface"

.field private static sDevIdCertEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public static getDevIdCertEntryInterface(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const-class v1, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    monitor-enter v1

    .line 45
    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->sDevIdCertEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->sDevIdCertEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    .line 44
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    sget-object v0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->sDevIdCertEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 274
    const-class v1, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    monitor-enter v1

    .line 275
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->sDevIdCertEntryInterface:Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;

    .line 274
    monitor-exit v1

    .line 277
    return-void

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deleteCertificate(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z
    .locals 2
    .param p1, "devIdCertEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 89
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "deleteCertificate : context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x0

    .line 92
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->deleteCertificate(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public deleteCertificate(Ljava/lang/String;)Z
    .locals 7
    .param p1, "devId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 73
    const-string v2, "AcmsDevIdCertEntryInterface"

    const-string v3, "deleteCertificate : context is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :goto_0
    return v1

    .line 76
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 77
    sget-object v4, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 78
    const-string v5, "devId=?"

    new-array v6, v2, [Ljava/lang/String;

    aput-object p1, v6, v1

    .line 76
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 79
    .local v0, "rows":I
    if-nez v0, :cond_1

    .line 80
    const-string v2, "AcmsDevIdCertEntryInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "Entry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found in db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_1
    const-string v1, "AcmsDevIdCertEntryInterface"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " removed successfully from db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 84
    goto :goto_0
.end method

.method public getAllDevApps()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 228
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 229
    const-string v0, "AcmsDevIdCertEntryInterface"

    .line 230
    const-string v1, "getAppIdsFromDevId : context is null"

    .line 229
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :goto_0
    return-object v3

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 234
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 235
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "appId"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    .line 233
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 237
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 238
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "Error occured in query : getAppIdsFromDevId"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 243
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 244
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v6, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 246
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    .line 250
    goto :goto_0

    .line 252
    .end local v6    # "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getAppIdsFromDevId(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "devId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 198
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 199
    :cond_0
    const-string v0, "AcmsDevIdCertEntryInterface"

    .line 200
    const-string v1, "getAppIdsFromDevId : context is null or devId is null"

    .line 199
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :goto_0
    return-object v5

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 204
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 205
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "appId"

    aput-object v3, v2, v8

    .line 206
    const-string v3, "devId=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 203
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 207
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 208
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "Error occured in query : getAppIdsFromDevId"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 213
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 214
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 217
    .local v6, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const-string v0, "appId"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 216
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 219
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v5, v6

    .line 220
    goto :goto_0

    .line 222
    .end local v6    # "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public getDevIdforAppId(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 171
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "getDevIdforAppId : context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :goto_0
    return-object v5

    .line 174
    :cond_0
    const/4 v7, 0x0

    .line 176
    .local v7, "devId":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 177
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 178
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "devId"

    aput-object v3, v2, v8

    .line 179
    const-string v3, "appId=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    .line 176
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 181
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 182
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "getDevIdforAppId: Cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 185
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "No results for query : getDevIdforAppId"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 189
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 191
    const-string v0, "devId"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 190
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 193
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v7

    .line 194
    goto :goto_0
.end method

.method public insertCertificate(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z
    .locals 3
    .param p1, "devIdCertEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 54
    const-string v1, "AcmsDevIdCertEntryInterface"

    const-string v2, "insertCertificate : context is null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const/4 v1, 0x0

    .line 68
    :goto_0
    return v1

    .line 57
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 58
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "AcmsDevIdCertEntryInterface"

    const-string v2, "insertCertificate : enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->updateCertificateForAppId(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 60
    const-string v1, "AcmsDevIdCertEntryInterface"

    .line 61
    const-string v2, "insertCertificate : Certificate not present, hence inserting"

    .line 60
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v1, "appId"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "devId"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 65
    sget-object v2, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 64
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 67
    :cond_1
    const-string v1, "AcmsDevIdCertEntryInterface"

    const-string v2, "insertCertificate : exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateCertificateForAppId(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z
    .locals 11
    .param p1, "devIdCertEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 132
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 133
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificateForAppId : context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 166
    :goto_0
    return v0

    .line 136
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 137
    .local v8, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 138
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 139
    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "appId"

    aput-object v3, v2, v9

    .line 140
    const-string v3, "appId=?"

    .line 141
    new-array v4, v10, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getAppId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x0

    .line 137
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 142
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 143
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificateForAppId : cursor is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 144
    goto :goto_0

    .line 147
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 148
    const-string v0, "AcmsDevIdCertEntryInterface"

    .line 149
    const-string v1, "updateCertificate : Certificate already present, hence updating"

    .line 148
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "devId"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v0, "clientIds"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getClientIds()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 155
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 156
    const-string v2, "appId=?"

    .line 157
    new-array v3, v10, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getAppId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    .line 154
    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 158
    .local v7, "updated":I
    const-string v0, "AcmsDevIdCertEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "row : appId ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 159
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 166
    goto/16 :goto_0

    .line 161
    .end local v7    # "updated":I
    :cond_2
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificate: Given appId not found!!"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 163
    goto/16 :goto_0
.end method

.method public updateCertificateForDevId(Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;)Z
    .locals 11
    .param p1, "devIdCertEntry"    # Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 97
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificateForDevId : context is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 128
    :goto_0
    return v0

    .line 100
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 101
    .local v8, "values":Landroid/content/ContentValues;
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 103
    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "devId"

    aput-object v3, v2, v9

    .line 104
    const-string v3, "devId=?"

    .line 105
    new-array v4, v10, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x0

    .line 101
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 106
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 107
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificateForDevId: Cursor is NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 108
    goto :goto_0

    .line 110
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 111
    const-string v0, "AcmsDevIdCertEntryInterface"

    .line 112
    const-string v1, "updateCertificate : Certificate already present, hence updating"

    .line 111
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v0, "clientIds"

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getClientIds()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntryInterface;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 117
    sget-object v1, Lcom/samsung/android/mirrorlink/acms/provider/AcmsDbProvider;->CONTENT_URI_OF_DEV_ID_CERT:Landroid/net/Uri;

    .line 118
    const-string v2, "devId=?"

    .line 119
    new-array v3, v10, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    .line 116
    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 120
    .local v7, "updated":I
    const-string v0, "AcmsDevIdCertEntryInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rows : devID ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 121
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/acms/provider/DevIdCertEntry;->getDevId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 128
    goto :goto_0

    .line 123
    .end local v7    # "updated":I
    :cond_2
    const-string v0, "AcmsDevIdCertEntryInterface"

    const-string v1, "updateCertificate: Given devID not found!!"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/acms/utils/AcmsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 125
    goto/16 :goto_0
.end method
