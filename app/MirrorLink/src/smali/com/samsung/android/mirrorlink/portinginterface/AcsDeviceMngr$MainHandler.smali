.class Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;
.super Landroid/os/Handler;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

.field private mListner:Landroid/location/LocationListener;

.field private mTm:Landroid/telephony/TelephonyManager;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Looper;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "devMgr"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .prologue
    .line 2415
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 2416
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2417
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.MainHandler() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2418
    iput-object p3, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 2419
    new-instance v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler$1;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mListner:Landroid/location/LocationListener;

    .line 2442
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.MainHandler() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2450
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MainHandler.handleMessage() - Enter - msg.what:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2451
    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2450
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2603
    :pswitch_0
    const-string v0, "TMSDevMngr"

    .line 2604
    const-string v1, "MainHandler.handleMessage() - Unknown message type"

    .line 2603
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2606
    :cond_0
    :goto_0
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607
    return-void

    .line 2456
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    .line 2457
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 2456
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mTm:Landroid/telephony/TelephonyManager;

    .line 2458
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    new-instance v1, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-direct {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$9(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;)V

    .line 2459
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$10(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    move-result-object v1

    .line 2460
    const/16 v2, 0x20

    .line 2459
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_0

    .line 2464
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mTm:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$10(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2466
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPhoneStateListner:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$10(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;

    move-result-object v1

    .line 2467
    const/4 v2, 0x0

    .line 2466
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2469
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mTm:Landroid/telephony/TelephonyManager;

    .line 2470
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$9(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;)V

    goto :goto_0

    .line 2474
    :pswitch_3
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - EVENT_TM_REGISTER_GPS_STATE_CHANGE:"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mListner:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    goto :goto_0

    .line 2488
    :pswitch_4
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - EVENT_TM_UNREGISTER_GPS_STATE_CHANGE:"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2489
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mListner:Landroid/location/LocationListener;

    if-eqz v0, :cond_0

    .line 2490
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mListner:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    goto/16 :goto_0

    .line 2494
    :pswitch_5
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - EVENT_INIT_GPS_SERVICE:"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2497
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - mGpsService is not null:"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->initGps()V

    .line 2500
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startGpsService()V

    goto/16 :goto_0

    .line 2505
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2507
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2508
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->setDeviceMngr(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    .line 2509
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->startGps()Z

    .line 2510
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2511
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeTestProvider(Ljava/lang/String;)V

    .line 2513
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v0 .. v10}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 2514
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2520
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2522
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2523
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->stopGps()Z

    .line 2524
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->deInitGpsService()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$13(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    .line 2525
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2526
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopClientGpsSource()V

    goto/16 :goto_0

    .line 2531
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2533
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->deInitGps()V

    goto/16 :goto_0

    .line 2538
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2540
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mGpsService:Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$12(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsGpsService;->setMinTime(J)V

    goto/16 :goto_0

    .line 2547
    :pswitch_a
    const-string v0, "TMSDevMngr"

    const-string v1, "MainHandler.handleMessage() - EVENT_INIT_GPS_SERVICE:"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2549
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2551
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->initLocationSink()V

    .line 2552
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->startLocationService()V

    goto/16 :goto_0

    .line 2561
    :pswitch_b
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2563
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2564
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->setDeviceMngr(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    .line 2565
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->startLocationSink()Z

    .line 2566
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2567
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeTestProvider(Ljava/lang/String;)V

    .line 2569
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x5

    invoke-virtual/range {v0 .. v10}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 2570
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocation:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$11(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "CARGPS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2576
    :pswitch_c
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2578
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2579
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->stopLocationSink()Z

    .line 2580
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->mDevMgr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->deInitLocationService()V
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$15(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V

    .line 2581
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2582
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->stopClientLocationService()V

    goto/16 :goto_0

    .line 2587
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2589
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->deInitLocationSink()V

    goto/16 :goto_0

    .line 2594
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2596
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mLocationSink:Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$14(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLocationSink;->setMinTime(J)V

    goto/16 :goto_0

    .line 2452
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
