.class Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;
.super Ljava/lang/Object;
.source "BtAppholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelfMacAddressStorage"
.end annotation


# static fields
.field private static final FILENAME:Ljava/lang/String; = "BluetoothMac.txt"


# instance fields
.field private mCxt:Landroid/content/Context;

.field private mFile:Ljava/io/File;

.field private mMacAddress:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "cntxt"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 709
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    .line 712
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    .line 715
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.SelfMacAddressStorage enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mCxt:Landroid/content/Context;

    .line 718
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->readMacFmStorage()V

    .line 720
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.SelfMacAddressStorage exit "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 758
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->writeMacToStorage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 785
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createStorage()V
    .locals 5

    .prologue
    .line 745
    const/4 v1, 0x0

    .line 748
    .local v1, "ret":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 755
    :goto_0
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SelfMacAddressStorage.createStorage ret "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    return-void

    .line 750
    :catch_0
    move-exception v0

    .line 752
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 753
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SelfMacAddressStorage.createStorage IOException "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMacAddress()Ljava/lang/String;
    .locals 3

    .prologue
    .line 787
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelfMacAddressStorage.getMacAddress "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method private readFmFile()V
    .locals 9

    .prologue
    .line 824
    const/4 v3, 0x0

    .line 828
    .local v3, "fin":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 829
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v4, "fin":Ljava/io/FileInputStream;
    :try_start_1
    const-string v6, "TMSBtAppholder"

    const-string v7, "SelfMacAddressStorage.readFmFile open Success"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const/16 v6, 0x400

    new-array v1, v6, [B

    .line 832
    .local v1, "b":[B
    const/4 v6, 0x0

    const/16 v7, 0x11

    invoke-virtual {v4, v1, v6, v7}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    .line 833
    .local v5, "len":I
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, " total bytes read from file is : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    new-instance v0, Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0x11

    const-string v8, "UTF-8"

    invoke-direct {v0, v1, v6, v7, v8}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 835
    .local v0, "address":Ljava/lang/String;
    invoke-static {v0}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 837
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    .line 839
    :cond_0
    const-string v6, "TMSBtAppholder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SelfMacAddressStorage.readFmFile Done"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 847
    if-eqz v4, :cond_3

    .line 851
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v3, v4

    .line 859
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "b":[B
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "len":I
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-void

    .line 841
    :catch_0
    move-exception v2

    .line 843
    .local v2, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 847
    if-eqz v3, :cond_1

    .line 851
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 853
    :catch_1
    move-exception v2

    .line 855
    const-string v6, "TMSBtAppholder"

    const-string v7, "SelfMacAddressStorage.readFmFile Exception"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 846
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 847
    :goto_2
    if-eqz v3, :cond_2

    .line 851
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 858
    :cond_2
    :goto_3
    throw v6

    .line 853
    :catch_2
    move-exception v2

    .line 855
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "TMSBtAppholder"

    const-string v8, "SelfMacAddressStorage.readFmFile Exception"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 853
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v0    # "address":Ljava/lang/String;
    .restart local v1    # "b":[B
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "len":I
    :catch_3
    move-exception v2

    .line 855
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "TMSBtAppholder"

    const-string v7, "SelfMacAddressStorage.readFmFile Exception"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_0

    .line 846
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "b":[B
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "len":I
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_2

    .line 841
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private readMacFmStorage()V
    .locals 3

    .prologue
    .line 725
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.init enter "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mCxt:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BluetoothMac.txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    .line 728
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 730
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.init file already exist "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->readFmFile()V

    .line 739
    :goto_0
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelfMacAddressStorage.init exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    return-void

    .line 735
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->createStorage()V

    .line 736
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.init No File "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private writeMacToStorage(Ljava/lang/String;)V
    .locals 3
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 760
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelfMacAddressStorage.writeMacToStorage enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    if-nez p1, :cond_0

    .line 764
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.writeMacToStorage wrong mac"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    :goto_0
    return-void

    .line 768
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    .line 770
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mCxt:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "BluetoothMac.txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    .line 771
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 773
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.writeMacToStorage file already exist "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->writeToFile()V

    .line 781
    const-string v0, "TMSBtAppholder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SelfMacAddressStorage.writeMacToStorage exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 778
    :cond_1
    const-string v0, "TMSBtAppholder"

    const-string v1, "SelfMacAddressStorage.writeMacToStorage No File Error"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private writeToFile()V
    .locals 6

    .prologue
    .line 793
    const/4 v1, 0x0

    .line 796
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    const-string v3, "TMSBtAppholder"

    const-string v4, "SelfMacAddressStorage.writeToFile Stream Obtained"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$SelfMacAddressStorage;->mMacAddress:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 799
    const-string v3, "TMSBtAppholder"

    const-string v4, "SelfMacAddressStorage.writeToFile Write Done"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 808
    if-eqz v2, :cond_2

    .line 812
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v1, v2

    .line 820
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 801
    :catch_0
    move-exception v0

    .line 803
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "TMSBtAppholder"

    const-string v4, "SelfMacAddressStorage.writeToFile Exception"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 808
    if-eqz v1, :cond_0

    .line 812
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 814
    :catch_1
    move-exception v0

    .line 816
    const-string v3, "TMSBtAppholder"

    const-string v4, "SelfMacAddressStorage.writeToFile Exception"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 807
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 808
    :goto_2
    if-eqz v1, :cond_1

    .line 812
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 819
    :cond_1
    :goto_3
    throw v3

    .line 814
    :catch_2
    move-exception v0

    .line 816
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "TMSBtAppholder"

    const-string v5, "SelfMacAddressStorage.writeToFile Exception"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 814
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    .line 816
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v3, "TMSBtAppholder"

    const-string v4, "SelfMacAddressStorage.writeToFile Exception"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 807
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 801
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
