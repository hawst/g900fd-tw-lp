.class Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;
.super Landroid/os/Handler;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TmsEngNativeEvtHandler"
.end annotation


# static fields
.field private static final DISMISS_BLACK_SCREEN:Ljava/lang/String; = "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"


# instance fields
.field private mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Lcom/samsung/android/mirrorlink/engine/TmsEngine;Landroid/os/Looper;)V
    .locals 2
    .param p2, "tmsEng"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 909
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 910
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 911
    const-string v0, "TMSEngine"

    .line 912
    const-string v1, "TmsEngNativeEvtHandler.TmsEngNativeEvtHandler() - Enter"

    .line 911
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 914
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngNativeEvtHandler.TmsEngNativeEvtHandler() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v13, 0x65

    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 922
    const-string v6, "TMSEngine"

    .line 923
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TmsEngNativeEvtHandler.handleMessage() - Enter - msg.what:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 924
    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 923
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 922
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const/4 v4, 0x0

    .line 926
    .local v4, "shutdownIntent":Landroid/content/Intent;
    iget v6, p1, Landroid/os/Message;->what:I

    sparse-switch v6, :sswitch_data_0

    .line 1141
    const-string v6, "TMSEngine"

    .line 1142
    const-string v7, "TmsEngNativeEvtHandler.handleMessage() - Unknown message type"

    .line 1141
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    :cond_0
    :goto_0
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() - Exit"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1146
    :cond_1
    :goto_1
    return-void

    .line 928
    :sswitch_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    if-eq v6, v13, :cond_2

    .line 929
    const-string v6, "TMSEngine"

    const-string v7, "failureEventsFromHandler=> state is not proper. Skipping NATIVE_EVT_INIT_SUCCESS"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 932
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 933
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS INIT SUCCCESS"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 934
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 949
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    if-nez v6, :cond_3

    .line 951
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() - mTmsEng is NULL"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() - send destroy intent, "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 956
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.mAppStateHandler!=null - send destroy message, "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 958
    .local v3, "msg1":Landroid/os/Message;
    iput v11, v3, Landroid/os/Message;->what:I

    .line 959
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 963
    .end local v3    # "msg1":Landroid/os/Message;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->start()I

    goto :goto_0

    .line 967
    :sswitch_1
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x68

    if-eq v6, v7, :cond_4

    .line 968
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_START_SUCCESS"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 971
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 972
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS START SUCCCESS"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 973
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 975
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$6(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getVncServerPort()I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$7(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setVncParams(Ljava/lang/String;I)V

    .line 980
    :cond_5
    new-instance v2, Landroid/content/Intent;

    const-string v6, "samsung.intent.action.MIRRORLINK_BOOTED"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 981
    .local v2, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 982
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 984
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngine.handleMessage(): mTMSServrDevc is not null "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getRegisterNetwork()Lcom/samsung/net/HostInterface$NWK_TYPE;
    invoke-static {v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$9(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/net/HostInterface$NWK_TYPE;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->start(Lcom/samsung/net/HostInterface$NWK_TYPE;)Z

    .line 987
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->initStateVars()V

    .line 991
    :cond_6
    invoke-static {v12}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$10(Z)V

    .line 992
    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z
    invoke-static {}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$11()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 993
    const-string v6, "TMSEngine"

    const-string v7, "Deinit is in pending state. Calling deinit"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->deinit()I

    .line 995
    invoke-static {v12}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$12(Z)V

    goto/16 :goto_0

    .line 1001
    .end local v2    # "intent":Landroid/content/Intent;
    :sswitch_2
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x6b

    if-eq v6, v7, :cond_7

    .line 1002
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_STOP_SUCCESS"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1005
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1006
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS STOP SUCCCESS"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1007
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$13(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1008
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1009
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v10}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$14(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1010
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_deinit()I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$15(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    goto/16 :goto_0

    .line 1015
    :sswitch_3
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x6e

    if-eq v6, v7, :cond_8

    .line 1016
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_DEINIT_SUCCESS"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1019
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1020
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS DEINIT SUCCCESS"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1023
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_DEINIT_SUCCESS "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    const-wide/16 v6, 0xa

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1029
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_release()I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$16(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    .line 1033
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 1034
    const-string v6, "TMSEngine"

    .line 1035
    const-string v7, "TmsEngNativeEvtHandler.mAppStateHandler!=null - send destroy message, "

    .line 1034
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 1037
    .restart local v3    # "msg1":Landroid/os/Message;
    iput v11, v3, Landroid/os/Message;->what:I

    .line 1038
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1043
    .end local v3    # "msg1":Landroid/os/Message;
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-static {v6, v9}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$17(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Z)V

    .line 1044
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$18(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Looper;->quit()V

    .line 1045
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$19(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;)V

    goto/16 :goto_0

    .line 1026
    :catch_0
    move-exception v1

    .line 1027
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.sleep exception"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1056
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :sswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1059
    .local v0, "appid":I
    const/4 v6, -0x1

    if-ne v0, v6, :cond_b

    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 1060
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 1061
    .local v5, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    if-eq v6, v10, :cond_a

    .line 1062
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    if-ne v6, v9, :cond_b

    .line 1063
    :cond_a
    const-string v6, "TMSEngine"

    const-string v7, "there is no way to handle this case in call state. so we ignore this VNC block event with this case - request to block by native"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1068
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "VNC FRAMEBUFFER BLOCKED"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1069
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$20(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1070
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$20(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncFrameBufferBlocked(I)V

    goto/16 :goto_0

    .line 1074
    .end local v0    # "appid":I
    :sswitch_5
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_TERMINATE_TOP_APP "

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$20(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1076
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->mTmsEng:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$20(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncTerminateTopApp()V

    goto/16 :goto_0

    .line 1080
    :sswitch_6
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    if-eq v6, v13, :cond_c

    .line 1081
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_INIT_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1084
    :cond_c
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1085
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS INIT FAILURE"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1086
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_INIT_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->performDeinit()V
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$21(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    goto/16 :goto_0

    .line 1091
    :sswitch_7
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x6e

    if-eq v6, v7, :cond_d

    .line 1092
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_DEINIT_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1095
    :cond_d
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1096
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS DEINIT FAILURE"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1097
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_DEINIT_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_release()I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$16(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    .line 1100
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 1101
    const-string v6, "TMSEngine"

    .line 1102
    const-string v7, "TmsEngNativeEvtHandler.mAppStateHandler!=null - send destroy message, "

    .line 1101
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 1104
    .restart local v3    # "msg1":Landroid/os/Message;
    iput v11, v3, Landroid/os/Message;->what:I

    .line 1105
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1109
    .end local v3    # "msg1":Landroid/os/Message;
    :cond_e
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    invoke-static {v6, v9}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$17(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Z)V

    .line 1110
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$18(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Looper;->quit()V

    .line 1111
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$19(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;)V

    goto/16 :goto_0

    .line 1115
    :sswitch_8
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x68

    if-eq v6, v7, :cond_f

    .line 1116
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_START_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1119
    :cond_f
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1120
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS START FAILURE"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1121
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_START_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->performDeinit()V
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$21(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    goto/16 :goto_0

    .line 1126
    :sswitch_9
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    move-result v6

    const/16 v7, 0x6b

    if-eq v6, v7, :cond_10

    .line 1127
    const-string v6, "TMSEngine"

    const-string v7, "stopExpiryTimerForSuccessEvent=> state is not proper. Skipping NATIVE_EVT_STOP_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1130
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    iget v7, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1131
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    const-string v7, "TMS STOP FAILURE"

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V

    .line 1132
    const-string v6, "TMSEngine"

    const-string v7, "TmsEngNativeEvtHandler.handleMessage() -NATIVE_EVT_STOP_FAILURE"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$13(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1134
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1135
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V
    invoke-static {v6, v10}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$14(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V

    .line 1136
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_deinit()I
    invoke-static {v6}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->access$15(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I

    goto/16 :goto_0

    .line 926
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_3
        0x9 -> :sswitch_2
        0xa -> :sswitch_6
        0xb -> :sswitch_8
        0xc -> :sswitch_7
        0xd -> :sswitch_9
        0x3e9 -> :sswitch_4
        0x3ea -> :sswitch_5
    .end sparse-switch
.end method
