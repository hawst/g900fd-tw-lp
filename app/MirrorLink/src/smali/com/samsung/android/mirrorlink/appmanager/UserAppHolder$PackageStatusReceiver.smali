.class Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UserAppHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;)V
    .locals 0

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;-><init>(Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 646
    const-string v4, "TMSUserAppHolder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " PackageStatusReceiver.onreceive enter "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 647
    invoke-virtual {p2}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 646
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 649
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 651
    const-string v4, "TMSUserAppHolder"

    .line 652
    const-string v5, " PackageStatusReceiver.onreceive: invalid actions "

    .line 651
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    :goto_0
    return-void

    .line 655
    :cond_0
    const-string v4, "TMSUserAppHolder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " PackageStatusReceiver.onreceive action "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 656
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 655
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 658
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 659
    const-string v4, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 660
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 662
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 663
    .local v3, "pkgName":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 664
    .local v2, "msgbundle":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    iget-object v4, v4, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 665
    .local v1, "msg":Landroid/os/Message;
    const-string v4, "PackageNames"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string v4, "State"

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 668
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder$PackageStatusReceiver;->this$0:Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;

    iget-object v4, v4, Lcom/samsung/android/mirrorlink/appmanager/UserAppHolder;->mHandler:Landroid/os/Handler;

    const-wide/16 v5, 0x1388

    invoke-virtual {v4, v1, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 670
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "msgbundle":Landroid/os/Bundle;
    .end local v3    # "pkgName":Ljava/lang/String;
    :cond_2
    const-string v4, "TMSUserAppHolder"

    const-string v5, " PackageStatusReceiver.onreceive exit "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
