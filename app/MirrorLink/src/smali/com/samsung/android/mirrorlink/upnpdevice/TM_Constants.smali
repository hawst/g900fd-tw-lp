.class public final Lcom/samsung/android/mirrorlink/upnpdevice/TM_Constants;
.super Ljava/lang/Object;
.source "TM_Constants.java"


# static fields
.field public static final ACMS_KEYSTORE_NAME:Ljava/lang/String; = "Acms_Keystore"

.field public static final ACTION_GET_APPLICATION_CERTIFICATION_INFO:Ljava/lang/String; = "GetApplicationCertificateInfo"

.field public static final ACTION_GET_APPLICATION_STATUS:Ljava/lang/String; = "GetApplicationStatus"

.field public static final ACTION_GET_APPLIST:Ljava/lang/String; = "GetApplicationList"

.field public static final ACTION_GET_APP_CERTIFICATION_STATUS:Ljava/lang/String; = "GetAppCertificationStatus"

.field public static final ACTION_GET_CERTIFIED_APPLICATION_LIST:Ljava/lang/String; = "GetCertifiedApplicationsList"

.field public static final ACTION_GET_CLIENT_PROFILE:Ljava/lang/String; = "GetClientProfile"

.field public static final ACTION_GET_MAX_NUM_PROFILES:Ljava/lang/String; = "GetMaxNumProfiles"

.field public static final ACTION_GET_NOTIFICATION:Ljava/lang/String; = "GetNotification"

.field public static final ACTION_GET_SUPPORTED_APP:Ljava/lang/String; = "GetSupportedApplications"

.field public static final ACTION_INVOKE_NOTI:Ljava/lang/String; = "InvokeNotiAction"

.field public static final ACTION_LAUNCH_APPLCATION:Ljava/lang/String; = "LaunchApplication"

.field public static final ACTION_SET_ALLOWED_APP:Ljava/lang/String; = "SetAllowedApplications"

.field public static final ACTION_SET_CLIENT_PROFILE:Ljava/lang/String; = "SetClientProfile"

.field public static final ACTION_TERMINATE_APPLCATION:Ljava/lang/String; = "TerminateApplication"

.field public static final ACTIVITY_STATE_CHANGED_EVENT:I = 0x64

.field public static final APPICON_DEPTH:I = 0x20

.field public static final APPICON_HEIGHT:I = 0x78

.field public static final APPICON_MIME:Ljava/lang/String; = "image/png"

.field public static final APPICON_WIDTH:I = 0x78

.field public static final APPLICATION_SERVER1:Ljava/lang/String; = "urn:schemas-upnp-org:service:TmApplicationServer:1"

.field public static final APP_LIST_TAG:Ljava/lang/String; = "appList"

.field public static final APP_NAME_STRING:Ljava/lang/String; = "AppName"

.field public static final BASE_APPID:I = 0x3e7

.field public static final BLUETOOTH_MACADDR:Ljava/lang/String; = "bdAddr"

.field public static final BT_STARTCONNECTION:Ljava/lang/String; = "startConnection"

.field public static final CARMODE_APP_NAME:Ljava/lang/String; = "com.sec.android.app.automotive.drivelink.jmini"

.field public static final CERTIFICATION_TAG:Ljava/lang/String; = "certification"

.field public static final CHARSET_UTF_8:Ljava/lang/String; = "UTF-8"

.field public static final CLIENT_PROFILE1:Ljava/lang/String; = "urn:schemas-upnp-org:service:TmClientProfile:1"

.field public static final CONTACTS_APP_NAME:Ljava/lang/String; = "com.android.contacts"

.field public static final CONTACT_STARTACTIVITY_GB:Ljava/lang/String; = "com.sec.android.app.contacts.ContactsEntryActivity"

.field public static final CONTACT_STARTACTIVITY_ICS:Ljava/lang/String; = "com.android.contacts.activities.PeopleActivity"

.field public static final DEFAULT_CATEGORY:I = 0x0

.field public static final FRIENDLY_NAME:Ljava/lang/String; = "SAMSUNG"

.field public static final HTTP_PORT:I = 0xfa4

.field public static final KEYSTORE_INSTANCE:Ljava/lang/String; = "PKCS12"

.field public static final KEYSTORE_PWD:Ljava/lang/String; = "8Q!@hs1#{1:2c$h"

.field public static final MANUFACTURE_NAME:Ljava/lang/String; = "SAMSUNGMOBILES"

.field public static final MAX_NUM_PROFILES:I = 0x1

.field public static final MMS_APP_NAME:Ljava/lang/String; = "com.android.mms"

.field public static final MODEL_DESCRIPTION:Ljava/lang/String; = "SAMSUNG terminal mode description , V1.0 supported"

.field public static final MODEL_NAME:Ljava/lang/String; = "SAMSUNG TM V1.0"

.field public static final MUSIC_APP_NAME:Ljava/lang/String; = "com.android.music"

.field public static final NOTIACTION_ACCEPTCALL:Ljava/lang/String; = "AcceptCall"

.field public static final NOTIACTION_READMSG:Ljava/lang/String; = "ReadMsg"

.field public static final NOTIACTION_REJECTCALL:Ljava/lang/String; = "RejectCall"

.field public static final NOTIACTION_SILENCECALL:Ljava/lang/String; = "SilenceCall"

.field public static final NO_OF_MIN_NOTI_ACTIONS:I = 0x2

.field public static final PHONE_APP_NAME:Ljava/lang/String; = "com.android.phone"

.field public static final POLICY:Ljava/lang/String; = "policy"

.field public static final POLICY_PREFERENCE:Ljava/lang/String; = "PreferencePolicy"

.field public static final SCM_BOOTED_INTENT:Ljava/lang/String; = "samsung.intent.action.MIRRORLINK_BOOTED"

.field public static final SCM_BOOT_COMPLETE_INTENT:Ljava/lang/String; = "samsung.intent.action.MIRRORLINK_BOOT_COMPLETED"

.field public static final SCM_MIRRORING_STARTED:Ljava/lang/String; = "samsung.intent.action.MIRRORING_STARTED"

.field public static final SCM_SHUTDOWN_INTENT:Ljava/lang/String; = "samsung.intent.action.MIRRORLINK_SHUTDOWN"

.field public static final START_APP_INTENT:Ljava/lang/String; = "StartApp"

.field public static final TMSERVER_APP_NAME:Ljava/lang/String; = "com.samsung.android.app.mirrorlink"

.field public static final UPNPNOTIFICATION_SERVER1:Ljava/lang/String; = "urn:schemas-upnp-org:service:TmNotificationServer:1"

.field public static final WIFI_SERVICE:Ljava/lang/String; = "wifi"

.field public static final X_SIGNATURE_TAG:Ljava/lang/String; = "X_Signature"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
