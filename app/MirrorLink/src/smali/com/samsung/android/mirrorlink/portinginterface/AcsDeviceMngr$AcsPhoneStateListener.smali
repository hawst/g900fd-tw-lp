.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AcsPhoneStateListener"
.end annotation


# instance fields
.field private mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
    .locals 0
    .param p1, "devMgr"    # Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .prologue
    .line 2649
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 2650
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 2651
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 2655
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AcsPhoneStateListener:onCallStateChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2656
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2655
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2657
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsPhoneStateListener;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->onCallStateChanged(ILjava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$16(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;ILjava/lang/String;)V

    .line 2659
    return-void
.end method
