.class Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler$1;
.super Ljava/lang/Object;
.source "AcsDeviceMngr.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;-><init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Looper;Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler$1;->this$1:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$MainHandler;

    .line 2419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 0
    .param p1, "loc"    # Landroid/location/Location;

    .prologue
    .line 2422
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 2425
    const-string v0, "TMSDevMngr"

    const-string v1, "GPS: onProviderDisabled "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2429
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 2432
    const-string v0, "TMSDevMngr"

    const-string v1, "GPS: onProviderEnabled "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 2439
    const-string v0, "TMSDevMngr"

    const-string v1, "GPS: onStatusChanged "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2440
    return-void
.end method
