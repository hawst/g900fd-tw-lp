.class Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;
.super Ljava/lang/Object;
.source "AcmsServiceMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StopSvcRunnable"
.end annotation


# instance fields
.field private service:Landroid/app/Service;

.field private startId:I


# direct methods
.method public constructor <init>(ILandroid/app/Service;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "svc"    # Landroid/app/Service;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput p1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->startId:I

    .line 106
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->service:Landroid/app/Service;

    .line 107
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->startId:I

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->service:Landroid/app/Service;

    iget v1, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->startId:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopSelfResult(I)Z

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/acms/api/AcmsServiceMonitor$StopSvcRunnable;->service:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0
.end method
