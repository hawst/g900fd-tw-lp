.class public Lcom/samsung/android/mirrorlink/engine/TmsEngine;
.super Ljava/lang/Object;
.source "TmsEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;,
        Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;,
        Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;
    }
.end annotation


# static fields
.field public static final NATIVE_EVT_DEINIT:I = 0x2

.field public static final NATIVE_EVT_DEINIT_FAILURE:I = 0xc

.field public static final NATIVE_EVT_DEINIT_SUCCESS:I = 0x8

.field public static final NATIVE_EVT_FRAMEBUFFER_BLOCK:I = 0x3e9

.field public static final NATIVE_EVT_INIT:I = 0x0

.field public static final NATIVE_EVT_INIT_FAILURE:I = 0xa

.field public static final NATIVE_EVT_INIT_SUCCESS:I = 0x6

.field public static final NATIVE_EVT_NONE:I = 0xd

.field public static final NATIVE_EVT_START:I = 0x1

.field public static final NATIVE_EVT_START_FAILURE:I = 0xb

.field public static final NATIVE_EVT_START_SUCCESS:I = 0x7

.field public static final NATIVE_EVT_STOP:I = 0x3

.field public static final NATIVE_EVT_STOP_FAILURE:I = 0xd

.field public static final NATIVE_EVT_STOP_SUCCESS:I = 0x9

.field public static final NATIVE_EVT_TERMINATE_TOP_APP:I = 0x3ea

.field public static final NATIVE_STATE_DEINIT_COMPLETED:I = 0x6f

.field public static final NATIVE_STATE_DEINIT_FAILURE:I = 0x70

.field public static final NATIVE_STATE_DEINIT_IN_PROGRESS:I = 0x6e

.field public static final NATIVE_STATE_INIT_COMPLETED:I = 0x66

.field public static final NATIVE_STATE_INIT_FAILURE:I = 0x67

.field public static final NATIVE_STATE_INIT_IN_PROGRESS:I = 0x65

.field public static final NATIVE_STATE_INVALID:I = 0x0

.field public static final NATIVE_STATE_NONE:I = 0x64

.field public static final NATIVE_STATE_OFF:I = 0x1

.field public static final NATIVE_STATE_ON:I = 0x2

.field public static final NATIVE_STATE_RUN:I = 0x3

.field public static final NATIVE_STATE_START_COMPLETED:I = 0x69

.field public static final NATIVE_STATE_START_FAILURE:I = 0x6a

.field public static final NATIVE_STATE_START_IN_PROGRESS:I = 0x68

.field public static final NATIVE_STATE_STOP_COMPLETED:I = 0x6c

.field public static final NATIVE_STATE_STOP_FAILURE:I = 0x6d

.field public static final NATIVE_STATE_STOP_IN_PROGRESS:I = 0x6b

.field public static final NATIVE_VNC_PARAM_BOTH_ORIENTATION:Ljava/lang/String; = "Both"

.field private static final NATIVE_VNC_PARAM_ENCODING_NONE:Ljava/lang/String; = "None"

.field private static final NATIVE_VNC_PARAM_ENCODING_PARTIAL:Ljava/lang/String; = "Partial"

.field private static final NATIVE_VNC_PARAM_ENCODING_STRICT:Ljava/lang/String; = "Strict"

.field public static final NATIVE_VNC_PARAM_LANDSCAPE:Ljava/lang/String; = "Landscape"

.field public static final NATIVE_VNC_PARAM_PORTRAIT:Ljava/lang/String; = "Portrait"

.field private static final TAG:Ljava/lang/String; = "TMSEngine"

.field private static final TMS_ENG_NWK_EVT:I = 0x1

.field private static final TMS_ENG_SCM_EVT:I = 0x2

.field private static final TOAST_STR_DEINIT_FAILURE:Ljava/lang/String; = "TMS DEINIT FAILURE"

.field private static final TOAST_STR_DEINIT_SUCCCESS:Ljava/lang/String; = "TMS DEINIT SUCCCESS"

.field private static final TOAST_STR_FRAMEBUFFER_BLOCKED:Ljava/lang/String; = "VNC FRAMEBUFFER BLOCKED"

.field private static final TOAST_STR_INIT_FAILURE:Ljava/lang/String; = "TMS INIT FAILURE"

.field private static final TOAST_STR_INIT_SUCCCESS:Ljava/lang/String; = "TMS INIT SUCCCESS"

.field private static final TOAST_STR_NETOWORK_DISCONNECTED:Ljava/lang/String; = "NETWORK DISCONNECTED"

.field private static final TOAST_STR_START_FAILURE:Ljava/lang/String; = "TMS START FAILURE"

.field private static final TOAST_STR_START_SUCCCESS:Ljava/lang/String; = "TMS START SUCCCESS"

.field private static final TOAST_STR_STOP_FAILURE:Ljava/lang/String; = "TMS STOP FAILURE"

.field private static final TOAST_STR_STOP_SUCCCESS:Ljava/lang/String; = "TMS STOP SUCCCESS"

.field private static isDeinitPending:Z

.field private static isInitInProgress:Z

.field private static isStartSuccess:Z


# instance fields
.field private isNativeReleaseDone:Z

.field private mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

.field private mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mAppStateHandler:Landroid/os/Handler;

.field public mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

.field private mBtAppListner:Landroid/os/Handler;

.field private mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

.field public mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

.field private mCurrentState:I

.field private mCxt:Landroid/content/Context;

.field public mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

.field private final mEnableToast:Z

.field private mEngToast:Landroid/widget/Toast;

.field private mEvtHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

.field private mIpAddr:Ljava/lang/String;

.field private mIsBtInited:Z

.field private mLooper:Landroid/os/Looper;

.field private mNativeContext:I

.field private mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

.field private mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

.field private mNwkType:I

.field public mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

.field mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

.field private mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

.field private mTmReadSettings:Lcom/samsung/android/mirrorlink/util/TmReadSettings;

.field private mVncEncodingMode:Ljava/lang/String;

.field private mVncOrientation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 152
    sput-boolean v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isStartSuccess:Z

    .line 153
    sput-boolean v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    .line 154
    sput-boolean v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 3
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    const/4 v2, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    .line 233
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$1;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppListner:Landroid/os/Handler;

    .line 177
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.TmsEngine() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    .line 180
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mLooper:Landroid/os/Looper;

    .line 181
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEnableToast:Z

    .line 183
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    .line 184
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 185
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEvtHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

    .line 187
    const-string v0, "Portrait"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncOrientation:Ljava/lang/String;

    .line 188
    const-string v0, "None"

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncEncodingMode:Ljava/lang/String;

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    .line 192
    iput v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    .line 194
    sput-boolean v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isStartSuccess:Z

    .line 195
    sput-boolean v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    .line 196
    sput-boolean v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    .line 198
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 200
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.TmsEngine() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Z)V
    .locals 0

    .prologue
    .line 143
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIsBtInited:Z

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    return v0
.end method

.method static synthetic access$10(Z)V
    .locals 0

    .prologue
    .line 153
    sput-boolean p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    return-void
.end method

.method static synthetic access$11()Z
    .locals 1

    .prologue
    .line 154
    sget-boolean v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    return v0
.end method

.method static synthetic access$12(Z)V
    .locals 0

    .prologue
    .line 154
    sput-boolean p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V
    .locals 0

    .prologue
    .line 772
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I
    .locals 1

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_deinit()I

    move-result v0

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I
    .locals 1

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_release()I

    move-result v0

    return v0
.end method

.method static synthetic access$17(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Z)V
    .locals 0

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/engine/TmsEngine;I)V
    .locals 0

    .prologue
    .line 822
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->stopExpiryTimerForSuccessEvent(I)V

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0

    .prologue
    .line 1149
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->performDeinit()V

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 0

    .prologue
    .line 1400
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->sendBuildVersion()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 402
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)I
    .locals 1

    .prologue
    .line 724
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getVncServerPort()I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)Lcom/samsung/net/HostInterface$NWK_TYPE;
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getRegisterNetwork()Lcom/samsung/net/HostInterface$NWK_TYPE;

    move-result-object v0

    return-object v0
.end method

.method private deinitMediaApps()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->deinit()Z

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->deinit()Z

    .line 296
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setListner(Landroid/os/Handler;)V

    .line 297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIsBtInited:Z

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    if-eqz v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->deinit()Z

    .line 305
    :cond_2
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    .line 307
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    .line 308
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    .line 309
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    if-eqz v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->deinit()Z

    .line 312
    :cond_3
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    .line 313
    return-void
.end method

.method private getRegisterNetwork()Lcom/samsung/net/HostInterface$NWK_TYPE;
    .locals 2

    .prologue
    .line 392
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    if-ne v0, v1, :cond_0

    .line 393
    sget-object v0, Lcom/samsung/net/HostInterface$NWK_TYPE;->WIFI:Lcom/samsung/net/HostInterface$NWK_TYPE;

    .line 395
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/net/HostInterface$NWK_TYPE;->USB:Lcom/samsung/net/HostInterface$NWK_TYPE;

    goto :goto_0
.end method

.method private handleNwkEvt(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 505
    const-string v1, "TMSEngine"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "TmsEngine.handleNwkEvt() - Enter -type: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 506
    const-string v2, "evt: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "data: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 505
    invoke-static {v1, v0}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    if-eq v0, v1, :cond_0

    .line 545
    :goto_0
    return-void

    .line 511
    :cond_0
    iget v0, p1, Landroid/os/Message;->arg2:I

    packed-switch v0, :pswitch_data_0

    .line 541
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.handleNwkEvt() - Unknown message type"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :goto_1
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.handleNwkEvt() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 513
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 514
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.handleNwkEvt() mIpAddr is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 519
    :cond_1
    const-string v0, "TMSEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TmsEngine.handleNwkEvt() mIpAddr %s"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 524
    :pswitch_1
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.handleNwkEvt():TMS_NWK_EVT_DISCONNECTED"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 526
    const-string v0, "TMSEngine"

    const-string v1, "mIpAddr is still valid"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    goto :goto_1

    .line 511
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initMediaApps()V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    if-nez v0, :cond_1

    .line 259
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppListner:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setListner(Landroid/os/Handler;)V

    .line 262
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z

    .line 264
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    if-nez v0, :cond_2

    .line 265
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 268
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z

    .line 272
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    if-nez v0, :cond_3

    .line 273
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z

    .line 276
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mDapAppHolder:Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/DapAppholder;->setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    if-nez v0, :cond_4

    .line 279
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    .line 281
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->init(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)Z

    .line 282
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCdbAppHolder:Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/CdbAppholder;->setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    goto :goto_0
.end method

.method private final native native_tms_appCntxtInfo(Ljava/lang/String;)I
.end method

.method private final native native_tms_appRotationInfo(I)I
.end method

.method private final native native_tms_cdb_start()I
.end method

.method private final native native_tms_cdb_stop()I
.end method

.method private final native native_tms_dap_start()I
.end method

.method private final native native_tms_dap_stop()I
.end method

.method private final native native_tms_deinit()I
.end method

.method private final native native_tms_getBuildVersion()Ljava/lang/Object;
.end method

.method private final native native_tms_getRtpParams()I
.end method

.method private final native native_tms_getState()I
.end method

.method private final native native_tms_getVncParams()I
.end method

.method private final native native_tms_getVncServerPort()I
.end method

.method private final native native_tms_init()I
.end method

.method private final native native_tms_release()I
.end method

.method private final native native_tms_rtpClient_start(Ljava/lang/String;)I
.end method

.method private final native native_tms_rtpClient_stop()I
.end method

.method private final native native_tms_rtpServer_start(Ljava/lang/String;)I
.end method

.method private final native native_tms_rtpServer_stop()I
.end method

.method private final native native_tms_setCdbParams(Ljava/lang/String;)I
.end method

.method private final native native_tms_setDapParams(Ljava/lang/String;)I
.end method

.method private final native native_tms_setRtpParams(Ljava/lang/String;)I
.end method

.method private final native native_tms_setVncEnable(I)I
.end method

.method private final native native_tms_setVncParams(Ljava/lang/String;)I
.end method

.method private final native native_tms_setup(Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;)I
.end method

.method private final native native_tms_start()I
.end method

.method private final native native_tms_stop()I
.end method

.method private final native native_tms_terminatVNC()I
.end method

.method private performDeinit()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1150
    sput-boolean v1, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    .line 1151
    sput-boolean v1, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    .line 1152
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1153
    const-string v1, "TMSEngine"

    .line 1154
    const-string v2, "TmsEngNativeEvtHandler.mAppStateHandler!=null - send deinit message, "

    .line 1153
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1156
    .local v0, "msg1":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1157
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1159
    .end local v0    # "msg1":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 5
    .param p0, "tmsEng_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1170
    const-string v2, "TMSEngine"

    .line 1171
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TmsEngNativeEvtHandler.postEventFromNative() - Enter - what:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1172
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1171
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1170
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "tmsEng_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .line 1177
    .local v0, "c":Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    if-nez v0, :cond_0

    .line 1178
    const-string v2, "TMSEngine"

    .line 1179
    const-string v3, "TmsEngNativeEvtHandler.postEventFromNative() - tmsEng is NULL"

    .line 1178
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    :goto_0
    return-void

    .line 1183
    :cond_0
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    if-eqz v2, :cond_1

    .line 1184
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1185
    .local v1, "m":Landroid/os/Message;
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1190
    .end local v1    # "m":Landroid/os/Message;
    :goto_1
    const-string v2, "TMSEngine"

    const-string v3, "TmsEngNativeEvtHandler.postEventFromNative() - Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1187
    :cond_1
    const-string v2, "TMSEngine"

    .line 1188
    const-string v3, "TmsEngNativeEvtHandler.postEventFromNative() - mNativeHandler is null"

    .line 1187
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private readConfig()V
    .locals 5

    .prologue
    .line 1197
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1202
    .local v0, "mSharedPreferences":Landroid/content/SharedPreferences;
    const-string v3, "screen_mode_key"

    const-string v4, "1"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1203
    .local v2, "screen_mode":Ljava/lang/String;
    const-string v3, "1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1204
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - NATIVE_VNC_PARAM_LANDSCAPE"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const-string v3, "Landscape"

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncOrientation:Ljava/lang/String;

    .line 1213
    :goto_0
    const-string v3, "encoding_mode_key"

    const-string v4, "1"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1214
    .local v1, "screen_enocoding_mode":Ljava/lang/String;
    const-string v3, "1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1215
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - NATIVE_VNC_PARAM_ENCODING_STRICT"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const-string v3, "Strict"

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncEncodingMode:Ljava/lang/String;

    .line 1227
    :goto_1
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    return-void

    .line 1207
    .end local v1    # "screen_enocoding_mode":Ljava/lang/String;
    :cond_0
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - NATIVE_VNC_PARAM_PORTRAIT"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    const-string v3, "Portrait"

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncOrientation:Ljava/lang/String;

    goto :goto_0

    .line 1217
    .restart local v1    # "screen_enocoding_mode":Ljava/lang/String;
    :cond_1
    const-string v3, "2"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1218
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - NATIVE_VNC_PARAM_ENCODING_PARTIAL"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    const-string v3, "Partial"

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncEncodingMode:Ljava/lang/String;

    goto :goto_1

    .line 1220
    :cond_2
    const-string v3, "3"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1221
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - NATIVE_VNC_PARAM_ENCODING_NONE"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const-string v3, "None"

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncEncodingMode:Ljava/lang/String;

    goto :goto_1

    .line 1224
    :cond_3
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.readConfig - Uknown"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private registerBuildInfoReqReceiver()V
    .locals 3

    .prologue
    .line 1375
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1376
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.tmserver.service.BUILD_INFO_REQ"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1378
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1379
    new-instance v1, Lcom/samsung/android/mirrorlink/engine/TmsEngine$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$2;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    .line 1392
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1394
    :cond_0
    return-void
.end method

.method private registerNetwork()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 373
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 374
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "nwk_type"

    const-string v3, "Usb"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "nwkType":Ljava/lang/String;
    const-string v2, "Usb"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 377
    const/4 v2, 0x4

    iput v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    .line 381
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    if-eqz v2, :cond_0

    .line 382
    const-string v2, "TMSEngine"

    const-string v3, "TmsEngine.registerNetwork(): mNwkMngr is not null "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEvtHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

    iget v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->registerForNwkEvents(Landroid/os/Handler;II)V

    .line 385
    :cond_0
    return-void

    .line 379
    :cond_1
    iput v5, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    goto :goto_0
.end method

.method private registerSSDPAliveReqReceiver()V
    .locals 3

    .prologue
    .line 1421
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    if-nez v1, :cond_0

    .line 1422
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1423
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.tmserver.service.SSDP_ALIVE_REQ"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1424
    new-instance v1, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    invoke-direct {v1, v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;)V

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    .line 1425
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1427
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private sendBuildVersion()V
    .locals 5

    .prologue
    .line 1402
    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v2, :cond_0

    .line 1404
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getBuildVersion()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1410
    .local v0, "buildversion":Ljava/lang/String;
    :goto_0
    const-string v2, "TMSEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " TmsEngine.sendBuildVersion() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.tmserver.service.BUILD_INFO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1412
    .local v1, "intentInfo":Landroid/content/Intent;
    const-string v2, "com.samsung.android.tmserver.service.BUILD_VERSION"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1413
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1414
    return-void

    .line 1408
    .end local v0    # "buildversion":Ljava/lang/String;
    .end local v1    # "intentInfo":Landroid/content/Intent;
    :cond_0
    const-string v0, "TMS NOT LOADED"

    .restart local v0    # "buildversion":Ljava/lang/String;
    goto :goto_0
.end method

.method private setParams()V
    .locals 4

    .prologue
    .line 555
    const-string v1, "TMSEngine"

    const-string v2, "TmsEngine.setParams(): Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->readConfig()V

    .line 559
    new-instance v0, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 560
    .local v0, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v1, "IpAddr"

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v1, "Port"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 562
    const-string v1, "Orientation"

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncOrientation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v1, "EncodingMode"

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mVncEncodingMode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    const-string v1, "TMSEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TmsEngine.setParams(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v1, :cond_0

    .line 588
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setVncParams(Ljava/lang/String;)I

    .line 592
    :cond_0
    const-string v1, "TMSEngine"

    const-string v2, "TmsEngine.setParams(): Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 403
    const-string v0, "TMSEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TmsEngine.showToast() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEnableToast:Z

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEngToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 407
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEngToast:Landroid/widget/Toast;

    .line 410
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEngToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 412
    :cond_0
    return-void

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEngToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private startExpiryTimerForSuccessEvent(I)V
    .locals 6
    .param p1, "event"    # I

    .prologue
    .line 774
    const/4 v1, 0x0

    .line 775
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x4e20

    .line 777
    .local v2, "time":I
    const-class v4, Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    monitor-enter v4

    .line 778
    packed-switch p1, :pswitch_data_0

    .line 808
    const/16 v0, 0xd

    .line 809
    .local v0, "eventForFailure":I
    :try_start_0
    const-string v3, "TMSEngine"

    .line 810
    const-string v5, "startExpiryTimerForSuccessEvent => Native timer was started for different event"

    .line 809
    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 814
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    if-eqz v3, :cond_0

    .line 815
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    invoke-virtual {v3, v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 816
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    int-to-long v4, v2

    invoke-virtual {v3, v1, v4, v5}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 818
    :cond_0
    return-void

    .line 780
    .end local v0    # "eventForFailure":I
    :pswitch_0
    :try_start_1
    const-string v3, "TMSEngine"

    .line 781
    const-string v5, "startExpiryTimerForSuccessEvent NATIVE_EVT_INIT => adding NATIVE_EVT_INIT_FAILURE"

    .line 780
    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    const/16 v0, 0xa

    .line 783
    .restart local v0    # "eventForFailure":I
    const/16 v3, 0x65

    iput v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    goto :goto_0

    .line 777
    .end local v0    # "eventForFailure":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 787
    :pswitch_1
    :try_start_2
    const-string v3, "TMSEngine"

    .line 788
    const-string v5, "startExpiryTimerForSuccessEvent NATIVE_EVT_DEINIT => adding NATIVE_EVT_DEINIT_FAILURE"

    .line 787
    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const/16 v0, 0xc

    .line 790
    .restart local v0    # "eventForFailure":I
    const/16 v3, 0x6e

    iput v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    goto :goto_0

    .line 794
    .end local v0    # "eventForFailure":I
    :pswitch_2
    const-string v3, "TMSEngine"

    .line 795
    const-string v5, "startExpiryTimerForSuccessEvent NATIVE_EVT_START => adding NATIVE_EVT_START_FAILURE"

    .line 794
    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    const/16 v0, 0xb

    .line 797
    .restart local v0    # "eventForFailure":I
    const/16 v3, 0x68

    iput v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    goto :goto_0

    .line 801
    .end local v0    # "eventForFailure":I
    :pswitch_3
    const-string v3, "TMSEngine"

    .line 802
    const-string v5, "startExpiryTimerForSuccessEvent NATIVE_EVT_STOP => adding NATIVE_EVT_STOP_FAILURE"

    .line 801
    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const/16 v0, 0xd

    .line 804
    .restart local v0    # "eventForFailure":I
    const/16 v3, 0x6b

    iput v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 778
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private stopExpiryTimerForSuccessEvent(I)V
    .locals 4
    .param p1, "what"    # I

    .prologue
    .line 824
    const/4 v0, 0x0

    .line 825
    .local v0, "msg":I
    const-class v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    monitor-enter v2

    .line 826
    packed-switch p1, :pswitch_data_0

    .line 885
    const/16 v0, 0x64

    .line 886
    :try_start_0
    const-string v1, "TMSEngine"

    .line 887
    const-string v3, "stopExpiryTimerForSuccessEvent is interrupted => Native timer was started for different event"

    .line 886
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 891
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 892
    const-string v1, "TMSEngine"

    .line 893
    const-string v2, "stopExpiryTimerForSuccessEvent removing failure message"

    .line 892
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;->removeMessages(I)V

    .line 896
    :cond_0
    return-void

    .line 828
    :pswitch_0
    :try_start_1
    const-string v1, "TMSEngine"

    .line 829
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_DEINIT_SUCCESS"

    .line 828
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    const/16 v1, 0x6f

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 832
    const/16 v0, 0xc

    .line 833
    goto :goto_0

    .line 836
    :pswitch_1
    const-string v1, "TMSEngine"

    .line 837
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_DEINIT_FAILURE"

    .line 836
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/16 v1, 0x70

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 839
    const/16 v0, 0xc

    .line 840
    goto :goto_0

    .line 843
    :pswitch_2
    const-string v1, "TMSEngine"

    .line 844
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_STOP_SUCCESS"

    .line 843
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    const/16 v1, 0x6c

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 846
    const/16 v0, 0xd

    .line 847
    goto :goto_0

    .line 850
    :pswitch_3
    const-string v1, "TMSEngine"

    .line 851
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_STOP_FAILURE"

    .line 850
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    const/16 v1, 0x6d

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 853
    const/16 v0, 0xd

    .line 854
    goto :goto_0

    .line 857
    :pswitch_4
    const-string v1, "TMSEngine"

    .line 858
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_START_SUCCESS"

    .line 857
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const/16 v1, 0x69

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 860
    const/16 v0, 0xb

    .line 861
    goto :goto_0

    .line 864
    :pswitch_5
    const-string v1, "TMSEngine"

    .line 865
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_START_FAILURE"

    .line 864
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    const/16 v1, 0x6a

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 867
    const/16 v0, 0xb

    .line 868
    goto :goto_0

    .line 871
    :pswitch_6
    const-string v1, "TMSEngine"

    .line 872
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_INIT_SUCCESS"

    .line 871
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const/16 v1, 0x66

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 874
    const/16 v0, 0xa

    .line 875
    goto :goto_0

    .line 878
    :pswitch_7
    const-string v1, "TMSEngine"

    .line 879
    const-string v3, "stopExpiryTimerForSuccessEvent NATIVE_EVT_INIT_FAILURE"

    .line 878
    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    const/16 v1, 0x67

    iput v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCurrentState:I

    .line 881
    const/16 v0, 0xa

    .line 882
    goto/16 :goto_0

    .line 825
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 826
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_5
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private unregisterSSDPAliveReqReceiver()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1430
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    if-eqz v0, :cond_0

    .line 1431
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->clean()V

    .line 1432
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->mNwkthread:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver$NetworkLooperThread;

    .line 1433
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;->access$0(Landroid/os/Handler;)V

    .line 1434
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1435
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mSSDPAliveReceiver:Lcom/samsung/android/mirrorlink/engine/TmsEngine$SSDPAliveReceiver;

    .line 1437
    :cond_0
    return-void
.end method


# virtual methods
.method public cdbStart()I
    .locals 1

    .prologue
    .line 1312
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_cdb_start()I

    .line 1313
    const/4 v0, 0x0

    return v0
.end method

.method public cdbStop()I
    .locals 1

    .prologue
    .line 1317
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_cdb_stop()I

    .line 1318
    const/4 v0, 0x0

    return v0
.end method

.method public clean()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.clean() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEvtHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

    .line 214
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    .line 215
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    .line 216
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppListner:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppListner:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 219
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppListner:Landroid/os/Handler;

    .line 221
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.clean() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method public dapStart()I
    .locals 1

    .prologue
    .line 1284
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_dap_start()I

    .line 1285
    const/4 v0, 0x0

    return v0
.end method

.method public dapStop()I
    .locals 1

    .prologue
    .line 1293
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_dap_stop()I

    .line 1294
    const/4 v0, 0x0

    return v0
.end method

.method public deinit()I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 419
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.deinit() - Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-eqz v3, :cond_1

    .line 425
    const-string v2, "TMSEngine"

    const-string v3, "TmsEngine.deinit() - (isNativeReleaseDone == true) "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_0
    :goto_0
    return v1

    .line 429
    :cond_1
    sget-boolean v3, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    if-eqz v3, :cond_2

    .line 430
    const-string v3, "TMSEngine"

    const-string v4, "Init is in progress. returning false"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    sput-boolean v1, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    move v1, v2

    .line 432
    goto :goto_0

    .line 446
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    if-eqz v3, :cond_3

    .line 447
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mEvtHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngEvtHandler;

    iget v5, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkType:I

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->unregisterForNwkEvents(Landroid/os/Handler;I)V

    .line 448
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    .line 451
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v3, :cond_4

    .line 452
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 453
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBuildInfoReqReceiver:Landroid/content/BroadcastReceiver;

    .line 457
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->deinitMediaApps()V

    .line 459
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->unregisterSSDPAliveReqReceiver()V

    .line 461
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    if-eqz v3, :cond_5

    .line 462
    const-string v3, "TMSEngine"

    const-string v4, "TmsEngine.deinit() - mTMSServrDevc stop called"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getClientProfileSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->setMLSessionEstablished(Z)V

    .line 464
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->stop()Z

    .line 465
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 467
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getState()I

    move-result v0

    .line 469
    .local v0, "nativeState":I
    if-ne v7, v0, :cond_7

    .line 470
    const-string v3, "TMSEngine"

    const-string v4, "Native was not started. Hence deiniting it directly"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.android.mirrorlink.action.DISMISS_BLACK_SCREEN"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 473
    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    .line 474
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_deinit()I

    .line 486
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-eqz v3, :cond_6

    .line 487
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->deinit()V

    .line 488
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 491
    :cond_6
    iput-object v6, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTmReadSettings:Lcom/samsung/android/mirrorlink/util/TmReadSettings;

    .line 492
    if-eq v1, v0, :cond_0

    .line 496
    const-string v1, "TMSEngine"

    const-string v3, "TmsEngine.deinit() - Exit"

    invoke-static {v1, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 497
    goto/16 :goto_0

    .line 475
    :cond_7
    if-ne v8, v0, :cond_8

    .line 476
    const-string v3, "TMSEngine"

    const-string v4, "Native was started. Hence stopping"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    invoke-direct {p0, v8}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    .line 478
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_stop()I

    goto :goto_1

    .line 481
    :cond_8
    const-string v3, "TMSEngine"

    const-string v4, "Native in OFF state. Hence calling native release"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_release()I

    .line 483
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    goto :goto_1
.end method

.method public getAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    return-object v0
.end method

.method public getTMSAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .locals 1

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    return-object v0
.end method

.method public getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    .locals 1

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    return-object v0
.end method

.method public init()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 322
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.init() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v0, :cond_0

    .line 324
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.init() - isNativeReleaseDone ==false"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :goto_0
    return v3

    .line 328
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    .line 331
    new-instance v0, Lcom/samsung/android/mirrorlink/util/TmReadSettings;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/util/TmReadSettings;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTmReadSettings:Lcom/samsung/android/mirrorlink/util/TmReadSettings;

    .line 332
    new-instance v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, p0, v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsEngine;Lcom/samsung/android/mirrorlink/engine/TmsEngine;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNativeHandler:Lcom/samsung/android/mirrorlink/engine/TmsEngine$TmsEngNativeEvtHandler;

    .line 333
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mLooper:Landroid/os/Looper;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setup(Ljava/lang/Object;Landroid/content/Context;Landroid/os/Looper;)I

    .line 334
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    .line 336
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-nez v0, :cond_1

    .line 337
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v0, p0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->setNativeInterface(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 340
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->initMediaApps()V

    .line 341
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->init()V

    .line 348
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->registerNetwork()V

    .line 350
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v0, :cond_2

    .line 351
    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    .line 352
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_init()I

    .line 355
    :cond_2
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;-><init>(Landroid/content/Context;Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    .line 357
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->registerSSDPAliveReqReceiver()V

    .line 359
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getAcsDeviceMngr()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAcsDeviceMngr:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    .line 360
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.init() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isNativeReleased()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    return v0
.end method

.method public rtpClientStart(Ljava/lang/String;)I
    .locals 1
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1266
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_rtpClient_start(Ljava/lang/String;)I

    .line 1267
    const/4 v0, 0x0

    return v0
.end method

.method public rtpClientStop()I
    .locals 1

    .prologue
    .line 1271
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_rtpClient_stop()I

    .line 1272
    const/4 v0, 0x0

    return v0
.end method

.method public rtpServerStart(Ljava/lang/String;)I
    .locals 1
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1251
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_rtpServer_start(Ljava/lang/String;)I

    .line 1252
    const/4 v0, 0x0

    return v0
.end method

.method public rtpServerStop()I
    .locals 1

    .prologue
    .line 1260
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_rtpServer_stop()I

    .line 1261
    const/4 v0, 0x0

    return v0
.end method

.method public sendAppCntxtInfo(Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1331
    const-string v0, "TMSEngine"

    const-string v1, " TmsEngine.sendAppCntxtInfo() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-eqz v0, :cond_0

    .line 1333
    const-string v0, "TMSEngine"

    const-string v1, " TmsEngine.sendAppCntxtInfo() : isNativeReleaseDone == true"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    :goto_0
    return-void

    .line 1336
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_appCntxtInfo(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendAppRotationInfo(I)V
    .locals 2
    .param p1, "angle"    # I

    .prologue
    .line 1356
    const-string v0, "TMSEngine"

    const-string v1, " TmsEngine.sendAppRotationInfo() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-eqz v0, :cond_0

    .line 1358
    const-string v0, "TMSEngine"

    const-string v1, " TmsEngine.sendAppRotationInfo() : isNativeReleaseDone == true"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    :goto_0
    return-void

    .line 1361
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_appRotationInfo(I)I

    goto :goto_0
.end method

.method public setAppStateHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1526
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppStateHandler:Landroid/os/Handler;

    .line 1527
    return-void
.end method

.method public setCdbParams(Ljava/lang/String;)I
    .locals 1
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1322
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setCdbParams(Ljava/lang/String;)I

    .line 1323
    const/4 v0, 0x0

    return v0
.end method

.method public setDapParams(Ljava/lang/String;)I
    .locals 1
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1303
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setDapParams(Ljava/lang/String;)I

    .line 1304
    const/4 v0, 0x0

    return v0
.end method

.method public setRtpParams(Ljava/lang/String;)I
    .locals 1
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 1242
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setRtpParams(Ljava/lang/String;)I

    .line 1243
    const/4 v0, 0x0

    return v0
.end method

.method public setVNCenable(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 1341
    const-string v1, "TMSEngine"

    const-string v2, " TmsEngine.setVNCenable() "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-eqz v1, :cond_0

    .line 1343
    const-string v1, "TMSEngine"

    const-string v2, " TmsEngine.setVNCenable() : isNativeReleaseDone == true"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    :goto_0
    return-void

    .line 1346
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 1347
    .local v0, "nativeFlag":I
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_setVncEnable(I)I

    goto :goto_0

    .line 1346
    .end local v0    # "nativeFlag":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public start()I
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 600
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.start() - Enter"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    sget-boolean v7, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isStartSuccess:Z

    if-eqz v7, :cond_0

    .line 602
    const-string v7, "TMSEngine"

    const-string v8, "Engine has started successfully already. Hence returning"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :goto_0
    return v11

    .line 606
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    if-nez v7, :cond_1

    .line 607
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.start() - (mNwkMngr == null)"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 610
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getConnectedIpAddress()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    .line 613
    iget-boolean v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIsBtInited:Z

    if-nez v7, :cond_2

    .line 614
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.start() - (mIsBtInited == false)"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 619
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mNwkMngr:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 622
    iget-boolean v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    if-eqz v7, :cond_5

    .line 626
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getState()I

    move-result v0

    .line 627
    .local v0, "State":I
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.handleNwkEvt():TMS_NWK_EVT_CONNECTED"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const/4 v7, 0x2

    if-ne v0, v7, :cond_5

    .line 630
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->setParams()V

    .line 631
    iget-boolean v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v7, :cond_3

    .line 632
    invoke-direct {p0, v12}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    .line 633
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_start()I

    .line 639
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mCxt:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 640
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v7, "rtp_server_self_port_key"

    const-string v8, "10500"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 641
    .local v5, "serrverSelfPort":Ljava/lang/String;
    const-string v7, "rtp_client_self_port_key"

    const-string v8, "10600"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 642
    .local v2, "clientSelfPort":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setRtpParams(Ljava/lang/String;II)V

    .line 647
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    if-eqz v7, :cond_4

    .line 648
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getServerMac()Ljava/lang/String;

    move-result-object v4

    .line 650
    .local v4, "receivedServerMac":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 652
    const-string v7, "TMSEngine"

    const-string v8, "BT mac address is not null. Hence setting the value"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->getFlagStartConnection()Z

    move-result v8

    invoke-virtual {v7, v4, v8}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setBtParams(Ljava/lang/String;Z)V

    .line 659
    .end local v4    # "receivedServerMac":Ljava/lang/String;
    :cond_4
    const-string v7, "dap_self_port_key"

    const-string v8, "9920"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 660
    .local v3, "dapselfPort":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setDapParams(Ljava/lang/String;I)V

    .line 661
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.handleNwkEvt():starting UPNP"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    const-string v7, "cdb_self_port_key"

    const-string v8, "9980"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 665
    .local v1, "cdbselfPort":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mIpAddr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setCdbParams(Ljava/lang/String;I)V

    .line 667
    iget-object v7, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    invoke-virtual {v7}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->setModelName()V

    .line 670
    .end local v0    # "State":I
    .end local v1    # "cdbselfPort":Ljava/lang/String;
    .end local v2    # "clientSelfPort":Ljava/lang/String;
    .end local v3    # "dapselfPort":Ljava/lang/String;
    .end local v5    # "serrverSelfPort":Ljava/lang/String;
    .end local v6    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_5
    const-string v7, "TMSEngine"

    const-string v8, "Engine started successfully"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    sput-boolean v12, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isStartSuccess:Z

    .line 684
    :cond_6
    :goto_1
    const-string v7, "TMSEngine"

    const-string v8, "TmsEngine.start() - Exit"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 675
    :cond_7
    const-string v7, "NETWORK DISCONNECTED"

    invoke-direct {p0, v7}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V

    .line 676
    sput-boolean v11, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isInitInProgress:Z

    .line 677
    sget-boolean v7, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    if-eqz v7, :cond_6

    .line 678
    const-string v7, "TMSEngine"

    const-string v8, "Deinit is in pending state. Calling deinit"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->deinit()I

    .line 680
    sput-boolean v11, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isDeinitPending:Z

    goto :goto_1
.end method

.method public stop()I
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 693
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.stop() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-nez v0, :cond_0

    .line 696
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_getState()I

    move-result v0

    if-ne v3, v0, :cond_0

    .line 697
    const-string v0, "NETWORK DISCONNECTED"

    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->showToast(Ljava/lang/String;)V

    .line 698
    invoke-direct {p0, v3}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->startExpiryTimerForSuccessEvent(I)V

    .line 699
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_stop()I

    .line 703
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    if-eqz v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mTMSServrDevc:Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->stop()Z

    .line 707
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-eqz v0, :cond_2

    .line 708
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->deinit()V

    .line 709
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mAppMngr:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 711
    :cond_2
    sput-boolean v2, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isStartSuccess:Z

    .line 712
    const-string v0, "TMSEngine"

    const-string v1, "TmsEngine.stop() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    return v2
.end method

.method public terminateVNCSession()V
    .locals 2

    .prologue
    .line 1530
    const-string v0, "TMSEngine"

    const-string v1, "terminateVNCSession enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->isNativeReleaseDone:Z

    if-eqz v0, :cond_0

    .line 1532
    const-string v0, "TMSEngine"

    const-string v1, " TmsEngine.terminateVNCSession() : isNativeReleaseDone == true"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    :goto_0
    return-void

    .line 1535
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->native_tms_terminatVNC()I

    .line 1537
    const-string v0, "TMSEngine"

    const-string v1, "terminateVNCSession exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
