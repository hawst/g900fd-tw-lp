.class public Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;
.super Ljava/lang/Object;
.source "AudioConnection.java"


# instance fields
.field private mediaAudioIn:I

.field private mediaAudioOut:I

.field private payloadTypes:Ljava/lang/String;

.field private phoneAudio:I

.field private voiceControl:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMediaAudioIn()I
    .locals 3

    .prologue
    .line 75
    const-string v0, "AudioConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Returning value of mediaAudioin:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioIn:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioIn:I

    return v0
.end method

.method public getMediaAudioOut()I
    .locals 3

    .prologue
    .line 60
    const-string v0, "AudioConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Returning value of mediaAudioOut:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioOut:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioOut:I

    return v0
.end method

.method public getPayloadTypes()Ljava/lang/String;
    .locals 3

    .prologue
    .line 120
    const-string v0, "AudioConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Returning value of payloadTypes:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->payloadTypes:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->payloadTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneAudio()I
    .locals 3

    .prologue
    .line 105
    const-string v0, "AudioConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Returning value of phoneAudio:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->phoneAudio:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->phoneAudio:I

    return v0
.end method

.method public getVoiceControl()I
    .locals 3

    .prologue
    .line 90
    const-string v0, "AudioConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Returning value of voiceControl:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->voiceControl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->voiceControl:I

    return v0
.end method

.method public setMediaAudioIn(I)V
    .locals 0
    .param p1, "mediaAudioIn"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioIn:I

    .line 84
    return-void
.end method

.method public setMediaAudioOut(I)V
    .locals 0
    .param p1, "mediaAudioOut"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->mediaAudioOut:I

    .line 69
    return-void
.end method

.method public setPayloadTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "payloadTypes"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->payloadTypes:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public setPhoneAudio(I)V
    .locals 0
    .param p1, "phoneAudio"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->phoneAudio:I

    .line 114
    return-void
.end method

.method public setVoiceControl(I)V
    .locals 0
    .param p1, "voiceControl"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->voiceControl:I

    .line 99
    return-void
.end method
