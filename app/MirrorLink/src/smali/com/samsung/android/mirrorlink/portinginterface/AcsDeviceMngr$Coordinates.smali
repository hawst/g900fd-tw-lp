.class Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;
.super Ljava/lang/Object;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Coordinates"
.end annotation


# instance fields
.field public mX:I

.field public mY:I


# direct methods
.method constructor <init>(II)V
    .locals 3
    .param p1, "inX"    # I
    .param p2, "inY"    # I

    .prologue
    .line 1604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1605
    const-string v0, "TMSDevMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " RotatedCoordinates Enter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1606
    iput p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mX:I

    .line 1607
    iput p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$Coordinates;->mY:I

    .line 1608
    return-void
.end method
