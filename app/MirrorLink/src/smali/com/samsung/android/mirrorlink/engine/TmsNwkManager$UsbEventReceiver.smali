.class Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TmsNwkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;)V
    .locals 0

    .prologue
    .line 479
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 490
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 492
    .local v3, "action":Ljava/lang/String;
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver:onReceive(): Enter"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v18, "android.hardware.usb.action.USB_STATE"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 498
    const/4 v6, 0x0

    .line 499
    .local v6, "connected":Z
    const/4 v5, 0x0

    .line 500
    .local v5, "configured":Z
    const/4 v10, 0x0

    .line 502
    .local v10, "isNcm":Z
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    if-eqz v18, :cond_0

    .line 503
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "connected"

    invoke-virtual/range {v18 .. v19}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 504
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "configured"

    invoke-virtual/range {v18 .. v19}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 505
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v18

    const-string v19, "ncm"

    invoke-virtual/range {v18 .. v19}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 507
    :cond_0
    if-eqz v6, :cond_6

    .line 509
    if-eqz v10, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 510
    const/4 v9, 0x0

    .line 511
    .local v9, "isMirrorLinkSessionEstablished":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$3(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v15

    .line 514
    .local v15, "serverDevice":Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    if-eqz v15, :cond_1

    .line 515
    invoke-virtual {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getClientProfileSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v16

    .line 516
    .local v16, "tmClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    if-eqz v16, :cond_1

    .line 517
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->isMirrorLinkSessionEstablished()Z

    move-result v9

    .line 521
    .end local v15    # "serverDevice":Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;
    .end local v16    # "tmClientProfileService":Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    :cond_1
    if-eqz v9, :cond_2

    .line 523
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver:onReceive(): terminal version reset and setNcmTethering(false)"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/Class;

    move-result-object v18

    const-string v19, "setNcmTethering"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    sget-object v22, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 526
    .local v11, "listener":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    .end local v11    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    const/16 v18, 0x0

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setNcmReady(ZZ)V

    .line 681
    .end local v5    # "configured":Z
    .end local v6    # "connected":Z
    .end local v9    # "isMirrorLinkSessionEstablished":Z
    .end local v10    # "isNcm":Z
    :cond_2
    :goto_1
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver:onReceive(): Exit"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    :cond_3
    :goto_2
    return-void

    .line 527
    .restart local v5    # "configured":Z
    .restart local v6    # "connected":Z
    .restart local v9    # "isMirrorLinkSessionEstablished":Z
    .restart local v10    # "isNcm":Z
    :catch_0
    move-exception v7

    .line 528
    .local v7, "e":Ljava/lang/Exception;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Could not connect to channel."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 569
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "isMirrorLinkSessionEstablished":Z
    :cond_4
    if-eqz v10, :cond_5

    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getLocalIpAddress(I)Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$6(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;I)Ljava/lang/String;

    move-result-object v8

    .line 571
    .local v8, "ipAddr":Ljava/lang/String;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "usbEventReceiver:onReceive(): connected "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    if-eqz v8, :cond_2

    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$7(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    .line 576
    const/16 v20, 0x1

    .line 575
    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V
    invoke-static {v0, v1, v2, v8}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V

    goto :goto_1

    .line 579
    .end local v8    # "ipAddr":Ljava/lang/String;
    :cond_5
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "usbEventReceiver:onReceive(): connected= true, configured= "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 580
    const-string v20, " NCM= "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", mTetheractive= "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", mUsbConnected= "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbConnected:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 579
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 583
    :cond_6
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver:onReceive(): disconnected"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$7(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V

    goto/16 :goto_1

    .line 605
    .end local v5    # "configured":Z
    .end local v6    # "connected":Z
    .end local v10    # "isNcm":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->ACTION_TETHER_STATE_CHANGED:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$9(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 607
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver.onReceive tether - ACTION_TETHER_STATE_CHANGED"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->EXTRA_ACTIVE_TETHER:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$10(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 610
    .local v4, "activeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v17, 0x0

    .line 611
    .local v17, "usbRegexs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v18

    if-eqz v18, :cond_2

    if-eqz v4, :cond_2

    .line 614
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/Class;

    move-result-object v18

    if-eqz v18, :cond_8

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCmClass:Ljava/lang/Class;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$4(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Ljava/lang/Class;

    move-result-object v18

    const-string v19, "getTetherableUsbRegexs"

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 616
    .restart local v11    # "listener":Ljava/lang/reflect/Method;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v18

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "usbRegexs":[Ljava/lang/String;
    check-cast v17, [Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3

    .line 639
    .restart local v17    # "usbRegexs":[Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/16 v18, 0x0

    move/from16 v19, v18

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v21

    if-lt v0, v1, :cond_a

    .line 668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mTetheractive:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$2(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 670
    const-string v18, "TMSSvc"

    const-string v19, "usbEventRceiver.onReceive usb untethered"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$14(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    .line 676
    const/16 v20, 0x2

    const/16 v21, 0x0

    .line 675
    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V
    invoke-static/range {v18 .. v21}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V

    goto/16 :goto_1

    .line 618
    .end local v11    # "listener":Ljava/lang/reflect/Method;
    :cond_8
    :try_start_2
    const-string v18, "TMSSvc"

    const-string v19, "mCmClass is not created, hence cant get the ip address"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v18

    if-nez v18, :cond_9

    .line 621
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCxt:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$11(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/content/Context;

    move-result-object v18

    const-string v20, "connectivity"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/net/ConnectivityManager;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$12(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Landroid/net/ConnectivityManager;)V

    .line 623
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mCm:Landroid/net/ConnectivityManager;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$5(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/ConnectivityManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$13(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Ljava/lang/Class;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_2

    .line 629
    .end local v17    # "usbRegexs":[Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 630
    .local v7, "e":Ljava/lang/NoSuchMethodException;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Could not connect to channel."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 632
    .end local v7    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v7

    .line 633
    .local v7, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Could not connect to channel."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 635
    .end local v7    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v7

    .line 636
    .local v7, "e":Ljava/lang/IllegalAccessException;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Could not connect to channel."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 639
    .end local v7    # "e":Ljava/lang/IllegalAccessException;
    .restart local v11    # "listener":Ljava/lang/reflect/Method;
    .restart local v17    # "usbRegexs":[Ljava/lang/String;
    :cond_a
    aget-object v12, v20, v19

    .local v12, "o":Ljava/lang/Object;
    move-object v14, v12

    .line 641
    check-cast v14, Ljava/lang/String;

    .line 642
    .local v14, "s":Ljava/lang/String;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    const/16 v18, 0x0

    :goto_4
    move/from16 v0, v18

    move/from16 v1, v22

    if-lt v0, v1, :cond_b

    .line 639
    add-int/lit8 v18, v19, 0x1

    move/from16 v19, v18

    goto/16 :goto_3

    .line 642
    :cond_b
    aget-object v13, v17, v18

    .line 644
    .local v13, "regex":Ljava/lang/String;
    invoke-virtual {v14, v13}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 646
    const-string v18, "TMSSvc"

    const-string v19, "usbEventReceiver.onReceive usb tether active"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mUsbConnected:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$8(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 649
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getLocalIpAddress(I)Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$6(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;I)Ljava/lang/String;

    move-result-object v8

    .line 651
    .restart local v8    # "ipAddr":Ljava/lang/String;
    const-string v18, "TMSSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "usbEventReceiver:onReceive(): connected "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    if-eqz v8, :cond_c

    .line 654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$7(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    .line 657
    const/16 v20, 0x1

    .line 656
    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V
    invoke-static {v0, v1, v2, v8}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V

    .line 660
    .end local v8    # "ipAddr":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$UsbEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$14(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Z)V

    goto/16 :goto_2

    .line 642
    :cond_d
    add-int/lit8 v18, v18, 0x1

    goto :goto_4
.end method
