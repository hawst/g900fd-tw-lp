.class public final Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;
.super Ljava/lang/Object;
.source "AppEntryInterface.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CertData"
.end annotation


# static fields
.field public static final ALL_COLUMNS:[Ljava/lang/String;

.field public static final APP_ID:Ljava/lang/String; = "appId"

.field public static final CERTIFICATE_ENTITY:Ljava/lang/String; = "certEntities"

.field public static final CERTIFICATE_INFO:Ljava/lang/String; = "certInfo"

.field public static final CERTIFICATE_STATE:Ljava/lang/String; = "certState"

.field public static final CERT_TYPE:Ljava/lang/String; = "certType"

.field public static final GRACE_PERIOD:Ljava/lang/String; = "gracePeriod"

.field public static final IS_CERTIFIED:Ljava/lang/String; = "isCertified"

.field public static final IS_MEMBER_APP:Ljava/lang/String; = "isMemberApp"

.field public static final IS_PENDING:Ljava/lang/String; = "isPending"

.field public static final IS_REVOKED:Ljava/lang/String; = "isRevoked"

.field public static final MAX_RETRY:Ljava/lang/String; = "maxRetry"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "packagename"

.field public static final QUERY_PERIOD:Ljava/lang/String; = "queryPeriod"

.field public static final SHOW:Ljava/lang/String; = "show"

.field public static final TABLE_NAME:Ljava/lang/String; = "appcertdata"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1132
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "appId"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1133
    const-string v2, "certInfo"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "certState"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 1134
    const-string v2, "certEntities"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "packagename"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 1135
    const-string v2, "isPending"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "certType"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 1136
    const-string v2, "queryPeriod"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "gracePeriod"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 1137
    const-string v2, "maxRetry"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "isCertified"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 1138
    const-string v2, "isRevoked"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "show"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "isMemberApp"

    aput-object v2, v0, v1

    .line 1132
    sput-object v0, Lcom/samsung/android/mirrorlink/acms/provider/AppEntryInterface$CertData;->ALL_COLUMNS:[Ljava/lang/String;

    .line 1168
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
