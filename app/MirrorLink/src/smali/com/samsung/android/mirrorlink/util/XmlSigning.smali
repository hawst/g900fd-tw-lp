.class public Lcom/samsung/android/mirrorlink/util/XmlSigning;
.super Ljava/lang/Object;
.source "XmlSigning.java"


# static fields
.field private static final RSA:Ljava/lang/String; = "RSA"

.field private static final TAG:Ljava/lang/String; = "XmlSigning"

.field private static final UTF_8:Ljava/lang/String; = "UTF-8"

.field private static mKeyPair:Ljava/security/KeyPair;

.field private static mXmlSigning:Lcom/samsung/android/mirrorlink/util/XmlSigning;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->getKeyPair(Landroid/content/Context;)Ljava/security/KeyPair;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mKeyPair:Ljava/security/KeyPair;

    .line 76
    return-void
.end method

.method private generateXmlSignature(Ljava/lang/String;Ljava/security/PrivateKey;)Ljava/lang/String;
    .locals 13
    .param p1, "xml"    # Ljava/lang/String;
    .param p2, "privateKey"    # Ljava/security/PrivateKey;

    .prologue
    .line 238
    :try_start_0
    const-string v10, "XmlSigning"

    const-string v11, "Enter generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v10, "UTF-8"

    invoke-virtual {p1, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    .line 242
    .local v9, "xmlBytes":[B
    new-instance v5, Lorg/xml/sax/InputSource;

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v5, v10}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 244
    .local v5, "in":Lorg/xml/sax/InputSource;
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 246
    .local v1, "dbfac":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v3

    .line 247
    .local v3, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v3, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 250
    .local v2, "descDoc":Lorg/w3c/dom/Document;
    invoke-static {}, Lorg/apache/xml/security/Init;->init()V

    .line 252
    const-string v10, "http://www.w3.org/2001/10/xml-exc-c14n#"

    .line 251
    invoke-static {v10}, Lorg/apache/xml/security/c14n/Canonicalizer;->getInstance(Ljava/lang/String;)Lorg/apache/xml/security/c14n/Canonicalizer;

    move-result-object v10

    .line 253
    invoke-interface {v2}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v11

    .line 252
    invoke-virtual {v10, v11}, Lorg/apache/xml/security/c14n/Canonicalizer;->canonicalizeSubtree(Lorg/w3c/dom/Node;)[B

    move-result-object v0

    .line 256
    .local v0, "canonicalizedXml":[B
    new-instance v10, Lorg/xml/sax/InputSource;

    new-instance v11, Ljava/io/ByteArrayInputStream;

    invoke-direct {v11, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v10, v11}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3, v10}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 257
    new-instance v7, Lorg/apache/xml/security/signature/XMLSignature;

    const-string v10, ""

    .line 258
    const-string v11, "http://www.w3.org/2000/09/xmldsig#rsa-sha1"

    const-string v12, "http://www.w3.org/2001/10/xml-exc-c14n#"

    .line 257
    invoke-direct {v7, v2, v10, v11, v12}, Lorg/apache/xml/security/signature/XMLSignature;-><init>(Lorg/w3c/dom/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .local v7, "signedDoc":Lorg/apache/xml/security/signature/XMLSignature;
    new-instance v8, Lorg/apache/xml/security/transforms/Transforms;

    invoke-direct {v8, v2}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Document;)V

    .line 261
    .local v8, "transforms":Lorg/apache/xml/security/transforms/Transforms;
    const-string v10, "http://www.w3.org/2000/09/xmldsig#enveloped-signature"

    invoke-virtual {v8, v10}, Lorg/apache/xml/security/transforms/Transforms;->addTransform(Ljava/lang/String;)V

    .line 262
    const-string v10, "http://www.w3.org/2001/10/xml-exc-c14n#"

    invoke-virtual {v8, v10}, Lorg/apache/xml/security/transforms/Transforms;->addTransform(Ljava/lang/String;)V

    .line 263
    const-string v10, ""

    invoke-virtual {v7, v10, v8}, Lorg/apache/xml/security/signature/XMLSignature;->addDocument(Ljava/lang/String;Lorg/apache/xml/security/transforms/Transforms;)V

    .line 265
    invoke-virtual {v7, p2}, Lorg/apache/xml/security/signature/XMLSignature;->sign(Ljava/security/Key;)V

    .line 266
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 267
    .local v6, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v7}, Lorg/apache/xml/security/signature/XMLSignature;->getElement()Lorg/w3c/dom/Element;

    move-result-object v10

    invoke-static {v10, v6}, Lorg/apache/xml/security/utils/XMLUtils;->outputDOM(Lorg/w3c/dom/Node;Ljava/io/OutputStream;)V

    .line 268
    const-string v10, "UTF-8"

    invoke-virtual {v6, v10}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_0} :catch_6

    move-result-object v10

    .line 291
    .end local v0    # "canonicalizedXml":[B
    .end local v1    # "dbfac":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v2    # "descDoc":Lorg/w3c/dom/Document;
    .end local v3    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v5    # "in":Lorg/xml/sax/InputSource;
    .end local v6    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v7    # "signedDoc":Lorg/apache/xml/security/signature/XMLSignature;
    .end local v8    # "transforms":Lorg/apache/xml/security/transforms/Transforms;
    .end local v9    # "xmlBytes":[B
    :goto_0
    return-object v10

    .line 269
    :catch_0
    move-exception v4

    .line 270
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const-string v10, "XmlSigning"

    const-string v11, "UnsupportedEncodingException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 291
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    const/4 v10, 0x0

    goto :goto_0

    .line 272
    :catch_1
    move-exception v4

    .line 273
    .local v4, "e":Lorg/apache/xml/security/c14n/CanonicalizationException;
    const-string v10, "XmlSigning"

    const-string v11, "CanonicalizationException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v4}, Lorg/apache/xml/security/c14n/CanonicalizationException;->printStackTrace()V

    goto :goto_1

    .line 275
    .end local v4    # "e":Lorg/apache/xml/security/c14n/CanonicalizationException;
    :catch_2
    move-exception v4

    .line 276
    .local v4, "e":Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;
    const-string v10, "XmlSigning"

    const-string v11, "InvalidCanonicalizerException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v4}, Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;->printStackTrace()V

    goto :goto_1

    .line 278
    .end local v4    # "e":Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;
    :catch_3
    move-exception v4

    .line 279
    .local v4, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v10, "XmlSigning"

    const-string v11, "ParserConfigurationException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-virtual {v4}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_1

    .line 281
    .end local v4    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_4
    move-exception v4

    .line 282
    .local v4, "e":Ljava/io/IOException;
    const-string v10, "XmlSigning"

    const-string v11, "IOException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 284
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 285
    .local v4, "e":Lorg/xml/sax/SAXException;
    const-string v10, "XmlSigning"

    const-string v11, "SAXException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {v4}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_1

    .line 287
    .end local v4    # "e":Lorg/xml/sax/SAXException;
    :catch_6
    move-exception v4

    .line 288
    .local v4, "e":Lorg/apache/xml/security/exceptions/XMLSecurityException;
    const-string v10, "XmlSigning"

    const-string v11, "XMLSecurityException in generateXmlSignature"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-virtual {v4}, Lorg/apache/xml/security/exceptions/XMLSecurityException;->printStackTrace()V

    goto :goto_1
.end method

.method private getKeyPair()Ljava/security/KeyPair;
    .locals 5

    .prologue
    .line 119
    const-string v3, "XmlSigning"

    const-string v4, "Enter getKeyPair"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :try_start_0
    const-string v3, "RSA"

    invoke-static {v3}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v2

    .line 123
    .local v2, "kpg":Ljava/security/KeyPairGenerator;
    const/16 v3, 0x800

    invoke-virtual {v2, v3}, Ljava/security/KeyPairGenerator;->initialize(I)V

    .line 124
    invoke-virtual {v2}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    .line 125
    .local v1, "kp":Ljava/security/KeyPair;
    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->sendPubKeyToDAP(Ljava/security/KeyPair;)V

    .line 126
    const-string v3, "XmlSigning"

    const-string v4, "Sending Private key"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v1    # "kp":Ljava/security/KeyPair;
    .end local v2    # "kpg":Ljava/security/KeyPairGenerator;
    :goto_0
    return-object v1

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v3, "XmlSigning"

    const-string v4, "Algorithm to generate private key is not present"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 131
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getKeyPair(Landroid/content/Context;)Ljava/security/KeyPair;
    .locals 24
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 137
    const-string v22, "XmlSigning"

    const-string v23, "Enter getKeyPair"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v10, 0x0

    .line 139
    .local v10, "kp":Ljava/security/KeyPair;
    const-string v22, "UPnPKey_Pref"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 142
    .local v21, "sharedPreferences":Landroid/content/SharedPreferences;
    :try_start_0
    const-string v22, "UPnPKey_Pref"

    const/16 v23, 0x0

    invoke-interface/range {v21 .. v23}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v22

    if-nez v22, :cond_1

    .line 143
    const-string v22, "XmlSigning"

    const-string v23, "There are no keys. Generate KeyPair."

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v22, "RSA"

    invoke-static/range {v22 .. v22}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v12

    .line 146
    .local v12, "kpg":Ljava/security/KeyPairGenerator;
    const/16 v22, 0x800

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/security/KeyPairGenerator;->initialize(I)V

    .line 147
    invoke-virtual {v12}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v10

    .line 148
    invoke-virtual {v10}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/security/PublicKey;->getEncoded()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v18

    .line 151
    .local v18, "publicKeyBytes":[B
    :try_start_1
    const-string v22, "publicKeyFilename"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v20

    .line 152
    .local v20, "public_out":Ljava/io/FileOutputStream;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 153
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    .line 161
    .end local v20    # "public_out":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_2
    invoke-virtual {v10}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/security/PrivateKey;->getEncoded()[B
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v14

    .line 164
    .local v14, "privateKeyBytes":[B
    :try_start_3
    const-string v22, "privateKeyFilename"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v16

    .line 165
    .local v16, "private_out":Ljava/io/FileOutputStream;
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/io/FileOutputStream;->write([B)V

    .line 166
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_2

    .line 174
    .end local v16    # "private_out":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_4
    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 175
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v22, "UPnPKey_Pref"

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 176
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 177
    const-string v22, "XmlSigning"

    const-string v23, "Generating KeyPair completed."

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v12    # "kpg":Ljava/security/KeyPairGenerator;
    .end local v14    # "privateKeyBytes":[B
    .end local v18    # "publicKeyBytes":[B
    :goto_2
    if-eqz v10, :cond_0

    .line 213
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->sendPubKeyToDAP(Ljava/security/KeyPair;)V

    .line 215
    :cond_0
    const-string v22, "XmlSigning"

    const-string v23, "Sending Private key"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v22, v10

    .line 220
    :goto_3
    return-object v22

    .line 154
    .restart local v12    # "kpg":Ljava/security/KeyPairGenerator;
    .restart local v18    # "publicKeyBytes":[B
    :catch_0
    move-exception v3

    .line 155
    .local v3, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 156
    const/16 v22, 0x0

    goto :goto_3

    .line 157
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v3

    .line 158
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 217
    .end local v3    # "e":Ljava/io/IOException;
    .end local v12    # "kpg":Ljava/security/KeyPairGenerator;
    .end local v18    # "publicKeyBytes":[B
    :catch_2
    move-exception v3

    .line 218
    .local v3, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v22, "XmlSigning"

    const-string v23, "Algorithm to generate private key is not present"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {v3}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 220
    const/16 v22, 0x0

    goto :goto_3

    .line 167
    .end local v3    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v12    # "kpg":Ljava/security/KeyPairGenerator;
    .restart local v14    # "privateKeyBytes":[B
    .restart local v18    # "publicKeyBytes":[B
    :catch_3
    move-exception v3

    .line 168
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 169
    const/16 v22, 0x0

    goto :goto_3

    .line 170
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v3

    .line 171
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 181
    .end local v3    # "e":Ljava/io/IOException;
    .end local v12    # "kpg":Ljava/security/KeyPairGenerator;
    .end local v14    # "privateKeyBytes":[B
    .end local v18    # "publicKeyBytes":[B
    :cond_1
    const-string v22, "XmlSigning"

    const-string v23, "There are Keypairs. Loading KeyPair."

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_5} :catch_2

    .line 183
    :try_start_6
    const-string v22, "publicKeyFilename"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v8

    .line 184
    .local v8, "filePublicKey":Ljava/io/FileInputStream;
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 185
    .local v6, "encodedPublicKey":[B
    invoke-virtual {v8, v6}, Ljava/io/FileInputStream;->read([B)I

    .line 186
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 188
    const-string v22, "privateKeyFilename"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v7

    .line 189
    .local v7, "filePrivateKey":Ljava/io/FileInputStream;
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    new-array v5, v0, [B

    .line 190
    .local v5, "encodedPrivateKey":[B
    invoke-virtual {v7, v5}, Ljava/io/FileInputStream;->read([B)I

    .line 191
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 193
    const-string v22, "RSA"

    invoke-static/range {v22 .. v22}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v9

    .line 194
    .local v9, "keyFactory":Ljava/security/KeyFactory;
    new-instance v19, Ljava/security/spec/X509EncodedKeySpec;

    move-object/from16 v0, v19

    invoke-direct {v0, v6}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_6 .. :try_end_6} :catch_2

    .line 196
    .local v19, "publicKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    :try_start_7
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v17

    .line 197
    .local v17, "publicKey":Ljava/security/PublicKey;
    new-instance v15, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-direct {v15, v5}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 198
    .local v15, "privateKeySpec":Ljava/security/spec/PKCS8EncodedKeySpec;
    invoke-virtual {v9, v15}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v13

    .line 199
    .local v13, "privateKey":Ljava/security/PrivateKey;
    new-instance v11, Ljava/security/KeyPair;

    move-object/from16 v0, v17

    invoke-direct {v11, v0, v13}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_7
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_7 .. :try_end_7} :catch_2

    .end local v10    # "kp":Ljava/security/KeyPair;
    .local v11, "kp":Ljava/security/KeyPair;
    move-object v10, v11

    .line 209
    .end local v5    # "encodedPrivateKey":[B
    .end local v6    # "encodedPublicKey":[B
    .end local v7    # "filePrivateKey":Ljava/io/FileInputStream;
    .end local v8    # "filePublicKey":Ljava/io/FileInputStream;
    .end local v9    # "keyFactory":Ljava/security/KeyFactory;
    .end local v11    # "kp":Ljava/security/KeyPair;
    .end local v13    # "privateKey":Ljava/security/PrivateKey;
    .end local v15    # "privateKeySpec":Ljava/security/spec/PKCS8EncodedKeySpec;
    .end local v17    # "publicKey":Ljava/security/PublicKey;
    .end local v19    # "publicKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    .restart local v10    # "kp":Ljava/security/KeyPair;
    :goto_4
    :try_start_8
    const-string v22, "XmlSigning"

    const-string v23, "Loading KeyPair completed."

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_2

    .line 200
    .restart local v5    # "encodedPrivateKey":[B
    .restart local v6    # "encodedPublicKey":[B
    .restart local v7    # "filePrivateKey":Ljava/io/FileInputStream;
    .restart local v8    # "filePublicKey":Ljava/io/FileInputStream;
    .restart local v9    # "keyFactory":Ljava/security/KeyFactory;
    .restart local v19    # "publicKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    :catch_5
    move-exception v3

    .line 201
    .local v3, "e":Ljava/security/spec/InvalidKeySpecException;
    :try_start_9
    invoke-virtual {v3}, Ljava/security/spec/InvalidKeySpecException;->printStackTrace()V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_4

    .line 203
    .end local v3    # "e":Ljava/security/spec/InvalidKeySpecException;
    .end local v5    # "encodedPrivateKey":[B
    .end local v6    # "encodedPublicKey":[B
    .end local v7    # "filePrivateKey":Ljava/io/FileInputStream;
    .end local v8    # "filePublicKey":Ljava/io/FileInputStream;
    .end local v9    # "keyFactory":Ljava/security/KeyFactory;
    .end local v19    # "publicKeySpec":Ljava/security/spec/X509EncodedKeySpec;
    :catch_6
    move-exception v3

    .line 204
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 205
    const/16 v22, 0x0

    goto/16 :goto_3

    .line 206
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v3

    .line 207
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_4
.end method

.method public static getXmlSigning(Landroid/content/Context;)Lcom/samsung/android/mirrorlink/util/XmlSigning;
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 79
    const-string v0, "XmlSigning"

    const-string v1, "Enter getXmlSigning"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mXmlSigning:Lcom/samsung/android/mirrorlink/util/XmlSigning;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "XmlSigning"

    const-string v1, "Creating new instance"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/samsung/android/mirrorlink/util/XmlSigning;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/util/XmlSigning;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mXmlSigning:Lcom/samsung/android/mirrorlink/util/XmlSigning;

    .line 84
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mXmlSigning:Lcom/samsung/android/mirrorlink/util/XmlSigning;

    return-object v0
.end method

.method private sendPubKeyToDAP(Ljava/security/KeyPair;)V
    .locals 3
    .param p1, "kp"    # Ljava/security/KeyPair;

    .prologue
    .line 225
    const-string v1, "XmlSigning"

    const-string v2, "sending public key to DAP"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v1

    invoke-interface {v1}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v0

    .line 227
    .local v0, "key":[B
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 229
    const-string v1, "XmlSigning"

    const-string v2, "AcsDeviceMngr is not null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getJAcsDeviceMngr()Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->sendUpnpComponentPublicKey([BI)V

    .line 233
    :cond_0
    return-void
.end method


# virtual methods
.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    const-string v0, "XmlSigning"

    const-string v1, "deinit - XmlSigning"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sput-object v2, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mKeyPair:Ljava/security/KeyPair;

    .line 91
    sput-object v2, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mXmlSigning:Lcom/samsung/android/mirrorlink/util/XmlSigning;

    .line 93
    return-void
.end method

.method public prepareSignedXml(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "xmlString"    # Ljava/lang/String;
    .param p2, "appendTag"    # Ljava/lang/String;

    .prologue
    .line 96
    const-string v5, "XmlSigning"

    const-string v6, "Enter prepareSignedXml "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v5, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mKeyPair:Ljava/security/KeyPair;

    if-nez v5, :cond_0

    .line 98
    const-string v5, "XmlSigning"

    const-string v6, "KeyPair not initialized "

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .end local p1    # "xmlString":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 101
    .restart local p1    # "xmlString":Ljava/lang/String;
    :cond_0
    sget-object v5, Lcom/samsung/android/mirrorlink/util/XmlSigning;->mKeyPair:Ljava/security/KeyPair;

    invoke-virtual {v5}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v2

    .line 102
    .local v2, "pKey":Ljava/security/PrivateKey;
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/mirrorlink/util/XmlSigning;->generateXmlSignature(Ljava/lang/String;Ljava/security/PrivateKey;)Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "signed":Ljava/lang/String;
    invoke-virtual {p1, p2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    const-string v6, "<\\"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v1, v5, v6

    .line 104
    .local v1, "index":I
    if-gtz v1, :cond_1

    .line 106
    const-string v5, "XmlSigning"

    const-string v6, "Could not sign the xml. Hence returning input xml"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "buff":Ljava/lang/StringBuffer;
    invoke-virtual {v0, v1, v3}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 112
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 113
    .local v4, "signedXml":Ljava/lang/String;
    const-string v5, "XmlSigning"

    const-string v6, "Exit prepareSignedXml"

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v4

    .line 114
    goto :goto_0
.end method
