.class public Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AcsDeviceMngr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AcsDeviceEvtReceiver"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Landroid/os/Handler;)V
    .locals 2
    .param p2, "h"    # Landroid/os/Handler;

    .prologue
    .line 2198
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2199
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceEvtReceiver.AcsDeviceEvtReceiver() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2201
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->mHandler:Landroid/os/Handler;

    .line 2203
    const-string v0, "TMSDevMngr"

    const-string v1, "AcsDeviceEvtReceiver.AcsDeviceEvtReceiver() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    .line 2212
    const-string v8, "TMSDevMngr"

    const-string v9, "AcsDeviceEvtReceiver.onReceive() - Enter"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2214
    .local v0, "action":Ljava/lang/String;
    const/4 v3, -0x1

    .line 2215
    .local v3, "deviceEvent":I
    const/4 v4, 0x0

    .line 2217
    .local v4, "eventValue":I
    if-nez v0, :cond_0

    .line 2219
    const-string v8, "TMSDevMngr"

    .line 2220
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): action is Null!"

    .line 2219
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400
    :goto_0
    return-void

    .line 2224
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    if-eqz v8, :cond_11

    .line 2227
    const-string v8, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2229
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_2

    .line 2231
    const-string v8, "TMSDevMngr"

    .line 2232
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): Screen OFF event"

    .line 2231
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2233
    const/4 v3, 0x2

    .line 2234
    const/4 v4, 0x1

    .line 2383
    :cond_1
    :goto_1
    if-lez v3, :cond_10

    .line 2385
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3, v4, v12}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    .line 2386
    .local v5, "m":Landroid/os/Message;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v7

    .line 2387
    .local v7, "ret":Z
    const-string v8, "TMSDevMngr"

    .line 2388
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "handleMessage() - ret value is "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2389
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2388
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2387
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399
    .end local v5    # "m":Landroid/os/Message;
    .end local v7    # "ret":Z
    :goto_2
    const-string v8, "TMSDevMngr"

    const-string v9, "AcsDeviceEvtReceiver.onReceive() - Exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2238
    :cond_2
    const-string v8, "TMSDevMngr"

    .line 2239
    const-string v9, "handleMessage() -no event registered for screen saver "

    .line 2238
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2242
    :cond_3
    const-string v8, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2244
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_4

    .line 2246
    const-string v8, "TMSDevMngr"

    .line 2247
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): Screen ON event"

    .line 2246
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2248
    const/4 v3, 0x2

    .line 2249
    const/4 v4, 0x0

    .line 2250
    goto :goto_1

    .line 2252
    :cond_4
    const-string v8, "TMSDevMngr"

    .line 2253
    const-string v9, "handleMessage() -no event registered for screen saver "

    .line 2252
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2257
    :cond_5
    const-string v8, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2259
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_6

    .line 2261
    const-string v8, "TMSDevMngr"

    .line 2262
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): CLOSE_SYSTEM_DIALOGS event"

    .line 2261
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    const-string v8, "reason"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2264
    .local v6, "reason":Ljava/lang/String;
    if-eqz v6, :cond_1

    const-string v8, "lock"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2272
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Z)V

    .line 2273
    const-string v8, "TMSDevMngr"

    .line 2274
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): device is LOCked"

    .line 2273
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2275
    const/4 v3, 0x1

    .line 2276
    const/4 v4, 0x1

    .line 2278
    goto/16 :goto_1

    .line 2280
    .end local v6    # "reason":Ljava/lang/String;
    :cond_6
    const-string v8, "TMSDevMngr"

    .line 2281
    const-string v9, "handleMessage() -no event registered for device lock "

    .line 2280
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2284
    :cond_7
    const-string v8, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2286
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_8

    .line 2288
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mIsDeviceLock:Z
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$3(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2290
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-static {v8, v12}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$2(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;Z)V

    .line 2291
    const-string v8, "TMSDevMngr"

    .line 2292
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): device is UN-LOCked"

    .line 2291
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293
    const/4 v3, 0x1

    .line 2294
    const/4 v4, 0x0

    .line 2297
    goto/16 :goto_1

    .line 2299
    :cond_8
    const-string v8, "TMSDevMngr"

    .line 2300
    const-string v9, "handleMessage() -no event registered for device lock "

    .line 2299
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2305
    :cond_9
    const-string v8, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2307
    const-string v8, "TMSDevMngr"

    .line 2308
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): CONFIGURATION_CHANGED event"

    .line 2307
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2309
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mCxt:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$4(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 2310
    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 2311
    .local v1, "config":Landroid/content/res/Configuration;
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit8 v8, v8, 0x8

    if-eqz v8, :cond_a

    .line 2313
    iget v8, v1, Landroid/content/res/Configuration;->uiMode:I

    const/16 v9, 0x23

    if-ne v8, v9, :cond_b

    .line 2315
    const-string v8, "TMSDevMngr"

    .line 2316
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): ACS_NIGHT_MODE_STATUS_ON event"

    .line 2315
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317
    const/16 v3, 0x8

    .line 2318
    const/4 v4, 0x1

    .line 2328
    :cond_a
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mEvts:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$1(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    and-int/lit16 v8, v8, 0x100

    if-eqz v8, :cond_1

    .line 2330
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # invokes: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->getOrientation()I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$5(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v2

    .line 2332
    .local v2, "currentOrientation":I
    const-string v8, "TMSDevMngr"

    .line 2333
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AcsDeviceEvtReceiver.onReceive(): mPrevConfiguration oreintation"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2334
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$6(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2333
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2332
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335
    const-string v8, "TMSDevMngr"

    .line 2336
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AcsDeviceEvtReceiver.onReceive(): current Configuration oreintation"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2337
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2336
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2335
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2339
    const-string v8, "TMSDevMngr"

    .line 2340
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AcsDeviceEvtReceiver.onReceive(): new oreintation oreintation-1-1"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2341
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2340
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2339
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mPrevOrientation:I
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$6(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)I

    move-result v8

    if-eq v8, v2, :cond_1

    .line 2344
    const-string v8, "TMSDevMngr"

    .line 2345
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AcsDeviceEvtReceiver.onReceive(): new oreintation oreintation"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2346
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 2345
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2344
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2348
    const/16 v3, 0x100

    .line 2349
    move v4, v2

    .line 2350
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    invoke-static {v8, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$7(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;I)V

    goto/16 :goto_1

    .line 2322
    .end local v2    # "currentOrientation":I
    :cond_b
    const-string v8, "TMSDevMngr"

    .line 2323
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): ACS_NIGHT_MODE_STATUS_OFF event"

    .line 2322
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324
    const/16 v3, 0x8

    .line 2325
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 2355
    .end local v1    # "config":Landroid/content/res/Configuration;
    :cond_c
    const-string v8, "com.samsung.intent.action.SEC_PRESENTATION_START"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 2356
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2357
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2358
    const-string v8, "TMSDevMngr"

    .line 2359
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AcsDeviceEvtReceiver.onReceive(): PRESENTATION_START displayName : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "displayName"

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2358
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2360
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v8

    const-string v9, "displayName"

    invoke-virtual {p2, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-ne v8, v9, :cond_d

    .line 2361
    const/16 v3, 0x200

    .line 2362
    const/4 v4, 0x1

    .line 2363
    goto/16 :goto_1

    .line 2364
    :cond_d
    const/4 v3, -0x1

    .line 2368
    goto/16 :goto_1

    :cond_e
    const-string v8, "com.samsung.intent.action.SEC_PRESENTATION_STOP"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2369
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2370
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2371
    const-string v8, "TMSDevMngr"

    .line 2372
    const-string v9, "AcsDeviceEvtReceiver.onReceive(): PRESENTATION_STOP event"

    .line 2371
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_f

    iget-object v8, p0, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr$AcsDeviceEvtReceiver;->this$0:Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;

    # getter for: Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->mDisplay:Landroid/view/Display;
    invoke-static {v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;->access$8(Lcom/samsung/android/mirrorlink/portinginterface/AcsDeviceMngr;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v8

    const-string v9, "displayName"

    invoke-virtual {p2, v9, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-ne v8, v9, :cond_f

    .line 2374
    const/16 v3, 0x200

    .line 2375
    const/4 v4, 0x0

    .line 2376
    goto/16 :goto_1

    .line 2377
    :cond_f
    const/4 v3, -0x1

    goto/16 :goto_1

    .line 2392
    :cond_10
    const-string v8, "TMSDevMngr"

    const-string v9, "AcsDeviceMngrHandler no proper action found"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2396
    :cond_11
    const-string v8, "TMSDevMngr"

    .line 2397
    const-string v9, "ACS device Manager not registered for any events :( "

    .line 2396
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
