.class Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TmsNwkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;)V
    .locals 0

    .prologue
    .line 453
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;-><init>(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 462
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    # getter for: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->mWiFiMngr:Landroid/net/wifi/WifiManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$0(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->getWiFiIpAddress()Ljava/lang/String;

    move-result-object v0

    .line 464
    .local v0, "ipAddr":Ljava/lang/String;
    const-string v1, "TMSSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "wifiEventReceiver:onReceive():Wifi Enabled "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    if-eqz v0, :cond_0

    .line 467
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V
    invoke-static {v1, v4, v4, v0}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V

    .line 476
    .end local v0    # "ipAddr":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    const-string v1, "TMSSvc"

    const-string v2, "wifiEventReceiver:onReceive():Wifi Disabled "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager$WifiEventReceiver;->this$0:Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;

    .line 474
    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 473
    # invokes: Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->notifyEvent(IILjava/lang/String;)V
    invoke-static {v1, v4, v2, v3}, Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;->access$1(Lcom/samsung/android/mirrorlink/engine/TmsNwkManager;IILjava/lang/String;)V

    goto :goto_0
.end method
