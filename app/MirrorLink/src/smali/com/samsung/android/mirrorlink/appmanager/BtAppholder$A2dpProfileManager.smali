.class Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;
.super Ljava/lang/Object;
.source "BtAppholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "A2dpProfileManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;
    }
.end annotation


# instance fields
.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCntxt:Landroid/content/Context;

.field private mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mService:Landroid/bluetooth/BluetoothA2dp;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1
    .param p1, "cntxt"    # Landroid/content/Context;
    .param p2, "adapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 2339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2341
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mCntxt:Landroid/content/Context;

    .line 2342
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;

    invoke-direct {v0, p0, p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;-><init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 2343
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2344
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Z
    .locals 1

    .prologue
    .line 2378
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->isTmConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 2430
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Landroid/bluetooth/BluetoothA2dp;
    .locals 1

    .prologue
    .line 2290
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothA2dp;)V
    .locals 0

    .prologue
    .line 2290
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 2473
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 2368
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;)V
    .locals 0

    .prologue
    .line 2409
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disConnectDevices()V

    return-void
.end method

.method private callBluetoothA2dpConnect(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "a2dp"    # Landroid/bluetooth/BluetoothA2dp;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2488
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "connect"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2489
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2497
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 2490
    :catch_0
    move-exception v0

    .line 2491
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2492
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2493
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2494
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2495
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callBluetoothA2dpDisconnect(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "a2dp"    # Landroid/bluetooth/BluetoothA2dp;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2501
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2502
    :cond_0
    const-string v2, "TMSBtAppholder"

    const-string v3, "Service is not connected"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2516
    :goto_0
    return-void

    .line 2507
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "disconnect"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2508
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2509
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 2510
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2511
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2512
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2513
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2514
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callBluetoothA2dpSetPriority(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;I)V
    .locals 7
    .param p1, "a2dp"    # Landroid/bluetooth/BluetoothA2dp;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "priority"    # I

    .prologue
    .line 2522
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setPriority"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2523
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2531
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 2524
    :catch_0
    move-exception v0

    .line 2525
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2526
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2527
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2528
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2529
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothA2dpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private connect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2432
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    if-nez v4, :cond_0

    .line 2434
    const-string v4, "TMSBtAppholder"

    const-string v5, "A2dpProfileManager : connect :(mService == null) "

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466
    :goto_0
    return-void

    .line 2438
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v3

    .line 2439
    .local v3, "sinks":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v3, :cond_1

    .line 2441
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2447
    :cond_1
    const-string v4, "TMSBtAppholder"

    const-string v5, " A2dpProfileManager connectSink"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    const/4 v1, 0x0

    .line 2450
    .local v1, "priority":Ljava/lang/reflect/Field;
    :try_start_0
    const-class v4, Landroid/bluetooth/BluetoothProfile;

    const-string v5, "PRIORITY_ON"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 2451
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2453
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    invoke-direct {p0, v4, p1, v5}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->callBluetoothA2dpSetPriority(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;I)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2465
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    invoke-direct {p0, v4, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->callBluetoothA2dpConnect(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    .line 2441
    .end local v1    # "priority":Ljava/lang/reflect/Field;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 2443
    .local v2, "sink":Landroid/bluetooth/BluetoothDevice;
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->callBluetoothA2dpDisconnect(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_1

    .line 2454
    .end local v2    # "sink":Landroid/bluetooth/BluetoothDevice;
    .restart local v1    # "priority":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 2456
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 2461
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 2463
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    .line 2457
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v0

    .line 2459
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method private disConnectDevices()V
    .locals 4

    .prologue
    .line 2411
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 2412
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v1, :cond_1

    .line 2414
    const-string v2, "TMSBtAppholder"

    const-string v3, "disConnectDevices : getConnectedDevices() Failed"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2422
    :cond_0
    return-void

    .line 2418
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 2420
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method

.method private disconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2475
    if-nez p1, :cond_0

    .line 2477
    const-string v0, "TMSBtAppholder"

    const-string v1, " A2dpProfileManager disconnect device is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2482
    :goto_0
    return-void

    .line 2480
    :cond_0
    const-string v0, "TMSBtAppholder"

    const-string v1, " A2dpProfileManager disconnect"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2481
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->callBluetoothA2dpDisconnect(Landroid/bluetooth/BluetoothA2dp;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method

.method private getConnectedDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2370
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_0

    .line 2372
    const-string v0, "TMSBtAppholder"

    const-string v1, "getConnectedDevices : (mService == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    const/4 v0, 0x0

    .line 2375
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private isTmConnected()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2380
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 2381
    .local v1, "listA2dpConnected":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v1, :cond_0

    .line 2383
    const-string v3, "TMSBtAppholder"

    const-string v4, " A2dpProfileManager (listA2dpConnected == null)"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403
    :goto_0
    return v2

    .line 2386
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2402
    const-string v3, "TMSBtAppholder"

    const-string v4, " A2dpProfileManager isTmConnected false"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2386
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 2388
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_1

    .line 2390
    const-string v4, "TMSBtAppholder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " A2dpProfileManager.isTmConnected : TM Addr "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 2391
    const-string v6, " Name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2390
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2392
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2394
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2396
    const-string v2, "TMSBtAppholder"

    const-string v3, " A2dpProfileManager isTmConnected true"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2397
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public deinit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2356
    const-string v0, "TMSBtAppholder"

    const-string v1, "A2DP deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 2359
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mService:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 2361
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    if-eqz v0, :cond_1

    .line 2362
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager$A2dpListener;->deinit()V

    .line 2364
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 2365
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2366
    return-void
.end method

.method public init()V
    .locals 4

    .prologue
    .line 2348
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 2350
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$A2dpProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 2352
    :cond_0
    return-void
.end method

.method public onStateChnaged()V
    .locals 2

    .prologue
    .line 2336
    const-string v0, "TMSBtAppholder"

    const-string v1, " A2dpProfileManager : onStateChnaged"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2337
    return-void
.end method
