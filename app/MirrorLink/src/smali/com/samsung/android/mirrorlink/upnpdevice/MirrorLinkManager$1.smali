.class Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;
.super Ljava/lang/Object;
.source "MirrorLinkManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 111
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-static {p2}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$0(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;)V

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    # getter for: Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$3(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    # getter for: Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsService:Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$3(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    # getter for: Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
    invoke-static {v2}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$1(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;->registerListener(Lcom/samsung/android/mirrorlink/acms/api/IAcmsListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$0(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;Lcom/samsung/android/mirrorlink/acms/api/IAcmsManager;)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    # getter for: Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$1(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    # getter for: Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->mAcmsListener:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$1(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;)Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;->clearMirrorlinkMgr()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$1;->this$0:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->access$2(Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager$AcmsListener;)V

    .line 106
    return-void
.end method
