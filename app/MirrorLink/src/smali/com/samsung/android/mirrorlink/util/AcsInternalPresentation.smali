.class public Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;
.super Landroid/app/InternalPresentation;
.source "AcsInternalPresentation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;
    }
.end annotation


# static fields
.field private static final MSG_SHOW_ANIMATION_SCREEN:I = 0x2

.field private static final MSG_SHOW_BLACK_SCREEN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TMSIntPresent"


# instance fields
.field private isMLRunningViewShowing:Z

.field private mBlackScreenView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMLRunningView:Landroid/view/View;

.field private mPowerDisconnectedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;)V
    .locals 1
    .param p1, "outerContext"    # Landroid/content/Context;
    .param p2, "display"    # Landroid/view/Display;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;I)V
    .locals 4
    .param p1, "outerContext"    # Landroid/content/Context;
    .param p2, "display"    # Landroid/view/Display;
    .param p3, "theme"    # I

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/app/InternalPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    .line 30
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    .line 31
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    .line 32
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    .line 33
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isMLRunningViewShowing:Z

    .line 36
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mPowerDisconnectedReceiver:Landroid/content/BroadcastReceiver;

    .line 46
    const-string v0, "TMSIntPresent"

    const-string v1, "AcsInternalPresentation Constuctor()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setWindowParams()V

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setMLRunningScreenView()V

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setBlackScreenView()V

    .line 54
    new-instance v0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;-><init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$PowerDisconnectedReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mPowerDisconnectedReceiver:Landroid/content/BroadcastReceiver;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mPowerDisconnectedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 57
    new-instance v0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation$1;-><init>(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    .line 77
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;Z)V
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isMLRunningViewShowing:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isMLRunningViewShowing:Z

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->showMirrorLinkRunningScreen()V

    return-void
.end method

.method private getResourceId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defType"    # Ljava/lang/String;

    .prologue
    .line 262
    const/4 v2, 0x0

    .line 264
    .local v2, "retId":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .line 265
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, p2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 269
    .end local v1    # "res":Landroid/content/res/Resources;
    :goto_0
    return v2

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private setBlackScreenView()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 244
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 249
    return-void
.end method

.method private setMLRunningScreenView()V
    .locals 4

    .prologue
    .line 252
    const-string v2, "TMSIntPresent"

    const-string v3, "setMLRunningScreenView()"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    if-nez v2, :cond_0

    .line 254
    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    .line 256
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 257
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const-string v2, "connected"

    const-string v3, "layout"

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getResourceId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 258
    .local v1, "layoutId":I
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    .line 259
    return-void
.end method

.method private setWindowParams()V
    .locals 7

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 222
    .local v3, "window":Landroid/view/Window;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 223
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 227
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    const-string v5, "TYPE_DISPLAY_OVERLAY"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 229
    .local v0, "dispOverlay":Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 230
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x480

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 232
    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 240
    .end local v0    # "dispOverlay":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    const-string v4, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setWindowParams : NoSuchFieldException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setWindowParams : IllegalArgumentException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 237
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 238
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setWindowParams : IllegalAccessException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showMirrorLinkRunningScreen()V
    .locals 8

    .prologue
    .line 186
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-nez v6, :cond_2

    .line 187
    const-string v6, "TMSIntPresent"

    const-string v7, "showMirrorLinkRunningScreen() using previous object"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_0
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    invoke-virtual {p0, v6}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setContentView(Landroid/view/View;)V

    .line 196
    const-string v6, "mirrorLinkStarted"

    const-string v7, "id"

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getResourceId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 197
    .local v5, "tv":Landroid/widget/TextView;
    if-eqz v5, :cond_0

    .line 199
    const-string v6, "system/fonts/SECRobotoLight-Bold.ttf"

    invoke-static {v6}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 200
    .local v2, "mSECRobotoLightBoldFont":Landroid/graphics/Typeface;
    if-nez v2, :cond_3

    .line 201
    const-string v6, "TMSIntPresent"

    const-string v7, "There is no system font - SECRoboto-bold"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget-object v6, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 209
    .end local v2    # "mSECRobotoLightBoldFont":Landroid/graphics/Typeface;
    :cond_0
    :goto_1
    const-string v6, "wizard_image_connected_loading"

    const-string v7, "id"

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getResourceId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 210
    .local v1, "imageViewId":I
    const-string v6, "rotate"

    const-string v7, "anim"

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->getResourceId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 212
    .local v0, "animId":I
    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 213
    .local v3, "myImage":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 214
    .local v4, "myRotation":Landroid/view/animation/Animation;
    if-eqz v3, :cond_1

    .line 215
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 217
    :cond_1
    return-void

    .line 189
    .end local v0    # "animId":I
    .end local v1    # "imageViewId":I
    .end local v3    # "myImage":Landroid/widget/ImageView;
    .end local v4    # "myRotation":Landroid/view/animation/Animation;
    .end local v5    # "tv":Landroid/widget/TextView;
    :cond_2
    const-string v6, "TMSIntPresent"

    const-string v7, "showMirrorLinkRunningScreen() using new object"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mMLRunningView:Landroid/view/View;

    .line 191
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setMLRunningScreenView()V

    goto :goto_0

    .line 204
    .restart local v2    # "mSECRobotoLightBoldFont":Landroid/graphics/Typeface;
    .restart local v5    # "tv":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1
.end method


# virtual methods
.method public deinit()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    const-string v0, "TMSIntPresent"

    const-string v1, "Enter deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 84
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mPowerDisconnectedReceiver:Landroid/content/BroadcastReceiver;

    .line 85
    return-void
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Landroid/app/InternalPresentation;->dismiss()V

    .line 107
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 112
    const-string v0, "TMSIntPresent"

    const-string v1, "acsInternalPresentation is stopped"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->dismiss()V

    .line 114
    invoke-super {p0}, Landroid/app/InternalPresentation;->onStop()V

    .line 115
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 121
    const-string v3, "TMSIntPresent"

    const-string v5, "onTouchEvent"

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v5, "getDisplayId"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 126
    .local v2, "getDisplayId":Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 127
    .local v0, "dispId":I
    if-nez v0, :cond_0

    .line 128
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 135
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 155
    const/4 v3, 0x1

    .line 169
    .end local v0    # "dispId":I
    .end local v2    # "getDisplayId":Ljava/lang/reflect/Method;
    :goto_0
    return v3

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e1":Ljava/lang/NoSuchMethodException;
    const-string v3, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onTouchEvent : NoSuchMethodException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .end local v1    # "e1":Ljava/lang/NoSuchMethodException;
    :cond_0
    :goto_1
    move v3, v4

    .line 169
    goto :goto_0

    .line 159
    :catch_1
    move-exception v1

    .line 160
    .local v1, "e1":Ljava/lang/IllegalArgumentException;
    const-string v3, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onTouchEvent : IllegalArgumentException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 161
    .end local v1    # "e1":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 162
    .local v1, "e1":Ljava/lang/IllegalAccessException;
    const-string v3, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onTouchEvent : IllegalAccessException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 163
    .end local v1    # "e1":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 164
    .local v1, "e1":Ljava/lang/reflect/InvocationTargetException;
    const-string v3, "TMSIntPresent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onTouchEvent : InvocationTargetException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public show()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->show(Z)V

    .line 90
    return-void
.end method

.method public show(Z)V
    .locals 4
    .param p1, "runningImage"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 94
    if-eqz p1, :cond_1

    .line 95
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v3, v1, v2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    invoke-super {p0}, Landroid/app/InternalPresentation;->show()V

    .line 102
    :cond_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v3, v1, v2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public showInternalBlackScreen()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 175
    const-string v0, "TMSIntPresent"

    const-string v1, "showInternalBlackScreen() using previous object"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setContentView(Landroid/view/View;)V

    .line 182
    return-void

    .line 177
    :cond_0
    const-string v0, "TMSIntPresent"

    const-string v1, "showInternalBlackScreen() using new object"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->mBlackScreenView:Landroid/view/View;

    .line 179
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/util/AcsInternalPresentation;->setBlackScreenView()V

    goto :goto_0
.end method
