.class public Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;
.super Ljava/lang/Object;
.source "CertifiedAppListGenerator.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSCertifiedAppListGenerator"


# instance fields
.field private mAcmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

.field private mAppListMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCntxt:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "cntxt"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "TMSCertifiedAppListGenerator"

    const-string v1, "CertifiedAppListGenerator.UserAppHolder() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mCntxt:Landroid/content/Context;

    .line 23
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mCntxt:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAcmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    .line 24
    const-string v0, "TMSCertifiedAppListGenerator"

    const-string v1, "CertifiedAppListGenerator.UserAppHolder() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public deinit()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAppListMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public getAcmsAppsManager()Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAcmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    return-object v0
.end method

.method public getAppListMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAppListMap:Ljava/util/Map;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "TMSCertifiedAppListGenerator"

    const-string v1, "CertifiedAppListGenerator.init() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAcmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->queryApps()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAppListMap:Ljava/util/Map;

    .line 30
    const-string v0, "TMSCertifiedAppListGenerator"

    const-string v1, "CertifiedAppListGenerator.init() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public removeUnsupportedApp(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string v0, "TMSCertifiedAppListGenerator"

    const-string v1, "Enter removeUnsupportedVncApp"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAppListMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAppListMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mAcmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/AcmsAppsManager;->removeUnsupportedApp(Ljava/lang/String;)V

    .line 43
    :cond_0
    return-void
.end method

.method public startApp(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v2, "TMSCertifiedAppListGenerator"

    const-string v3, "CertifiedAppListGenerator.startApp() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    .local v1, "filterIntent":Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 58
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/CertifiedAppListGenerator;->mCntxt:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    const-string v2, "TMSCertifiedAppListGenerator"

    const-string v3, "CertifiedAppListGenerator.startApp() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "TMSCertifiedAppListGenerator"

    .line 61
    const-string v3, "Exception caught: can not Find the activity"

    .line 60
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 63
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method
