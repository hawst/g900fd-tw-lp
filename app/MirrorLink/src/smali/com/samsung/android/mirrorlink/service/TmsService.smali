.class public Lcom/samsung/android/mirrorlink/service/TmsService;
.super Landroid/app/Service;
.source "TmsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;,
        Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;
    }
.end annotation


# static fields
.field private static final MIRRORLINK_DBVIEWER_SERVICE_INTENT:Ljava/lang/String; = "com.samsung.android.mirrorlink.Dbviewer"

.field private static final MIRRORLINK_TM_SERVICE_INTENT:Ljava/lang/String; = "com.samsung.android.mirrorlink.service.TMS_SERVICE_ACTION"

.field static final MSG_REGISTER_CLIENT:I = 0x1

.field static final MSG_SELF_NOTIFY_DISPLAY_TO_STOP:I = 0x15

.field static final MSG_SERVICE_STARTED:I = 0x3

.field static final MSG_SERVICE_STOPPED:I = 0x4

.field static final MSG_UNREGISTER_CLIENT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TMSSvc"


# instance fields
.field private isDeinitInProgress:Z

.field private isInitPending:Z

.field private isStartServiceCalled:Z

.field private mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

.field mClients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field final mMessenger:Landroid/os/Messenger;

.field private mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

.field private mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

.field private mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

.field private mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

.field mValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mClients:Ljava/util/ArrayList;

    .line 41
    iput v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mValue:I

    .line 363
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/mirrorlink/service/TmsService$IncomingHandler;-><init>(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMessenger:Landroid/os/Messenger;

    .line 19
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/service/TmsService;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/service/TmsService;Z)V
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/service/TmsService;)V
    .locals 0

    .prologue
    .line 475
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->init()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/service/TmsService;Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/service/TmsService;)V
    .locals 0

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->deinit()V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/service/TmsService;)V
    .locals 0

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->destroy()V

    return-void
.end method

.method private declared-synchronized deinit()V
    .locals 2

    .prologue
    .line 498
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.deinit() - enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    if-eqz v0, :cond_0

    .line 502
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.deinit() - mSvcManager.deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->deinit()I

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    if-eqz v0, :cond_1

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    .line 512
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isStartServiceCalled:Z

    .line 513
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.deinit() - exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    monitor-exit p0

    return-void

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private destroy()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 562
    const-string v1, "TMSSvc"

    const-string v2, "TmsService.destroy() - Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v1, "TMSSvc"

    const-string v2, "TmsService.destroy() - Enter mSvcManager cleaned"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    if-eqz v1, :cond_0

    .line 567
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->clean()V

    .line 568
    iput-object v4, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    .line 571
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    .line 572
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    if-eqz v1, :cond_1

    .line 573
    const-string v1, "TMSSvc"

    const-string v2, "Is pending is set to true. service can start now"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iput-boolean v3, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    .line 575
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->startService()V

    .line 599
    :goto_0
    const-string v1, "TMSSvc"

    const-string v2, "TmsService.destroy() - Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    return-void

    .line 579
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMessenger:Landroid/os/Messenger;

    const/4 v2, 0x0

    const/16 v3, 0x15

    .line 580
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 579
    invoke-static {v2, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    :goto_1
    const-string v1, "TMSSvc"

    const-string v2, "Calling stop self"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->stopSelf()V

    goto :goto_0

    .line 581
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private getMirrorLinkDbViewerMngr()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    if-nez v0, :cond_0

    .line 404
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->init()V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    return-object v0
.end method

.method private getMirrorLinkManager()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    if-nez v0, :cond_0

    .line 396
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->init()V

    .line 399
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    return-object v0
.end method

.method private declared-synchronized init()V
    .locals 2

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.init() - enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->init()I

    .line 493
    :cond_0
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.init() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    monitor-exit p0

    return-void

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private startService()V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    if-nez v0, :cond_0

    .line 460
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.startService() - mSvcManager = null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    new-instance v0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    if-nez v0, :cond_1

    .line 465
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.startService() - mAppStateHandler = null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    new-instance v0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;-><init>(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->setAppStateHAndler(Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;)V

    .line 469
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isStartServiceCalled:Z

    if-nez v0, :cond_2

    .line 470
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->start()V

    .line 473
    :cond_2
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 372
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onBind()- Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    if-eqz p1, :cond_2

    .line 375
    const-string v0, "com.samsung.android.MIRRORLINK_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    const-string v0, "TMSSvc"

    const-string v1, "Bind intent is for Mirrorlink manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getMirrorLinkManager()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    move-result-object v0

    .line 390
    :goto_0
    return-object v0

    .line 379
    :cond_0
    const-string v0, "com.samsung.android.mirrorlink.Dbviewer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    const-string v0, "TMSSvc"

    const-string v1, "Bind intent is for Mirrorlink DbViewer manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getMirrorLinkDbViewerMngr()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    move-result-object v0

    goto :goto_0

    .line 383
    :cond_1
    const-string v0, "com.samsung.android.mirrorlink.service.TMS_SERVICE_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 384
    const-string v0, "TMSSvc"

    const-string v1, "Bind intent is for TMS Service"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    .line 389
    :cond_2
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onBind()- Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 90
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->incrementCounter()V

    .line 91
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onCreate() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v0, "TMSSvc"

    const-string v1, "Incremented monitor value "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isStartServiceCalled:Z

    .line 94
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    .line 95
    iput-boolean v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    .line 96
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onCreate() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 418
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onDestroy() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-static {}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->stopLogger()V

    .line 423
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 425
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->deinit()V

    .line 430
    :cond_1
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    .line 431
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->deinit()V

    .line 434
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    .line 447
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->decrementCounter()V

    .line 448
    const-string v0, "TMSSvc"

    const-string v1, "Decremented monitor value "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onDestroy() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 452
    invoke-static {}, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->killTMServiceProcess()V

    .line 453
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 545
    const-string v0, "TMSSvc"

    const-string v1, "On rebind Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    if-eqz p1, :cond_1

    .line 547
    const-string v0, "com.samsung.android.MIRRORLINK_SERVICE"

    .line 548
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 547
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 548
    if-eqz v0, :cond_0

    .line 549
    const-string v0, "TMSSvc"

    const-string v1, "Re Bind intent is for Mirrorlink manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getMirrorLinkManager()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    .line 552
    :cond_0
    const-string v0, "com.samsung.android.mirrorlink.Dbviewer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 553
    const-string v0, "TMSSvc"

    .line 554
    const-string v1, "Re Bind intent is for Mirrorlink DbViewer manager"

    .line 553
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getMirrorLinkDbViewerMngr()Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    .line 559
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 560
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    .line 261
    const-string v0, "TMSSvc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TmsService.onStartCommand() - Enter startId= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isDeinitInProgress:Z

    if-eqz v0, :cond_0

    .line 264
    const-string v0, "TMSSvc"

    const-string v1, "Deinit in progress. Not starting the service"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isInitPending:Z

    .line 305
    :goto_0
    return v3

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    if-nez v0, :cond_1

    .line 270
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onStartCommand() - mSvcManager = null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    new-instance v0, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    if-nez v0, :cond_2

    .line 275
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onStartCommand() - mAppStateHandler = null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    new-instance v0, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;-><init>(Lcom/samsung/android/mirrorlink/service/TmsService;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mSvcManager:Lcom/samsung/android/mirrorlink/service/TmsSvcManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/service/TmsSvcManager;->setAppStateHAndler(Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;)V

    .line 280
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isStartServiceCalled:Z

    if-nez v0, :cond_5

    .line 281
    const-string v0, "TMSSvc"

    const-string v1, "Is start service is not called.  So starting service."

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    if-eqz p1, :cond_3

    .line 283
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 285
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->start()V

    .line 303
    :cond_4
    :goto_1
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onStartCommand() - START_NOT_STICKY Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    if-eqz v0, :cond_4

    .line 288
    const-string v0, "TMSSvc"

    const-string v1, "Tethering again"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->doTether()Z

    goto :goto_1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 517
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onUnbind() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    if-eqz p1, :cond_1

    .line 525
    const-string v0, "com.samsung.android.MIRRORLINK_SERVICE"

    .line 526
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 526
    if-eqz v0, :cond_0

    .line 527
    const-string v0, "TMSSvc"

    const-string v1, "Un Bind intent is for Mirrorlink manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkManager;->deinit()V

    .line 533
    :cond_0
    const-string v0, "com.samsung.android.mirrorlink.Dbviewer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    const-string v0, "TMSSvc"

    const-string v1, "Un Bind intent is for Db viewer manager"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mMirrorlinkDbViewerManager:Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/MirrorLinkDbViewerMngr;->deinit()V

    .line 540
    :cond_1
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.onUnbind() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    const/4 v0, 0x1

    return v0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 104
    const-string v0, "TMSSvc"

    const-string v1, "TmsService start() - Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    if-nez v0, :cond_0

    .line 131
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.start() - mUsbNwkUtility created"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/service/TmsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mAppStateHandler:Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->setStateHandler(Lcom/samsung/android/mirrorlink/service/TmsService$AppStateHandler;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->mUsbNwkUtility:Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/service/UsbNwkUtility;->doTether()Z

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mirrorlink/service/TmsService;->isStartServiceCalled:Z

    .line 139
    const-string v0, "TMSSvc"

    const-string v1, "TmsService.start() - Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method
