.class Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager$1;
.super Landroid/os/RemoteCallbackList;
.source "DataServicesManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Lcom/mirrorlink/android/commonapi/IDataServicesListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager$1;->this$0:Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager;

    .line 28
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/mirrorlink/android/commonapi/IDataServicesListener;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DataServicesManager$1;->onCallbackDied(Lcom/mirrorlink/android/commonapi/IDataServicesListener;Ljava/lang/Object;)V

    return-void
.end method

.method public onCallbackDied(Lcom/mirrorlink/android/commonapi/IDataServicesListener;Ljava/lang/Object;)V
    .locals 2
    .param p1, "callback"    # Lcom/mirrorlink/android/commonapi/IDataServicesListener;
    .param p2, "packName"    # Ljava/lang/Object;

    .prologue
    .line 32
    const-string v0, "TMSDataServicesManager"

    const-string v1, "DataServicesManager.onCallbackDied()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-super {p0, p1}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;)V

    .line 34
    return-void
.end method
