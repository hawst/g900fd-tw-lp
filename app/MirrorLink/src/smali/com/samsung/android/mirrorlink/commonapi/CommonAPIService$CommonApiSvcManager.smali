.class public Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;
.super Lcom/mirrorlink/android/commonapi/ICommonAPIService$Stub;
.source "CommonAPIService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CommonApiSvcManager"
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TMSCommonApiSvcManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Landroid/content/Context;)V
    .locals 2
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 677
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-direct {p0}, Lcom/mirrorlink/android/commonapi/ICommonAPIService$Stub;-><init>()V

    .line 678
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "Enter Constructor CommonAPIService"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->mContext:Landroid/content/Context;

    .line 682
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;

    invoke-direct {v0, p1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;-><init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)V

    invoke-static {p1, v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$2(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$NativeEvtHandler;)V

    .line 687
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "Exit Constructor CommonAPIService"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    return-void
.end method


# virtual methods
.method public applicationStarted(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "commonApiLevel"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 722
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.applicationStarted()  "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 725
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    .line 726
    const-string v1, "CommonAPIService.applicationStarted() : Either mTmsAppManager or packageName is null"

    .line 724
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :goto_0
    return-void

    .line 730
    :cond_1
    const-string v0, "TMSCommonApiSvcManager"

    .line 731
    const-string v1, "CommonAPIService.applicationStarted(): calling mTmsAppManager.onApplicationStarted()"

    .line 729
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onApplicationStarted(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public applicationStopping(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 740
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.applicationStopping()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    invoke-virtual {p0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->isTmsEngineStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 756
    :goto_0
    return-void

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_2

    .line 747
    :cond_1
    const-string v0, "TMSCommonApiSvcManager"

    .line 748
    const-string v1, "CommonAPIService.applicationStopping() : Either mTmsAppManager or packageName is null"

    .line 746
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 752
    :cond_2
    const-string v0, "TMSCommonApiSvcManager"

    .line 753
    const-string v1, "CommonAPIService.applicationStopping(): calling mTmsAppManager.onApplicationStopping()"

    .line 751
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->onApplicationStopping(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAppPackageName()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 890
    const/4 v3, 0x0

    .line 891
    .local v3, "packname":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->mContext:Landroid/content/Context;

    .line 892
    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 891
    check-cast v0, Landroid/app/ActivityManager;

    .line 894
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 896
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    .line 898
    .local v2, "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v4, 0x0

    .line 899
    .local v4, "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-eqz v2, :cond_1

    .line 900
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 913
    :cond_1
    :goto_0
    if-nez v4, :cond_3

    .line 914
    const-string v6, "TMSCommonApiSvcManager"

    .line 915
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getAppPackageName No matching processl PID "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 916
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 915
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 914
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    .end local v2    # "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :goto_1
    return-object v5

    .line 900
    .restart local v2    # "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .restart local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 901
    .local v1, "appInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v7, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    if-ne v7, v8, :cond_0

    .line 902
    const-string v6, "TMSCommonApiSvcManager"

    .line 903
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getAppPackageName successful Pid "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 904
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 903
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 902
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    move-object v4, v1

    .line 906
    const-string v6, "TMSCommonApiSvcManager"

    .line 907
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getAppPackageName successful processinfo "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 908
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 907
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 906
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 920
    .end local v1    # "appInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_3
    iget-object v6, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    if-nez v6, :cond_4

    .line 921
    const-string v6, "TMSCommonApiSvcManager"

    .line 922
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getAppPackageName packageList is null PID "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 923
    iget v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 924
    iget-object v8, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 922
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 921
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 927
    :cond_4
    const-string v5, "TMSCommonApiSvcManager"

    .line 928
    const-string v6, "getAppPackageName processInfo.pkgList is present "

    .line 927
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    iget-object v5, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v3, v5, v6

    .line 949
    const-string v5, "TMSCommonApiSvcManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getAppPackageName"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "appInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v4    # "processInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_5
    move-object v5, v3

    .line 952
    goto/16 :goto_1
.end method

.method public getAppServerService()Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;
    .locals 1

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getAppServerSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMApplicationServerService;

    move-result-object v0

    .line 1019
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioConnMngr()Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mAudioConnMngr:Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$13(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;

    move-result-object v0

    return-object v0
.end method

.method public getCertificationManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/ICertificationListener;)Lcom/mirrorlink/android/commonapi/ICertificationManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/ICertificationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 762
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getCertificationManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$4(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$4(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/ICertificationListener;)V

    .line 768
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "getCertificationManager.exit()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mCertificationManager:Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$4(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/CertificationManager;

    move-result-object v0

    return-object v0
.end method

.method public getClientProfileService()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1005
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1011
    :cond_0
    :goto_0
    return-object v0

    .line 1008
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->getmTMSServrDevc()Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMServerDevice;->getClientProfileSrvc()Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;

    move-result-object v0

    goto :goto_0
.end method

.method public getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;
    .locals 1

    .prologue
    .line 702
    invoke-static {}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v0

    return-object v0
.end method

.method public getCommonAPIServiceApiLevel()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 715
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getCommonAPIServiceApiLevel()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    const/4 v0, 0x1

    return v0
.end method

.method public getConnectionManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IConnectionListener;)Lcom/mirrorlink/android/commonapi/IConnectionManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IConnectionListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 775
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getConnectionManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$5(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$5(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IConnectionListener;)V

    .line 780
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getConnectionManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$5(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method public getContextManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IContextListener;)Lcom/mirrorlink/android/commonapi/IContextManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IContextListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 787
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getContextManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$6(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$6(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IContextListener;)V

    .line 793
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getContextManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$6(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    move-result-object v0

    return-object v0
.end method

.method public getDataConnMngr()Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDataConnMngr:Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$14(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DataConnMngr;

    move-result-object v0

    return-object v0
.end method

.method public getDataServicesManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDataServicesListener;)Lcom/mirrorlink/android/commonapi/IDataServicesManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDataServicesListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 866
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDataServicesManager()- return null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDeviceInfoManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;)Lcom/mirrorlink/android/commonapi/IDeviceInfoManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 800
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDeviceInfoManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$7(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 805
    if-eqz p2, :cond_0

    .line 806
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$7(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceInfoListener;)V

    .line 808
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDeviceInfoManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$7(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceStatusManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;)Lcom/mirrorlink/android/commonapi/IDeviceStatusManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 815
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDeviceStatusManager():Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$8(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 819
    if-eqz p2, :cond_0

    .line 820
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$8(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDeviceStatusListener;)V

    .line 822
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDisplayManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$8(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDisplayListener;)Lcom/mirrorlink/android/commonapi/IDisplayManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IDisplayListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 829
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDisplayManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$9(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$9(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IDisplayListener;)V

    .line 836
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getDisplayManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$9(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    move-result-object v0

    return-object v0
.end method

.method public getEventMappingManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IEventMappingListener;)Lcom/mirrorlink/android/commonapi/IEventMappingManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/IEventMappingListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 843
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getEventMappingManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$10(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 846
    if-eqz p2, :cond_0

    .line 847
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$10(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->registerCallBack(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/IEventMappingListener;)V

    .line 849
    :cond_0
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getEventMappingManager():exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$10(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationManager(Ljava/lang/String;Lcom/mirrorlink/android/commonapi/INotificationListener;)Lcom/mirrorlink/android/commonapi/INotificationManager;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/mirrorlink/android/commonapi/INotificationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 856
    const-string v0, "TMSCommonApiSvcManager"

    const-string v1, "CommonAPIService.getNotificationManager()"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    if-nez v0, :cond_0

    .line 998
    const/4 v0, 0x0

    .line 1000
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    goto :goto_0
.end method

.method public getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    return-object v0
.end method

.method public getUpnpCommonApiUtil()Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$15(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    .line 1027
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$3(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->getUpnpCommonApiUtil(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v1

    .line 1026
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$16(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;)V

    .line 1029
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$15(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    move-result-object v0

    return-object v0
.end method

.method public getVncCore()Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    .locals 1

    .prologue
    .line 985
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mVncCore:Lcom/samsung/android/mirrorlink/commonapi/VncCore;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$0(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/commonapi/VncCore;

    move-result-object v0

    return-object v0
.end method

.method public isTmsEngineStarted()Z
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->mTmsEngine:Lcom/samsung/android/mirrorlink/engine/TmsEngine;
    invoke-static {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$1(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    if-nez v0, :cond_0

    .line 707
    const/4 v0, 0x0

    .line 709
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyKnobSupport()V
    .locals 4

    .prologue
    .line 692
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.app.mirrorlink.sip"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 693
    .local v0, "connected":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 694
    .local v1, "ml":Landroid/os/Bundle;
    const-string v2, "mlconnected"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 695
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 696
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 697
    const-string v2, "TMSCommonApiSvcManager"

    const-string v3, "notifyKnobSupport with mlconnected"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    return-void
.end method

.method public setTmsEngine(Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V
    .locals 3
    .param p1, "tmsEngine"    # Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    .prologue
    .line 956
    const-string v1, "TMSCommonApiSvcManager"

    const-string v2, "Stub\'s : setTmsEngine"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    invoke-static {v1, p1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$11(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;Lcom/samsung/android/mirrorlink/engine/TmsEngine;)V

    .line 958
    if-eqz p1, :cond_2

    .line 959
    const-string v1, "TMSCommonApiSvcManager"

    const-string v2, "setTmsEngine"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_setup(Ljava/lang/Object;)I

    .line 964
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$12(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 965
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$12(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 978
    :cond_0
    return-void

    .line 965
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;

    .line 966
    .local v0, "notify":Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;
    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    invoke-interface {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;->handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V

    goto :goto_0

    .line 970
    .end local v0    # "notify":Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;
    :cond_2
    const-string v1, "TMSCommonApiSvcManager"

    const-string v2, "resetTmsEngine"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$12(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 972
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->this$0:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    # getter for: Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->list:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->access$12(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;

    .line 973
    .restart local v0    # "notify":Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;
    sget-object v2, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    invoke-interface {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;->handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V

    goto :goto_1
.end method
