.class public Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;
.super Ljava/lang/Object;
.source "AppIdGen.java"


# static fields
.field private static final BASE_APPID:I = 0x63

.field public static final INVALID_APPID:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "TMSAppMngr"


# instance fields
.field private usedAppIdCount:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x63

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    .line 37
    const-string v0, "TMSAppMngr"

    const-string v1, "AppIdGen.AppIdGen enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iput v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    .line 39
    return-void
.end method


# virtual methods
.method public getNewAppId()I
    .locals 3

    .prologue
    .line 43
    const-string v0, "TMSAppMngr"

    const-string v1, "AppIdGen.getNewAppId enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    .line 45
    const-string v0, "TMSAppMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppIdGen.getNewAppId exit.new appid  = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 46
    iget v2, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget v0, p0, Lcom/samsung/android/mirrorlink/appmanager/AppIdGen;->usedAppIdCount:I

    return v0
.end method
