.class public Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
.super Ljava/lang/Object;
.source "UpnpCommonApiUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Upnp/UpnpCommonApiUtil"

.field private static volatile sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;


# instance fields
.field private mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

.field private mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V
    .locals 0
    .param p1, "deviceAppManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 27
    return-void
.end method

.method public static declared-synchronized deinit()V
    .locals 3

    .prologue
    .line 37
    const-class v1, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 39
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_0
    monitor-exit v1

    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getUpnpCommonApiUtil(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;
    .locals 1
    .param p0, "appManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    invoke-direct {v0, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;-><init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;)V

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    .line 33
    :cond_0
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->sUpnpCommonApiUtil:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;

    return-object v0
.end method


# virtual methods
.method public acmsAppStateChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 174
    const-string v0, "Upnp/UpnpCommonApiUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter acmsAppStateChanged "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "Upnp/UpnpCommonApiUtil"

    const-string v1, "mCommonApiCallBack is not null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    sget-object v1, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;->UPDATE_CERTIFICATION_STATUS_CHANGE:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;->onEvent(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack$ResultCode;Ljava/lang/Object;)V

    .line 181
    :cond_0
    const-string v0, "Upnp/UpnpCommonApiUtil"

    const-string v1, "Exit acmsAppStateChanged "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public getApplicationCertificationInformation(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7
    .param p1, "entity"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 111
    const-string v3, "Upnp/UpnpCommonApiUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Enter getApplicationCertificationStatus for package"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-nez v3, :cond_0

    .line 115
    const-string v3, "Upnp/UpnpCommonApiUtil"

    .line 116
    const-string v4, "mTmsAppManager object is null..! So, returning default values for getApplicationCertificationInformation"

    .line 114
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "ENTITY"

    const-string v4, " "

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v3, "CERTIFIED"

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    const-string v3, "RESTRICTED"

    const-string v4, " "

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v3, "NONRESTRICTED"

    const-string v4, " "

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v3, "Upnp/UpnpCommonApiUtil"

    .line 124
    const-string v4, "mTmsAppManager object is null..! So, returned default values for getApplicationCertificationInformation "

    .line 122
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .end local v1    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 128
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v3, p2}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 129
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_1

    .line 130
    const-string v3, "Upnp/UpnpCommonApiUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Package does not exists :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_1
    iget-boolean v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-nez v3, :cond_2

    .line 134
    const-string v3, "Upnp/UpnpCommonApiUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Application is not certified : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 138
    .restart local v1    # "bundle":Landroid/os/Bundle;
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-nez v3, :cond_3

    .line 139
    const-string v3, "ENTITY"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v3, "CERTIFIED"

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 143
    :cond_3
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 153
    const-string v3, "ENTITY"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v3, "CERTIFIED"

    invoke-virtual {v1, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 143
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 144
    .local v2, "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    iget-object v4, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 145
    const-string v3, "ENTITY"

    iget-object v4, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v3, "CERTIFIED"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    const-string v3, "RESTRICTED"

    iget-object v4, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mRestricted:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v3, "NONRESTRICTED"

    iget-object v4, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mNonrestricted:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getApplicationCertificationStatus(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 46
    const-string v2, "Upnp/UpnpCommonApiUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Enter getApplicationCertificationStatus for package"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-nez v2, :cond_0

    .line 50
    const-string v2, "Upnp/UpnpCommonApiUtil"

    .line 51
    const-string v3, "mTmsAppManager object is null..! So, returning default values for ApplicationCertificationStatus"

    .line 49
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "HAS_VALID_CERTIFICATE"

    .line 53
    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v2, "ADVERTISED_AS_CERTIFIEDAPP"

    .line 56
    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 60
    const-string v2, "Upnp/UpnpCommonApiUtil"

    .line 61
    const-string v3, "mTmsAppManager object is null..! So, returned default values for ApplicationCertificationStatus "

    .line 59
    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .end local v1    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 65
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 66
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_1

    .line 67
    const-string v2, "Upnp/UpnpCommonApiUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Package does not exists :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v1, 0x0

    goto :goto_0

    .line 70
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 71
    .restart local v1    # "bundle":Landroid/os/Bundle;
    const-string v2, "HAS_VALID_CERTIFICATE"

    iget-boolean v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    const-string v2, "ADVERTISED_AS_CERTIFIEDAPP"

    iget-boolean v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    const-string v2, "Upnp/UpnpCommonApiUtil"

    const-string v3, "Exit getApplicationCertificationStatus"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getApplicationCertifyingEntities(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 79
    const-string v4, "Upnp/UpnpCommonApiUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Enter getApplicationCertificationStatus for package"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    if-nez v4, :cond_0

    .line 83
    const-string v3, "Upnp/UpnpCommonApiUtil"

    .line 84
    const-string v4, "mTmsAppManager object is null..! So, returning null values for getApplicationCertifyingEntities"

    .line 82
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v3, " "

    .line 107
    :goto_0
    return-object v3

    .line 88
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mTmsAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v4, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 89
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_1

    .line 90
    const-string v4, "Upnp/UpnpCommonApiUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Package does not exists :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-boolean v4, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->flagCertified:Z

    if-nez v4, :cond_2

    .line 94
    const-string v4, "Upnp/UpnpCommonApiUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Application is not certified : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_2
    const-string v2, ""

    .line 98
    .local v2, "entities":Ljava/lang/String;
    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    if-eqz v3, :cond_3

    .line 99
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 100
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_3

    .line 101
    iget-object v3, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    iget-object v3, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 106
    :cond_3
    const-string v3, "Upnp/UpnpCommonApiUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exit getApplicationCertificationStatus with entities "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 101
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 102
    .local v1, "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public registerCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 160
    const-string v0, "Upnp/UpnpCommonApiUtil"

    .line 161
    const-string v1, "UpnpCommonApiUtil. registerCommonApiCallBack() - Enter"

    .line 160
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 163
    return-void
.end method

.method public unRegisterCommonApiCallBack(Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;)V
    .locals 2
    .param p1, "callback"    # Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .prologue
    .line 167
    const-string v0, "Upnp/UpnpCommonApiUtil"

    .line 168
    const-string v1, "UpnpCommonApiUtil. UnregisterCommonApiCallBack() - Enter"

    .line 167
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpCommonApiUtil;->mCommonApiCallBack:Lcom/samsung/android/mirrorlink/commonapi/ICommonApiCallBack;

    .line 171
    return-void
.end method
