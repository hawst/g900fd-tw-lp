.class public Lcom/samsung/android/mirrorlink/commonapi/VncCore;
.super Ljava/lang/Object;
.source "VncCore.java"


# static fields
.field private static final BIT_FIVE_SET:I = 0x20

.field private static final BIT_FOUR_SET:I = 0x10

.field private static final BIT_ONE_SET:I = 0x2

.field private static final BIT_TEN_SET:I = 0x400

.field private static final BIT_THREE_SET:I = 0x8

.field private static final BIT_TWO_SET:I = 0x4

.field private static final BIT_ZERO_SET:I = 0x1

.field private static final CCC:Ljava/lang/String; = "CCC"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSVncCore"


# instance fields
.field private isDriveModeEnabled:Ljava/lang/Boolean;

.field private isMicOpen:Ljava/lang/Boolean;

.field private isNightModeEnabled:Ljava/lang/Boolean;

.field private mCallbackRegistedAppMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

.field private mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

.field private mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

.field private mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

.field private mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

.field private mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

.field private mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

.field private mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

.field private mFbBlockedAppId:I

.field private mFbBlockedReason:I

.field private mFbBlockedRect:Landroid/os/Bundle;

.field private mHandleBlockingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteDisplayConnection:I

.field private mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

.field private mSurface:Landroid/view/Surface;

.field private mblockedAppid:I


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "cmnApi"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isDriveModeEnabled:Ljava/lang/Boolean;

    .line 50
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isNightModeEnabled:Ljava/lang/Boolean;

    .line 52
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isMicOpen:Ljava/lang/Boolean;

    .line 97
    const-string v0, "TMSVncCore"

    const-string v1, "Enter Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 100
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 103
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mHandleBlockingMap:Ljava/util/Map;

    .line 107
    const-string v0, "TMSVncCore"

    const-string v1, "Exit Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method private fillClientPixelFormatFromString(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 210
    new-instance v0, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 211
    .local v0, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 212
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 213
    .local v1, "pixelFormat":Landroid/os/Bundle;
    const-string v2, "BITS_PER_PIXEL"

    .line 214
    const-string v3, "bitsperpixel"

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 213
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    const-string v2, "BLUE_MAX"

    .line 216
    const-string v3, "bluemax"

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 215
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 217
    const-string v2, "DEPTH"

    const-string v3, "depth"

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    const-string v2, "GREEN_MAX"

    .line 219
    const-string v3, "greenmax"

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 218
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 220
    const-string v2, "RED_MAX"

    .line 221
    const-string v3, "redmax"

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 220
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 222
    return-object v1
.end method

.method private fillDisplayConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    .locals 12
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 226
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;-><init>()V

    .line 228
    .local v0, "d":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    new-instance v4, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v4}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 229
    .local v4, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v4, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 233
    const-string v9, "horiRes"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 234
    .local v3, "horiRes":I
    const-string v9, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : horiRes"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 234
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setHoriRes(I)V

    .line 238
    const-string v9, "vertRes"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 239
    .local v7, "vertRes":I
    const-string v9, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : vertRes"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 239
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-virtual {v0, v7}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setVertRes(I)V

    .line 243
    const-string v9, "width"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 244
    .local v8, "width":I
    const-string v9, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : width"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0, v8}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setWidth(I)V

    .line 247
    const-string v9, "height"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 248
    .local v2, "height":I
    const-string v9, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : height"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual {v0, v2}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setHeight(I)V

    .line 251
    const-string v9, "distance"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 252
    .local v1, "distance":I
    const-string v9, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : distance"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 252
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setDistance(I)V

    .line 256
    const-string v9, "sHoriRes"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 257
    .local v5, "sHorires":I
    const-string v9, "TMSVncCore"

    .line 258
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : server Horizontal resolution"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 258
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 257
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setsHoriRes(I)V

    .line 262
    const-string v9, "sVertRes"

    invoke-virtual {v4, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 263
    .local v6, "sVertres":I
    const-string v9, "TMSVncCore"

    .line 264
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "fillClientDisplayConfigFromString : server vertical resolution"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 264
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 263
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v0, v6}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setsVertRes(I)V

    .line 268
    return-object v0
.end method

.method private fillEventConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/EventConfig;
    .locals 18
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 559
    new-instance v2, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;-><init>()V

    .line 560
    .local v2, "d":Lcom/samsung/android/mirrorlink/commonapi/EventConfig;
    new-instance v10, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v10}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 561
    .local v10, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 563
    const-string v15, "kblangcode"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 564
    .local v7, "keyboardLanguage":Ljava/lang/String;
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromString : keyboardLanguage "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 565
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 564
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    invoke-virtual {v2, v7}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setKeyboardLanguage(Ljava/lang/String;)V

    .line 568
    const-string v15, "kbcountrycode"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 569
    .local v6, "keyboardCountry":Ljava/lang/String;
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromString : keyboardCountry"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 570
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 569
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    invoke-virtual {v2, v6}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setKeyboardCountry(Ljava/lang/String;)V

    .line 574
    const-string v15, "uilangcode"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 575
    .local v14, "uiLanguage":Ljava/lang/String;
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromString : uiLanguage"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 576
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 575
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    invoke-virtual {v2, v14}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setUILanguage(Ljava/lang/String;)V

    .line 579
    const-string v15, "uicountrycode"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 580
    .local v13, "uiCountry":Ljava/lang/String;
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromString : uiCountry"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 581
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 580
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    invoke-virtual {v2, v13}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setUICountry(Ljava/lang/String;)V

    .line 584
    const-string v15, "knobsupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 585
    .local v8, "knobSupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : KnobSupport"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 586
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 585
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    invoke-virtual {v2, v8}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setKnobSupport(I)V

    .line 589
    const-string v15, "devicekeysupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 590
    .local v3, "deviceKeySupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : DeviceKeySupport"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 591
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 590
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-virtual {v2, v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setDeviceKeySupport(I)V

    .line 594
    const-string v15, "multimediakeysupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 595
    .local v9, "mmKeySupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : MultimediaKeySupport"

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 596
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 595
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    invoke-virtual {v2, v9}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setMmKeySupport(I)V

    .line 599
    const-string v15, "functionKeySupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 600
    .local v4, "funcKeySupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : FunctionKeySupport "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 601
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 600
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setFuncKeySupport(I)V

    .line 604
    const-string v15, "itukeysupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 605
    .local v1, "ITUKeySupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : ITUKeySupport "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 606
    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 605
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    if-lez v1, :cond_0

    const/4 v5, 0x1

    .line 608
    .local v5, "ituKeySupport":Z
    :goto_0
    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setItuKeySupport(Z)V

    .line 610
    const-string v15, "toucheventsupport"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 611
    .local v12, "touchSupport":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : touchSupport "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 612
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 611
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-virtual {v2, v12}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setTouchSupport(I)V

    .line 615
    const-string v15, "pressuremask"

    invoke-virtual {v10, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 616
    .local v11, "pressureMask":I
    const-string v15, "TMSVncCore"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "fillEventConfigFromInt : pressureMask "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 617
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 616
    invoke-static/range {v15 .. v16}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    invoke-virtual {v2, v11}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->setPressureMask(I)V

    .line 620
    return-object v2

    .line 607
    .end local v5    # "ituKeySupport":Z
    .end local v11    # "pressureMask":I
    .end local v12    # "touchSupport":I
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private fillEventMappingFromString(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 699
    const-string v7, "TMSVncCore"

    const-string v8, "fillEventMappingFromString () : Enter"

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 708
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 709
    const-string v7, "TMSVncCore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Event String ="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const-string v7, ";"

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 713
    .local v1, "events":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, v1

    if-lt v2, v7, :cond_1

    .line 738
    .end local v1    # "events":[Ljava/lang/String;
    .end local v2    # "i":I
    :cond_0
    const-string v7, "TMSVncCore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Final list size ="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    return-object v3

    .line 717
    .restart local v1    # "events":[Ljava/lang/String;
    .restart local v2    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 719
    .local v0, "b":Landroid/os/Bundle;
    aget-object v7, v1, v2

    const-string v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 721
    .local v5, "remote":[Ljava/lang/String;
    const-string v7, "TMSVncCore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "remote event  "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, v5, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  local  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 722
    aget-object v9, v5, v11

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 721
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    aget-object v7, v5, v11

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 726
    .local v4, "localValue":I
    aget-object v7, v5, v10

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 728
    .local v6, "remoteValue":I
    const-string v7, "TMSVncCore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "After Conversion local event"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "remote"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 729
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 728
    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const-string v7, "LOCAL_EVENT"

    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 732
    const-string v7, "REMOTE_EVENT"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 734
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 713
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method private fillFbBlockInfoFromString(Ljava/lang/String;)V
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1125
    new-instance v2, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 1126
    .local v2, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 1128
    const-string v5, "top"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1129
    .local v4, "top":I
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : top"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    const-string v5, "bottom"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1132
    .local v0, "bottom":I
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : bottom"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    const-string v5, "left"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1135
    .local v1, "left":I
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : left"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    const-string v5, "right"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1138
    .local v3, "right":I
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : right"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    const-string v5, "reason"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    .line 1141
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : FbBlockedReason"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1142
    iget v7, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1141
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1144
    const-string v5, "appId"

    invoke-virtual {v2, v5}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    .line 1145
    const-string v5, "TMSVncCore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fillFbBlockInfoFromString : FbBlockedAppId"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146
    iget v7, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1145
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    .line 1149
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    const-string v6, "X"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1150
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    const-string v6, "WIDTH"

    sub-int v7, v3, v1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1151
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    const-string v6, "Y"

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1152
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    const-string v6, "HEIGHT"

    sub-int v7, v4, v0

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1154
    return-void
.end method

.method private fillMLVersion(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 1185
    new-instance v3, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v3}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 1186
    .local v3, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v3, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 1188
    const-string v4, "majorversion"

    invoke-virtual {v3, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1189
    .local v1, "majorVersion":I
    const-string v4, "TMSVncCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fillMLVersion : majorversion"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    const-string v4, "minorversion"

    invoke-virtual {v3, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1192
    .local v2, "minorVersion":I
    const-string v4, "TMSVncCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fillMLVersion : minorversion"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1195
    .local v0, "mMLVersion":Landroid/os/Bundle;
    const-string v4, "TMSVncCore"

    const-string v5, "inseting Minor and Major Values into a bundle"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    const-string v4, "VERSION_MAJOR"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1198
    const-string v4, "VERSION_MINOR"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1200
    const-string v4, "TMSVncCore"

    .line 1201
    const-string v5, "Bundle Created and returning to cbMLVersion() method"

    .line 1200
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    return-object v0
.end method

.method private fillServerScalingConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    .locals 7
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 329
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;-><init>()V

    .line 331
    .local v0, "d":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    new-instance v2, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 332
    .local v2, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 336
    const-string v4, "horizontal"

    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 337
    .local v1, "horiRes":I
    const-string v4, "TMSVncCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fillServerScalingConfigFromString : horiRes"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 338
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 337
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setHoriRes(I)V

    .line 341
    const-string v4, "vertical"

    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 342
    .local v3, "vertRes":I
    const-string v4, "TMSVncCore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fillServerScalingConfigFromString : vertRes"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 343
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 342
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->setVertRes(I)V

    .line 346
    return-object v0
.end method

.method private fillVKInfoFromString(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 13
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 780
    new-instance v0, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 781
    .local v0, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 783
    const-string v10, "isVkbAvail"

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 784
    .local v1, "vkAvail":I
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    .line 785
    .local v2, "vkAvailability":Z
    :goto_0
    const-string v10, "TMSVncCore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fillVKInfoFromString : vkAvailability"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    const-string v10, "isVkbTocuh"

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 788
    .local v8, "vkTouch":I
    if-eqz v8, :cond_1

    const/4 v9, 0x1

    .line 789
    .local v9, "vkTouchSupport":Z
    :goto_1
    const-string v10, "TMSVncCore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fillVKInfoFromString : vkTouchSupport"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    const-string v10, "isVkbKnob"

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 792
    .local v6, "vkKnob":I
    if-eqz v6, :cond_2

    const/4 v7, 0x1

    .line 793
    .local v7, "vkKnobSupport":Z
    :goto_2
    const-string v10, "TMSVncCore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fillVKInfoFromString : vkKnobSupport"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    const-string v10, "isVkbDrive"

    invoke-virtual {v0, v10}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 796
    .local v3, "vkDrive":I
    if-eqz v3, :cond_3

    const/4 v4, 0x1

    .line 797
    .local v4, "vkDriveMode":Z
    :goto_3
    const-string v10, "TMSVncCore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fillVKInfoFromString : vkDriveMode"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 800
    .local v5, "vkInfo":Landroid/os/Bundle;
    const-string v10, "AVAILABLE"

    invoke-virtual {v5, v10, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 801
    const-string v10, "TOUCH_SUPPORT"

    invoke-virtual {v5, v10, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 802
    const-string v10, "KNOB_SUPPORT"

    invoke-virtual {v5, v10, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 803
    const-string v10, "DRIVE_MODE"

    invoke-virtual {v5, v10, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 804
    return-object v5

    .line 784
    .end local v2    # "vkAvailability":Z
    .end local v3    # "vkDrive":I
    .end local v4    # "vkDriveMode":Z
    .end local v5    # "vkInfo":Landroid/os/Bundle;
    .end local v6    # "vkKnob":I
    .end local v7    # "vkKnobSupport":Z
    .end local v8    # "vkTouch":I
    .end local v9    # "vkTouchSupport":Z
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 788
    .restart local v2    # "vkAvailability":Z
    .restart local v8    # "vkTouch":I
    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 792
    .restart local v6    # "vkKnob":I
    .restart local v9    # "vkTouchSupport":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 796
    .restart local v3    # "vkDrive":I
    .restart local v7    # "vkKnobSupport":Z
    :cond_3
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 743
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method private isDisplayConfigChanged(Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;)Z
    .locals 3
    .param p1, "displayConfig"    # Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .prologue
    const/4 v0, 0x1

    .line 166
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    if-nez v1, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v0

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getDistance()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getDistance()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 171
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 173
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHoriRes()I

    move-result v1

    .line 174
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getHoriRes()I

    move-result v2

    .line 173
    if-ne v1, v2, :cond_0

    .line 176
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getVertRes()I

    move-result v1

    .line 177
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getVertRes()I

    move-result v2

    .line 176
    if-ne v1, v2, :cond_0

    .line 179
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 181
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsHoriRes()I

    move-result v1

    .line 182
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsHoriRes()I

    move-result v2

    .line 181
    if-ne v1, v2, :cond_0

    .line 184
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsVertRes()I

    move-result v1

    .line 185
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsVertRes()I

    move-result v2

    .line 184
    if-ne v1, v2, :cond_0

    .line 187
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppHoriRes()I

    move-result v1

    .line 188
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppHoriRes()I

    move-result v2

    .line 187
    if-ne v1, v2, :cond_0

    .line 190
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppPixels()I

    move-result v1

    .line 191
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppPixels()I

    move-result v2

    .line 190
    if-ne v1, v2, :cond_0

    .line 193
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppVertRes()I

    move-result v1

    .line 194
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getAppVertRes()I

    move-result v2

    .line 193
    if-ne v1, v2, :cond_0

    .line 196
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadCols()I

    move-result v1

    .line 197
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadCols()I

    move-result v2

    .line 196
    if-ne v1, v2, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadRows()I

    move-result v1

    .line 200
    invoke-virtual {p1}, Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;->getsPadRows()I

    move-result v2

    .line 199
    if-ne v1, v2, :cond_0

    .line 206
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private isRemoteDisplayConnectionChanged(I)Z
    .locals 3
    .param p1, "config"    # I

    .prologue
    const/4 v0, 0x1

    .line 363
    const-string v1, "TMSVncCore"

    const-string v2, "isRemoteDisplayConnectionChanged() -- Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 365
    const-string v1, "TMSVncCore"

    .line 366
    const-string v2, "isRemoteDisplayConnectionChanged().mRemoteDisplayConnection value is -1"

    .line 365
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :goto_0
    return v0

    .line 368
    :cond_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    if-eq v1, p1, :cond_1

    .line 370
    const-string v1, "TMSVncCore"

    .line 371
    const-string v2, "isRemoteDisplayConnectionChanged() : mRemoteDisplayConnection value != received callback value"

    .line 369
    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 375
    :cond_1
    const-string v0, "TMSVncCore"

    .line 376
    const-string v1, "isRemoteDisplayConnectionChanged() : Returning False"

    .line 374
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cbMLVersion(Ljava/lang/String;)Z
    .locals 4
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 1159
    const-string v2, "TMSVncCore"

    const-string v3, "cbMLVersion() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const/4 v1, 0x0

    .line 1162
    .local v1, "ret":Z
    if-nez p1, :cond_0

    .line 1163
    const-string v2, "TMSVncCore"

    const-string v3, "cbMLVersion() String is null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    :goto_0
    return v1

    .line 1168
    :cond_0
    const-string v2, "TMSVncCore"

    const-string v3, "Calling fillMLVersion () to create a bundle"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillMLVersion(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1170
    .local v0, "mlVersion":Landroid/os/Bundle;
    const-string v2, "TMSVncCore"

    const-string v3, "Bundle Created and received from fillMLVersion() method"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    const-string v2, "VERSION_MAJOR"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 1173
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->getMajorVersionValue()I

    move-result v3

    .line 1172
    if-ne v2, v3, :cond_1

    .line 1174
    const-string v2, "VERSION_MINOR"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 1175
    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->getMinorVersionValue()I

    move-result v3

    .line 1174
    if-eq v2, v3, :cond_2

    .line 1176
    :cond_1
    const-string v2, "TMSVncCore"

    const-string v3, "Now getting ML version Call Back from VNC "

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;->notifyDeviceInfoChanged(Landroid/os/Bundle;)V

    .line 1180
    :cond_2
    const-string v2, "TMSVncCore"

    const-string v3, "cbMLVersion() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public clearPackageFromMap(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 652
    const-string v0, "TMSVncCore"

    const-string v1, "vncCore().clearPackageFromMap() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    if-nez p1, :cond_0

    .line 655
    const-string v0, "TMSVncCore"

    .line 656
    const-string v1, "VncCore().clearPackageFromMap() : Died 3rd party Application\'s Package Name is Not present in MAP"

    .line 654
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    :goto_0
    return-void

    .line 662
    :cond_0
    const-string v0, "TMSVncCore"

    .line 663
    const-string v1, "VncCore().clearPackageFromMap() : Died 3rd party Application\'s Package Name is present in MAP. Hence, Removing from the MAP"

    .line 661
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    const-string v0, "TMSVncCore"

    const-string v1, "VncCore().clearPackageFromMap() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getClientPixelFormat()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 280
    const-string v1, "TMSVncCore"

    const-string v2, "getClientPixelFormat() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getClientPixelFormat()Ljava/lang/String;

    move-result-object v0

    .line 282
    .local v0, "format":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 283
    const-string v1, "TMSVncCore"

    const-string v2, "getClientPixelFormat() return Null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const/4 v1, 0x0

    .line 286
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillClientPixelFormatFromString(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method public getDisplayConfig()Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    .locals 3

    .prologue
    .line 136
    const-string v1, "TMSVncCore"

    const-string v2, "getClientDisplayConfig() "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getClientDisplayInfo()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "d":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 140
    const-string v1, "TMSVncCore"

    const-string v2, "getClientDisplayConfig() return Null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v1, 0x0

    .line 146
    :goto_0
    return-object v1

    .line 144
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillDisplayConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 146
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    goto :goto_0
.end method

.method public getDisplaySurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mSurface:Landroid/view/Surface;

    return-object v0
.end method

.method public getEventConfig()Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 421
    const-string v3, "TMSVncCore"

    const-string v4, "getEventConfig() Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 424
    .local v0, "b":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getEventConfig()Ljava/lang/String;

    move-result-object v1

    .line 425
    .local v1, "d":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 426
    const-string v3, "TMSVncCore"

    const-string v4, "getEventConfig() return Null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 473
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    return-object v0

    .line 430
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_0
    const-string v3, "TMSVncCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "String Got in eventconfig is : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillEventConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    .line 433
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    if-eqz v3, :cond_1

    .line 434
    const-string v2, "KEYBOARD_LANGUAGE"

    .line 435
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKeyboardLanguage()Ljava/lang/String;

    move-result-object v3

    .line 434
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v2, "KEYBOARD_COUNTRY"

    .line 437
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKeyboardCountry()Ljava/lang/String;

    move-result-object v3

    .line 436
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v2, "UI_LANGUAGE"

    .line 439
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getUILanguage()Ljava/lang/String;

    move-result-object v3

    .line 438
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v2, "UI_COUNTRY"

    .line 441
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getUICountry()Ljava/lang/String;

    move-result-object v3

    .line 440
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const-string v2, "KNOB_KEY_SUPPORT"

    .line 443
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKnobSupport()I

    move-result v3

    .line 442
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 444
    const-string v2, "DEVICE_KEY_SUPPORT"

    .line 445
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getDeviceKeySupport()I

    move-result v3

    .line 444
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 446
    const-string v2, "FUNC_KEY_SUPPORT"

    .line 447
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getFuncKeySupport()I

    move-result v3

    .line 446
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 448
    const-string v2, "ITU_KEY_SUPPORT"

    .line 449
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getItuKeySupport()Z

    move-result v3

    .line 448
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 450
    const-string v2, "DEVICE_KEY_SUPPORT"

    .line 451
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getDeviceKeySupport()I

    move-result v3

    .line 450
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 452
    const-string v2, "MULTIMEDIA_KEY_SUPPORT"

    .line 453
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getMmKeySupport()I

    move-result v3

    .line 452
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 454
    const-string v2, "TOUCH_SUPPORT"

    .line 455
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getTouchSupport()I

    move-result v3

    .line 454
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    const-string v2, "PRESSURE_MASK"

    .line 457
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getPressureMask()I

    move-result v3

    .line 456
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 472
    :goto_1
    const-string v2, "TMSVncCore"

    const-string v3, "getEventConfig() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 459
    :cond_1
    const-string v3, "KEYBOARD_LANGUAGE"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v3, "KEYBOARD_COUNTRY"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v3, "UI_LANGUAGE"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v3, "UI_COUNTRY"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v2, "KNOB_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 464
    const-string v2, "DEVICE_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 465
    const-string v2, "FUNC_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 466
    const-string v2, "ITU_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 467
    const-string v2, "DEVICE_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 468
    const-string v2, "MULTIMEDIA_KEY_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 469
    const-string v2, "TOUCH_SUPPORT"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 470
    const-string v2, "PRESSURE_MASK"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public getEventMapping()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    const-string v2, "TMSVncCore"

    const-string v3, "getEventMapping() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getEventMapping()Ljava/lang/String;

    move-result-object v0

    .line 627
    .local v0, "d":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 628
    const-string v2, "TMSVncCore"

    const-string v3, "getEventMapping() return Null"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const/4 v1, 0x0

    .line 636
    :goto_0
    return-object v1

    .line 633
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillEventMappingFromString(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 635
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v2, "TMSVncCore"

    const-string v3, "getEventMapping() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getKeyEventListSupport()Z
    .locals 5

    .prologue
    .line 818
    const-string v2, "TMSVncCore"

    const-string v3, "getKeyEventListSupport() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    const/4 v0, 0x0

    .line 820
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getKeyEventListSupport()I

    move-result v1

    .line 821
    .local v1, "val":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 822
    const/4 v0, 0x1

    .line 824
    :cond_0
    const-string v2, "TMSVncCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getKeyEventListSupport() Exit "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    return v0
.end method

.method public getMicStatus()Z
    .locals 3

    .prologue
    .line 690
    const/4 v0, 0x0

    .line 691
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getMicStatus()I

    move-result v1

    .line 692
    .local v1, "val":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 693
    const/4 v0, 0x1

    .line 695
    :cond_0
    return v0
.end method

.method public getOrientationSwitchSupport()Z
    .locals 5

    .prologue
    .line 398
    const-string v2, "TMSVncCore"

    const-string v3, "getOrientationSwitchSupport() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const/4 v0, 0x0

    .line 400
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getOrientationSwitchSupport()I

    move-result v1

    .line 401
    .local v1, "supp":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 402
    const/4 v0, 0x1

    .line 404
    :cond_0
    const-string v2, "TMSVncCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getOrientationSwitchSupport() Exit "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    return v0
.end method

.method public getRemoteDisplayConnection()I
    .locals 3

    .prologue
    .line 381
    const-string v0, "TMSVncCore"

    const-string v1, "getRemoteDisplayConnection() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v0

    .line 383
    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getRemoteDisplayConnection()I

    move-result v0

    .line 382
    iput v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    .line 384
    const-string v0, "TMSVncCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getRemoteDisplayConnection() Exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 385
    iget v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 384
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    return v0
.end method

.method public getServerScalingConfig()Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    .locals 3

    .prologue
    .line 314
    const-string v1, "TMSVncCore"

    const-string v2, "getServerScalingConfig() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getServerScalingConfig()Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "d":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 318
    const-string v1, "TMSVncCore"

    const-string v2, "getClientDisplayConfig() return Null"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v1, 0x0

    .line 325
    :goto_0
    return-object v1

    .line 322
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillServerScalingConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 324
    const-string v1, "TMSVncCore"

    const-string v2, "getServerScalingConfig() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    goto :goto_0
.end method

.method public getVirtualKeyboardSupport()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 757
    const-string v3, "TMSVncCore"

    const-string v4, "VncCore().getVirtualKeyboardSupport() Enter"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getServerVKBSupport()Ljava/lang/String;

    move-result-object v1

    .line 761
    .local v1, "d":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 763
    const-string v3, "TMSVncCore"

    const-string v4, "VncCore().getVirtualKeyboardSupport(): Received string from native is null"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 765
    .local v0, "b":Landroid/os/Bundle;
    const-string v3, "AVAILABLE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 766
    const-string v3, "TOUCH_SUPPORT"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 767
    const-string v3, "KNOB_SUPPORT"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 768
    const-string v3, "DRIVE_MODE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 775
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    return-object v0

    .line 771
    :cond_0
    const-string v3, "TMSVncCore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "VncCore().getVirtualKeyboardSupport() Received string from native is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    invoke-direct {p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillVKInfoFromString(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 774
    .local v2, "info":Landroid/os/Bundle;
    const-string v3, "TMSVncCore"

    const-string v4, "VncCore().getVirtualKeyboardSupport() Exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 775
    goto :goto_0
.end method

.method public getdriveMode()Z
    .locals 3

    .prologue
    .line 672
    const/4 v0, 0x0

    .line 673
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getDriveMode()I

    move-result v1

    .line 674
    .local v1, "val":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 675
    const/4 v0, 0x1

    .line 677
    :cond_0
    return v0
.end method

.method public getnightMode()Z
    .locals 3

    .prologue
    .line 681
    const/4 v0, 0x0

    .line 682
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_getNightMode()I

    move-result v1

    .line 683
    .local v1, "val":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 684
    const/4 v0, 0x1

    .line 686
    :cond_0
    return v0
.end method

.method public resetCtxtInfo(ZZLjava/lang/String;)Z
    .locals 11
    .param p1, "fbCtxt"    # Z
    .param p2, "audioCtxt"    # Z
    .param p3, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 928
    const-string v9, "TMSVncCore"

    const-string v10, "resetCtxtInfo() Enter"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    const/4 v5, 0x0

    .line 930
    .local v5, "ret":Z
    if-eqz p1, :cond_0

    move v3, v7

    .line 931
    .local v3, "ifbCtxt":I
    :goto_0
    if-eqz p2, :cond_1

    move v2, v7

    .line 933
    .local v2, "iaudioCtxt":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v4

    .line 935
    .local v4, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-nez v4, :cond_2

    .line 936
    const-string v7, "TMSVncCore"

    const-string v9, "resetCtxtInfo : AppManager is null"

    invoke-static {v7, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    :goto_2
    return v8

    .end local v2    # "iaudioCtxt":I
    .end local v3    # "ifbCtxt":I
    .end local v4    # "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    :cond_0
    move v3, v8

    .line 930
    goto :goto_0

    .restart local v3    # "ifbCtxt":I
    :cond_1
    move v2, v8

    .line 931
    goto :goto_1

    .line 941
    .restart local v2    # "iaudioCtxt":I
    .restart local v4    # "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    :cond_2
    invoke-virtual {v4, p3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 942
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v1, :cond_3

    .line 943
    const-string v7, "TMSVncCore"

    const-string v9, "resetCtxtInfo : appInfo is null"

    invoke-static {v7, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 947
    :cond_3
    iget v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 949
    .local v0, "appId":I
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v8

    invoke-virtual {v8, v3, v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_resetCtxtInfo(III)I

    move-result v6

    .line 951
    .local v6, "val":I
    if-ne v6, v7, :cond_4

    .line 952
    const/4 v5, 0x1

    .line 954
    :cond_4
    const-string v7, "TMSVncCore"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "resetCtxtInfo() Exit "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v5

    .line 955
    goto :goto_2
.end method

.method public setConnectionManager(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 124
    return-void
.end method

.method public setContextManager(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 116
    return-void
.end method

.method public setDeviceManager(Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceInfoManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceInfoManager;

    .line 112
    return-void
.end method

.method public setDeviceStatusManager(Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 128
    return-void
.end method

.method public setDisplayManager(Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .line 120
    return-void
.end method

.method public setDisplaySurface(Landroid/view/Surface;)V
    .locals 0
    .param p1, "surface"    # Landroid/view/Surface;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mSurface:Landroid/view/Surface;

    .line 277
    return-void
.end method

.method public setEventMappingManager(Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;)V
    .locals 0
    .param p1, "mngr"    # Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    .line 132
    return-void
.end method

.method public setFbCtxtInfo(Ljava/util/List;Ljava/lang/String;Z)Z
    .locals 23
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "handleBlocking"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 837
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const-string v19, "TMSVncCore"

    const-string v20, "VncCore.setFbCtxtInfo() : Enter"

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    const/4 v14, 0x0

    .line 839
    .local v14, "ret":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v12

    .line 840
    .local v12, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-nez v12, :cond_0

    .line 841
    const-string v19, "TMSVncCore"

    const-string v20, "VncCore.setFbCtxtInfo : AppManager is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    const/4 v14, 0x0

    .line 923
    .end local v14    # "ret":Z
    :goto_0
    return v14

    .line 846
    .restart local v14    # "ret":Z
    :cond_0
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v4

    .line 847
    .local v4, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v4, :cond_1

    .line 848
    const-string v19, "TMSVncCore"

    const-string v20, "VncCore.setFbCtxtInfo : appInfo is null"

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const/4 v14, 0x0

    goto :goto_0

    .line 851
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 852
    const-string v19, "TMSVncCore"

    .line 853
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "VncCore.setFBCtxtInfo : Inserting a new Package Info to the MAP"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 854
    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 853
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 852
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v10

    .line 869
    .local v10, "listSize":I
    const-string v19, "TMSVncCore"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "VncCore.setFbCtxtInfo : getting list size: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 870
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 869
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mHandleBlockingMap:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 873
    const-string v19, "TMSVncCore"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "VncCore().seFBCtxtInfo().Adding HBlocking to Map :"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 874
    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 873
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    new-instance v11, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v11}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 877
    .local v11, "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    const-string v19, "TMSVncCore"

    .line 878
    const-string v20, "VncCore.setFbCtxtInfo : passing bundle by bundle to Native using loop"

    .line 877
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_4

    .line 922
    const-string v19, "TMSVncCore"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "VncCore.setFbCtxtInfo() : Exit "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 858
    .end local v10    # "listSize":I
    .end local v11    # "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    :cond_3
    const-string v19, "TMSVncCore"

    .line 859
    const-string v20, "VncCore.setFBCtxtInfo : Incrementing the Value for the Package Received Blocking"

    .line 857
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 862
    .local v18, "value":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v20, v0

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-interface/range {v19 .. v21}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    const-string v19, "TMSVncCore"

    .line 864
    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "VncCore.setFBCtxtInfo : Values of the existing package in the MAP"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 865
    iget-object v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Values :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 864
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 863
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 879
    .end local v18    # "value":I
    .restart local v10    # "listSize":I
    .restart local v11    # "params":Lcom/samsung/android/mirrorlink/util/TmParams;
    :cond_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 880
    .local v5, "b":Landroid/os/Bundle;
    const-string v19, "RECT"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v13

    .line 881
    .local v13, "rect":Landroid/os/Bundle;
    if-nez v13, :cond_5

    .line 882
    const-string v19, "TMSVncCore"

    .line 883
    const-string v20, "VncCore.setFbCtxtInfo : Recieved rect bundle as null"

    .line 882
    invoke-static/range {v19 .. v20}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 886
    :cond_5
    const-string v19, "X"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 887
    .local v9, "left":I
    const-string v19, "Y"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 888
    .local v16, "top":I
    const-string v19, "WIDTH"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 889
    .local v15, "right":I
    const-string v19, "HEIGHT"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 891
    .local v6, "bottom":I
    const-string v19, "APPLICATION_CATEGORY"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    int-to-long v2, v0

    .line 893
    .local v2, "appCategory":J
    const-string v19, "CONTENT_CATEGORY"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    int-to-long v7, v0

    .line 895
    .local v7, "contentCategory":J
    const-string v19, "TMSVncCore"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "setFbCtxtInfo() appCategory "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 896
    const-string v22, " contentCategory "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 895
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const-string v19, "TMSVncCore"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "setFbCtxtInfo() bottom "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " left "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 898
    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " right "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " top "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 897
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const-string v19, "appCategory"

    long-to-int v0, v2

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 901
    const-string v19, "contentCategory"

    long-to-int v0, v7

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 902
    const-string v19, "appId"

    iget v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 903
    const-string v19, "appTrustLvl"

    iget v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mTrustLevel:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 904
    const-string v19, "cntntTrustLvl"

    iget v0, v4, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mDisplayInfoTustLevel:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 905
    const-string v19, "rulesInfo"

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 906
    const-string v19, "bottom"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v6}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 907
    const-string v19, "left"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v9}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 908
    const-string v19, "right"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v15}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 909
    const-string v19, "top"

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 910
    const-string v21, "handleBlocking"

    if-eqz p3, :cond_6

    const/16 v19, 0x1

    :goto_2
    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 911
    const-string v19, "items_remaining"

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v10}, Lcom/samsung/android/mirrorlink/util/TmParams;->set(Ljava/lang/String;I)V

    .line 912
    const-string v19, "TMSVncCore"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "VncCore.setFbCtxtInfo : getting list size: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 913
    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 912
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v19

    .line 916
    invoke-virtual {v11}, Lcom/samsung/android/mirrorlink/util/TmParams;->flatten()Ljava/lang/String;

    move-result-object v21

    .line 915
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_setFbCtxtInfo(Ljava/lang/String;)I

    move-result v17

    .line 917
    .local v17, "val":I
    if-nez v17, :cond_2

    .line 918
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 910
    .end local v17    # "val":I
    :cond_6
    const/16 v19, 0x0

    goto :goto_2
.end method

.method public setFbOrientationSupported(I)Z
    .locals 3
    .param p1, "fbOrientation"    # I

    .prologue
    .line 390
    const-string v1, "TMSVncCore"

    const-string v2, "setFbOrientationSupported() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/4 v0, 0x1

    .line 392
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_setFbOrientationSupported(I)I

    .line 393
    const-string v1, "TMSVncCore"

    const-string v2, "setFbOrientationSupported() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    return v0
.end method

.method public setFbStatus(Z)Z
    .locals 3
    .param p1, "fbStatus"    # Z

    .prologue
    .line 829
    const-string v1, "TMSVncCore"

    const-string v2, "setFbStatus() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const/4 v0, 0x0

    .line 831
    .local v0, "ret":Z
    const-string v1, "TMSVncCore"

    const-string v2, "setFbStatus() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    return v0
.end method

.method public setKeyEventList([J)Z
    .locals 3
    .param p1, "keyEventList"    # [J

    .prologue
    .line 809
    const-string v1, "TMSVncCore"

    const-string v2, "setKeyEventList() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    const/4 v0, 0x0

    .line 812
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_setKeyEventList([J)I

    .line 813
    const-string v1, "TMSVncCore"

    const-string v2, "setKeyEventList() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    return v0
.end method

.method public setMicOpen(ZZ)I
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "voiceEnabled"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 647
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v3

    if-eqz p1, :cond_0

    move v2, v0

    .line 648
    :goto_0
    if-eqz p2, :cond_1

    .line 647
    :goto_1
    invoke-virtual {v3, v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_enableMicOnClient(II)I

    move-result v0

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 648
    goto :goto_1
.end method

.method public setNativeClientDisplayConfig(Ljava/lang/String;)V
    .locals 3
    .param p1, "config"    # Ljava/lang/String;

    .prologue
    .line 150
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeClientDisplayConfig() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "displayConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    if-eqz p1, :cond_0

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillDisplayConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v0

    .line 155
    :cond_0
    if-eqz v0, :cond_1

    .line 156
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isDisplayConfigChanged(Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    .line 158
    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->notifyDisplayConfigurationChanged(Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;)V

    .line 159
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 162
    :cond_1
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeClientDisplayConfig() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public setNativeClientPixelFormat(Ljava/lang/String;)V
    .locals 3
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 290
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeClientPixelFormat() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/4 v0, 0x0

    .line 292
    .local v0, "b":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 293
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillClientPixelFormatFromString(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 295
    :cond_0
    if-eqz v0, :cond_1

    .line 296
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDisplayManager:Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/mirrorlink/commonapi/DisplayManager;->notifyPixelFormatChanged(Landroid/os/Bundle;)V

    .line 298
    :cond_1
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeClientPixelFormat() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    return-void
.end method

.method public setNativeDriveMode(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1221
    const-string v0, "TMSVncCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setNativeDriveMode() Enter"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isDriveModeEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1224
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->notifyDriveModeChange(Z)V

    .line 1230
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isDriveModeEnabled:Ljava/lang/Boolean;

    .line 1233
    :cond_0
    const-string v0, "TMSVncCore"

    const-string v1, "setNativeDriveMode() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    return-void
.end method

.method public setNativeEventMapping(Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 478
    const-string v4, "TMSVncCore"

    const-string v5, "setNativetEventConfig() Enter"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    new-instance v2, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v2}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 481
    .local v2, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 483
    const-string v4, "remoteevent"

    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 486
    .local v3, "remoteEvent":I
    const-string v4, "localevent"

    invoke-virtual {v2, v4}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 489
    .local v1, "localEvent":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 491
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "REMOTE_EVENT"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 492
    const-string v4, "LOCAL_EVENT"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 493
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    invoke-virtual {v4, v0}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->notifyEventMappingChanged(Landroid/os/Bundle;)V

    .line 495
    const-string v4, "TMSVncCore"

    const-string v5, "setNativetEventConfig() Exit"

    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method public setNativeFbBlockInfo(Ljava/lang/String;)Z
    .locals 13
    .param p1, "config"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 959
    const-string v10, "TMSVncCore"

    const-string v11, "setNativeFbBlockInfo() Enter"

    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const/4 v7, 0x0

    .line 962
    .local v7, "ret":Z
    if-nez p1, :cond_0

    .line 963
    const-string v8, "TMSVncCore"

    const-string v9, "setNativeFbBlockInfo() config is null"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    .end local v7    # "ret":Z
    :goto_0
    return v7

    .line 966
    .restart local v7    # "ret":Z
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v10}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    .line 968
    .local v6, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-nez v6, :cond_1

    .line 969
    const-string v9, "TMSVncCore"

    const-string v10, "setNativeFbBlockInfo : AppManager is null"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 970
    goto :goto_0

    .line 973
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillFbBlockInfoFromString(Ljava/lang/String;)V

    .line 975
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_4

    .line 976
    const-string v10, "TMSVncCore"

    .line 977
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Calling ContextManager().notifyOnFramebufferUnBlocked() App with App id as :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 978
    iget v12, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mblockedAppid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 977
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 976
    invoke-static {v10, v11}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mblockedAppid:I

    invoke-virtual {v6, v10}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 982
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v0, :cond_2

    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    if-nez v10, :cond_3

    .line 983
    :cond_2
    const-string v9, "TMSVncCore"

    .line 984
    const-string v10, "setNativeFbUnBlockInfo() App : Received Appinfo is null"

    .line 983
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 985
    goto :goto_0

    .line 987
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyOnFramebufferUnBlocked(Ljava/lang/String;)V

    .line 988
    iput v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mblockedAppid:I

    move v7, v9

    .line 989
    goto :goto_0

    .line 1000
    .end local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    :cond_4
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    iput v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mblockedAppid:I

    .line 1002
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    invoke-virtual {v6, v10}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 1003
    .restart local v0    # "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v0, :cond_5

    .line 1004
    const-string v9, "TMSVncCore"

    const-string v10, "setNativeFbBlockInfo : appInfo is null"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 1005
    goto :goto_0

    .line 1008
    :cond_5
    const-string v8, "TMSVncCore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "setNativeFbBlockInfo() App "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 1009
    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1008
    invoke-static {v8, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x4

    const/4 v10, 0x4

    if-eq v8, v10, :cond_6

    .line 1012
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x8

    const/16 v10, 0x8

    if-eq v8, v10, :cond_6

    .line 1013
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x10

    const/16 v10, 0x10

    if-eq v8, v10, :cond_6

    .line 1014
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x20

    const/16 v10, 0x20

    if-eq v8, v10, :cond_6

    .line 1015
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit16 v8, v8, 0x400

    const/16 v10, 0x400

    if-ne v8, v10, :cond_8

    .line 1017
    :cond_6
    const-string v8, "TMSVncCore"

    .line 1018
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " VncCore() : Received Reason value from Client for FB Blocking is :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1019
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Hence Blocking FBUpdate "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1018
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1017
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncFrameBufferBlocked(I)V

    .line 1119
    :cond_7
    :goto_1
    const-string v8, "TMSVncCore"

    const-string v9, "setNativeFbBlockInfo() Exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1022
    :cond_8
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x1

    if-eq v8, v9, :cond_9

    .line 1023
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    and-int/lit8 v8, v8, 0x2

    const/4 v9, 0x2

    if-ne v8, v9, :cond_12

    .line 1024
    :cond_9
    const-string v8, "TMSVncCore"

    .line 1025
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " VncCore() : Received Reason value from Client for FB Blocking is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1026
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1025
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1024
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    const/4 v1, -0x1

    .line 1029
    .local v1, "blockedCount":I
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1030
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCallbackRegistedAppMap:Ljava/util/Map;

    .line 1031
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1034
    const-string v8, "TMSVncCore"

    .line 1035
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " VncCore() : Received Reason value from Client for FB Blocking may be 0 or 1 bit. Blocked count is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1036
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1035
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1033
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    :cond_a
    const/4 v5, 0x0

    .line 1040
    .local v5, "isHandleBlock":Z
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1041
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 1042
    const-string v8, "TMSVncCore"

    .line 1043
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " VncCore() : Received isHandleBlock value  for FB Blocking is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1044
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1043
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1042
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    :cond_b
    if-nez v5, :cond_c

    .line 1047
    const-string v8, "TMSVncCore"

    .line 1048
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VncCore() : Handleblock is false. Hence, Terminating the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1049
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1048
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1047
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->vncFrameBufferBlocked(I)V

    .line 1055
    :cond_c
    if-nez v1, :cond_d

    if-eqz v5, :cond_d

    .line 1056
    const-string v8, "TMSVncCore"

    .line 1057
    const-string v9, "VncCore() : BlockCount = 0 & HandleBlocking =True:"

    .line 1056
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    const-string v8, "TMSVncCore"

    .line 1060
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() :  App id CCC Certified. Hence, Notifying Callback to the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1061
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1060
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1058
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    const-string v8, "TMSVncCore"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeFBBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1064
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1063
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    .line 1066
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1065
    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyFramebufferBlocked(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1068
    :cond_d
    if-lez v1, :cond_7

    if-eqz v5, :cond_7

    .line 1069
    const-string v8, "TMSVncCore"

    .line 1070
    const-string v9, "VncCore() : BlockCount > 0 & HandleBlocking =True:"

    .line 1069
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    const/4 v4, 0x0

    .line 1072
    .local v4, "isCCCcertified":Z
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    .line 1073
    .local v2, "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    if-eqz v2, :cond_f

    .line 1074
    iget-object v8, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v8, :cond_f

    .line 1076
    iget-object v8, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_e
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_10

    .line 1083
    :cond_f
    if-eqz v4, :cond_11

    .line 1084
    const-string v8, "TMSVncCore"

    .line 1085
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VncCore() :  App id CCC Certified. Hence, Notifying Callback to the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1086
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1085
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1084
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    const-string v8, "TMSVncCore"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeFBBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1089
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1088
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    .line 1091
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1090
    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyFramebufferBlocked(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1076
    :cond_10
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 1077
    .local v3, "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    const-string v9, "CCC"

    iget-object v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1078
    const/4 v4, 0x1

    goto :goto_2

    .line 1093
    .end local v3    # "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    :cond_11
    const-string v8, "TMSVncCore"

    .line 1094
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "VncCore() : App id Not CCC Certified. Hence, Terminating the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1095
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1094
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1093
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1096
    if-eqz v6, :cond_7

    .line 1097
    iget v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    goto/16 :goto_1

    .line 1104
    .end local v1    # "blockedCount":I
    .end local v2    # "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    .end local v4    # "isCCCcertified":Z
    .end local v5    # "isHandleBlock":Z
    :cond_12
    const-string v8, "TMSVncCore"

    .line 1105
    const-string v9, " VncCore() : Updating the Test Application to handle the FB blocking"

    .line 1104
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedReason:I

    .line 1117
    iget-object v10, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mFbBlockedRect:Landroid/os/Bundle;

    iget-object v11, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 1116
    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyFramebufferBlocked(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public setNativeMicOpen(Ljava/lang/String;)V
    .locals 5
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 1237
    if-eqz p1, :cond_1

    .line 1238
    new-instance v1, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v1}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 1239
    .local v1, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v1, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 1240
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    .line 1241
    const-string v3, "micstatus"

    invoke-virtual {v1, v3}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1240
    invoke-virtual {v2, v3}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->convertEnabledToBoolean(I)Z

    move-result v0

    .line 1248
    .local v0, "micEnabled":Z
    const-string v2, "TMSVncCore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setNativeMicOpen() Enter micEnabled:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isMicOpen:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v0, :cond_0

    .line 1253
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    .line 1254
    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->notifyMicrohphoneStatusChanged(Z)V

    .line 1255
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isMicOpen:Ljava/lang/Boolean;

    .line 1258
    :cond_0
    const-string v2, "TMSVncCore"

    const-string v3, "setNativeMicOpen() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1260
    .end local v0    # "micEnabled":Z
    .end local v1    # "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    :cond_1
    return-void
.end method

.method public setNativeNightMode(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1206
    const-string v0, "TMSVncCore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setNativeNightMode() Enter"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isNightModeEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1208
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mDeviceStatusManager:Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/commonapi/DeviceStatusManager;->notifyNightModeChanged(Z)V

    .line 1214
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isNightModeEnabled:Ljava/lang/Boolean;

    .line 1217
    :cond_0
    const-string v0, "TMSVncCore"

    const-string v1, "setNativeNightMode() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    return-void
.end method

.method public setNativeRemoteDisplayConnection(I)V
    .locals 2
    .param p1, "config"    # I

    .prologue
    .line 350
    const-string v0, "TMSVncCore"

    const-string v1, "setNativeRemoteDisplayConnection() Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->isRemoteDisplayConnectionChanged(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    const-string v0, "TMSVncCore"

    .line 354
    const-string v1, "setNativeRemoteDisplayConnection() : Sending Display callback notification to Connection Manager"

    .line 352
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    iput p1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    .line 356
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 357
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mRemoteDisplayConnection:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyRemoteDisplayConnectionChanged(I)V

    .line 359
    :cond_0
    const-string v0, "TMSVncCore"

    const-string v1, "setNativeRemoteDisplayConnection() Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    return-void
.end method

.method public setNativeServerScalingConfig(Ljava/lang/String;)V
    .locals 3
    .param p1, "config"    # Ljava/lang/String;

    .prologue
    .line 302
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeServerScalingConfig() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v0, 0x0

    .line 304
    .local v0, "displayConfig":Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;
    if-eqz p1, :cond_0

    .line 305
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillServerScalingConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    if-eqz v1, :cond_0

    .line 307
    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mServerScalingConfig:Lcom/samsung/android/mirrorlink/commonapi/DisplayConfig;

    .line 310
    :cond_0
    const-string v1, "TMSVncCore"

    const-string v2, "setNativeServerScalingConfig() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    return-void
.end method

.method public setNativetEventConfig(Ljava/lang/String;)V
    .locals 4
    .param p1, "config"    # Ljava/lang/String;

    .prologue
    .line 499
    const-string v2, "TMSVncCore"

    const-string v3, "setNativetEventConfig() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const/4 v1, 0x0

    .line 501
    .local v1, "eventConfig":Lcom/samsung/android/mirrorlink/commonapi/EventConfig;
    if-eqz p1, :cond_1

    .line 502
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->fillEventConfigFromString(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    move-result-object v1

    .line 504
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 505
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "KEYBOARD_LANGUAGE"

    .line 506
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKeyboardLanguage()Ljava/lang/String;

    move-result-object v3

    .line 505
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    const-string v2, "KEYBOARD_COUNTRY"

    .line 508
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKeyboardCountry()Ljava/lang/String;

    move-result-object v3

    .line 507
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const-string v2, "UI_LANGUAGE"

    .line 510
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getUILanguage()Ljava/lang/String;

    move-result-object v3

    .line 509
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v2, "UI_COUNTRY"

    .line 512
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getUICountry()Ljava/lang/String;

    move-result-object v3

    .line 511
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v2, "KNOB_KEY_SUPPORT"

    .line 514
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getKnobSupport()I

    move-result v3

    .line 513
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 515
    const-string v2, "DEVICE_KEY_SUPPORT"

    .line 516
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getDeviceKeySupport()I

    move-result v3

    .line 515
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 517
    const-string v2, "FUNC_KEY_SUPPORT"

    .line 518
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getFuncKeySupport()I

    move-result v3

    .line 517
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 519
    const-string v2, "ITU_KEY_SUPPORT"

    .line 520
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getItuKeySupport()Z

    move-result v3

    .line 519
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 521
    const-string v2, "DEVICE_KEY_SUPPORT"

    .line 522
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getDeviceKeySupport()I

    move-result v3

    .line 521
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 523
    const-string v2, "MULTIMEDIA_KEY_SUPPORT"

    .line 524
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getMmKeySupport()I

    move-result v3

    .line 523
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 525
    const-string v2, "TOUCH_SUPPORT"

    .line 526
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getTouchSupport()I

    move-result v3

    .line 525
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 527
    const-string v2, "PRESSURE_MASK"

    .line 528
    invoke-virtual {v1}, Lcom/samsung/android/mirrorlink/commonapi/EventConfig;->getPressureMask()I

    move-result v3

    .line 527
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 530
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    if-nez v2, :cond_2

    .line 531
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    .line 532
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->notifyEventConfigurationChanged(Landroid/os/Bundle;)V

    .line 542
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->notifyKnobSupport()V

    .line 545
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    const-string v2, "TMSVncCore"

    const-string v3, "setNativetEventConfig() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    return-void

    .line 537
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 538
    iput-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventConfig:Lcom/samsung/android/mirrorlink/commonapi/EventConfig;

    .line 539
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mEventMappingManager:Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/EventMappingManager;->notifyEventConfigurationChanged(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public showVirtualKeyboard(ZLjava/lang/String;Z)Z
    .locals 3
    .param p1, "vbKeyboardFlag"    # Z
    .param p2, "textEntry"    # Ljava/lang/String;
    .param p3, "keyEventList"    # Z

    .prologue
    .line 748
    const-string v1, "TMSVncCore"

    const-string v2, "showVirtualKeyboard() Enter"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const/4 v0, 0x0

    .line 750
    .local v0, "ret":Z
    const-string v1, "TMSVncCore"

    const-string v2, " NOT SUPPORTED "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const-string v1, "TMSVncCore"

    const-string v2, "showVirtualKeyboard() Exit"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    return v0
.end method

.method public switchFbOrientation(Z)Z
    .locals 4
    .param p1, "fbOrientation"    # Z

    .prologue
    .line 409
    const-string v2, "TMSVncCore"

    const-string v3, "switchFbOrientation() Enter"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v1, 0x1

    .line 411
    .local v1, "ret":Z
    const/4 v0, 0x0

    .line 412
    .local v0, "orientation":I
    if-eqz p1, :cond_0

    .line 413
    const/4 v0, 0x1

    .line 415
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/VncCore;->mCommonApi:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v2}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_switchFbOrientation(I)I

    .line 416
    const-string v2, "TMSVncCore"

    const-string v3, "switchFbOrientation() Exit"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return v1
.end method
