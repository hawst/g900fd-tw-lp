.class public Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;
.super Ljava/lang/Object;
.source "AudioConnMngr.java"

# interfaces
.implements Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener;


# static fields
.field private static final BIT_FOUR_SET:I = 0x10

.field private static final BIT_ONE_SET:I = 0x2

.field private static final BIT_THREE_SET:I = 0x8

.field private static final BIT_TWO_SET:I = 0x4

.field private static final BIT_ZERO_SET:I = 0x1

.field private static final CCC:Ljava/lang/String; = "CCC"

.field private static final LOG_TAG:Ljava/lang/String; = "TMSAudioConnMngr"


# instance fields
.field private mA2dpConn:Z

.field private mAudioBlockedAppId:I

.field private mAudioBlockedReason:I

.field private mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

.field private mCallbackRegistedAppMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

.field private mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

.field private mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

.field private mHandleBlockingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mHfpConn:Z

.field private mRtpClientConn:Z

.field private mRtpClientPayloadType:I

.field private mRtpServerConn:Z

.field private mRtpServerPayloadType:I

.field private mblockedReasonValue:I

.field private oldPayloadTypes:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V
    .locals 2
    .param p1, "commonApiSvcManager"    # Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "Enter Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 55
    new-instance v0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    .line 58
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "Exit Constructor"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method private fillAudioBlockInfoFromString(Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 522
    new-instance v0, Lcom/samsung/android/mirrorlink/util/TmParams;

    invoke-direct {v0}, Lcom/samsung/android/mirrorlink/util/TmParams;-><init>()V

    .line 523
    .local v0, "param":Lcom/samsung/android/mirrorlink/util/TmParams;
    invoke-virtual {v0, p1}, Lcom/samsung/android/mirrorlink/util/TmParams;->unflatten(Ljava/lang/String;)V

    .line 525
    const-string v1, "reason"

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    .line 526
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fillFbBlockInfoFromString : AudioBlockedReason"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string v1, "appId"

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/util/TmParams;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedAppId:I

    .line 529
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fillFbBlockInfoFromString : AudioBlockedAppId"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedAppId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    return-void
.end method


# virtual methods
.method public clearPackageFromMap(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 504
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "clearPackageFromMap() : Enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    if-nez p1, :cond_0

    .line 506
    const-string v0, "TMSAudioConnMngr"

    .line 507
    const-string v1, "Died 3rd party Application\'s Package Name is Not present in MAP"

    .line 506
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :goto_0
    return-void

    .line 512
    :cond_0
    const-string v0, "TMSAudioConnMngr"

    .line 513
    const-string v1, "Died 3rd party Application\'s Package Name is present in MAP. Hence, Removing from the MAP"

    .line 512
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "clearPackageFromMap() : Exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAudioConnections()Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;
    .locals 2

    .prologue
    .line 72
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.getAudioConnections() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    return-object v0
.end method

.method public getOldPayload()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.getOldPayload() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->oldPayloadTypes:Ljava/lang/String;

    return-object v0
.end method

.method public handleEvent(Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;)V
    .locals 2
    .param p1, "e"    # Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    .prologue
    .line 536
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STARTED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_1

    .line 537
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mRtpAppHolder:Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/AudioRtpAppholder;->setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    .line 538
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsEngine()Lcom/samsung/android/mirrorlink/engine/TmsEngine;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/engine/TmsEngine;->mBtAppHolder:Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->setCommonApi(Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;)V

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    sget-object v0, Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;->SERVICE_STOPPED:Lcom/samsung/android/mirrorlink/commonapi/ITMServerListener$Event;

    if-ne p1, v0, :cond_0

    .line 543
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioConnManager.handleEvent() engine is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetAudioContext(Ljava/lang/String;)Z
    .locals 8
    .param p1, "packName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 281
    const-string v6, "TMSAudioConnMngr"

    const-string v7, "resetAudioContext() Enter"

    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 284
    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v2

    .line 286
    .local v2, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    const/4 v1, 0x0

    .line 287
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v2, :cond_0

    .line 288
    invoke-virtual {v2, p1}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 290
    :cond_0
    if-nez v1, :cond_1

    .line 291
    const-string v5, "TMSAudioConnMngr"

    .line 292
    const-string v6, "AudioConnManager.resetAudioCtxtInfo : appInfo is null"

    .line 291
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_0
    return v4

    .line 295
    :cond_1
    iget v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 297
    .local v0, "appId":I
    const-string v6, "TMSAudioConnMngr"

    .line 298
    const-string v7, "AudioConnManager.resetAudioCtxtInfo : Calling Native Method"

    .line 297
    invoke-static {v6, v7}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v6, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    invoke-virtual {v6}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v6

    .line 301
    invoke-virtual {v6, v0}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_reSetAudioCtxtInfo(I)I

    move-result v3

    .line 302
    .local v3, "val":I
    if-ne v3, v5, :cond_2

    .line 304
    const-string v4, "TMSAudioConnMngr"

    .line 305
    const-string v6, "AudioConnManager.resetAudioCtxtInfo : Reset of Audio context on Native is successful"

    .line 303
    invoke-static {v4, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 306
    goto :goto_0

    .line 309
    :cond_2
    const-string v5, "TMSAudioConnMngr"

    .line 310
    const-string v6, "AudioConnManager.resetAudioCtxtInfo :  Reset of Audio context on Native is failure"

    .line 308
    invoke-static {v5, v6}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioCtxtInfo(Z[ILjava/lang/String;Z)Z
    .locals 12
    .param p1, "audioAvail"    # Z
    .param p2, "audioCategories"    # [I
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "handleBlocking"    # Z

    .prologue
    .line 221
    const-string v9, "TMSAudioConnMngr"

    const-string v10, "AudioConnManager.setAudioCtxtInfo() Enter"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const/4 v4, 0x0

    .line 227
    .local v4, "ret":Z
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 228
    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v3

    .line 229
    .local v3, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-nez v3, :cond_0

    .line 230
    const-string v9, "TMSAudioConnMngr"

    .line 231
    const-string v10, "AudioConnManager.setAudioCtxtInfo : AppManager is null"

    .line 230
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v9, 0x0

    .line 276
    :goto_0
    return v9

    .line 236
    :cond_0
    invoke-virtual {v3, p3}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppName(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v1

    .line 237
    .local v1, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-nez v1, :cond_1

    .line 238
    const-string v9, "TMSAudioConnMngr"

    .line 239
    const-string v10, "AudioConnManager.setAudioCtxtInfo : appInfo is null"

    .line 238
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v9, 0x0

    goto :goto_0

    .line 242
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v9, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 243
    const-string v9, "TMSAudioConnMngr"

    .line 244
    const-string v10, "AudioConnManager.setAudioCtxtInfo : Inserting a new Package Info to the MAP"

    .line 243
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    :goto_1
    iget v0, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 256
    .local v0, "appId":I
    const/4 v2, 0x0

    .line 257
    .local v2, "avail":I
    if-eqz p1, :cond_2

    .line 258
    const/4 v2, 0x1

    .line 260
    :cond_2
    array-length v5, p2

    .line 261
    .local v5, "size":I
    const-string v9, "TMSAudioConnMngr"

    .line 262
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "AudioConnManager.setAudioCtxtInfo().size value is :  "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 261
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v9, "TMSAudioConnMngr"

    .line 266
    const-string v10, "AudioConnManager.setAudioCtxtInfo().Adding HBlocking to Map :  "

    .line 265
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 269
    invoke-virtual {v9}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getCommonAPIService()Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;

    move-result-object v9

    .line 270
    invoke-virtual {v9, v2, p2, v0, v5}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService;->native_ca_setAudioCtxtInfo(I[III)I

    move-result v6

    .line 271
    .local v6, "val":I
    const/4 v9, 0x1

    if-ne v6, v9, :cond_3

    .line 272
    const/4 v4, 0x1

    .line 275
    :cond_3
    const-string v9, "TMSAudioConnMngr"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "AudioConnManager.setAudioCtxtInfo() Exit "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v9, v4

    .line 276
    goto :goto_0

    .line 248
    .end local v0    # "appId":I
    .end local v2    # "avail":I
    .end local v5    # "size":I
    .end local v6    # "val":I
    :cond_4
    const-string v9, "TMSAudioConnMngr"

    .line 249
    const-string v10, "AudioConnManager.setAudioCtxtInfo : Incrementing the Value for the Package Received Blocking"

    .line 247
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 252
    .local v7, "value":I
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "value":I
    .local v8, "value":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public setBtA2dpInfo(Z)V
    .locals 3
    .param p1, "a2dpConnected"    # Z

    .prologue
    .line 101
    const-string v0, "TMSAudioConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioManager.setBtA2dpInfo() prev "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mA2dpConn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mA2dpConn:Z

    if-eq v0, p1, :cond_0

    .line 103
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mA2dpConn:Z

    .line 104
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mA2dpConn:Z

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 107
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtA2dpInfo() BTA2DP Media out "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)V

    .line 121
    :cond_0
    return-void

    .line 109
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 112
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtA2dpInfo() RTP Media out"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 116
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtA2dpInfo() No Media out"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBtHfpInfo(Z)V
    .locals 6
    .param p1, "hfpConnected"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 77
    const-string v0, "TMSAudioConnMngr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioManager.setBtHfpInfo() prev "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    if-eq v0, p1, :cond_0

    .line 79
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    .line 80
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 83
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtHfpInfo() Voice and Phone Use BTHFP"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v1}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)V

    .line 98
    :cond_0
    return-void

    .line 85
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientConn:Z

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v5}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 88
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtHfpInfo() Voice Command can use RTP"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0, v3}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 93
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setBtHfpInfo() No Phone Audio"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setConnectionManager(Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;)V
    .locals 0
    .param p1, "connMngr"    # Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    .line 63
    return-void
.end method

.method public setContextManager(Lcom/samsung/android/mirrorlink/commonapi/ContextManager;)V
    .locals 0
    .param p1, "connMngr"    # Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 67
    return-void
.end method

.method public setNativeAudioBlockInfo(Ljava/lang/String;)Z
    .locals 11
    .param p1, "config"    # Ljava/lang/String;

    .prologue
    .line 317
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "setNativeAudioBlockInfo() Enter"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v7, 0x0

    .line 320
    .local v7, "ret":Z
    if-nez p1, :cond_0

    .line 321
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "setNativeAudioBlockInfo() config is null"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    .end local v7    # "ret":Z
    :goto_0
    return v7

    .line 324
    .restart local v7    # "ret":Z
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCommonAPIService:Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;

    .line 325
    invoke-virtual {v8}, Lcom/samsung/android/mirrorlink/commonapi/CommonAPIService$CommonApiSvcManager;->getTmsAppManager()Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-result-object v6

    .line 326
    .local v6, "receivedTmsAppMngrObject":Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    if-nez v6, :cond_1

    .line 327
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "setNativeAudioBlockInfo : AppManager is null"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const/4 v7, 0x0

    goto :goto_0

    .line 331
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->fillAudioBlockInfoFromString(Ljava/lang/String;)V

    .line 333
    const/4 v0, 0x0

    .line 335
    .local v0, "appInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v6, :cond_2

    .line 337
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 339
    :cond_2
    if-nez v0, :cond_3

    .line 340
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "setNativeAudioBlockInfo : appInfo is null"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const/4 v7, 0x0

    goto :goto_0

    .line 345
    :cond_3
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    if-nez v8, :cond_6

    .line 346
    const-string v8, "TMSAudioConnMngr"

    .line 347
    const-string v9, "setNativeAudioBlockInfo :Received reason is 0, So handling unblocking"

    .line 346
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    and-int/lit8 v8, v8, 0x8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_4

    .line 350
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    and-int/lit8 v8, v8, 0x10

    const/16 v9, 0x10

    if-ne v8, v9, :cond_9

    .line 351
    :cond_4
    const-string v8, "TMSAudioConnMngr"

    .line 352
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : Blocked Reason value from Client that needs to Unblock is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 353
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 352
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 351
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v8, "TMSAudioConnMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeAudioBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 356
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 355
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyOnAudioUnBlocked(Ljava/lang/String;)V

    .line 393
    :cond_5
    :goto_1
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "Notifying Audio Unblocking() Exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const/4 v8, -0x1

    iput v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    .line 399
    :cond_6
    const-string v8, "TMSAudioConnMngr"

    .line 400
    const-string v9, "setNativeAudioBlockInfo() : Storing mAudioBlockedReason in a temp variable to handle AudioUnBlocking"

    .line 398
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    iput v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    .line 404
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    and-int/lit8 v8, v8, 0x2

    const/4 v9, 0x2

    if-eq v8, v9, :cond_7

    .line 405
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    and-int/lit8 v8, v8, 0x4

    const/4 v9, 0x4

    if-ne v8, v9, :cond_e

    .line 406
    :cond_7
    const-string v8, "TMSAudioConnMngr"

    .line 407
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : Received Reason value from Client for Audio Blocking is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 408
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 409
    const-string v10, "Server Terminating the APP using APPID"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 407
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 406
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    .line 498
    :cond_8
    :goto_2
    const-string v8, "TMSAudioConnMngr"

    const-string v9, "setNativeAudioBlockInfo() Exit"

    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_9
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    and-int/lit8 v8, v8, 0x1

    const/4 v9, 0x1

    if-ne v8, v9, :cond_c

    .line 361
    const-string v8, "TMSAudioConnMngr"

    .line 362
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : Blocked Reason value from Client that needs to Unblock is: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 363
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 362
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 361
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const/4 v5, 0x0

    .line 366
    .local v5, "isHandleBlock":Z
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 367
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    .line 368
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 371
    :cond_a
    if-eqz v5, :cond_b

    .line 373
    const-string v8, "TMSAudioConnMngr"

    .line 374
    const-string v9, "AudioConnMngr().setNativeAudioUnBlockingInfo() : Handle blocking parameter is set, so notifying the App"

    .line 372
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    .line 376
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyOnAudioUnBlocked(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 379
    :cond_b
    const-string v8, "TMSAudioConnMngr"

    .line 380
    const-string v9, "AudioConnMngr().setNativeAudioUnBlockingInfo() : Handle blocking parameter is NOT set, So, Stopping the App"

    .line 378
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    goto/16 :goto_1

    .line 384
    .end local v5    # "isHandleBlock":Z
    :cond_c
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    and-int/lit8 v8, v8, 0x2

    const/4 v9, 0x2

    if-eq v8, v9, :cond_d

    .line 385
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    and-int/lit8 v8, v8, 0x4

    const/4 v9, 0x4

    if-ne v8, v9, :cond_5

    .line 386
    :cond_d
    const-string v8, "TMSAudioConnMngr"

    .line 387
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : Blocked Reason value from Client that needs to Unblock is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 388
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mblockedReasonValue:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 387
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 386
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    goto/16 :goto_1

    .line 412
    :cond_e
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    and-int/lit8 v8, v8, 0x1

    const/4 v9, 0x1

    if-ne v8, v9, :cond_17

    .line 413
    const-string v8, "TMSAudioConnMngr"

    .line 414
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " AudioConnMngr() : Received Reason value from Client for Audio Blocking is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 415
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 414
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 413
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const/4 v1, -0x1

    .line 418
    .local v1, "blockedCount":I
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 419
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mCallbackRegistedAppMap:Ljava/util/Map;

    .line 420
    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 422
    :cond_f
    const/4 v5, 0x0

    .line 424
    .restart local v5    # "isHandleBlock":Z
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 425
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHandleBlockingMap:Ljava/util/Map;

    iget-object v9, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 428
    :cond_10
    if-nez v1, :cond_11

    if-eqz v5, :cond_11

    .line 430
    const-string v8, "TMSAudioConnMngr"

    .line 431
    const-string v9, "AudioConnMnrg() : BlockCount = 0 & HandleBlocking =True:"

    .line 430
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v8, "TMSAudioConnMngr"

    .line 434
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() :  App id CCC Certified. Hence, Notifying Callback to the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 435
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 434
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 432
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v8, "TMSAudioConnMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeAudioBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 438
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 437
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    .line 440
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 439
    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyAudioBlocked(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 442
    :cond_11
    if-lez v1, :cond_16

    if-eqz v5, :cond_16

    .line 443
    const-string v8, "TMSAudioConnMngr"

    .line 444
    const-string v9, "AudioConnMnrg() : BlockCount = 0 & HandleBlocking =True:"

    .line 443
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v8, "TMSAudioConnMngr"

    .line 446
    const-string v9, "Same application is blocked more than once. "

    .line 445
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const/4 v4, 0x0

    .line 448
    .local v4, "isCCCcertified":Z
    iget-object v2, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppCertInfo:Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;

    .line 449
    .local v2, "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    if-eqz v2, :cond_13

    .line 450
    iget-object v8, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    if-eqz v8, :cond_13

    .line 452
    iget-object v8, v2, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;->mAppCertificateEntityInfo:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_12
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_14

    .line 459
    :cond_13
    if-eqz v4, :cond_15

    .line 461
    const-string v8, "TMSAudioConnMngr"

    .line 462
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() :  App id CCC Certified. Hence, Notifying Callback to the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 463
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 462
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 460
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string v8, "TMSAudioConnMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeAudioBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 466
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 465
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    .line 468
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 467
    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyAudioBlocked(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 452
    :cond_14
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;

    .line 453
    .local v3, "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    const-string v9, "CCC"

    iget-object v10, v3, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;->mEntityName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 454
    const/4 v4, 0x1

    goto :goto_3

    .line 471
    .end local v3    # "entityInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertEntityInfo;
    :cond_15
    const-string v8, "TMSAudioConnMngr"

    .line 472
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : App id Not CCC Certified. Hence, Terminating the APP:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 473
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 472
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 471
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    if-eqz v6, :cond_8

    .line 475
    iget v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    goto/16 :goto_2

    .line 480
    .end local v2    # "certInfo":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo$AppCertInfo;
    .end local v4    # "isCCCcertified":Z
    :cond_16
    const-string v8, "TMSAudioConnMngr"

    .line 481
    const-string v9, "AudioConnMngr() : Either Application is not registered for handle blocking or it is not registered for called"

    .line 479
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget v8, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    invoke-virtual {v6, v8}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->stopApp(I)Z

    goto/16 :goto_2

    .line 486
    .end local v1    # "blockedCount":I
    .end local v5    # "isHandleBlock":Z
    :cond_17
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    and-int/lit8 v8, v8, 0x8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_18

    .line 487
    iget v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    and-int/lit8 v8, v8, 0x10

    const/16 v9, 0x10

    if-ne v8, v9, :cond_8

    .line 488
    :cond_18
    const-string v8, "TMSAudioConnMngr"

    .line 489
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AudioConnMngr() : Received Reason value from Client for Audio Blocking is:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 490
    iget v10, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 489
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 488
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string v8, "TMSAudioConnMngr"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setNativeAudioBlockInfo : for pckgName"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 493
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 492
    invoke-static {v8, v9}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v8, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mContextManager:Lcom/samsung/android/mirrorlink/commonapi/ContextManager;

    iget v9, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioBlockedReason:I

    .line 495
    iget-object v10, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    .line 494
    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/mirrorlink/commonapi/ContextManager;->notifyAudioBlocked(ILjava/lang/String;)V

    goto/16 :goto_2
.end method

.method public setOldPayload(Ljava/lang/String;)V
    .locals 2
    .param p1, "oldPayload"    # Ljava/lang/String;

    .prologue
    .line 173
    const-string v0, "TMSAudioConnMngr"

    const-string v1, "AudioManager.setOldPayload() "

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->oldPayloadTypes:Ljava/lang/String;

    .line 176
    return-void
.end method

.method public setRtpClientInfo(Z)V
    .locals 7
    .param p1, "conn"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 184
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioManager.setRtpClientInfo() conn prev "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientConn:Z

    if-eq p1, v1, :cond_0

    .line 186
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientConn:Z

    .line 187
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientConn:Z

    if-eqz v1, :cond_2

    .line 188
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v6}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioIn(I)V

    .line 189
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() RTP Media in "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    if-eqz v1, :cond_1

    .line 191
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 192
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 193
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() BTHFP voiceControl"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :goto_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientPayloadType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 213
    .local v0, "IntWrapper":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPayloadTypes(Ljava/lang/String;)V

    .line 214
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() notifying callback "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)V

    .line 217
    .end local v0    # "IntWrapper":Ljava/lang/Integer;
    :cond_0
    return-void

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v6}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 196
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v6}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 197
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() RTP Voice Control"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v5}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioIn(I)V

    .line 201
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() RTP Media in Not Connected "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mHfpConn:Z

    if-eqz v1, :cond_3

    .line 203
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 204
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 205
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() BTHFP voiceControl"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v5}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setVoiceControl(I)V

    .line 208
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v5}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPhoneAudio(I)V

    .line 209
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() No Voice Control"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setRtpClientInfo(ZI)V
    .locals 4
    .param p1, "conn"    # Z
    .param p2, "payloadType"    # I

    .prologue
    .line 161
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioManager.setRtpClientInfo() payload prev "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientPayloadType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() : updating old value of payload"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientPayloadType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 165
    .local v0, "IntParsing":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setOldPayload(Ljava/lang/String;)V

    .line 167
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpClientInfo() : updating new value of payload"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iput p2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpClientPayloadType:I

    .line 169
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpClientInfo(Z)V

    .line 170
    return-void
.end method

.method public setRtpServerInfo(Z)V
    .locals 4
    .param p1, "conn"    # Z

    .prologue
    .line 135
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioManager.setRtpServerInfo() conn prev "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    if-eq p1, v1, :cond_0

    .line 137
    iput-boolean p1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    .line 138
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerConn:Z

    if-eqz v1, :cond_1

    .line 140
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 141
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() RTP Media out "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :goto_0
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerPayloadType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 154
    .local v0, "IntWrapper":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setPayloadTypes(Ljava/lang/String;)V

    .line 155
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() notifying callback "

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mConnectionManager:Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/ConnectionManager;->notifyAudioConnectionsChanged(Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;)V

    .line 158
    .end local v0    # "IntWrapper":Ljava/lang/Integer;
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mA2dpConn:Z

    if-eqz v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 146
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() A2DP Media out"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mAudioConn:Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnection;->setMediaAudioOut(I)V

    .line 150
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() No Media out"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setRtpServerInfo(ZI)V
    .locals 4
    .param p1, "conn"    # Z
    .param p2, "payloadType"    # I

    .prologue
    .line 124
    const-string v1, "TMSAudioConnMngr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioManager.setRtpServerInfo() payload prev "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerPayloadType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() : updating old value of payload"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget v1, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerPayloadType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 128
    .local v0, "IntParsing":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setOldPayload(Ljava/lang/String;)V

    .line 129
    const-string v1, "TMSAudioConnMngr"

    const-string v2, "AudioManager.setRtpServerInfo() : updating new value of payload"

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iput p2, p0, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->mRtpServerPayloadType:I

    .line 131
    invoke-virtual {p0, p1}, Lcom/samsung/android/mirrorlink/commonapi/AudioConnMngr;->setRtpServerInfo(Z)V

    .line 132
    return-void
.end method
