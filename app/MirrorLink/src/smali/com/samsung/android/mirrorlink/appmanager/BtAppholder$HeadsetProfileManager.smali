.class Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;
.super Ljava/lang/Object;
.source "BtAppholder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HeadsetProfileManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;
    }
.end annotation


# instance fields
.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCntxt:Landroid/content/Context;

.field private mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mService:Landroid/bluetooth/BluetoothHeadset;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1
    .param p1, "cntxt"    # Landroid/content/Context;
    .param p2, "adapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 2583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mCntxt:Landroid/content/Context;

    .line 2586
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2587
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;

    invoke-direct {v0, p0, p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;-><init>(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)V

    iput-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 2588
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Z
    .locals 1

    .prologue
    .line 2716
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->isTmConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1

    .prologue
    .line 2626
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Landroid/bluetooth/BluetoothHeadset;
    .locals 1

    .prologue
    .line 2536
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothHeadset;)V
    .locals 0

    .prologue
    .line 2536
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 2749
    invoke-direct {p0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 2615
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;)V
    .locals 0

    .prologue
    .line 2693
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disConnectDevices()V

    return-void
.end method

.method private callBluetoothHfpConnect(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "hfp"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2762
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2763
    :cond_0
    const-string v2, "TMSBtAppholder"

    const-string v3, "Service is disconnected already"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2777
    :goto_0
    return-void

    .line 2768
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "connect"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2769
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2770
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 2771
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2772
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2773
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2774
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2775
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpConnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callBluetoothHfpDisconnect(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1, "hfp"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2781
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2782
    :cond_0
    const-string v2, "TMSBtAppholder"

    const-string v3, "Service is disconnected already"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2796
    :goto_0
    return-void

    .line 2787
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "disconnect"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2788
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2789
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 2790
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpDisconnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2791
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2792
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpDisconnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2793
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2794
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpDisconnect."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private callBluetoothHfpSetPriority(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;I)V
    .locals 7
    .param p1, "hfp"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "priority"    # I

    .prologue
    .line 2800
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2801
    :cond_0
    const-string v2, "TMSBtAppholder"

    const-string v3, "Service is disconnected already"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2815
    :goto_0
    return-void

    .line 2806
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setPriority"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2807
    .local v1, "listener":Ljava/lang/reflect/Method;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2808
    .end local v1    # "listener":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 2809
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2810
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 2811
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2812
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 2813
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "TMSBtAppholder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to callBluetoothHfpSetPriority."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private connect(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 13
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2628
    const-string v9, "TMSBtAppholder"

    const-string v10, " HeadsetProfileManager connectHeadset"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2629
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    move-result-object v8

    .line 2631
    .local v8, "uuids":[Landroid/os/ParcelUuid;
    if-eqz v8, :cond_2

    .line 2632
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " !! HeadsetProfileManager uuid length ="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v8

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2634
    const/4 v2, 0x0

    .line 2635
    .local v2, "i":I
    const/4 v2, 0x0

    :goto_0
    array-length v9, v8

    if-lt v2, v9, :cond_0

    .line 2640
    :try_start_0
    const-string v9, "android.bluetooth.BluetoothUuid"

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 2641
    .local v7, "uuidClass":Ljava/lang/Class;
    const-string v9, "Handsfree"

    invoke-virtual {v7, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 2642
    .local v1, "handsFree":Ljava/lang/reflect/Field;
    const-string v9, "isUuidPresent"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 2643
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v12

    aput-object v12, v10, v11

    .line 2642
    invoke-virtual {v7, v9, v10}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 2645
    .local v3, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    .line 2646
    .local v5, "ret":Z
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v7, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 2647
    if-eqz v5, :cond_1

    .line 2650
    const-string v9, "TMSBtAppholder"

    .line 2651
    const-string v10, " !! HeadsetProfileManager has BluetoothUuid.Handsfree !!"

    .line 2650
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653
    const-class v9, Landroid/bluetooth/BluetoothProfile;

    const-string v10, "PRIORITY_ON"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 2657
    .local v4, "priority":Ljava/lang/reflect/Field;
    const-string v9, "TMSBtAppholder"

    .line 2658
    const-string v10, " !! HeadsetProfileManager has BluetoothUuid.Handsfree !!"

    .line 2657
    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2660
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v10

    invoke-direct {p0, v9, p1, v10}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->callBluetoothHfpSetPriority(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;I)V

    .line 2661
    iget-object v9, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    invoke-direct {p0, v9, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->callBluetoothHfpConnect(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_5

    .line 2662
    const/4 v9, 0x1

    .line 2687
    .end local v1    # "handsFree":Ljava/lang/reflect/Field;
    .end local v2    # "i":I
    .end local v3    # "m":Ljava/lang/reflect/Method;
    .end local v4    # "priority":Ljava/lang/reflect/Field;
    .end local v5    # "ret":Z
    .end local v7    # "uuidClass":Ljava/lang/Class;
    :goto_1
    return v9

    .line 2636
    .restart local v2    # "i":I
    :cond_0
    aget-object v6, v8, v2

    .line 2637
    .local v6, "u":Landroid/os/ParcelUuid;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " !! HeadsetProfileManager uuid="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 2665
    .end local v6    # "u":Landroid/os/ParcelUuid;
    .restart local v1    # "handsFree":Ljava/lang/reflect/Field;
    .restart local v3    # "m":Ljava/lang/reflect/Method;
    .restart local v5    # "ret":Z
    .restart local v7    # "uuidClass":Ljava/lang/Class;
    :cond_1
    :try_start_1
    const-string v9, "TMSBtAppholder"

    const-string v10, " HeadsetProfileManager NOT !!!BluetoothUuid.Handsfree"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_5

    .line 2666
    const/4 v9, 0x0

    goto :goto_1

    .line 2668
    .end local v1    # "handsFree":Ljava/lang/reflect/Field;
    .end local v3    # "m":Ljava/lang/reflect/Method;
    .end local v5    # "ret":Z
    .end local v7    # "uuidClass":Ljava/lang/Class;
    :catch_0
    move-exception v0

    .line 2669
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2687
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :goto_2
    const/4 v9, 0x0

    goto :goto_1

    .line 2670
    :catch_1
    move-exception v0

    .line 2671
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2672
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2673
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2674
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v0

    .line 2675
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2676
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_4
    move-exception v0

    .line 2677
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2678
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_5
    move-exception v0

    .line 2679
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v9, "TMSBtAppholder"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Could not connect to connect method."

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2682
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .end local v2    # "i":I
    :cond_2
    const-string v9, "TMSBtAppholder"

    const-string v10, " HeadsetProfileManager (uuids == null)"

    invoke-static {v9, v10}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->fetchUuidsWithSdp()Z

    .line 2685
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method private disConnectDevices()V
    .locals 4

    .prologue
    .line 2695
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 2696
    .local v1, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v1, :cond_1

    .line 2698
    const-string v2, "TMSBtAppholder"

    const-string v3, "disConnectDevices : getConnectedDevices() Failed"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2709
    :cond_0
    return-void

    .line 2702
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 2704
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->isMacTMClient(Ljava/lang/String;)Z
    invoke-static {v3}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$17(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2706
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->disconnect(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method

.method private disconnect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 2751
    if-nez p1, :cond_0

    .line 2753
    const-string v0, "TMSBtAppholder"

    const-string v1, " HeadsetProfileManager disconnect device is null"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2758
    :goto_0
    return-void

    .line 2756
    :cond_0
    const-string v0, "TMSBtAppholder"

    const-string v1, " HeadsetProfileManager disconnect"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2757
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->callBluetoothHfpDisconnect(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0
.end method

.method private getConnectedDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2617
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    if-nez v0, :cond_0

    .line 2619
    const-string v0, "TMSBtAppholder"

    const-string v1, "getConnectedDevices : (mService == null)"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2620
    const/4 v0, 0x0

    .line 2622
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private isTmConnected()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2718
    invoke-direct {p0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    .line 2719
    .local v1, "listConnected":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v1, :cond_0

    .line 2721
    const-string v3, "TMSBtAppholder"

    const-string v4, " HeadsetProfileManager (setConnected == null)"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741
    :goto_0
    return v2

    .line 2724
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2740
    const-string v3, "TMSBtAppholder"

    const-string v4, " HeadsetProfileManager isTmConnected false"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2724
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 2726
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_1

    .line 2728
    const-string v4, "TMSBtAppholder"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " HeadsetProfileManager.isTmConnected :  TM Addr "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 2729
    const-string v6, " Name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2728
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2730
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2732
    # getter for: Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->mTmClientMac:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder;->access$16()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2734
    const-string v2, "TMSBtAppholder"

    const-string v3, " HeadsetProfileManager isTmConnected true"

    invoke-static {v2, v3}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public deinit()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2600
    const-string v0, "TMSBtAppholder"

    const-string v1, "HEADSET deinit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2601
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 2603
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mService:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 2605
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    if-eqz v0, :cond_1

    .line 2606
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    check-cast v0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;

    invoke-virtual {v0}, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager$HfpListener;->deinit()V

    .line 2608
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 2609
    iput-object v3, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2610
    return-void
.end method

.method public init()V
    .locals 4

    .prologue
    .line 2592
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 2594
    iget-object v0, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mCntxt:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/mirrorlink/appmanager/BtAppholder$HeadsetProfileManager;->mListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 2596
    :cond_0
    return-void
.end method

.method public onStateChnaged()V
    .locals 2

    .prologue
    .line 2580
    const-string v0, "TMSBtAppholder"

    const-string v1, " HeadsetProfileManager : onStateChnaged"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581
    return-void
.end method
