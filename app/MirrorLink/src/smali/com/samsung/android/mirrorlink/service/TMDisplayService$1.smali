.class Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;
.super Ljava/lang/Object;
.source "TMDisplayService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mirrorlink/service/TMDisplayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;


# direct methods
.method constructor <init>(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 202
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v2, v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    .line 203
    const-string v1, "TMSDisplayService"

    const-string v2, "Attached."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v1, 0x0

    .line 209
    const/4 v2, 0x1

    .line 208
    :try_start_0
    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 210
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 211
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 225
    const-string v1, "TMSDisplayService"

    const-string v2, "Disconnected. So TMDisplayService will be unbinded"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iput-object v3, v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mService:Landroid/os/Messenger;

    .line 228
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    # getter for: Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mIsBound:Z
    invoke-static {v1}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$0(Lcom/samsung/android/mirrorlink/service/TMDisplayService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->access$1(Lcom/samsung/android/mirrorlink/service/TMDisplayService;Z)V

    .line 231
    iget-object v1, p0, Lcom/samsung/android/mirrorlink/service/TMDisplayService$1;->this$0:Lcom/samsung/android/mirrorlink/service/TMDisplayService;

    iget-object v1, v1, Lcom/samsung/android/mirrorlink/service/TMDisplayService;->mMessenger:Landroid/os/Messenger;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
