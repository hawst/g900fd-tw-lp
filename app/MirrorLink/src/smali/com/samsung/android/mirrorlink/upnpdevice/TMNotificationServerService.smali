.class public Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;
.super Ljava/lang/Object;
.source "TMNotificationServerService.java"

# interfaces
.implements Lcom/samsung/upnp/control/ActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;,
        Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TMSUPnP"

.field private static mNotiEnabledProfiles:[Z

.field public static mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;


# instance fields
.field mAppServerStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;

.field private mContext:Landroid/content/Context;

.field private mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

.field private mKeyMngr:Landroid/app/KeyguardManager;

.field private mNnotifSupportedApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNotifers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotiEnabledProfiles:[Z

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;Landroid/content/Context;)V
    .locals 6
    .param p1, "myDeviceAppManager"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 28
    new-instance v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;

    invoke-direct {v4, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;)V

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mAppServerStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;

    .line 62
    const-string v4, "TMSUPnP"

    .line 63
    const-string v5, "TMNotificationServerService.TMNotificationServerService enter"

    .line 62
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iput-object p1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 65
    iput-object p2, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mContext:Landroid/content/Context;

    .line 66
    const/4 v4, 0x1

    new-array v4, v4, [Z

    sput-object v4, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotiEnabledProfiles:[Z

    .line 67
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    invoke-virtual {v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNnotifSupportedApps:Ljava/util/ArrayList;

    .line 68
    new-instance v4, Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;

    invoke-direct {v4, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;-><init>(Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;)V

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;

    .line 71
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotifers:Ljava/util/ArrayList;

    .line 72
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNnotifSupportedApps:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 74
    const/4 v2, 0x0

    .local v2, "count":I
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNnotifSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "nofApp":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 96
    .end local v2    # "count":I
    .end local v3    # "nofApp":I
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mAppServerStatusCb:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$AppServerStatusCb;

    invoke-virtual {v4, v5}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->registerCallback(Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager$StatusCallack;)V

    .line 98
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mContext:Landroid/content/Context;

    const-string v5, "keyguard"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    iput-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    .line 100
    const-string v4, "TMSUPnP"

    .line 101
    const-string v5, "TMNotificationServerService.TMNotificationServerService exit"

    .line 100
    invoke-static {v4, v5}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void

    .line 75
    .restart local v2    # "count":I
    .restart local v3    # "nofApp":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    .line 76
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNnotifSupportedApps:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getAppInfoFromAppId(I)Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    move-result-object v0

    .line 78
    .local v0, "app":Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;
    if-eqz v0, :cond_2

    const-string v4, "com.android.phone"

    iget-object v5, v0, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 81
    invoke-direct {p0, v0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->getPhoneAppEventsListener(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    move-result-object v1

    .line 82
    .local v1, "appNotifier":Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotificationListener:Lcom/samsung/android/mirrorlink/upnpdevice/NotificationListener;

    invoke-virtual {v1, v4}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;->registerNotiHandler(Lcom/samsung/android/mirrorlink/upnpdevice/IAppNotifiListener;)V

    .line 83
    iget-object v4, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotifers:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    .end local v1    # "appNotifier":Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static disableNotiForClient(I)V
    .locals 3
    .param p0, "clientId"    # I

    .prologue
    .line 459
    const-string v0, "TMSUPnP"

    .line 460
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMNotificationServerService.disableNotiForClient enter clientId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 461
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 460
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 459
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotiEnabledProfiles:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p0

    .line 463
    return-void
.end method

.method public static enbleNotiForClient(I)V
    .locals 3
    .param p0, "clientId"    # I

    .prologue
    .line 452
    const-string v0, "TMSUPnP"

    .line 453
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMNotificationServerService.enbleNotiForClient enter clientId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 454
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 453
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 452
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotiEnabledProfiles:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p0

    .line 456
    return-void
.end method

.method private getNotifier(I)Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    .locals 6
    .param p1, "appId"    # I

    .prologue
    .line 428
    const-string v3, "TMSUPnP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TMNotificationServerService.getNotifier enter appId="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 429
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 428
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const/4 v2, 0x0

    .line 431
    .local v2, "notifer":Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotifers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 440
    :goto_1
    const-string v3, "TMSUPnP"

    const-string v4, "TMNotificationServerService.getNotifier exit"

    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return-object v2

    .line 432
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotifers:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "notifer":Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    check-cast v2, Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

    .line 433
    .restart local v2    # "notifer":Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    invoke-interface {v2}, Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;->getRelatedAppId()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 434
    const-string v3, "TMSUPnP"

    .line 435
    const-string v4, "TMNotificationServerService.getNotifier found notifier "

    .line 434
    invoke-static {v3, v4}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 438
    :cond_1
    const/4 v2, 0x0

    .line 431
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getPhoneAppEventsListener(Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;)Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;
    .locals 4
    .param p1, "app"    # Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;

    .prologue
    .line 105
    new-instance v0, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;

    iget-object v1, p0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mContext:Landroid/content/Context;

    .line 106
    const-string v2, "com.android.phone"

    iget v3, p1, Lcom/samsung/android/mirrorlink/appmanager/TMSAppInfo;->mAppId:I

    .line 105
    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/mirrorlink/appmanager/PhoneAppEventsListner;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    return-object v0
.end method

.method private getStringBuilder()Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    return-object v0
.end method

.method public static sendNotify(Ljava/lang/String;)V
    .locals 1
    .param p0, "notiId"    # Ljava/lang/String;

    .prologue
    .line 466
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;

    if-eqz v0, :cond_0

    .line 467
    sget-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;->OnNotiReceived(Ljava/lang/String;)V

    .line 469
    :cond_0
    return-void
.end method


# virtual methods
.method public actionControlReceived(Lcom/samsung/upnp/Action;)Z
    .locals 27
    .param p1, "action"    # Lcom/samsung/upnp/Action;

    .prologue
    .line 114
    const-string v24, "TMSUPnP"

    .line 115
    const-string v25, "TMNotificationServerService. actionControlReceived enter "

    .line 114
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    if-nez p1, :cond_1

    .line 117
    const/16 v21, 0x0

    .line 398
    :cond_0
    :goto_0
    return v21

    .line 119
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mKeyMngr:Landroid/app/KeyguardManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v24

    if-eqz v24, :cond_2

    .line 121
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived phone locked"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 123
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->DEVICE_LOCKED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 122
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 124
    const/16 v21, 0x0

    goto :goto_0

    .line 127
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v5

    .line 129
    .local v5, "actionName":Ljava/lang/String;
    const-string v24, "GetSupportedApplications"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 130
    const-string v24, "TMSUPnP"

    .line 131
    const-string v25, "TMNotificationServerService.actionControlReceived Received GetSupportedApplications action"

    .line 130
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v24, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 135
    .local v10, "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_4

    .line 137
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived ACTION_GET_SUPPORTED_APP failed No ProfileID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 139
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 138
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 180
    :cond_3
    :goto_1
    const/16 v21, 0x0

    goto :goto_0

    .line 142
    :cond_4
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v20

    .line 145
    .local v20, "profileID":I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v12

    .line 146
    .local v12, "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v12, :cond_5

    .line 148
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 149
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 147
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_1

    .line 160
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v7

    .line 161
    .local v7, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v7, :cond_3

    .line 162
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v22

    .line 164
    .local v22, "size":I
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->getStringBuilder()Ljava/lang/StringBuilder;

    move-result-object v8

    .line 165
    .local v8, "appStr":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .line 166
    .local v11, "count":I
    :goto_2
    move/from16 v0, v22

    if-lt v11, v0, :cond_7

    .line 174
    :cond_6
    const-string v24, "AppIDs"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 167
    :cond_7
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v24, "0x"

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 167
    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    add-int/lit8 v11, v11, 0x1

    .line 170
    move/from16 v0, v22

    if-eq v11, v0, :cond_6

    .line 172
    const-string v24, ","

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 182
    .end local v7    # "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "appStr":Ljava/lang/StringBuilder;
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v11    # "count":I
    .end local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v20    # "profileID":I
    .end local v22    # "size":I
    :cond_8
    const-string v24, "SetAllowedApplications"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 183
    const-string v24, "TMSUPnP"

    .line 184
    const-string v25, "TMNotificationServerService.actionControlReceived Received SetAllowedApplications action"

    .line 183
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v24, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 188
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_9

    .line 190
    const-string v24, "TMSUPnP"

    .line 191
    const-string v25, "TMNotificationServerService.actionControlReceived ACTION_SET_ALLOWED_APP failed No ProfileID"

    .line 189
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 195
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 193
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 249
    :goto_3
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 198
    :cond_9
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v20

    .line 201
    .restart local v20    # "profileID":I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v12

    .line 202
    .restart local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v12, :cond_a

    .line 204
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 205
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 203
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_3

    .line 208
    :cond_a
    const-string v24, "AppIDs"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 209
    if-nez v10, :cond_b

    .line 210
    const-string v24, "TMSUPnP"

    .line 211
    const-string v25, "TMNotificationServerService.actionControlReceived ACTION_SET_ALLOWED_APP failed No AppIDs"

    .line 210
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 215
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 213
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_3

    .line 218
    :cond_b
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 220
    .local v6, "appIds":Ljava/lang/String;
    iget-object v0, v12, Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;->mNotiSettings:Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;

    move-object/from16 v17, v0

    .line 221
    .local v17, "notiSettings":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;
    const/4 v9, 0x0

    .line 222
    .local v9, "apps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mDeviceAppManager:Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;

    move-object/from16 v24, v0

    .line 223
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/appmanager/TMSAppManager;->getNotificationSupportedApps()Ljava/util/ArrayList;

    move-result-object v18

    .line 224
    .local v18, "notificationSupportedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v18, :cond_c

    .line 225
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v6, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;->setNotiSupportedApps(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v9

    .line 228
    :cond_c
    if-nez v9, :cond_d

    .line 230
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 231
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 229
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 235
    :cond_d
    const/4 v13, 0x0

    .local v13, "i":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    .restart local v11    # "count":I
    :goto_4
    if-lt v13, v11, :cond_e

    .line 247
    :goto_5
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 236
    :cond_e
    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Integer;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->getNotifier(I)Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;

    move-result-object v19

    .line 237
    .local v19, "notifier":Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    if-nez v19, :cond_f

    .line 239
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 240
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->BAD_APPID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 238
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_5

    .line 243
    :cond_f
    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v0, v1, v9}, Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;->setClient(ILjava/util/ArrayList;)Z

    move-result v23

    .line 245
    .local v23, "startMonitoring":Z
    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;->motinor(Z)Z

    .line 235
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 250
    .end local v6    # "appIds":Ljava/lang/String;
    .end local v9    # "apps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v11    # "count":I
    .end local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v13    # "i":I
    .end local v17    # "notiSettings":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpClientNotificationSettings;
    .end local v18    # "notificationSupportedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v19    # "notifier":Lcom/samsung/android/mirrorlink/appmanager/IUpnpAppEventNotifier;
    .end local v20    # "profileID":I
    .end local v23    # "startMonitoring":Z
    :cond_10
    const-string v24, "GetNotification"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_15

    .line 251
    const-string v24, "TMSUPnP"

    .line 252
    const-string v25, "TMNotificationServerService.actionControlReceived Received SetAllowedApplications action"

    .line 251
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v24, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 256
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_11

    .line 257
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived failed No ProfileID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 260
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 258
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 310
    :goto_6
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 264
    :cond_11
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v20

    .line 267
    .restart local v20    # "profileID":I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v12

    .line 269
    .restart local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v12, :cond_12

    .line 271
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 272
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 270
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_6

    .line 287
    :cond_12
    const-string v24, "NotiID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 288
    if-nez v10, :cond_13

    .line 289
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived failed No NotiID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 292
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 290
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_6

    .line 295
    :cond_13
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v15

    .line 297
    .local v15, "notiID":Ljava/lang/String;
    invoke-static {v15}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->toXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 298
    .local v14, "noti":Ljava/lang/String;
    if-nez v14, :cond_14

    .line 300
    const-string v24, "TMSUPnP"

    .line 301
    const-string v25, "TMNotificationServerService.actionControlReceived Received forming notification xml failed"

    .line 299
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 304
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 302
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_6

    .line 307
    :cond_14
    const-string v24, "Notification"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v14}, Lcom/samsung/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 312
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v14    # "noti":Ljava/lang/String;
    .end local v15    # "notiID":Ljava/lang/String;
    .end local v20    # "profileID":I
    :cond_15
    const-string v24, "InvokeNotiAction"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1c

    .line 313
    const-string v24, "TMSUPnP"

    .line 314
    const-string v25, "TMNotificationServerService.actionControlReceived Received InvokeNotiAction action"

    .line 313
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/16 v21, 0x0

    .line 319
    .local v21, "ret":Z
    const-string v24, "ProfileID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 320
    .restart local v10    # "arg":Lcom/samsung/upnp/Argument;
    if-nez v10, :cond_16

    .line 322
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived failed No ProfileID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 324
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 323
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 389
    :goto_7
    const-string v24, "TMSUPnP"

    .line 390
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "TMNotificationServerService.actionControlReceived exit action ret = "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 391
    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 390
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 389
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    if-nez v21, :cond_0

    .line 393
    const-string v24, "TMSUPnP"

    .line 394
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "TMNotificationServerService.actionControlReceived exit action.getStatusByString() ret = "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 395
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/upnp/Action;->getStatusByString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 394
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 393
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 327
    :cond_16
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getIntegerValue()I

    move-result v20

    .line 330
    .restart local v20    # "profileID":I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/mirrorlink/upnpdevice/TMClientProfileService;->getClientProfileFromId(I)Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;

    move-result-object v12

    .line 331
    .restart local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    if-nez v12, :cond_17

    .line 333
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 334
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_PROFILEID:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 332
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_7

    .line 347
    :cond_17
    const-string v24, "NotiID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 348
    if-nez v10, :cond_18

    .line 350
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived failed No NotiID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 352
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 351
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_7

    .line 355
    :cond_18
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v16

    .line 357
    .local v16, "notiId":Ljava/lang/String;
    const-string v24, "ActionID"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/upnp/Argument;

    move-result-object v10

    .line 358
    if-nez v10, :cond_19

    .line 360
    const-string v24, "TMSUPnP"

    const-string v25, "TMNotificationServerService.actionControlReceived failed No ActionID"

    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 362
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 361
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto/16 :goto_7

    .line 366
    :cond_19
    invoke-virtual {v10}, Lcom/samsung/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 367
    .local v4, "actionId":Ljava/lang/String;
    const-string v24, "TMSUPnP"

    .line 368
    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "TMNotificationServerService.actionControlReceived notiId =  "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 369
    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "actionId= "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 368
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 367
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const/16 v24, 0x2

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x10

    invoke-static/range {v24 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 372
    .local v3, "actId":I
    if-nez v3, :cond_1a

    .line 373
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNotification(Ljava/lang/String;)Z

    move-result v21

    .line 374
    goto/16 :goto_7

    .line 376
    :cond_1a
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->getNotificationFromNotiId(Ljava/lang/String;)Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;

    move-result-object v14

    .line 377
    .local v14, "noti":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    if-nez v14, :cond_1b

    .line 379
    sget-object v24, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorCode()I

    move-result v24

    .line 380
    sget-object v25, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->ACTION_FALIED:Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService$eNOTI_ACTION_ERROR;->getErrorDesc()Ljava/lang/String;

    move-result-object v25

    .line 378
    move-object/from16 v0, p1

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 381
    const/16 v21, 0x0

    .line 382
    goto/16 :goto_7

    .line 383
    :cond_1b
    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v3}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->invokeAction(Ljava/lang/String;I)Z

    move-result v21

    .line 384
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNotification(Ljava/lang/String;)Z

    goto/16 :goto_7

    .line 398
    .end local v3    # "actId":I
    .end local v4    # "actionId":Ljava/lang/String;
    .end local v10    # "arg":Lcom/samsung/upnp/Argument;
    .end local v12    # "cp":Lcom/samsung/android/mirrorlink/upnpdevice/ClientprofileNode;
    .end local v14    # "noti":Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;
    .end local v16    # "notiId":Ljava/lang/String;
    .end local v20    # "profileID":I
    .end local v21    # "ret":Z
    :cond_1c
    const/16 v21, 0x0

    goto/16 :goto_0
.end method

.method public cleanUp()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 490
    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;

    .line 491
    sput-object v0, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mNotiEnabledProfiles:[Z

    .line 492
    return-void
.end method

.method public clearNotiLaunchedApp(I)V
    .locals 3
    .param p1, "appId"    # I

    .prologue
    .line 472
    const-string v0, "TMSUPnP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TMNotificationServerService.clearNotiLaunchedApp enter appId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 473
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 472
    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    invoke-static {p1}, Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotification;->clearNotification(I)Z

    .line 475
    const-string v0, "TMSUPnP"

    const-string v1, "TMNotificationServerService.clearNotiLaunchedApp exit"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method public regForUpnpNotiEvents(Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;)V
    .locals 2
    .param p1, "mUpnpNotificationListener"    # Lcom/samsung/android/mirrorlink/upnpdevice/UpnpNotificationListener;

    .prologue
    .line 446
    const-string v0, "TMSUPnP"

    const-string v1, "TMNotificationServerService.regForUpnpNotiEvents enter"

    invoke-static {v0, v1}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    sput-object p1, Lcom/samsung/android/mirrorlink/upnpdevice/TMNotificationServerService;->mUpnpNotiListener:Lcom/samsung/android/mirrorlink/upnpdevice/IUpnpNotiListener;

    .line 448
    return-void
.end method
