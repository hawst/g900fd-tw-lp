.class public Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;
.super Ljava/lang/Object;
.source "TMProcessMonitor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TMProcessMonitor"

.field private static sProcessMonitorCntr:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized decrementCounter()V
    .locals 4

    .prologue
    .line 37
    const-class v1, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    .line 38
    const-string v0, "TMProcessMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decrementing - Process monitor value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 39
    sget v3, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 38
    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit v1

    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized incrementCounter()V
    .locals 4

    .prologue
    .line 31
    const-class v1, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    .line 32
    const-string v0, "TMProcessMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Incrementing - Process monitor value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 33
    sget v3, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit v1

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized killTMServiceProcess()V
    .locals 3

    .prologue
    .line 44
    const-class v1, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;

    monitor-enter v1

    :try_start_0
    const-string v0, "TMProcessMonitor"

    const-string v2, "killTMServiceProcess - Enter"

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    sget v0, Lcom/samsung/android/mirrorlink/service/TMProcessMonitor;->sProcessMonitorCntr:I

    if-nez v0, :cond_0

    .line 47
    const-string v0, "TMProcessMonitor"

    const-string v2, "killing TMService process"

    invoke-static {v0, v2}, Lcom/samsung/android/mirrorlink/portinginterface/AcsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :cond_0
    monitor-exit v1

    return-void

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
